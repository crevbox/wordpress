<?php global $GlobalMonthsArray; ?>
<div class="qw-gridview">
	<?php while ( have_posts() ) : the_post(); ?>
            <?php

            setup_postdata( $post );

            $url = '';
            if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(60,60)  ) )    
            {
                $url       = $thumb['0'];
            } 
            $e = array(
                'id' =>  $post->ID,
                'date' =>  get_post_meta($post->ID,EVENT_PREFIX.'date',true),
                'location' =>  get_post_meta($post->ID,EVENT_PREFIX.'location',true),
                'street' =>  get_post_meta($post->ID,EVENT_PREFIX.'street',true),
                'city' =>  get_post_meta($post->ID,EVENT_PREFIX.'city',true),
                'permalink' =>  esc_url(get_permalink($post->ID)),
                'title' =>  $post->post_title,
                'thumb' => $url
            );

            if(!array_key_exists("date", $e) || $e['date']==""){
                $e['date'] = "2016-01-01";
            }
       
            $d = explode('-',$e['date']);

            if(array_key_exists(1, $d)){
                $time = mktime(0, 0, 0, $d[1]);
            }

            ?>




            <div class="row qw-archive-item">
                <div class="col-md-3 col-lg-3">
                    <p class="qw-dateblock">
                        <span class="qw-dateday">
                            <?php echo esc_attr($d[2]);?>
                        </span>
                        <span class="qw-datemonth">
                            <?php 
                            $thedate = get_post_meta($post->ID, EVENT_PREFIX.'date', true);

                             $formatteddate = date_i18n(get_option("date_format", "d F "), strtotime( $thedate ));

                            echo date_i18n("F", strtotime( $thedate ));
                            ?>

                          
                        </span>
                        <span class="qw-dateyear">
                            <?php echo esc_attr($d[0]);?>
                        </span>
                    </p>
                </div>
                <div class="col-md-9 col-lg-9">
                    <h4 class="qw-page-subtitle qw-knifetitle qw-top0"> <?php echo esc_attr($e['city']); ?> // <a href="<?php echo esc_url($e['permalink']); ?>" class="qw-coloredtext"><?php echo esc_attr__("Read More", "_s"); ?></a></h4>
                    <hr class="qw-separator">
                    <h1 class="qw-page-title qw-top15">
                        <a href="<?php echo esc_url($e['permalink']); ?>"><?php echo esc_attr($e['title']); ?></a>
                    </h1>
                </div>
            </div>


    <?php endwhile; ?>
</div>

