<?php global $GlobalMonthsArray; ?>
<div class="row qw-gridview">
<?php while ( have_posts() ) : the_post(); ?>
	<div class="col-xs-6 col-sd-6 col-md-4 col-lg-4 qw-archive-item qw-animated">
		<div class="qw-gridview-text qw-negative">
			<h3 class="qw-gridview-title qw-caps text-center qw-negative qw-ellipsis">
				<a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a>
			</h3>
			
			<?php
			$pDate = get_post_meta($post->ID,'_podcast_date',true);
            $date_array = explode('T',$pDate);
            $pDate = explode('-',$date_array[0]);
            if(array_key_exists($pDate[1],$GlobalMonthsArray)){

            	?>
            	<div class="qw-separator-mini"></div>
            	<p class="qw-gridview-date qw-small qw-caps  text-center">	
            	<?php
           		echo esc_attr($pDate[2]).' '.esc_attr($GlobalMonthsArray[$pDate[1]]).' '.esc_attr($pDate[0]);
           		?></p><?php
           	}
			?>
			
		</div>
		<?php if ( has_post_thumbnail( ) ){ ?>
		<a href="<?php esc_url(the_permalink()); ?>" class="qw-gridview-imagelink qw-animated">
			<?php /*the_post_thumbnail( 'gridthumb',array('class'=>'img-responsive') );*/ ?>



			<?php if (has_post_thumbnail( $post->ID ) ): ?>
			  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'gridthumb' ); ?>
			  <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="thumbnail">
			<?php endif; ?>


			<span class="qw-gridview-overlay qw-negative qw-animated">
				<i class="qticon-caret-right qw-animated"></i>
			</span>
		</a>
		<?php } ?>
	</div>
<?php endwhile; // end of the loop. ?>
</div>
