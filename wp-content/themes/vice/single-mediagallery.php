<?php
/**
 * The gallery template file.
 *
 */
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>


	<section id="post-<?php the_ID(); ?>" <?php post_class( 'qw-pagesection' ); ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-lg-8">
								<h1 class="qw-page-title"><?php echo esc_attr(get_the_title()); ?></h1>
							</div>
						</div>
					</header>
					<hr class="qw-separator">
					<div class="qw-page-content">
						<?php get_template_part( 'sharepage' ); ?>

						<div class="qw-thecontent">
								
							
								<div class="Collage effect-parent" data-fx="<?php echo esc_js(esc_attr(get_post_meta($post->ID, 'fx', true))); ?>">
								<?php
									//print_r( $galleryId);
									$events = get_post_meta($post->ID, 'galleryitem', true); 
									//print_r($events);
									if(is_array($events)){
										foreach($events as $event){ 
											$img =  wp_get_attachment_image_src($event['image'],'medium');
										//	print_r($event['video']);
											$link = '';
											if(array_key_exists('video',$event)){
												if($event['video'] != ''){
													$link = $event['video'];
												}
											}
											if($link =='') {
												$img2 =  wp_get_attachment_image_src($event['image'],'large');
												$link = esc_url($img2[0]);
											}
											?>
											<div class="Image_Wrapper" data-caption="<?php echo  esc_js(esc_attr($event['title'])); ?>"><a href="<?php echo esc_url(esc_attr($link)); ?>" class="qw-disableembedding" ><img src="<?php echo esc_url(esc_attr($img[0])) ; ?>" alt="<?php echo esc_attr__("Zoom","_s");?>"/></a></div>				
											<?php
										}
									}
								?>
								</div>
								<div class="qw-top30"></div>
										
						</div>	

						
							

					</div>							
				</div>
				
				<div class="col-md-3">
				<div class="panel-group qw-main-sidebar" id="mainsidebar">
					<div class="qw-colcaption-title qw-negative text-center qw-caps">
						<?php 
						/*
						*
						* Print the title of the widget sidebar, but allow translarions if the title is the same as default
						*/
						$mod = get_theme_mod('QT_sidebar_caption',"Expand");
						if($mod != "Expand"){	
							echo esc_attr($mod);
						}else{
							echo esc_attr__("Expand","_s");
						}
						?>
					</div>	
					<?php dynamic_sidebar( 'right-sidebar' ); ?>
				</div>
				</div>
				
			</div>
		</div>
	</section>

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>