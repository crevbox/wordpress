<?php
/**
 *  Archive template file.
 *
 */
?>
<?php get_header(); ?>
	<section class="qw-pagesection qw-archive">
		<div class="container">
			<div class="row">
				<div class="<?php $c = qw_glob_sidebar_class(); echo esc_attr($c["archive"]);?>">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-8">
								<hr class="qw-separator">
								<h1 class="qw-page-title qw-top15">
							<?php
                            if ( is_category() ) : single_cat_title();
                            elseif ( is_search() ) : printf( esc_attr(__( 'Search Results for: %s', '_s' )), '<span>' . esc_attr(get_search_query()) . '</span>' );
                            elseif ( is_tag() ) : single_tag_title();
                            elseif ( is_author() ) :
                                the_post();
                                printf( esc_attr(__( 'Author: %s', '_s' )), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' );
                                rewind_posts();
                            elseif ( is_day() ) : printf( __( 'Day: %s', '_s' ), '<span>' . esc_attr(get_the_date()) . '</span>' );
                            elseif ( is_month() ) : printf( __( 'Month: %s', '_s' ), '<span>' . esc_attr(get_the_date( 'F Y' )) . '</span>' );
                            elseif ( is_year() ) :  printf( __( 'Year: %s', '_s' ), '<span>' . esc_attr(get_the_date( 'Y' )) . '</span>' );
                            elseif ( is_tax( 'post_format', 'post-format-aside' ) ) : esc_attr__( 'Asides', '_s' );
                            elseif ( is_tax( 'post_format', 'post-format-image' ) ) : esc_attr__( 'Images', '_s');
                            elseif ( is_tax( 'post_format', 'post-format-video' ) ) : esc_attr__( 'Videos', '_s' );
                            elseif ( is_tax( 'post_format', 'post-format-quote' ) ) : esc_attr__( 'Quotes', '_s' );
                            elseif ( is_tax( 'post_format', 'post-format-link' ) ) : esc_attr__( 'Links', '_s' );
                            elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) : esc_attr__( 'Galleries', '_s' );
                            elseif ( is_tax( 'post_format', 'post-format-audio' ) ) : esc_attr__( 'Sounds', '_s' );

                            elseif(is_post_type_archive( 'podcast' ) || is_tax('filter')):  	
                            			$termname = '';
							    		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
							            if(is_object($term)){
							                echo esc_attr($term->name).' ';
							            } 
				            			echo esc_attr(__("Podcasts","_s"));
				             elseif(is_post_type_archive( 'release' ) || is_tax('genre')):  	
                            			$termname = '';
							    		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
							            if(is_object($term)){
							                echo esc_attr($term->name).' ';
							            } 
				            			echo esc_attr(__("Releases","_s"));
				            elseif(is_post_type_archive( 'artist' ) || is_tax('artistgenre')):  	
                            			$termname = '';
							    		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
							            if(is_object($term)){
							                echo esc_attr($term->name.' ');
							            } 
				            			echo esc_attr(__("Artists","_s"));
				            elseif(is_post_type_archive( 'event' ) || is_tax('eventtype')):  	
                            			$termname = '';
							    		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
							            if(is_object($term)){
							                echo esc_attr($term->name).' ';
							            } else {
				            				echo esc_attr(__("Events","_s"));
				            			}
                            else :esc_attr__( 'Archives', '_s' );
                            endif;
                        	?>
						</h1>
							</div>
							<div class="col-sm-12 col-md-12 col-lg-4">
								<?php
									get_template_part( 'sharepage' );
								?>
							</div>
						</div>
						
			            <?php
			                $term_description = term_description();
			                if ( ! empty( $term_description ) ) :
			                    printf( '<div class="taxonomy-description">%s</div>', esc_attr($term_description) );
			                endif;
			            ?>
					</header>
					<div class="qw-separator qw-separator-thin"></div>
					<?php 								
						if ( is_search() ) {
							?><div class="qw-archivesearch qw-darkbg text-center">
								<h3><?php echo __('Search again:','_S');  ?></h3><?php
								get_search_form(); 
							?></div><div class="qw-spacer"></div><?php
						}
						?>
					<div class="qw-page-content">
						<?php
						if(is_post_type_archive( 'podcast' ) || is_tax('filter')){
							get_template_part( 'loop', 'archivepodcast' );
						}else if(is_post_type_archive( 'release' ) || is_tax('genre')){
							get_template_part( 'loop', 'archiverelease' );
						}else if(is_post_type_archive( 'artist' ) || is_tax('artistgenre')){
							get_template_part( 'loop', 'archiveartist' );
						}else if(is_post_type_archive( 'event' ) || is_tax('eventtype')){
							get_template_part( 'loop', 'archiveevent' );
						 }else{
							get_template_part( 'loop', 'archive' );
						 }
						?>
					</div>							
				</div>
				<div class="col-md-1 col-lg-1">
					<?php page_navi(); ?>
				</div>

				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>