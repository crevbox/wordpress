<?php
/*
*
*   Module
*
*/
global $tempEvent; // variables defined in the mother page
$moduleId = $tempEvent['moduleId'];
?>
<div class="qw-section-padding qw-gbcolorlayer">
	<div class="container">

		
		<?php if ($tempEvent['hide_title'] == '' || $tempEvent['hide_title'] == '0'){	?>
			<h1 class="qw-moduletitle"><?php  echo esc_attr(get_the_title( $tempEvent['moduleId'])); ?></h1>	
		<?php } ?>

		<?php
			echo apply_filters( 'the_content', get_post_field('post_content', $moduleId) );
		?>
			
	</div>
</div>