<?php

function qantumpro_create_form_row($name,$type,$value,$label){
	
		switch ($type){
					
					case "section":
						return '
							<h1>'.esc_attr($label).'</h1>';
					break;
					case "text":
						return '
							<p>
								<label for="'.esc_attr($name).'">'.esc_attr($label).'</label><br />
								<input type="text" style="width:98%" value="'.esc_attr($value).'" id="id'.esc_attr($name).'" name="'.esc_attr($name).'" />
							</p>
						';
					break;
					case "link":
						return '
							<p>
								<label for="'.esc_attr($name).'">'.esc_attr($label).'</label><br />
								<input type="text" style="width:98%" value="'.esc_attr($value).'" id="id'.esc_attr($name).'" name="'.esc_attr($name).'" />
							</p>
						';
					break;
					case "medialink":
						return '
							<p>
								<label for="'.esc_attr($name).'">'.esc_attr($label).'</label><br />
								<input type="text" style="width:98%" value="'.esc_attr($value).'" id="id'.esc_attr($name).'" name="'.esc_attr($name).'" />
							</p>
						';
					break;
					case "textarea":
						return '
							<p>
								<label for="'.esc_attr($name).'">'.esc_attr($label).'</label>
								<textarea class="widefat" id="id'.esc_attr($name).'" name="'.esc_attr($name).'" style="height:80px">'.wp_kses_post($value).'</textarea>
							
							</p>
						';
					break;
					
					case 'img':
						return '<p> 
									<label for="'.esc_attr($name).'">'.esc_attr($label).'</label><br /> 
									
									<img src="'.esc_attr($value).'" id="img'.esc_attr($name).'" style="float:left;width:250px;" />
									<input type="text" id="'.esc_attr($name).'" style="width:300px" class="uploader_field" name="'.esc_attr($name).'" value="'.esc_url($value).'" /> 
									<input type="button" class="uploader_element" id="cdog-thickbox'.esc_attr($name).'" name="'.esc_attr($name).'"  value="Upload '.esc_attr($label).'" /><br/> 
									
								</p> <p style="clear:both">&nbsp;</p>';
					;
					case 'file':
						return '<p> 
									<label for="'.esc_attr($name).'">'.esc_attr($label).'</label><br /> 
									
									
									<input type="text" id="'.esc_attr($name).'" style="width:300px" class="uploader_field" name="'.esc_attr($name).'" value="'.esc_url($value).'" /> 
									<input type="button" class="uploader_element" id="cdog-thickbox'.esc_attr($name).'" name="'.esc_attr($name).'"  value="Upload '.esc_attr($label).'" /><br/> 
									
								</p> ';
					;
					
				}
}


add_action( 'admin_print_scripts-post.php', 'cd_opengraph_enqueue' );  
add_action( 'admin_print_scripts-post-new.php', 'cd_opengraph_enqueue' );  
function cd_opengraph_enqueue()  
{  
    wp_enqueue_script( 'cdog-thickbox', THEMEURL. '/custom-types/thickbox-hijack.js', array(), NULL );  
}  


?>