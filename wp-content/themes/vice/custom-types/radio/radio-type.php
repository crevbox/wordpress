<?php

/* = custom post type release
===================================================*/



add_action('init', 'radiochannel_register_type');  


function radiochannel_register_type() {

   
	
	$labelsradio = array(
		'name' => esc_attr__("Radio channels","labelpro"),
		'singular_name' => esc_attr__("Radio channel","labelpro"),
		'add_new' => esc_attr__('Add new channel',"labelpro"),
		'add_new_item' => esc_attr__("Add new radio channel","labelpro"),
		'edit_item' => esc_attr__("Edit radio channel","labelpro"),
		'new_item' => esc_attr__("New radio channel","labelpro"),
		'all_items' => esc_attr__('All radio channels',"labelpro"),
		'view_item' => esc_attr__("View radio channel","labelpro"),
		'search_items' => esc_attr__("Search radio channels","labelpro"),
		'not_found' =>  esc_attr__("No radio channels found","labelpro"),
		'not_found_in_trash' => esc_attr__("No radio channels found in Trash","labelpro"), 
		'parent_item_colon' => '',
		'menu_name' => esc_attr__("Radio channels","labelpro")
	);
	$args = array(
		'labels' => $labelsradio,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'page-attributes' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_menu' => true,
		'menu_icon' => get_template_directory_uri() . '/custom-types/radio/icon.png',
		'supports' => array('title', 'thumbnail','editor', 'page-attributes' )
	); 
    register_post_type( 'radiochannel' , $args );



	/* ============= create custom taxonomy for the releases ==========================*/
	 $labels = array(
    'name' => esc_attr__( 'Radio channel genres','radiochannel' ),
    'singular_name' => esc_attr__( 'Genre','labelpro' ),
    'search_items' =>  esc_attr__( 'Search by genre','labelpro' ),
    'popular_items' => esc_attr__( 'Popular genres','labelpro' ),
    'all_items' => esc_attr__( 'All genres','labelpro' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => esc_attr__( 'Edit genre','labelpro' ), 
    'update_item' => esc_attr__( 'Update genre','labelpro' ),
    'add_new_item' => esc_attr__( 'Add New genre','labelpro' ),
    'new_item_name' => esc_attr__( 'New genre Name','labelpro' ),
    'separate_items_with_commas' => esc_attr__( 'Separate genres with commas','labelpro' ),
    'add_or_remove_items' => esc_attr__( 'Add or remove genres','labelpro' ),
    'choose_from_most_used' => esc_attr__( 'Choose from the most used genres','labelpro' ),
    'menu_name' => esc_attr__( 'Genres','labelpro' ),
  ); 

  register_taxonomy('radio-genre','radiochannel',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'radio-genre' ),

  ));

}




/* = Fields
===================================================*/




			
$radio_details = array(
	
	array(
		'label' => 'Radio title',		
		'id'    => 'radio_title',
		'type'  => 'text'
		),
	array(
		'label' => 'Radio subtitle',		
		'id'    => 'radio_subtitle',
		'type'  => 'text'
		),
	array(
		'label' => 'MP3 Stream URL',		
		'id'    => 'mp3_stream_url',
		'type'  => 'text'
		),
	/*array(
		'label' => 'Main Channel',
		'desc'	=> 'Check to set as main channel', // description
		'id' 	=> 'main_channel',
		'type' 	=> 'checkbox'
	),*/
	array(
		'label' => 'Add to Playlist',
		'desc'	=> 'Check to add', // description
		'id' 	=> 'add_to_custom_playlist',
		'type' 	=> 'checkbox'
	)
	
);

$radiochannel_details_box = new custom_add_meta_box( 'radio_details', 'Radio channel details', $radio_details, 'radiochannel', true );



