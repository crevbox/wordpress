<?php


/*
*
*
*
*				MODULES PAGES SPECIAL FIELDS
*
*
*
*
*/



add_action('init', 'qw_modules');  


if(!function_exists('qw_modules')){
function qw_modules() {

   
	
	$labels = array(
		'name' => ucfirst(__('module','_s').'s'),
		'singular_name' =>  ucfirst(__('module','_s').'s'),
		'add_new' => 'Add new ',
		'add_new_item' => 'Add new '.__('module','_s'),
		'edit_item' => 'Edit '.__('module','_s'),
		'new_item' => 'New '.__('module','_s'),
		'all_items' => 'All '.__('module','_s').'s',
		'view_item' => 'View '.__('module','_s'),
		'search_items' => 'Search '.__('module','_s').'s',
		'not_found' =>  'No module found',
		'not_found_in_trash' => 'No modules found in Trash', 
		'parent_item_colon' => '',
		'menu_name' => ucfirst(__('module','_s').'s')
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true,
		'hierarchical' => true,//deve essere true per poter essere scelto dalla pagina modulare
		'menu_position' => null,
		'page-attributes' => true,
		'show_in_nav_menus' => false,
		'show_in_admin_bar' => false,
		'menu_icon' => get_template_directory_uri() . '/custom-types/qt-modularpages/puzzle.png',
		'supports' => array('title', 'editor' )
	); 
    register_post_type( 'qantummodule' , $args );

}}


$fields_module = array(


      array (
			'label' => 'Module Type',
			'id' => 'module_type',
			'class' => 'qw-conditional-fields',
			'type' => 'chosen',
			'options' => array(
               array('label' => __('Fullscreen caption','_s'),
                     'value' => 'fullscreencaption',
                     'revealfields'=>array("#postdivrich"),
                     'hidefields'=>array('#qw_galleryid','#qw_elements_quantity_container','#qw_sliderid','#qw_hide_past_events')
                     ),
                array('label' => __('Content section','_s'),
                     'value' => 'contentsection',
                     'revealfields'=>array("#postdivrich"),
                     'hidefields'=>array('#qw_galleryid','#qw_sliderid','#qw_elements_quantity_container','#qw_simplesliderid','#qw_hide_past_events')		
                     ),
                array('label' => __('Gallery','_s'),
                     'value' => 'gallery',
                     'revealfields'=>array('#qw_galleryid'),
                     'hidefields'=>array('#qw_elements_quantity_container','#qw_sliderid','#qw_simplesliderid','#qw_hide_past_events')
                     ),
               array('label' => __('Revolution Slider','_s'),
                     'value' => 'revslider',
                     'revealfields'=>array('#qw_sliderid'),
                     'hidefields'=>array('#qw_galleryid','#qw_elements_quantity_container','#qw_simplesliderid',"#postdivrich",'#qw_hide_past_events')
                     ),
               array('label' => __('Simple Slider','_s'),
                     'value' => 'contentslider',
                     'revealfields'=>array('#qw_simplesliderid'),
                     'hidefields'=>array('#qw_galleryid','#qw_elements_quantity_container','#qw_sliderid',"#postdivrich",'#qw_hide_past_events')
                     ),
               array('label' => __('Events map','_s'),
                     'value' => 'events_carousel',
                     'revealfields'=>array('#qw_elements_quantity_container','#qw_hide_past_events'),
                     'hidefields'=>array('#qw_galleryid','#qw_sliderid','#qw_simplesliderid')			                         
                     ),
               array('label' => __('Events carousel','_s'),
                     'value' => 'events_carousel_scroll',
               		 'revealfields'=>array('#qw_elements_quantity_container','#qw_hide_past_events'),
               		 'hidefields'=>array('#qw_galleryid','#qw_sliderid','#qw_simplesliderid',"#postdivrich")
               		 ),
               array('label' => __('Releases carousel','_s'),
                     'value' => 'releases_carousel',
                     'revealfields'=>array('#qw_elements_quantity_container'),
                     'hidefields'=>array('#qw_galleryid','#qw_sliderid','#qw_simplesliderid',"#postdivrich",'#qw_hide_past_events')
                     ),
               array('label' => __('Podcast carousel','_s'),
                     'value' => 'podcast_carousel',
                     'revealfields'=>array('#qw_elements_quantity_container'),
                     'hidefields'=>array('#qw_galleryid','#qw_sliderid','#qw_simplesliderid',"#postdivrich",'#qw_hide_past_events')
                     ),
               array('label' => __('Artists carousel','_s'),
                     'value' => 'artists_carousel',
                     'revealfields'=>array('#qw_elements_quantity_container'),
                     'hidefields'=>array('#qw_galleryid','#qw_sliderid','#qw_simplesliderid',"#postdivrich",'#qw_hide_past_events')
                     ),
               array('label' => __('News','_s'),
                     'value' => 'news_carousel',
               		 'revealfields'=>array('#archivelinkurl','#archivelinktext'),
               		 'hidefields'=>array('#qw_galleryid','#qw_sliderid','#qw_simplesliderid','#qw_elements_quantity_container',"#postdivrich",'#qw_hide_past_events')
               		 ),

               array('label' => __('News carousel','_s'),
                     'value' => 'news_carousel_scroll',
               		 'revealfields'=>array('#archivelinkurl','#archivelinktext', '#qw_elements_quantity_container'),
               		 'hidefields'=>array('#qw_galleryid','#qw_sliderid','#qw_simplesliderid',"#postdivrich",'#qw_hide_past_events')
               		 )
               )

		)

		, array (
			'label' => 'Elements quantity',
			'class' => 'qw-hidden',
			'id' => 'qw_elements_quantity',
			'containerid' => 'qw_elements_quantity_container',
			'type' => 'select',
			'options' => array(
			                   array('label' => '3','value' => '3'),
			                   array('label' => '6','value' => '6'),
			                   array('label' => '9','value' => '9'),
			                   array('label' => '12','value' => '12'),
			                   array('label' => '15','value' => '15'),
			                   array('label' => '18','value' => '18'),
			                   array('label' => '21','value' => '21'),
			                   array('label' => '24','value' => '24'),
			                   array('label' => '24','value' => '27')

			                   )

		)
		, array (
			'label' => 'Hide past events',
			'class' => 'qw-hidden',
			'id' => 'hide_past_events',
			'containerid' => 'qw_hide_past_events',
			'type' => 'checkbox'
		)

		, array (
			'label' => 'Slider ID (number)',
			'class' => 'qw-hidden',
			'id' => 'sliderid',
			'containerid' => 'qw_sliderid',
			'type' => 'text'
		)
		, array (
			'label' => 'Gallery ID',
			'class' => 'qw-hidden',
			'id' => 'galleryid',
			'containerid' => 'qw_galleryid',
			'type' => 'post_chosen',
            'multiple' => true,
            'posttype' =>  'mediagallery'
		)
		, array (
			'label' => 'Slider ID',
			'class' => 'qw-hidden',
			'id' => 'simplesliderid',
			'containerid' => 'qw_simplesliderid',
			'type' => 'post_chosen',
            'multiple' => true,
            'posttype' =>  'contentslide'
		)
		, array (
			'label' => 'Archive link URL',
			'class' => 'qw-hidden',
			'id' => 'archive_link_url',
			'containerid' => 'archivelinkurl',
			'type' => 'text'
		)
		, array (
			'label' => 'Archive link text',
			'class' => 'qw-hidden',
			'id' => 'archive_link_text',
			'containerid' => 'archivelinktext',
			'type' => 'text'
		)
		


	     
	
);
$sample_box_module = new custom_add_meta_box( 'modulesettings', 'Module Settings', $fields_module, 'qantummodule', true );


/*
*
*
*
*				MODULAR PAGES SPECIAL FIELDS
*
*
*
*
*/

$fields = array(
	array( // Repeatable & Sortable Text inputs
		'label'	=> 'Modules', // <label>
		'template' => 'page-modular.php', // only showing with this template chosen
		'id'	=> 'modules', // field id and name
		'type'	=> 'repeatable', // type of field
		'sanitizer' => array( // array of sanitizers with matching kets to next array
			'featured' => 'meta_box_santitize_boolean',
			'title' => 'sanitize_text_field',
			'desc' => 'wp_kses_data'
		),
		
		'repeatable_fields' => array ( // array of fields to be repeated
			'module' => array(
				'label' => 'Module',
				'id' => 'module',
				'type' => 'pageselect',
				//'pagetemplate' => "page-module.php", //not supported by wp
				'posttype' => 'qantummodule'
			),
			array(
				'label' => 'Menu Title',			
				'id' 	=> 'menutitle',
				'type' 	=> 'text'
			),
			array(
				'label' => 'Menu Icon',			
				'id' 	=> 'icon',
				'type' 	=> 'iconchoice'
			),
			array(
				'label' => 'Base color palette',
				'id' 	=> 'qwlightness',
				//'class' => 'qw-conditional-fields',
				'type' 	=> 'select',
				'options' => array(
				                 array('label' => __('Dark','_s'),'value' => 'qw_palette_dark'),
				                array('label' => __('Light','_s'),'value' => 'qw_palette_light')
								
								)
			),
			'background_image' => array(
				'label' => 'Background Image',
				'id' => 'background_image',
				'type' => 'image'
			),
			'overlay_pattern' => array(
				'label' => 'Overlay pattern',
				'id' 	=> 'overlay_pattern',
				'type' 	=> 'checkbox'
			),
			'background_color' => array(
				'label' => 'Background Color (HEX, like #000000)',
				'id' => 'background_color',
				'type' => 'color'
			),
			'background_opacity' => array(
				'label' => 'Background opacity (0 to 100)',
				'desc'	=> '0 to 100', // description
				'id' => 'background_opacity',
				'type' => 'number'
			),
			'opaquemenu' => array(
				'label' => 'Make header bar opaque in this section',
				'id' 	=> 'opaquemenu',
				'type' 	=> 'checkbox'
			),
			'hide_title' => array(
				'label' => 'Hide title',
				'desc'	=> 'Check to hide the title', // description
				'id' 	=> 'hide_title',
				'type' 	=> 'checkbox'
			),
			'hide_module' => array(
				'label' => 'Hide this module',
				'desc'	=> 'Check to soft-disable the module without deleting it.', // description
				'id' 	=> 'hide_module',
				'type' 	=> 'checkbox'
			),
			'hide_mobile' => array(
				'label' => 'Hide on mobile',
				'desc'	=> 'Hide this module on mobile devices: based on user agent. IPAD are considered as desktop.', // description
				'id' 	=> 'hide_mobile',
				'type' 	=> 'checkbox'
			),
			'hide_desktop' => array(
				'label' => 'Hide in desktop',
				'desc'	=> 'The module will be visible only on mobile devices. Based on user agent.', // description
				'id' 	=> 'hide_desktop',
				'type' 	=> 'checkbox'
			)
			
		)
	)
);






/*
*
*
*
*				MODULAR PAGES SETTINGS
*
*
*
*
*/
$pageSettings = array(
     array(
			'label' => 'Background video (http://www.youtube.com/?v=VIDEOID)',
			'id' => 'bgvideo',
			'template' => 'page-modular.php', // only showing with this template chosen
			'type' => 'text'
		),
     array(
			'label' => 'Enable audio of the video',
			'id' => 'bgvideoAudio',
			'desc'	=> 'Check to enable the music of the video', // description
			'template' => 'page-modular.php', // only showing with this template chosen
			'type' 	=> 'checkbox'
		),
     /*array(
			'label' => 'Enable video controls',
			'id' => 'videoControls',
			'desc'	=> 'Check to show the video controls', // description
			'type' 	=> 'checkbox'
		),*/
	// Sorry, no place for controls in this theme
     array(
				'label' => 'Hide footer widgets',
				'desc'	=> 'Check to hide the footer widgets', // description
				'template' => 'page-modular.php', // only showing with this template chosen
				'id' 	=> 'hide_footerwidgets',
				'type' 	=> 'checkbox'
			),

     array(
				'label' => 'Disable fullpage JS',
				'desc'	=> 'The modular page will scroll and behave like a normal long page', // description
				'template' => 'page-modular.php', // only showing with this template chosen
				'id' 	=> 'disable_fullpage_js',
				'type' 	=> 'checkbox'
			),
	
);



$pagesettings = new custom_add_meta_box( 'pagesettings', 'Page Settings', $pageSettings, 'page', true );
$pagemodeules = new custom_add_meta_box( 'modules', 'Modules', $fields, 'page', true );
?>
<?php
	if(is_admin()){

		
		function qw_admin_footer_function() {
			?>
			<div id="qwModalForm"><?php get_template_part("part","icons"); ?><a href="#close" id="qw-closemodal"><span class="qticon-close"></span></a></div>
			<?php
		}
		add_action('admin_footer', 'qw_admin_footer_function');

	}
	if(!function_exists('qw_admin_icons_list')){
		function qw_admin_icons_list(){
			wp_register_style( 'qw-qticons', get_template_directory_uri() . '/css/qticons.css', false, '1.0.0' );

			wp_enqueue_style( 'qw-qticons' );		
		}
	}
	add_action( 'admin_enqueue_scripts', 'qw_admin_icons_list' );
?>