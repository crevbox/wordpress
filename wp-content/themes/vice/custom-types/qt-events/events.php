<?php
if(!defined('CUSTOM_TYPE_EVENT')){
	define ('CUSTOM_TYPE_EVENT','event');
}
define ('EVENT_PREFIX','event');
define ( 'CUSTOM_PLUGIN_DIR_EVENTS', get_template_directory_uri() . '/custom-types/qt-events/' );

/* = import CSS and JS for ADMIN
==========================================================================*/
add_action( 'admin_enqueue_scripts', 'enqueue_qtevents_scripts' );
function enqueue_qtevents_scripts() {	
	
	global $post_type;
    if ( get_current_post_type() == CUSTOM_TYPE_EVENT ) {
		wp_enqueue_script( 'script-events', CUSTOM_PLUGIN_DIR_EVENTS.'/script-admin.js','jquery' , '1.0.0',true);
	}
}

/* = main function 
=========================================*/

function event_register_type() {

	$labelsevent = array(
        'name' => __(ucfirst(CUSTOM_TYPE_EVENT)).'s',
        'singular_name' => __(ucfirst(CUSTOM_TYPE_EVENT)),
        'add_new' => 'Add New ',
        'add_new_item' => 'Add New '.__(ucfirst(CUSTOM_TYPE_EVENT)),
        'edit_item' => 'Edit '.__(ucfirst(CUSTOM_TYPE_EVENT)),
        'new_item' => 'New '.__(ucfirst(CUSTOM_TYPE_EVENT)),
        'all_items' => 'All '.__(ucfirst(CUSTOM_TYPE_EVENT)).'s',
        'view_item' => 'View '.__(ucfirst(CUSTOM_TYPE_EVENT)),
        'search_items' => 'Search '.__(ucfirst(CUSTOM_TYPE_EVENT)).'s',
        'not_found' =>  'No '.CUSTOM_TYPE_EVENT.' found',
        'not_found_in_trash' => 'No '.CUSTOM_TYPE_EVENT.'s found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => __(ucfirst(CUSTOM_TYPE_EVENT)).'s'
    );

  $args = array(
        'labels' => $labelsevent,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
         'menu_icon' => CUSTOM_PLUGIN_DIR_EVENTS.'assets/menu-icon.png',
    	'page-attributes' => true,
    	'show_in_nav_menus' => true,
    	'show_in_admin_bar' => true,
    	'show_in_menu' => true,
        'supports' => array('title','thumbnail','editor','page-attributes'/*,'post-formats'*/)
  ); 

    register_post_type( CUSTOM_TYPE_EVENT , $args );
  
    //add_theme_support( 'post-formats', array( 'gallery','status','video','audio' ) );
	
	 $labels = array(
		'name' => __( 'Event type','labelpro' ),
		'singular_name' => __( 'Types','labelpro' ),
		'search_items' =>  __( 'Search by genre','labelpro' ),
		'popular_items' => __( 'Popular genres','labelpro' ),
		'all_items' => __( 'All events','labelpro' ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Type','labelpro' ), 
		'update_item' => __( 'Update Type','labelpro' ),
		'add_new_item' => __( 'Add New Type','labelpro' ),
		'new_item_name' => __( 'New Type Name','labelpro' ),
		'separate_items_with_commas' => __( 'Separate Types with commas','labelpro' ),
		'add_or_remove_items' => __( 'Add or remove Types','labelpro' ),
		'choose_from_most_used' => __( 'Choose from the most used Types','labelpro' ),
		'menu_name' => __( 'Types','labelpro' ),
	  ); 

	  register_taxonomy('eventtype','event',array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'eventtype' ),
	  ));

    $prefix = EVENT_PREFIX;
    $fields = array(
		 array(
			'label' => 'Date',
			'id'    => EVENT_PREFIX . 'date',
			'type'  => 'date'
		),
		array(
			'label' => 'Street',
			'id'    => EVENT_PREFIX . 'street',
			'type'  => 'text'
		),
		 array(
			'label' => 'City',
			'id'    => EVENT_PREFIX . 'city',
			'type'  => 'text'
		),
		array(
			'label' => 'CAP',
			'id'    => EVENT_PREFIX . 'cap',
			'type'  => 'text'
		), 
		 array(
			'label' => 'Coordinates',
			  'desc'  => 'In google maps do a right click on the event and click "what\'s here" than copy coords from the search box.<br /><br>
			Coords must be written like this: 38.900867,1.419283', // description
			'id'    => EVENT_PREFIX . 'coord',
			'type'  => 'text'
		),

		array(
			'label' => 'Website',
			'id'    => EVENT_PREFIX . 'website',
			'type'  => 'text'
		),
		array(
			'label' => 'Location',
			'id'    => EVENT_PREFIX . 'location',
			'type'  => 'text'
		),
		 array(
			'label' => 'Phone',
			'id'    => EVENT_PREFIX . 'phone',
			'type'  => 'text'
		),
		  array(
			'label' => 'Facebook Event Link',
			'id'    => EVENT_PREFIX . 'facebooklink',
			'type'  => 'text'
		),
		array(
			'label' => 'Add Facebook Event Comments',
			'desc'  => 'Check if you want to add a Facebook comment form',
			'id'    => $prefix . 'fbcomments',
			'type'  => 'checkbox',
			'sanitizer' => 'meta_box_santitize_boolean'
		),
		
		array( // Repeatable & Sortable Text inputs
			'label'	=> 'Ticket Buy Links', // <label>
			'desc'	=> 'Add one for each link to external websites', // description
			'id'	=> $prefix.'repeatablebuylinks', // field id and name
			'type'	=> 'repeatable', // type of field
			'sanitizer' => array( // array of sanitizers with matching kets to next array
				'featured' => 'meta_box_santitize_boolean',
				'title' => 'sanitize_text_field',
				'desc' => 'wp_kses_data'
			),
			'repeatable_fields' => array ( // array of fields to be repeated
				'custom_buylink_anchor' => array(
					'label' => 'Ticket Buy Text',
					'desc'	=> '(example: This website, or Ticket One, or something else)',
					'id' => 'cbuylink_anchor',
					'type' => 'text'
				),
				'custom_buylink_url' => array(
					'label' => 'Ticket Buy URL ',
					'desc'	=> '(example: http://...)', // description
					'id' => 'cbuylink_url',
					'type' => 'text'
				)
			)
		)                       
    );
	

	

    if(post_type_exists(CUSTOM_TYPE_EVENT)){

        if(function_exists('custom_meta_box_field')){
            $sample_box 		= new custom_add_meta_box(CUSTOM_TYPE_EVENT, ucfirst(CUSTOM_TYPE_EVENT).' details', $fields, CUSTOM_TYPE_EVENT, true );
        			}

       
		
    }
	
}

add_action('init', 'event_register_type');  

/* = Visualization
========================================================*/



function do_eventmap($coord,$title){
			$latlong = explode(',',$coord);


?>

	<h2 class="qw-page-subtitle qw-top30"><?php echo esc_attr__("Event Map","_s"); ?></h2><div class="qw-separator"></div>
 	<style>
      
    </style>
   

    <?php
			
			return "
				<div id=\"map\" class=\"qtevent_map\" style=\"width: 100%; height: 370px\"></div> 
				<script type=\"text/javascript\" id=\"qteventscript\" >
				var mylat = ".esc_attr($latlong[0]).";
				var mylon = ".esc_attr($latlong[1]).";
				var nomesede = \"".addslashes($title)."\";
				var locations = [
				  [nomesede, mylat, mylon, 4]
				];
				var map = new google.maps.Map(document.getElementById('map'), {
				  zoom: 16, center: new google.maps.LatLng(mylat, mylon), mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				var infowindow = new google.maps.InfoWindow();
				var marker, i;
			
				for (i = 0; i < locations.length; i++) {  
				  marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					map: map
				  });
				  google.maps.event.addListener(marker, 'click', (function(marker, i) {
					return function() {
					  infowindow.setContent(locations[i][0]);
					  infowindow.open(map, marker);
					}
				  })(marker, i));
				}
			  </script>";
		}
function do_eventcomments($link){
	return '<h2 class=\"qw-page-subtitle qw-top30\">'.esc_attr__("Comments","_s").'</h2><hr class="qw-separator"><div id="fb-root"></div><div class="eventcomments"><script src="http://connect.facebook.net/'.esc_url(get_bloginfo( 'language' )).'/all.js#appId=187479484629057&amp;xfbml=1"></script><fb:comments href="'.esc_url($link).'" num_posts="100" width="550"></fb:comments></div>';

}




if(!is_admin()){
	//add_filter ('the_content','add_event_fields_in_content');
}

//include 'widget.php';
include 'eventslist_by_date.php';
//include 'shortcode.php';