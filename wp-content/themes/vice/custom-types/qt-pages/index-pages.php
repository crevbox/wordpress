<?php

function add_special_fields() {
    $custom_background = array(
		array(
			'label' => 'Custom page background',
			'id' =>  'qw_post_custom_bg',
			'type' => 'image'
		)                    
    );



    $page_options = array(
        array(
            'label' => 'Custom page background',
            'id' =>  'qw_post_custom_bg',
            'type' => 'image'
        )  ,
        array(
            'label' => 'Automatic slideshow',
            'desc' => 'Only applied to archive pages',
            'id' =>  'qw_auto_slideshow',
            'type' => 'checkbox'
        )                    
    );



    if(post_type_exists('page')){
        if(function_exists('custom_meta_box_field')){
            $page_options = new custom_add_meta_box('page_options', 'Page design options', $page_options, 'page', true );
        }
    }

    if(post_type_exists('post')){
        if(function_exists('custom_meta_box_field')){
            $main_box = new custom_add_meta_box('custom_background', 'Custom background', $custom_background, 'post', true );
        }
    }



    if(post_type_exists('release')){
        if(function_exists('custom_meta_box_field')){
            $main_box = new custom_add_meta_box('custom_background', 'Custom background', $custom_background, 'release', true );
        }
    }
    if(post_type_exists('podcast')){
        if(function_exists('custom_meta_box_field')){
            $main_box = new custom_add_meta_box('custom_background', 'Custom background', $custom_background, 'podcast', true );
        }
    }
    if(post_type_exists('artist')){
        if(function_exists('custom_meta_box_field')){
            $main_box = new custom_add_meta_box('custom_background', 'Custom background', $custom_background, 'artist', true );
        }
    }
    if(post_type_exists('event')){
        if(function_exists('custom_meta_box_field')){
            $main_box = new custom_add_meta_box('custom_background', 'Custom background', $custom_background, 'event', true );
        }
    }
    if(post_type_exists('mediagallery')){
        if(function_exists('custom_meta_box_field')){
            $main_box = new custom_add_meta_box('custom_background', 'Custom background', $custom_background, 'mediagallery', true );
        }
    }
}

add_action('init', 'add_special_fields');  
//include 'post_taxonomy-fields.php';