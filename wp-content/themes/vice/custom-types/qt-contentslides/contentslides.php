<?php

define ('CT_CONTENTSLIDE','contentslide');



/* = main function 
=========================================*/

function qtcontentslide_register_type() {

	$labelsoption = array(
        'name' => __("Simple slider",'_s').'s',
        'singular_name' => __("Simple slider",'_s'),
        'add_new' => 'Add New ',
        'add_new_item' => 'Add New '.__("Simple slider",'_s'),
        'edit_item' => 'Edit '.__("Simple slider",'_s'),
        'new_item' => 'New '.__("Simple slider",'_s'),
        'all_items' => 'All '.__("Simple sliders",'_s'),
        'view_item' => 'View '.__("Simple slider",'_s'),
        'search_items' => 'Search '.__("Simple sliders",'_s'),
        'not_found' =>  'No '.__("Simple slider",'_s').' found',
        'not_found_in_trash' => 'No '.__("Simple sliders",'_s').' found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => __("Simple sliders",'_s')
    );

  	$args = array(
        'labels' => $labelsoption,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => get_template_directory_uri() . '/custom-types/qt-contentslides/assets/icon.png',
       // 'menu_icon' => get_template_directory_uri() . '/custom-types/qt-events/.'assets/menu-icon.png',
    	'page-attributes' => false,
    	'show_in_nav_menus' => false,
    	'show_in_admin_bar' => true,
    	'show_in_menu' => true,
        'supports' => array('title','editor')
  	); 

    register_post_type( CT_CONTENTSLIDE , $args );
  
    //add_theme_support( 'post-formats', array( 'gallery','status','video','audio' ) );
	
	$labels = array(
		'name' => __( 'Simple slider type','_s' ),
		'singular_name' => __( 'Types','_s' ),
		'search_items' =>  __( 'Search by type','_s' ),
		'popular_items' => __( 'Popular type','_s' ),
		'all_items' => __( 'All types','_s' ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Type','_s' ), 
		'update_item' => __( 'Update Type','_s' ),
		'add_new_item' => __( 'Add New Type','_s' ),
		'new_item_name' => __( 'New Type Name','_s' ),
		'separate_items_with_commas' => __( 'Separate Types with commas','_s' ),
		'add_or_remove_items' => __( 'Add or remove Types','_s' ),
		'choose_from_most_used' => __( 'Choose from the most used Types','_s' ),
		'menu_name' => __( 'Types','_s' ),
	); 

	

    $fields = array(
        array(
			'label' => 'Autoplay milliseconds',
			'id' =>  'milliseconds',
			'type' => 'text'
		),       
		array( // Repeatable & Sortable Text inputs
			'label'	=> 'Simple slider', // <label>
			'id'	=> 'galleryitem', // field id and name
			'type'	=> 'repeatable', // type of field
			'sanitizer' => array( // array of sanitizers with matching kets to next array
				'featured' => 'meta_box_santitize_boolean',
				'title' => 'sanitize_text_field',
				'desc' => 'wp_kses_data'
			)
			,'repeatable_fields' => array ( // array of fields to be repeated
				'text' => array(
					'label' => 'Content',
					'id' =>  'text',
					'type' => 'textarea'
				),
				'image' => array(
					'label' => 'Image',
					'id' => 'image',
					'type' => 'image'
				)
			)
		)	                       
    );
    if(post_type_exists(CT_CONTENTSLIDE)){
        if(function_exists('custom_meta_box_field')){
            $sample_box 		= new custom_add_meta_box(CT_CONTENTSLIDE, 'Details', $fields, CT_CONTENTSLIDE, true );
        }
    }
	
}

add_action('init', 'qtcontentslide_register_type');  
