<?php

define ('CUSTOMTYPE_GALLERY','mediagallery');



/* = main function 
=========================================*/

function qtgallery_register_type() {

	$labelsoption = array(
        'name' => __("Qantum Gallery",'_s'),
        'singular_name' => __("Qantum Gallery",'_s'),
        'add_new' => 'Add New ',
        'add_new_item' => 'Add New '.__("Qantum Gallery",'_s'),
        'edit_item' => 'Edit '.__("Qantum Gallery",'_s'),
        'new_item' => 'New '.__("Qantum Gallery",'_s'),
        'all_items' => 'All '.__("Galleries",'_s'),
        'view_item' => 'View '.__("Gallery",'_s'),
        'search_items' => 'Search '.__("Galleries",'_s'),
        'not_found' =>  'No '.__("Gallery",'_s').' found',
        'not_found_in_trash' => 'No '.__("Galleries",'_s').' found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => __("Galleries",'_s')
    );

  	$args = array(
        'labels' => $labelsoption,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
         'menu_icon' => get_template_directory_uri() . '/custom-types/qt-gallery/assets/films.png',
    	'page-attributes' => false,
    	'show_in_nav_menus' => true,
    	'show_in_admin_bar' => true,
    	'show_in_menu' => true,
        'supports' => array('title','thumbnail','editor')
  	); 

    register_post_type( CUSTOMTYPE_GALLERY , $args );
  
    //add_theme_support( 'post-formats', array( 'gallery','status','video','audio' ) );
	
	$labels = array(
		'name' => __( 'Gallery type','_s' ),
		'singular_name' => __( 'Types','_s' ),
		'search_items' =>  __( 'Search by type','_s' ),
		'popular_items' => __( 'Popular type','_s' ),
		'all_items' => __( 'All types','_s' ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Type','_s' ), 
		'update_item' => __( 'Update Type','_s' ),
		'add_new_item' => __( 'Add New Type','_s' ),
		'new_item_name' => __( 'New Type Name','_s' ),
		'separate_items_with_commas' => __( 'Separate Types with commas','_s' ),
		'add_or_remove_items' => __( 'Add or remove Types','_s' ),
		'choose_from_most_used' => __( 'Choose from the most used Types','_s' ),
		'menu_name' => __( 'Types','_s' ),
	); 

	

    $fields = array(
                    
		array(
			'label' => 'Effect',
			'class' => 'fx',
			'id' => 'fx',
			'type' => 'select',
			'options' => array(
			                //   array('label' => 'Default','value' => ''),
			                   array('label' => '3D Fade from bottom','value' => 'effect-1'),
			                   array('label' => '3D Fade from bottom','value' => 'effect-2'),
			                   array('label' => '2D Fade from bottom','value' => 'effect-3'),
			                   array('label' => '3D Fade from top','value' => 'effect-4'),
			                   array('label' => '3D Flip','value' => 'effect-5'),
			                   array('label' => 'Zoom in','value' => 'effect-6')
			                   )
					),
		array( // Repeatable & Sortable Text inputs
			'label'	=> 'Gallery', // <label>
			'id'	=> 'galleryitem', // field id and name
			'type'	=> 'repeatable', // type of field
			'sanitizer' => array( // array of sanitizers with matching kets to next array
				'featured' => 'meta_box_santitize_boolean',
				'title' => 'sanitize_text_field',
				'desc' => 'wp_kses_data'
			)
			,'repeatable_fields' => array ( // array of fields to be repeated
				'title' => array(
					'label' => 'Title',
					'id' =>  'title',
					'type' => 'text'
				),
				'video' => array(
					'label' => 'Video',
					'id' =>  'video',
					'description' => 'Youtube or Vimeo url',
					'type' => 'text'
				),
				'image' => array(
					'label' => 'Image',
					'id' => 'image',
					'type' => 'image'
				)
			)
		)	                       
    );
    if(post_type_exists(CUSTOMTYPE_GALLERY)){
        if(function_exists('custom_meta_box_field')){
            $sample_box 		= new custom_add_meta_box(CUSTOMTYPE_GALLERY, 'Details', $fields, CUSTOMTYPE_GALLERY, true );
        }
    }
	
}

add_action('init', 'qtgallery_register_type');  






/**
 *
 *	Gallery shortcode
 *
 * 
 */

if(!function_exists('qtGalleryShortcodeFunc')) {
	function qtGalleryShortcodeFunc($atts) {
		//return 'hello';
		extract( shortcode_atts( array(
				'id' => false
		), $atts ) );
		if($id){

			$events = get_post_meta($id, 'galleryitem', true); 
			//print_r($events);
			//
			$result = '';
			if(is_array($events)){
				$result .= '<div class="Collage effect-parent" data-fx="effect-2" style="opacity: 1;">';
				foreach($events as $event){ 
					$img =  wp_get_attachment_image_src($event['image'],'medium');
				//	print_r($event['video']);
					$link = '';
					if(array_key_exists('video',$event)){
						if($event['video'] != ''){
							$link = $event['video'];
						}
					}
					if($link =='') {
						$img2 =  wp_get_attachment_image_src($event['image'],'large');
						$link = $img2[0];
					}
					$result .='


					<div class="Image_Wrapper" data-caption="'.esc_js(esc_attr($event['title'])).'">
					<a href="'.esc_url($link).'" class="qw-disableembedding" >
					<img src="'.esc_url($img[0]).'" alt="'.esc_attr__("click to zoom","_e").'" /></a></div>	';

				
				}
				$result .= '</div>';
				return $result;
			}
		}
	}
	
}
	
/**
 *
 *	Adding the shortcode to the PHP
 *
 * 
 */


function qt_register_shortcode_gallery_func(){
   add_shortcode('qtgallery', 'qtGalleryShortcodeFunc');
}
add_action( 'init', 'qt_register_shortcode_gallery_func');



