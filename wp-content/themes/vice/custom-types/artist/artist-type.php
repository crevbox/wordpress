<?php
define ('CUSTOM_TYPE_ARTIST','artist');
add_action('init', 'artist_register_type');  
function artist_register_type() {
	$name = CUSTOM_TYPE_ARTIST;
	
	$labels = array(
		'name' => "Artist".'s',
		'singular_name' => "Artist",
		'add_new' => 'Add New ',
		'add_new_item' => 'Add New '."Artist",
		'edit_item' => 'Edit '."Artist",
		'new_item' => 'New '."Artist",
		'all_items' => 'All '."Artist".'s',
		'view_item' => 'View '."Artist",
		'search_items' => 'Search '."Artist".'s',
		'not_found' =>  'No artist found',
		'not_found_in_trash' => 'No artists found in Trash', 
		'parent_item_colon' => '',
		'menu_name' => "Artist".'s'
	);
	

		
    $args = array(
        'labels' => $labels,
        'singular_label' => __(ucfirst(CUSTOM_TYPE_ARTIST)),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'page',
		'has_archive' => true,
		'publicly_queryable' => true,
		'rewrite' => true,
		'query_var' => true,
		'exclude_from_search' => false,
		'can_export' => true,
        'hierarchical' => false,
		'page-attributes' => true,
		'menu_icon' => get_template_directory_uri() . '/custom-types/artist/icon.png',
        'supports' => array('title', 'thumbnail','editor', 'page-attributes' )

    );  
    register_post_type( CUSTOM_TYPE_ARTIST , $args );
	
	$labels = array(
		'name' => __( 'Artist genres','labelpro' ),
		'singular_name' => __( 'Genres','labelpro' ),
		'search_items' =>  __( 'Search by genre','labelpro' ),
		'popular_items' => __( 'Popular genres','labelpro' ),
		'all_items' => __( 'All Artists','labelpro' ).'s',
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Genre','labelpro' ), 
		'update_item' => __( 'Update Genre','labelpro' ),
		'add_new_item' => __( 'Add New Genre','labelpro' ),
		'new_item_name' => __( 'New Genre Name','labelpro' ),
		'separate_items_with_commas' => __( 'Separate Genres with commas','labelpro' ),
		'add_or_remove_items' => __( 'Add or remove Genres','labelpro' ),
		'choose_from_most_used' => __( 'Choose from the most used Genres','labelpro' ),
		'menu_name' => __( 'Genres','labelpro' )
	); 
	register_taxonomy('artistgenre','artist',array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'artistgenre' )
	));
}

/* = meta box 
========================================================================*/

function cd_artist_meta_cb( $post ) 
{
	include 'vars.php';	
	$post_type = get_post_type( $post );
	wp_nonce_field( 'save_'.esc_attr($post_type).'_meta', $post_type.'_nonce' );
	$n=0;
	foreach($fields as $f){
		$f[2] = get_post_meta( $post->ID,  $f[0], true );
		$repeat = false;
		if(isset($f[5])){
			if($f[5]==true){
				$repeat = true;
			}
		}	
		echo qantumpro_create_form_row( $f[0], $f[1], $f[2], $f[3], $n,$repeat);
		$n++;
	}
    echo '<div class="canc">&nbsp;</div>';
}


// dynamic function to call on every custom post type
add_action( 'add_meta_boxes', 'cd_add_artist_meta' );
function cd_add_artist_meta(){
	add_meta_box( 'artist-meta', __( ucfirst('artist').' data' ), 'cd_artist_meta_cb', 'artist', 'normal', 'high' );
}
// ======================== save ====================== 
add_action( 'save_post', 'cd_'.CUSTOM_TYPE_ARTIST.'_meta_save' );
function cd_artist_meta_save( $id )
{
	$typename = CUSTOM_TYPE_ARTIST;
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if( !isset( $_POST[$typename.'_nonce'] ) || !wp_verify_nonce( $_POST[$typename.'_nonce'], 'save_'.esc_attr($typename).'_meta' ) ) return;
	if( !current_user_can( 'edit_post',$id ) ) return;
	$allowed = array(
		'p'	=> array()
	);
	include 'vars.php';
	include get_template_directory().'/custom-types/form_saving.php';
	foreach($fields as $f){
		if(isset($f[0]) && isset($_POST[$f[0]])){
			qantumpro_save_form_row($f[0], $f[1], $_POST[$f[0]], $f[3],$id);
		}
	}

}





$artist_tab_custom = array(
	array(
		'label' => 'Choose first tab',
		'id'    => 'custom_tab_order',
		'type' => 'radio',
		'options' => array(
			array('label' => esc_attr__('Bio','_s'),'value' => 'bio'),
			array('label' => esc_attr__('Music','_s'),'value' => 'mus'),
			array('label' => esc_attr__('Video','_s'),'value' => 'vid'),
			array('label' => esc_attr__('Booking','_s'),'value' => 'boo'),
			array('label' => esc_attr__('Custom content','_s'),'value' => 'cus')
			)


		),
	array(
		'label' => 'Add a custom tab content here',
		'id' => false,
		'type'  => 'chapter'
		),
	array(
		'label' => 'Custom tab title',
		'id'    => 'custom_tab_title',
		'type'  => 'text'
		),
	array(
		'label' => 'Custom tab content',
		'id'    => 'custom_tab_content',
		'type'  => 'editor'
		)
);
$artist_tab_custom_box = new custom_add_meta_box( 'artist_customtab', 'Tabs Customizer', $artist_tab_custom, 'artist', true );



?>