<?php

define ('CUSTOM_TYPE_RELEASE','release');
add_action('init', 'release_register_type');  
function release_register_type() {
	$labelsrelease = array(
		'name' => __(ucfirst(CUSTOM_TYPE_RELEASE)).'s',
		'singular_name' => __(ucfirst(CUSTOM_TYPE_RELEASE)),
		'add_new' => 'Add New ',
		'add_new_item' => 'Add New '.__(ucfirst(CUSTOM_TYPE_RELEASE)),
		'edit_item' => 'Edit '.__(ucfirst(CUSTOM_TYPE_RELEASE)),
		'new_item' => 'New '.__(ucfirst(CUSTOM_TYPE_RELEASE)),
		'all_items' => 'All '.__(ucfirst(CUSTOM_TYPE_RELEASE)).'s',
		'view_item' => 'View '.__(ucfirst(CUSTOM_TYPE_RELEASE)),
		'search_items' => 'Search '.__(ucfirst(CUSTOM_TYPE_RELEASE)).'s',
		'not_found' =>  'No '.CUSTOM_TYPE_RELEASE.' found',
		'not_found_in_trash' => 'No '.CUSTOM_TYPE_RELEASE.'s found in Trash', 
		'parent_item_colon' => '',
		'menu_name' => __(ucfirst(CUSTOM_TYPE_RELEASE)).'s'
	);
	$args = array(
		'labels' => $labelsrelease,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'page-attributes' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_menu' => true,
		'menu_icon' => get_template_directory_uri() . '/custom-types/release/icon.png',
		'supports' => array('title', 'thumbnail','editor' )
	); 
    register_post_type( CUSTOM_TYPE_RELEASE , $args );

	/* ============= create custom taxonomy for the releases ==========================*/
	 $labels = array(
    'name' => __( 'Release genres','labelpro' ),
    'singular_name' => __( 'Genre','labelpro' ),
    'search_items' =>  __( 'Search by genre','labelpro' ),
    'popular_items' => __( 'Popular genres','labelpro' ),
    'all_items' => __( 'All releases','labelpro' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit genre','labelpro' ), 
    'update_item' => __( 'Update genre','labelpro' ),
    'add_new_item' => __( 'Add New genre','labelpro' ),
    'new_item_name' => __( 'New genre Name','labelpro' ),
    'separate_items_with_commas' => __( 'Separate genres with commas','labelpro' ),
    'add_or_remove_items' => __( 'Add or remove genres','labelpro' ),
    'choose_from_most_used' => __( 'Choose from the most used genres','labelpro' ),
    'menu_name' => __( 'genres','labelpro' ),
  ); 
  register_taxonomy('genre','release',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'genre' ),
  ));
}



$prefix = 'track_';

$fields = array(
	
	array( // Repeatable & Sortable Text inputs
		'label'	=> 'Release Tracks', // <label>
		'desc'	=> 'Add one for each track in the release', // description
		'id'	=> 'track_repeatable', // field id and name
		'type'	=> 'repeatable', // type of field
		'sanitizer' => array( // array of sanitizers with matching kets to next array
			'featured' => 'meta_box_santitize_boolean',
			'title' => 'sanitize_text_field',
			'desc' => 'wp_kses_data'
		),
		
		'repeatable_fields' => array ( // array of fields to be repeated
			'releasetrack_track_title' => array(
				'label' => 'Title',
				'id' => 'releasetrack_track_title',
				'type' => 'text'
			),
			'releasetrack_artist_name' => array(
				'label' => 'Artists',
				'desc'	=> '(All artists separated bu comma)', // description
				'id' => 'releasetrack_artist_name',
				'type' => 'text'
			),
			'releasetrack_mp3_demo' => array(
				'label' => 'MP3 Demo',
				'desc'	=> '(Never upload your full quality tracks, someone can steal them)', // description
				'id' => 'releasetrack_mp3_demo',
				'type' => 'file'
			),
			/**/'releasetrack_soundcloud_url' => array(
				'label' => 'Soundcloud or Youtube',
				'desc'	=> 'Will be transformed into an embedded player in the release page', // description
				'id' 	=> 'releasetrack_scurl',
				'type' 	=> 'text'
			),
			'releasetrack_buy_url' => array(
				'label' => 'Buy or download link',
				'desc'	=> 'A link to buy or download the single track', // description
				'id' 	=> 'releasetrack_buyurl',
				'type' 	=> 'text'
			),
			'icon_type' => array(
				'label' => 'Track icon (cart icon is default)',
				'id' 	=> 'icon_type',
				'type' 	=> 'select',
				'default' => 'cart',
				'options' => array(
			                array('label' => 'cart','value' => 'cart'),
			                array('label' => 'download','value' => 'download'),
			                array('label' => 'none','value' => 'none')
			                )
			),
	
		)
	)
);



$fields_links = array(
	
	array( // Repeatable & Sortable Text inputs
		'label'	=> 'Custom Buy Links', // <label>
		'desc'	=> 'Add one for each link to external websites', // description
		'id'	=> $prefix.'repeatablebuylinks', // field id and name
		'type'	=> 'repeatable', // type of field
		'sanitizer' => array( // array of sanitizers with matching kets to next array
			'featured' => 'meta_box_santitize_boolean',
			'title' => 'sanitize_text_field',
			'desc' => 'wp_kses_data'
		),
		'repeatable_fields' => array ( // array of fields to be repeated
			'custom_buylink_anchor' => array(
				'label' => 'Custom Buy Text',
				'desc'	=> '(example: Itunes, Beatport, Trackitdown)',
				'id' => 'cbuylink_anchor',
				'type' => 'text'
			),
			'custom_buylink_url' => array(
				'label' => 'Custom Buy URL ',
				'desc'	=> '(example: http://...)', // description
				'id' => 'cbuylink_url',
				'type' => 'text'
			)
		)
	)
);


	

$fields_release = array(
	
    array(
		'label' => 'Label',
		'id'    => 'general_release_details_label',
		'type'  => 'text'
		),
    array(
		'label' => 'Release date (YYYY-MM-DD)',
		'id'    => 'general_release_details_release_date',
		'type'  => 'date'
		),
    array(
		'label' => 'Buy link',
		'id'    => 'general_release_details_buy_link',
		'type'  => 'text'
		),
    array(
		'label' => 'Buy link text',
		'id'    => 'general_release_details_buy_link_text',
		'type'  => 'text'
		),
    array(
		'label' => 'Catalog Number',
		'id'    => 'general_release_details_catalognumber',
		'type'  => 'text'
		),
);


$details_box = new custom_add_meta_box( 'release_details', 'Release Details', $fields_release, 'release', true );
$sample_box = new custom_add_meta_box( 'release_tracks', 'Release Tracks', $fields, 'release', true );
$buylinks_box = new custom_add_meta_box( 'release_buylinkss', 'Custom Buy Links', $fields_links, 'release', true );



/* = meta box 
========================================================================*/
/*
$current_post_type = CUSTOM_TYPE_RELEASE;
function cd_release_meta_cb( $post ) {
	include 'vars.php';
	$post_type = get_post_type( $post );
	wp_nonce_field( 'save_'.esc_attr($post_type).'_meta', $post_type.'_nonce' );
	foreach($fields as $f){
		$f[2] = get_post_meta( $post->ID,  $f[0], true );
		echo qantumpro_create_form_row( $f[0], $f[1], $f[2], $f[3]);
	}
    echo '<div style="clear:both">&nbsp;</div>';
}

//____________________________________________________________________________________
// dynamic function to call on every custom post type
add_action( 'add_meta_boxes', 'cd_add_release_meta' );
function cd_add_release_meta(){
	add_meta_box( 'release-meta', __( ucfirst('release').' data' ), 'cd_release_meta_cb', 'release', 'normal', 'high' );}

// ======================== save ====================== 
add_action( 'save_post', 'cd_'.esc_attr($current_post_type).'_meta_save' );
function cd_release_meta_save( $id )
{
	$typename = CUSTOM_TYPE_RELEASE;
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if( !isset( $_POST[$typename.'_nonce'] ) || !wp_verify_nonce( $_POST[$typename.'_nonce'], 'save_'.esc_attr($typename).'_meta' ) ) return;
	if( !current_user_can( 'edit_post',$id ) ) return;
	$allowed = array(
		'p'	=> array()
	);
	include 'vars.php';
	include get_template_directory().'/custom-types/form_saving.php';
	foreach($fields as $f){
		if(isset($f[0]) && isset($_POST[$f[0]])){
			qantumpro_save_form_row($f[0], $f[1], $_POST[$f[0]], $f[3],$id);
		}
	}
}

*/

