<?php
/**
 * The main template file.
 *
 */
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

	<section id="post-<?php the_ID(); ?>" <?php post_class( 'qw-pagesection' ); ?>>
		<div class="container">
			<div class="row">
				<div class="<?php $c = qw_glob_sidebar_class(); echo esc_attr($c["content"]);?>">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-lg-10">
								<h1 class="qw-page-title"><?php echo esc_attr(get_the_title()); ?></h1>
								<h4 class="qw-page-subtitle qw-knifetitle"><?php echo esc_attr__("Posted By:","_s")." ".esc_attr(get_the_author())." ".esc_attr__("On:","_s").' '; _s_posted_on(); ?></h4>
							</div>
						</div>
					</header>
					<hr class="qw-separator">
					<div class="qw-page-content qw-top30">
								<div class="row">
									<div class="col-md-12 col-lg-8" id="qwjquerycontent">
										<div class="qw-padded qw-glassbg">
											<p class="qw-categories"><?php echo esc_attr(the_category(' '));  ?></p>
										</div>

										<?php
										if ( has_post_thumbnail( ) ){
											the_post_thumbnail( 'full',array('class'=>'img-responsive') );
										}
										?>	

										<div class="qw-thecontent">
											<?php the_content(); ?>
										</div>	
										<hr class="qw-separator">
										
										<div class="qw-spacer-15"></div>
										<?php if ( comments_open() || '0' != get_comments_number() ){  comments_template(); } ?>
										
										

										<?php
											wp_link_pages( array(
												'before' => '<div class="page-links">' . esc_attr__( 'Pages:', '_s' ),
												'after'  => '</div>',
											) );
										?>
										<div class="qw-spacer-15"></div>
										
									</div>
									<div class="col-md-12 col-lg-4 ">
										<?php get_template_part( 'sharepage' ); ?>
										
										
										<?php
										$tag_list = get_the_tag_list( '', esc_attr__( ' / ', '_s' ) );
										if ( '' != $tag_list ) {
											?>
											<h4 class="qw-page-subtitle"><?php esc_attr__( 'Tags', '_s' ) ?></h4>
											<hr class="qw-separator qw-separator-thin">
											<div class="qw-padded qw-glassbg">
												<p class="qw-tags">
													<?php echo get_the_tag_list( '', esc_attr__( ' / ', '_s' ) ); ?>
												</p>
											</div>
											<?php
										}
										?>
										<div class="qw-spacer-15"></div>
										<?php get_template_part ('inc/related_posts'); ?>

										
									</div>

								</div>

							
						<?php _s_content_nav( 'nav-below' ); ?>
						<div class="qw-spacer"></div>
					</div>							
				</div>
				
				<?php get_sidebar(); ?>
				
			</div>
		</div>
	</section>

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>