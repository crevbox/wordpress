<?php
/*
Template Name: Modular page
*/
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php
	/*
	*
	*
	*	Video background
	*
	*/
	$videoData = '';
	$videocontrols = ' data-controls="0" ';
	$videoaudio = ' data-audio="0" ';
	$vbg = esc_attr(get_post_meta($post->ID, 'bgvideo', true));
	$audio = esc_attr(get_post_meta($post->ID, 'bgvideoAudio', true));
	$controls = esc_attr(get_post_meta($post->ID, 'videoControls', true));


	


	if($vbg != ''){ 
		$videoData = ' data-videourl="'.esc_js(esc_url($vbg)).'" data-audio="'.esc_js(esc_attr($audio)).'" data-controls="'.esc_js(esc_attr($controls)).'" ';
	}



	$DisableFullpageJsClass = '';
	$DisableFullpageJs = esc_attr(get_post_meta($post->ID, 'disable_fullpage_js', true));



	if($DisableFullpageJs == 1) {
		$DisableFullpageJsClass = "qw-normalScrollingPage";
	}
?>

<div id="fullpage" class="<?php echo esc_attr($DisableFullpageJsClass); ?>"  <?php if($vbg != ''){ echo ' data-videourl="'.esc_js(esc_url($vbg)).'" data-audio="'.esc_js(esc_attr($audio)).'" data-controls="'.esc_js(esc_attr($controls)).'" ';} ?>>
	<?php 
	/*
	*
	*	"Module Research" Loop [ID: 1]
	*
	*/
	$events = get_post_meta($post->ID, 'modules', true);   
	$dataAnchors = '';
	$packagesMenu = array();
	if(is_array($events)){
		foreach($events as $event){ 
			if(!isset($event['hide_module'])){
				$event['hide_module'] = '';
			}

			/*
			*
			*	From 2.3.0 you can hide a module on selective devices
			*
			*/
			
			if(!isset($event['hide_mobile'])){
				$event['hide_mobile'] = '';
			}


			if(!isset($event['hide_desktop'])){
				$event['hide_desktop'] = '';
			}

			// needs to be hidden in mobile
			$is_mobile = my_wp_is_mobile();
			if (true == $is_mobile  && $event['hide_mobile'] == '1'){
				$event['hide_module'] = 1;
			}

			// needs to be hidden in desktop
			if ((false == $is_mobile || '' == $is_mobile)   && $event['hide_desktop'] == 1){
				$event['hide_module'] = 1;
			
			}
			


			if($event['module'] != '' && ($event['hide_module'] == '' || $event['hide_module'] == '0')){
				$neededEvents = array('module','title','background_image','background_color','background_opacity','hide_title','hide_module');
				$missing = '';
				foreach($neededEvents as $n){
					if(!array_key_exists($n,$event)){
						$missing .= $n.', '; 
					  	$event[$n] = '';
					}
				}
				global $tempEvent;
				$tempEvent = $event;
				$img =  esc_url(wp_get_attachment_url($event['background_image']));
				$moduleId = $event['module'];
				$tempEvent['moduleId'] = $moduleId;
				

				$module = $event['module']; // ID of the module i'm creating
				$post->currentmodule = $module;
				$module_type = get_post_meta( $module, $key = 'module_type', $single = true );
				
				$post->qw_elements_quantity = get_post_meta( $module, $key = 'qw_elements_quantity', $single = true );
				

				if($module_type != ''){	

					$dataAnchors .= $event['menutitle'].',';	
					$overlay_pattern = '';
					if(array_key_exists('overlay_pattern', $event)){
						$overlay_pattern = " qw-overlaypattern ";
					//	die("UUUU");
					}

					$opaquemenu = '';
					if(array_key_exists('opaquemenu', $event)){
						$opaquemenu = "1";
					}

					if($event['qwlightness'] == ''){
						$event['qwlightness'] = ' qw_palette_dark ';
					}


					$normalslide = '';
					switch ($module_type) {
						case 'gallery':
						case 'contentsection':
						case 'events_carousel':
						$normalslide = ' qwNormalSect ';
						break;

					} 
					

					?>
					<div class="fullPageSection 

						<?php if($DisableFullpageJs == 1) { ?> qw-parallax-background-css <?php } ?>

						<?php 
						echo esc_attr($module_type); 
						echo esc_attr($normalslide); 
						echo esc_attr($overlay_pattern); 
						echo esc_attr($event['qwlightness']); 
						?>" 
						data-bgattachment="fixed"
						data-opaqueMenu="<?php echo esc_attr($opaquemenu); ?>"
						data-anchor="<?php echo esc_attr(qw_slugify($event['menutitle'], $moduleId)); ?>"
						id="<?php if($DisableFullpageJs == 1 || $is_mobile) { echo esc_attr(qw_slugify($event['menutitle'], $moduleId)); } else { echo 'section'.esc_attr($moduleId); }?>" 
						data-bgcolor="<?php echo esc_attr($event['background_color']); ?>" 
						data-bgopacity="<?php echo esc_attr($event['background_opacity']); ?>" 
						data-bgimage="<?php if($img != '') {echo esc_js(esc_url($img)); }?>"

						<?php if($DisableFullpageJs == 1) { ?>data-type="background"<?php } ?>

						>
						<div class="qw-section-content">
							<?php
							get_template_part( 'module', $module_type );
							?>
						</div>
					</div>
					<?php
					$packagesMenu[] = array('title' => $event['menutitle'], 'link' => qw_slugify($event['menutitle'], $moduleId), 'icon' =>  $event['icon']);
				}
				$tempEvent = array();
			}//if module is defined
		}//foreach
	}//if
	?>
	<?php
	if(get_post_meta($post->ID, 'hide_footerwidgets', true) != '1'){
	?>
	<div class="<?php if($DisableFullpageJs != 1) {  ?> <?php } ?> fullPageSection qw-footer-container qwNormalSect" data-anchor="footer" id="<?php if($DisableFullpageJs == 1 || $is_mobile) { echo esc_attr('footer'); } else { echo 'section-footer'; } ?>" data-opaqueMenu="1">
		
		<div class="qw-fixedcontents " >
			<div class="qw-section-content">
				<div class="qw-vp">
					<div class="qw-vc">
						<?php get_template_part( 'part', 'footerwidgets'); ?>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<?php
	}
	?>

</div>
<div data-slideanchors="<?php echo esc_js(esc_attr($dataAnchors)); ?>" id="onepageAnchors"></div>

<?php
/*
*
*	Big slides navigation menu
*
*/
//if(!my_wp_is_mobile()){
?>
<div class="qw-vp fpMenuContainer">
	<div class="qw-vc">
		<ul class="qw-sections-menu fullpageMenu qw-animated" id="qw-nav">
			<?php
			foreach($packagesMenu as $p){
				?>
				<li>
					<a href="#<?php echo esc_attr($p['link']);?>" class="qwsmoothscroll" data-placement="right" data-toggle="tooltip" title="<?php echo esc_attr($p['title']);?>" data-menuanchor="<?php echo esc_attr($p['link']);?>"><span class="<?php echo esc_attr((($p['icon'] != '')? $p['icon']: "qticon-record")); ?>"></span></a>
				</li>
				<?php
			}
			?>
			<?php if(get_post_meta($post->ID, 'hide_footerwidgets', true) != '1'){ ?>
			<li>
				<a href="#footer"  class="qwsmoothscroll" data-placement="right" data-toggle="tooltip" title="<?php echo esc_attr__("More","_s"); ?>" data-menuanchor="footer"><span class="qticon-record"></span></a>
			</li>
			<?php } ?>
		</ul>
	</div>
</div>
<!-- end of the fpMenuContainer -->
<?php
//}
?>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>