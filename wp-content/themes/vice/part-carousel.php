<div id="autocarousel" class="carousel slide automaticcarousel" data-ride="carousel">
  
 
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
   

    <?php 
    $active = 'active';
    $n = 0;
    while ( have_posts() ) : the_post();  $n++;?>

        <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array(2000, 800), true, '' ); ?>
        <div class="item <?php echo $active; $active = ''; ?>" data-bgimage="<?php echo $src[0]; ?>" data-bgcolor="#000000" data-bgopacity="60">
            <div class="qw-gbcolorlayer" >
                 <div class="carousel-caption">
                
               
                    <h2>
                        <a href="<?php the_permalink(); ?>">
                            <?php  
                            //the_post_thumbnail( 'thumbnail',array('class'=>'img-responsive ') );   
                            ?>
                            <?php the_title(); ?>
                        </a>
                    </h2>
                </div>
            </div>
        </div>

    <?php endwhile; // end of the loop. ?>



    <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php 
    $active = 'active';
    for ($i = 0; $i < $n; $i++){ ?>
    <li data-target="#autocarousel" data-slide-to="<?php $i ?>" class="<?php echo $active; $active = ''; ?>"></li>
 
    <?php } ?>
  </ol>

  </div>
 
  <!-- Controls -->
  <a class="left carousel-control" href="#autocarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#autocarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div> <!-- Carousel -->