<?php 
//header('X-UA-Compatible: IE=edge,chrome=1'); 
$mobile = 'qw-is_desktop';
if(my_wp_is_mobile()){
    $mobile = 'qw-is_mobile';
}

$DisableFullpageJsClass = '';
if(is_page()){
   $DisableFullpageJs = esc_attr(get_post_meta($post->ID, 'disable_fullpage_js', true));
   if($DisableFullpageJs){
        $DisableFullpageJsClass = 'qw-disableFullpageBody';
   }
}


$automatic_slideshow = '';
if(is_page() && get_post_meta(get_the_id(), 'qw_auto_slideshow', true)){

    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
    elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
    else { $paged = 1; }

    if($paged == 1){
        $automatic_slideshow = "automatic_slideshow";
    }
    
}


?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="<?php echo esc_attr( get_bloginfo( 'charset') ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body  <?php body_class(array("qw-".esc_attr(qt_menutype()), $mobile, $DisableFullpageJsClass, $automatic_slideshow )); ?> id="theBody" data-qwajx="<?php echo get_theme_mod('QT_ajaxPageLoad', "0"); ?>" data-maincolor="<?php echo esc_attr(get_theme_mod('QTcol', "#ffcc00")); ?>" data-jsdebug="<?php echo esc_attr(get_theme_mod('qt_js_debug', "0")); ?>">
    	
        <?php do_action('qt_after_body'); ?>
        <div class="qw-totalcontainer" id="content">
            <div class="qw-content-wrapper" id="canvas" >
            <?php get_template_part('part','custombackground'); ?>