<?php
/*
Template Name: Woocommerce page
*/
?>
<?php get_header(); ?>

<?php
//if ( have_posts() ) : while ( have_posts() ) : the_post();

?>
	<section id="post-<?php the_ID(); ?>" <?php post_class( 'qw-pagesection qw-woocommerce' ); ?>>
		<div class="container">
			<div class="row">

				<div class="col-md-9">

					


					
					<div class="qw-thecontent">
						<?php woocommerce_content(); ?>
					</div>	
					<?php
						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_attr__( 'Pages:', '_s' ),
							'after'  => '</div>',
						) );
					?>						
				</div>
				
				<div class="col-md-3">
					<div class="panel-group qw-main-sidebar" id="mainsidebar">
						<div class="qw-colcaption-title qw-negative text-center qw-caps">
							<?php 
							/*
							*
							* Print the title of the widget sidebar, but allow translarions if the title is the same as default
							*/
							$mod = get_theme_mod('QT_sidebar_caption',"Expand");
							if($mod != "Expand"){	
								echo esc_attr($mod);
							}else{
								echo esc_attr__("Expand","_s");
							}
							?>
						</div>	
						<?php dynamic_sidebar( 'right-sidebar' ); ?>
					</div>
				</div>
				
			</div>
		</div>
	</section>
<?php 
//endwhile; endif; // end of the loop. ?>
<?php get_footer(); ?>