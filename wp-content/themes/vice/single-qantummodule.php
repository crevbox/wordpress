<?php
/*
preview single modules
*/
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section id="post-<?php the_ID(); ?>" <?php post_class( 'qw-pagesection' ); ?>>
	<div class="container">
		<header class="qw-page-header">
			<div class="row">
				<div class="col-sm-12 col-lg-8">
					<h1 class="qw-page-title"><?php echo esc_attr(get_the_title()); ?></h1>
				</div>
			</div>
		</header>
		<div class="qw-separator qw-separator-thin"></div>
		<div class="qw-page-content">						
			<?php
			$tag_list = get_the_tag_list( '', __( ' / ', '_s' ) );
			if ( '' != $tag_list ) {
				?><p class="qw-tags"><?php
				echo '<strong>'.esc_attr__( 'Tagged as: ', '_s' ).'</strong><br>';
				echo get_the_tag_list( '', __( ' / ', '_s' ) );
				?></p><div class="qw-separator-superthin"></div><?php
			}
			?>
			<div class="qw-thecontent">
				<?php the_content(); ?>
			</div>	

			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_attr__( 'Pages:', '_s' ),
					'after'  => '</div>',
				) );
			?>
		</div>
	</div>
</section>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>