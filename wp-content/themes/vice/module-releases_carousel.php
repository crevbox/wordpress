<?php
/*
*
*   Module
*
*/
global $post; // the main object of the modular page containing this module
global $GlobalMonthsArray; 
global $tempEvent; // variables defined in the mother page
$moduleId = esc_attr($tempEvent['moduleId']);
$qw_elements_quantity = esc_attr(get_post_meta( $post->currentmodule, $key = 'qw_elements_quantity', $single = true ));
$qw_lightness = esc_url($tempEvent['qwlightness']);
?>
<div class="qw-vp qw-gbcolorlayer">
	<div class="qw-vc">
		<div class="container">
			<?php 
			if ($tempEvent['hide_title'] == '' || $tempEvent['hide_title'] == '0'){	?>
			<h1 class="qw-moduletitle"><?php  echo esc_attr(get_the_title( $tempEvent['moduleId'])); ?></h1>	
			<?php } ?>
			<div class="row qw-itemscarousel qw-releasecarousel">		
				<?php
				/*
				*
				*
				*
				*	Start of the carousel
				*
				*
				*/
				?>
				<div class="col-sm-12 col-md-10 col-md-offset-1  col-lg-10 col-lg-offset-1">
					<?php 

					$args  = array(
					    'post_type' => 'release',
					    'post_status' => 'publish',
					    'posts_per_page'=>$qw_elements_quantity
					    ,'suppress_filters' => false
					);
					//$events = get_posts($args); 




					$wpbp = new WP_Query($args ); 
					$wp_query = $wpbp; ?>
					




					
					    <div id="carouselevents<?php echo esc_attr($moduleId);?>" class="carousel slide qw-itemscarousel">
						    
						    <!-- Wrapper for slides -->
						    <div class="carousel-inner">
						        <?php
						            $active = 'active';
						            $col = 0;

						            /*
						            if($qw_elements_quantity > count($events)){$qw_elements_quantity = count($events); }


						            for($i = 0; $i < $qw_elements_quantity; $i++){
									*/
						            $i = 0;
						            if ($wpbp->have_posts()) : while ($wpbp->have_posts()) : $wpbp->the_post(); 



						                $page = $post;
						                setup_postdata( $page );
						                $image  = '';
						                if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), array(500,500)  ) )  {
						                    $image       = $thumb['0'];// '<img src="'.esc_url($thumb['0']).'" class="img-responsive hidden-xs" alt="'.esc_attr($page->post_title).'">';
						                } 
						                $e = array(
						                'id' =>  $page->ID,
						                'date' =>  esc_attr(get_post_meta($page->ID,'general_release_details_release_date',TRUE)),
						                'permalink' =>  esc_url(get_permalink($page->ID)),
						                'title' =>  esc_attr($page->post_title),
						                'thumb' => $thumb
						                );
						                if($e['date'] != '') {
							                $d = explode('-',$e['date']);
							                $time = mktime(0, 0, 0, $d[1]);
							                $pDate = $e['date'];
							                 $date_array = explode('T',$pDate); 
							                $pDate = explode('-',$date_array[0]);
							                if(array_key_exists($pDate[1],$GlobalMonthsArray)){
							                    $e['releasedate'] = esc_attr($pDate[2]).' '.esc_attr($GlobalMonthsArray[$pDate[1]]).' '.esc_attr($pDate[0]);
							                }
						                } else {
						                	$e['releasedate'] = "";
						                }
						                /*
						                *
						                *   Column
						                *
						                */
						                if ($i == 0 || ($i % 3 == 0)){ 
						                     echo '<div class="item '.esc_attr($active).'"><div class="row">';
						                    $open = 1;
						                    
						                }
						                ?>
						                <div class="col-xs-12 col-sm-4  col-md-4">
						                    <div data-link="<?php echo esc_js(esc_url($e['permalink'])); ?>" class="qw-itemscarousel-item">
						                        <div class="row">					                         
						                            <div class="col-xs-12 col-md-12 qw-hideoverflow qw-dynamiccovers">
						                                <div class="qw-cover-fx-container">
							                                <?php
							                                	echo '<img src="'.esc_url($image).'" class="img-responsive hidden-xs" alt="'.esc_attr($page->post_title).'">';
							                                ?>
							                                <div class="qw-cover-actions qw-animated qw-verydark">
							                                	<a href="#" class="qticon-play" data-addtoplaylyst data-releasedata="<?php echo esc_js(esc_url(add_query_arg( array('qw_get_json_data' => '1'), $e['permalink'] )));?>"  data-toggle="tooltip" title="<?php echo esc_attr__('Add to playlist','_s'); ?>"></a>
							                                	<a href="#" class="qticon-search qwjquerycontent" data-qwjquerycontent="<?php echo esc_js(esc_url($e['permalink']));?>" data-toggle="tooltip" title="<?php echo esc_attr__('Tracklist','_s'); ?>"></a>
							                                </div>

							                                <div class="qw-itemscarousel-text qw-cover-fx qw-animated qw-negative">
							                                	 <p class="qw-itemscarousel-title qw-caps">
							                                   		<a data-toggle="tooltip" title="<?php echo esc_attr__('Go to page','_s'); ?>" href="<?php echo esc_url($e['permalink']); ?>"> <?php echo esc_attr($e['title']); ?></a>
								                                </p>
								                                <div class="qw-separator-mini hidden-xs qw-animated"></div>
								                                <p class="qw-itemscarousel-detail qw-small qw-caps"> 
								                                    <?php echo esc_attr($e['releasedate']); ?>
								                                </p>
							                                </div>
							                            </div>
						                            </div>
						                        </div>					                        
						                    </div>
						                </div>
						                <?php 
						                if  (($i+1) % 3 == 0){ 
						                    echo '</div></div>';
						                    $open = 0; 
						                    $col++;
						                } 
						                $active = ''; 

						                $i ++;

						            endwhile;endif;
						        //}/*foreach*/ 





						    
						    if(isset($open)){
						      if($open == 1) { 
						        ?> 
						        </div> </div>
						        <?php 
						        $col++; 
						      }
						    } 

						    ?>
					    </div>				   
					</div>
					
					</div>				
				<?php
				wp_reset_postdata();
				wp_reset_query();

				/*
				*
				*
				*
				*	End of the carousel
				*
				*
				*
				*/
				?>
			</div>
		</div>
		<!-- Indicators -->
		<?php
		 if($i > 3){
		?>
	    <ol class="carousel-indicators" id="carouselindicators<?php echo esc_attr($moduleId);?>">
	    <?php
	      $active = 'active';
	      for($c = 0; $c < ($i/3); $c++){
	       
	        ?>
	        <li data-target="#carouselevents<?php echo esc_js(esc_attr($moduleId));?>" data-slide-to="<?php echo esc_js(esc_attr($c)); ?>" class="<?php echo esc_attr($active); ?>"></li>
	        <?php
	        $active = '';
	      }
	      ?>
	    </ol>
	    <?php
		}
	    ?>


	    <?php
		if($qw_elements_quantity > 3){
		?>
		<a href="#carouselevents<?php echo esc_js(esc_attr($moduleId));?>" class="qw-itemscarousel-control left qw-animated" role="button" data-slide="prev">
			<span class="qticon-chevron-light-left"></span>
		</a>
		<a href="#carouselevents<?php echo esc_js(esc_attr($moduleId));?>" class="qw-itemscarousel-control right qw-animated"  role="button" data-slide="next">
			<span class="qticon-chevron-light-right"></span>
		</a>     
		<?php
		}
		?>


    </div>
</div>





	




