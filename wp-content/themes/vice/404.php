<?php
/*

*/
?>
<?php get_header(); ?>


	<section id="post-<?php the_ID(); ?>" <?php post_class( 'qw-pagesection' ); ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-md-12 ">
								<hr class="qw-separator">
								<h1 class="qw-page-title qw-top15"><?php echo esc_attr__("404: Sorry, this page doesn't exist", "_s"); ?></h1>
							</div>
							
						</div>
					</header>
					<hr class="qw-separator qw-separator-thin">
					<div class="qw-page-content">
						<div class="row">
							
							<div class="col-md-12">
								
										<?php
										query_posts('post_type=post&post_status=publish&posts_per_page=10&paged='. get_query_var('paged'));
										get_template_part( 'loop', 'archive' );
										?>
										<div class="qw-thecontent">
											
										</div>	
										
						
							</div>
							
						</div>

					</div>							
				</div>
				
				<div class="col-md-3">
					<div class="panel-group qw-main-sidebar" id="mainsidebar">
						<div class="qw-colcaption-title qw-negative text-center qw-caps">
							<?php 
							/*
							*
							* Print the title of the widget sidebar, but allow translarions if the title is the same as default
							*/
							$mod = get_theme_mod('QT_sidebar_caption',"Expand");
							if($mod != "Expand"){	
								echo esc_attr($mod);
							}else{
								echo esc_attr__("Expand","_s");
							}
							?>
						</div>	
						<?php dynamic_sidebar( 'right-sidebar' ); ?>
					</div>
				</div>
				
			</div>
		</div>
	</section>

<?php get_footer(); ?>