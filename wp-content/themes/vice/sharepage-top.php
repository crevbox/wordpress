<?php
	wp_reset_postdata();
	global $post;
	$data_media = '';
	if(is_singular()) {
		$image = qw_get_thumbnail_url($post->ID);
		$data_media = '';
		if($image){
			$data_media = $image;
		}

		$title = get_the_title($post->ID);
		$data_title = '';
		if($title){
			$data_title = $title;
		}

		$description = get_excerpt_by_id($post->ID);
		$data_description = '';
		if($description){
			$data_description = str_replace("\n", " ", wp_strip_all_tags($description));
		}
		$permalink = get_the_permalink();

	} else {
		$image = '';
		$data_title = '';
		$data_description = '';
		$permalink = get_the_permalink();
	}


	/*

	Update 04 10 2015 added @via twitter auto detected from customizer options


	*/
	$twitter_channel = '';
	global $optarray;
	if(array_key_exists('QT_twitter', $optarray)){
		$twitter_url = $optarray['QT_twitter'];
		$twarr = explode('/', $twitter_url);
		$twitter_url =$twarr[count($twarr) - 1];
		$twitter_channel = $twitter_url;
	}
	
	

?>
<a href="#" class="qw-shareall" id="qwShareall" data-toggleclass="open" data-target="qwShareallC" data-toggleclass2="hidden-xs" data-target2="qwLogo">
    <span class="qticon-share"></span>
</a>
<div class="qw-sharepage-top qw-animated" id="qwShareallC">
	<a href="#" data-type="twitter" 
	data-url="<?php echo esc_js(esc_url($permalink)); ?>" 	 
	data-description="<?php 	echo esc_js(esc_attr($data_description)); ?>" 
	data-via="<?php 	echo esc_js(esc_attr($twitter_channel)); ?>"	
	data-media="<?php 	echo esc_js(esc_attr($data_media)); ?>" 
	class="prettySocial" ><span class="qticon-twitter"></span></a>

	<a href="#" id="qtFacebookShareButton"
	data-type="facebook" 
	data-url="<?php echo esc_js(esc_url($permalink)); ?>" 	 
	data-description="<?php 	echo esc_js(esc_attr($data_description)); ?>"
	data-media="<?php 	echo esc_js(esc_attr($data_media)); ?>" 
	class="prettySocial"><span class="qticon-facebook"></span></a>

	<a href="#" 
	data-type="googleplus" 
	data-url="<?php echo esc_js(esc_url($permalink)); ?>" 
	data-description="<?php 	echo esc_js(esc_attr($data_description)); ?>" 
	data-media="<?php 	echo esc_js(esc_attr($data_media)); ?>" 
	class="prettySocial" 	><span class="qticon-googleplus"></span></a>

	<a href="#" data-type="pinterest" 
	data-url="<?php echo esc_js(esc_url($permalink)); ?>"  
	data-description="<?php 	echo esc_js(esc_attr($data_description)); ?>"  
	class="prettySocial" data-media="<?php 	echo esc_js(esc_attr($data_media)); ?>" ><span class="qticon-pinterest"></span></a>

</div>