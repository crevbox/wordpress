<?php


if(!function_exists('qp_get_group')){
function qp_get_group( $group_name , $post_id = NULL ){
  global $post; 	  
 if(!$post_id){ $post_id = $post->ID; }
  $post_meta_data = get_post_meta($post->ID, $group_name, true);  
  return $post_meta_data;
}}

add_filter( 'the_category', 'fix_html5_error_by_wp' );
function fix_html5_error_by_wp( $text) {
    $text = str_replace('rel="category tag"', '', $text);
    return $text;
}












// get_artist_tracks: used in the artist's pages, is needed to automatically find all the tracks by this artist and create the link to the tracks in his page
if(!function_exists('get_artist_tracks')){
function get_artist_tracks( $name ){
	wp_reset_postdata();
	$result         = '';
	$args           = array(

		'post_type' => 'release',
		 'posts_per_page' => 200,
 		 'meta_query' => array(
				array(
					'key' => 'track_repeatable',
					'value' => $name,
					'compare' => 'LIKE'
				)
			)
	);
	$the_query_meta = new WP_Query( $args );
	global $post;
	while ( $the_query_meta->have_posts() ):
		$the_query_meta->the_post();
		setup_postdata( $post );
		$thumbcode = '';
		if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(
			 '100',
			'100' 
		) ) ) {

			$url       = $thumb[ '0' ];
			$thumbcode = '<img class="qw-artist-release-img" src="' . esc_url(esc_attr($url)) . '" alt="' . esc_attr($post->post_title) . '" />';
		} 
		$result .= '<li><a href="' . esc_url(get_permalink( $post->ID )) . '" class="qw-author-release qw-animated qw-darkbg">' . $thumbcode . esc_attr($post->post_title) . '</a></li>';
	endwhile;
	wp_reset_postdata();
	return $result;
}}


// qw_permalink_by_name: this functions is needed to find the permalink to an artist by searching for specific post type with the name as title
function qw_permalink_by_name( $name, $posttype = 'artist' ){
	return esc_url( get_permalink( get_page_by_title($name , $output = 'OBJECT', $posttype )));
}

function sanitizeMagicfieldsExternalUrl( $url ){
	$urlar = explode( '://', $url );
	return 'http://' . $urlar[ count( $urlar ) - 1 ];
}




//====================================== creation of social icons in the artist pages =======================
 function social_link_icons($id){
	$res = '';

	$links_array=array(  
		'_artist_website' 	=> 'qticon-earth',
		'_artist_facebook'	=>'qticon-facebook',
		'_artist_beatport'	=>'qticon-beatport',
		'_artist_hearthis'	=>'qticon-hearthis',
		'_artist_soundcloud'=>'qticon-soundcloud',
		'_artist_mixcloud'	=>'qticon-mixcloud',
		'_artist_myspace'	=>'qticon-myspace',
		'_artist_instagram'	=>'qticon-instagram',
		'_artist_residentadv'=>'qticon-resident-advisor',
		'_artist_twitter'	=>'qticon-twitter'
	);		
	foreach ($links_array as $l => $r){
		$valore = get_post_meta( $id,  $l, true );
		//echo $id;
		if($valore!=''){
			$res .= '<li><a target="_blank" rel="external nofollow" href="'.esc_url($valore).'" class="'.esc_attr($r).' qw-disableembedding" ></a></li>';
		}
	}
	return $res;
}




?>