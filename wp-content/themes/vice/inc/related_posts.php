<?php
global $options;
global $post;
$tags = wp_get_post_tags($post->ID);
$col_class  =   4;
$no_rows    =   3;
if(array_key_exists('content_class', $options)){
  if($options['content_class']=='col-md-12'){
      $col_class  =   3;
      $no_rows    =   4;
  }
}

wp_reset_query();
$my_query = '';
if ($tags) {
       
    $first_tag = $tags[0]->term_id;
    $args = array(
        'tag__in'       => array($first_tag),
        'post__not_in'  => array($post->ID),
        'showposts'     => $no_rows,
        'meta_query'    => array(
                                array(
                                    'key' => '_thumbnail_id',
                                    'compare' => 'EXISTS'
                                ),
                            )
    );    
    $my_query = new WP_Query($args);
 } else {
    $args = array(
        'post_per_page' => 3,
        'post_type' => 'post',
        'post_status' => 'publish'
    );
    $my_query = new WP_Query($args);
}
if(!$my_query->have_posts()){
    $args = array(
        'post_per_page' => 3,
        'post_type' => 'post',
        'post_status' => 'publish'
    );
    $my_query = new WP_Query($args);

}


        
if ( $my_query->have_posts() ) { ?>	

  <div class="qw-related-posts hidden-xs"> 
        <h3 class="qw-page-subtitle"><?php echo esc_attr__('Related Posts', '_s'); ?></h3>   
        <hr class="qw-separator qw-separator-thin">
        <div class="row">
                <?php
                $n = 0;
                while ($my_query->have_posts() && $n < 3) {
                   $my_query->the_post();
                   if(has_post_thumbnail() ){
                    get_template_part('inc/related_post_unit');   
                   }
                   $n++;
                } //end while
                ?>
        </div>
  </div>		

<?php } 


 
wp_reset_query();
?> 

