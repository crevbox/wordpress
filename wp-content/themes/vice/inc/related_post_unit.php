<?php
global $options;
$thumb_id   =   get_post_thumbnail_id($post->ID);
$link       =   esc_url(get_permalink());
$title      =   esc_attr(get_the_title());

?>
<div class="col-sm-4 col-md-4 col-lg-12 qw-related-post-unit ">
    <div class="row"> 
        <?php
        $col2 = '12';
        if ( has_post_thumbnail( ) ){
            $col2 = '8';
            ?>
            <div class="col-md-12 col-lg-4">
             <a href="<?php echo esc_url($link); ?>" class="qw-animated qw-negative qw-imgl">
            <?php
            the_post_thumbnail( 'full',array('class'=>'img-responsive qw-fancyborder-bottom qw-animated') );
            ?></a></div><?php
        }

        ?>  
        <div class="col-md-12 col-lg-<?php echo esc_attr($col2); ?> qw-related-post-text">
            <a href="<?php echo esc_url($link); ?>" ><?php echo esc_attr($title); ?></a>

        </div>
    </div>
</div>
