<?php

/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package _s
 */

if ( ! function_exists( '_s_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable.
 */
function _s_content_nav( $nav_id ) {
	global $wp_query, $post;

	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
	}

	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;

	$nav_class = ( is_single() ) ? 'post-navigation' : 'paging-navigation';

	?>
	<div class="qw-padded qw-glassbg">
		<nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="<?php echo esc_attr($nav_class); ?> ">

		<?php if ( is_single() ) : // navigation links for single posts ?>

			<?php previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav">' . _x( '<span class="qticon-arrow-left"></span>', 'Previous post link', '_s' ) . '</span> %title' ); ?>
			<?php next_post_link( '<div class="nav-next">%link</div>', '%title <span class="meta-nav">' . _x( '<span class="qticon-arrow-right"></span>', 'Next post link', '_s' ) . '</span>' ); ?>

		<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="qticon-arrow-left"></span> Older posts', '_s' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="qticon-arrow-right"></span>', '_s' ) ); ?></div>
			<?php endif; ?>

		<?php endif; ?>
		<div class="canc"></div>
		</nav><!-- #<?php echo esc_html( $nav_id ); ?> -->

	</div>
	<?php
}
endif; // _s_content_nav

if ( ! function_exists( '_s_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function _s_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<div class="row">
				<div class="col-xs-2 col-sd-2 col-sm-2 col-md-2">
					<div class="qw-comment-image">
						<span class="qticon-earth qticons-big qw-user-icon"></span>
					</div>
				</div>
				<div class="col-xs-10 col-sd-10 col-sm-10 col-md-10">
					<p class="qw-commentheader qw-small qw-caps">
						<?php esc_attr__( 'Pingback:', '_s' ); ?> <?php edit_comment_link( '<i class="glyphicon  glyphicon-pencil"></i>', '<span class="edit-link">', '</span>' ); ?>
					</p>
					<div class="comment-text">
					 <?php comment_author_link(); ?> 
					</div>
				</div>
			</div>
		</div>

	<?php else : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
		<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
			<div class="row">
				<div class="col-xs-2 col-sd-2 col-sm-2 col-md-2">
					<div class="qw-comment-image">
						<?php 
							$avatar = get_avatar( $comment, $args['avatar_size'] );
							if ( 0 != $args['avatar_size'] && $avatar != '' ){
						 		echo get_avatar( $comment, $args['avatar_size'] );
						 	}else{
						 		?><span class="qticon-chat-bubble qticons-big qw-user-icon"></span><?php
						 	}
						?>
						
					</div>
					<?php
						comment_reply_link( array_merge( $args, array(
							'add_below' => 'div-comment',
							'depth'     => $depth,
							'max_depth' => $args['max_depth'],
							'before'    => '<div class="reply">',
							'after'     => '</div>',
						) ) );
					?>
				</div>
				<div class="col-xs-10 col-sd-10 col-sm-10 col-md-10">

					<div class="comment-content">
						<p class="qw-commentheader qw-small qw-caps">
							<?php printf( __( '%s <br> ', '_s' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
							<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
								<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', '_s' ), get_comment_date(), get_comment_time() ); ?>
							</a>
							<?php edit_comment_link( '<i class="glyphicon  glyphicon-pencil"></i>', '<span class="edit-link">', '</span>' ); ?>
						</p>

						<?php if ( '0' == $comment->comment_approved ) : ?>
						<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', '_s' ); ?></p>
						<?php endif; ?>
						<div class="comment-text">
							<?php comment_text(); ?>							
						</div>
						
					</div><!-- .comment-content -->
				</div>


			</div>			
		</article><!-- .comment-body -->

	<?php
	endif;
}
endif; // ends check for _s_comment()

if ( ! function_exists( '_s_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function _s_posted_on() {
    $time_string = '<time datetime="%1$s">%2$s</time>';
    $time_string = sprintf( $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date() )
    );

   echo  $time_string;
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 */
function _s_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so _s_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so _s_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in _s_categorized_blog.
 */
function _s_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', '_s_category_transient_flusher' );
add_action( 'save_post',     '_s_category_transient_flusher' );
