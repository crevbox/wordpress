// Admin Scripts for QantumThemes Functions
(function() {

    /**
     *
     *  Gallery shortcode
     *  @param {string} [button name] [description] value = qtgallery 
     *  IMPORTANT: In php:  array_push($buttons, "qtgallery");
     * 
     */
    tinymce.create("tinymce.plugins.gallery_button_plugin", {

        init : function(ed, url) {

            var my_options = qt_gallery_shortcodes;

            ed.addButton('qtgallery', {
                type: 'listbox',
                text: 'Choose a gallery',
                icon: false,
                tooltip: 'Insert QantumGallery',
                fixedWidth: true,
                onselect: function(e)
                {
                    var value = this.value();
                    var selection = ed.selection;
                    var rng = selection.getRng();
                    ed.focus();
                    var return_text = '[qtgallery id="'+value+'"]';
                    ed.execCommand("mceInsertContent", 0, return_text);  
                },
                values: my_options,
                onPostRender: function() 
                {
                    ed.my_control = this; // ui control element
                }
            });
        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : "Extra Buttons",
                author : "Narayan Prusty",
                version : "1"
            };
        }
    });

    tinymce.PluginManager.add("qt_shortcodes_plugin", tinymce.plugins.gallery_button_plugin);
})();
