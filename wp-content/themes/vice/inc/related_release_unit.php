<?php
global $options;
$thumb_id   =   get_post_thumbnail_id($post->ID);
$link       =   esc_url(get_permalink());
$title      =   esc_attr(get_the_title());

?>
<div class="col-sm-4 col-md-4 qw-related-post-unit qw-top15"> 
        <?php
        $col2 = '12';
        if ( has_post_thumbnail( ) ){
            
            ?>
            
            <a href="<?php echo esc_url($link); ?>" class="qw-animated qw-negative qw-imgl">
            <?php
            the_post_thumbnail( 'full',array('class'=>'img-responsive qw-fancyborder-bottom qw-animated') );
            ?></a><?php
        }

        ?>  

</div>
