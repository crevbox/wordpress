<?php
/*
*
*	This file is used to create the "load next" effect
*	with assets/lib/transition
*	
*
*/

function getCurrentUrl($full = true) {
    $s = &$_SERVER;
    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
    $sp = strtolower($s['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port = $s['SERVER_PORT'];
    $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.esc_attr($port);
    $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : $s['SERVER_NAME'];
    $uri = $protocol . '://' . $host . $port . $s['REQUEST_URI'];
    $segments = explode('?', $uri, 2);
    $url = $segments[0];
    return $url;
}


if(!function_exists('qtJsonPreview')){
	function qtJsonPreview(){
		$args = array(
		   'p' => $postid = url_to_postid( getCurrentUrl() )
		);
		$posts_query = new WP_Query($args);
		global $post;
		while ($posts_query->have_posts()) : $posts_query->the_post();
			$dataImage = 'false';
			if( has_post_thumbnail()){
				$big = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 1300,800) ;   
				$dataImage = '"'.esc_url($big[0]).'" '; 
			}

			$response =  array(
				"image"=>              esc_js($dataImage),
				"date"=>               esc_js(esc_attr(get_the_date())),
				"author"=>             esc_js(esc_attr(get_the_author())),
				"title"=>             esc_js(esc_attr(get_the_title())),
				"permalink"=>          esc_js(esc_url(esc_attr(get_permalink()))),
				"title_secondary"=>    "",
				"content"=> esc_js(apply_filters('the_content',get_the_content()))
			);

			$response = json_encode ($response);

		endwhile; 
		wp_reset_query();
		return $response;

	}

}
if(isset($_GET['jsonPreview'])){
	if($_GET['jsonPreview']==1){
		die(qtJsonPreview());
	}
}
?>