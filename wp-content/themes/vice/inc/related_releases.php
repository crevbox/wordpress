<?php
global $options;
global $post;
/////////$tags = wp_get_post_tags($post->ID);

$terms = get_the_terms( $post->ID , 'genre', 'string');
//Pluck out the IDs to get an array of IDS



$col_class  =   4;
$no_rows    =   3;
if(array_key_exists('content_class', $options)){
  if($options['content_class']=='col-md-12'){
      $col_class  =   3;
      $no_rows    =   4;
  }
}


if ($terms) {
        $term_ids = wp_list_pluck($terms,'term_id');
        wp_reset_query();

         $my_query = new WP_Query( array(
          'post_type' => 'release',
          'tax_query' => array(
                        array(
                            'taxonomy' => 'genre',
                            'field' => 'id',
                            'terms' => $term_ids,
                            'operator'=> 'IN' //Or 'AND' or 'NOT IN'
                         )),
          'posts_per_page' => 3,
        //  'ignore_sticky_posts' => true,
          'orderby' => 'rand',
          'meta_query'    => array(
                                    array(
                                        'key' => '_thumbnail_id',
                                        'compare' => 'EXISTS'
                                    ),
                                ),
          'post__not_in'=>array($post->ID)
       ) );
        
        
      //  $my_query = new WP_Query($args);
        
        
        if ( $my_query->have_posts() ) { ?>	

          <div class="qw-related-posts qw-top15 hidden-xs"> 
                <h4 class="qw-page-subtitle"><?php echo esc_attr__('Related Releases', '_s'); ?></h4>   
                <hr class="qw-separator qw-separator-thin">
                <div class="row">
                        <?php
                        $n = 0;
                        while ($my_query->have_posts() && $n < 3) {
                           $my_query->the_post();
                           if(has_post_thumbnail() ){
                            get_template_part('inc/related_release_unit');   
                           }
                           $n++;
                        } //end while
                        ?>
                </div>
          </div>		

        <?php } //endif have post
}// end if tags

 
wp_reset_query();
?> 

