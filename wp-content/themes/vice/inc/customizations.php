<?php
/*
*
*	Get custom values of the theme
*
*/

global $optarray;
global $qw_custom_styles;
$qw_custom_styles = '';

/*
*
*	Debug to check all the theme mods
*
*/
//add_action( 'wp_head', 'debugMe' );
function debugMe(){

}


$optarray = thsp_cbp_get_options_values();

/*
*
*	Favicon
*
*/
add_action( 'wp_head', 'qt_favicon_print' );
function qt_favicon_print() {
	$i = get_theme_mod('QT_Favicon');
	if($i != ''){
		echo '<link rel="icon" href="'.esc_url($i).'" />' ;
	}
}


/*
*
*	Fonts 
*
*/
add_action('wp_head','QTenableGoogleFonts',1000);
function QTenableGoogleFonts(){
	if(get_theme_mod('QT_font_enable',"1") == '1'){
		global $qw_custom_styles;
		
		$QT_content_font = get_theme_mod('QT_content_font',"Droid Sans");
		$QT_content_font_name = $QT_content_font;
		$QT_content_font = str_replace("|", "%7C", $QT_content_font);
		$QT_content_font = str_replace(" ", "+", $QT_content_font);
		echo '<link href="https://fonts.googleapis.com/css?family='.esc_attr($QT_content_font).'" rel="stylesheet" type="text/css">';
		$qw_custom_styles .= 'body, p {font-family: "'.esc_attr($QT_content_font_name).'", sans-serif; }';

		
		$QT_captions_font = get_theme_mod('QT_captions_font',"Marvel");
		$QT_captions_font_name = $QT_captions_font;
		$QT_captions_font = str_replace("|", "%7C", $QT_captions_font);
		$QT_captions_font = str_replace(" ", "+", $QT_captions_font);
		echo '<link href="https://fonts.googleapis.com/css?family='.esc_attr($QT_captions_font).'" rel="stylesheet" type="text/css">';
		$qw_custom_styles .= 'h1, h2, h3, h4, h5, qw-page-title, .tooltip, #nav , h1.qw-page-title, h1.qw-moduletitle{font-family: "'.esc_attr($QT_captions_font_name).'", sans-serif;font-weight:'.esc_attr(get_theme_mod('QT_captions_font_weight',"600")).';}';

	}
}


/*
*
*	Colors 
*
*/
add_action('wp_head','QTcustomColor',10);
function QTcustomColor(){
	global $optarray;
	$color = esc_attr(get_theme_mod('QTcol'));

	if($color == ''){
		$color = "#94d500";
	}


	if($color != ''){
		echo 
		'<style type="text/css">
		#qwAjaxPreloader .circle, #qw-nav span,   .btn-primary:hover, .btn:hover, a.btn:hover,.btn-primary,  ul.qw-vertical-tab-menu li a, .qw-panel-contents,.qw-fancyborder-top,.qw-fancyborder-bottom ,.qw-fancyborder-right,.qw-fancyborder-left,.gallery-icon a:hover img,.comment-form input[type="text"]:focus, .comment-form textarea:focus {border-color: '.esc_attr($color).' !important;}
		.tooltip.left .tooltip-arrow, .qw-sharelabel::after {border-left-color: '.esc_attr($color).' !important;}
		.qw-marker::after, #qwAjaxPreloader .circle1, .tooltip.top .tooltip-arrow, h1.qw-moduletitle:before,h1.qw-moduletitle:after, .qw-dateblock::after{border-top-color:'.esc_attr($color).';}
		.qw-preloader .circle,.qw-preloader .circle1, #qwAjaxPreloader .circle1, .tooltip.bottom .tooltip-arrow, .qw-slides-menu.fp-slidesNav a.active img, .qw-slides-menu.fp-slidesNav a.active:hover img, .qw-slides-menu a.active img {border-bottom-color:'.esc_attr($color).';}
		.qw-preloader .circle, .qw-preloader .circle1,.qw-separator::after { border-top-color:'.esc_attr($color).';}
		.qw-colcaption-title:after, .tooltip.right .tooltip-arrow, .qw-cover-fx::after, .qw-gridview-imagelink .qw-gridview-overlay::after { border-right-color:'.esc_attr($color).';}
		.qw-dateblock,.qw-sharepage span.qw-sharelabel, p.qw-categories a, .qw-separator::before, .comment-reply-link, .qw-thecontent a.btn:hover, ul.qw-vertical-tab-menu li a:hover,.page-links a:hover, .qw-pagination li a:hover,  .qw-img-verticalcarousel:before,  .carousel-indicators .active, .qw-negative, .btn-primary, a.btn:hover, .btn:hover, .qw-menutype1 #qwtoggle li a:hover, .qw-slides-menu a,  .tooltip-inner, a.qw-marker-header.qw-negative:hover, .fp-slidesNav .active span, .qw_palette_dark .qw-markercontents, .qw-markercontents,  .qw-menutype1 #qwtoggle > li > a.active,.qw_palette_dark .qw-related-post-unit a.qw-negative, .qw-footer-container aside header.widget-header, .qw-marker, .qw-marker.active .qw-markercontents .qw-marker-header,  .qw-itemscarousel-text.qw-cover-fx , #theVolCursor, ol.carousel-indicators li.active, .qw_palette_light .qw-related-post-unit a.qw-negative, .qw-sharepage span.qw-sharelabel {background-color: '.esc_attr($color).';}
		input[type="button"], input[type="submit"], input[type="reset"], input[type="file"]::-webkit-file-upload-button, button ,.comment-form input[type="submit"] {background: '.esc_attr($color).';	background-color: '.esc_attr($color).';}
		::-webkit-scrollbar-thumb, input.wpcf7-submit, .qw_palette_dark  input.wpcf7-submit  {background-color: '.esc_attr($color).';}
		input.wpcf7-submit , .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {background-color: '.esc_attr($color).' !important;}
		.qw-artists-names a, .qw_palette_dark h3.widget-title a:hover, h3.widget-title a:hover,  #qw-nav  li a:hover, #qw-nav  li a.active, .qw_palette_dark .qw-thecontent a:hover, .qw-footer-bar a:hover, 
		a.qw-itemscarousel-control:hover , .qw-footer-container a:hover, a.qw-negative:hover,.fp-controlArrow.fp-prev:hover, .fp-controlArrow.fp-next:hover, .qw-itemscarousel-control:hover, .qw-verydark a:hover, a:hover,p.qw-tags a,.qw-commentheader a:hover, .qw_palette_dark a:hover , .contentsection a {color: '.esc_attr($color).';}
		.qw-simpleslder .fp-slidesNav li a.active, .qw_palette_dark a.qw-slidelink.active, .fullpageMenu li.active a, h1.qw-moduletitle span,.qw-buylinks a, .qw-thecontent a, .qw-coloredtext,a.rsswidget,a#cancel-comment-reply-link,p.qw-tags a,#wp-calendar a, a.rsswidget, .qw_palette_dark .qw-footer-container a.rsswidget {color: '.esc_attr($color).';}
		 .qw_palette_dark .qw-thecontent a.btn:hover { color: #fff;}
		</style>';
	}
}
	
/*
*
*	Fullscreen background
*
*/
add_action('wp_head','QTfsbg',10);
function QTfsbg(){
	$QT_fsbg = get_theme_mod("QT_fsbg","1");
	if($QT_fsbg == '1'){
		global $qw_custom_styles;
		$qw_custom_styles .= ' body#theBody.custom-background {-moz-background-size:cover;-webkit-background-size:cover;background-size:cover;background-position:center !important;background-attachment:fixed;}
			body#theBody.is_safari.is_mobile { background-attachment: local !important; background-size: 100% auto !important; -moz-background-size:100% auto !important;-webkit-background-size:100% auto !important;background-position: 0 0 !important; } ';
	}
}

/*
*
*	Logo
*
*/
function qt_showlogo(){
	$logo = esc_attr(get_theme_mod("QT_logo",""));
	if(function_exists('icl_get_home_url')){
		$link = icl_get_home_url();
		
	} else {
		$link = get_home_url();
	}
	if($logo != ''){
		echo '<a href="'.esc_url(esc_attr($link)).'" class="qw_site_logo" id="qwLogo"><img src="'.esc_attr($logo).'" class="img-responsive" alt="'.esc_attr__("Home","_s").'"></a>';
	}else{
		echo '<a href="'.esc_url(esc_attr($link)).'" class="qw_site_logo" id="qwLogo"><h3 class="qw-sitetitle">'.esc_attr(get_bloginfo( 'name' )).'</h3></a>';
	}
}

/*
*
*	Logo
*
*/
function qt_showlogo_big(){
	$logo = get_theme_mod("QT_logo_big","");
	if(function_exists('icl_get_home_url')){
		$link = icl_get_home_url();
	} else {
		$link = get_home_url();
	}
	if($logo != ''){
		
		return '<a href="'.esc_url(esc_attr($link)).'" class="qw-sidebarlogo" id="qwLogoBig" ><img src="'.esc_attr(esc_url($logo)).'" class="img-responsive" alt="'.esc_attr__("Home","_s").'"></a>';
	}
	return;
}

/*
*
*	Footer 
*
*/

function qt_footertext(){
	global $optarray;
	if(isset($optarray['QT_footer_text'])){
		echo esc_attr($optarray['QT_footer_text']);
	}else{
		return "¨¨W¨P¨L¨O¨C¨K¨E¨R¨.¨C¨O¨M¨¨";
	}
}




/*
*
*	Menu version 
*
*/

function qt_menutype(){
	return esc_attr(get_theme_mod('QT_menutype','menutype1'));
}



/*
*
*	Add custom body classes
*
*/

add_filter( 'body_class', 'qt_body_class' );
function qt_body_class( $classes ) {
	global $optarray;
	if(isset($optarray['QT_colour_palette'])){
		$classes[] = esc_attr(get_theme_mod('QT_colour_palette','qw_palette_dark'));
	}
	return $classes;
}



/*
*
*	PRINT ALL THE CUSTOM STYLES 
*
*/


add_action('wp_head','qt_output_custom_styles',9000);

function qt_output_custom_styles(){
	global $optarray;
	global $qw_custom_styles;
	echo '

<!-- THEME CUSTOMIZATION ==================================== -->

';
	echo '<style type="text/css">';
	echo wp_kses_post($qw_custom_styles);
	echo '</style>';
	echo '

<!-- THEME CUSTOMIZATION END ================================ -->

';
}



?>