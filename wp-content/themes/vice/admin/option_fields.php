<?php
$options = array (

/////////////////////////////////////////////////////////////////////////////////////////

// Activation of the customizer

/////////////////////////////////////////////////////////////////////////////////

array( "type" => "open"),//__________________ apertura

array( "name" => '<i class="icon-key"></i> Activation Key',
	   //"image" => "icon_settings.png",
	  "type" => "section"),



array( 
      "name" => "Envato Username",
	"id" => "qt_envato_user",
	"type" => "text",
	"desc" => 'The Envato username used to purchase the product. Is Case Sensitive!'
	),

array( "name" => "Purchase Code",
	"id" => "qt_purchase_code",
	"type" => "text",
	"desc" => 'The purchase code of your product. <br>
	<a href="http://themeforest.net/downloads" target="_blank">Get Your Purchase Code Here</a>'
	),

array( "name" => "Activation Key",
	"id" => "qt_activation_key",
	"type" => "text",
	"desc" => 'The Activation Key is needed only for extra services.<br> Is not needed for the theme to work.<br> A valid activation key is needed to use the Beatport Importer plugin.<br> 
	<a href="http://www.qantumthemes.com/activation/" target="_blank">Get Your Activation Key Here</a>'
	),

array( "type" => "close"),//__________________ chiusura //////////////////////////




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				 // Mixcloud Podcast Importer
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


array( "type" => "open"),//__________________ apertura
array( "name" => '<i class="icon-cloud"></i> Import Mixcloud',
		"type" => "section"),//__________________ apertura	

array( "name" => "Import Podcast",
		"desc" => "Performing the dinamic import",
		"qwaction" => 'importMixcloudNow',
		"type" => "dynamicaction"),

array( "name" => "Mixcloud Channel to import",
		"desc" => "Copy here the mixcloud user flax, eg. for http://.mixcloud.com/CarlCox the flag is \"CarlCox\" ",
		"id" => "importMixcloudNow",
		"dyn_action" => 'importMixcloudNow',
		"type" => "action_field"),

array( "type" => "close") //__________________ chiusura ///////////////////////////



);

?>