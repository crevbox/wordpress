<?php
$qw_glob_sidebar_class = qw_glob_sidebar_class();
if($qw_glob_sidebar_class['sidebar'] == '1'){
?>
<div class="col-md-3 col-lg-3">
<div class="panel-group qw-main-sidebar" id="mainsidebar">
	<div class="qw-colcaption-title qw-negative text-center qw-caps">
		<?php 
		/*
		*
		* Print the title of the widget sidebar, but allow translarions if the title is the same as default
		*/
		$mod = get_theme_mod('QT_sidebar_caption',"Expand");
		if($mod != "Expand"){	
			echo esc_attr($mod);
		}else{
			echo esc_attr__("Expand","_s");
		}
		?>
	</div>	
	<?php dynamic_sidebar( 'right-sidebar' ); ?>
</div>
</div>
<?php
}
?>