<?php
		$generic_sidebar_att = array('before_widget' => '<aside class="widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<section class="wheader "><header class="widget-header"><h3 class="widget-title ">',
		'after_title'   => '</h3></header></section>');
		?>

		<div class="container">
				<div class="row">
		            <div class="col-md-3">
		                <?php 
							if(is_active_sidebar('sidebar-1')){
								dynamic_sidebar( 'sidebar-1' ); 
							}else{
								//the_widget( 'WP_Widget_Recent_Posts','title=News&number=5',$generic_sidebar_att);
							}
						?>
		            </div>
		            <div class="col-md-3">
		                <?php 
							if(is_active_sidebar('sidebar-2')){
								dynamic_sidebar( 'sidebar-2' ); 
							}else{
								//the_widget( 'WP_Widget_Recent_Comments','title=Comments&number=4',$generic_sidebar_att);
							}
						?>
		            </div>
		            <div class="col-md-3">
		                 <?php 
							if(is_active_sidebar('sidebar-3')){
								dynamic_sidebar( 'sidebar-3' ); 
							}else{
								//the_widget( 'WP_Widget_Meta','title=Links', $generic_sidebar_att);
							}
						?>
		            </div>
		            <div class="col-md-3">
		                 <?php 
							if(is_active_sidebar('sidebar-4')){
								dynamic_sidebar( 'sidebar-4' ); 
							}else{
								//the_widget( 'WP_Widget_Tag_Cloud','title=Tags',$generic_sidebar_att);
							}
						?>
		            </div>
		        </div>
			</div>

