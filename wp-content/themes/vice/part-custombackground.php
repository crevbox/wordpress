<?php  
/*
*
*
*	part-custombackground.php
*	get the page custom background information and create a special CSS
*	Why in the footer? to let it work via ajax!
*
*/
wp_reset_postdata();
wp_reset_query();
if(is_singular() || is_page()){
	wp_reset_postdata();
	wp_reset_query();
	global $post;
	if(is_object($post)){
		$meta = get_post_meta($post->ID,"qw_post_custom_bg",true);
		if(false != $meta){
			$url = wp_get_attachment_url(esc_attr($meta)); ?>
			
			<!-- CUSTOM IMAGE BACKGROUND <?php echo esc_Attr($url); ?>  THE CODE IS HERE TO WORK WITH AJAX PAGE LOADING! -->
			<style type="text/css">
				body {background-image:url("<?php echo esc_Attr($url); ?>") !important;background-attachment:fixed;background-position:center center;background-size:cover;}
				body#theBody.is_safari.is_mobile { background-attachment: local !important; background-size: 100% auto !important; -moz-background-size:100% auto !important;-webkit-background-size:100% auto !important; background-position: 0 0 !important; }
			</style>
			<?php
		} else { ?>
			<!--  no valid custom background specified -->
		<?php
		}

	}

}