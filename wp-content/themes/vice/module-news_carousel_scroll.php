<?php
/*
*
*   Module
*
*/
global $post; // the main object of the modular page containing this module
global $GlobalMonthsArray; 
global $tempEvent; // variables defined in the mother page
$moduleId = esc_attr($tempEvent['moduleId']);
$qw_elements_quantity = esc_attr(get_post_meta( $post->currentmodule, $key = 'qw_elements_quantity', $single = true ));
$qw_lightness = esc_url($tempEvent['qwlightness']);
?>
<div class="qw-vp qw-gbcolorlayer qw-news-carousel-scroll">
	<div class="qw-vc">
		<div class="container">
			<?php 
			if ($tempEvent['hide_title'] == '' || $tempEvent['hide_title'] == '0'){	?>
			<h1 class="qw-moduletitle"><?php  echo esc_attr(get_the_title( $tempEvent['moduleId'])); ?></h1>	
			<?php } ?>
			<div class="row qw-news-carousel-scroll">		
				<?php
				/*
				*
				*
				*
				*	Start of the carousel
				*
				*
				*/
				?>
				<div class="col-sm-12 col-md-10 col-md-offset-1  col-lg-10 col-lg-offset-1">
					<?php 

					$args  = array(
					    'post_type' => 'post',
					    'post_status' => 'publish',
					    'ignore_sticky_posts' => 1,
					    'posts_per_page'=>$qw_elements_quantity
					    ,'suppress_filters' => false
					);
					//$events = get_posts($args); 




					$wpbp = new WP_Query($args ); 
					$wp_query = $wpbp; ?>
					




					
					    <div id="carouselevents<?php echo esc_attr($moduleId);?>" class="carousel slide qw-itemscarousel">
						    
						    <!-- Wrapper for slides -->
						    <div class="carousel-inner">
						        <?php
						            $active = 'active';
						            $col = 0;

						            /*
						            if($qw_elements_quantity > count($events)){$qw_elements_quantity = count($events); }


						            for($i = 0; $i < $qw_elements_quantity; $i++){
									*/
						            $i = 0;
						            if ($wpbp->have_posts()) : while ($wpbp->have_posts()) : $wpbp->the_post(); 



						                $page = $post;
						                setup_postdata( $page );
						                $image  = '';
						                if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), array(500,500)  ) )  {
						                    $image       = $thumb['0'];// '<img src="'.esc_url($thumb['0']).'" class="img-responsive hidden-xs" alt="'.esc_attr($page->post_title).'">';
						                } 
						                $e = array(
						                'id' =>  $page->ID,
						                'date' =>  esc_attr(get_post_meta($page->ID,'general_release_details_release_date',TRUE)),
						                'permalink' =>  esc_url(get_permalink($page->ID)),
						                'title' =>  esc_attr($page->post_title),
						                'thumb' => $thumb
						                );
						                if($e['date'] != '') {
							                $d = explode('-',$e['date']);
							                $time = mktime(0, 0, 0, $d[1]);
							                $pDate = $e['date'];
							                 $date_array = explode('T',$pDate); 
							                $pDate = explode('-',$date_array[0]);
							                if(array_key_exists($pDate[1],$GlobalMonthsArray)){
							                    $e['releasedate'] = esc_attr($pDate[2]).' '.esc_attr($GlobalMonthsArray[$pDate[1]]).' '.esc_attr($pDate[0]);
							                }
						                } else {
						                	$e['releasedate'] = "";
						                }
						                /*
						                *
						                *   Column
						                *
						                */
						                if ($i == 0 || ($i % 3 == 0)){ 
						                     echo '<div class="item '.esc_attr($active).'"><div class="row">';
						                    $open = 1;
						                    
						                }
						                ?>
						                <div class="col-xs-12 col-sm-4  col-md-4">
						                   

						                    <hr class="qw-separator qw-separator-thin qw-top0">
											<p class="qw-small qw-caps hidden-sm hidden-md hidden-xs"><?php the_time(esc_attr(get_option('date_format'))) ?></p>
											<?php if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium"  ) )  {?>
													<a data-qwjquerycontent="<?php echo esc_js(get_permalink($post->ID)); ?>"  href="<?php echo esc_url(get_permalink($post->ID)); ?>" class="hidden-sm hidden-md hidden-xs qwjquerycontent qw-newscarousel-featimage" data-bgimage="<?php echo esc_js(esc_url($thumb['0'])); ?>">
														<img src="<?php echo esc_url($thumb['0']) ; ?>" class="img-responsive hidden" alt="<?php esc_attr(the_title()); ?>">
													</a>
													
											<?php } ?>
											<?php $permalink = esc_url(get_permalink($post->ID)); ?>
											<div class="qw-glassbg qw-padded qw-contents">
												<h3>
													<a class="qwjquerycontent" data-qwjquerycontent="<?php echo esc_js(esc_url($permalink)); ?>" href="<?php echo esc_url($permalink); ?>">
														<?php the_title(); ?>
													</a>
												</h3>
												<div class="qt-excerpt-3rows hidden-sm hidden-md hidden-xs">
													<?php the_excerpt();?>
												</div>
												
												<a class="qwjquerycontent btn btn-primary btn-xs" data-qwjquerycontent="<?php echo esc_js(esc_url($permalink)); ?>" href="<?php echo esc_url($permalink); ?>">
													<?php
													echo esc_attr__("Read More","_s");
													?>
												</a>

											</div>




						                </div>
						                <?php 
						                if  (($i+1) % 3 == 0){ 
						                    echo '</div></div>';
						                    $open = 0; 
						                    $col++;
						                } 
						                $active = ''; 

						                $i ++;

						            endwhile;endif;
						    
						    if(isset($open)){
						      if($open == 1) { 
						        ?> 
						        </div> </div>
						        <?php 
						        $col++; 
						      }
						    } 

						    ?>
					    </div>				   
					</div>
					
					</div>				
				<?php
				wp_reset_postdata();
				wp_reset_query();

				/*
				*
				*
				*
				*	End of the carousel
				*
				*
				*
				*/
				?>
			</div>
		</div>
		<!-- Indicators -->
		<?php
		 if($i > 3){
		?>
	    <ol class="carousel-indicators hidden-sm hidden-md hidden-xs" id="carouselindicators<?php echo esc_attr($moduleId);?>">
	    <?php
	      $active = 'active';
	      for($c = 0; $c < ($i/3); $c++){
	       
	        ?>
	        <li data-target="#carouselevents<?php echo esc_js(esc_attr($moduleId));?>" data-slide-to="<?php echo esc_js(esc_attr($c)); ?>" class="<?php echo esc_attr($active); ?>"></li>
	        <?php
	        $active = '';
	      }
	      ?>
	    </ol>
	    <?php
		}
	    ?>


	    <?php
		if($qw_elements_quantity > 3){
			?>
			<a href="#carouselevents<?php echo esc_js(esc_attr($moduleId));?>" class="qw-itemscarousel-control left qw-animated " role="button" data-slide="prev">
				<span class="qticon-chevron-light-left"></span>
			</a>
			<a href="#carouselevents<?php echo esc_js(esc_attr($moduleId));?>" class="qw-itemscarousel-control right qw-animated"  role="button" data-slide="next">
				<span class="qticon-chevron-light-right"></span>
			</a>     
			<?php
		}
		?>

		<?php  
		$archive_link_url = get_post_meta( $moduleId, 'archive_link_url', true );
		$archive_link_text = get_post_meta( $moduleId, 'archive_link_text', true );
		if($archive_link_url){ ?>
			<p class="text-center qw-top30 qw-top30  hidden-sm hidden-md hidden-xs">
			<a href="<?php echo esc_attr($archive_link_url); ?>" class="btn btn-primary"><?php echo esc_attr($archive_link_text); ?></a>
			</p>
		<?php } ?>	


    </div>
</div>





	




