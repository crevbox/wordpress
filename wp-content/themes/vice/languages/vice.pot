# WordPress Blank Pot
# Copyright (C) 2014 ...
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Vice Translation File\n"
"Report-Msgid-Bugs-To: Translator Name <translations@example.com>\n"
"POT-Creation-Date: 2016-06-15 08:29+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: Your Name <you@example.com>\n"
"Language-Team: QantumThemes <info@qantumthemes.com>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Textdomain-Support: yesX-Generator: Poedit 1.6.4\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html_x:1,2c;esc_html__;"
"esc_attr_e;esc_attr_x:1,2c;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;"
"_x:1,2c;_n:1,2;_n_noop:1,2;__ngettext:1,2;__ngettext_noop:1,2;_c,_nc:4c,1,2\n"
"X-Poedit-Basepath: ../\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:17
msgid "404: Sorry, this page doesn't exist"
msgstr ""

#: 404.php:56 page.php:63 sidebar.php:17 single-mediagallery.php:77
#: single-radiochannel.php:58 woocommerce.php:45
#: customizer-boilerplate/options.php:763
msgid "Expand"
msgstr ""

#: archive.php:19 blog.php:34
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: archive.php:23 blog.php:38
#, php-format
msgid "Author: %s"
msgstr ""

#: archive.php:25 blog.php:40
#, php-format
msgid "Day: %s"
msgstr ""

#: archive.php:26 blog.php:41
#, php-format
msgid "Month: %s"
msgstr ""

#: archive.php:27 blog.php:42
#, php-format
msgid "Year: %s"
msgstr ""

#: archive.php:28 blog.php:43
msgid "Asides"
msgstr ""

#: archive.php:29 blog.php:44
msgid "Images"
msgstr ""

#: archive.php:30 blog.php:45 single-artist.php:183 single-artist.php:252
msgid "Videos"
msgstr ""

#: archive.php:31 blog.php:46
msgid "Quotes"
msgstr ""

#: archive.php:32 blog.php:47
msgid "Links"
msgstr ""

#: archive.php:33 blog.php:48 custom-types/qt-gallery/gallery.php:19
#: custom-types/qt-gallery/gallery.php:21
#: custom-types/qt-gallery/gallery.php:23
#: custom-types/qt-gallery/gallery.php:25
msgid "Galleries"
msgstr ""

#: archive.php:34 blog.php:49
msgid "Sounds"
msgstr ""

#: archive.php:42 blog.php:57
msgid "Podcasts"
msgstr ""

#: archive.php:49 blog.php:64
msgid "Releases"
msgstr ""

#: archive.php:56 blog.php:71
msgid "Artists"
msgstr ""

#: archive.php:63 blog.php:78
msgid "Events"
msgstr ""

#: archive.php:65
msgid "Archives"
msgstr ""

#: archive.php:88 blog.php:87
msgid "Search again:"
msgstr ""

#: blog.php:80
msgid "Blog"
msgstr ""

#: comments.php:27 custom-types/qt-events/events.php:242
msgid "Comments"
msgstr ""

#: comments.php:33 comments.php:57
msgid "Comment navigation"
msgstr ""

#: comments.php:34 comments.php:58
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:35 comments.php:59
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:70
msgid "Comments are closed."
msgstr ""

#: comments.php:79
msgid "Required fields are marked *"
msgstr ""

#: comments.php:84
msgid "Leave a Reply"
msgstr ""

#: comments.php:85
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: comments.php:86
msgid "Cancel Reply"
msgstr ""

#: comments.php:87
msgid "Post Comment"
msgstr ""

#: comments.php:88
msgid "Comment"
msgstr ""

#: comments.php:92
#, php-format
msgid "You must be <a href=\"%s\">logged in</a> to post a comment."
msgstr ""

#: comments.php:97
#, php-format
msgid ""
"Logged in as <a href=\"%1$s\">%2$s</a>. <a href=\"%3$s\" title=\"Log out of "
"this account\">Log out?</a>"
msgstr ""

#: comments.php:104
msgid "Your email address will not be published. "
msgstr ""

#: comments.php:115
msgid "Name"
msgstr ""

#: comments.php:116 single-artist.php:206
msgid "Email"
msgstr ""

#: comments.php:117 single-event.php:124
msgid "Website"
msgstr ""

#: functions.php:64
msgid "Primary Menu"
msgstr ""

#: functions.php:99
msgid "January"
msgstr ""

#: functions.php:100
msgid "February"
msgstr ""

#: functions.php:101
msgid "March"
msgstr ""

#: functions.php:102
msgid "April"
msgstr ""

#: functions.php:103
msgid "May"
msgstr ""

#: functions.php:104
msgid "June"
msgstr ""

#: functions.php:105
msgid "July"
msgstr ""

#: functions.php:106
msgid "August"
msgstr ""

#: functions.php:107
msgid "September"
msgstr ""

#: functions.php:108
msgid "October"
msgstr ""

#: functions.php:109
msgid "November"
msgstr ""

#: functions.php:110
msgid "December"
msgstr ""

#: functions.php:174
msgid "Footer Sidebar 1"
msgstr ""

#: functions.php:182
msgid "Footer Sidebar 2"
msgstr ""

#: functions.php:190
msgid "Footer Sidebar 3"
msgstr ""

#: functions.php:198
msgid "Footer Sidebar 4"
msgstr ""

#: functions.php:206
msgid "Right Sidebar"
msgstr ""

#: functions.php:222
msgid "Search"
msgstr ""

#: loop-archive.php:30 single.php:18
msgid "Posted By:"
msgstr ""

#: loop-archive.php:30 loop-archiveevent.php:54 module-news_carousel.php:61
msgid "Read More"
msgstr ""

#: module-artists_carousel.php:74
msgid "Read more"
msgstr ""

#: module-events_carousel.php:140
msgid "Info & Ticket"
msgstr ""

#: module-gallery.php:47 custom-types/qt-gallery/gallery.php:172
msgid "click to zoom"
msgstr ""

#: module-releases_carousel.php:118 module-releases_carousel_old.php:96
msgid "Add to playlist"
msgstr ""

#: module-releases_carousel.php:119 module-releases_carousel_old.php:97
msgid "Tracklist"
msgstr ""

#: module-releases_carousel.php:124 module-releases_carousel_old.php:102
msgid "Go to page"
msgstr ""

#: page-fullwidth.php:20 page-fullwidth.php:24 single-qantummodule.php:20
#: single-qantummodule.php:24 single.php:60 single.php:67
msgid " / "
msgstr ""

#: page-fullwidth.php:23 single-qantummodule.php:23
msgid "Tagged as: "
msgstr ""

#: page-fullwidth.php:34 page.php:39 single-qantummodule.php:34
#: single-radiochannel.php:34 single.php:48 woocommerce.php:27
msgid "Pages:"
msgstr ""

#: page-modular.php:222
msgid "More"
msgstr ""

#: part-events_carousel_slider.php:8
msgid "Upcoming Events"
msgstr ""

#: part-events_carousel_slider.php:84
msgid "More Info"
msgstr ""

#: part-playlist.php:15
msgid "Play"
msgstr ""

#: part-playlist.php:16
msgid "Cover"
msgstr ""

#: part-playlist.php:17
msgid "Release"
msgstr ""

#: part-playlist.php:17 single-release.php:113
msgid "Label"
msgstr ""

#: part-playlist.php:18
msgid "Track Title"
msgstr ""

#: part-playlist.php:18
msgid "Track Authors"
msgstr ""

#: part-playlist.php:19
msgid "Page"
msgstr ""

#: part-playlist.php:20
msgid "Buy"
msgstr ""

#: part-playlist.php:21
msgid "Delete"
msgstr ""

#: part-playlist.php:45
msgid "Audio and Video volume"
msgstr ""

#: part-playlist.php:46 part-playlist.php:47
msgid "Mute / unmute"
msgstr ""

#: part-playlist.php:64
msgid "Hide Player"
msgstr ""

#: part-playlist.php:66
msgid "Updating playlist"
msgstr ""

#: part-tracklist.php:105
msgid "Add all tracks to playlist"
msgstr ""

#: searchform.php:4
msgid "Type and press enter &hellip;"
msgstr ""

#: sharepage.php:54
msgid "SHARE"
msgstr ""

#: single-artist.php:96 single-artist.php:249
msgid "Biography"
msgstr ""

#: single-artist.php:131 single-artist.php:250
#: custom-types/artist/artist-type.php:133
msgid "Music"
msgstr ""

#: single-artist.php:200 single-artist.php:251
#: custom-types/artist/artist-type.php:135
msgid "Booking"
msgstr ""

#: single-artist.php:202
msgid "Agency"
msgstr ""

#: single-artist.php:204 single-event.php:123
msgid "Phone"
msgstr ""

#: single-event.php:40
msgid "At"
msgstr ""

#: single-event.php:117
msgid "Event Details"
msgstr ""

#: single-event.php:120
msgid "Date"
msgstr ""

#: single-event.php:121
msgid "Location"
msgstr ""

#: single-event.php:122
msgid "Address"
msgstr ""

#: single-event.php:125
msgid "Event"
msgstr ""

#: single-event.php:147
msgid "Buy on:"
msgstr ""

#: single-mediagallery.php:49
msgid "Zoom"
msgstr ""

#: single-podcast.php:56
msgid "play"
msgstr ""

#: single-release.php:19
msgid "Out on:"
msgstr ""

#: single-release.php:32
msgid "Tracklist:"
msgstr ""

#: single-release.php:38
msgid "Review:"
msgstr ""

#: single-release.php:74
msgid "Release:"
msgstr ""

#: single-release.php:78
msgid "Buy On:"
msgstr ""

#: single-release.php:86
msgid "Online shop"
msgstr ""

#: single-release.php:119
msgid "Release Date"
msgstr ""

#: single.php:18
msgid "On:"
msgstr ""

#: single.php:63
msgid "Tags"
msgstr ""

#: custom-types/artist/artist-type.php:46
msgid "Artist genres"
msgstr ""

#: custom-types/artist/artist-type.php:47
#: custom-types/artist/artist-type.php:60 custom-types/radio/radio-type.php:69
msgid "Genres"
msgstr ""

#: custom-types/artist/artist-type.php:48 custom-types/qt-events/events.php:67
#: custom-types/radio/radio-type.php:57
#: custom-types/release/release-type.php:46
msgid "Search by genre"
msgstr ""

#: custom-types/artist/artist-type.php:49 custom-types/qt-events/events.php:68
#: custom-types/radio/radio-type.php:58
#: custom-types/release/release-type.php:47
msgid "Popular genres"
msgstr ""

#: custom-types/artist/artist-type.php:50
msgid "All Artists"
msgstr ""

#: custom-types/artist/artist-type.php:53
msgid "Edit Genre"
msgstr ""

#: custom-types/artist/artist-type.php:54
msgid "Update Genre"
msgstr ""

#: custom-types/artist/artist-type.php:55
msgid "Add New Genre"
msgstr ""

#: custom-types/artist/artist-type.php:56
msgid "New Genre Name"
msgstr ""

#: custom-types/artist/artist-type.php:57
msgid "Separate Genres with commas"
msgstr ""

#: custom-types/artist/artist-type.php:58
msgid "Add or remove Genres"
msgstr ""

#: custom-types/artist/artist-type.php:59
msgid "Choose from the most used Genres"
msgstr ""

#: custom-types/artist/artist-type.php:99
#: custom-types/podcast/podcast-type.php:99
#: custom-types/sliders/sliders-type.php:86
msgid " data"
msgstr ""

#: custom-types/artist/artist-type.php:132
msgid "Bio"
msgstr ""

#: custom-types/artist/artist-type.php:134
msgid "Video"
msgstr ""

#: custom-types/artist/artist-type.php:136
msgid "Custom content"
msgstr ""

#: custom-types/podcast/podcast-type.php:51
msgid "Podcast filters"
msgstr ""

#: custom-types/podcast/podcast-type.php:52
msgid "Filter"
msgstr ""

#: custom-types/podcast/podcast-type.php:53
msgid "Search by filter"
msgstr ""

#: custom-types/podcast/podcast-type.php:54
msgid "Popular filters"
msgstr ""

#: custom-types/podcast/podcast-type.php:55
msgid "All Podcasts"
msgstr ""

#: custom-types/podcast/podcast-type.php:58
msgid "Edit Filter"
msgstr ""

#: custom-types/podcast/podcast-type.php:59
msgid "Update Filter"
msgstr ""

#: custom-types/podcast/podcast-type.php:60
msgid "Add New Filter"
msgstr ""

#: custom-types/podcast/podcast-type.php:61
msgid "New Filter Name"
msgstr ""

#: custom-types/podcast/podcast-type.php:62
msgid "Separate Filters with commas"
msgstr ""

#: custom-types/podcast/podcast-type.php:63
msgid "Add or remove Filters"
msgstr ""

#: custom-types/podcast/podcast-type.php:64
msgid "Choose from the most used Filters"
msgstr ""

#: custom-types/podcast/podcast-type.php:65
msgid "Filters"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:13
#: custom-types/qt-contentslides/contentslides.php:14
#: custom-types/qt-contentslides/contentslides.php:16
#: custom-types/qt-contentslides/contentslides.php:17
#: custom-types/qt-contentslides/contentslides.php:18
#: custom-types/qt-contentslides/contentslides.php:20
#: custom-types/qt-contentslides/contentslides.php:22
msgid "Simple slider"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:19
#: custom-types/qt-contentslides/contentslides.php:21
#: custom-types/qt-contentslides/contentslides.php:23
#: custom-types/qt-contentslides/contentslides.php:25
msgid "Simple sliders"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:54
msgid "Simple slider type"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:55
#: custom-types/qt-contentslides/contentslides.php:68
#: custom-types/qt-events/events.php:66 custom-types/qt-events/events.php:79
#: custom-types/qt-gallery/gallery.php:54
#: custom-types/qt-gallery/gallery.php:67
msgid "Types"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:56
#: custom-types/qt-gallery/gallery.php:55
msgid "Search by type"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:57
#: custom-types/qt-gallery/gallery.php:56
msgid "Popular type"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:58
#: custom-types/qt-gallery/gallery.php:57
msgid "All types"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:61
#: custom-types/qt-events/events.php:72 custom-types/qt-gallery/gallery.php:60
msgid "Edit Type"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:62
#: custom-types/qt-events/events.php:73 custom-types/qt-gallery/gallery.php:61
msgid "Update Type"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:63
#: custom-types/qt-events/events.php:74 custom-types/qt-gallery/gallery.php:62
msgid "Add New Type"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:64
#: custom-types/qt-events/events.php:75 custom-types/qt-gallery/gallery.php:63
msgid "New Type Name"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:65
#: custom-types/qt-events/events.php:76 custom-types/qt-gallery/gallery.php:64
msgid "Separate Types with commas"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:66
#: custom-types/qt-events/events.php:77 custom-types/qt-gallery/gallery.php:65
msgid "Add or remove Types"
msgstr ""

#: custom-types/qt-contentslides/contentslides.php:67
#: custom-types/qt-events/events.php:78 custom-types/qt-gallery/gallery.php:66
msgid "Choose from the most used Types"
msgstr ""

#: custom-types/qt-events/events.php:65
msgid "Event type"
msgstr ""

#: custom-types/qt-events/events.php:69
msgid "All events"
msgstr ""

#: custom-types/qt-events/events.php:204
msgid "Event Map"
msgstr ""

#: custom-types/qt-gallery/gallery.php:13
#: custom-types/qt-gallery/gallery.php:14
#: custom-types/qt-gallery/gallery.php:16
#: custom-types/qt-gallery/gallery.php:17
#: custom-types/qt-gallery/gallery.php:18
msgid "Qantum Gallery"
msgstr ""

#: custom-types/qt-gallery/gallery.php:20
#: custom-types/qt-gallery/gallery.php:22
#: custom-types/qt-modularpages/index-modularpages.php:82
msgid "Gallery"
msgstr ""

#: custom-types/qt-gallery/gallery.php:53
msgid "Gallery type"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:26
#: custom-types/qt-modularpages/index-modularpages.php:27
#: custom-types/qt-modularpages/index-modularpages.php:29
#: custom-types/qt-modularpages/index-modularpages.php:30
#: custom-types/qt-modularpages/index-modularpages.php:31
#: custom-types/qt-modularpages/index-modularpages.php:32
#: custom-types/qt-modularpages/index-modularpages.php:33
#: custom-types/qt-modularpages/index-modularpages.php:34
#: custom-types/qt-modularpages/index-modularpages.php:38
msgid "module"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:72
msgid "Fullscreen caption"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:77
msgid "Content section"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:87
msgid "Revolution Slider"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:92
msgid "Simple Slider"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:97
msgid "Events map"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:102
msgid "Releases carousel"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:107
msgid "Podcast carousel"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:112
msgid "Artists carousel"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:117
msgid "News"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:251
#: customizer-boilerplate/options.php:50
msgid "Dark"
msgstr ""

#: custom-types/qt-modularpages/index-modularpages.php:252
#: customizer-boilerplate/options.php:53
msgid "Light"
msgstr ""

#: custom-types/qt-pages/post_taxonomy-fields.php:17
msgid "Category type layout"
msgstr ""

#: custom-types/qt-pages/post_taxonomy-fields.php:77
msgid "If not specified, will be taken the general value of the QantumPanel"
msgstr ""

#: custom-types/radio/radio-type.php:16 custom-types/radio/radio-type.php:28
msgid "Radio channels"
msgstr ""

#: custom-types/radio/radio-type.php:17
msgid "Radio channel"
msgstr ""

#: custom-types/radio/radio-type.php:18
msgid "Add new channel"
msgstr ""

#: custom-types/radio/radio-type.php:19
msgid "Add new radio channel"
msgstr ""

#: custom-types/radio/radio-type.php:20
msgid "Edit radio channel"
msgstr ""

#: custom-types/radio/radio-type.php:21
msgid "New radio channel"
msgstr ""

#: custom-types/radio/radio-type.php:22
msgid "All radio channels"
msgstr ""

#: custom-types/radio/radio-type.php:23
msgid "View radio channel"
msgstr ""

#: custom-types/radio/radio-type.php:24
msgid "Search radio channels"
msgstr ""

#: custom-types/radio/radio-type.php:25
msgid "No radio channels found"
msgstr ""

#: custom-types/radio/radio-type.php:26
msgid "No radio channels found in Trash"
msgstr ""

#: custom-types/radio/radio-type.php:55
msgid "Radio channel genres"
msgstr ""

#: custom-types/radio/radio-type.php:56
#: custom-types/release/release-type.php:45
msgid "Genre"
msgstr ""

#: custom-types/radio/radio-type.php:59
msgid "All genres"
msgstr ""

#: custom-types/radio/radio-type.php:62
#: custom-types/release/release-type.php:51
msgid "Edit genre"
msgstr ""

#: custom-types/radio/radio-type.php:63
#: custom-types/release/release-type.php:52
msgid "Update genre"
msgstr ""

#: custom-types/radio/radio-type.php:64
#: custom-types/release/release-type.php:53
msgid "Add New genre"
msgstr ""

#: custom-types/radio/radio-type.php:65
#: custom-types/release/release-type.php:54
msgid "New genre Name"
msgstr ""

#: custom-types/radio/radio-type.php:66
#: custom-types/release/release-type.php:55
msgid "Separate genres with commas"
msgstr ""

#: custom-types/radio/radio-type.php:67
#: custom-types/release/release-type.php:56
msgid "Add or remove genres"
msgstr ""

#: custom-types/radio/radio-type.php:68
#: custom-types/release/release-type.php:57
msgid "Choose from the most used genres"
msgstr ""

#: custom-types/release/release-type.php:44
msgid "Release genres"
msgstr ""

#: custom-types/release/release-type.php:48
msgid "All releases"
msgstr ""

#: custom-types/release/release-type.php:58
msgid "genres"
msgstr ""

#: customizer-boilerplate/options.php:33
msgid "Theme Colors"
msgstr ""

#: customizer-boilerplate/options.php:46
msgid "Pick a colour scheme"
msgstr ""

#: customizer-boilerplate/options.php:67
msgid "Main Color"
msgstr ""

#: customizer-boilerplate/options.php:80
msgid "General settings "
msgstr ""

#: customizer-boilerplate/options.php:93
msgid "Hide past events in archives"
msgstr ""

#: customizer-boilerplate/options.php:97
#: customizer-boilerplate/options.php:882
msgid "No"
msgstr ""

#: customizer-boilerplate/options.php:100
msgid "Yes, hide them"
msgstr ""

#: customizer-boilerplate/options.php:116
msgid "Background Image"
msgstr ""

#: customizer-boilerplate/options.php:129
#: customizer-boilerplate/options.php:133
msgid "Fullscreen background"
msgstr ""

#: customizer-boilerplate/options.php:136
msgid "Repeat image"
msgstr ""

#: customizer-boilerplate/options.php:151
msgid "Header function"
msgstr ""

#: customizer-boilerplate/options.php:164
msgid "Header Function"
msgstr ""

#: customizer-boilerplate/options.php:168
msgid "Social Sharing"
msgstr ""

#: customizer-boilerplate/options.php:171
#: customizer-boilerplate/options.php:324
msgid "Social links"
msgstr ""

#: customizer-boilerplate/options.php:174
msgid "Language switcher WPML"
msgstr ""

#: customizer-boilerplate/options.php:335
msgid "Facebook page"
msgstr ""

#: customizer-boilerplate/options.php:346
msgid "Amazon page"
msgstr ""

#: customizer-boilerplate/options.php:357
msgid "Blogger"
msgstr ""

#: customizer-boilerplate/options.php:368
msgid "Beatport"
msgstr ""

#: customizer-boilerplate/options.php:379
msgid "Behance"
msgstr ""

#: customizer-boilerplate/options.php:390
msgid "Bebo"
msgstr ""

#: customizer-boilerplate/options.php:401
msgid "Flickr page"
msgstr ""

#: customizer-boilerplate/options.php:412
msgid "Pinterest page"
msgstr ""

#: customizer-boilerplate/options.php:423
msgid "Rss page"
msgstr ""

#: customizer-boilerplate/options.php:434
msgid "Triplevision page"
msgstr ""

#: customizer-boilerplate/options.php:445
msgid "Tumblr page"
msgstr ""

#: customizer-boilerplate/options.php:456
msgid "Twitter"
msgstr ""

#: customizer-boilerplate/options.php:467
msgid "Vimeo"
msgstr ""

#: customizer-boilerplate/options.php:478
msgid "Wordpress"
msgstr ""

#: customizer-boilerplate/options.php:489
msgid "Whatpeopleplay"
msgstr ""

#: customizer-boilerplate/options.php:500
msgid "Youtube"
msgstr ""

#: customizer-boilerplate/options.php:511
msgid "Soundcloud"
msgstr ""

#: customizer-boilerplate/options.php:522
msgid "Myspace"
msgstr ""

#: customizer-boilerplate/options.php:533
msgid "Google+"
msgstr ""

#: customizer-boilerplate/options.php:544
msgid "Itunes"
msgstr ""

#: customizer-boilerplate/options.php:555
msgid "Instagram"
msgstr ""

#: customizer-boilerplate/options.php:566
msgid "Juno"
msgstr ""

#: customizer-boilerplate/options.php:577
msgid "Lastfm"
msgstr ""

#: customizer-boilerplate/options.php:588
msgid "Linkedin"
msgstr ""

#: customizer-boilerplate/options.php:599
msgid "Mixcloud"
msgstr ""

#: customizer-boilerplate/options.php:610
msgid "Resident Advisor"
msgstr ""

#: customizer-boilerplate/options.php:621
msgid "Reverbnation"
msgstr ""

#: customizer-boilerplate/options.php:635
msgid "Itunes Podcast"
msgstr ""

#: customizer-boilerplate/options.php:646
msgid "Spotify"
msgstr ""

#: customizer-boilerplate/options.php:657
msgid "HearThis"
msgstr ""

#: customizer-boilerplate/options.php:674
msgid "Branding"
msgstr ""

#: customizer-boilerplate/options.php:687
msgid "Logo header (Max 560 x 100px)"
msgstr ""

#: customizer-boilerplate/options.php:700
msgid "Logo in sidebar (any size)"
msgstr ""

#: customizer-boilerplate/options.php:714
msgid "Favicon (.ico)"
msgstr ""

#: customizer-boilerplate/options.php:730
msgid "Footer"
msgstr ""

#: customizer-boilerplate/options.php:743
msgid "Footer text"
msgstr ""

#: customizer-boilerplate/options.php:756
msgid "Sidebar"
msgstr ""

#: customizer-boilerplate/options.php:769
msgid "Sidebar Caption"
msgstr ""

#: customizer-boilerplate/options.php:782
msgid "Enable sidebar"
msgstr ""

#: customizer-boilerplate/options.php:786
#: customizer-boilerplate/options.php:822
#: customizer-boilerplate/options.php:921
msgid "Enable"
msgstr ""

#: customizer-boilerplate/options.php:789
#: customizer-boilerplate/options.php:825
#: customizer-boilerplate/options.php:924
msgid "Disable"
msgstr ""

#: customizer-boilerplate/options.php:805
msgid "Ajax page load"
msgstr ""

#: customizer-boilerplate/options.php:818
msgid "Enable Ajax Page Load"
msgstr ""

#: customizer-boilerplate/options.php:841
msgid "Music player"
msgstr ""

#: customizer-boilerplate/options.php:854
msgid "Music player visible"
msgstr ""

#: customizer-boilerplate/options.php:858
msgid "Visible"
msgstr ""

#: customizer-boilerplate/options.php:861
msgid "Invisible"
msgstr ""

#: customizer-boilerplate/options.php:875
msgid "Autoplay on site open"
msgstr ""

#: customizer-boilerplate/options.php:879
msgid "Yes"
msgstr ""

#: customizer-boilerplate/options.php:901
msgid "Google Fonts"
msgstr ""

#: customizer-boilerplate/options.php:917
msgid "Enable Google Fonts"
msgstr ""

#: customizer-boilerplate/options.php:941
msgid "Captions Font Weight"
msgstr ""

#: customizer-boilerplate/options.php:960
msgid "Text Font"
msgstr ""

#: inc/customizations.php:131 inc/customizations.php:151
msgid "Home"
msgstr ""

#: inc/extras.php:60
#, php-format
msgid "Page %s"
msgstr ""

#: inc/pagination.php:5
msgid "page"
msgstr ""

#: inc/related_artist.php:64
msgid "Related Artists"
msgstr ""

#: inc/related_podcast.php:61
msgid "Related Podcast"
msgstr ""

#: inc/related_posts.php:54
msgid "Related Posts"
msgstr ""

#: inc/related_releases.php:53
msgid "Related Releases"
msgstr ""

#: inc/template-tags.php:39
msgctxt "Previous post link"
msgid "<span class=\"qticon-arrow-left\"></span>"
msgstr ""

#: inc/template-tags.php:40
msgctxt "Next post link"
msgid "<span class=\"qticon-arrow-right\"></span>"
msgstr ""

#: inc/template-tags.php:45
msgid "<span class=\"qticon-arrow-left\"></span> Older posts"
msgstr ""

#: inc/template-tags.php:49
msgid "Newer posts <span class=\"qticon-arrow-right\"></span>"
msgstr ""

#: inc/template-tags.php:82
msgid "Pingback:"
msgstr ""

#: inc/template-tags.php:122
#, php-format
msgid "%s <br> "
msgstr ""

#: inc/template-tags.php:124
#, php-format
msgctxt "1: date, 2: time"
msgid "%1$s at %2$s"
msgstr ""

#: inc/template-tags.php:130
msgid "Your comment is awaiting moderation."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:229 plugins/tgm/plugins.php:123
msgid "Install Required Plugins"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:230 plugins/tgm/plugins.php:124
msgid "Install Plugins"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:231 plugins/tgm/plugins.php:125
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:232
msgid "Something went wrong."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:233 plugins/tgm/plugins.php:127
#, php-format
msgid "This theme requires the following plugin: %1$s."
msgid_plural "This theme requires the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:234 plugins/tgm/plugins.php:128
#, php-format
msgid "This theme recommends the following plugin: %1$s."
msgid_plural "This theme recommends the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:235 plugins/tgm/plugins.php:129
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to install the %s plugin. "
"Contact the administrator of this site for help on getting the plugin "
"installed."
msgid_plural ""
"Sorry, but you do not have the correct permissions to install the %s "
"plugins. Contact the administrator of this site for help on getting the "
"plugins installed."
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:236 plugins/tgm/plugins.php:130
#, php-format
msgid "The following required plugin is currently inactive: %1$s."
msgid_plural "The following required plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:237 plugins/tgm/plugins.php:131
#, php-format
msgid "The following recommended plugin is currently inactive: %1$s."
msgid_plural "The following recommended plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:238 plugins/tgm/plugins.php:132
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to activate the %s "
"plugin. Contact the administrator of this site for help on getting the "
"plugin activated."
msgid_plural ""
"Sorry, but you do not have the correct permissions to activate the %s "
"plugins. Contact the administrator of this site for help on getting the "
"plugins activated."
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:239 plugins/tgm/plugins.php:133
#, php-format
msgid ""
"The following plugin needs to be updated to its latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgid_plural ""
"The following plugins need to be updated to their latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:240 plugins/tgm/plugins.php:134
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to update the %s plugin. "
"Contact the administrator of this site for help on getting the plugin "
"updated."
msgid_plural ""
"Sorry, but you do not have the correct permissions to update the %s plugins. "
"Contact the administrator of this site for help on getting the plugins "
"updated."
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:241 plugins/tgm/plugins.php:135
msgid "Begin installing plugin"
msgid_plural "Begin installing plugins"
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:242
msgid "Begin activating plugin"
msgid_plural "Begin activating plugins"
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:243 plugins/tgm/plugins.php:137
msgid "Return to Required Plugins Installer"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:244
msgid "Return to the dashboard"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:245
#: plugins/tgm/class-tgm-plugin-activation.php:2166
#: plugins/tgm/plugins.php:138
msgid "Plugin activated successfully."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:246
#: plugins/tgm/class-tgm-plugin-activation.php:1823
msgid "The following plugin was activated successfully:"
msgid_plural "The following plugins were activated successfully:"
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/class-tgm-plugin-activation.php:247
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:248
msgid "Dismiss this notice"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:593
#: plugins/tgm/class-tgm-plugin-activation.php:2402
msgid "Return to the Dashboard"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:685
msgid ""
"The remote plugin package is does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:685
#: plugins/tgm/class-tgm-plugin-activation.php:689
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:689
msgid ""
"The remote plugin package consists of more than one file, but the files are "
"not packaged in a folder."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1328
#: plugins/tgm/class-tgm-plugin-activation.php:1505
msgid "Private Repository"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1332
#: plugins/tgm/class-tgm-plugin-activation.php:1509
msgid "Pre-Packaged"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1337
msgid "WordPress Repository"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1341
msgid "Required"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1343
msgid "Recommended"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1347
msgid "Not Installed"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1349
msgid "Installed But Not Activated"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1456
#, php-format
msgctxt "%2$s = plugin name in screen reader markup"
msgid "Install %2$s"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1478
#, php-format
msgctxt "%2$s = plugin name in screen reader markup"
msgid "Activate %2$s"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1530
#, php-format
msgid ""
"No plugins to install or activate. <a href=\"%1$s\">Return to the Dashboard</"
"a>"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1546
msgid "Plugin"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1547
msgid "Source"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1548
msgid "Type"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1549
msgid "Status"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1567
msgid "Install"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1568
msgid "Activate"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1679
msgid "No plugins are available to be installed at this time."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:1800
msgid "No plugins are available to be activated at this time."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2149
msgid "Install package not available."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2150
#, php-format
msgid "Downloading install package from <span class=\"code\">%s</span>&#8230;"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2151
msgid "Unpacking the package&#8230;"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2152
msgid "Installing the plugin&#8230;"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2153
msgid "Plugin install failed."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2154
msgid "Plugin installed successfully."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2165
msgid "Plugin activation failed."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2288
#, php-format
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2289
#, php-format
msgid "The installation of %1$s failed."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2293
msgid ""
"The installation and activation process is starting. This process may take a "
"while on some hosts, so please be patient."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2294
#, php-format
msgid "%1$s installed and activated successfully."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2294
#: plugins/tgm/class-tgm-plugin-activation.php:2301
msgid "Show Details"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2294
#: plugins/tgm/class-tgm-plugin-activation.php:2301
msgid "Hide Details"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2295
msgid "All installations and activations have been completed."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2296
#, php-format
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2300
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2301
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2302
msgid "All installations have been completed."
msgstr ""

#: plugins/tgm/class-tgm-plugin-activation.php:2303
#, php-format
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: plugins/tgm/plugins.php:126
msgid "Something went wrong with the plugin API."
msgstr ""

#: plugins/tgm/plugins.php:136
msgid "Activate installed plugin"
msgid_plural "Activate installed plugins"
msgstr[0] ""
msgstr[1] ""

#: plugins/tgm/plugins.php:139
#, php-format
msgid "All plugins installed and activated successfully. %s"
msgstr ""
