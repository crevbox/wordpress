<?php
/*
*
*   Module
*
*/
global $tempEvent; // variables defined in the mother page
$moduleId = $tempEvent['moduleId'];
$qw_lightness = $tempEvent['qwlightness'];
$contentId = get_post_meta( $moduleId, $key = 'simplesliderid', $single = true );
$milliseconds = esc_attr(get_post_meta( $contentId[0], $key = 'milliseconds', $single = true ));

?>
<div class="<?php echo esc_attr($qw_lightness); ?> qw-simpleslider" data-milliseconds="<?php echo esc_js($milliseconds); ?>">
	<?php
	$events = get_post_meta($contentId[0], 'galleryitem', true); 
	if($events != ''){
	?>

			<?php
				$i = 0;
				if(is_array($events)){
					foreach($events as $event){ 
						$databgimage = '';
						if($event['image'] != ''){
							$img = wp_get_attachment_image_src($event['image'],'full');
							$databgimage = ' data-bgimage="'.esc_url($img[0]).'" ';
						}
						$text = add_filter($event['text'], 'do_shortcode'); 
						?>
						<div class="fullPageSlide" data-bgimage="<?php echo esc_url($img[0]); ?>"  id="slide-<?php echo esc_attr($moduleId).'-'.esc_attr($i);?>" data-anchor="slide-<?php echo esc_attr($moduleId).'-'.esc_attr($i);?>">				
							<div class="qw-vp   qw-gbcolorlayer" >
								<div class="qw-vc text-center">
										<?php echo wp_kses_post($event['text']); ?>
								</div>
							</div>
						</div>
						<?php
					}
				}
			?>
			<ul class="qw-slides-menu fp-slidesNav">
				<?php
				if(is_array($events)){
				$active = 'active';
				foreach($events as $m){
					?>
					<li>
					<a class="qw-slidelink <?php echo esc_attr($active); ?>"  href="#"><span class="qticon-record"></span></a>
					</li>
					<?php
					$active = '';
				}
				}
				?>
			</ul>
	<?php
	}
	?>
</div>


