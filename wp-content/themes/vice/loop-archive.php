<?php while ( have_posts() ) : the_post(); ?>
	<?php
	global $post;
	$sticky = '';
	if(is_sticky($post->ID)){
		$sticky = " qw-padded qw-glassbg ";
	}
	?>
	<div <?php post_class("qw-archive-item".esc_attr($sticky)); ?> id="post-<?php the_ID(); ?>">
		<div class="row">
			<div class="col-md-3 col-lg-3">
				<p class="qw-dateblock">
					<span class="qw-dateday">
						<?php the_time('d');?>
					</span>
					<span class="qw-datemonth">
						<?php the_time('F');?>
					</span>
					<span class="qw-dateyear">
						<?php the_time('Y');?>
					</span>
				</p>

			</div>
			<div class="col-md-9 col-lg-9">

				
				
				<h4 class="qw-page-subtitle qw-knifetitle qw-top0">
				<?php echo esc_attr__("Posted By:","_s"); ?> <?php echo get_the_author(); ?> // <a href="<?php esc_url(the_permalink()); ?>" class="qw-coloredtext"><?php echo esc_attr__("Read More", "_s"); ?></a>
				</h4>
				
				<hr class="qw-separator">
				<h1 class="qw-page-title qw-top15">
					<a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a>
				</h1>

				
				<a href="<?php esc_url(the_permalink()); ?>">
					<?php the_post_thumbnail( 'large',array('class'=>'img-responsive') ); ?>
				</a>
				
				
				<?php the_excerpt(); ?>	
			</div>
		</div>
	</div>
<?php endwhile; // end of the loop. ?>