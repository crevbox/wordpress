<?php
/*
*
*   Module
*
*/
global $post; // the main object of the modular page containing this module
global $GlobalMonthsArray; 
global $tempEvent; // variables defined in the mother page
$moduleId = $tempEvent['moduleId'];
$qw_elements_quantity = esc_attr(get_post_meta( $post->currentmodule, $key = 'qw_elements_quantity', $single = true ));
$qw_lightness = $tempEvent['qwlightness'];
if($qw_lightness == ''){$qw_lightness == 'light';}
?>
<div class="qw-vp qw-gbcolorlayer">
	<div class="qw-vc">
		<div class="container">
			<div class="qw-section-padding">

			
				<?php 
				if ($tempEvent['hide_title'] == '' || $tempEvent['hide_title'] == '0'){	?>
				<h1 class="qw-moduletitle"><?php  echo esc_attr(get_the_title( $tempEvent['moduleId'])); ?></h1>	
				<?php } ?>
				<div class="qw-news-carousel row <?php echo esc_attr($qw_lightness); ?>">				
						<?php
						global $post;
						$args = array( 'numberposts' => '3','post_type' => 'post', 'post_status' => 'publish','suppress_filters' => false );
						$recent_posts = get_posts( $args );
						foreach( $recent_posts as $post ) : setup_postdata($post);
			                $n = "12";
							?>
							<div class="col-sm-4 col-md-4">
								<hr class="qw-separator qw-separator-thin qw-top0">
								<!-- <h4 class="qw-page-subtitle qw-knifetitle"><?php the_time(esc_attr(get_option('date_format'))) ?></h4> -->
								<p class="qw-small qw-caps"><?php the_time(esc_attr(get_option('date_format'))) ?></p>

								
								<?php
								 	if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium"  ) )  {?>
										
										<a data-qwjquerycontent="<?php echo esc_js(get_permalink($post->ID)); ?>"  href="<?php echo esc_url(get_permalink($post->ID)); ?>" class="qwjquerycontent qw-newscarousel-featimage" data-bgimage="<?php echo esc_js(esc_url($thumb['0'])); ?>">
											<img src="<?php echo esc_url($thumb['0']) ; ?>" class="img-responsive hidden" alt="<?php esc_attr(the_title()); ?>">
										</a>
										
										<?php								
					                } 
								?>
								<?php
								$permalink = esc_url(get_permalink($post->ID));
								?>
								<div class="qw-glassbg qw-padded qw-contents">
									<h3>
										<a class="qwjquerycontent" data-qwjquerycontent="<?php echo esc_js(esc_url($permalink)); ?>" href="<?php echo esc_url($permalink); ?>">
											<?php the_title(); ?>
										</a>
									</h3>
									<?php the_excerpt();?>
									<a class="qwjquerycontent btn btn-primary btn-xs" data-qwjquerycontent="<?php echo esc_js(esc_url($permalink)); ?>" href="<?php echo esc_url($permalink); ?>">
										<?php
										echo esc_attr__("Read More","_s");
										?>
									</a>

								</div>
								
							</div>
							<?php
						endforeach;
						wp_reset_postdata();

						?>			
								
				</div>
				<?php  
				$archive_link_url = get_post_meta( $moduleId, 'archive_link_url', true );
				$archive_link_text = get_post_meta( $moduleId, 'archive_link_text', true );
				if($archive_link_url){ ?>
					<p class="text-center qw-top30 qw-top30">
					<a href="<?php echo esc_attr($archive_link_url); ?>" class="btn btn-primary"><?php echo esc_attr($archive_link_text); ?></a>
					</p>
				<?php } ?>	
				
			</div>
		</div>
	</div>
</div>

	




