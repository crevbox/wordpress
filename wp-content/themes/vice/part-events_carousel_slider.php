<?php
global $GlobalMonthsArray; 
global $post;

$qw_elements_quantity = $post->qw_elements_quantity;

?>
<h3 class="qw-page-subtitle qw-knifetitle"><?php echo __("Upcoming Events", "_s"); ?>
<!-- Controls -->
<span class="pull-right">
    <a href="#carouselevents<?php echo esc_attr($post->ID);?>" role="button" data-slide="prev">
        <span class="qticon-chevron-left"></span>
    </a>
    <a href="#carouselevents<?php echo esc_attr($post->ID);?>" role="button" data-slide="next">
        <span class="qticon-chevron-right"></span>
    </a>
</span>
</h3>
<hr class="qw-separator">
<?php 
$args  = array(
    'post_type' => 'event',
    'meta_key' => EVENT_PREFIX.'date',
    'orderby' => 'meta_value',
    'order' => 'ASC'
    ,'suppress_filters' => false
);
$events = get_posts($args); 
if(count($events) > 0){ ?>
    <div id="carouselevents<?php echo esc_attr($post->ID);?>" class="carousel slide qw-module-smallslideshow qw-top15">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php
            $active = 'active';
            $col = 0;
            for($i = 0; $i < $qw_elements_quantity; $i++){
                $page = $events[$i];
                if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), array(60,60)  ) )  {
                $thumb       = $thumb['0'];
                } 
                $e = array(
                'id' =>  $page->ID,
                'date' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'date',true)),
                'location' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'location',true)),
                'street' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'street',true)),
                'city' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'city',true)),
                'permalink' =>  esc_url(get_permalink($page->ID)),
                'title' =>  esc_attr($page->post_title),
                'thumb' => $thumb
                );

                $d = explode('-',$e['date']);
                $time = mktime(0, 0, 0, $d[1]);

                /*
                *
                *   Apri una colonna
                *
                */

                if ($i == 0 || ($i % 3 == 0)){ 
                     echo '<div class="item '.esc_attr($active).'">';
                    $open = 1;
                }
                ?>
                    <div class="row qw-archive-item">
                      <div class="col-xs-2 col-md-2 col-lg-2">
                          <p class="qw-dateblock">
                              <span class="qw-dateday">
                                  <?php echo esc_attr($d[2]);?>
                              </span>
                              <span class="qw-datemonth">
                                  <?php 
                                  echo /*$GlobalMonthsArray[$d[1]]*/$d[1];
                                  ?>
                              </span>
                          </p>
                      </div>
                      <div class="col-xs-10  col-md-10 col-lg-10">
                          <h1 class="qw-page-title">
                              <a href="<?php echo esc_attr($e['permalink']); ?>"><?php echo esc_attr($e['title']); ?></a>
                          </h1>
                          <div class="canc"></div>
                          <p class="qw-small qw-caps"> <?php echo esc_attr($e['city']); ?> // <a href="<?php echo esc_url($e['permalink']); ?>" class="qw-coloredtext"><?php echo esc_attr__("More Info", "_s"); ?></a></p>
                          <hr class="qw-separator qw-separator-thin">
                      </div>
                    </div>
                <?php 
                if  (($i+1) % 3 == 0){ 
                    echo '</div>';
                    $open = 0; 
                    $col++;
                } 
            $active = ''; 
            
        }/*foreach*/ 
    
    if(isset($open)){
      if($open == 1) { 
        ?> 
        </div> 
        <?php 
        $col++; 
      }
    } 
    ?>
    </div>
    <!-- Indicators -->
    <ol class="carousel-indicators qw-darkbg qw-fancyborder-bottom" id="carouselindicators<?php echo esc_attr($post->ID);?>">
      <?php
       $active = 'active';
        for($c = 0; $c < ($qw_elements_quantity/3); $c++){
        ?>
        <li data-target="#carouselevents<?php echo esc_attr($post->ID);?>" data-slide-to="<?php echo esc_js(esc_attr($c)); ?>" class="<?php echo esc_attr($active); ?>"></li>
        <?php
        $active = '';
      }
      ?>
    </ol>
</div>
<?php } /* if >0 */ ?>