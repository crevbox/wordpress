<?php
/*
*
*   Module
*
*/
global $post; // the main object of the modular page containing this module
global $GlobalMonthsArray; 
global $tempEvent; // variables defined in the mother page
$qw_elements_quantity = 50;//get_post_meta( $post->currentmodule, $key = 'qw_elements_quantity', $single = true );
$qw_lightness = esc_attr($tempEvent['qwlightness']);
$moduleId = esc_attr($tempEvent['moduleId']);
$hidepast = get_post_meta($moduleId,'hide_past_events',true);
?>
<div class="qw-fixedcontents qtVideoBg" style="overflow-x:hidden !important">
	<?php 
	if ($tempEvent['hide_title'] == '' || $tempEvent['hide_title'] == '0'){	?>
	<h1 class="qw-moduletitle qw-fixedtitle"><?php  echo esc_attr(get_the_title( $tempEvent['moduleId'])); ?></h1>	
	<?php } ?>

	    <div class="qw-fixedcontents-layer2-box qw-hidingbox qw-animated">

	        <?php
	        /*
	        *
	        *	Create the list of events and the map
	        *
	        */

			$args  = array(
			    'post_type' => 'event',
			    'post_status' => 'publish',
			    'meta_key' => EVENT_PREFIX.'date',
			    'orderby' => 'meta_value',
			    'posts_per_page'   => 32,
			    'order' => 'ASC',
			    'suppress_filters' => false

			);



			if($hidepast == 1){
				$args['meta_query'] = array(
	            array(
	                'key' => 'eventdate',
	                'value' => date('Y-m-d'),
	                'compare' => '>=',
	                'type' => 'date'
	                 )
	           );
			}



			$events = get_posts($args); 


			//print_r($events);
			//die (count($events));

			if(count($events) > 0){ ?>
			    <div id="carouselevents<?php echo esc_attr($moduleId);?>" class="qw-module-scrollable qw-module-smallslideshow" data-lightness="<?php echo esc_attr($qw_lightness); ?>" <?php if(!my_wp_is_mobile()){ ?>data-dynamicmap="dynamicmap<?php echo esc_attr($post->currentmodule); ?>"<?php } ?>>
			    <div class="qw-scrollable-inner">
			        <?php
			            $active = 'active';
			            $col = 0;

			            if(count($events) < $qw_elements_quantity){
			               $qw_elements_quantity = count($events);
			            }
			            for($i = 0; $i < $qw_elements_quantity; $i++){
			                $page = $events[$i];
			                if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), array(150,150)  ) )  {
			                $thumb       = $thumb['0'];
			                } 
			                $e = array(
			                'id' =>  $page->ID,
			                'date' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'date',true)),
			                'location' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'location',true)),
			                'street' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'street',true)),
			                'city' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'city',true)),
			                'permalink' =>  esc_url(get_permalink($page->ID)),
			                'title' =>  esc_attr($page->post_title),
			                'thumb' => $thumb
			                );
			                

			                if($hidepast == 1){
			                	if($e['date'] < date("Y-m-d")){ continue; }	
			                }
			                

			                $d = explode('-',$e['date']);
			                $time = mktime(0, 0, 0, $d[1]);

			                $e["textdate"] = esc_attr($d[2]).' '.esc_attr($GlobalMonthsArray[$d[1]]).' '.esc_attr($d[0]);


			                $jsdata = array();
			                foreach($e as $a => $b){
			                	$jsdata[$a] = esc_js($b);
			                }

			                ?>
			                    <div class="row qw-events_carousel_listitem">
			                      <div class="col-xs-3 col-md-2 col-lg-2">
			                          <p class="qw-dateblock">
			                              <span class="qw-dateday">
			                                  <?php echo esc_attr($d[2]);?>
			                              </span>
			                              <span class="qw-datemonth">
			                                  <?php 
			                                  echo esc_attr($d[1]);
			                                  ?>
			                              </span>
			                          </p>
			                      </div>
			                      <div class="col-xs-9  col-md-10 col-lg-10">
			                                <?php
			                                    /*
			                                    *
			                                    *   Prepare coords for the map
			                                    *
			                                    */
			                                    $coord = esc_attr(get_post_meta($page->ID,EVENT_PREFIX . 'coord', true));
			                                    if($coord != ''){
			                                    $coor = explode(",",esc_attr(get_post_meta($page->ID,EVENT_PREFIX . 'coord', true)));
			                                    if(count($coor) == 2){
		                                            ?>
		                                            <a id="mapCT<?php echo esc_attr($e['id']); ?>" href="<?php echo esc_url($e['permalink']); ?>" 
		                                            	class="qwNoAjax"
		                                                data-clicktarget = "mapbutton<?php echo esc_js(esc_attr($post->ID));?>"
		                                                data-mapid = "carouselevents<?php echo esc_js(esc_attr($post->ID));?>"
		                                                data-markerid = "<?php echo esc_js(esc_attr($jsdata['id'])); ?>"
		                                                data-markertitle = "<?php echo esc_js(esc_attr($jsdata['title'])); ?>"
		                                                data-markerimg = "<?php echo esc_js(esc_url($jsdata['thumb'])); ?>"
		                                                data-markerdate = "<?php echo esc_js(esc_attr($jsdata['textdate'])); ?>"
		                                                data-markerlink = "<?php echo esc_js(esc_url($jsdata['permalink'])); ?>"
		                                                data-markeranchor = "<?php echo esc_js(esc_attr__('Info & Ticket','_s')); ?>"
		                                                data-markerlocation = "<?php echo esc_js(esc_attr($jsdata['location'])).' - '.esc_js(esc_attr($jsdata['city'])); ?>"
		                                                data-mapitem data-dynamicmaptarget="dynamicmap<?php echo esc_js(esc_attr($post->currentmodule)); ?>" 
		                                                data-lat="<?php echo esc_js($coor[0]); ?>" 
		                                                data-lon="<?php echo esc_js($coor[1]); ?>">
		                                            <?php
			                                        }
			                                    }else{
			                                        ?>
			                                        <a href="<?php echo esc_url($e['permalink']); ?>">
			                                        <?php

			                                    }
			                                ?>
			                                <span class="qw-page-title">
			                                    <?php echo esc_attr($e['title']); ?>
			                                </span>
			                                <div class="canc"></div>
			                                <span class="qw-details qw-small qw-caps"> 
			                                    <?php echo esc_attr($e['location']).' // '.esc_attr($e['city']); ?>
			                                </span>
			                                <div class="qw-separator qw-separator-thin"></div>
			                            </a>
			                      </div>
			                    </div>
			                <?php 
			        	}/*foreach*/ 
			    ?>
			    </div>
			</div>
			<?php 
			} 
			/* if >0 
			//=========== END OF THE MAP LOOP ==============*/ 
			?>

			<a href="#" class="qw-module-scrollable-top qw-negative qw-animated" data-scrollingtarget="carouselevents<?php echo esc_attr($moduleId);?>" data-scrollof="58"><div class="icon qticon-chevron-up"></div></a>
			<a href="#" class="qw-module-scrollable-down qw-negative qw-animated" data-scrollingtarget="carouselevents<?php echo esc_attr($moduleId);?>" data-scrollof="-58"><div class="icon qticon-chevron-down"></div></a>
	    </div>
	    <div class="qw-fixedcontents-layer1 qw-dynamic-map" id="dynamicmap<?php echo esc_attr($post->currentmodule); ?>"></div>
</div>