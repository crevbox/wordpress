<?php
/*
*
*   Module
*
*/

wp_reset_postdata();
global $post; // the main object of the modular page containing this module
global $GlobalMonthsArray; 
global $tempEvent; // variables defined in the mother page
$moduleId = $tempEvent['moduleId'];
$qw_elements_quantity = esc_attr(get_post_meta( $post->currentmodule, 'qw_elements_quantity',  true ));
$qw_lightness = $tempEvent['qwlightness'];
?>
<div class="qw-vp qw-gbcolorlayer">
	<div class="qw-vc">
		<?php if ($tempEvent['hide_title'] == '' || $tempEvent['hide_title'] == '0'){	?>
			<h1 class="qw-moduletitle"><?php  echo esc_attr(get_the_title( $tempEvent['moduleId'])); ?></h1>	
		<?php } ?>
		<div class="qw-artist-carousel <?php echo esc_attr($qw_lightness); ?>">
				<?php
				/*
				*
				*
				*
				*	Start of the carousel
				*
				*
				*/

				$args  = array(
				    'post_type' => 'artist'
				    ,'posts_per_page'=>$qw_elements_quantity
				    ,'orderby'=> array(  'menu_order' => 'ASC' ,	'post_date' => 'DESC')				    
				    ,'suppress_filters' => false // added recently for wpml
				);
				$events = get_posts($args); 
				$howMany = count($events);
				if($howMany > 0){ ?>
					<?php
					if(count($events) < $qw_elements_quantity){
		               $qw_elements_quantity = count($events);
		            }
		            $menuArray = array(); 
					for($i = 0; $i < $qw_elements_quantity; $i++){

						$page = $events[$i];
						setup_postdata( $page );
						$e = array(
			                'id' =>  $page->ID,
			              //  'date' =>  get_post_meta($page->ID,'general_release_details_release_date',TRUE),
			                'permalink' =>  esc_url(get_permalink($page->ID)),
			                'title' =>  $page->post_title,
			                );
						$thumb  = '';
						$databgimage = '';
		                if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), array(744,414)  ) )  {
		                    $thumbhtml       = '<a class="qwjquerycontent" data-qwjquerycontent="'.esc_js($e['permalink']).'" href="'.esc_url(get_permalink($e['id'])).'">
		                    <img src="'.esc_url($thumb['0']).'" class="img-responsive qw-artist-carousel-pic" alt="'.esc_attr($page->post_title).'"></a>';
		                    $databgimage = ' data-bgimage="'.esc_url($thumb['0']).'" ';
		                    $menuArray[] = array('image'=>esc_url($thumb['0']), 'title' => esc_attr($e['title']));
		                    //die($thumb['0']);
		                } 
					?>
					<div class="fullPageSlide qw-quantity-<?php echo esc_attr($howMany); /* fix for when there is only one artist */ ?>" id="slide<?php echo esc_attr($e['id']);?>">
						<div class="container">
							<div class="row qw-artist-carousel-item">
								<div class="col-xs-12 col-xs-offset-3 col-xs-offset-0 col-sm-12  col-md-10 col-md-offset-1  col-lg-10 col-lg-offset-1">
									<div class="qw-bgpanel">
										<div class="row qw-top30">
											<div class="col-xs-12 col-sm-5 col-sm-4 col-md-5">
												<?php echo '<a class="qwjquerycontent" data-qwjquerycontent="'.esc_js($e['permalink']).'" href="'.esc_url(get_permalink($e['id'])).'"><img src="'.esc_url($thumb['0']).'" class="img-responsive qw-artist-carousel-pic" alt="'.esc_attr($page->post_title).'"></a>' ?>
												<a data-qwjquerycontent="<?php echo esc_js($e['permalink']);?>"  href="<?php echo esc_url($e['permalink']); ?>" class="hidden-xs qwjquerycontent btn btn-primary qw-animated qw-fullwidth"><?php echo esc_attr(__("Read more",'_s')); ?></a>
											</div>
											<div class="col-xs-12 col-sm-7 col-sm-8 col-md-7 qw-thecontent">
												<div class="qw-padded qw-glassbg">
													<?php
													/*
													*
													*	Artist nationality
													*
													*/
													$nat = get_post_meta($page->ID, '_artist_nationality',true);
													$res = get_post_meta($page->ID, '_artist_resident',true);
													if($nat != '' || $res != ''){
													?>
													<h4 class="qw-page-subtitle qw-knifetitle "><?php echo esc_attr($nat).' // '.esc_attr($res); ?></h4>
													<hr class="qw-separator">
													<?php }	?>


													<h1 class="qw-page-title qw-top15"><?php echo esc_attr($e['title']);?></h1>
													<div class="hidden-xs">
													<?php
														  the_excerpt();
													?>
													</div>
													

													<ul class="qw-social hidden-xs">
													<?php
													echo social_link_icons($e['id']);
													?>
													</ul>
								   				</div>
							   				</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
			<?php }} /* If and loop closing */ ?>
			<?php
			/*
			*
			*
			*
			*	End of the carousel
			*
			*
			*
			*/
			wp_reset_query();
			wp_reset_postdata();
			?>
		</div>
		<?php
		/*
		*
		*	Navigation menu
		*
		*/
		if(!my_wp_is_mobile()){
			?>
			<ul class="qw-slides-menu fp-slidesNav hidden-xs qw-quantity-<?php echo esc_attr($howMany); /* fix for when there is only one artist */ ?>">
				<?php
				$active = 'active';
				foreach($menuArray as $m){
					?>
					<li>
					<a data-toggle="tooltip" class="qw-slidelink <?php echo esc_attr($active); ?>" title="<?php echo esc_attr($m['title']); ?>" href="#"><img src="<?php echo esc_url($m['image']); ?>" alt="<?php echo esc_attr($m['title']); ?>" class="img-responsive"></a>
					</li>
					<?php
					$active = '';
				}
				?>
			</ul>
		<?php }  ?>
	</div>
</div>
