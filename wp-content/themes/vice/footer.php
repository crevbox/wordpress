
				<?php
				wp_reset_postdata();
				if(!is_tax() && !is_category() && !is_tag() && !is_tax() ){
					
					
					
					if(basename( get_page_template()) != 'page-modular.php'){
						/*
						*	If is modular page, footer are loaded as module (optional)
						*
						*/
						?>
						<div class="qw-spacer-50"></div>
						<div class="qw-footer-container" id="qwStaticFooter">							
									<?php get_template_part( 'part', 'footerwidgets'); ?>			
						</div>
						<?php


					}
				}else{

					?>
					<div class="qw-spacer-50"></div>
					<div class="qw-footer-container" id="qwStaticFooter">
						<?php get_template_part( 'part', 'footerwidgets'); ?>						
					</div>
					<?php
				}
				?>

				
				
			</div><!-- .qw-content-wrapper (header.php)-->
		</div><!-- .qw-totalcontainer (header.php) -->
		<?php get_template_part('part','header'); ?>
		<?php get_template_part( 'part','playlist'); ?>
		
			
      	<?php wp_footer(); ?>

      	
  </body>
</html>
