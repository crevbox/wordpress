<?php 

$args  = array(
    'post_type' => 'radiochannel',
    'post_status' => 'publish',
    'posts_per_page'=>-1
    ,'suppress_filters' => false
);

$wpbp = new WP_Query($args ); 
$wp_query = $wpbp; 
$active = 'active';




if ($wpbp->have_posts()) : while ($wpbp->have_posts()) : $wpbp->the_post(); 



    $page = $post;
    setup_postdata( $page );
    $image  = '';
    if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), array(500,500)  ) )  {
        $image       = $thumb['0'];// '<img src="'.esc_url($thumb['0']).'" class="img-responsive hidden-xs" alt="'.esc_attr($page->post_title).'">';
    } 
    $e = array(
    'id' =>  $page->ID,
    'date' =>  esc_attr(get_post_meta($page->ID,'general_release_details_release_date',TRUE)),
    'permalink' =>  esc_url(get_permalink($page->ID)),
    'radio_title' =>  esc_attr(get_post_meta($page->ID,'radio_title',TRUE)),
    'radio_subtitle' =>  esc_attr(get_post_meta($page->ID,'radio_subtitle',TRUE)),
    'orderby' => 'menu_order',
    'mp3_stream_url' => esc_attr(get_post_meta($page->ID,'mp3_stream_url',TRUE)),
    'thumb' => $image
    );
   	$activeTrack = 'activeTrack';

    if(get_post_meta($page->ID,'add_to_custom_playlist',TRUE) != '1'){ continue; }



    ?>






		<tr class="qw-animated <?php echo $activeTrack; $activeTrack=''; ?>" >                        
			<td class="qw-animated">                       
				<a href="#" class="firstAdded" 
					data-releasepage="<?php echo esc_attr($e['permalink']); ?>" 
					data-playtrack="<?php echo esc_attr($e['mp3_stream_url']);?>" 
					data-title="<?php echo esc_attr($e['radio_title']); ?>" 
					data-author="<?php echo esc_attr($e['radio_subtitle']); ?>" 
					data-buyurl="" 
					data-cover="<?php echo esc_attr($e['thumb']); ?>">
			 	<span class="qticon-play"></span>
			 	</a>
			</td>                        
			<td class="qw-animated">
				 <img src="<?php echo esc_attr($e['thumb']); ?>" class="img-responsive qw-playlist-thumb">
			</td>
			
			<td class="qw-animated hidden-xs">
				 <span class="line1"><?php echo esc_attr($e['radio_title']); ?></span>
			</td>                        
			 <td class="qw-animated">
			 	<span class="line1"><?php echo esc_attr($e['radio_subtitle']); ?></td>
			 <td class="text-center qw-animated hidden-xs">
			 <a href="<?php echo esc_attr($e['permalink']); ?>">
			 	<span class="qticon-forward"></span></a></td><td class="text-center qw-animated hidden-xs qw-buylink">
			 </td>
			 <td class="text-center qw-animated hidden-xs" data-removerow="">
			 	<!-- <a href="#"><span class="qticon-close-sign"></span></a>                         -->
			 </td>
		 </tr>





    <?php 
	


endwhile;endif;

wp_reset_postdata();
wp_reset_query();

/*
*
*
*
*	End of the radio station list
*
*
*
*/
?>