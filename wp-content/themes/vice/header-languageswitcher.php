<div class="qw-languages-list-top" id="qwLLT">
<?php  
	if(function_exists('icl_get_languages')){
	    $languages = icl_get_languages('skip_missing=0&orderby=name&order=asc');
	    if(!empty($languages)){
	        foreach($languages as $l){
	     	  	?>
	        	<a href="<?php echo $l['url']; ?>"><img src="<?php echo $l['country_flag_url']; ?>"> <?php echo $l['code']; ?></a>
	        	<?php
	        }
	    }
	}
?>
</div>