<?php
/*
*
*   Module
*
*/
global $tempEvent; // variables defined in the mother page
$moduleId = $tempEvent['moduleId'];
$galleryId = get_post_meta( $moduleId, $key = 'galleryid', $single = true );
?>

<div class="qw-section-padding qw-gbcolorlayer">

	<?php 
	if ($tempEvent['hide_title'] == '' || $tempEvent['hide_title'] == '0'){	?>
	<h1 class="qw-moduletitle"><?php  echo esc_attr(get_the_title( $tempEvent['moduleId'])); ?></h1>	
	<?php } ?>
	<?php
		$galleryContainer = get_post($moduleId);
		//echo apply_filters( 'the_content', get_post_field('post_content', $moduleId) );
	?>
	<div class="container">
				<?php
					echo apply_filters( 'the_content', get_post_field('post_content', $moduleId) );
				?>
			
				<div class="Collage effect-parent" data-fx="<?php echo get_post_meta($galleryId[0], 'fx', true); ?>">
				<?php
					//print_r( $galleryId);
					$events = get_post_meta($galleryId[0], 'galleryitem', true); 
					//print_r($events);
					if(is_array($events)){
						foreach($events as $event){ 
							$img =  wp_get_attachment_image_src($event['image'],'medium');
						//	print_r($event['video']);
							$link = '';
							if(array_key_exists('video',$event)){
								if($event['video'] != ''){
									$link = $event['video'];
								}
							}
							if($link =='') {
								$img2 =  wp_get_attachment_image_src($event['image'],'large');
								$link = $img2[0];
							}
							?>
							<div class="Image_Wrapper" data-caption="<?php echo  esc_js(esc_attr($event['title'])); ?>"><a href="<?php echo esc_url($link); ?>" class="qw-disableembedding" ><img src="<?php echo esc_url($img[0]) ; ?>" alt="<?php echo esc_attr__("click to zoom","_e"); ?>" /></a></div>				
							<?php
						}
					}
				?>
				</div>
				
		
   </div>

</div>




