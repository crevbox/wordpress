<?php
/**
 * The release template file.
 *
 */
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>


	<section class="qw-pagesection">
		<div class="container" >
			<div class="row">
				<div class="<?php $c = qw_glob_sidebar_class(); echo esc_attr($c["content"]);?>">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-lg-8">
								<h1 class="qw-page-title"><?php echo esc_attr(get_the_title()); ?></h1>
								<h4 class="qw-page-subtitle qw-knifetitle"><?php echo  esc_attr__('Out on:','_s').' '.esc_attr(get_post_meta($post->ID,'general_release_details_label',TRUE)); ?></h4>
							</div>
						</div>
					</header>
					<hr class="qw-separator">
					
					<div class="qw-page-content qw-top30" id="qwjquerycontent">

							
								<div class="row">
									<div class="col-md-7 col-lg-8" >
										

										<h4 class="qw-page-subtitle"><?php echo esc_attr__("Tracklist:","_s");?></h4>
										<hr class="qw-separator qw-separator-thin">
										<?php get_template_part( 'part', 'tracklist' ); ?>



										<h4 class="qw-page-subtitle qw-top15"><?php echo esc_attr__("Review:","_s");?>	</h4>	
										<hr class="qw-separator qw-separator-thin">
										<div class="qw-thecontent">
											<?php the_content(); ?>
										</div>	

										<?php 
										get_template_part ('inc/related_releases'); 
										?>

										<?php if ( comments_open() || '0' != get_comments_number() ){  comments_template(); } ?>

									</div>
									<div class="col-md-5 col-lg-4">
										

										<?php get_template_part( 'sharepage' ); ?>


										<?php
										$attId = get_post_thumbnail_id( $post->ID );

										if ( $attId  ){
											$img =  wp_get_attachment_image_src($attId, "full"  );
											
											echo '<a href="'.esc_url(esc_url($img[0])).'">';
											the_post_thumbnail( 'cover-thumb',array('class'=>'qw-cover-image') );
											echo '</a>';
										}
										?>

										<div class="qw-padded qw-glassbg">
											<p class="qw-categories"><?php  echo get_the_term_list( $post->ID, 'genre', '', ' ', '' );  ?></p>
										</div>


										<h4 class="qw-page-subtitle"><?php echo esc_attr__("Release:","_s");?> <?php echo esc_attr(get_post_meta($post->ID,'general_release_details_catalognumber',TRUE)); ?></h4>
										<hr class="qw-separator qw-separator-thin">
										
			                            <div class="qw-buylinks  qw-small qw-caps">
			                             	<p><strong><?php echo esc_attr__("Buy On:","_s");?> </strong>
											<?php
											$det = get_post_meta($post->ID,'general_release_details_buy_link',TRUE);
											if ( $det!='' ) {
											?>
											 <a target="_blank" href="<?php echo esc_url(esc_attr($det)); ?>" class="qw-buylink">
											 <?php 
											 $bl = get_post_meta($post->ID, "general_release_details_buy_link_text",true); 
											 echo (($bl != '')? esc_attr($bl) : esc_attr__("Online shop","_s"));
											  ?>
											</a>
											<?php } ?>  


											<?php
											$eventslinks= qp_get_group('track_repeatablebuylinks');    
											if(is_array($eventslinks)){
											  if(count($eventslinks)>0){
											    foreach($eventslinks as $e){ 
											    	if ($e === reset($eventslinks))
														 	echo ' / ';
											        if ($e['cbuylink_url']!='') {
											            echo '<a href="'.esc_attr(esc_url($e['cbuylink_url'])).'" target="_blank">'.esc_attr($e['cbuylink_anchor']).'</a>';
											        }
											        if ($e != end($eventslinks))
														 	echo ' / ';
											    }
											  }
											}
											?>
											<br>
											<?php 
				                              $det = get_post_meta($post->ID,'general_release_details_label',TRUE);
				                              if ( $det!='' ) {
				                                  ?>
				                                     <?php echo esc_attr__("Label",'_s'); ?>: <strong><?php echo esc_attr($det); ?></strong><br>
				                              <?php } ?> 
				                              <?php 
				                              $det = get_post_meta($post->ID,'general_release_details_release_date',TRUE);
				                              if ( $det!='' ) {
				                              ?>
				                                  <?php echo esc_attr(__("Release Date",'_s')); ?>: <strong><?php echo  esc_attr($det); ?></strong><br>
				                              <?php } ?>  


											</p>
										</div>

										

									

									</div>
							
								</div>

								
							
						
						
						
					</div>							
				</div>
				
				<?php get_sidebar(); ?>
				
			</div>
		</div>
	</section>

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>