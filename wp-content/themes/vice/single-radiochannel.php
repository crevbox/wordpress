<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>


	<section id="post-<?php the_ID(); ?>" <?php post_class( 'qw-pagesection' ); ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-8">
								<hr class="qw-separator">
								<h1 class="qw-page-title qw-top15"><?php echo esc_attr(get_the_title()); ?></h1>
							</div>
							<div class="col-sm-12 col-md-12 col-lg-4">
								<?php
									get_template_part( 'sharepage' );
								?>
							</div>
						</div>
					</header>
					<hr class="qw-separator qw-separator-thin">
					<div class="qw-page-content">
						<div class="row">
							
							<div class="col-md-12">
								
										
										<div class="qw-thecontent">
											<?php the_content(); ?>
										</div>	
										<?php
											wp_link_pages( array(
												'before' => '<div class="page-links">' . esc_attr__( 'Pages:', '_s' ),
												'after'  => '</div>',
											) );
										?>
						
							</div>
							
						</div>

					</div>							
				</div>
				
				<div class="col-md-3">
					<div class="panel-group qw-main-sidebar" id="mainsidebar">
						<div class="qw-colcaption-title qw-negative text-center qw-caps">
							<?php 
							/*
							*
							* Print the title of the widget sidebar, but allow translations if the title is the same as default
							*/
							$mod = get_theme_mod('QT_sidebar_caption',"Expand");
							if($mod != "Expand"){	
								echo esc_attr($mod);
							}else{
								echo esc_attr__("Expand","_s");
							}
							?>
						</div>	
						<?php dynamic_sidebar( 'right-sidebar' ); ?>
					</div>
				</div>
				
			</div>
		</div>
	</section>

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>