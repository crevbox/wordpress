<?php 
global $GlobalMonthsArray; 
global $post;
$qw_elements_quantity = get_post_meta( $post->ID, $key = 'qw_elements_quantity', $single = true );
$args  = array(
    'post_type' => 'event',
    'meta_key' => EVENT_PREFIX.'date',
    'orderby' => 'meta_value',
    'order' => 'ASC',
    'posts_per_page'=>'3'
    ,'suppress_filters' => false
);
$events = get_posts($args); 
if(count($events) > 0){ 
    for($i = 0; $i < 1; $i++){
        $page = $events[$i];
        if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), array(60,60)  ) )  {
        $thumb       = $thumb['0'];
        } 
        $e = array(
        'id' =>  $page->ID,
        'date' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'date',true)),
        'location' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'location',true)),
        'street' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'street',true)),
        'city' =>  esc_attr(get_post_meta($page->ID,EVENT_PREFIX.'city',true)),
        'permalink' =>  esc_attr(esc_url(get_permalink($page->ID))),
        'title' =>  esc_attr($page->post_title),
        'thumb' => esc_url($thumb)
        );
        $d = explode('-',$e['date']);
        $time = mktime(0, 0, 0, $d[1]);
        ?>
        <h1 class="qw-page-title"><?php echo esc_attr($e['title']); ?></h1>

        <h3 class="qw-page-subtitle qw-knifetitle"><?php echo esc_attr($e['date']).' // '.esc_attr($e['location']); ?></h3>
        <hr class="qw-separator">

    <?php 

    /*
    *
    *   Prepare coords for the map
    *
    */
    $coord = get_post_meta($page->ID,EVENT_PREFIX . 'coord', true);
    if($coord != ''){
        $coor = explode(",",get_post_meta($page->ID,EVENT_PREFIX . 'coord', true));
        //echo count($coor) ;
        if(count($coor) == 2){
            ?>
            <span class="qw-dynamicmap" data-dynamicmap="dynamicmap<?php echo esc_js(esc_attr($post->currentmodule)); ?>" data-lat="<?php echo esc_js(esc_attr($coor[0])); ?>" data-lon="<?php echo esc_js(esc_url($coor[1])); ?>"></span>
            <?php
        }
    }

    }/*for*/ 
} /* if >0 */ ?>