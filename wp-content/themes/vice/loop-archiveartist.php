<div class="row qw-gridview">



<?php
$n = 0;
while ( have_posts() ) : the_post(); $n = $n + 1;

    ?>
    	<div class="col-xs-6 col-sd-6 col-md-4 col-lg-4 qw-archive-item qw-animated">
		<div class="qw-gridview-text qw-negative">
			<h3 class="qw-gridview-title qw-caps text-center qw-negative">
				<a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a>
			</h3>
			<?php
			$nat = get_post_meta($post->ID, '_artist_nationality','Nationality');
			$res = get_post_meta($post->ID, '_artist_resident','Resident in');
			if($nat != '' || $res != ''){
				?>
					<div class="qw-separator-mini"></div>
					<p class="qw-gridview-date qw-small qw-caps  text-center">
						<?php  echo esc_attr($nat).' // '.esc_attr($res);  ?>
					</p>
				<?php 
			}	?>
		</div>
		<?php if ( has_post_thumbnail( ) ){ ?>
		<a href="<?php esc_url(the_permalink()); ?>" class="qw-gridview-imagelink qw-animated">
			<?php the_post_thumbnail( 'large',array('class'=>'img-responsive') ); ?>
			<span class="qw-gridview-overlay qw-negative qw-animated">
				<i class="qticon-caret-right qw-animated"></i>
			</span>
		</a>
		<?php } ?>
	</div>

	<?php
endwhile; // end of the loop. 
?>


</div>
