  <?php 
  /*
  *
  *   This creates the tracklist of the single releases
  *
  */

  if(has_post_thumbnail()){
	  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array("50","50") );
	  $thumbnail = $thumb['0']; 
	}



	$events= qp_get_group('track_repeatable');   
	if(is_array($events)){
	  foreach($events as $event){ 
	  $neededEvents = array('releasetrack_track_title','releasetrack_scurl','releasetrack_buyurl','releasetrack_artist_name', 'icon_type');
	  foreach($neededEvents as $n){
		  if(!array_key_exists($n,$events)){
			  $events[$n] = '';
		  }
	  }
		if(!isset($defaulttrack)){
		  $defaulttrack = $event['releasetrack_mp3_demo'];
		}
		?>
		<div class="qw-track qw-glassbg"> 
		  <?php 
		  // TRACK PLAYER FROM SOUNDCLOUD OR YOUTUBE
		  $hide_mp3_link=false;
		  if(isset($event['releasetrack_scurl'])){
			  if($event['releasetrack_scurl']!=''){
				//$hide_mp3_link=true;
				?>
				 <a href="<?php echo esc_url($event['releasetrack_scurl']); ?>"><?php echo esc_attr($event['releasetrack_track_title']); ?></a>
			   <?php  
			  }
		  } 
		  ?>
		  <?php 
		  // Single track buy link
		  $trackBuyLink='';
		  $colspan = ' colspan="2" ';

		  
		   if(isset($event['releasetrack_buyurl'])){
				if($event['releasetrack_buyurl']!=''){
					if($event['releasetrack_mp3_demo'] != ''){
						$colspan = ' colspan="3" ';
					}
					$icon = 'shopping-cart';
					$icontype_js = 'cart';
					if ( array_key_exists ( 'icon_type' , $event ) ) {
						if ( $event['icon_type'] == 'download') {
							$icon = 'download-alt';
							$icontype_js = 'download';
						}
					}
					$trackBuyLink= $event['icon_type'] .'<a href="'.esc_url($event['releasetrack_buyurl']).'" target="_blank" class="qw-track-buylink"><i class="glyphicon glyphicon-'.$icon.'"></i></a>';
				}
		   }

		   ?> 
		  <div class="qw-track-play">
			   <?php if ($hide_mp3_link == false && $event['releasetrack_mp3_demo'] != ''){ 
			   echo '<a class="playable-mp3-link" data-playtrack="'.esc_js(esc_url($event['releasetrack_mp3_demo'])).'" data-title="'.esc_js(esc_attr($event['releasetrack_track_title'])).'" data-author="'.esc_js(esc_attr($event['releasetrack_artist_name'])).'"  data-buyurl="'.esc_js(esc_url($event['releasetrack_buyurl'])).'" data-cover="'.esc_js(esc_url($thumbnail)).'" 
				data-releasepage="'.get_the_permalink($post->ID).'" data-icontype="'.esc_attr( $icontype_js ).'" href="#"><span class="qticon-caret-right"></span></a>';
				} ?>
		  </div>
		  <div class="qw-track-text">
			<p class="qw-track-name">
			<?php echo esc_attr($event['releasetrack_track_title']); ?>
			<?php echo $trackBuyLink; ?>
			</p>
			<p class="qw-artists-names qw-small qw-caps">
			<?php 
			  $aname = $event['releasetrack_artist_name'];
			  $nar=explode(',',$aname);
			  $n=0;
			  foreach($nar as $aname){
				$n++;
				$aname = trim($aname);
				$link = qw_permalink_by_name($aname);
				if($link!=''){
				 echo '<a href="'.esc_url($link).'" class="qw-artistname-title">'.esc_attr($aname).'</a>';  
				}else{
				  echo esc_attr($aname);  
				};
				if($n<count($nar))
				echo ', ';
			  }
			?>
			</p>
		  </div>
		  <div class="canc"></div>

		</div>

		

<?php }//foreach
}//end debuf if
?>
<p class="text-center qw-track qw-glassbg"><a href="#" class="btn btn-primary" data-addtoplaylyst data-releasedata="<?php echo esc_js(esc_url(add_query_arg( array('qw_get_json_data' => '1'), get_the_permalink($post->ID) )));?>"><span class="glyphicon glyphicon-plus"></span><span class="qw-caps qw-small"> <?php echo esc_attr__("Add all tracks to playlist", "_s"); ?></span></a></p>

