<?php if ( QANTUMTHEMES_WOOCOMMERCE_IS_ACTIVE ) {  ?>
	<a class="cart-contents qw-btn qt-wc_topcart"  href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'sonik' ); ?>">
		  <i class="qticon-cart"></i><?php echo WC()->cart->get_cart_total(); ?>
		  
	</a>
<?php  } ?>