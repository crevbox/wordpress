<?php
/**
 * The podcast template file.
 *
 */
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>


	<section class="qw-pagesection">
		<div class="container">
			<div class="row">
				<div class="<?php $c = qw_glob_sidebar_class(); echo esc_attr($c["content"]);?>">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-lg-10">
								<h1 class="qw-page-title"><?php echo esc_attr(get_the_title()); ?></h1>
								<h4 class="qw-page-subtitle qw-knifetitle">
									<?php
									$pDate = esc_attr(get_post_meta($post->ID,'_podcast_date',true));
			                        $date_array = explode('T',$pDate);
			                        $pDate = explode('-',$date_array[0]);
			                        global $GlobalMonthsArray;

			                        if(array_key_exists($pDate[1],$GlobalMonthsArray)){
			                       		echo esc_attr($pDate[2]).' '.esc_attr($GlobalMonthsArray[$pDate[1]]).' '.esc_attr($pDate[0]);
			                       	}
									?>
								</h4>								
							</div>
						</div>
					</header>
					
					<hr class="qw-separator">
					<div class="qw-page-content qw-top30">
						<div class="row">
							<div class="col-md-6 col-lg-7" id="qwjquerycontent">
								<div class="qw-padded qw-glassbg">
									<p class="qw-categories"><?php  echo get_the_term_list( $post->ID, 'filter', '', ' ', '' );  ?></p>
								</div>
								<div class="qw-thecontent">
									<?php 
			        				//======================= PLAYER ======================
			                        $pUrl = esc_url(esc_attr(get_post_meta($post->ID,'_podcast_resourceurl',true)));
			                        if($pUrl!=''){
			                        	$link = str_replace("https://","http://",$pUrl);
			        					?><div class="qw-spacer qw-spacer-15"></div>
	                 
	                                   <div class="qw-track qw-top15" data-autoembed="<?php echo esc_js(esc_url($link)); ?>"> 
								          <div class="qw-track-play">
								               <a class="playable-mp3-link" data-playtrack="<?php echo esc_js(esc_url(sanitizeMagicfieldsExternalUrl($link))); ?>" data-title="<?php echo  esc_js(the_title()); ?>" data-author=""   data-cover="<?php echo esc_js(esc_url(qw_get_thumbnail_url($id, $size = 'thumbnail'))); ?>" data-releasepage="<?php echo  esc_js(esc_url(get_the_permalink($post->ID))); ?>"  href="#"><span class="qticon-caret-right"></span></a>
								          </div>
								          <div class="qw-track-text">
								            <p class="qw-track-name"><?php esc_attr($page->post_title); ?></p>
								            <p class="qw-artists-names qw-small qw-caps"><?php esc_attr__("play","_s"); ?></p>
								          </div>
								          <div class="canc"></div>
								        </div>
	                            
			                        <?php } ?>
		                            <?php the_content(); ?>
		                            <?php get_template_part ('inc/related_podcast'); ?>

								</div>
							</div>
							<div class="col-md-6 col-lg-5">
								<?php
									get_template_part( 'sharepage' );
								?>
								<?php
	                            if ( has_post_thumbnail( ) ){
	                            	?><a class="swipebox qw-animated qw-imagefx" href="<?php echo esc_url(esc_attr(qw_get_thumbnail_url($id, $size = 'large'))); ?>"> <?php
	                                the_post_thumbnail( 'cover-thumb',array('class'=>'img-fullwidth') );
	                                ?></a><?php
	                            }
	                            ?>


							</div>
						</div>

						<?php _s_content_nav( 'nav-below' ); ?>
						<div class="qw-spacer"></div>
					</div>							
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>