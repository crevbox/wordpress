
/*
*
*   WELCOME
*   
*   This is the main Javascript File containing libraries and functions for running the theme
*   _____________________________________________________________________________
*
*   
*   1.7.0
*
*
*/



/* jshint unused:vars */

(function ( $ ) { 

	"use strict";

	if($.getIOSWindowHeight === "undefined"){
	  $.getIOSWindowHeight = $(window).height();
	}

	var TheAjaxControl = 0;




	jQuery.swipeboxState = 0;
	$.fn.initializeAfterAjax = function(){
		TheAjaxControl++;
		$.fn.fullHeightStuff();
		$.fn.interfaceFunctions(); // initialize all the interface functions
		$.fn.dynamicBackgrounds (); // dynamic background pictures and colors
		$.fn.initBsCarousel(); // initialize the bootstrap carousel adding extra callbacks
		$.fn.scrollableListBtns(); // initialize lists of scrollable items like in the google map
		$.fn.dynamicMaps(); // create a dynamic map with markers
		$.fn.qwJqueryContent(); // Initialize links with ajax content loaded dynamically
		$.fn.transformlinks("#content");
		$.fn.autoembed("[data-autoembed]");
		$('.Collage').collageCaption();
		$.fn.qwSimpleSlider ();
		$.fn.qwUpdateSocialData(); // new 2015 10 01 updates sharing data
		$('.prettySocial').prettySocial();
		$("#qwAjaxPreloader").fadeTo( "slow" ,0).promise().done(function(){$("#qwAjaxPreloader").css({"top":"-1000px"});});
		$("body").off("click",'[data-toggle="qw-collapse"]');
		$.fn.qwCollapsingWidgets(); // widgets
		$.fn.qwDataToggle(); // enable generic class toggle function
		if(jQuery.qtSwipeboxEnable === true){ // added by qt swipebox plugin
			jQuery('.swipebox, .gallery a, a[href*=".jpg"], a[href*="jpeg"], a[href*=".png"], a[href*=".gif"]').swipebox();
			jQuery('.Collage a').swipebox({
				beforeOpen: function() {
					jQuery.swipeboxState = 1;
				} // called before opening
			});
		}
		var todoAfterResize = [$.fn.fullHeightStuff, $.fn.collageResize, jQuery.fn.NewYoutubeResize,  $.fn.createSlimscrollNormalpage, $.fn.createSlimscrollPlaylist]; // Array of functions to execute after page resize
		$.fn.resizingTrigger(todoAfterResize); // executes all the functions that has to be done after window resize
		$.fn.collageInit();
		$.fn.collageResize();
		$.fn.qwInitAjaxPageLoad();
		$.fn.bgVideo(); // video background
		$( 'a[data-toggle="tab"]' ).on( 'shown.bs.tab', function() { jQuery.fn.NewYoutubeResize();});
		$.fn.qwAddScrollspyFunctions();
		return true; // needed to execute loader stop
	};

	$.fn.qwInitTheme = function(){
		$.fn.qwExecuteOnlyOnce();
		$.fn.fullHeightStuff();
		$.fn.bodyScroll ();
		$.fn.addGeneralPreloader(); // this creates a preloader screen effect
		$.fn.interfaceFunctions(); // initialize all the interface functions
		$.fn.qwCollapsingWidgets(); // widgets
		$.fn.initBsCarousel(); // initialize the bootstrap carousel adding extra callbacks
		$.fn.scrollableListBtns(); // initialize lists of scrollable items like in the google map
		$.fn.dynamicMaps(); // create a dynamic map with markers
		$.fn.qwJqueryContent(); // Initialize links with ajax content loaded dynamically
		$.fn.initializeSoundmanager(); // handles the player
		$.fn.bgVideo(); // video background
		$.fn.initModal ();    // Modal windows, specifically designed for the theme
		$.fn.transformlinks("#content");
		$.fn.autoembed("[data-autoembed]");
		$.fn.createSlimscrollPlaylist();
		$.fn.qwDataToggle(); // enable generic class toggle function
		$('.Collage').collageCaption();
		$.fn.qwSimpleSlider ();
		$('.prettySocial').prettySocial(); 
		$.fn.qwInitAjaxPageLoad();
		var todoAfterResize = [
			 $.fn.fullHeightStuff, 
			 $.fn.collageResize, 
			 $.fn.createSlimscrollNormalpage, 
			 $.fn.createSlimscrollPlaylist, 
			 jQuery.fn.NewYoutubeResize
			 ]; // Array of functions to execute after page resize
		$.fn.resizingTrigger(todoAfterResize); // executes all the functions that has to be done after window resize
		jQuery.fn.NewYoutubeResize();
		$.fn.dynamicBackgrounds (); // dynamic background pictures and colors
		$.fn.parallaxBackground();
		$( 'a[data-toggle="tab"]' ).on( 'shown.bs.tab', function() { jQuery.fn.NewYoutubeResize();});
		$.fn.qwAddScrollspyFunctions();
		return true; // needed to execute loader stop
	};

	$.fn.qwNormalPageSectionsResize = function(){
		if($("#fullpage").hasClass("qw-normalScrollingPage")){
			$(".fp-scrollable, .slimScrollDiv, .fullPageSection").css({"height":"auto"});
		}
	};
	$.fn.qwWindowLoad = function(){
		$.fn.collageInit();
	};
	$.fn.qwScrollTop = function(){
		$('html, body').animate({
			scrollTop: 0
		},300, 'easeOutExpo');
		return true;
	};
	$.fn.qwInitAjaxPageLoad = function(){
		if($("body").attr("data-qwajx") !== "1" || $("body").hasClass("qw-is_mobile")){
			$("#qwAjaxPreloader").remove();
			return;
		}

		$("#qwAjaxPreloader").fadeTo( "slow" ,0).promise().done(function(){$("#qwAjaxPreloader").css({"top":"-1000px"});});
		
		$("#content, #nav, #qwMusicPlaylist, #QWplayerbar, #qwFixedHeader, #qwModalContent").off("click",'a');

		$("#content, #nav, #qwMusicPlaylist, #QWplayerbar, #qwFixedHeader, #qwModalContent").not("[target='_blank']").on("click",'a', function(e) {
			var that = $(this),
				href = $(this).attr('href');
			$.qwHrefUrl = href;
			if(href !== undefined){
				if ((href.match(/^https?\:/i)) && (!href.match(document.domain)))    {
					 window.location.replace(href);
				} 
				else if(href.match(document.domain) && !that.hasClass("qwjquerycontent") && !href.match("wp-admin") && !that.hasClass("qwNoAjax") && !that.parent().hasClass("qwNoAjaxChild")&& !href.match("mailto:") && !href.match("checkout")&& !href.match(".zip") && !href.match(".jpg") && !href.match(".gif") && !href.match(".mp3") && !href.match(".png") && !href.match(".rar")&& !href.match("product") && !href.match("shop") && !href.match("cart") && !href.match("download_file=") ) 
				{
					e.preventDefault();
					if (window.history.pushState) {
						var pageurl = href;
						if (pageurl !== window.location) {
							window.history.pushState({
							path: pageurl,
							state:'new'
							}, '', pageurl);
						}
					}
					if($("#qwMusicPlaylist").hasClass("open")){
						$("#qwMusicPlaylist").removeClass("open");
					}
					$.fn.closeModal();
					if($("body").hasClass("qw-menu-open")){
						$("#nav").closest(".qwMenuScroll").removeClass("menuopen");
					}
					$( "#content" ).fadeTo( "fast" ,0, function() {
						$("#qwAjaxPreloader").css({"top":"50%"}).fadeTo( "slow" ,1);
						 $.fn.qwScrollTop();
						
					}).promise().done(function(){
						qwExecuteAjaxLink(href);
					});
				}
			}
		});

		function qwExecuteAjaxLink(link){
			 var docClass = false;
			$.ajax({
				url: link,
				success:function(data) {
					/*
					*   Retrive the contents
					*/
					$.ajaxData = data;
					$.contents = $($.ajaxData).filter("#content").html();
					$.title = $($.ajaxData).filter("title").text();
					$.qwBodyMatches = data.match(/<body.*class=["']([^"']*)["'].*>/);
					if(typeof($.qwBodyMatches) !== 'undefined'){
					   docClass = $.qwBodyMatches[1];
					}else{
						window.location.replace(link);
					}
					$.wpadminbar = $($.ajaxData).filter("#wpadminbar").html(); 
					$.langswitcher = $($.ajaxData).find("#qwLLT").html(); 
					/*
					*   Start putting the data in the page
					*/
					if(docClass !== false && $.contents !== undefined){
						$.fn.closeModal();
						$("body").attr("class",docClass);
						$("title").text($.title);
						$("#wpadminbar").html($.wpadminbar);
						$("#qwLLT").html($.langswitcher);
						$("#content").html( $.contents ).delay(300).promise().done(function(){
							if($.fn.initializeAfterAjax()){
								$("#content").find("script").each(function() {
									eval($(this).text());
								});
								$( "#content" ).fadeTo( "fast" ,1);

							}else{
								window.location.replace(link);
							}
						});   

					}else{
						window.location.replace(link);
					}
				},
				error: function () {
					//Go to the link normally
					window.location.replace(link);
				}
			});
			
		}
		// from rosspenman.com/pushstate-jquery/
		$(window).on("popstate", function(e) {
			if (e.originalEvent.state !== null) {
				var href = location.href;
				if(href !== undefined){
					if (!href.match(document.domain))    {
						window.location.replace(href);
					} else {
						if($("#qwMusicPlaylist").hasClass("open")){
							$("#qwMusicPlaylist").removeClass("open");
						}
						$.fn.closeModal();                                
						if($("body").hasClass("qw-menu-open")){
							$("#nav").closest(".qwMenuScroll").removeClass("menuopen");
						}
						$( "#content" ).fadeTo( "fast" ,0, function() {
							$("#qwAjaxPreloader").css({"top":"-1000px"}).fadeTo( "fast" ,1);
							 $.fn.qwScrollTop();
							qwExecuteAjaxLink(href);
						});
					}
				}
			}
		});
	}; // $.fn.qwInitAjaxPageLoad






	/* Execute only once (not after ajax load)
	=================================================================*/
	$.fn.activateLink = function(link){
	   // console.log("activateLink");
		$(".fullpageMenu li a").removeClass("active");
		$('[href="'+link+'"]').addClass("active");
	};

	/* Execute only once (not after ajax load)
	=================================================================*/
	$.fn.qwAddScrollspyFunctions = function(){
		$("#fullpage .fullPageSection").each(function(){
		   
			var that = $(this),
				id = that.attr("id");

			$('#'+id).scrollfire({ 
				 // Offsets
				offset: 0,
				topOffset: 70,
				bottomOffset: 70,

				// Fires once when element begins to come in from the top
				onTopIn: function() {
					// console.log("onTopIn----> "+id);
					$.fn.activateLink("#"+id);
					$(".fullPageSection").removeClass("active");
					that.addClass("active");

				},
				onBottomIn: function() {
					// console.log("onBottom");
					$.fn.activateLink("#"+id);
					$(".fullPageSection").removeClass("active");
					that.addClass("active");
				}
			}).delay(100).promise().done(function(){
				// console.log("Fix");
				$(".fullpageMenu li a.active").removeClass("active");
				$(".fullpageMenu li:first-child a").addClass("active");		
			});
		}); 
	};


	/* Execute only once (not after ajax load)
	=================================================================*/
	$.fn.qwActivateModuleOnClick = function(){
		$("body").on("click", ".fp-prev, .fp-next", function(){
			var that = $(this);
			$(".fullPageSection").removeClass("active");
			that.closest(".fullPageSection").addClass("active");
		});

	};


	/* Execute only once (not after ajax load)
	=================================================================*/
	$.fn.qwExecuteOnlyOnce = function(){
		$("body").append('<div class="qw-preloader" id="qwAjaxPreloader"><div class="circle"></div><div class="circle1"></div></div>');
		/* Adding preloader for the ajax loading */
		$("body").off("click","a[data-autotoggle]");
		$("body").on("click","a[data-autotoggle]", function(e){
			e.preventDefault();
			var that = $(this);
			$("#"+that.attr("data-target")).toggleClass(that.attr("data-autotoggle"));
			return true;
		});    
		/* Mousemove detection */
		var i = null,
		qwBody = $("body.qw-is_desktop");
		if(!qwBody.hasClass("qw-mouse-still")){
			qwBody.addClass("qw-mouse-still");
		}
		qwBody.mousemove(function() {
			if(!qwBody.hasClass("qw-mouse-moving")){
				qwBody.addClass("qw-mouse-moving");
			}
			qwBody.removeClass("qw-mouse-still");
			clearTimeout(i);
			i = setTimeout(function(){
				qwBody.removeClass("qw-mouse-moving");
				qwBody.addClass("qw-mouse-still");
			}, 4000);
		});

		$("body").off("click","a#playlistOpener");
		$("body").on("click","a#playlistOpener", function(e){
			e.preventDefault();
			$("html, body").toggleClass("playlistOpen");
			$("#qwMusicPlaylist").toggleClass("open");
		});
	};


	/* Scripts to convert colors from rgb to hex, used in the dynamic backgrounds */

	
	function hexToRgb2(hex) {
		if(hex !== null){
			var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
			hex = hex.replace(shorthandRegex, function(m, r, g, b) {
				return r + r + g + g + b + b;
			});
			var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
			if(result){
			var r = parseInt(result[1], 16),
				g = parseInt(result[2], 16),
				b = parseInt(result[3], 16);
				return r+","+g+","+b;
			}
		}
		return;
	}





	/* Menu
	=================================================================*/
	$("body").on("click","#qwtoggle > li.menu-item-has-children > a",function (e) {
		e.preventDefault();
		var that = $(this);

		if(that.hasClass("active")){
		   that.removeClass("active");
		   $('#qwtoggle > li > ul').slideUp();
		}else{

			 $("#qwtoggle .active").removeClass("active");
			if (false === that.next().is(':visible')) {
				$('#qwtoggle > li > ul').slideUp();
				$("#qwtoggle .active").removeClass("active");
			}
			that.next().slideToggle();
			
			that.addClass('active');
		}
	});
	// Slimscroll Menu
	$("body").on("click","#qwMenuToggle", function(e){
		e.preventDefault();
		var theBody = $("body"),
			theMenuScroll = $("#nav").closest(".qwMenuScroll");
		if(theBody.hasClass("qw-menu-open")){
			$("#nav").closest(".qwMenuScroll").removeClass("menuopen");
		}
		theBody.toggleClass("qw-menu-open").delay(500).promise().done(function(){
			if(theBody.hasClass("qw-menu-open")){
				theMenuScroll.addClass("menuopen");
			}
		});

		if(typeof($.fn.fullpage.setAllowScrolling) == 'function'){
			  if(theBody.hasClass("qw-menu-open")){
				$.fn.fullpage.setAllowScrolling(false);
			  }else{
				$.fn.fullpage.setAllowScrolling(true);
			  }
		}
	});

	$.fn.qtmenuToggle = function(){
		if($("body").hasClass("qw-menu-open")){
			$("#qwMenuToggle").click();
		}
		//alert("closemenu");
	}


	/* Detect when page is scrolled
	=================================================================*/
	$.fn.bodyScroll = function(){
		var timer;
		var   theBodyScroll = $("body");
		if($("body").not("page-template-page-modular")){
			$("#content").scroll(function(){
				if ( timer ) { clearTimeout(timer) }
				timer = setTimeout(function(){
					if($(window).scrollTop() > 0){
					  theBodyScroll.addClass("qw-scrolled");
					}else{
					  theBodyScroll.removeClass("qw-scrolled");
					}
				}, 600);
			});
		}
	}


	/* Video background
	=================================================================*/
	$.fn.bgVideo = function(){



		//console.log("Calling $.fn.bgVideo");
		if($("data-videourl").length === 0){
			if(typeof(player) == "object"){
			   if(typeof(player.stopVideo) == "function"){
				   $("#okplayer").fadeTo("slow",0,function(){
						player.stopVideo();
				   });
				}
			}
		}
		if(!$("body").hasClass("qw-is_mobile")){
			$("[data-videourl]").each(function(i,c){ 
				var tvolume = 0;
				if($(c).attr("data-audio") === "1"){
				  tvolume = 100;
				}
				if($(c).attr("data-videourl") != ''){


					//console.log("We have video URL");

					if($("#qwPlayerPlay").attr("data-state") == "play"){
						if(typeof(player) == "object"){
							player.stopVideo();
						}
					}
					if(typeof(player) == "object"){


						//console.log("==== CASE 1 ====== \n typeof(player) == object");

						$("#okplayer").fadeTo("slow",0,function(){
						   player.stopVideo();
						   player.clearVideo();
						   var videoIdArray = $(c).attr("data-videourl")/*.split("https").join("http")*/.split("watch?v=").join("v/").split("com/?v=").join("com/v/");
						   videoIdArray = $(c).attr("data-videourl")/*.split("https").join("http")*/.split("watch?v=").join("v/").split("com/?v=").join("com/v/");
						   videoIdArray = videoIdArray+"?version=3";
						   player.loadVideoByUrl(videoIdArray, 0, "large");
							$("#okplayer").fadeTo("slow",1, function(){
								player.playVideo();
							});
						});
						if(typeof(player.setVolume)=="function"){
							player.setVolume(tvolume);
						}
					} else{

						//console.log("==== CASE 2 ====== \n typeof(player) IS NOT  object");

						if(typeof(player)!="undefined"){
							player.setVolume(100);
						}
						//console.log("Now we do okvideo");
						var optionsVideo = { 
							source: $(c).attr("data-videourl"), 
							volume: tvolume, 
							loop: true,
							hd:true, 
							highdef:true,
							adproof: true,
							annotations: false,                      
							// onFinished: function() {  },
							// unstarted: function() {  },
							onPlay: function() { 
								$("#okplayer").delay(1000).promise().done(function(){
									$("#okplayer").fadeTo("slow",1); 
								});
							}
						 };
						$(window).data('okoptions', optionsVideo);


						$.okvideo(optionsVideo);
						$.oldVideoUrl = $(c).attr("data-videourl");
					} 
				}
			});
		} 
	};



	/* Gallery init
	=================================================================*/
	// Here we apply the actual CollagePlus plugin
	// 
	var tci = 0;
	$.fn.collageInit = function() {
		if(tci === 0){
			tci ++;
			$('.Collage').fadeTo("fast",0, function(){
				$('.Collage').removeWhitespace().collagePlus({
					'fadeSpeed'     : 0,
					'targetHeight'  : 230,
					'effect'        : $.CollageFx ,
					'direction'     : 'vertical',
					'allowPartialLastRow' : true
				}).fadeTo("fast",1);
			});
		} else{
			$("#qwAjaxPreloader").fadeTo( "fast" ,0).promise().done(function(){$("#qwAjaxPreloader").css({"top":"-1000px"});});
		}
	};
	$.fn.collageResize = function() {
		if($.swipeboxState !== 1){
			tci = 0;
			$.fn.collageInit();
		}
	}



	/* Create Fullpage Sections
	=================================================================*/

	$.fn.createFullpageSections = function(){  
		if($(".page-template-page-modular-php").length > 0){
			var FullpageColorsArray = Array();
			$.ScrollElements = {"elements":""};
			  
		
			var tooltipsArray = $("#onepageAnchors").attr("data-slideanchors").split(",");

			$('#fullpage').fullpage({
				verticalCentered: false,
				resize : false,
				sectionsColor : FullpageColorsArray,
				//anchors:['firstSlide', 'secondSlide'],
				scrollingSpeed: 1800,
				easing: 'easeInOutQuad',
				menu: '#qw-nav',
				navigation: false,
				//navigationPosition: 'right',
				navigationTooltips: tooltipsArray,
				slidesNavigation: false,
				//slidesNavPosition: 'bottom',
				loopBottom: false,
				loopTop: false,
				loopHorizontal: true,
				autoScrolling: true,
				scrollOverflow: true,
				css3: false,
				//paddingTop: '80px',
				//paddingBottom: '80px',
				normalScrollElements: '.qw-is_desktop .qwNormalSect',
				normalScrollElementTouchThreshold: 6,
				keyboardScrolling: true,
				touchSensitivity: 5,
				continuousVertical: false,
				animateAnchor: true,
				sectionSelector: '.fullPageSection',
				slideSelector: '.fullPageSlide',

				//events
				onLeave: function(index, nextIndex, direction){
					 $.fn.qtmenuToggle();
				},
				afterLoad: function(anchorLink, index){
					$.tmenu = $($('.fullPageSection').get((index-1))).attr("data-opaquemenu");
					if($($('.fullPageSection').get((index-1))).hasClass("qw_palette_light")){
						$.palette = "qw_palette_light";
						$("body").removeClass("qw_palette_dark").addClass("qw_palette_light");
					} else if ($($('.fullPageSection').get((index-1))).hasClass("qw_palette_dark")){
						$.palette = "qw_palette_dark";
						$("body").removeClass("qw_palette_light").addClass("qw_palette_dark");
					}
					if($.tmenu == "1"){
						$("body").addClass("qw-opaqueMenu");
						$("body").removeClass("qw-transparentMenu");
					}else{
						$("body").removeClass("qw-opaqueMenu");
						$("body").addClass("qw-transparentMenu");
					}


				},
				afterRender: function(){ $.fn.qwNormalPageSectionsResize(); },
				afterResize: function(){ $.fn.qwNormalPageSectionsResize(); },
				afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){ 
				   // $.fn.updateSlidesmenu(anchorLink, index, slideAnchor, slideIndex);
				},
				onSlideLeave: function(anchorLink, index, slideIndex, direction){}
			});


		}
	}

	/* Collapsing widgets 
	=================================================================*/
	$.fn.qwCollapsingWidgets = function(){
		$(".qw-panel-collapse").each(function(){
			var that 	= $(this),
				height 	= that.find(".qw-panel-contents").outerHeight();
			that.height(height);
		});
		$("body").off("click",'[data-toggle="qw-collapse"]');
		$("body").on("click",'[data-toggle="qw-collapse"]',function(e){
			e.preventDefault();
			var that 	= $(this),
				target = that.parent().parent().parent().find(".qw-panel-collapse"),
				theparent  = $("#"+that.attr('data-parent')) ;
			if(that.hasClass("open")){
				target.toggleClass("collapsed");
				that.removeClass("open");
			}else{
				theparent.find(".qw-panel-collapse").not(".collapsed").addClass("collapsed");
				theparent.find(".open").removeClass("open");
				target.toggleClass("collapsed");
				that.toggleClass("open");
			}
		});
	}

	/* dynamicBackgrounds
	=================================================================*/

	$.fn.dynamicBackgrounds = function(){

		$("[data-bgimage]").each(function(i,c){
			var that = $(this),
				bg = that.attr("data-bgimage"),
				bgattachment = that.attr("data-bgattachment");
		   
			if(bgattachment == undefined){
			   bgattachment = "static";
			}
			if(bg !== ''){
				  
				  that.css({"background-image":"url("+bg+")", "background-attachment":bgattachment,"background-size":"cover","background-position": "0"});
			}
		});
		$("[data-bgcolor]").each(function(i,c){
			var that = $(this),
				bgcolor = that.attr("data-bgcolor");
			if(bgcolor !== ""){
				if(that.attr("data-bgopacity")){
					var bgopacity = that.attr("data-bgopacity");
					var bgcolor = "rgba("+hexToRgb2(bgcolor)+","+(bgopacity/100)+")";
				}
				that.find(".fp-tableCell, .qw-fixedcontents-layer2, .qw-gbcolorlayer").css({"background-color":bgcolor});
			}
		});
	}




	/* Initialize bootstrap carousel 
	=================================================================*/

	$.fn.initBsCarousel = function(){
		$('.carousel').carousel();
		$('.carousel').on('slide.bs.carousel', function (e) {
			// 
			//      Good to have for special actions on slide
			//      ==================
			
			 var that = $(this);
			 var id = that.attr("id");
			 var index = $(e.relatedTarget).index();
			 $("[data-slide-to]").removeClass("active");
			 $("[data-slide-to=\""+index+"\"]").addClass("active");
			
		});
	}



	/* Fixed height elements
	=================================================================*/
	$.fn.fullHeightStuff = function(){
		if ( !$("body").hasClass("qw-is_mobile")){
			$(".qw-fixedcontents, .qw-fixedcontents-layer1").height( $(window).height() );
		}else{

			 $(".qw-fixedcontents, .qw-fixedcontents-layer1").css( {"height":"auto"} );
		}
	}


	/* dynamic Maps
	=================================================================*/

	$.fn.dynamicMaps = function(){
		var map, mapElement, myLatlng, mapOptions;
		if(typeof google !== 'undefined'){
			$.fn.createRichMarker();
			var googleMapsColorLight    = [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}];
			var googleMapsColorDark     = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]}];
			// Array with all the locations:
			var bounds = new google.maps.LatLngBounds ();
		  //  google.maps.event.addDomListener(window, 'load', 
			   // function () {
			var alreadyCreated = 0;

			//  ====================================
			//  Loop start (this is a map composed by list of markers)
			//  data-dynamicmap = mappa contenente più markers
			//  ====================================

			$("[data-dynamicmap]").each(function(i,c){

				// per ogni mappa
				var that = $(this),
					target = that.attr("data-dynamicmap"),
					lightness = that.attr("data-lightness");

				var mapcolor = googleMapsColorLight;
				if (lightness === "qw_palette_dark" || lightness === ""){
					var mapcolor = googleMapsColorDark;
				}

				//
				//  CREATING THE MAP
				//  ============================================


				var isDraggable = function(){
					if($("body").hasClass("is_mobile") && $("body").hasClass("is_safari") ){
						
						return false;
					}
					return true;

				}

				if(alreadyCreated === 0){
					alreadyCreated = 1;
					mapOptions = {
							zoom: 8,
							styles:mapcolor,
							panControl: false,
							scrollwheel: false,
							zoomControl: false,
							draggable: isDraggable(),
							zoomControlOptions: {
								style: google.maps.ZoomControlStyle.LARGE,
								position: google.maps.ControlPosition.LEFT_CENTER
							},
							mapTypeControl: false,
							scaleControl: true,
							streetViewControl: false,
							overviewMapControl: false 
						};
					mapElement = document.getElementById(target);
					map = new google.maps.Map(mapElement, mapOptions);
					
				}
		
				
				//that.find("[data-mapitem]").each(function(i,c){
				$(that.find('[data-mapitem]').get().reverse()).each(function(i,c){
						// Adding markers

						var mapitem = $(this);
						mapitem.lat = mapitem.attr("data-lat"),
						mapitem.lon = mapitem.attr("data-lon");

						if( mapitem.lat !== "" && mapitem.lon !== ""){
							myLatlng = new google.maps.LatLng(mapitem.lat, mapitem.lon);
							bounds.extend (myLatlng);
							
							//  MARKER // hidden
							//  ============================================
							var markerid = mapitem.attr("data-markerid"),
								mapid = mapitem.attr("data-mapid"),
								clicktarget = mapitem.attr("data-clicktarget"),
								markertitle = mapitem.attr("data-markertitle"),
								markerimg = mapitem.attr("data-markerimg"),
								markerdate = mapitem.attr("data-markerdate"),
								markerlink = '<a href="'+mapitem.attr("data-markerlink")+'" class="btn btn-small btn-primary">'+mapitem.attr("data-markeranchor")+'</a>',
								markerlocation = mapitem.attr("data-markerlocation");

							if(markerimg != ''){
								markerimg = '<div class="qw-marker-img"><img src="'+markerimg+'"></div>';
							}

							mapitem.marker = new RichMarker({
							  position: myLatlng,
							  map: map,
							  zoom: 11,
							  draggable: false,
							  content: '<div id="mapmarker'+markerid+'" class="qw-marker inactive">'+
								markerimg+
								'<div class="qw-markercontents">'+
								'<a class="qw-marker-header" data-mapct="'+markerid+'"> <i class="icon qticon-map qt-hidden-if-active"></i><span class="qt-visible-if-active">'+markertitle+'</span></a>'+
								'<div class="qw-marker-body">'+markerdate+'<br>'+markerlocation+'</div>'+
								'<div class="qw-marker-footer">'+markerlink+'</div>'+
								'</div><div class="canc"></div>'+
								'</div>'
							});

							//  CLICK
							//  ============================================
							mapitem.click(function(e){
								e.preventDefault();
								$(".qw-marker").removeClass("active").addClass("inactive");
								$("#mapmarker"+markerid).removeClass("inactive").addClass("active"); 
								
								var lat = mapitem.attr("data-lat"),
									lon = mapitem.attr("data-lon");
								myLatlng = new google.maps.LatLng(lat, lon);
								map.setCenter(myLatlng);
								map.panTo(myLatlng);
							   // map.setZoom(9);
																   
							});
						}
				}); // LOOP: data-mapitem each

				//  Map Loop end
				//  ============================================
				jQuery.locnumber = that.find("[data-mapitem]").length;
				map.fitBounds (bounds);
				jQuery.themap = map;

			}).delay(2000).promise().done(function(){
				$("a.qw-marker-header").click(function(e){
					var buttonid = $(this).attr("data-mapct");
					$("#mapCT"+buttonid).click();
				});
				if(jQuery.locnumber <= 1){
					jQuery.themap.setZoom(7);
				}
				

			}); // data DYNAMICMAP EACH
		}
	}


	/* Resizing Trigger
	=================================================================*/
	$.fn.resizingTrigger = function(functionlist){
		var counter =0;

		$(window).resize(function() {
			clearTimeout(resizeId);
			resizeId = setTimeout(doneResizing, 500);
		});
		function doneResizing(){
			if(counter >= 1){
				
				$.each(functionlist, function(i, c){
					if(typeof(c) === "function"){
						c();
					}
				});  
			}
			counter++;
		}
		var resizeId = setTimeout(doneResizing, 500);
	}

	/* Scrollable list buttons (like the map)
	=================================================================*/
	$.fn.scrollableListBtns = function(functionlist){

		$("#theBody.qw-is_desktop a[data-scrollingtarget]").click(function(e){
			e.preventDefault();
			var that = $(this),
				target = that.attr("data-scrollingtarget"),
				quantity = that.attr("data-scrollof"),
				targetContainer = $("#"+target),
				targetState = targetContainer.scrollTop(),
				scTarg = targetState - quantity;
				$("#"+target).animate({"scrollTop":scTarg},300);
		});
	}

	/* Squared images
	=================================================================*/
	$.fn.squaredImages = function(){
		$("[data-squared]").each(function(i,c){
			var that = $(this);
			that.height(that.width());
		});
	}




	/* Interface functions
	=================================================================*/

	$.fn.interfaceFunctions = function(){
	   
		$("[data-toggle=\"tooltip\"]").tooltip(); // enable tooltips
		$('.swipebox, .Collage a, .gallery a, a[href*=".jpg"], a[href*="jpeg"], a[href*=".png"], a[href*=".gif"], a[href*="zip"], a[href*="mp3"], a[href*="wav"], a[href*="mov"], a[href*="swf"]').addClass("qwNoAjax");
		$("body.is_explorer .qw-vp, body.is_explorer .qw-section-content, body.is_explorer .qw-simpleslider , body.is_explorer .qw-gbcolorlayer, body.is_explorer .qw-menutype1 #nav, body.is_explorer .qw-menutype1 #canvas").css({"height":$(window).height()+"px"});

		// Initialize fullscreen layout
		// 
		// 
		// THIS IS WHAT BREAKS THE STUFF
		if($.fn.fullpage !== undefined){
			if($.fn.fullpage.destroy !== undefined){
				if(typeof($.fn.fullpage.destroy) === 'function'){
					//$.fn.fullpage.destroy('all');
					$("html").removeClass("page-template-page-modular-html");
				}
			}
		}

		$(".qw-menutype1 #nav").slimScroll({
			height:"100%",
			width:"320px",
			allowPageScroll: true,
			size: '10px',
			position: 'right',
			color: $("#theBody").attr("data-maincolor"),
			alwaysVisible: true,
			id: 'dvScrollTest'
		});

		if($("body").hasClass("page-template-page-modular-php")){
			   
				if ( $("body").hasClass("qw-is_mobile")){
				   $("body").css({"overflow-y":"visible"});
					/*
					*
					* Smooth scrolling
					*
					*/
					$('body').off("click",'a.qwsmoothscroll');
					$('body').on("click",'a.qwsmoothscroll', function(event){     
						$('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
					});
					$.fn.qwActivateModuleOnClick();
				}else{
				 
					$("html").addClass("page-template-page-modular-html");
					if($("#content").hasClass("scrollable")){
						$('.scrollable').slimScroll({
							destroy:true
						});
						$('html, body').css({
								'overflow' : 'hidden',
								'height' : '100%'
							});
						$("#content").css({"height":"auto"});
						var $elem = $('#content'),
						events = jQuery._data( $elem[0], "events" );
						if (events) {
							jQuery._removeData( $elem[0], "events" );
						}
						$("#content").removeClass("scrollable");
					}

					$.fn.createFullpageSections();        
				  
					if($("#fullpage").hasClass("qw-normalScrollingPage")){
						$('body').off("click",'a.qwsmoothscroll');
						$('body').on("click",'a.qwsmoothscroll', function(event){     
							event.preventDefault();
							$('a.qwsmoothscroll.active').removeClass("active");
							$('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
							$(this).addClass("active");
						});
						$('html, body').css({
							'overflow' : 'visible',
							'height' : 'auto'
						});
						$.fn.qwActivateModuleOnClick();
					}   
				}
		   // 
		} else {


			var $elem = $('#content'),
			events = jQuery._data( $elem[0], "events" );
			if (events) {
				jQuery._removeData( $elem[0], "events" );
			}
			$('.scrollable').slimScroll({
				destroy:true
				});
		   // $("html, body").css({"overflow-y":"auto","overflow-x":"hidden", height:"auto"});
			$('html, body').css({
							'overflow' : 'visible',
							'height' : 'auto'
			});
			
		}
		$(".qw-menutype1 #nav").closest(".slimScrollDiv").addClass("qwMenuScroll");
		// Hide status bar in the player hover
		$("a.qw-hidesstat").removeAttr("href");
	}

	/* Detect when page with slimscroll is being scrolled
	=================================================================*/
	$.fn.createSlimscrollNormalpage = function(){
		if(!$("body").hasClass("page-template-page-modular-php")){     
			var scrollticker; // - don't need to set this in every scroll
			var qwWin = $(window);
			var qwScrollTop = qwWin.scrollTop();
			if(qwScrollTop > 0 ){
					if(!$("body").hasClass("qw-scrolled")){
						$("body").addClass("qw-scrolled");
					}
				}else{
					if($("body").hasClass("qw-scrolled")){
						$("body").removeClass("qw-scrolled");
					}
				}
			qwWin.scroll(function() {
			  if(scrollticker) { window.clearTimeout(scrollticker); scrollticker = null; }
			  scrollticker=window.setTimeout(function(){
				qwScrollTop = qwWin.scrollTop();
				if(qwScrollTop > 0 ){
					if(!$("body").hasClass("qw-scrolled")){
						$("body").addClass("qw-scrolled");
					}
				} else {
					if($("body").hasClass("qw-scrolled")){
						$("body").removeClass("qw-scrolled");
					}
				}
			  }, 250); // Timeout in msec
			});
		}
	};




	/* =================================================================


	MUSIC PLAYER


	=================================================================*/

	$.fn.createSlimscrollPlaylist = function(){

		$('.isScrollable').slimScroll({
				destroy:true
		});

		if($("body").hasClass(".admin-bar")){
				var realheight = $(window).height() - 44;
			}else{
				var realheight = $(window).height() - 22;
		}
		$("#qwMusicPlaylist > div").slimScroll({"height":realheight+"px","width":"100%"});
		$("#qwMusicPlaylist > div").addClass("isScrollable");
	}

	/* Playlist manager
	=================================================================*/
	$.fn.playlistManager = function(){
		var playlistContainer = $("#qwPlaylistTable"),
			releaseArchive = playlistContainer.attr("data-archive"),
			radioArchive = playlistContainer.attr("data-archiveradio");


		// Transform array into playlist
		//==================================
		$.fisrtAdded = "firstAdded";

		$.fn.playlistManager.addTracks = function(releaseArray, callback){
			// update 2016 03 09 we are receiving an array
			
		   	var track;
			$.each(releaseArray, function( index, value ) {
				var releaseContent = value;
			   $.qantumReleaseContent = releaseContent;
			   //return;
			   if($.qantumReleaseContent === "null" || $.qantumReleaseContent === "undefined" || !releaseContent){
					return;
			   }
				if(typeof(releaseContent) !== 'object'){
				  
				   $("#QWplayerbar").removeClass("open");
					return;
				}


				// 2016 03 09 added js radio channels support to avoid issue when no releases are added
				if(releaseContent['post_type'] === "radiochannel"){
					// only for radio station
					if(typeof(releaseContent['customMeta']['radio_subtitle']) !== "undefined"){
						var radioSubtitle = releaseContent['customMeta']['radio_subtitle'][0];
					}
					if(typeof(releaseContent['customMeta']['mp3_stream_url']) !== "undefined"){
						var mp3Url = releaseContent['customMeta']['mp3_stream_url'][0];
					}
				} else {

					var elements = releaseContent['customMeta']['track_repeatable'][0],
					label = releaseContent['customMeta']['general_release_details_label']
					
				}

				var permalink = releaseContent['permalink'],
				releaseTitle = releaseContent['post_title'],
				thumbnail = releaseContent['thumbnail'];
				





				if (thumbnail !== ''){
					var cover = thumbnail;
					thumbnail =    "<img src=\""+thumbnail+"\" class=\"img-responsive qw-playlist-thumb\">";
				}

				

				if(releaseContent['post_type'] === "radiochannel"){


					
				  
					$.mp3Url = mp3Url;
					if(typeof(mp3Url) != "undefined" && mp3Url !== ''){
						   track = '<tr class="qw-animated">\
							<td class="qw-animated">\
							<a href="#" class="'+$.fisrtAdded+'" data-releasepage="'+permalink+'" data-playtrack="'+mp3Url+'" data-title="'+releaseTitle+'" data-author="'+radioSubtitle+'" data-buyurl="" data-cover="'+cover+'"><span class="qticon-play"></span></a></td>\
							<td class="qw-animated">'+thumbnail+'</td><td class="qw-animated hidden-xs"><span class="line1">'+releaseTitle+'</span></td>\
							<td class="qw-animated"><span class="line1">'+releaseTitle+'</span><br>'+radioSubtitle+'</td>\
							<td class="text-center qw-animated hidden-xs"><a href="'+permalink+'"><span class="qticon-forward"></span></a></td>';
							track = track + '<td class="text-center qw-animated qw-buylink"></td>';   
							
							track = track + '<td class="text-center qw-animated hidden-xs" ><a href="#" data-removerow><span class="qticon-close-sign"></span></a>\
							</td></tr>';
							$.fisrtAdded = '';
							playlistContainer.prepend(track);
					}
				} else if (releaseContent['post_type'] === "release"){


					/**
					 *
					 *  Adding releases to playlist
					 *
					 *
					 * 
					 */

					var thtml = ''; // to delete
					var count = 0;
					var i;
					for (i in elements) {
						if (elements.hasOwnProperty(i)) {
							count++;
						}
					}

					
					
					
					
				  
					if(count > 0){
						track = "";
						$.each(elements, function( index, value ) {
							$.mp3Url = value.releasetrack_mp3_demo;

							if(value.icon_type === '' || undefined === value.icon_type) {
								value.icon_type = 'cart';
							}

							if(typeof($.mp3Url) != "undefined" && value.releasetrack_mp3_demo !== ''){
								   track = '<tr class="qw-animated">\
									<td class="qw-animated">\
									<a href="#" class="'+$.fisrtAdded+'" data-releasepage="'+permalink+'" data-playtrack="'+value.releasetrack_mp3_demo+'" \
									data-title="'+value.releasetrack_track_title.split("/’").join("'")+'" data-author="'+value.releasetrack_artist_name+'" \
									data-buyurl="'+value.releasetrack_buyurl+'" data-cover="'+cover+'" data-icontype="'+value.icon_type+'"><span class="qticon-play"></span></a></td>\
									<td class="qw-animated">'+thumbnail+'</td><td class="qw-animated hidden-xs"><span class="line1">'+releaseTitle+'</span><br>'+label+'</td>\
									<td class="qw-animated"><span class="line1"><a class="nomw" href="'+permalink+'">'+value.releasetrack_track_title+'</a></span><br>'+value.releasetrack_artist_name+'</td>\
									<td class="text-center qw-animated hidden-xs"><a href="'+permalink+'"><span class="qticon-forward"></span></a></td>';
									if(value.releasetrack_buyurl !==''){
										
										track = track + '<td class="text-center qw-animated hidden-xs qw-buylink"><a href="'+value.releasetrack_buyurl+'"><span class="qticon-'+value.icon_type+'"></span></a></td>';    
									}else{
										track = track + '<td class="text-center qw-animated qw-buylink"></td>';    
									}
									
									track = track + '<td class="text-center qw-animated hidden-xs" ><a href="#" data-removerow><span class="qticon-close-sign"></span></a>\
									</td></tr>';
									$.fisrtAdded = '';
									playlistContainer.append(track);
									
							}
						});
					}  // end adding releases to playlist    
				}   
			}); // End FOR each release in the array

			
		

			/*
			*
			*
			*   Playlist click play function
			*
			*/
			$("body").off("click","[data-playtrack]");
			$("body").on("click","[data-playtrack]", function(e){
				e.preventDefault();
				var that = $(this);
				
				if($.mySound !== undefined){

					if($.mySound.url !== that.attr("data-playtrack")){

						
						$.mySound.stop();


						$("#qwPlayerPlay").attr("data-state", "stop");
						$(".activeTrack").removeClass("activeTrack");
						that.closest("tr").addClass("activeTrack");
						$("#qwPlayerTitle").html("<a href=\""+that.attr("data-releasepage")+"\">"+that.attr("data-title")+"</a>");
						$("#qwPlayerAuthor").html(that.attr("data-author"));
						$("#qwPlayerCover").html("<a href=\""+that.attr("data-releasepage")+"\"><img src=\""+that.attr("data-cover")+"\"></a>");
						$("#qwPlayerPlay").attr("data-mp3",that.attr("data-playtrack"));


						if(that.attr("data-buyurl")){
							if(that.attr("data-buyurl") !== ''){
								$("#qwTrackBuy").html("<a href=\""+that.attr("data-buyurl")+"\" class=\"qw-ui-button\"><span class=\"qticon-"+that.attr("data-icontype")+"\"></span></a>").find("a").removeClass("hidden");    
							}
							
						}else{
							$("#qwTrackBuy a").addClass("hidden");
						}


						//$("#qwPlayerPlay").click();

						$.mySound.url =that.attr("data-playtrack");
						$.mySound.play();
						$("#qwPlayerPlay span").addClass("qticon-pause").removeClass("qticon-play");
						$("#qwPlayerPlay").attr("data-state", "play"); 



					}else{
						// I'm actually pressing again the play of the same sound
						if($("#qwPlayerPlay").attr("data-state") == "play"){
							//  $("#qwPlayerPlay").click();
							// click play on track that is loaded but stopped
							$.mySound.stop();
							$("#qwPlayerPlay span").addClass("qticon-play").removeClass("qticon-pause");
							$("#qwPlayerPlay").attr("data-state", "stop"); 

						}else{
							// click play on track that is loaded but playing
							$.mySound.play();
							$("#qwPlayerPlay span").addClass("qticon-pause").removeClass("qticon-play");
							$("#qwPlayerPlay").attr("data-state", "play"); 
						}
					}

				}else{

					$("#qwPlayerPlay").attr("data-state", "stop");
					$(".activeTrack").removeClass("activeTrack");
					that.closest("tr").addClass("activeTrack");
					$("#qwPlayerTitle").html("<a href=\""+that.attr("data-releasepage")+"\">"+that.attr("data-title")+"</a>");
					$("#qwPlayerAuthor").html(that.attr("data-author"));
					if(that.attr("data-cover") != ""){$("#qwPlayerCover").html("<a href=\""+that.attr("data-releasepage")+"\"><img src=\""+that.attr("data-cover")+"\"></a>");}
					$("#qwPlayerPlay").attr("data-mp3",that.attr("data-playtrack"));

					if(that.attr("data-buyurl")){
						if(that.attr("data-buyurl") !== ''){
							$("#qwTrackBuy").html("<a href=\""+that.attr("data-buyurl")+"\" class=\"qw-ui-button\"><span class=\"qticon-cart\"></span></a>").find("a").removeClass("hidden");
						}
					}else{
						$("#qwTrackBuy a").addClass("hidden");
					}

					$("#qwPlayerPlay").click();
				}            
			});
			$("[data-removerow] span").click(function(e){
				e.preventDefault();
				$(this).closest("tr").remove();
			});
			
			return true;
		}


		// Load tracks from url to playlist
		//==================================
		$.fn.playlistManager.loadPlaylist = function(url, callback, argument){
			$("#qwUpdatingPlaylist").addClass("updating").delay(200).promise().done(function(){
				var doCallback;
				$.ajax({
					url: url,
					async: true,
					cache: false,
					contentType: false,
					processData: false,
					success: function (returndata) { 



						$("#qwUpdatingPlaylist").removeClass("updating");                 
						if(doCallback = $.fn.playlistManager.addTracks($.parseJSON(returndata))){
							if(typeof(callback) === 'function'){
								callback(argument);
								
							}
							$.fn.playlistManager.loadFirst();
							return true;
						};
					}
					,
					error: function(e) {
						$("#qwUpdatingPlaylist").removeClass("updating");
					}
				});
			});
			return true;
		}

		// Load first track in the player
		//==================================
		var qtPlayed = 0;
		$("[data-videourl]").each(function(i,c){
			if($(this).attr("data-audio") == "1"){
				qtPlayed = 1; // disable player music if the video has its audio
			}
		});


		$.alreadyAddedFirst = false;
		$.fn.playlistManager.loadFirst = function(){
			if($.alreadyAddedFirst === true) {
				return;
			}
			var that = $("#qwPlaylistTable a.firstAdded"),
				track = that.attr("data-playtrack");
			if(track === undefined) {
				return;
			}
			$("#qwPlayerPlay").attr("data-state", "stop");
			$(".activeTrack").removeClass("activeTrack");
			that.closest("tr").addClass("activeTrack");
			$("#qwPlayerTitle").html("<a href=\""+that.attr("data-releasepage")+"\">"+that.attr("data-title")+"</a>");
			$("#qwPlayerAuthor").html(that.attr("data-author"));
			$("#qwPlayerCover").html("<a href=\""+that.attr("data-releasepage")+"\"><img src=\""+that.attr("data-cover")+"\"></a>");
			$("#qwPlayerPlay").attr("data-mp3",that.attr("data-playtrack"));
			if($.mySound !== undefined) {
				$.mySound.stop();
				$.mySound.url =that.attr("data-playtrack");
			}
			if(that.attr("data-buyurl")){
				if(that.attr("data-buyurl") !== ''){
					$("#qwTrackBuy").html("<a href=\""+that.attr("data-buyurl")+"\" class=\"qw-ui-button\"><span class=\"qticon-"+that.attr("data-icontype")+"\"></span></a>").find("a").removeClass("hidden");    
				}
				
			}else{
				$("#qwTrackBuy a").addClass("hidden");
			}
			if(qtPlayed == 0 && $("#QWplayerbar").attr("data-autoplay") === "1" && !$("body").hasClass("qw-is_mobile")){
				$("#qwPlayerPlay").click();
				if($.mySound !== undefined) {
					//$.mySound.play();
				}
				$("#qwPlayerPlay span").addClass("qticon-pause").removeClass("qticon-play");
				$("#qwPlayerPlay").attr("data-state", "play"); 
				qtPlayed = 1;
			}

			$.alreadyAddedFirst = true;
			return;
		}

		// Initialize the playlist
		//==================================
		var loadradios;

		if(loadradios = $.fn.playlistManager.loadPlaylist(radioArchive, $.fn.playlistManager.loadPlaylist, releaseArchive )){
		}
		

		// Attach Functions to buttons all around
		// LOAD A PLAYLIST INTO THE PLAYLIST OF THE SITE
		//==================================
		$("body").off("click", "[data-addtoplaylyst]");
		$("body").on("click", "[data-addtoplaylyst]", function(e){
			e.preventDefault();
			$.fn.playlistManager.loadPlaylist($(this).attr("data-releasedata"), false, false);
		});

		var qwPlayerLoaded = 0;
		$("#qwPlayerPlay").click(function(e){
			e.preventDefault();
			var that = $(this),
				playerIcon = that.find("span"),
				playerMp3 = that.attr("data-mp3"),
				playerBarLength = 0;


			

			$.playerState = that.attr("data-state");
			if($.playerState == "play"){
				playerIcon.addClass("qticon-play").removeClass("qticon-pause");
				that.attr("data-state", "stop"); 

				if($.mySound !== undefined){
					$.mySound.pause(); 
				}
				if(typeof(player) == "object"){
					
					if(typeof(player.setVolume)=="function"){
					  //  player.setVolume(100);
					}
				}
			}else{
				if(typeof(player) == "object"){
					if(typeof(player.setVolume)=="function"){
						player.setVolume(0);
					}
				}
				playerIcon.addClass("qticon-pause").removeClass("qticon-play");
				that.attr("data-state", "play");
				if($.mySound === undefined || $.mySound.url !== playerMp3){
						
						$.mySound = soundManager.createSound({
							id: 'currentSound',
							url: playerMp3,
							onfinish: function() {
								$("#qwPlayerNext").click();
							}
							,whileplaying : function(){
								// console.log("=====================================================");
								// console.log("Duration: "+typeof(this.duration)+this.duration);
								// console.log("position: "+typeof(this.position)+this.position);
								playerBarLength = (this.position/this.duration) * 100;
								$("#qwProgressBar").css('width', playerBarLength + '%' );
								$("#qwCursorPlayer").css('left', playerBarLength + '%' );
							} ,whileloading:  function(){
								if(this.bytesTotal != null){
									$("#qwLoadingBar").css('width', ((this.bytesLoaded/this.bytesTotal) * 100) + '%');
									qwPlayerLoaded = (this.bytesLoaded/this.bytesTotal) * 100;
								}
							}

						});
					 
				}else if($.mySound !== undefined){
					if( $.mySound.url !== playerMp3 ){

					}
				}
				/*
				*
				*
				*
				*
				*
				**      Play the sound!
				*
				*
				*
				*
				*
				*/
				$.mySound.play();   
			}
		});

		$("#qwPlayerPrev").click(function(e){
			var nowPlaying = $(".activeTrack"),
				prevPlaying = nowPlaying.prev();
			if(prevPlaying !== undefined){
				if(prevPlaying.find("a[data-playtrack]").attr("data-playtrack") !== "[Play]"){
					prevPlaying.find("a[data-playtrack]").click();
				}
			}
		});
		$("#qwPlayerNext").click(function(e){
			var nowPlaying = $(".activeTrack"),
				nextPlaying = nowPlaying.next();
			if(nextPlaying !== undefined){
				nextPlaying.find("a[data-playtrack]").click();
			}
		});

		var progressBarContainer = $("#qwPlayerBar").width(),
			thePlayerCursor = $("#qwCursorPosition"),
			relX = 0,
			qwActualCursorPlace = (relX / progressBarContainer * 100);

		$("#qwPlayerBar").mousemove(function(event){       
			relX = event.pageX - $(this).offset().left;
			qwActualCursorPlace = relX / progressBarContainer * 100;
			thePlayerCursor.css({"width":qwActualCursorPlace+"%"});
		}).mouseout(function(){
			thePlayerCursor.css({"width":"0%"});
		});
		$("#qwPlayerBar").click(function(event){
			console.log("Progress bar click");
			if($.mySound !== undefined){   
				if($.mySound.playState != 0){    
					relX = event.pageX - $(this).offset().left;
					qwActualCursorPlace = relX;
					$.mySound.setPosition($.mySound.durationEstimate/progressBarContainer*qwActualCursorPlace);
				}
			}
		});

		/* Volume controls ======================= */
		$("#qwPlayerVolume").after("<div id=\"qtVolumeControl\" class=\"qw-animated\"><div id=\"theVolCursor\" class=\"qw-animated\"></div></div>");
		
		var volBarCont = $("#qtVolumeControl").height(),
			relY = 100,
			theVolCursor = $("#theVolCursor"),
			qwActualVolume = 100,
			nextV = 100;

		$("#qtVolumeControl").click(function(e){
			e.preventDefault();
			relY = e.pageY - $(this).offset().top;
			nextV = 100 - relY;
			theVolCursor.css({"height":nextV+"%"});
			if($.mySound !== undefined){   
				if($.mySound.playState != 0){    
					$.mySound.setVolume(nextV);
					qwActualVolume = nextV;
				}
			}
			if(typeof(player) == "object"){
				if($("#qwPlayerPlay").attr("data-state") == "stop"){   
				   // player.setVolume(nextV);
					if(typeof(player.setVolume)=="function"){
						player.setVolume(nextV);
						qwActualVolume = nextV;
					}
				}
			}
			$(this).delay(800).promise().done(function(){$(this).removeClass("open");});
		});

		oldVolume = 60;
		if(typeof(qwActualVolume) !== "undefined"){
			var oldVolume = qwActualVolume;    
		}
		
		$("#qwPlayerVolumeMute").click(function(e){
			theVolCursor.css({"height":"0"});
			if(typeof(player) == "object"){
				player.setVolume(0);
				qwActualVolume = 0;
			}
			$.mySound.setVolume(0);
		});
		$("#qwPlayerVolumeUnmute").click(function(e){
			theVolCursor.css({"height":oldVolume+"%"});
			// we only reenable volume of video if music is not playing
			if(typeof(player) == "object" && $("#qwPlayerPlay").attr("data-state") == "stop"){
				player.setVolume(100);
				qwActualVolume = 100;
			}
			$.mySound.setVolume(oldVolume);
		});


	}




	/* Initialize Soundmanager
	=================================================================*/
	$("#qwPlayerBar").append("<div id=\"qwProgressBar\"></div>");
	$("#qwPlayerBar").append("<div id=\"qwLoadingBar\"></div>");
	$("#qwPlayerBar").append("<div id=\"qwDurationBar\"></div>");
	$("#qwPlayerBar").append("<div id=\"qwCursorPlayer\"></div>");
	$("#qwPlayerBar").append("<div id=\"qwCursorPosition\"></div>");

	$.fn.initializeSoundmanager = function(){
		//var flashPath = $("#qwPlayerPlay").attr("data-soundmanagerswf");
		soundManager.setup({
		  url: $("#qwPlayerPlay").attr("data-soundmanagerswf"),
		  flashVersion: 9,
		  onready: function() {       
			$.fn.playlistManager();
			// $.fn.addSingleTrackPlayFunction();
			
		  }     
		  ,ontimeout: function() {
			// Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?

		  }
		});

		soundManager.flash9Options = {
		  isMovieStar: null,      // "MovieStar" MPEG4 audio mode. Null (default) = auto detect MP4, AAC etc. based on URL. true = force on, ignore URL
		  usePeakData: true,     // enable left/right channel peak (level) data
		  useWaveformData: false, // enable sound spectrum (raw waveform data) - WARNING: May set CPUs on fire.
		  useEQData: false,       // enable sound EQ (frequency spectrum data) - WARNING: Also CPU-intensive.
		  onbufferchange: null,   // callback for "isBuffering" property change
		  ondataerror: null   // callback for waveform/eq data access error (flash playing audio in other tabs/domains)
		}
	}


	/* json qwJqueryContent
	=================================================================*/
	$.fn.qwJqueryContent = function(){
		$("body").off("click", "a.qwjquerycontent");
		$("body").on("click", "a.qwjquerycontent", function(e){
			
			e.preventDefault();
			if($.fn.preloaderVisibility !== undefined){
				$.fn.preloaderVisibility(1);
			}
			$.ajax({
				url: $(this).attr("data-qwjquerycontent"),
				//type: 'POST',
				//data: formData,
				async: true,
				cache: true,
				contentType: false,
				processData: false,
				success: function (returndata) {
					if($.fn.preloaderVisibility !== undefined){
						$.fn.preloaderVisibility(0);
					}
					var $response = $(returndata).find('#qwjquerycontent');
					if($response !== undefined){
					   $.fn.createModalContent($response.html());
					}            
				}
			});
		});
	}


	/* Create a generic preloader
	=================================================================*/
	$.fn.preloaderVisibility = function(state){
		if(state === 1){
			$("#qwPreloaderBox").addClass("active");
		}
		else {$("#qwPreloaderBox").removeClass("active");}
	}
	$.fn.addGeneralPreloader = function(){
		$("body").append(" <div id=\"qwPreloaderBox\"><div class=\"bubblingG\"><span id=\"bubblingG_1\"></span><span id=\"bubblingG_2\"></span><span id=\"bubblingG_3\"></span></div></div>");
	   
	}





	/* json createModalContent
	=================================================================*/
	$.fn.initModal = function(){
		
		$("body").append("<div id=\"qwModal\" class=\"qw-modal-overlay \"><div id=\"qwModalContent\" class=\"qw-modal-content\"></div><a id=\"qwModalClose\" ><span class=\"qticon-close\"></span></a></div>");
		$("#qwModalClose").click(function(e){
			e.preventDefault();
			$.fn.closeModal ();
		});
		 $(document).keyup(function(e) {
		  if (e.keyCode == 27) {  $.fn.closeModal (); }   // esc
		});
	}


	$.fn.createModalContent = function(content){
		$("#qwModalContent").html(content);
		$("#qwModal").addClass("open");
		
		if($("body").hasClass("page-template-page-modular-php")){
			$.fn.fullpage.setAllowScrolling(false);
		}
	/*
		$("#qwModal").slimScroll({
		  width: '100%',
		  height: '100vh'
		});
		*/



		if($.fn.fullpage !== undefined && $(".qw-normalScrollingPage").length < 1){
			if(typeof($.fn.fullpage)=='function'){
				$("#qwModal").slimScroll({
				  width: '100%',
				  height: '100vh'
				});
			}
		} else {
			$("html, body").css({"height": "100%", "overflow":"hidden"});
		}

		$.fn.transformlinks("#qwModal");
		$( 'a[data-toggle="tab"]' ).on( 'shown.bs.tab', function( evt ) { jQuery.fn.NewYoutubeResize();  });
	}

	$.fn.closeModal = function(){
		
		$("html, body").css({"height": "auto", "overflow":"initial"});
		$("#qwPreloaderBox").removeClass("active");
		if($("#qwModal").hasClass("open")){
			$("#qwModal").removeClass("open");
		
			 $('#qwModal').slimScroll({
				destroy:true
			});

			if($(".qw-normalScrollingPage").length > 0){
			   
				$("html, body").css({"height":"auto"});

				//$(".slimScrollDiv").remove();
			} else {
				if($.fn.fullpage !== undefined && $(".qw-normalScrollingPage").length < 1){
					if(typeof($.fn.fullpage)=='function'){
						$.fn.fullpage.setAllowScrolling(true);
					}
				}
			}
		}
	}

	$(document).keyup(function(e) {
	  if (e.keyCode == 27) {  $.fn.closeModal (); }   // esc
	});





	/*
	*
	*   PARALLAX BACKGROUNDS V3
	*   ============================================================================================================================================
	*
	*/




	$.fn.parallax = function(options) {
		var windowHeight = $(window).height();
		// Establish default settings
		var settings = $.extend({
			speed        : 0.15
		}, options);

	   
		// Iterate over each object in collection
		return this.each( function() {
			// Save a reference to the element
			var $this = $(this);
			var scrollTop = $(window).scrollTop();
			var offset = $this.offset().top;
			var height = $this.outerHeight();

			// Check if above or below viewport
			if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
			   // return;
			}
			var yBgPosition = Math.round((offset - scrollTop) * settings.speed);
			// Apply the Y Background Position to Set the Parallax Effect
		   
			$this.css('background-position', 'center ' + yBgPosition + 'px');


			// Set up Scroll Handler
			$(document).scroll(function(){
				scrollTop = $(window).scrollTop();
				offset = $this.offset().top;
				height = $this.outerHeight();

				// Check if above or below viewport
				if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
					return;
				}
				yBgPosition = Math.round((offset - scrollTop) * settings.speed);
				// Apply the Y Background Position to Set the Parallax Effect
				$this.css('background-position', 'center ' + yBgPosition + 'px');
			});
		});
	}

	$.fn.parallaxBackground = function(){
		$('[data-type="background"]').each(function(){
			var bgobj = jQuery(this); // assigning the object 
			var qwbgimg = bgobj.attr("data-bgimage");
			if(qwbgimg !== "" && typeof(qwbgimg) !== "undefined"){
				$(this).css({"background": "url("+qwbgimg+")","background-size":"cover","background-position":"center center","background-repeat":"no-repeat"   });
				bgobj.parallax({
					speed : 0.15
				});
			}
			
		});
	}


	/* parallax end */


	/* = media links transformation
	=================================================================================================*/

	$.fn.transformlinks = function (targetContainer) {
		jQuery(targetContainer).find("a[href*='youtube.com'],a[href*='youtu.be'],a[href*='mixcloud.com'],a[href*='soundcloud.com']").not('.qw-disableembedding').each(function() {

			var that = jQuery(this);
			var mystring = that.attr('href');

			var width = that.parent().width();
			
			if(width === 0){
				width = that.parent().parent().parent().width();
			}
			if(width === 0){
				width = that.parent().parent().parent().width();
			}
			if(width === 0){
			   
				width = that.parent().parent().parent().parent().width();
			}
			var element = that;

			//=== YOUTUBE https
			var expression = /(http|https):\/\/(\w{0,3}\.)?youtube\.\w{2,3}\/watch\?v=[\w-]{11}/gi;
			var videoUrl = mystring.match(expression);
			if (videoUrl !== null) {
				for (var count = 0; count < videoUrl.length; count++) {

					mystring = mystring.replace(videoUrl[count], embedVideo(videoUrl[count], width, (width/16*9)));
					replacethisHtml(mystring);
					
				}
			}               
			//=== SOUNDCLOUD
			var expression = /(http|https)(\:\/\/soundcloud.com\/+([a-zA-Z0-9\/\-_]*))/g;
			var videoUrl = mystring.match(expression);
			if (videoUrl !== null) {
				for (count = 0; count < videoUrl.length; count++) {
					var finalurl = videoUrl[count].replace(':', '%3A');
					var siteprotocol = window.location.protocol;
					if(siteprotocol.match(/^https?\:/i)) {
						finalurl = finalurl.replace("http:","https:");
					} else {
						finalurl = finalurl.replace("https:","http:");
					}
					jQuery.getJSON(window.location.protocol+'//soundcloud.com/oembed?maxheight=140&format=js&url=' + finalurl + '&iframe=true&callback=?', function(response) {
						replacethisHtml(response.html);
					});
				}
			}
			//=== MIXCLOUD
			var expression = /(http|https)(\:\/\/www\.mixcloud\.com\/[\w-]{0,150}\/[\w-]{0,150}\/[\w-]{0,1})/ig;
			videoUrl = mystring.match(expression);
			if (videoUrl !== null) {
				for (count = 0; count < videoUrl.length; count++) {
					mystring = mystring.replace(videoUrl[count], embedMixcloudPlayer(videoUrl[count]));
					replacethisHtml(mystring);
				}
			}
			//=== STRING REPLACE (FINAL FUNCTION)
			function replacethisHtml(mystring) {
				element.replaceWith(mystring);

			}
		});
		$.fn.NewYoutubeResize();
	}

	function embedMixcloudPlayer(content) {
		var finalurl = ((encodeURIComponent(content)));
		

		var siteprotocol = window.location.protocol;
		if(siteprotocol.match(/^https?\:/i)) {
			finalurl = finalurl.replace("http:","https:");
		} else {
			finalurl = finalurl.replace("https:","http:");
		}

		var embedcode ='<iframe data-state="0" class="mixcloudplayer" width="100%" height="80" src="//www.mixcloud.com/widget/iframe/?feed='+finalurl+'&embed_uuid=addfd1ba-1531-4f6e-9977-6ca2bd308dcc&stylecolor=&embed_type=widget_standard" frameborder="0"></iframe><div class="canc"></div>';    
		return embedcode;
	}

	function embedVideo(content, width, height) {
		height = width / 16 * 9;
		var youtubeUrl = content;
		var youtubeId = youtubeUrl.match(/=[\w-]{11}/);
		var strId = youtubeId[0].replace(/=/, '');
		var result = '<iframe width="'+width+'" height="'+height+'" src="'+window.location.protocol+'//www.youtube.com/embed/' + strId + '?html5=1" frameborder="0" class="youtube-player" allowfullscreen></iframe>';
		return result;
	}

	function embedYahooVideo(content) {
		return;
		var yahooUrl = content;
		var id = yahooUrl.match(/\d{8}/);
		var vidId = yahooUrl.match(/\d{7}/);
		var result = '<div class="embedded_video">\n';
		result += '<object width="100%">\n';
		result += '<param name="movie" value="http://d.yimg.com/static.video.yahoo.com/yep/YV_YEP.swf?ver=2.2.46" />\n';
		result += '<param name="allowFullScreen" value="true" />\n';
		result += '<param name="AllowScriptAccess" VALUE="always" />\n';
		result += '<param name="bgcolor" value="#000000" />\n';
		result += '<param name="flashVars" value="id=' + id + '&vid=' + vidId + '&lang=en-us&intl=us&embed=1&ap=9460582" />\n';
		result += '<embed src="'+window.location.protocol+'://d.yimg.com/static.video.yahoo.com/yep/YV_YEP.swf?ver=2.2.46" type="application/x-shockwave-flash"  allowFullScreen="true" AllowScriptAccess="always" bgcolor="#000000" flashVars="id=' + id + '&vid=' + vidId + '&lang=en-us&intl=us&embed=1&ap=9460582" >\n';
		result += '</embed>\n';
		result += '</object>\n';
		result += '</div>\n';
		return result;
	}

	//New youtube resize 25 jul 2014

	$.fn.NewYoutubeResize = function  (){
		jQuery("#content iframe, iframe.youtube-player").each(function(i,c){ // .youtube-player
			var QTthat = jQuery(this);
				if(QTthat.attr("src")){
					var href = QTthat.attr("src");
					if(href.match("youtube.com") || href.match("vimeo.com") || href.match("vevo.com")){
						var width = QTthat.parent().width(),
							height = QTthat.height();
						QTthat.css({"width":width});
						QTthat.height(width/16*9);
					}  
				}
		});
	}

	function replacethisHtml2(mystring) {
		element.replaceWith(mystring);
	}

	$.fn.ajaxCallSoundCloud = function(finalurl){

		jQuery.getJSON(window.location.protocol+'//soundcloud.com/oembed?maxheight=140&format=js&url=' + finalurl + '&iframe=true&callback=?', function(response) {
			replacethisHtml2(response.html);
		});
	}

	// AUtoembed div replacement (mixcloud/soundcloud)
	$.fn.autoembed = function (selector){



		$.each($(selector),function(i,c){ 
			var that = $(c);

			var mystring = that.attr('data-autoembed');
			
			var width = that.parent().width();
			
			if(width === 0){
				width = that.parent().parent().parent().width();

			}
			if(width === 0){
				width = that.parent().parent().parent().width();
			}
			if(width === 0){
			   
				width = that.parent().parent().parent().parent().width();
			}
			//   width = 300;
			
			var element = that;
			var count, expression, videoUrl;
			//=== STRING REPLACE (FINAL FUNCTION)
			


			//=== YOUTUBE https
			expression = /(http|https):\/\/(\w{0,3}\.)?youtube\.\w{2,3}\/watch\?v=[\w-]{11}/gi;
			videoUrl = mystring.match(expression);
			if (videoUrl !== null) {
				for (count = 0; count < videoUrl.length; count++) {
					mystring = mystring.replace(videoUrl[count], embedVideo(videoUrl[count], width, (width/16*9)));
					replacethisHtml2(mystring);
					
				}
			}               
			//=== SOUNDCLOUD
			expression = /(http|https)(\:\/\/soundcloud.com\/+([a-zA-Z0-9\/\-_]*))/g;
			videoUrl = mystring.match(expression);
			if (videoUrl !== null) {
				for (count = 0; count < videoUrl.length; count++) {
					var finalurl = videoUrl[count].replace(':', '%3A');
					//finalurl = finalurl.replace("https","http");
					var siteprotocol = window.location.protocol;
					if(siteprotocol.match(/^https?\:/i)) {
						finalurl = finalurl.replace("http:","https:");
					} else {
						finalurl = finalurl.replace("https:","http:");
					}
					$.fn.ajaxCallSoundCloud(finalurl);
					
				}
			}
			//=== MIXCLOUD
			expression = /http\:\/\/www\.mixcloud\.com\/[\w-]{0,150}\/[\w-]{0,150}\/[\w-]{0,1}/ig;
			videoUrl = mystring.match(expression);
			if (videoUrl !== null) {
				
				for (count = 0; count < videoUrl.length; count++) {
					mystring = mystring.replace(videoUrl[count], embedMixcloudPlayer(videoUrl[count]));
					replacethisHtml2(mystring);
				}
			}
			
		});

	}

	/* Simple Slider
	=================================================================*/
	$.fn.qwSimpleSlider = function(){
		$(".qw-simpleslider").each(function(i,c){
			var that = $(c);
			var milliseconds = that.attr("data-milliseconds");
			if(milliseconds === '' || milliseconds === undefined){
				milliseconds = 5000;
			}
			setInterval(
			 	function(){
					if($("body").hasClass("qw-mouse-still")){
						that.find(".fp-next").click();
					}
				}, milliseconds
			);
		});
	}

	/* Generic function to toggle classes on targeted elements
	=================================================================*/
	 var qwDataToggleClass = function() {
		var t = $(this);
		$("#"+t.attr("data-target")).toggleClass(t.attr("data-toggleclass"));
		if(t.attr("data-target2") !== undefined){
			$("#"+t.attr("data-target2")).toggleClass(t.attr("data-toggleclass2"));
		}
	}
	$.fn.qwDataToggle = function(){
		$("body").off("click", "[data-toggleclass]", qwDataToggleClass );
		$("body").on("click","[data-toggleclass]",qwDataToggleClass);
	}

	/* Function to update parameters of sharing function
	=================================================================*/
	$.fn.qwUpdateSocialData = function(){
		var socialLinks = $("#qwShareallC .prettySocial"),
			title = $(document).find("title").text(),
			pageurl = location.href;
		socialLinks.attr("data-url",pageurl).attr("data-description",title);
	}



		/* Document Ready 
	=================================================================*/
	$(document).ready(function() {
	   $.fn.qwInitTheme();
	});
	$( window ).load(function() {
	  $.fn.qwWindowLoad();
	});



}(jQuery));



