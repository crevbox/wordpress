<?php

require_once ('class-tgm-plugin-activation.php');



///////////////////////////////////////////////////////////////////////////////////////////
/////// Required Plugins
///////////////////////////////////////////////////////////////////////////////////////////

if(function_exists('vc_set_as_theme')){
     vc_set_as_theme();
}


if( !function_exists('qantum_required_plugins') ):
function qantum_required_plugins() {
	$plugins = array(
          array(
                    'name'                   => 'QT Support', 
                    'slug'                   => 'qt-support', 
                    'source'                 => get_template_directory_uri()  . '/plugins/qt-support.zip',
                    'required'               => false,
                    'version'                => '1.0',
                    'force_activation'  => false, 
                    'force_deactivation'=> true,
                    'external_url'           => ''
          )
          ,
	     array(
                    'name'     			=> 'QT Widgets', 
                    'slug'     			=> 'qt-widgets', 
                    'source'   			=> get_template_directory_uri()  . '/plugins/qt-widgets.zip',
                    'required' 			=> true,
                    'version' 			=> '1.0.5',
                    'force_activation' 	=> false, 
                    'force_deactivation'=> true,
                    'external_url' 		=> ''
		)
		,
		array(
                    'name'     			=> 'Revolution Slider', 
                    'slug'     			=> 'revslider', 
                    'source'   			=> get_template_directory_uri()  . '/plugins/revslider.zip',
                    'required' 			=> false,
                    'version' 			=> '5.4.5.1',
                    'force_activation' 	=> false, 
                    'force_deactivation'=> true,
                    'external_url' 		=> ''
		)
		,array(
                    'name'     			=> 'QT Preloader', 
                    'slug'     			=> 'qt-preloader', 
                    'source'   			=> get_template_directory_uri()  . '/plugins/qt-preloader.zip',
                    'required' 			=> false,
                    'version' 			=> '',
                    'force_activation' 	=> false, 
                    'force_deactivation'=> true,
                    'external_url' 		=> ''
		)  
          ,array(
                    'name'                   => 'QT Ajax RevSlider', 
                    'slug'                   => 'qt_ajax_revslider', 
                    'source'                 => get_template_directory()  . '/plugins/qt_ajax_revslider.zip',
                    'required'               => false,
                    'version'                => '2.0',
                    'force_activation'  => false, 
                    'force_deactivation'=> true,
                    'external_url'           => ''
          ) 
		,array(
                    'name'     			=> 'QT Easy Installer', 
                    'slug'     			=> 'easy_installer', 
                    'source'   			=> get_template_directory_uri()  . '/plugins/easy_installer.zip',
                    'required' 			=> false,
                    'version' 			=> '',
                    'force_activation' 	=> false, 
                    'force_deactivation'=> true,
                    'external_url' 		=> ''
		)  
		,array(
                    'name'     			=> 'QT Beatport Importer Plugin', 
                    'slug'     			=> 'qt-preloader', 
                    'source'   			=> get_template_directory_uri()  . '/plugins/qt-beatport-importer-plugin.zip',
                    'required' 			=> false,
                    'version' 			=> '',
                    'force_activation' 	=> false, 
                    'force_deactivation'=> true,
                    'external_url' 		=> ''
		)  
		,array(
                    'name'     			=> 'QT Swipebox Photo And Video', 
                    'slug'     			=> 'qt-swipebox', 
                    'source'   			=> get_template_directory_uri()  . '/plugins/qt-swipebox.zip',
                    'required' 			=> true,
                    'version' 			=> '',
                    'force_activation' 	=> false, 
                    'force_deactivation'=> true,
                    'external_url' 		=> ''
		)  
		,array(
                    'name'     			=> 'Bootstrap Shortcodes for WordPress', 
                    'required' 			=> false,
                    'slug'     			=> 'bootstrap-3-shortcodes'
		)  
          /*,array(
                    'name'                   => 'Bootstrap 3 Shortcodes', 
                    'required'               => false,
                    'slug'                   => 'bootstrap-shortcodes'
          ) */
		,array(
                    'name'     			=> 'Contact Form 7', 
                    'required' 			=> true,
                    'slug'     			=> 'contact-form-7'
		)        
	);

	$config = array(
		'domain'       		=> 'vice',         	
		'default_path' 		=> '',                         	
		'parent_menu_slug' 	=> 'themes.php', 				
		'parent_url_slug' 	=> 'themes.php', 				
		'menu'         		=> 'install-required-plugins', 	
		'has_notices'      	=> true,                       
		'is_automatic'    	=> true,
		'message' 			=> '',							
		'strings'      		=> array(
			'page_title'                       			=> __( 'Install Required Plugins', '_s' ),
			'menu_title'                       			=> __( 'Install Plugins', '_s' ),
			'installing'                       			=> __( 'Installing Plugin: %s', '_s' ), 
			'oops'                             			=> __( 'Something went wrong with the plugin API.', '_s' ),
			'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_install'  				=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 				=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 					=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_update' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
			'install_link' 					 	=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
			'activate_link' 				  	=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
			'return'                           			=> __( 'Return to Required Plugins Installer', '_s' ),
			'plugin_activated'                             => esc_attr__( 'Plugin activated successfully.', '_s' ),
               'complete'                              => esc_attr__( 'All plugins installed and activated successfully. %s', '_s' ) ,
               'nag_type'						=> 'updated'
		)
	);
	tgmpa($plugins, $config);
}
endif; // end   _s_required_plugins  
?>
