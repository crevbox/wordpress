<?php
/*
*
*   Module
*
*/
global $post; // the main object of the modular page containing this module
global $GlobalMonthsArray; 
global $tempEvent; // variables defined in the mother page
$moduleId = $tempEvent['moduleId'];
$qw_elements_quantity = esc_attr(get_post_meta( $post->currentmodule, $key = 'qw_elements_quantity', $single = true ));
$qw_lightness = $tempEvent['qwlightness'];
if($qw_lightness == ''){$qw_lightness == 'light';}
?>

<div class="qw-vp qw-gbcolorlayer">
	<div class=" qw-vc">

		<?php 
		if ($tempEvent['hide_title'] == '' || $tempEvent['hide_title'] == '0'){	?>
		<h1 class="qw-moduletitle"><?php  echo esc_attr(get_the_title( $tempEvent['moduleId'])); ?></h1>	
		<?php } ?>

		<div class="qw-podcast-carousel <?php echo esc_attr($qw_lightness); ?>">
			

			
			
				<?php
				/*
				*
				*
				*
				*	Start of the carousel
				*
				*
				*/
				?>

				<?php
				$args  = array(
				    'post_type' => 'podcast',
				    'posts_per_page'=>$qw_elements_quantity
				    ,'suppress_filters' => false
				);
				$events = get_posts($args); 
				if(count($events) > 0){ ?>
					<?php

					if(count($events) < $qw_elements_quantity){
		               $qw_elements_quantity = count($events);
		            }
			        

			        $menuArray = array(); 

					for($i = 0; $i < $qw_elements_quantity; $i++){
						$page = $events[$i];
						setup_postdata( $page );
						$permalink = esc_url(get_permalink($post->ID));
						$e = array(
			                'id' =>  $page->ID,
			                'permalink' =>  $permalink,
			                'title' =>  esc_attr($page->post_title),
			                );
						$thumb  = array();
		                if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), array(500,500)  ) )  {
		                    $thumbhtml = '<img src="'.esc_url($thumb['0']).'" class="img-responsive qw-artist-carousel-pic" alt="'.esc_attr($e['title']).'" >';
		                    $menuArray[] = array('image'=>$thumb['0'], 'title' => $e['title']);
		                }else{
		                	$thumb[0] = '';
		                }              
					?>

					<div class="fullPageSlide" id="slide<?php echo esc_attr($e['id']);?>" data-anchor="slidePodcast<?php echo esc_attr($i); ?>">
						<div class="container">
							<div class="row qw-artist-carousel-item">
								<div class="col-sm-12 col-md-10 col-md-offset-1  col-lg-10 col-lg-offset-1">
									<div class="qw-bgpanel">
										
										<div class="row qw-top30">
											<div class=" hidden-xs  col-sm-4 col-md-3">
					<a href="<?php echo esc_url(get_permalink($e['id'])); ?>">
													<?php echo '<img src="'.esc_url($thumb['0']).'" class="img-responsive qw-artist-carousel-pic" alt="'.esc_attr($e['title']).'" >'; ?>
												</a>
											</div>
											<div class="col-xs-12 col-sm-8 col-md-9 qw-thecontent">
												
												<h4 class="qw-page-subtitle qw-knifetitle qw-top0">
													<?php
													$pDate = esc_attr(get_post_meta($page->ID,'_podcast_date',true));
							                        $date_array = explode('T',$pDate);
							                        $pDate = explode('-',$date_array[0]);
							                        global $GlobalMonthsArray;

							                        if(array_key_exists($pDate[1],$GlobalMonthsArray)){
							                       		echo esc_attr($pDate[2]).' '.esc_attr($GlobalMonthsArray[$pDate[1]]).' '.esc_attr($pDate[0]);
							                       	}
													?>
												</h4>		
												<div class="qw-separator "></div>
												<h1 class="qw-page-title qw-top15"><?php echo esc_attr($e['title']);?></h1>

												<div class="qw-separator-superthin "></div>
												<?php 
						        				//======================= PLAYER ======================
						                        $pUrl = get_post_meta($page->ID,'_podcast_resourceurl',true);
						                        if($pUrl!=''){
						                        	$link = esc_url(str_replace("https://","http://",$pUrl));
						        					?>
				                 					<div class="qw-spacer qw-spacer-15">

				                 					</div>
				                                   <div class="qw-track qw-top15" data-autoembed="<?php echo esc_js(esc_url($link)); ?>"> 
											          <div class="qw-track-play">
											          		<a class="playable-mp3-link" data-playtrack="<?php echo sanitizeMagicfieldsExternalUrl(esc_js($link)); ?>" data-title="<?php echo esc_js(esc_attr($e['title'])); ?>" data-author=""   data-cover="<?php echo esc_js(esc_url($thumb[0])); ?>" data-releasepage="<?php echo  esc_js(esc_url(get_the_permalink($post->ID))); ?>"  href="#"><span class="qticon-caret-right"></span></a>
											          	</div>
											          <div class="qw-track-text">
											            <p class="qw-track-name"><?php echo esc_attr($e['title']); ?></p>
											            <p class="qw-artists-names qw-small qw-caps"><?php echo esc_attr(get_post_meta($page->ID,'_podcast_artist',true)); ?></p>
											          </div>
											          <div class="canc"></div>
											        </div>
				                            
						                        <?php } ?>
											
											   
										   </div>
										
										</div>

									</div>

								</div>
							</div>
						</div>	
					</div>	
					<?php
					} // For  $i < $qw_elements_quantity
					?>
				<?php
				} // IF
				?>
				<?php
				/*
				*
				*
				*
				*	End of the carousel
				*
				*
				*
				*/
			?>
		
			
		</div>

		<?php
		/*
		*
		*	Navigation menu
		*
		*/
		if(!my_wp_is_mobile()){
		?>
		<ul class="qw-slides-menu fp-slidesNav">
			<?php
			$active = 'active';
			foreach($menuArray as $m){
				?>
				
				<li>
				<a data-toggle="tooltip" class="qw-slidelink <?php echo esc_attr($active); ?>" title="<?php echo esc_attr($m['title']); ?>" href="#"><img src="<?php echo esc_url($m['image']); ?>" class="img-responsive" alt="<?php echo esc_attr($m['title']); ?>" ></a>
				</li>
				<?php
				$active = '';
			}
			?>
		</ul>
		<?php
		}else{
		?>


		<?php
		}
		?>


		
	</div>
</div>

	




