<?php
/**
 * The event template file.
 *
 */
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php
 $e = array(
  'id' =>  $post->ID,
  'date' =>  esc_attr(get_post_meta($post->ID,EVENT_PREFIX.'date',true)),
  'location' =>  esc_attr(get_post_meta($post->ID,EVENT_PREFIX.'location',true)),
  'street' =>  esc_attr(get_post_meta($post->ID,EVENT_PREFIX.'street',true)),
  'city' =>  esc_attr(get_post_meta($post->ID,EVENT_PREFIX.'city',true)),
  'permalink' =>  esc_url(esc_url(get_permalink($post->ID))),
  'title' =>  esc_attr($post->post_title),
  //'thumb' => $thumb
);
?>

<section class="qw-pagesection">
	<div class="container">
		<div class="row">
			<div class="<?php $c = qw_glob_sidebar_class(); echo esc_attr($c["content"]);?>">
				<header class="qw-page-header">
					<div class="row">
						<div class="col-sm-12 col-lg-8">
							<h1 class="qw-page-title"><?php echo esc_attr(get_the_title()); ?></h1>
							<h3 class="qw-page-subtitle qw-knifetitle">
								
								<?php 
								$thedate = get_post_meta($post->ID, EVENT_PREFIX.'date', true);
								if( $thedate ){
									$formatteddate = date_i18n(get_option("date_format", "d F Y"), strtotime( $thedate ));
									echo $formatteddate;
								}
 								?>
                                <?php
                                echo esc_attr__("At", "_s");
                                ?>
								<?php echo esc_attr($e['location']); ?></h3>
						</div>
					</div>
				</header>
				<hr class="qw-separator">

				<div class="qw-page-content qw-top30" id="qwjquerycontent">
					<div class="row">


						<div class="col-md-8">
							<div class="qw-padded qw-glassbg">
								<p class="qw-categories"><?php  echo get_the_term_list( $post->ID, 'eventtype', '', ' ', '' );  ?></p>
							</div>

							

                            <div class="qw-thecontent">






								<?php
								/*
								*
								*	Create event details
								*
								*/





								wp_reset_query();
								global $post;
					            $id = $post->ID;
								
								if(get_post_type( $id ) != CUSTOM_TYPE_EVENT){
									return $content;
								}
								
								$street = esc_attr(get_post_meta($id,EVENT_PREFIX . 'street',true)); 
								$city = esc_attr(get_post_meta($id,EVENT_PREFIX . 'city',true)); 
								$cap = esc_attr(get_post_meta($id,EVENT_PREFIX . 'cap',true)); 
								$website = esc_url(get_post_meta($id,EVENT_PREFIX . 'website',true)); 
								$phone = esc_attr(get_post_meta($id,EVENT_PREFIX . 'phone',true)); 
								$coord = esc_attr(get_post_meta($id,EVENT_PREFIX . 'coord',true)); 
								$location = esc_attr(get_post_meta($id,EVENT_PREFIX . 'location',true)); 
								$date = esc_attr(get_post_meta($id,EVENT_PREFIX . 'date',true)); 
								$facebooklink = esc_url(get_post_meta($id,EVENT_PREFIX . 'facebooklink',true)); 
								$fbcomments = esc_attr(get_post_meta($id,EVENT_PREFIX . 'fbcomments',true)); 
								$title = esc_attr(get_the_title($id));
								$map = '';
								
								if($city!=''){
									$map = 	'
											<div class="gmap" id="map" data-center="'.esc_js(esc_attr($street)).' '.esc_js(esc_attr($city)).'" data-zoom="15">
												<address>
												  <strong>'.esc_attr(get_the_title($id)).'</strong><br />
												  '.esc_attr($street).'<br />
												  '.esc_attr($cap).' '.esc_attr($city).'
												</address>
											  </div>';
								}

								/*
								*
								*
								*	Print the data out
								*
								*
								*/
								echo '
								<h2 class="qw-page-subtitle qw-top30">'.esc_attr__("Event Details","_s").'</h2><hr class="qw-separator">
								<table class="table eventtable">
									<tbody>
									'.(($date!='') ? '<tr><th>'.esc_attr__("Date","_s").':</th> <td>'.esc_attr($formatteddate).'</td></tr>' : '').'
									'.(($location!='') ? '<tr><th>'.esc_attr__("Location","_s").':</th> <td>'.esc_attr($location).'</td></tr>' : '').'
									'.(($street!='' || $city!='') ? '<tr><th>'.esc_attr__("Address","_s").':</th> <td> '.esc_attr($street).' -  '.esc_attr($cap).' '.esc_attr($city).'</td></tr>' : '').'
									'.(($phone!='') ? '<tr><th>'.esc_attr__("Phone","_s").':</th> <td>'.esc_attr($phone).'</td></tr>' : '').'
									'.(($website!='') ? '<tr><th>'.esc_attr__("Website","_s").':</th> <td><a href="'.esc_attr($website).'" target="_blank" rel="external nofollow">'.esc_attr($website).'</a>'.'</td></tr>' : '').'
									'.(($facebooklink!='') ? '<tr><th>'.esc_attr__("Event","_s").':</th> <td><a href="'.esc_attr($facebooklink).'" target="_blank" rel="external nofollow">'.esc_attr($facebooklink).'</a>'.'</td></tr>' : '').'
									</tbody>
								</table>';							
								

								/*
								*
								*
								*	Print the BUY links
								*
								*
								*/
								?><p class="qw-buylinks"><?php
								$eventslinks= qp_get_group(EVENT_PREFIX.'repeatablebuylinks',$id); 	 
								if(is_array($eventslinks)){
									if(count($eventslinks)>0){
										foreach($eventslinks as $e){ 
											if(isset($e['cbuylink_url'])){
												if($e['cbuylink_url']!=''){
													echo '
													<a href="'.esc_url($e['cbuylink_url']).'" class="btn btn-small qw-buy-link custombuylink">
													 <i class="icon-shopping-cart icon-white"></i>
													 '.esc_attr__("Buy on:","_s").' '.esc_attr($e['cbuylink_anchor']).'</a>
													';
												}
											}
										}
									}
								}
								?></p>


								<?php the_content(); ?>
							</div>	




						</div>

						<div class="col-md-4">
							<?php
								get_template_part( 'sharepage' );
							?>
							<?php
							$attId = get_post_thumbnail_id( $post->ID );
							if ( $attId  ){
								$img =  wp_get_attachment_image_src($attId, "full"  );
								echo '<a href="'.esc_url($img[0]).'">';
								the_post_thumbnail( 'large',array('class'=>'img-responsive') );
								echo '</a>';
							}
							?>

						</div>

						<div class="col-md-12">
							<?php
							/*
							*
							*	Print map if declared
							*
							*/
							echo (($coord!='') ? do_eventmap($coord,$title):'');
							?>
						</div>


					
					</div>
					<div class="qw-spacer"></div>
					<?php _s_content_nav( 'nav-below' ); ?>
					<div class="qw-spacer"></div>
				</div>							
			</div>
			
			<?php get_sidebar(); ?>
			
		</div>
	</div>
</section>

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>