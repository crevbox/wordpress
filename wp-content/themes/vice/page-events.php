<?php
/*
Template Name: Events Archive
*/

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }




$args = array(
	'posts_per_page' => 3,
	'post_status' => 'publish',
	'order' => 'ASC',
	'post_type' => 'event',
	'paged' => $paged,
	'orderby' => 'meta_value', 
	'meta_key' =>EVENT_PREFIX.'date'
   /* 'meta_query' , array(
    array(
        'key' => 'eventdate',
        'value' => date('Y-m-d'),
        'compare' => '>=',
        'type' => 'date'
         )
  	)*/
);

if(get_theme_mod( 'hide_past_events', '0') == '1') {
	
	
	$args['meta_query'] = array(
        array(
            'key' => 'eventdate',
            'value' => date('Y-m-d'),
            'compare' => '>=',
            'type' => 'date'
             )
      	);
}



?>
<?php get_header(); ?>
	<section class="qw-pagesection qw-archive">


		<?php  if($paged == 1 && get_post_meta(get_the_id(), 'qw_auto_slideshow', true) == 1) { ?>
			<?php 
			query_posts($args); 
			get_template_part('part','carousel' ); 
			wp_reset_query();
			?>
		<?php } ?>
		

		<div class="container">
			<div class="row">
				<div class="col-sm-11">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-8">
								<hr class="qw-separator">
								<h1 class="qw-page-title qw-top15"><?php the_title(); ?></h1>
							</div>
							<div class="col-sm-12 col-md-12 col-lg-4">
								<?php
									get_template_part( 'sharepage' );
								?>
							</div>
						</div>
					</header>
					<div class="qw-separator qw-separator-thin"></div>
					
					<div class="qw-page-content">
						<?php
						

				        if($paged == 1) {
				        	the_content();
				        }
				        $args['posts_per_page'] = 4;
						query_posts($args); 
						get_template_part( 'loop', 'archiveevent' ); 
						?>
					</div>							
				</div>
				<div class="col-md-1 col-lg-1">
					<?php page_navi(); ?>
				</div>

				
			</div>
		</div>
	</section>
<?php get_footer(); ?>