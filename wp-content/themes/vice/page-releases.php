<?php
/*
Template Name: Releases Archive
*/

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
<?php get_header(); ?>
	<section class="qw-pagesection qw-archive">

		<?php  if($paged == 1 && get_post_meta(get_the_id(), 'qw_auto_slideshow', true) == 1) { ?>
			<?php 
			query_posts('posts_per_page=3&post_status=publish&sort_order=ASC&post_type=release&paged=' . $paged); 
			get_template_part('part','carousel' ); 
			wp_reset_query();
			?>
		<?php } ?>

		<div class="container">
			<div class="row">
				<div class="col-sm-11">
					<header class="qw-page-header">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-8">
								<hr class="qw-separator">
								<h1 class="qw-page-title qw-top15"> <?php the_title(); ?> </h1>
							</div>
							<div class="col-sm-12 col-md-12 col-lg-4">
								<?php
									get_template_part( 'sharepage' );
								?>
							</div>
						</div>
					</header>
					<div class="qw-separator qw-separator-thin"></div>
					
					<div class="qw-page-content">
						<?php
					

				        if($paged == 1) {
				        	the_content();
				        }

						query_posts('posts_per_page=6&post_status=publish&sort_order=ASC&post_type=release&paged=' . $paged); 
						get_template_part( 'loop', 'archiverelease' ); 
						?>
					</div>							
				</div>
				<div class="col-md-1 col-lg-1">
					<?php page_navi(); ?>
				</div>

				
			</div>
		</div>
	</section>
<?php get_footer(); ?>