<div class="container" id="qw-iconsMarket">
      <h2 class="iconstitle">Click on the icon of your choice to use it</h2>
      <ul class="glyphs css-mapping">
        <li>
          <div class="icon qticon-amazon"></div>
          <input type="text" readonly="readonly" value="qticons-amazon">
        </li>
        <li>
          <div class="icon qticon-beatport"></div>
          <input type="text" readonly="readonly" value="qticons-beatport">
        </li>
        <li>
          <div class="icon qticon-cd"></div>
          <input type="text" readonly="readonly" value="qticons-cd">
        </li>
        <li>
          <div class="icon qticon-chevron-right"></div>
          <input type="text" readonly="readonly" value="qticons-chevron-right">
        </li>
        <li>
          <div class="icon qticon-eq"></div>
          <input type="text" readonly="readonly" value="qticons-eq">
        </li>
        <li>
          <div class="icon qticon-googleplus"></div>
          <input type="text" readonly="readonly" value="qticons-googleplus">
        </li>
        <li>
          <div class="icon qticon-man"></div>
          <input type="text" readonly="readonly" value="qticons-man">
        </li>
        <li>
          <div class="icon qticon-phone"></div>
          <input type="text" readonly="readonly" value="qticons-phone">
        </li>
        <li>
          <div class="icon qticon-resident-advisor"></div>
          <input type="text" readonly="readonly" value="qticons-resident-advisor">
        </li>
        <li>
          <div class="icon qticon-space"></div>
          <input type="text" readonly="readonly" value="qticons-space">
        </li>
        <li>
          <div class="icon qticon-whatpeopleplay"></div>
          <input type="text" readonly="readonly" value="qticons-whatpeopleplay">
        </li>
        <li>
          <div class="icon qticon-wordpress"></div>
          <input type="text" readonly="readonly" value="qticons-wordpress">
        </li>
        <li>
          <div class="icon qticon-yahoo"></div>
          <input type="text" readonly="readonly" value="qticons-yahoo">
        </li>
        <li>
          <div class="icon qticon-youtube"></div>
          <input type="text" readonly="readonly" value="qticons-youtube">
        </li>
        <li>
          <div class="icon qticon-zoom-out"></div>
          <input type="text" readonly="readonly" value="qticons-zoom-out">
        </li>
        <li>
          <div class="icon qticon-cart"></div>
          <input type="text" readonly="readonly" value="qticons-cart">
        </li>
        <li>
          <div class="icon qticon-cassette"></div>
          <input type="text" readonly="readonly" value="qticons-cassette">
        </li>
        <li>
          <div class="icon qticon-download"></div>
          <input type="text" readonly="readonly" value="qticons-download">
        </li>
        <li>
          <div class="icon qticon-event"></div>
          <input type="text" readonly="readonly" value="qticons-event">
        </li>
        <li>
          <div class="icon qticon-headphones"></div>
          <input type="text" readonly="readonly" value="qticons-headphones">
        </li>
        <li>
          <div class="icon qticon-map"></div>
          <input type="text" readonly="readonly" value="qticons-map">
        </li>
        <li>
          <div class="icon qticon-photobucket"></div>
          <input type="text" readonly="readonly" value="qticons-photobucket">
        </li>
        <li>
          <div class="icon qticon-reverbnation"></div>
          <input type="text" readonly="readonly" value="qticons-reverbnation">
        </li>
        <li>
          <div class="icon qticon-star"></div>
          <input type="text" readonly="readonly" value="qticons-star">
        </li>
        <li>
          <div class="icon qticon-stop"></div>
          <input type="text" readonly="readonly" value="qticons-stop">
        </li>
        <li>
          <div class="icon qticon-th"></div>
          <input type="text" readonly="readonly" value="qticons-th">
        </li>
        <li>
          <div class="icon qticon-th-large"></div>
          <input type="text" readonly="readonly" value="qticons-th-large">
        </li>
        <li>
          <div class="icon qticon-thunder"></div>
          <input type="text" readonly="readonly" value="qticons-thunder">
        </li>
        <li>
          <div class="icon qticon-zoom-in"></div>
          <input type="text" readonly="readonly" value="qticons-zoom-in">
        </li>
        <li>
          <div class="icon qticon-torso"></div>
          <input type="text" readonly="readonly" value="qticons-torso">
        </li>
        <li>
          <div class="icon qticon-triplevision"></div>
          <input type="text" readonly="readonly" value="qticons-triplevision">
        </li>
        <li>
          <div class="icon qticon-tumblr"></div>
          <input type="text" readonly="readonly" value="qticons-tumblr">
        </li>
        <li>
          <div class="icon qticon-twitter"></div>
          <input type="text" readonly="readonly" value="qticons-twitter">
        </li>
        <li>
          <div class="icon qticon-upload"></div>
          <input type="text" readonly="readonly" value="qticons-upload">
        </li>
        <li>
          <div class="icon qticon-vimeo"></div>
          <input type="text" readonly="readonly" value="qticons-vimeo">
        </li>
        <li>
          <div class="icon qticon-volume"></div>
          <input type="text" readonly="readonly" value="qticons-volume">
        </li>
        <li>
          <div class="icon qticon-soundcloud"></div>
          <input type="text" readonly="readonly" value="qticons-soundcloud">
        </li>
        <li>
          <div class="icon qticon-sound"></div>
          <input type="text" readonly="readonly" value="qticons-sound">
        </li>
        <li>
          <div class="icon qticon-skype"></div>
          <input type="text" readonly="readonly" value="qticons-skype">
        </li>
        <li>
          <div class="icon qticon-skip-forward"></div>
          <input type="text" readonly="readonly" value="qticons-skip-forward">
        </li>
        <li>
          <div class="icon qticon-skip-fast-forward"></div>
          <input type="text" readonly="readonly" value="qticons-skip-fast-forward">
        </li>
        <li>
          <div class="icon qticon-skip-fast-backward"></div>
          <input type="text" readonly="readonly" value="qticons-skip-fast-backward">
        </li>
        <li>
          <div class="icon qticon-skip-backward"></div>
          <input type="text" readonly="readonly" value="qticons-skip-backward">
        </li>
        <li>
          <div class="icon qticon-share"></div>
          <input type="text" readonly="readonly" value="qticons-share">
        </li>
        <li>
          <div class="icon qticon-search"></div>
          <input type="text" readonly="readonly" value="qticons-search">
        </li>
        <li>
          <div class="icon qticon-rss"></div>
          <input type="text" readonly="readonly" value="qticons-rss">
        </li>
        <li>
          <div class="icon qticon-rewind"></div>
          <input type="text" readonly="readonly" value="qticons-rewind">
        </li>
        <li>
          <div class="icon qticon-pinterest"></div>
          <input type="text" readonly="readonly" value="qticons-pinterest">
        </li>
        <li>
          <div class="icon qticon-menu"></div>
          <input type="text" readonly="readonly" value="qticons-menu">
        </li>
        <li>
          <div class="icon qticon-heart"></div>
          <input type="text" readonly="readonly" value="qticons-heart">
        </li>
        <li>
          <div class="icon qticon-exclamationmark"></div>
          <input type="text" readonly="readonly" value="qticons-exclamationmark">
        </li>
        <li>
          <div class="icon qticon-close"></div>
          <input type="text" readonly="readonly" value="qticons-close">
        </li>
        <li>
          <div class="icon qticon-chevron-up"></div>
          <input type="text" readonly="readonly" value="qticons-chevron-up">
        </li>
        <li>
          <div class="icon qticon-cd-note"></div>
          <input type="text" readonly="readonly" value="qticons-cd-note">
        </li>
        <li>
          <div class="icon qticon-bebo"></div>
          <input type="text" readonly="readonly" value="qticons-bebo">
        </li>
        <li>
          <div class="icon qticon-arrow-down"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-down">
        </li>
        <li>
          <div class="icon qticon-arrow-expand"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-expand">
        </li>
        <li>
          <div class="icon qticon-behance"></div>
          <input type="text" readonly="readonly" value="qticons-behance">
        </li>
        <li>
          <div class="icon qticon-chat-bubble"></div>
          <input type="text" readonly="readonly" value="qticons-chat-bubble">
        </li>
        <li>
          <div class="icon qticon-arrow-left"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-left">
        </li>
        <li>
          <div class="icon qticon-blogger"></div>
          <input type="text" readonly="readonly" value="qticons-blogger">
        </li>
        <li>
          <div class="icon qticon-chat-bubbles"></div>
          <input type="text" readonly="readonly" value="qticons-chat-bubbles">
        </li>
        <li>
          <div class="icon qticon-close-sign"></div>
          <input type="text" readonly="readonly" value="qticons-close-sign">
        </li>
        <li>
          <div class="icon qticon-exclamationmark-sign"></div>
          <input type="text" readonly="readonly" value="qticons-exclamationmark-sign">
        </li>
        <li>
          <div class="icon qticon-help-buoy"></div>
          <input type="text" readonly="readonly" value="qticons-help-buoy">
        </li>
        <li>
          <div class="icon qticon-minus"></div>
          <input type="text" readonly="readonly" value="qticons-minus">
        </li>
        <li>
          <div class="icon qticon-plane"></div>
          <input type="text" readonly="readonly" value="qticons-plane">
        </li>
        <li>
          <div class="icon qticon-play"></div>
          <input type="text" readonly="readonly" value="qticons-play">
        </li>
        <li>
          <div class="icon qticon-plus"></div>
          <input type="text" readonly="readonly" value="qticons-plus">
        </li>
        <li>
          <div class="icon qticon-plus-sign"></div>
          <input type="text" readonly="readonly" value="qticons-plus-sign">
        </li>
        <li>
          <div class="icon qticon-power"></div>
          <input type="text" readonly="readonly" value="qticons-power">
        </li>
        <li>
          <div class="icon qticon-questionmark"></div>
          <input type="text" readonly="readonly" value="qticons-questionmark">
        </li>
        <li>
          <div class="icon qticon-questionmark-sign"></div>
          <input type="text" readonly="readonly" value="qticons-questionmark-sign">
        </li>
        <li>
          <div class="icon qticon-quote"></div>
          <input type="text" readonly="readonly" value="qticons-quote">
        </li>
        <li>
          <div class="icon qticon-record"></div>
          <input type="text" readonly="readonly" value="qticons-record">
        </li>
        <li>
          <div class="icon qticon-replay"></div>
          <input type="text" readonly="readonly" value="qticons-replay">
        </li>
        <li>
          <div class="icon qticon-pencil"></div>
          <input type="text" readonly="readonly" value="qticons-pencil">
        </li>
        <li>
          <div class="icon qticon-magnet"></div>
          <input type="text" readonly="readonly" value="qticons-magnet">
        </li>
        <li>
          <div class="icon qticon-google"></div>
          <input type="text" readonly="readonly" value="qticons-google">
        </li>
        <li>
          <div class="icon qticon-empty"></div>
          <input type="text" readonly="readonly" value="qticons-empty">
        </li>
        <li>
          <div class="icon qticon-chevron-light-up"></div>
          <input type="text" readonly="readonly" value="qticons-chevron-light-up">
        </li>
        <li>
          <div class="icon qticon-at-sign"></div>
          <input type="text" readonly="readonly" value="qticons-at-sign">
        </li>
        <li>
          <div class="icon qticon-at"></div>
          <input type="text" readonly="readonly" value="qticons-at">
        </li>
        <li>
          <div class="icon qticon-arrow-up"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-up">
        </li>
        <li>
          <div class="icon qticon-arrow-shrink"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-shrink">
        </li>
        <li>
          <div class="icon qticon-arrow-right"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-right">
        </li>
        <li>
          <div class="icon qticon-arrow-move-up"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-move-up">
        </li>
        <li>
          <div class="icon qticon-arrow-move-right"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-move-right">
        </li>
        <li>
          <div class="icon qticon-arrow-move-left"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-move-left">
        </li>
        <li>
          <div class="icon qticon-arrow-move-down"></div>
          <input type="text" readonly="readonly" value="qticons-arrow-move-down">
        </li>
        <li>
          <div class="icon qticon-briefcase"></div>
          <input type="text" readonly="readonly" value="qticons-briefcase">
        </li>
        <li>
          <div class="icon qticon-chat-bubbles-outline"></div>
          <input type="text" readonly="readonly" value="qticons-chat-bubbles-outline">
        </li>
        <li>
          <div class="icon qticon-comment"></div>
          <input type="text" readonly="readonly" value="qticons-comment">
        </li>
        <li>
          <div class="icon qticon-facebook"></div>
          <input type="text" readonly="readonly" value="qticons-facebook">
        </li>
        <li>
          <div class="icon qticon-home"></div>
          <input type="text" readonly="readonly" value="qticons-home">
        </li>
        <li>
          <div class="icon qticon-minus-sign"></div>
          <input type="text" readonly="readonly" value="qticons-minus-sign">
        </li>
        <li>
          <div class="icon qticon-mixcloud"></div>
          <input type="text" readonly="readonly" value="qticons-mixcloud">
        </li>
        <li>
          <div class="icon qticon-music"></div>
          <input type="text" readonly="readonly" value="qticons-music">
        </li>
        <li>
          <div class="icon qticon-mute"></div>
          <input type="text" readonly="readonly" value="qticons-mute">
        </li>
        <li>
          <div class="icon qticon-navigate"></div>
          <input type="text" readonly="readonly" value="qticons-navigate">
        </li>
        <li>
          <div class="icon qticon-news"></div>
          <input type="text" readonly="readonly" value="qticons-news">
        </li>
        <li>
          <div class="icon qticon-pause"></div>
          <input type="text" readonly="readonly" value="qticons-pause">
        </li>
        <li>
          <div class="icon qticon-paypal"></div>
          <input type="text" readonly="readonly" value="qticons-paypal">
        </li>
        <li>
          <div class="icon qticon-loop"></div>
          <input type="text" readonly="readonly" value="qticons-loop">
        </li>
        <li>
          <div class="icon qticon-gear"></div>
          <input type="text" readonly="readonly" value="qticons-gear">
        </li>
        <li>
          <div class="icon qticon-eject"></div>
          <input type="text" readonly="readonly" value="qticons-eject">
        </li>
        <li>
          <div class="icon qticon-chevron-light-right"></div>
          <input type="text" readonly="readonly" value="qticons-chevron-light-right">
        </li>
        <li>
          <div class="icon qticon-caret-up"></div>
          <input type="text" readonly="readonly" value="qticons-caret-up">
        </li>
        <li>
          <div class="icon qticon-caret-right"></div>
          <input type="text" readonly="readonly" value="qticons-caret-right">
        </li>
        <li>
          <div class="icon qticon-caret-left"></div>
          <input type="text" readonly="readonly" value="qticons-caret-left">
        </li>
        <li>
          <div class="icon qticon-card"></div>
          <input type="text" readonly="readonly" value="qticons-card">
        </li>
        <li>
          <div class="icon qticon-caret-down"></div>
          <input type="text" readonly="readonly" value="qticons-caret-down">
        </li>
        <li>
          <div class="icon qticon-camera"></div>
          <input type="text" readonly="readonly" value="qticons-camera">
        </li>
        <li>
          <div class="icon qticon-checkmark"></div>
          <input type="text" readonly="readonly" value="qticons-checkmark">
        </li>
        <li>
          <div class="icon qticon-contrast"></div>
          <input type="text" readonly="readonly" value="qticons-contrast">
        </li>
        <li>
          <div class="icon qticon-fastforward"></div>
          <input type="text" readonly="readonly" value="qticons-fastforward">
        </li>
        <li>
          <div class="icon qticon-itunes"></div>
          <input type="text" readonly="readonly" value="qticons-itunes">
        </li>
        <li>
          <div class="icon qticon-juno"></div>
          <input type="text" readonly="readonly" value="qticons-juno">
        </li>
        <li>
          <div class="icon qticon-lastfm"></div>
          <input type="text" readonly="readonly" value="qticons-lastfm">
        </li>
        <li>
          <div class="icon qticon-linkedin"></div>
          <input type="text" readonly="readonly" value="qticons-linkedin">
        </li>
        <li>
          <div class="icon qticon-list"></div>
          <input type="text" readonly="readonly" value="qticons-list">
        </li>
        <li>
          <div class="icon qticon-location"></div>
          <input type="text" readonly="readonly" value="qticons-location">
        </li>
        <li>
          <div class="icon qticon-fullscreen"></div>
          <input type="text" readonly="readonly" value="qticons-fullscreen">
        </li>
        <li>
          <div class="icon qticon-earth"></div>
          <input type="text" readonly="readonly" value="qticons-earth">
        </li>
        <li>
          <div class="icon qticon-chevron-light-left"></div>
          <input type="text" readonly="readonly" value="qticons-chevron-light-left">
        </li>
        <li>
          <div class="icon qticon-chevron-light-down"></div>
          <input type="text" readonly="readonly" value="qticons-chevron-light-down">
        </li>
        <li>
          <div class="icon qticon-chevron-left"></div>
          <input type="text" readonly="readonly" value="qticons-chevron-left">
        </li>
        <li>
          <div class="icon qticon-chevron-down"></div>
          <input type="text" readonly="readonly" value="qticons-chevron-down">
        </li>
        <li>
          <div class="icon qticon-checkmark-sign"></div>
          <input type="text" readonly="readonly" value="qticons-checkmark-sign">
        </li>
        <li>
          <div class="icon qticon-deviantart"></div>
          <input type="text" readonly="readonly" value="qticons-deviantart">
        </li>
        <li>
          <div class="icon qticon-fire"></div>
          <input type="text" readonly="readonly" value="qticons-fire">
        </li>
        <li>
          <div class="icon qticon-flickr"></div>
          <input type="text" readonly="readonly" value="qticons-flickr">
        </li>
        <li>
          <div class="icon qticon-forrst"></div>
          <input type="text" readonly="readonly" value="qticons-forrst">
        </li>
        <li>
          <div class="icon qticon-forward"></div>
          <input type="text" readonly="readonly" value="qticons-forward">
        </li>
        <li>
          <div class="icon qticon-dribbble"></div>
          <input type="text" readonly="readonly" value="qticons-dribbble">
        </li>
        <li>
          <div class="icon qticon-digg"></div>
          <input type="text" readonly="readonly" value="qticons-digg">
        </li>
      </ul>
      <p>Icons created by QantumThemes. Use allowed only within this theme.</p>
    </div>