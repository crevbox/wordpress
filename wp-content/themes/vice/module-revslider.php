<?php
/*
*
*   Module
*
*/
global $tempEvent; // variables defined in the mother page
global $post;
global $wpdb;
global $table_prefix;
if (!isset($wpdb->tablename)) {
	$wpdb->revsliders_sliders = $table_prefix . 'revslider_sliders';
}
$slider_id = get_post_meta( $tempEvent['moduleId'], 'sliderid', true );
if ( $slider_alias = $wpdb->get_var( "SELECT alias FROM $wpdb->revsliders_sliders WHERE ( id = \"" . sanitize_title_for_query( $slider_id ) . "\" ) " ) ) {
	if(function_exists('putRevSlider')){
		putRevSlider($slider_alias);
	}
}