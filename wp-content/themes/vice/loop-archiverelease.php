<?php global $GlobalMonthsArray; ?>
<div class="row qw-gridview">
<?php $n = 0; ?>
<?php while ( have_posts() ) : the_post(); $n = $n + 1; ?>
	<div class="col-xs-6 col-sd-6 col-md-4 col-lg-4 qw-archive-item qw-animated">
		<div class="qw-gridview-text qw-negative">
			<h3 class="qw-gridview-title qw-caps text-center qw-negative qw-ellipsis">
				<a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a>
			</h3>
				<?php
				$pDate = get_post_meta($post->ID,'general_release_details_release_date',TRUE);
	            $date_array = explode('T',$pDate);
	            $pDate = explode('-',$date_array[0]);
	            ?><div class="qw-separator-mini"></div>
	            <p class="qw-gridview-date qw-small qw-caps  text-center">&nbsp;
	            <?php  
	            if(array_key_exists(1, $pDate)){
		            if(array_key_exists($pDate[1],$GlobalMonthsArray)){
		            	?>
		            	
		            		
		            	<?php
	           		echo esc_attr($pDate[2]).' '.esc_attr($GlobalMonthsArray[$pDate[1]]).' '.esc_attr($pDate[0]);
		           		

		           	}
		        }
				?>&nbsp;
				</p>
		</div>
		<?php if ( has_post_thumbnail( ) ){ ?>
		<a href="<?php esc_url(the_permalink()); ?>" class="qw-gridview-imagelink qw-animated">
			<?php the_post_thumbnail( "gridthumb" ,array('class'=>'img-responsive') ); ?>
			<span class="qw-gridview-overlay qw-negative qw-animated">
				<i class="qticon-caret-right qw-animated"></i>
			</span>
		</a>
		<?php } ?>
	</div>
	<?php /* if($n % 3 == 0){ ?><div class="canc hidden-md"></div><?php } */ ?>
<?php endwhile; // end of the loop. ?>
</div>
