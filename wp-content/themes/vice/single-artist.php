<?php
/**
 * The artist template file.
 *
 */
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php
/*
*
*	Get all necessary variables
*
*/
$dets = array(get_post_meta($post->ID,'_artist_booking_contact_name', true),
get_post_meta($post->ID,'_artist_booking_contact_phone', true),
get_post_meta($post->ID,'_artist_booking_contact_email', true));

$haveBookings = $dets[1].$dets[2].$dets[0];
$artist_title = esc_attr(get_the_title());

// NEW: CHOOSE CUSTOM TAB FIRST
$cu_ta_fst = get_post_meta($post->ID,'custom_tab_order', true);

// NEW: ADD CUSTOM TAB CONTENT
$custom_tab_title = get_post_meta($post->ID,'custom_tab_title', true);
$custom_tab_content = get_post_meta($post->ID,'custom_tab_content', true);






?>



<section class="qw-pagesection">

	<div class="container">

		<div class="row">
			<div class="<?php $c = qw_glob_sidebar_class(); echo esc_attr($c["content"]);?>">
				<header class="qw-page-header"  >
					<div class="row">
						<div class="col-sm-12 col-lg-8">
							<h1 class="qw-page-title"><?php echo esc_attr(get_the_title()); ?></h1>
							
							<?php
							/*
							*
							*	Artist nationality
							*
							*/
							$nat = get_post_meta($post->ID, '_artist_nationality', true);
							$res = get_post_meta($post->ID, '_artist_resident', true);
							if($nat != '' || $res != ''){
							?>
							<h4 class="qw-page-subtitle qw-knifetitle">
								<?php 
								echo esc_attr($nat)
								.' // '.esc_attr($res); 
								?>
							</h4>
							<?php }	?>
							
						</div>
					</div>
				</header>
				<hr class="qw-separator">
				<div class="qw-page-content qw-top30" >
					<div class="row">
						<div class="col-md-3">
							<?php get_template_part( 'sharepage' );?>
						</div>
						<div class="col-md-9" id="qwjquerycontent">
							<div class="row">
								<div class="col-md-9" >
									<div class="qw-padded qw-glassbg">
										<p class="qw-categories"><?php  echo get_the_term_list( $post->ID, 'artistgenre', '', ' ', '' );  ?></p>
									</div>
						
									<?php
									/*
									*
									*	================================= BEGINNING OF ARTIST DETAILS TABS =================================
									*
									*/
									?>			

									
									<div class="tab-content">

					                    <div class="tab-pane fade <?php if($cu_ta_fst =="bio" || $cu_ta_fst == ''){ ?> in active <?php } ?>" id="bio">
					                            <h3 class="qw-page-subtitle qw-knifetitle"><?php echo __("Biography",'_s'); ?></h3>


					                            <hr class="qw-separator">
					                            <div class="qw-thecontent">
					                            <?php
					                            if ( has_post_thumbnail( ) ){
					                                the_post_thumbnail( 'full',array('class'=>'img-responsive') );
					                            }
					                            ?>

					                             <?php
					                            // ===================== social link icons ==============================================
							   
												$socialicons = social_link_icons($post->ID);							
												if($socialicons != '') {
													?>
													<div class="qw-padded qw-glassbg">
													<ul class="qw-social text-center iconmed">
													<?php
													echo $socialicons;
													?>
													</ul>
													</div>
											   <?php 	} 
											   // Social Links End
											   ?>


					                            <?php the_content(); ?>
					                            </div> 
					                    </div>

					                   
					                    <div class="tab-pane fade <?php if($cu_ta_fst =="mus" ){ ?> in active <?php } ?>" id="music">
					                         <h3 class="qw-page-subtitle qw-knifetitle"><?php echo esc_attr__("Music",'_s'); ?></h3>
					                         <hr class="qw-separator">
					                        <ul class="qw-artist-release-list">
				                        <?php 
											$detTracks = '';
											$query = new WP_Query();
											$query->query( array(
												'post_type' => 'release'
												,'posts_per_page' => 200,
												'posts_status' => 'publish',
												'meta_query' => array(
													
													
													// 'relation'  => 'AND',
													
													/*array(
														'key' => 'track_repeatable',
														'value' => sprintf('%s\";s:24:', $artist_title),
														'compare' => 'LIKE'
													),*/

													
													array(
														'key' => 'track_repeatable',
														'value' => $artist_title,
														'compare' => 'LIKE'
													)

												)
											   )
											 );

											


											if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); 

												

												global $post;
												$we_have_to_exclude_this = 0; // used if a artist has the name of a track title
												setup_postdata( $post );

												$events= qp_get_group('track_repeatable'); 

												if(is_array($events)){
													foreach($events as $event) {
														if(array_key_exists('releasetrack_track_title', $event)) {
															if($event['releasetrack_track_title'] == $artist_title || strpos( $event['releasetrack_track_title'], $artist_title ) !== false) {
																// $we_have_to_exclude_this = 1;
															}
														}
													}
												}

												if($we_have_to_exclude_this == 0) {

													$detTracks = 1;


													echo '<li><a href="' . esc_url(get_permalink( $post->ID )) . '" class="qw-author-release qw-animated qw-darkbg">';
													if ( $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), array(
														 '60',
														'60' 
													) ) ) {

														$url       = $thumb[ '0' ];
														echo '<img class="qw-artist-release-img" src="' . esc_url(esc_attr($url)) . '" alt="' . esc_attr($query->post->title) . '" />';
													} 
													the_title();
													echo '</a></li>';

												}
											
											endwhile; endif; 
											wp_reset_postdata();
											wp_reset_query();
				                         ?>
					                        </ul>
					                    </div>
					                   


					                    <?php 

					                    $videos = array(get_post_meta($post->ID,'_artist_youtubevideo1', true),
					                                    get_post_meta($post->ID,'_artist_youtubevideo2', true),
					                                    get_post_meta($post->ID,'_artist_youtubevideo3', true));
					                    $detVideo = $videos[1].$videos[2].$videos[0];
					                    if($detVideo!=''){   ?>
					                    <div class="tab-pane fade <?php if($cu_ta_fst =="vid" ){ ?> in active <?php } ?>" id="videos">
					                        <h3 class="qw-page-subtitle qw-knifetitle"><?php echo esc_attr__("Videos",'_s'); ?></h3>
					                        <hr class="qw-separator">
					                        <?php 
					                        foreach($videos as $v){
					                        	if($v != ''){
					                        		echo '<a href="'.esc_url($v).'">'.esc_url($v).'</a>';
					                        	}
					                        }
					                        ?>
					                    </div>
					                    <?php } ?>

					                     <?php 
					                     	
					                     	if($haveBookings != ''){ 
					                     	?>
					                         <div class="tab-pane fade qw-booking-details <?php if($cu_ta_fst =="boo" ){ ?> in active <?php } ?>" id="info">                                   
					                                <h3 class="qw-page-subtitle qw-knifetitle"><?php echo esc_attr__("Booking",'_s'); ?></h3>
					                                <hr class="qw-separator">
					                                <?php  if($dets[0]!=''){echo '<p class="qw-top30"><span class="qw-detTit qw-negative">'.esc_attr__('Agency','_s').'</span>
					                               	 	<span class="qw-detail-value">'.esc_attr($dets[0]).'</span></p>';} ?>
					                                <?php  if($dets[1]!=''){echo '<p><span class="qw-detTit qw-negative">'.esc_attr__('Phone','_s').'</span>
					                            		<span class="qw-detail-value">'.esc_attr($dets[1]).'</span></p>';} ?>
					                                <?php  if($dets[2]!=''){echo '<p><span class="qw-detTit qw-negative">'.esc_attr__('Email','_s').'</span>
					                            		<span class="qw-detail-value">'.esc_attr($dets[2]).'</span></p>';} ?>
					                          </div>
					                      <?php } ?>



					                       <?php 
					                     	
					                     	if($custom_tab_title != ''){ 
					                     	?>

							                    <div class="tab-pane fade <?php if($cu_ta_fst =="cus" ){ ?> in active <?php } ?>" id="extra">
							                            <h3 class="qw-page-subtitle qw-knifetitle"><?php echo esc_attr($custom_tab_title); ?></h3>
							                            <hr class="qw-separator">
							                            <div class="qw-thecontent">
							                            <?php echo apply_filters('the_content', $custom_tab_content); ?>
							                            </div> 
							                    </div>
					                      <?php } ?>




									</div>

									<?php
									/*
									*
									*	================================= END OF ARTIST DETAILS TABS =================================
									*
									*/
									?>
								</div>	
								<div class="col-md-3">
									<?php
									/*
									*
									*	Tab links
									*
									*/
									?>
									<ul class="qw-vertical-tab-menu">
									    <li <?php if($cu_ta_fst =="bio" || $cu_ta_fst == ''){ ?>class="active"<?php } ?>><a href="#bio" data-toggle="tab" class="animated"><?php echo esc_attr__("Biography",'_s'); ?></a></li>
									    <?php if($detTracks!=''){  ?><li <?php if($cu_ta_fst =="mus"){ ?>class="active"<?php } ?>><a href="#music" class="animated" data-toggle="tab"><?php echo esc_attr__("Music",'_s'); ?></a></li><?php  } ?>
									    <?php if($haveBookings != ''){ ?> <li <?php if($cu_ta_fst =="boo"){ ?>class="active"<?php } ?>><a href="#info" class="animated" data-toggle="tab"><?php echo esc_attr__("Booking",'_s'); ?></a></li><?php  } ?>
									    <?php if($detVideo!=''){  ?><li <?php if($cu_ta_fst =="vid"){ ?>class="active"<?php } ?>><a href="#videos" class="animated" data-toggle="tab"><?php echo esc_attr__("Videos",'_s'); ?></a></li><?php  } ?>
					                    <?php if($custom_tab_title!=''){  ?><li <?php if($cu_ta_fst =="cus"){ ?>class="active"<?php } ?>><a href="#extra" class="animated" data-toggle="tab"><?php echo esc_attr($custom_tab_title); ?></a></li><?php  } ?>
									</ul>
								</div>
								<div class="col-md-9">
								<?php get_template_part ('inc/related_artist'); ?>
								</div>
							</div>
								
						</div>
						
					</div>
				</div>							
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>