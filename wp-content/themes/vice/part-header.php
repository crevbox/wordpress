<div class="qw-header-fixed qw-animated" id="qwFixedHeader">
	<a href="#" class="menu-qwicon" id="qwMenuToggle">
		  <span class="menu-global menu-top"></span>
		  <span class="menu-global menu-middle"></span>
		  <span class="menu-global menu-bottom"></span>
	</a>
	<?php qt_showlogo(); ?>
    <?php
        global $optarray;
        $headeroptions = $optarray['QT_headerfunctions'];
        switch($headeroptions){
            case '0':
            case '':
            default:
                get_template_part( 'sharepage','top');
                break;
            case '1':
                get_template_part( 'sharepage','links-top');
                break;
            case '2':
                get_template_part( 'header','languageswitcher');
                break;
            case '3':
                get_template_part ('header', 'cart');
                
        }
    ?>
</div>
 <div id="nav">
    <div class="qw-scroller">        
        <div class="qw-menucontainer">
            <?php echo qt_showlogo_big(); ?>
            <?php
            if ( has_nav_menu( 'primary' ) ) {
                wp_nav_menu( array(
                'theme_location' => 'primary',
                'depth' => 2,
                'container' => false,
                'items_wrap' => '<ul id="qwtoggle" class="qw-sidebarmenu toggle">%3$s</ul>'
                ) );
            }
            ?> 
            <div class="qw-footertext">
                <?php  echo get_theme_mod('QT_footer_text',"Copyright 2015"); ?>
                <?php do_action('wpml_add_language_selector'); ?>
            </div>
            <div class="qw-socialicons">
                <?php get_template_part( 'part','social'); ?>
            </div>
        </div>
    </div>
</div>	