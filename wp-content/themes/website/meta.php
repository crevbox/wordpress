<?php
/**
 * @package    WordPress
 * @subpackage Website
 */

// Option path
$option_path = 'meta'.(get_post_type() == 'post' ? (is_singular() ? '/single' : '/list') : '');
$theme_option_path = get_post_type().'/'.$option_path;
$post_option_path  = 'options/'.str_replace('/', '_', $option_path);

// Meta visibility
$visible = Website::io($post_option_path, $theme_option_path.'/visible');

// Meta
if ($visible === true || $visible === 'on') {

	$meta_items = Website::to($theme_option_path.'/items');

	if (!empty($meta_items)) {

		echo '<ul class="meta">';

		foreach ($meta_items as $meta_item) {

			switch ($meta_item) {
				case 'comments':
					if (Website::to(get_post_type().'/comments') && (comments_open() || have_comments())) {
						echo Website::getMetaTemplate('<li class="comments"><a href=":comments_link" title=":comments_number_text">:comments_number_text</a></li>');
					}
					break;
				case 'author':
					echo Website::getMetaTemplate('<li class="author"><a href=":author_link" title=":author_name">:author_name</a></li>');
					break;
				case 'date':
					echo Website::getMetaTemplate('<li class="date published updated"><a href=":date_month_link" title=":0">:date</a></li>', sprintf(__('View all posts from %s', 'website'), get_the_date('F')));
					break;
				case 'date_time':
					echo Website::getMetaTemplate(
						'<li class="date published updated"><a href=":date_month_link" title=":0">:1</a></li>',
						sprintf(__('View all posts from %s', 'website'), get_the_date('F')),
						sprintf(__('%1$s at %2$s', 'website'), Website::getMeta('date'), Website::getMeta('time'))
					);
					break;
				case 'time_diff':
					echo Website::getMetaTemplate('<li class="date"><a href=":link" title=":title">:time_diff</a></li>');
					break;
				case 'category':
					echo Website::getMetaTemplate('[if:category_list]<li class="category">:category_list</li>[endif]');
					break;
				case 'tags':
					echo Website::getMetaTemplate('[if:tags_list]<li class="tags">:tags_list</li>[endif]');
					break;
				case 'link':
					echo Website::getMetaTemplate('<li class="link"><a href=":link" title=":title">:0</a></li>', __('Permalink', 'website'));
					break;
				case 'edit':
					echo Website::getMetaTemplate('[if:edit_link]<li class="edit"><a href=":edit_link" title=":0">:0</a></li>[endif]', __('Edit', 'website'));
					break;
			}

		}

		echo '</ul>';

	}

}