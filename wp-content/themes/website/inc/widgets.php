<?php
/**
 * @package    WordPress
 * @subpackage Website
 */

namespace Website\Widgets\Widget;

use Drone\Widgets\Widget;
use Drone\Template;

if (!defined('ABSPATH')) {
	exit;
}

/**
 * Search
 */
class Search extends Widget
{

	protected function onWidget(array $args, &$html)
	{

		$template = Template::instance(<<<'EOT'
			<form action=":home_url" method="get">
				<input type="submit" value="" />
				<div class="input">
					<input type="text" name="s" placeholder=":search_text" value=":search_query" />
				</div>
			</form>
EOT
		);

		$template
			->home_url(home_url('/'))
			->search_text(__('search', 'website'))
			->search_query(get_search_query());

		$html = $template->build();

	}

	public function __construct()
	{
		parent::__construct(__('Search', 'website'), array(
			'description' => __('A search form for your site.', 'website')
		));
	}

}

/**
 * Social media
 */
class Social extends Widget
{

	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('select', 'type', 'vertical', __('Type', 'website'), '', array('options' => array(
			'vertical'   => __('Vertical', 'website'),
			'horizontal' => __('Horizontal', 'website')
		)));
		$options->addOption('collection', 'icons', array('icon' => '', 'title' => '', 'url' => is_ssl() ? 'https://' : 'http://'), __('Icons', 'website'), '', array('type' => 'social_media'));
		$options->addOption('boolean', 'target_blank', false, '', '', array('caption' => __('Open links in new window', 'website')));
	}

	public function onOptionsCompatybility(array &$data, $version)
	{

		if (version_compare($version, '3.0') < 0) {
			$data['icons'] = array();
			for ($i = 0; $i <= 99; $i++) {
				if (isset($data["slot_{$i}_icon"]) && isset($data["slot_{$i}_title"]) && isset($data["slot_{$i}_url"])) {
					if ($data["slot_{$i}_icon"] || $data["slot_{$i}_title"] || $data["slot_{$i}_url"]) {
						$data['icons'][] = array(
							'icon'  => $data["slot_{$i}_icon"],
							'title' => $data["slot_{$i}_title"],
							'url'   => $data["slot_{$i}_url"]
						);
					} else {
						$data['icons'][] = array(
							'icon'  => '_',
							'title' => '',
							'url'   => ''
						);
					}
				}
			}
			while (count($data['icons']) > 0 && $data['icons'][count($data['icons'])-1]['icon'] == '_') {
				unset($data['icons'][count($data['icons'])-1]);
			}
		}

		if (version_compare($version, '4.2-beta-2') < 0) {
			if (isset($data['icons']) && is_array($data['icons'])) {
				$i = 0;
				while (isset($data['icons'][$i]['icon'])) {
					if ($data['icons'][$i]['icon'] == '_') {
						$data['icons'][$i]['icon'] = '';
					} else {
						$data['icons'][$i]['icon'] = preg_replace('/\.png$/', '', $data['icons'][$i]['icon']);
					}
					$i++;
				}
			}
		}

	}

	protected function onWidget(array $args, &$html)
	{

		$scheme = \Website::to('appearance/scheme');
		if ($args['id'] == 'sidebar-bottom') {
			$scheme = $scheme == 'bright' ? 'dark' : 'bright';
		}

		// Template
		$template = Template::instance(<<<'EOT'
			<ul class=":type">
				:icons
			</ul>
EOT
		);

		$template
			->type($this->wo('type'));

		// Icons
		foreach ($this->wo_('icons')->options as $option) {

			if ($option->value('icon') && (!$option->value('url') || (!$option->value('title') && $this->wo('type') == 'vertical'))) {
				continue;
			}

			$template->icons .= Template::instance(<<<'EOT'
				<li>
					[if:src]
						<a href=":url" title=":title" style="background-image: url(':src');"[if:target_blank] target="_blank"[endif]>
							:title
						</a>
					[endif]
				</li>
EOT
			, array(
				'src'          => $option->value('icon') ? $option->option('icon')->imageURI("data/img/{$scheme}/social") : false,
				'url'          => $option->value('url'),
				'title'        => $option->value('title'),
				'target_blank' => $this->wo('target_blank')
			));

		}

		$html = $template->build();

	}

	public function __construct()
	{
		parent::__construct(__('Social media', 'website'), array(
			'description' => __('Social media icons.', 'website')
		));
	}

}

/**
 * Site info
 */
class Info extends Widget
{

	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('text', 'title', get_bloginfo('name'), __('Site title', 'website'));
		$options->addOption('text', 'tagline', get_bloginfo('description'), __('Tagline', 'website'));
	}

	protected function onWidget(array $args, &$html)
	{

		$template = Template::instance(<<<'EOT'
			<hgroup>
				<h1>:title</h1>
				<h2>:tagline</h2>
			</hgroup>
EOT
		);

		$template
			->title($this->wo('title'))
			->tagline($this->wo_('tagline')->translate());

		$html = $template->build();

		$this->wo_('title')->value = ''; // needed to avoid double title

	}

	public function __construct()
	{
		parent::__construct(__('Site info', 'website'), array(
			'description' => __('Website title and tagline.', 'website')
		));
	}

}