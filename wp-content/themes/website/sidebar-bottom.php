<?php
/**
 * @package    WordPress
 * @subpackage Website
 */
?>

<aside id="aside-bottom" class="clear">
	<ul>
		<?php dynamic_sidebar('sidebar-bottom'); ?>
	</ul>
</aside>