<?php
/**
 * @package    WordPress
 * @subpackage Website
 */
?>

<section class="main">
	<?php Website::title(); ?>
	<?php get_template_part('content', 'page'); ?>
</section>