<?php get_header(); ?>
	
	<!-- body wrapper START -->
	<div class="body-wrapper">
		
		<!-- content with sidebar START -->
		<section class="section">
			
			<?php get_template_part( '/templates/trendy', 'breadcrumb' ); ?>
			
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<div id="posts-grid1" class="row">
						<?php get_template_part( '/templates/posts', 'loop' ); ?>
						</div>
						<?php trendy_pagination(); ?>
					</div>
					<div class="col-sm-4">
						<?php get_sidebar( 'blog' ); ?>						
					</div>
				</div>
			</div>
		</section>
		<!-- content with sidebar END -->
		
	</div>
	<!-- body wrapper END -->
	
<?php get_footer(); ?>