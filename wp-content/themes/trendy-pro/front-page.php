<?php get_header(); ?>

<?php
	global $trendy_options;
	if ( $trendy_options['home-slider'] ) {
		get_template_part( '/templates/home', 'slider' );
	} 
?>

<?php
	if ( !empty( $trendy_options['homepage-layout'] ) ) {
		
		$home_layout = $trendy_options['homepage-layout'];
		switch ( $home_layout ) {
		
			case "sidebar_left":			
				get_template_part( '/templates/sidebar', 'left' );		
				break;
		
			case "sidebar_right":			
				get_template_part( '/templates/sidebar', 'right' );		
				break;
		
			case "fluid_template":			
				get_template_part( '/templates/fluid', 'template' );		
				break;
				
			default:
				get_template_part( '/templates/no', 'sidebar' );
		}
		
	} else {
		get_template_part( '/templates/no', 'sidebar' );
	}
?>
	
<?php get_footer(); ?>