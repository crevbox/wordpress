
		
	<footer class="site-footer">
				
		<?php
			// footer social profiles' icons
			global $trendy_options;
			if( $trendy_options['footer-social'] ){
				get_template_part( '/templates/social', 'nav' );
			}
		?>
		
		<?php if( $trendy_options['trendy-footer'] ) : ?>
		<div class="ft-inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<?php 
							// footer logo
							if( !empty( $trendy_options['footer-logo']['url'] ) ){
								echo '<p><img src="' . esc_url( $trendy_options['footer-logo']['url'] ) . '" class="ft-logo" alt="' . get_bloginfo('name') . '"></p>';
							}
							// footer description text
							if( !empty( $trendy_options['footer-text'] ) ){
								echo '<p>' . esc_html( $trendy_options['footer-text'] ) . '</p>';
							}
							// footer menu START
							$has_footer_menu = (has_nav_menu('footer_menu')) ? 'true' : 'false';
							
							if($has_footer_menu == 'true') {
								$args = array(
									'theme_location' => 'footer_menu',
									'menu_class' => 'list-inline ft-nav',
									'container'	 => 'false',
									'depth'		 => 1
								);
								wp_nav_menu( $args );
							}
							// footer menu END
							//footer banner ad
							if( $trendy_options['footer-ad'] && !empty( $trendy_options['footer-ad-image'] ) && !empty( $trendy_options['footer-ad-url'] ) ) {
								echo '<div class="ad-wrap"><a href="' . esc_url( $trendy_options['footer-ad-url'] ) . '" title="' . esc_attr__( 'Sponser', 'trendy-pro' ) . '"><img src="' . esc_url( $trendy_options['footer-ad-image']['url'] ) . '" alt="' . esc_attr__( 'Sponser', 'trendy-pro' ) . '"></a></div>';
							}
						?>						
											
					</div>
				</div>
			</div>
		</div>
		<?php
			endif;
			// copyright message
			get_template_part( '/templates/copyright', 'message' ); 
		?>
		
	</footer>
	
	<!-- Back to Top Button -->
	<a href="#0" class="go-top"><i class="fa fa-angle-up"></i></a>
	
	<?php wp_footer(); ?>

</body>
</html>