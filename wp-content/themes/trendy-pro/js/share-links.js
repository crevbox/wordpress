jQuery( document ).ready( function($){
	
	'use strict';
	// Social Sharing Links
	$(function(){
		 	
		createFacebookShare ( $( '.share-list' ).find( '.facebook' ) );
		createTwitterShare ( $( '.share-list' ).find( '.twitter' ) );
		createLinkedinShare ( $( '.share-list' ).find( '.linkedin' ) );
		createGooglePlusShare ( $( '.share-list' ).find( '.google-plus' ) );
		createPinterestShare ( $( '.share-list' ).find( '.pinterest' ) );
		createRedditShare ( $( '.share-list' ).find( '.reddit' ) );
		
		// facebook share link
		function createFacebookShare(item) {
            if (item.length && !item.attr('onclick')) {
               item.attr('onclick', "window.open('http://www.facebook.com/sharer/sharer.php?display=popup&u=" + encodeURIComponent(window.location.href) + "', '_blank', 'top=100,left=100,toolbar=0,status=0,width=620,height=400'); return false;");
            }
        }
		
		// twitter share link
		function createTwitterShare(item) {
            if (item.length && !item.attr('onclick')) {
				var link_title = $('h1').text();
                item.attr('onclick', "window.open('https://twitter.com/intent/tweet?text=" + link_title + "&url=" + encodeURIComponent(window.location.href) + "', '_blank', 'top=100,left=100,toolbar=0,status=0,width=620,height=300'); return false;");
            }
        }
		
		// linkedin share link
		function createLinkedinShare(item) {
            if (item.length && !item.attr('onclick')) {
				var link_title = $('h1').text();
                item.attr('onclick', "window.open('http://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(window.location.href) + "&title=" + link_title + "&source=" + encodeURIComponent(window.location.href) + "', '_blank', 'top=100,left=100,toolbar=0,status=0,width=620,height=300'); return false;");
            }
        }

		// google plus share link
		 function createGooglePlusShare(item) {
            if (item.length && !item.attr('onclick')) {
                item.attr('onclick', "window.open('https://plus.google.com/share?url=" + encodeURIComponent(window.location.href) + "', '_blank', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;");
            }
        }
		
		//pinterest share link
		function createPinterestShare(item) {
            if (item.length && !item.attr('onclick')) {
				var thumb_url = $('#trendy-featured-img').attr('data-url');
                item.attr('onclick', "window.open('http://pinterest.com/pin/create/button/?url=" + encodeURIComponent(window.location.href) + "&media=" + thumb_url + "', '_blank', 'top=100,left=100,toolbar=0,status=0,width=620,height=400'); return false;");
            }
        }
		
		//reddit share link
		function createRedditShare(item) {
            if (item.length && !item.attr('onclick')) {
				var link_title = $('h1').text();
                item.attr('onclick', "window.open('http://www.reddit.com/submit?url=" + encodeURIComponent(window.location.href) + "&amp;title=" + link_title + "', '_blank', 'top=100,left=100,toolbar=0,status=0,width=620,height=400'); return false;");
            }
        }
	});
	
});