/* ==========================================================================
   NOTE:
   This file is being used to activate and set options for all jQuery plugins
   for this theme. Please don't modify this file unless necessary. This will 
   make it easy for you to upgrade your theme with new theme files easily 
   when new version of this theme will be available.    
   --------------------------------------------------------------------------
    TABLE OF CONTENT
   --------------------------------------------------------------------------
   01 - Preloader
   02 - Back to Top Button
   03 - Responsive Video
   04 - Search Button
   05 - Featured Carousel
   ========================================================================== */

jQuery( document ).ready( function($){
	
	'use strict';
	
	
	/* 01 - Preloader */
    $('.spinner-wrap').fadeOut();
	$('.preloader').delay(500).fadeOut('slow');
	
	
	$("#posts-grid-wrap").mpmansory({
		childrenClass: 'post-item', // default is a div
		columnClasses: 'padding', //add classes to items
		breakpoints:{
			lg: 6, 
			md: 12, 
			sm: 12,
			xs: 12
		},
		distributeBy: { order: false, height: false, attr: 'data-order', attrOrder: 'asc' }
	});
	$("#posts-grid1").mpmansory({
		childrenClass: 'post-item', // default is a div
		columnClasses: 'padding', //add classes to items
		breakpoints:{
			lg: 6, 
			md: 12, 
			sm: 12,
			xs: 12
		},
		distributeBy: { order: false, height: false, attr: 'data-order', attrOrder: 'asc' }
	});
	$("#posts-grid2").mpmansory({
		childrenClass: 'post-item', // default is a div
		columnClasses: 'padding', //add classes to items
		breakpoints:{
			lg: 3, 
			md: 4, 
			sm: 6,
			xs: 12
		},
		distributeBy: { order: false, height: false, attr: 'data-order', attrOrder: 'asc' }
	});
	$("#posts-grid3").mpmansory({
		childrenClass: 'post-item', // default is a div
		columnClasses: 'padding', //add classes to items
		breakpoints:{
			lg: 4, 
			md: 6, 
			sm: 6,
			xs: 12
		},
		distributeBy: { order: false, height: false, attr: 'data-order', attrOrder: 'asc' }
	});
	
	
	/* 02 - Back to Top Button */
	$(function(){
		var offset = 300,
		offset_opacity = 1200,
		scroll_top_duration = 700,
		$back_to_top = $('.go-top');
		$(window).scroll(function(){
			( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
			if( $(this).scrollTop() > offset_opacity ) { 
				$back_to_top.addClass('cd-fade-out');
			}
		});
		$back_to_top.on('click', function(event){
			event.preventDefault();
			$('body,html').animate({
				scrollTop: 0 ,
				}, scroll_top_duration
			);
		});
	});
	
	
	/* 03 - Responsive Video */
	$(".video-wrap, .embed-youtube").fitVids();
	
	
	/* 04 - Search Button */
	$('.search-btn').on('click', function(){
		$('.search-wrap').slideToggle();
		$('.search-wrap:visible').find('.search-field').focus();
	});
	
	
	/* 05 - Featured Carousel */
	$("#featured1, .loop-carousel").owlCarousel({
		items: 1,
		video:true,
		dots: false,
		autoplay: true,
		autoplayHoverPause: true,
		autoplaySpeed: 1000,
		nav:true,
		navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>']
	});
	$("#featured2").owlCarousel({
		items:1,
		loop: true,
		video:true,
		autoHeight:true,
		dots: false,
		autoplay: true,
		autoplayHoverPause: true,
		autoplaySpeed: 1000,
		nav:true,
		navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
		responsive:{
			0:{
				items:1,
				nav:true
			},
			767:{
				items:2,
				nav:false
			},
			1140:{
				items:3,
				nav:true,
				loop:false
			},
			1440:{
				items:4,
				nav:true,
				loop:false
			},
			1900:{
				items:5,
				nav:true,
				loop:false
			}
		}
	});	
	  	
});