<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
<?php get_header(); ?>
<!-- body wrapper START -->
<div class="body-wrapper fluid inner-page">
	
	<!-- content with sidebar START -->
	<section class="section">
	
		<div class="inner-banner img-bg dark-overlay" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/bg-pattern.png);">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="search-query">RESULTS FOR <span class="light-weight">"<?php global $wp; echo get_search_query( false ) ? get_search_query( false ) : 'random search'; ?>"</span></h1>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div id="posts-grid2" class="row">
						<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
						<?php
							$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'trendy-post' );
							$thumb_url = $post_thumb[0];
							$post_format = get_post_format();
							$current_post_type = get_post_type($post->ID);
						?>
							<div class="post-item">
								<article <?php post_class('mini-post'); ?>>
									<?php if ( $post_format == 'video' ) :?>
										<?php if ( is_plugin_active( 'trendy-extensions/trendy_extensions.php' ) ) : ?>
											<?php
												$video_source = get_field( 'choose_video_source' );
												$video_id = NULL;
												if( $video_source == 'youtube' ) :
												$video_id = get_field( 'youtube_video_id' );
											?>
											<div class="video-wrap"><iframe width="560" height="315" src="<?php echo esc_url( 'https://www.youtube.com/embed/' . $video_id ); ?>" allowfullscreen></iframe></div>
											<?php
												elseif ( $video_source == 'vimeo' ) :
												$video_id = get_field( 'vimeo_video_id' );
											?>
											<div class="video-wrap"><iframe src="<?php echo esc_url( 'https://player.vimeo.com/video/' . $video_id ); ?>?title=0&byline=0&portrait=0" width="640" height="360" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
											<?php endif; ?>
										<?php endif; ?>
										<div class="post-content has-border-top">
									<?php else : ?>
										<?php if ( class_exists('MultiPostThumbnails') ) : ?>				
											<?php
												$total_thumbs = 2; //hold total featured images assigned to the post
												$thumbs_counter = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'image' . $total_thumbs, NULL, 'trendy-post');
												if ( $thumbs_counter ) :
											?>
											<div class="owl-carousel featured-carousel loop-carousel">
												<?php if ( $thumb_url ) : ?>
												<div><a href="<?php the_permalink();?>" class="post-thumbnail" title="<?php get_the_title();?>"><img src="<?php echo esc_url( $thumb_url ); ?>" alt="<?php get_the_title();?>"></a></div>
												<?php endif; ?>
												<?php
													while ( $thumbs_counter != '' ){
														echo '<div><a href="' . get_the_permalink() . '" class="post-thumbnail" title="' . get_the_title() . '"><img src="' . $thumbs_counter . '" alt="' . get_the_title() . '"></a></div>';
														$total_thumbs += 1;
														$thumbs_counter = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'image' . $total_thumbs, NULL, 'trendy-post');
													}
												?>
											</div>
											<div class="post-content">
											<?php elseif ( $thumb_url ) : ?>
												<a href="<?php the_permalink();?>" class="post-thumbnail" title="<?php the_title();?>">
													<img src="<?php echo esc_url( $thumb_url ); ?>" alt="<?php the_title();?>">
												</a>
												<div class="post-content">
											<?php else : ?>
												<div class="post-content has-border-top">
											<?php endif; ?>
																	
										<?php elseif ( $thumb_url ) : ?>
											<a href="<?php the_permalink();?>" class="post-thumbnail" title="<?php the_title();?>">
												<img src="<?php echo esc_url( $thumb_url ); ?>" alt="<?php the_title();?>">
											</a>
											<div class="post-content">
										<?php else: ?>
											<div class="post-content has-border-top">
										<?php endif; ?>
										
									<?php endif; ?>
									
										<h3 class="post-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
										<?php the_excerpt();?>
										<footer class="clearfix">
											<a href="<?php comments_link(); ?>" class="comments" title="<?php echo esc_attr__( 'Total Comments', 'trendy-pro' ); ?>"><span><?php echo get_comments_number(); ?></span> <i class="fa fa-comments-o"></i></a>
											<ul class="list-inline post-meta">
												<li><a class="post-author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php echo get_the_author_meta( 'display_name' ); ?>"><?php echo get_the_author_meta( 'display_name' ); ?></a></li>
												<li><?php the_time( 'Y-m-d' ); ?></li>
											</ul>
										</footer>
									</div>
									<?php echo '<span class="result-type">' . $current_post_type . '</span>'; ?>
								</article>
							</div>
						<?php endwhile; else : ?>
						<div class="not-found">
							<section class="section text-center">
								<div class="container">
									<div class="row">
										<div class="col-lg-12">
											<h1><?php echo esc_html__( 'Sorry', 'trendy-pro' ); ?><span class="alt-font">!</span><small><?php echo esc_html__( 'No results for your search.', 'trendy-pro' ); ?></small></h1>
											<p><?php echo esc_html__( 'Why not try another search, or go back to homepage.', 'trendy-pro' ); ?></p>
											<?php get_search_form( $echo = true ); ?>
											<a class="btn btn-primary" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html__( 'Go To Homepage', 'trendy-pro' ); ?></a>
										</div>
									</div>
								</div>
							</section>
						</div>
						<?php endif; ?>
					</div>
					<?php trendy_pagination(); ?>
				</div>
			</div>
		</div>
		
	</section>
	<!-- content with sidebar END -->
	
</div>
<!-- body wrapper END -->

<?php get_footer(); ?>