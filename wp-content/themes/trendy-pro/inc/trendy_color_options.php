<?php

function trendy_custom_color_options(){
	
	global $trendy_options;
	$trendy_options_css = '';
	
	// text color on dark background
	$trendy_accent_color = $trendy_options['trendy-accent-color'];
	
	
	
	/* options panel generated css styles */	
	$trendy_options_css .= '.top-navbar .social-nav li a:hover,
.mini-post .post-title a:hover,
.comment-list li .comment-body .reply a:hover,
.author-box .vcard a:hover,
.site-footer .ft-nav a:hover,
.comment-author .url:hover,
.aside-widget .widget-inner li a:hover,
.aside-widget .widget-inner li a:focus,
.breadcrumb-wrap .breadcrumb a:hover,
.logged-in-as a:hover,
.logged-in-as a:focus,
.aside-widget .widget-inner .tagcloud a:hover,
.aside-widget .widget-inner .tagcloud a:focus,
.calendar_wrap #wp-calendar a:hover,
.calendar_wrap #wp-calendar a:focus,
.calendar_wrap #wp-calendar #today,
blockquote cite a:hover {
	color:' . $trendy_accent_color . ';
}';	
	$trendy_options_css .= '.categories-wrap ul li.active span,
.category-label li a:hover,
.btn-default:hover,
.btn-default:focus,
.btn-primary,
input[type=button],
input[type=submit],
button,
.page-numbers li span.current,
.paginated-links > span {
	background-color:' . $trendy_accent_color . '; border-color:' . $trendy_accent_color . ';
}';
	$trendy_options_css .= '.author-box .social-nav li a:hover,
.spinner,
.go-top,
.mini-post:hover .post-content .result-type {
	background-color:' . $trendy_accent_color . ';
}';
	
	
	
	// custom css editor styles ( user entered styles )
	if( !empty( $trendy_options['trendy-css-editor'] ) ){
		$trendy_options_css .= $trendy_options['trendy-css-editor'];
	}
	
	
	wp_add_inline_style( 'main_css', $trendy_options_css );
	
}

?>