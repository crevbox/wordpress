<?php

add_action( 'tgmpa_register', 'trendy_register_required_plugins' );

/* required plugins for this theme. */
function trendy_register_required_plugins() {

	$plugins = array(
		
		// Trendy Extensions
		array(
			'name'      => esc_html__( 'Trendy Extensions', 'trendy-pro' ),
			'slug'      => 'trendy-extensions',
			'source'    => 'https://github.com/uipro/trendy-extensions/raw/master/trendy-extensions.zip',
			'required'           => true,
		),

		// Multiple Post Thumbnails
		array(
			'name'      => esc_html__( 'Multiple Post Thumbnails', 'trendy-pro' ),
			'slug'      => 'multiple-post-thumbnails',
			'required'  => true,
		),

		// One Click Demo Import
		array(
			'name'      => esc_html__( 'One Click Demo Import', 'trendy-pro' ),
			'slug'      => 'one-click-demo-import',
			'required'  => true,
		),

	);

	$config = array(
		'id'           => 'trendy-pro',
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'has_notices'  => true,
		'dismissable'  => true,
		'dismiss_msg'  => esc_html__( 'Please install required plugins, for Trendy Theme, listed below.', 'trendy-pro' ),
		'is_automatic' => false,
		'message'      => '',
	);

	tgmpa( $plugins, $config );
}
