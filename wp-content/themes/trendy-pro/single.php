<?php get_header(); ?>

<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>

	<!-- body wrapper START -->
	<div class="body-wrapper">
	<?php if( have_posts() ) : while( have_posts()  ) : the_post();
	
		$trendy_field = 'trendy_post_style_two';
		if ( is_plugin_active( 'trendy-extensions/trendy_extensions.php' ) ) :
			if ( get_field( 'choose_post_style' ) ) {		
				$trendy_field = get_field( 'choose_post_style' );
			}
		endif;
	
		if ( $trendy_field == 'trendy_post_style_three' ) {			
			get_template_part( '/templates/post', 'template3' );			
		} elseif ( $trendy_field == 'trendy_post_style_two' ) {			
			get_template_part( '/templates/post', 'template2' );			
		} else {			
			get_template_part( '/templates/post', 'template1' );			
		}
		
	endwhile; endif; ?>
	</div>
	<!-- body wrapper END -->
	
<?php get_footer(); ?>