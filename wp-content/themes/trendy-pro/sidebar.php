<?php if ( is_active_sidebar( 'blog' ) ) { ?>
	<!-- sidebar START -->
	<aside class="sidebar">
		<?php dynamic_sidebar( 'blog' ); ?>
	</aside>
	<!-- sidebar END -->
<?php } ?>