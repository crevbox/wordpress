<?php if ( is_active_sidebar( 'home' ) ) { ?>
	<!-- sidebar START -->
	<aside class="sidebar">
		<?php dynamic_sidebar( 'home' ); ?>
	</aside>
	<!-- sidebar END -->
<?php } ?>