<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<?php wp_head(); ?>
	
</head>
<body <?php body_class(); ?>>
	
	<?php
		global $trendy_options;
	?>
	
	<!-- preloader START -->
	<div class="preloader">
		<div class="spinner-wrap">
			<div class="spinner"></div>
			<span class="preloader-text"><?php echo esc_html__( 'loading...', 'trendy-pro' ); ?></span>
		</div>
	</div>
	<!-- preloader END -->
	
	<?php
		// top navigation bar
		get_template_part( '/templates/top', 'nav' );
	?>
	
	<?php
		// page header
		if( $trendy_options['header-style'] == '1' ){
			get_template_part( '/templates/header', 'one' );
		} else {
			get_template_part( '/templates/header', 'two' );
		}
	?>
		
	<!-- main navigation START -->
	<div class="nav-wrap">
		<nav class="navbar navbar-default navbar-fixed-top affix-top" data-spy="affix" data-offset-top="400">
			<div class="container">
			
				<!-- search button -->
				<a href="javascript:void(0);" class="search-btn"><i class="fa fa-search"></i></a>
				
				<!-- navbar header -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only"><?php echo esc_html__( 'Toggle navigation', 'trendy-pro' ); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand" title="<?php bloginfo('name'); ?>"><?php if( !empty( $trendy_options['trendy-logo']['url'] ) ) : ?><?php echo '<img src="' . esc_url( $trendy_options['trendy-logo']['url'] ) . '"'; ?> alt="<?php bloginfo('name'); ?>"><?php else : ?><span><?php bloginfo('name'); ?></span><?php endif; ?></a>
				</div>
				
				<!-- navbar -->
				<div id="navbar" class="navbar-collapse collapse">
					<?php
						// main menu START
						$has_main_menu = (has_nav_menu('main_menu')) ? 'true' : 'false';
						
						if($has_main_menu == 'true') {
							$args = array(
								'theme_location'	=> 'main_menu',
								'menu_class'		=> 'nav navbar-nav',
								'container'			=> 'false',
								'depth'				=> 2,
								'walker'			=> new wp_bootstrap_navwalker()
							);
							wp_nav_menu( $args );
						}
						else {
							echo '<ul class="nav navbar-nav"><li><a href="javascript:void(0);">' . esc_html__( 'No menu assigned!', 'trendy-pro' ) . '</a></li></ul>';
						}
						// main menu END
					?>
				</div>
				<!--/.navbar-collapse -->
				
			</div>
			<div class="search-wrap">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<!-- search form START -->
							<?php get_search_form( $echo = true ); ?>
							<!-- search form END -->
						</div>
					</div>
				</div>
			</div>
		</nav>		
	</div>
	<!-- main navigation END -->