<?php

/*
*	Thanks for using our Trendy Pro - Personal Blog WordPress Theme
*	Our Portfolio: http://themeforest.net/user/uipro/portfolio
*/


/* ------------------------------------------------
   *  Adding Support for Title Tag
--------------------------------------------------- */
if ( ! function_exists( '_wp_render_title_tag' ) ) {
	function trendy_theme_slug_render_title() {
		add_theme_support( 'title-tag' );
	}
	add_action( 'after_setup_theme', 'trendy_theme_slug_render_title' );
}


/* ------------------------------------------------
   *  Adding Support for Menus
--------------------------------------------------- */
if( ! function_exists( 'trendy_load_theme_textdomain' ) ){
	function trendy_load_theme_textdomain() {
		load_theme_textdomain( 'trendy-pro', get_template_directory() . '/languages' );
	}
}
add_action( 'after_setup_theme', 'trendy_load_theme_textdomain' );


/* ------------------------------------------------
   *  Adding Support for Menus
--------------------------------------------------- */
add_theme_support( 'menus' );
/* registering theme menus */
if( ! function_exists( 'trendy_register_wp_theme_menus' ) ){
	function trendy_register_wp_theme_menus() {
		register_nav_menus(
			array(
				'main_menu' => esc_html__( 'Main Menu', 'trendy-pro' ),
				'footer_menu' => esc_html__( 'Footer Menu', 'trendy-pro' ),
				'top_menu_bar' => esc_html__( 'Top Menu Bar', 'trendy-pro' )
			)
		);
	}
}
add_action( 'init', 'trendy_register_wp_theme_menus' );



/* ------------------------------------------------
   *  TGM plugin activation
--------------------------------------------------- */
require_once get_template_directory() . '/inc/tgm/class-tgm-plugin-activation.php'; 
require_once get_template_directory() . '/inc/tgm/trendy_require_plugins.php';
  
 
 
/* ------------------------------------------------
   *  Register Custom Navigation Walker (for bootstrap nav)
--------------------------------------------------- */
require_once get_template_directory() . '/inc/bootstrap_navwalker.php';



/* ------------------------------------------------
   *  Adding support for title-tag
--------------------------------------------------- */
add_action( 'after_setup_theme', 'trendy_wp_slug_setup' );
function trendy_wp_slug_setup() {
	add_theme_support( 'title-tag' );
}


/* ------------------------------------------------
   *  Adding support for featured images
--------------------------------------------------- */
add_theme_support( 'post-thumbnails' );
// custom image sizes for this theme
if (!function_exists('trendy_img_sizes')) {
	function trendy_img_sizes(){
		add_image_size( 'trendy-mini-thumb', 120, 120, true ); //default
		add_image_size( 'trendy-thumb', 200, 140, true ); //default
		add_image_size( 'trendy-post', 720, 500, true ); //default
		add_image_size( 'trendy-slide', 1400, 800, true ); //default
	}
}
trendy_img_sizes();



/* ------------------------------------------------
   *  Adding support for multiple featured images in theme
--------------------------------------------------- */
if (class_exists('MultiPostThumbnails')) {
	$types = array('post');
	foreach($types as $type) {
		new MultiPostThumbnails(array(
			'label'		=> esc_html__( 'Second Image', 'trendy-pro' ),
			'id'		=> 'image2',
			'post_type'	=> $type
		) );
		new MultiPostThumbnails(array(
			'label'		=> esc_html__( 'Third Image', 'trendy-pro' ),
			'id'		=> 'image3',
			'post_type'	=> $type
		) );
		new MultiPostThumbnails(array(
			'label'		=> esc_html__( 'Fourth Image', 'trendy-pro' ),
			'id'		=> 'image4',
			'post_type'	=> $type
		) );
		new MultiPostThumbnails(array(
			'label'		=> esc_html__( 'Fifth Image', 'trendy-pro' ),
			'id'		=> 'image5',
			'post_type'	=> $type
		) );
	}
}



/* ------------------------------------------------
   *  post formats
--------------------------------------------------- */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'trendy-extensions/trendy_extensions.php' ) ) {
	if( ! function_exists( 'trendy_post_formats' ) ){
		function trendy_post_formats(){
			add_theme_support( 'post-formats', array(
				'image',
				'video'
			) );
		}
	}
	add_action( 'after_setup_theme', 'trendy_post_formats' );
}




/* ------------------------------------------------
   *  Excerpt length
--------------------------------------------------- */
if( ! function_exists( 'trendy_excerpt_length' ) ){
	function trendy_excerpt_length( $length ){
		return 23;
	}
}
add_filter( 'excerpt_length', 'trendy_excerpt_length', 999 );
//custom excerpt ending
if( ! function_exists( 'trendy_excerpt_ending' ) ){
	function trendy_excerpt_ending( $ending ) {
		return ' ...';
	}
}
add_filter('excerpt_more', 'trendy_excerpt_ending');




/* ------------------------------------------------
   *  Pagination settings
--------------------------------------------------- */
if ( !function_exists( 'trendy_pagination' ) ) {
	
	function trendy_pagination() {
		
		$prev_arrow = is_rtl() ? '&rarr;' : '&larr;';
		$next_arrow = is_rtl() ? '&larr;' : '&rarr;';
				
		global $wp_query;
		$total = $wp_query->max_num_pages;
		$big = 999999999; // need an unlikely integer
		if( $total > 1 )  {
			 if( !$current_page = get_query_var('paged') )
				 $current_page = 1;
			 if( get_option('permalink_structure') ) {
				 $format = 'page/%#%/';
			 } else {
				 $format = '&paged=%#%';
			 }
			echo paginate_links(array(
				'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'		=> $format,
				'current'		=> max( 1, get_query_var('paged') ),
				'total' 		=> $total,
				'mid_size'		=> 1,
				'type' 			=> 'list',
				'prev_text'		=> $prev_arrow,
				'next_text'		=> $next_arrow,
			 ) );
		}
	}
	
}



/* ------------------------------------------------
   *  Post Likes
--------------------------------------------------- */
require_once get_template_directory() . '/inc/post-like.php'; 
  
 
 
/* ------------------------------------------------
   *  Defautl Video Size
--------------------------------------------------- */
if ( ! isset( $content_width ) ) {
	$content_width = 1140;
}
 
 
/* ------------------------------------------------
   *  Adding Support for Feed Links
--------------------------------------------------- */
add_theme_support( 'automatic-feed-links' );  
 
 
/* ------------------------------------------------
   *  Including Default Google Fonts
--------------------------------------------------- */
if( ! function_exists( 'trendy_wp_fonts_url' ) ){
	function trendy_wp_fonts_url() {
		$font_url = '';
		if ( 'off' !== _x( 'on', 'Google font: on or off', 'trendy-pro' ) ) {
			$font_url = add_query_arg( 'family', urlencode( 'Playfair Display:400,400italic|Roboto:300,400,500,700&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
		}
		return $font_url;
	}
}


/* ------------------------------------------------
   *  Integrating redux framework
--------------------------------------------------- */
/** remove redux menu under the tools **/
if ( class_exists( 'Redux' ) ) {	
	add_action( 'admin_menu', 'trendy_wp_remove_redux_menu', 12 );
	function trendy_wp_remove_redux_menu() {
		remove_submenu_page('tools.php','redux-about');
	}
}


/* ------------------------------------------------
   *  ACF ( adding custom fields )
--------------------------------------------------- */
// choose post style ( signle post )
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_post-style',
		'title' => esc_html__( 'Post Style', 'trendy-pro' ),
		'fields' => array (
			array (
				'key' => 'field_58c61f3bc01f5',
				'label' => esc_html__('Choose Post Style', 'trendy-pro' ),
				'name' => 'choose_post_style',
				'type' => 'radio',
				'choices' => array (
					'trendy_post_style_one' => esc_html__( 'Post Style One', 'trendy-pro' ),
					'trendy_post_style_two' => esc_html__( 'Post Style Two', 'trendy-pro' ),
					'trendy_post_style_three' => esc_html__( 'Post Style Three', 'trendy-pro' ),
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'trendy_post_style_one',
				'layout' => 'vertical',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_featured-post',
		'title' => esc_html__( 'Featured Post', 'trendy-pro' ),
		'fields' => array (
			array (
				'key' => 'field_58cbc16d0780a',
				'label' => esc_html__( 'Mark As Featured Post', 'trendy-pro' ),
				'name' => 'trendy_featured_post',
				'type' => 'true_false',
				'instructions' => esc_html__( 'By checking this option, this post will be marked as Featured Post. Featured Posts can be displayed in homepage slider, by selecting an option from Theme Options - Home page - Display Posts By.', 'trendy-pro' ),
				'message' => '',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_video-link',
		'title' => esc_html__( 'Video Link', 'trendy-pro' ),
		'fields' => array (
			array (
				'key' => 'field_58c67faf0fd51',
				'label' => esc_html__( 'Video Source', 'trendy-pro' ),
				'name' => 'choose_video_source',
				'type' => 'select',
				'choices' => array (
					'youtube' => esc_html__( 'YouTube Video', 'trendy-pro' ),
					'vimeo' => esc_html__( 'Vimeo Video', 'trendy-pro' ),
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_58c6678834129',
				'label' => esc_html__( 'Insert YouTube Video ID', 'trendy-pro' ),
				'name' => 'youtube_video_id',
				'type' => 'text',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_58c67faf0fd51',
							'operator' => '==',
							'value' => 'youtube',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => esc_html__( 'e.g. hpeYWdkUtcE', 'trendy-pro' ),
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_58c6809323aa6',
				'label' => esc_html__( 'Insert Vimeo Video ID', 'trendy-pro' ),
				'name' => 'vimeo_video_id',
				'type' => 'text',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_58c67faf0fd51',
							'operator' => '==',
							'value' => 'vimeo',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => esc_html__( 'e.g. 176916362', 'trendy-pro' ),
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_format',
					'operator' => '==',
					'value' => 'video',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}



/* ------------------------------------------------
   *  Adding support for widgets
--------------------------------------------------- */
if( ! function_exists( 'trendy_register_sidebars' ) ){
	function trendy_register_sidebars(){
		//blog sidebar
		register_sidebar(array(
			'name' => esc_html__( 'Blog Sidebar', 'trendy-pro' ),
			'id' => 'blog',
			'description' => esc_html__( 'Displays on the side of pages in the blog section', 'trendy-pro' ),
			'before_widget' => '<div class="aside-widget"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<h4 class="widget-title"><span>',
			'after_title' => '</span></h4>'
		));
		//home sidebar
		register_sidebar(array(
			'name' => esc_html__( 'Home Sidebar', 'trendy-pro' ),
			'id' => 'home',
			'description' => esc_html__( 'Displays on the side of homepage with sidebar', 'trendy-pro' ),
			'before_widget' => '<div class="aside-widget"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<h4 class="widget-title"><span>',
			'after_title' => '</span></h4>'
		));
	}
}
add_action( 'widgets_init', 'trendy_register_sidebars' );


 
/* ------------------------------------------------
   *  Add Custom Class to body_class()
--------------------------------------------------- */
if( ! function_exists( 'trendy_body_classes' ) ){
	function trendy_body_classes( $classes, $class ) {
		$current_page = 'inner-page';
		if ( is_home() || is_archive() || is_single() || is_front_page() || is_search() ) {
			$current_page = 'not-inner-page';
		}
		$classes[] = $current_page;	 
		return $classes;
	}
}
add_filter( 'body_class', 'trendy_body_classes', 10, 3 );

 
/* ------------------------------------------------
   *  Enqueue Theme Styles
--------------------------------------------------- */
if( ! function_exists( 'trendy_wp_theme_css' ) ){
	function trendy_wp_theme_css() {
		wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css' );
		wp_enqueue_style( 'trendy_fonts', trendy_wp_fonts_url(), array(), '1.0.0' );
		wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0', 'all' );
		wp_enqueue_style( 'trendy_plugins_css', get_template_directory_uri() . '/css/plugins.css' );
		wp_enqueue_style( 'trendy_main_css', get_template_directory_uri() . '/style.css' );
		
		if ( is_plugin_active( 'trendy-extensions/trendy_extensions.php' ) ) {
			// Color Options
			require_once get_template_directory() . '/inc/trendy_color_options.php';
			trendy_custom_color_options();
		}
	}
}
add_action( 'wp_enqueue_scripts', 'trendy_wp_theme_css' );
 
 
/* ------------------------------------------------
   *  Enqueue Theme Scripts
--------------------------------------------------- */
if( ! function_exists( 'trendy_wp_theme_js' ) ){
	function trendy_wp_theme_js() {
		wp_enqueue_script( 'modernizr_respond_js', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js', '', '', false );
		wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array('jquery'), '3.3.7', true );
		wp_enqueue_script( 'trendy_plugins_js', get_template_directory_uri() . '/js/plugins.js', array('jquery', 'bootstrap_js'), '', true );
		wp_enqueue_script( 'trendy_main_js', get_template_directory_uri() . '/js/main.js', array('jquery', 'bootstrap_js', 'trendy_plugins_js'), '', true );
	}
}
add_action( 'wp_enqueue_scripts', 'trendy_wp_theme_js' );
 
 
/* ------------------------------------------------
   *  Conditional Scripts
--------------------------------------------------- */
if( ! function_exists( 'trendy_share_links_js' ) ){
	function trendy_share_links_js() {
		if ( is_single() ) {
			wp_enqueue_script( 'trendy_share_js', get_template_directory_uri() . '/js/share-links.js', array('jquery', 'bootstrap_js', 'trendy_plugins_js'), '', true );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'trendy_share_links_js' );
 
 
/* ------------------------------------------------
   *  Enqueue Comment Reply Script ( on single post page )
--------------------------------------------------- */
if( ! function_exists( 'trendy_enqueue_comments_reply' ) ){
	function trendy_enqueue_comments_reply() {
		if( get_option( 'thread_comments' ) )  {
			wp_enqueue_script( 'comment-reply' );
		}
	}
}
add_action( 'comment_form_before', 'trendy_enqueue_comments_reply' );
 
 
/* ------------------------------------------------
   *  Demo Data Import
--------------------------------------------------- */
if( ! function_exists( 'trendy_import_files' ) ){
	function trendy_import_files() {
	  return array(
		array(
		  'import_file_name'             => esc_html__('Demo Import', 'trendy-pro' ),
		  'categories'                   => array( esc_html__('Theme Content and Options', 'trendy-pro' ) ),
		  'local_import_file'            => trailingslashit( get_template_directory() ) . 'demo/trendy-demo-content.xml',
      	  'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demo/trendy-widgets.json',
      	  'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demo/trendy-customizer.dat',
		  'local_import_redux'           => array(
			array(
			  'file_path'   => trailingslashit( get_template_directory() ) . 'demo/redux.json',
			  'option_name' => 'trendy_options',
			),
		  ),
		  'import_preview_image_url'     => trailingslashit( get_template_directory() ) . 'screenshot.png',
		),
	  );
	}
}
add_filter( 'pt-ocdi/import_files', 'trendy_import_files' );

?>