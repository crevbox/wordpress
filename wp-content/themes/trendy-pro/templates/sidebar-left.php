<!-- body wrapper START -->
<div class="body-wrapper sidebar-left">
	<?php global $trendy_options; ?>
	<!-- content with sidebar START -->
	<div class="section <?php echo esc_attr( $trendy_options['home-slider'] ? 'has-slider' : 'no-slider' );  ?>">		
		<div class="container">
			<div class="row">
				<div class="col-sm-8 content-area">
					<div id="posts-grid1" class="row">
					<?php get_template_part( '/templates/posts', 'loop' ); ?>
					</div>
					<?php trendy_pagination(); ?>
				</div>
				<div class="col-sm-4 sidebar-area">
					<?php get_sidebar( 'home' ); ?>						
				</div>
			</div>
		</div>		
	</div>
	<!-- content with sidebar END -->	
</div>
<!-- body wrapper END -->