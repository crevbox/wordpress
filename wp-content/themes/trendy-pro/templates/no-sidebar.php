<!-- body wrapper START -->
<div class="body-wrapper fullwidth">
	<?php global $trendy_options; ?>
	<!-- content with sidebar START -->
	<div class="section <?php echo esc_attr( $trendy_options['home-slider'] ? 'has-slider' : 'no-slider' );  ?>">		
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div id="posts-grid3" class="row">
					<?php get_template_part( '/templates/posts', 'loop' ); ?>
					</div>
					<?php trendy_pagination(); ?>
				</div>
			</div>
		</div>		
	</div>
	<!-- content with sidebar END -->	
</div>
<!-- body wrapper END -->