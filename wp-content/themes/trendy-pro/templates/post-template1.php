<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
<!-- content with sidebar START -->
<article id="post-<?php the_ID(); ?>" <?php post_class( 'single-post style1' ); ?> itemscope itemtype="http://schema.org/BlogPosting">
	<div class="section">		
		<?php
			$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'trendy-slide' );
			$thumb_url = $post_thumb[0];
			$thumb_width = $post_thumb[1];
			$thumb_height = $post_thumb[2];
			$post_format = get_post_format();
		?>
		<meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="<?php the_permalink(); ?>" content="<?php the_permalink(); ?>">		
		<!-- post header START -->
		<header class="post-header">
						
			<?php
				if ( $post_format == 'video' ) :
				if ( is_plugin_active( 'trendy-extensions/trendy_extensions.php' ) ) :
			?>
			
			<div id="featured1" class="owl-carousel featured-carousel">
				<div class="featured-post dark-overlay">
					<?php
						$video_source = get_field( 'choose_video_source' );
						$video_id = NULL;
						if( $video_source == 'youtube' ) :
						$video_id = get_field( 'youtube_video_id' );
					?>
					<a class="owl-video" href="<?php echo esc_url( 'https://www.youtube.com/watch?v=' . get_field( 'youtube_video_id' ) ); ?>"></a>
					<span class="sr-only" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
						<!-- schema meta information for YouTube video -->
						<meta itemprop="name" content="<?php the_title(); ?>"/>
						<meta itemprop="thumbnailUrl" content="http://i1.ytimg.com/vi/<?php echo esc_url( $video_id ); ?>/1.jpg" />
						<meta itemprop="contentUrl" content="<?php echo esc_url( 'https://www.youtube.com/watch?v=' . get_field( 'youtube_video_id' ) ); ?>"/>
						<meta itemprop="embedUrl" content="<?php echo esc_url( 'https://www.youtube.com/embed/' . $video_id ); ?>"/>  
						<meta itemprop="description" content="<?php the_title(); ?>"/>
						<meta itemprop="width" content="560" />
						<meta itemprop="height" content="315" />
						<meta itemprop="uploadDate" content="<?php the_time( 'Y-m-d' ); ?>" />
						<span id="trendy-featured-img" class="sr-only" data-url="http://i1.ytimg.com/vi/<?php echo esc_url( $video_id ); ?>/0.jpg"></span>
					</span>
					<?php
						endif;
						if( $video_source == 'vimeo' ) :
						$video_id = get_field( 'vimeo_video_id' );
						$video_thumb =  unserialize(wp_remote_fopen("http://vimeo.com/api/v2/video/$video_id.php"));
					?>
					<a class="owl-video" href="<?php echo esc_url( 'https://vimeo.com/' . get_field( 'vimeo_video_id' ) ); ?>"></a>
					<span class="sr-only" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">										
						<!-- schema meta information for Vimeo video -->
						<meta itemprop="name" content="<?php the_title(); ?>"/>
						<meta itemprop="thumbnailUrl" content="<?php echo esc_url( $video_thumb[0]['thumbnail_small'] ); ?>" />
						<meta itemprop="contentUrl" content="<?php echo esc_url( 'https://vimeo.com/' . get_field( 'vimeo_video_id' ) ); ?>"/>
						<meta itemprop="embedUrl" content="<?php echo esc_url( 'https://player.vimeo.com/video/' . $video_id ); ?>"/>  
						<meta itemprop="description" content="<?php the_title(); ?>"/>
						<meta itemprop="width" content="500" />
						<meta itemprop="height" content="281" />
						<meta itemprop="uploadDate" content="<?php the_time( 'Y-m-d' ); ?>" />
						<span id="trendy-featured-img" class="sr-only" data-url="<?php echo esc_url( $video_thumb[0]['thumbnail_medium'] ); ?>"></span>
					</span>
					<?php endif; ?>
				</div>
			</div>
			<?php else : ?>
				<div id="featured1" class="owl-carousel featured-carousel">
					<div class="featured-post dark-overlay" style="background-image:url(http://placehold.it/1400x800);"></div>
				</div>
			<?php endif; ?>
			<?php elseif ( class_exists('MultiPostThumbnails') && $thumb_url ) : ?>
				<div id="featured1" class="owl-carousel featured-carousel">
					<div class="featured-post dark-overlay" style="background-image:url(<?php echo esc_url( $thumb_url ); ?>);"></div>
					<?php
						$total_thumbs = 2; //hold total featured images assigned to the post
						$thumbs_counter = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'image' . $total_thumbs, NULL, 'trendy-slide');
						while ( $thumbs_counter != '' ){
							echo '<div class="featured-post dark-overlay" style="background-image:url(' . $thumbs_counter . ');"></div>';
							$total_thumbs += 1;
							$thumbs_counter = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'image' . $total_thumbs, NULL, 'trendy-slide');
						}
					?>
				</div>
				<span id="trendy-featured-img" class="sr-only" data-url="<?php echo esc_url( $thumb_url ); ?>"></span>
			<?php elseif ( $thumb_url ) : ?>
				<div id="featured1" class="owl-carousel featured-carousel">
					<div class="featured-post dark-overlay" style="background-image:url(<?php echo esc_url( $thumb_url ); ?>);"></div>
				</div>
				<span id="trendy-featured-img" class="sr-only" data-url="<?php echo esc_url( $thumb_url ); ?>"></span>
			<?php else : ?>
				<div id="featured1" class="owl-carousel featured-carousel">
					<div class="featured-post dark-overlay" style="background-image:url(http://placehold.it/1400x800);"></div>
				</div>
			<?php endif; ?>
																
			<!-- schema item image object (only visible to search engines) -->
			<span class="sr-only" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
				<meta itemprop="url" content="<?php echo esc_url( $thumb_url ); ?>">
				<meta itemprop="width" content="<?php echo esc_attr( $thumb_width ); ?>">
				<meta itemprop="height" content="<?php echo esc_attr( $thumb_height ); ?>">
			</span>
						
			<!-- publisher information (HIDDEN ON SCREEN)-->
			<!-- schema item publisher information (update logo url and publisher name to your own) -->
			<?php global $trendy_options; ?>
			<span class="sr-only" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
				<!-- publisher logo -->
				<span class="sr-only" itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
					<meta itemprop="url" content="<?php if( !empty( $trendy_options['trendy-logo']['url'] ) ){
						echo esc_url( $trendy_options['trendy-logo']['url'] );
					} ?>">
				</span>
				<!-- publisher name -->
				<meta itemprop="name" content="<?php echo get_the_author(); ?>">
			</span>
			<!-- author info -->
			<span class="sr-only" itemprop="author" itemscope itemtype="http://schema.org/Person">
				<meta itemprop="name" content="<?php echo get_the_author_meta( 'display_name' ); ?>">
			</span>
							
			<div class="inner-block">
				<div class="container">
					<div class="row">
						<div class="col-lg-10">
							<ul class="category-label categories clearfix">
								<li><?php the_category( ' ' ); ?></li>
							</ul>
							<h1 itemprop="name headline" class="post-headline"><?php the_title(); ?></h1>
							<ul class="list-inline post-meta">
								<li><a class="post-author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php echo get_the_author_meta( 'display_name' ); ?>"><?php echo get_the_author_meta( 'display_name' ); ?></a></li>
								<li>
									<a href="<?php echo get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>" title="Published Date">
										<time datetime="<?php the_time( 'Y-m-d' ); ?>"><?php the_time( 'F j, Y' ); ?></time>
									</a>
									<!-- date published -->
									<meta itemprop="datePublished" content="<?php the_time( 'Y-m-d' ); ?>">
									<!-- date modified -->
									<meta itemprop="dateModified" content="<?php the_modified_time( 'Y-m-d' ); ?>">
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- post header END -->
		
		<?php if ( ! is_front_page() ) : get_template_part( '/templates/trendy', 'breadcrumb' ); endif; ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-8 post-content">
				
					<?php if ( $trendy_options['sharing-options'] ) : get_template_part( '/templates/share', 'before' ); endif; ?>
					
					<?php the_content(); ?>
					
					<?php
						$defaults = array(
							'before'           => '<p class="paginated-links">' . '<i class="ti-agenda"></i>',
							'after'            => '</p>',
							'link_before'      => '<span>',
							'link_after'       => '</span>',
							'next_or_number'   => 'number',
							'separator'        => ' ',
							'nextpagelink'     => esc_html__( 'Next page', 'trendy-pro' ),
							'previouspagelink' => esc_html__( 'Previous page', 'trendy-pro' ),
							'pagelink'         => '%',
							'echo'             => 1
						);
						
						wp_link_pages( $defaults );									
					?>
					
					<?php the_tags( '<ul class="category-label tags clearfix"><li><span>Tags</span></li><li>', '</li><li>', '</li></ul>' ); ?>
					
					<?php if ( ! $trendy_options['sharing-options'] ) : get_template_part( '/templates/share', 'after' ); endif; ?>
					
					<?php get_template_part( '/templates/previous', 'next' ); ?>
					
					<?php if ( $trendy_options['show-author-box'] ) : get_template_part( '/templates/author', 'box' ); endif; ?>
					
					<?php comments_template(); ?>
					
				</div>
				<div class="col-md-4">
					
					<?php get_sidebar(); ?>
					
				</div>
			</div>
		</div>			
	</div>
</article>
<!-- content with sidebar END -->