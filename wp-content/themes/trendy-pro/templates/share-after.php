<div class="meta-wrap clearfix after">
	<a href="<?php comments_link(); ?>" class="comments" title="<?php echo esc_attr__( 'Total Comments', 'trendy-pro' ); ?>"><span><?php echo get_comments_number(); ?></span> <i class="fa fa-comments-o"></i></a>
	<?php get_template_part( '/templates/trendy', 'likes' ); ?>
	<ul id="share-list" class="list-inline share-list clearfix">
		<li><a class="facebook" href="javascript:void(0);" title="<?php echo esc_attr__( 'Share on facebook', 'trendy-pro' ); ?>"><i class="fa fa-facebook"></i></a></li>
		<li><a class="twitter" href="javascript:void(0);" title="<?php echo esc_attr__( 'Share on twitter', 'trendy-pro' ); ?>"><i class="fa fa-twitter"></i></a></li>
		<li><a class="linkedin" href="javascript:void(0);" title="<?php echo esc_attr__( 'Share on linkedin', 'trendy-pro' ); ?>"><i class="fa fa-linkedin"></i></a></li>
		<li><a class="pinterest" href="javascript:void(0);" title="<?php echo esc_attr__( 'Share on pinterest', 'trendy-pro' ); ?>"><i class="fa fa-pinterest-p"></i></a></li>
		<li><a class="google-plus" href="javascript:void(0);" title="<?php echo esc_attr__( 'Share on google plus', 'trendy-pro' ); ?>"><i class="fa fa-google-plus"></i></a></li>
		<li><a class="reddit" href="javascript:void(0);" title="<?php echo esc_attr__( 'Share on reddit', 'trendy-pro' ); ?>"><i class="fa fa-reddit-alien"></i></a></li>
	</ul>
</div>