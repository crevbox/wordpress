<?php
	global $trendy_options;
	$filter_posts_by = $trendy_options['slider-post-option'];
	$featured_posts_count = $trendy_options['slider-post-count'];
	$slider_style = $trendy_options['slider-style'];
	$featured_posts_count = $featured_posts_count ? intval( $featured_posts_count ) : 5 ;
	
	switch ( $filter_posts_by ) {
		
		case "_post_like_count":			
			$args = array(
			  'post_type' => array( 'post' ),
			  'meta_key' => '_post_like_count',
			  'orderby' => 'meta_value_num',
			  'order' => 'DESC',
			  'posts_per_page' => $featured_posts_count,
			  'ignore_sticky_posts' => 1
			);			
			break;
			
		case "featured_posts":
			$args = array(
			  'post_type' => array( 'post' ),
			  'meta_key' => 'trendy_featured_post',
			  'orderby' => 'meta_value_num',
			  'order' => 'DESC',
			  'posts_per_page' => $featured_posts_count,
			  'ignore_sticky_posts' => 1
			);
			break;
			
		case "comment_count":
			$args = array(
			  'post_type' => array( 'post' ),
			  'orderby' => 'comment_count',
			  'order' => 'DESC',
			  'posts_per_page' => $featured_posts_count,
			  'ignore_sticky_posts' => 1
			);
			break;
			
		default:
			$args = array(
			  'post_type' => array( 'post' ),
			  'posts_per_page' => $featured_posts_count,
			  'ignore_sticky_posts' => 1
			);
	}
	
	$slider_query = new WP_Query( $args );
	
	
	if ( $slider_style == 'multi_slide' ) {
?>
	<div id="featured2" class="owl-carousel featured-carousel multi">
	<?php if( $slider_query->have_posts() ) : while( $slider_query->have_posts() ) : $slider_query->the_post(); ?>
	<?php
		global $post; $post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'trendy-slide' );
		$thumb_url = $post_thumb[0];
		$post_format = get_post_format();
	?>
		<?php if ( $post_format == 'video' ) : ?>
		<div class="featured-post dark-overlay">
			<?php
				$video_source = get_field( 'choose_video_source' );
				if( $video_source == 'youtube' ) {
					$video_id = esc_url( 'https://www.youtube.com/watch?v=' . get_field( 'youtube_video_id' ) );
				} elseif ( $video_source == 'vimeo' ) {
					$video_id = esc_url( 'https://vimeo.com/' . get_field( 'vimeo_video_id' ) );
				}
			?>
			<a class="owl-video" href="<?php echo esc_url( $video_id ); ?>"></a>
		<?php elseif ( $thumb_url ) : ?>
			<div class="featured-post dark-overlay" style="background-image:url(<?php echo esc_url( $thumb_url ); ?>);">
		<?php else : ?>
			<div class="featured-post dark-overlay">
		<?php endif; ?>
			
			<div class="post-extras">
				<?php get_template_part( '/templates/trendy', 'likes' ); ?>
			</div>
			<div class="inner-block">
				<ul class="category-label clearfix">
					<li><?php the_category( ' ' ); ?></li>
				</ul>
				<h2><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
				<ul class="list-inline post-meta">
					<li><a class="post-author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php echo get_the_author_meta( 'display_name' ); ?>"><?php echo get_the_author_meta( 'display_name' ); ?></a></li>
					<li><time datetime="<?php the_time( 'Y-m-d' ); ?>"><?php the_time( 'F j, Y' ); ?></time></li>
					<a href="<?php comments_link(); ?>" class="comments" title="<?php echo esc_attr__( 'Total Comments', 'trendy-pro' ); ?>"><span><?php echo get_comments_number(); ?></span> <i class="fa fa-comments-o"></i></a>
				</ul>
			</div>
		</div>
	<?php endwhile; endif; wp_reset_postdata(); ?>
	</div>
<?php } else { ?>
	<div id="featured1" class="owl-carousel featured-carousel home-slider">
	<?php if( $slider_query->have_posts() ) : while( $slider_query->have_posts() ) : $slider_query->the_post(); ?>
	<?php
		global $post; $post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'trendy-slide' );
		$thumb_url = $post_thumb[0];
		$post_format = get_post_format();
	?>
		<?php if ( $post_format == 'video' ) : ?>
		<div class="featured-post dark-overlay">
			<?php
				$video_source = get_field( 'choose_video_source' );
				if( $video_source == 'youtube' ) {
					$video_id = esc_url( 'https://www.youtube.com/watch?v=' . get_field( 'youtube_video_id' ) );
				} elseif ( $video_source == 'vimeo' ) {
					$video_id = esc_url( 'https://vimeo.com/' . get_field( 'vimeo_video_id' ) );
				}
			?>
			<a class="owl-video" href="<?php echo esc_url( $video_id ); ?>"></a>
		<?php elseif ( $thumb_url ) : ?>
			<div class="featured-post dark-overlay" style="background-image:url(<?php echo esc_url( $thumb_url ); ?>);">
		<?php else : ?>
			<div class="featured-post dark-overlay">
		<?php endif; ?>
			
			<div class="post-extras">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							
							<a href="<?php comments_link(); ?>" class="comments" title="<?php echo esc_attr__( 'Total Comments', 'trendy-pro' ); ?>"><span><?php echo get_comments_number(); ?></span> <i class="fa fa-comments-o"></i></a>
							<?php get_template_part( '/templates/trendy', 'likes' ); ?>
							
						</div>
					</div>
				</div>
			</div>
			<div class="inner-block">
				<div class="container">
					<div class="row">
						<div class="col-lg-10">
							<ul class="category-label clearfix">
								<li><?php the_category( ' ' ); ?></li>
							</ul>
							<h2><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
							<ul class="list-inline post-meta">
								<li><a class="post-author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php echo get_the_author_meta( 'display_name' ); ?>"><?php echo get_the_author_meta( 'display_name' ); ?></a></li>
								<li><time datetime="<?php the_time( 'Y-m-d' ); ?>"><?php the_time( 'F j, Y' ); ?></time></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; endif; wp_reset_postdata(); ?>
	</div>
<?php } ?>