<?php
	global $trendy_options;
	if( $trendy_options['top-nav-show'] ) :
	$social_nav = $trendy_options['top-social-nav'] ? TRUE : FALSE ;
?>
<!-- top navbar START -->
<div class="top-navbar <?php echo esc_attr( $social_nav ? 'has-social-nav' : 'no-social-nav' ); ?>">
	<div class="container">
		<div class="row">
			<div class="<?php echo esc_attr( $trendy_options['top-social-nav'] ? 'col-sm-6' : 'col-lg-12' ); ?> hidden-xs">
				<?php
					// show current date if enabled from theme options
					if( $trendy_options['trendy-date-tme'] ) : ?>
					<span class="date-time"><?php echo date('D, F d, Y'); ?></span>
				<?php
					endif;
					// top menu bar START
					if( $trendy_options['trendy-top-nav'] ) :
						$has_top_menu = (has_nav_menu('top_menu_bar')) ? 'true' : 'false';						
						if($has_top_menu == 'true') {
							$args = array(
								'theme_location' => 'top_menu_bar',
								'menu_class' => 'list-inline top-nav',
								'container'	 => 'false',
								'depth'		 => 1
							);
							wp_nav_menu( $args );
						}
					endif;
					// top menu bar END
				?>

			</div>
				<?php // social profiles' links
				if( $social_nav ) : ?>
				<div class="col-sm-6 clearfix">
					<?php get_template_part( '/templates/social', 'nav' ); ?>
				</div>
				<?php endif; ?>
		</div>
	</div>
</div>
<!-- top navbar END -->
<?php endif; ?>