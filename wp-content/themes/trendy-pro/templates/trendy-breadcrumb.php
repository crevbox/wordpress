<?php // Breadcrumbs
if( ! function_exists( 'trendy_breadcrumb' ) ){
	function trendy_breadcrumb() {
		
			global $post;
			
			if (!is_front_page()) {
				
				// if it is not home page display breadcrumb
				echo '<ol class="breadcrumb">';		
				echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="';
				echo esc_url( home_url( '/' ) );
				echo '" title="' . esc_attr__( 'Home', 'trendy-pro' ) . '" itemprop="url">';
				echo 'Home <meta itemprop="title" content="Home">';
				echo "</a></li>";	
					
				// if it is a custom page & it is a parent page (top level)
				if ( is_page() && $post->post_parent == 0 ) {
						global $wp;
						$current_url = get_home_url(add_query_arg(array(),$wp->request));					
						echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="';
						wp_title('');
						echo '"><meta itemprop="url" content="' . $current_url . '"><span>';
						wp_title('');
						echo '</span></li>';
						
				}		
				// if it is a custom page & it is a child page
				if ( is_page() && $post->post_parent > 0 ) {
						
						$parent = array_reverse(get_post_ancestors($post->ID));
						$first_parent = get_page($parent[0]);					
						echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="';
						echo get_page_link($parent[0]);
						echo '" title="'. $first_parent->post_name . '" itemprop="url">';
						echo esc_html( $first_parent->post_name );
						echo '<meta itemprop="title" content="' . $first_parent->post_name . '"></a></li>';
						global $wp;
						$current_url = get_home_url(add_query_arg(array(),$wp->request));					
						echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="';
						wp_title('');
						echo '"><meta itemprop="url" content="' . $current_url . '"><span>';
						wp_title('');
						echo '</span></li>';
						
				}
				
				// if it is blog listing page show blog page title
				if( is_home() ){
					
					echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span>';
					echo apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );
					echo "</span></li>";	
									
				}			
				// if it is a single blog post then show its category + current post title
				if( is_singular( 'post' ) ){
					
					$current_page_title = apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );
					if ( $current_page_title == 'Blog' ) :
					echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="';
					echo get_permalink( get_option( 'page_for_posts' ) );
					echo '" title="'. $current_page_title . '" itemprop="url">';
					echo esc_html( $current_page_title );
					echo '<meta itemprop="title" content="' . $current_page_title . '"></a></li>';
					endif;
					echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="';
					the_title();
					echo '"><span>';
					the_title();
					echo '</span></li>';
								
				}			
				// if it is a category page then show blog titile + category title
				if (is_category()) {
					
					$current_page_title = apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );
					if ( $current_page_title == 'Blog' ) :
					echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="';
					echo get_permalink( get_option( 'page_for_posts' ) );
					echo '" title="'. $current_page_title . '" itemprop="url">';
					echo esc_html( $current_page_title );
					echo '<meta itemprop="title" content="' . $current_page_title . '"></a></li>';
					endif;
					echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="';
					echo esc_attr( $archive_title = substr( get_the_archive_title(), 10 ) );
					global $wp;
					$current_url = get_home_url(add_query_arg(array(),$wp->request));
					echo '"><meta itemprop="url" content="' . $current_url . '"><span>';
					echo 'Category <strong class="text-uppercase">"' . $archive_title . '"</strong>';
					echo '</span></li>';
					
				}			
				// if it is an author page then show blog title + Contributing author's display name
				if (is_author()) {
					
					$current_page_title = apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );
					if ( $current_page_title == 'Blog' ) :
					echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="';
					echo get_permalink( get_option( 'page_for_posts' ) );
					echo '" title="'. $current_page_title . '" itemprop="url">';
					echo esc_html( $current_page_title );
					echo "</a></li>";
					endif;
					echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="';
					the_author();
					echo '" ';
					global $wp;
					$current_url = get_home_url(add_query_arg(array(),$wp->request));
					echo '"><meta itemprop="url" content="' . $current_url . '"><span>';
					echo 'All contributions by <strong class="text-uppercase">"';
					the_author();
					echo '"</strong></span></li>';
					
				}			
				// if it is a page that lists posts associated with a single tag, then show blog titile + tag name
				if (is_tag()) {
					
					$current_page_title = apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );
					if ( $current_page_title == 'Blog' ) :
					echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="';
					echo get_permalink( get_option( 'page_for_posts' ) );
					echo '" title="'. $current_page_title . '" itemprop="url">';
					echo esc_html( $current_page_title );
					echo '<meta itemprop="title" content="' . $current_page_title . '"></a></li>';
					endif;
					echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="';
					echo single_tag_title();
					global $wp;
					$current_url = get_home_url(add_query_arg(array(),$wp->request));
					echo '"><meta itemprop="url" content="' . $current_url . '"><span>';
					echo 'Tag <strong class="text-uppercase">"';
					echo single_tag_title();
					echo '"</strong></span></li>';
					
				}			
				// if it is a page that lists posts based on day, month or year, then show blog titile + date
				if (is_day() || is_month() || is_year()) {
					
					$current_page_title = apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );
					if ( $current_page_title == 'Blog' ) :
					echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="';
					echo get_permalink( get_option( 'page_for_posts' ) );
					echo '" title="'. $current_page_title . '" itemprop="url">';
					echo esc_html( $current_page_title );
					echo '<meta itemprop="title" content="' . $current_page_title . '"></a></li>';
					endif;
					//if it is a day archive
					if (is_day()){
						
						echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="Archive for ';
						the_time('F jS, Y');
						global $wp;
						$current_url = get_home_url(add_query_arg(array(),$wp->request));
						echo '"><meta itemprop="url" content="' . $current_url . '"><span>';
						echo 'Archive for <strong class="text-uppercase">"'; the_time('F jS, Y');
						echo '"</strong></span></li>';
						
					//if it is a month archive
					} elseif (is_month()){
						
						echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="Archive for ';
						the_time('F, Y');
						global $wp;
						$current_url = get_home_url(add_query_arg(array(),$wp->request));
						echo '"><meta itemprop="url" content="' . $current_url . '"><span>';
						echo 'Archive for <strong class="text-uppercase">"'; the_time('F, Y');
						echo '"</strong></span></li>';
						
					// if it is a year archive
					} elseif (is_year()){
						
						echo '<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="Archive for ';
						the_time('Y');
						global $wp;
						$current_url = get_home_url(add_query_arg(array(),$wp->request));
						echo '"><meta itemprop="url" content="' . $current_url . '"><span>';
						echo 'Archive for <strong class="text-uppercase">"'; the_time('Y');
						echo '"</strong></span></li>';
						
					}
					
				}				
			elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
			elseif (is_search()) {global $wp; $current_url = get_home_url(add_query_arg(array(),$wp->request)); echo'<li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><meta itemprop="title" content="Search Results"><meta itemprop="url" content="' . $current_url . '"><span>Results for "<span>' . esc_html( get_search_query( false ) ) . '</span>"</span></li>';}
			echo '</ol>';
			}
	}
}
?>

<div class="breadcrumb-wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php trendy_breadcrumb(); ?>
			</div>
		</div>
	</div>
</div>