<?php
global $trendy_options;
$post = get_page_by_path( $trendy_options['featured-slug'], OBJECT, 'post' );

if ( $post ) :
$post_title = get_the_title( $post->ID );
$post_thumbnail = get_the_post_thumbnail_url( $post->ID, 'trendy-mini-thumb' );
$post_link = get_the_permalink( $post->ID );
?>
<div class="site-author clearfix pull-right">
	<?php if( $post_thumbnail ) : ?>
	<a href="<?php echo esc_url( $post_link ); ?>" title="<?php echo esc_attr( $post_title ); ?>"><img class="pull-left" src="<?php echo esc_url( $post_thumbnail ); ?>" alt="<?php echo esc_attr( $post_title ); ?>"></a>
	<?php endif; ?>
	<h4 class="alt-font">Featured Article</h4>
	<p class="no-margin"><a href="<?php echo esc_url( $post_link ); ?>" title="<?php echo esc_attr( $post_title ); ?>"><?php echo esc_html( $post_title ); ?></a></p>
</div>
<?php endif; ?>