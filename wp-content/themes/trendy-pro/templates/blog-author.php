<div class="site-author clearfix">
	<?php
		global $trendy_options;
		$thumbnail_link = !empty( $trendy_options['author-thumbnail'] ) ? $trendy_options['author-thumbnail']['url'] : '';
		$author_name = !empty( $trendy_options['author-name'] ) ? $trendy_options['author-name'] : '';
		$author_info = !empty( $trendy_options['author-info'] ) ? $trendy_options['author-info'] : '';
		$profile_url = !empty( $trendy_options['profile-url'] ) ? $trendy_options['profile-url'] : 'javascript:void(0);';
	?>
	<a href="<?php echo esc_url( $profile_url ); ?>" title="<?php echo esc_attr( $author_name ); ?>"><img src="<?php echo esc_url( $thumbnail_link ); ?>" alt="<?php echo esc_attr( $author_name ); ?>" class="avatar pull-left"></a>
	<h4 class="alt-font"><?php echo esc_html( $author_name ); ?></h4>
	<p class="no-margin"><?php echo esc_html( $author_info ); ?></p>
</div>