<?php

if( ! function_exists( 'trendy_build_social_list' ) ){
	function trendy_build_social_list(){
		
		$social_list = '';
		global $trendy_options;
		
		$facebook_link = $twitter_link = $linkedin_link = $pinterest_link = $google_plus_link = $youtube_link = $tumblr_link = $instagram_link = $reddit_link = $flickr_link = $dribbble_link = $vimeo_link = $soundcloud_link = $vk_link = $behance_link = $github_link = '';
		
		$total_links = 0;
		
		if( !empty( $trendy_options['facebook'] ) ){
			$facebook_link = $trendy_options['facebook'];
			$total_links++;
		}
		if( !empty( $trendy_options['twitter'] ) ){
			$twitter_link = $trendy_options['twitter'];
			$total_links++;
		}
		if( !empty( $trendy_options['linkedin'] ) ){
			$linkedin_link = $trendy_options['linkedin'];
			$total_links++;
		}
		if( !empty( $trendy_options['pinterest'] ) ){
			$pinterest_link = $trendy_options['pinterest'];
			$total_links++;
		}
		if( !empty( $trendy_options['google-plus'] ) ){
			$google_plus_link = $trendy_options['google-plus'];
			$total_links++;
		}
		if( !empty( $trendy_options['youtube'] ) ){
			$youtube_link = $trendy_options['youtube'];
			$total_links++;
		}
		if( !empty( $trendy_options['tumblr'] ) ){
			$tumblr_link = $trendy_options['tumblr'];
			$total_links++;
		}
		if( !empty( $trendy_options['instagram'] ) ){
			$instagram_link = $trendy_options['instagram'];
			$total_links++;
		}
		if( !empty( $trendy_options['reddit'] ) ){
			$reddit_link = $trendy_options['reddit'];
			$total_links++;
		}
		if( !empty( $trendy_options['flickr'] ) ){
			$flickr_link = $trendy_options['flickr'];
			$total_links++;
		}
		if( !empty( $trendy_options['dribbble'] ) ){
			$dribbble_link = $trendy_options['dribbble'];
			$total_links++;
		}	
		if( !empty( $trendy_options['vimeo'] ) ){
			$vimeo_link = $trendy_options['vimeo'];
			$total_links++;
		}
		if( !empty( $trendy_options['soundcloud'] ) ){
			$soundcloud_link = $trendy_options['soundcloud'];
			$total_links++;
		}
		if( !empty( $trendy_options['vk'] ) ){
			$vk_link = $trendy_options['vk'];
			$total_links++;
		}
		if( !empty( $trendy_options['behance'] ) ){
			$behance_link = $trendy_options['behance'];
			$total_links++;
		}
		if( !empty( $trendy_options['github'] ) ){
			$github_link = $trendy_options['github'];
			$total_links++;
		}
		
		
		if( $total_links ){
			
			$social_list = '<ul class="social-nav col' . $total_links . ' clearfix">';
			
			//if facebook link is available
			if( $facebook_link ){
				$social_list .= '<li><a class="facebook" href="' . esc_url( $facebook_link ) . '" title="' . esc_attr__( 'Facebook', 'trendy-pro' ) . '"><i class="fa fa-facebook"></i></a></li>';
			}

			//if twitter link is available
			if( $twitter_link ){
				$social_list .= '<li><a class="twitter" href="' . esc_url( $twitter_link ) . '" title="' . esc_attr__( 'Twitter', 'trendy-pro' ) . '"><i class="fa fa-twitter"></i></a></li>';
			}
			
			//if linkedin link is available
			if( $linkedin_link ){
				$social_list .= '<li><a class="linkedin" href="' . esc_url( $linkedin_link ) . '" title="' . esc_attr__( 'Linkedin', 'trendy-pro' ) . '"><i class="fa fa-linkedin"></i></a></li>';
			}
			
			//if pinterest link is available
			if( $pinterest_link ){
				$social_list .= '<li><a class="pinterest" href="' . esc_url( $pinterest_link ) . '" title="' . esc_attr__( 'Pinterest', 'trendy-pro' ) . '"><i class="fa fa-pinterest-p"></i></a></li>';
			}
			
			//if google-plus link is available
			if( $google_plus_link ){
				$social_list .= '<li><a class="google-plus" href="' . esc_url( $google_plus_link ) . '" title="' . esc_attr__( 'Google Plus', 'trendy-pro' ) . '"><i class="fa fa-google-plus"></i></a></li>';
			}
			
			//if youtube link is available
			if( $youtube_link ){
				$social_list .= '<li><a class="youtube" href="' . esc_url( $youtube_link ) . '" title="' . esc_attr__( 'YouTube', 'trendy-pro' ) . '"><i class="fa fa-youtube-play"></i></a></li>';
			}
			
			//if tumblr link is available
			if( $tumblr_link ){
				$social_list .= '<li><a class="tumblr" href="' . esc_url( $tumblr_link ) . '" title="' . esc_attr__( 'Tumblr', 'trendy-pro' ) . '"><i class="fa fa-tumblr"></i></a></li>';
			}
			
			//if instagram link is available
			if( $instagram_link ){
				$social_list .= '<li><a class="instagram" href="' . esc_url( $instagram_link ) . '" title="' . esc_attr__( 'Instagram', 'trendy-pro' ) . '"><i class="fa fa-instagram"></i></a></li>';
			}
			
			//if reddit link is available
			if( $reddit_link ){
				$social_list .= '<li><a class="reddit" href="' . esc_url( $reddit_link ) . '" title="' . esc_attr__( 'Reddit', 'trendy-pro' ) . '"><i class="fa fa-reddit-alien"></i></a></li>';
			}
			
			//if flickr link is available
			if( $flickr_link ){
				$social_list .= '<li><a class="flickr" href="' . esc_url( $flickr_link ) . '" title="' . esc_attr__( 'Flickr', 'trendy-pro' ) . '"><i class="fa fa-flickr"></i></a></li>';
			}
			
			//if dribbble link is available
			if( $dribbble_link ){
				$social_list .= '<li><a class="dribbble" href="' . esc_url( $dribbble_link ) . '" title="' . esc_attr__( 'Dribbble', 'trendy-pro' ) . '"><i class="fa fa-dribbble"></i></a></li>';
			}
			
			//if vimeo link is available
			if( $vimeo_link ){
				$social_list .= '<li><a class="vimeo" href="' . esc_url( $vimeo_link ) . '" title="' . esc_attr__( 'Vimeo', 'trendy-pro' ) . '"><i class="fa fa-vimeo"></i></a></li>';
			}
			
			//if Soundcloud link is available
			if( $soundcloud_link ){
				$social_list .= '<li><a class="soundcloud" href="' . esc_url( $soundcloud_link ) . '" title="' . esc_attr__( 'Soundcloud', 'trendy-pro' ) . '"><i class="fa fa-soundcloud"></i></a></li>';
			}
			
			//if vk link is available
			if( $vk_link ){
				$social_list .= '<li><a class="vk" href="' . esc_url( $vk_link ) . '" title="' . esc_attr__( 'vk', 'trendy-pro' ) . '"><i class="fa fa-vk"></i></a></li>';
			}
			
			//if behance link is available
			if( $behance_link ){
				$social_list .= '<li><a class="behance" href="' . esc_url( $behance_link ) . '" title="' . esc_attr__( 'Behance', 'trendy-pro' ) . '"><i class="fa fa-behance"></i></a></li>';
			}
			
			//if github link is available
			if( $github_link ){
				$social_list .= '<li><a class="github" href="' . esc_url( $github_link ) . '" title="' . esc_attr__( 'GitHub', 'trendy-pro' ) . '"><i class="fa fa-github"></i></a></li>';
			}
			
			$social_list .= '</ul>';
			
		}
		
		return $social_list;
	}
}

echo trendy_build_social_list();

?>