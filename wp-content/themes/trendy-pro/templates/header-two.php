<?php global $trendy_options; $hasad = 'no-header-ad'; ?>
<!-- page header START -->
<?php if ( !empty( $trendy_options['header-ad-image']['url'] ) ) : $hasad = 'has-header-ad'; endif; ?>
<div class="logo-wrap style2 <?php echo esc_attr( $hasad ); ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo" title="<?php bloginfo('name'); ?>"><?php if( !empty( $trendy_options['trendy-logo']['url'] ) ) : ?><?php echo '<img src="' . esc_url( $trendy_options['trendy-logo']['url'] ) . '"'; ?> alt="<?php bloginfo('name'); ?>"><?php else : ?><span><?php bloginfo('name'); ?></span><?php endif; ?></a>
			</div>
			<div class="col-lg-8">
			
				<?php
					//header banner ad
					if( $trendy_options['header-ad'] && !empty( $trendy_options['header-ad-image']['url'] ) ) {
						echo '<div class="header-ad"><a href="' . esc_url( $trendy_options['header-ad-url'] ) . '" title="' . esc_attr__( 'Sponser', 'trendy-pro' ) . '"><img src="' . esc_url( $trendy_options['header-ad-image']['url'] ) . '" alt="' . esc_attr__( 'Sponser', 'trendy-pro' ) . '"></a></div>';
					}
				?>
				
			</div>
		</div>
	</div>
</div>
<!-- page header END -->