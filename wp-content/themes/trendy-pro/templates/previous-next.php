<?php
$prev_post_link = get_previous_post_link();
$next_post_link = get_next_post_link();
if ( $next_post_link || $prev_post_link ) : ?>
<div class="prev-next clearfix">
	<?php if ( $prev_post_link ) : ?>
	<div class="prev-article">
		<small>Previous Article</small>
		<h5 class="h4"><?php echo get_previous_post_link(); ?></h5>
	</div>
	<?php endif; if ( $next_post_link ) : ?>
	<div class="next-article">
		<small>Next Article</small>
		<h5 class="h4"><?php echo get_next_post_link(); ?></h5>
	</div>
	<?php endif; ?>
</div>
<?php endif; ?>