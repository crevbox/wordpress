<div class="author-box">
	<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php echo get_the_author_meta( 'display_name' ); ?>">
		<?php if ( get_avatar( get_the_author_meta('ID') ) ) : echo get_avatar( get_the_author_meta('ID'), 100 ); endif; ?>
	</a>
	<div class="desc">
		<h5 class="vcard author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author_meta( 'display_name' ); ?></a></h5>
		<div class="author-url">
			<?php $curauth = get_userdata( get_the_author_meta('ID') ); ?>
			<a href="<?php echo esc_url( $curauth->user_url ); ?>"><?php echo esc_url( $curauth->user_url ); ?></a>
		</div>
		<div class="td-author-description"><?php the_author_meta( 'description' ); ?></div>
	</div>
</div>