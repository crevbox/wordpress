<?php
	global $trendy_options;
	$copyright_msg = '';
	$copyright_msg .= esc_html__( 'Copyright &copy; ', 'trendy-pro' );
	if( !empty( $trendy_options['trendy-copyright'] ) ){
		$copyright_msg .= $trendy_options['trendy-copyright'];
	} else {
		$copyright_msg .= get_bloginfo('name');
		$copyright_msg .= ' ';
		$copyright_msg .= date('Y');
		$copyright_msg .= esc_html__( '. All rights reserved.', 'trendy-pro' );
	}							
?>
<div class="copyright"><?php echo esc_html( $copyright_msg ); ?></div>