<?php
	global $trendy_options;
	if ( $trendy_options['show-likes'] ) :
?>
<?php echo get_simple_likes_button( get_the_ID() ); ?>
<?php endif; ?>