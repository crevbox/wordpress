<!-- page header START -->
<div class="logo-wrap style1 no-header-ad">
	<div class="container">
		<div class="row">
			<div class="col-md-4 hidden-sm">
			
				<?php
					global $trendy_options;
					// blog author info
					if( $trendy_options['show-author'] ) { get_template_part( '/templates/blog', 'author' );	}
				?>
				
			</div>
			<div class="col-md-4 text-center">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo" title="<?php bloginfo('name'); ?>"><?php if( !empty( $trendy_options['trendy-logo']['url'] ) ) : ?><?php echo '<img src="' . esc_url( $trendy_options['trendy-logo']['url'] ) . '"'; ?> alt="<?php bloginfo('name'); ?>"><?php else : ?><span><?php bloginfo('name'); ?></span><?php endif; ?></a>
			</div>
			<div class="col-md-4 hidden-sm">
			
				<?php
					// featured article
					if( $trendy_options['show-featured'] ) { get_template_part( '/templates/featured', 'article' );	}
				?>
				
			</div>
		</div>
	</div>
</div>
<!-- page header END -->