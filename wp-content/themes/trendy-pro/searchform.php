<form class="search-form" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label class="sr-only"><?php echo esc_html__( 'Search for:', 'trendy-pro' ); ?></label>
	<input class="search-field" type="text" value="" name="s" placeholder="<?php echo esc_attr__( 'Start Typing...', 'trendy-pro' ); ?>" />
</form>