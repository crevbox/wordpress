<?php
	// Template Name: Without Sidebar
?>
<?php get_header(); ?>
	
	<!-- body wrapper START -->
	<div class="body-wrapper no-sidebar">
		
		<!-- content with sidebar START -->
		<section class="section">
			<?php if( have_posts() ) : while( have_posts()  ) : the_post(); ?>
			<div class="inner-banner parallax dark-overlay" style="background-image:url(<?php the_post_thumbnail_url( 'trendy-slide' ); ?>);">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
			<?php get_template_part( '/templates/trendy', 'breadcrumb' ); ?>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 content-area">
						<?php the_content(); ?>
						<?php if( comments_open( $post->ID ) && !is_front_page() ) : ?>
							<?php comments_template(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endwhile; endif; ?>
		</section>
		<!-- content with sidebar END -->
		
	</div>
	<!-- body wrapper END -->
	
<?php get_footer(); ?>