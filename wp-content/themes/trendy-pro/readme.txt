Theme Name: Trendy Pro
Theme URI: http://themeforest.net/user/uipro/portfolio
Author: uipro
Author URI: http://uipro.net/
Description: Trendy Pro is a premium, clear and easy to use personal blog WordPress theme for professional bloggers.
Version: 1.0.0
License: Themeforest Standard Licenses
License URI: http://themeforest.net/licenses/standard


DOCUMENTATION
-------------
For instructions on how to setup theme and information about theme features, please see documentation folder available in downloaded (.zip) file.