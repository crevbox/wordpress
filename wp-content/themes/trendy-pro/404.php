<?php
	// Template Name: Without Sidebar
?>
<?php get_header(); ?>
	
	<!-- body wrapper START -->
	<div class="body-wrapper not-found">
		
		<!-- content with sidebar START -->
		<section class="section text-center">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h1><?php echo esc_html__( '404', 'trendy-pro' ); ?> <small><?php echo esc_html__( 'Error, page not found!', 'trendy-pro' ); ?></small></h1>
						<p><?php echo esc_html__( 'Why not try a search, or go back to homepage.', 'trendy-pro' ); ?></p>
						<?php get_search_form( $echo = true ); ?>
						<a class="btn btn-primary" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html__( 'Go To Homepage', 'trendy-pro' ); ?></a>
					</div>
				</div>
			</div>
		</section>
		<!-- content with sidebar END -->
		
	</div>
	<!-- body wrapper END -->
	
<?php get_footer(); ?>