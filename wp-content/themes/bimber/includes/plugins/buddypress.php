<?php
/**
 * BuddyPress plugin functions
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Move Snax tabs to the beginning of profile tabs.
 *
 * @param array  $main_nav      Navigation config.
 * @param string $id            Component id.
 *
 * @return array
 */
function bimber_snax_bp_component_main_nav( $main_nav, $id ) {
	if ( ! bimber_can_use_plugin( 'snax/snax.php' ) ) {
		return $main_nav;
	}

	$lists_component_id = snax_posts_bp_component_id();
	$items_component_id = snax_items_bp_component_id();
	$votes_component_id = snax_votes_bp_component_id();

	if ( $lists_component_id === $id ) {
		$main_nav['position'] = 4;
	}

	if ( $items_component_id === $id ) {
		$main_nav['position'] = 6;
	}

	if ( $votes_component_id === $id ) {
		$main_nav['position'] = 8;
	}

	return $main_nav;
}

/**
 * Adjust the markup of a directory (groups, members) search form
 *
 * @param string $html HTML markup.
 *
 * @return string
 */
function bimber_bp_directory_search_form( $html ) {
	$html = str_replace( '<input type="submit"', '<input class="g1-button g1-button-simple" type="submit"', $html );
	return $html;
}

function bimber_bp_member_add_button_class_filters() {
	add_filter( 'bp_get_add_friend_button', 			'bimber_bp_get_solid_button' );
}

function bimber_bp_member_remove_button_class_filters() {
	remove_filter( 'bp_get_add_friend_button', 			'bimber_bp_get_solid_button' );
}

function bimber_bp_group_add_button_class_filters() {
	add_filter( 'bp_get_group_join_button', 			'bimber_bp_get_solid_button' );
}

function bimber_bp_group_remove_button_class_filters() {
	remove_filter( 'bp_get_group_join_button', 			'bimber_bp_get_solid_button' );
}

/**
 * Adjust BuddyPress button classes.
 */
function bimber_bp_get_simple_xs_button( $button ) {
	if ( ! isset( $button['g1'] ) ) {
		$button['link_class'] .= ' g1-button g1-button-xs g1-button-simple';

		// Add our special key for tracking purposes.
		$button['g1'] = true;
	}

	return $button;
}

function bimber_bp_get_solid_button( $button ) {
	$button['link_class'] .= ' g1-button g1-button-m g1-button-simple';
	// Add our special key for tracking purposes
	$button['g1'] = true;

	return $button;
}

/**
 * Render dynamic CSS for the #header-cover-image
 *
 * @param array $params Parameters.
 *
 * @return string
 */
function bimber_cover_image_callback( $params = array() ) {
	if ( empty( $params ) ) {
		return;
	}

	return '
        #buddypress #header-cover-image {
            height: ' . absint( $params['height'] ) . 'px;
            background-image: url(' . esc_url( $params['cover_image'] ) . ');
        }
    ';
}

function bimber_cover_image_css( $settings = array() ) {
	/**
	 * If you are using a child theme, use bp-child-css
	 * as the theme handle
	 */
	$settings['theme_handle'] = 'bp-parent-css';

	// Adjust size
	$settings['height'] = 300;

	$settings['callback'] = 'bimber_cover_image_callback';

	return $settings;
}

function bimber_render_markup_before_list_items_loop() {
	echo '<div class="g1-indent">';
}

function bimber_render_markup_after_list_items_loop() {
	echo '</div>';
}

/**
 * Return current user profile url
 *
 * @param string $link          Author posts link.
 * @param int 	 $author_id		Author id.
 *
 * @return string
 */
function bimber_bp_get_author_link( $link, $author_id ) {
	$link = bp_core_get_user_domain( $author_id );
	$link = trailingslashit( $link . bp_get_profile_slug() );

	return $link;
}

/** PROFILE ************/


/**
 * Whether or not to show the "Change Cover Image" link
 *
 * @return bool
 */
function bimber_bp_show_cover_image_change_link() {
	$show = bp_core_can_edit_settings() && bp_displayed_user_use_cover_image_header();

	return apply_filters( 'bimber_bp_show_cover_image_change_link', $show );
}

/**
 * Render the "Change Cover Image" link
 */
function bimber_bp_render_cover_image_change_link() {
	$link = bp_get_members_component_link( 'profile', 'change-cover-image' );

	?>
	<a class="g1-bp-change-image" href="<?php echo esc_url( $link ); ?>" title="<?php  esc_attr_e( 'Change Cover Image', 'buddypress' ); ?>"><?php esc_html_e( 'Change Cover Image', 'buddypress' ); ?></a>
	<?php
}

/**
 * Whether or not to show the "Change Profile Photo" link
 *
 * @return bool
 */
function bimber_bp_show_profile_photo_change_link() {
	$show = bp_core_can_edit_settings() && buddypress()->avatar->show_avatars;

	return apply_filters( 'bimber_bp_show_profile_photo_change_link', $show );
}

/**
 * Render the "Change Profile Photo" link
 */
function bimber_bp_render_profile_photo_change_link() {
	$link = bp_get_members_component_link( 'profile', 'change-avatar' );

	?>
	<a class="g1-bp-change-avatar" href="<?php echo esc_url( $link ); ?>" title="<?php esc_attr_e( 'Change Profile Photo', 'buddypress' ); ?>"><?php esc_html_e( 'Change Profile Photo', 'buddypress' ); ?></a>
	<?php
}


/** GROUP ************/


/**
 * Whether or not to show the "Change Cover Image" link
 *
 * @return bool
 */
function bimber_bp_show_group_cover_image_change_link() {
	$show = bp_core_can_edit_settings() && bp_group_use_cover_image_header();

	return apply_filters( 'bimber_bp_show_group_cover_image_change_link', $show );
}

/**
 * Render the "Change Cover Image" link
 */
function bimber_bp_render_group_cover_image_change_link() {
	$group_link = bp_get_group_permalink();
	$admin_link = trailingslashit( $group_link . 'admin' );
	$link = trailingslashit( $admin_link . 'group-cover-image' );

	?>
	<a class="g1-bp-change-image" href="<?php echo esc_url( $link ); ?>" title="<?php  esc_attr_e( 'Change Cover Image', 'buddypress' ); ?>"><?php esc_html_e( 'Change Cover Image', 'buddypress' ); ?></a>
	<?php
}

/**
 * Whether or not to show the "Change Profile Photo" link
 *
 * @return bool
 */
function bimber_bp_show_group_photo_change_link() {
	$show = bp_core_can_edit_settings() && ! bp_disable_group_avatar_uploads() && buddypress()->avatar->show_avatars;

	return apply_filters( 'bimber_bp_show_group_photo_change_link', $show );
}

/**
 * Render the "Change Profile Photo" link
 */
function bimber_bp_render_group_photo_change_link() {
	$group_link = bp_get_group_permalink();
	$admin_link = trailingslashit( $group_link . 'admin' );
	$link = trailingslashit( $admin_link . 'group-avatar' );

	?>
	<a class="g1-bp-change-avatar" href="<?php echo esc_url( $link ); ?>" title="<?php esc_attr_e( 'Change Group Photo', 'buddypress' ); ?>"><?php esc_html_e( 'Change Group Photo', 'buddypress' ); ?></a>
	<?php
}

/**
 * Override default page template for BuddyPress pages.
 *
 * @param str $template  Template to load.
 * @return str
 */
function bimber_bp_load_no_sidebar_page_template( $template ) {

	if ( 'standard' === bimber_get_theme_option( 'bp', 'enable_sidebar' ) ){
		return $template;
	}

	if ( is_buddypress() && strpos( $template, 'page.php' )) {
		$template = str_replace( 'page.php', 'g1-template-page-full.php', $template );
	}
	if ( is_buddypress() && strpos( $template, 'index-directory.php' )) {
		$template = str_replace( 'index-directory.php', 'index-directory-full.php', $template );
	}

	return $template;
}
