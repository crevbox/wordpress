<?php
/**
 * BuddyPress - Users Cover Image Header
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<div class="g1-column g1-column-2of3 bp-layout-standard" id="cover-image-container">
	<a id="header-cover-image" href="<?php bp_displayed_user_link(); ?>"></a>

			<?php if ( bimber_bp_show_cover_image_change_link() ) : ?>
				<?php bimber_bp_render_cover_image_change_link(); ?>
			<?php endif; ?>

	<div id="item-header-cover-image">
	</div><!-- #item-header-cover-image -->

</div><!-- #cover-image-container -->
