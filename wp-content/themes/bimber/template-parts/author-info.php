<?php
/**
 * The Template Part for displaying "About Author" box.
 *
 * For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme 4.10
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}
?>
<?php if ( post_type_supports( get_post_type(), 'author' ) && get_the_author_meta( 'description' ) ) : ?>
	<section class="g1-row author-info" itemscope="" itemtype="http://schema.org/Person">
		<div class="g1-row-inner author-info-inner">
			<div class="g1-column g1-column-1of2 author-overview">
				<header class="author-title">
					<h2 class="g1-delta g1-delta-1st"><?php 
					global $allowedposttags;
					$bimber_allowedposttags = $allowedposttags;
					$bimber_allowedposttags['span']['itemprop'] = true;
					printf( wp_kses( __( 'Written by <a href="%s"><span itemprop="name" >%s</span></a>', 'bimber' ), $bimber_allowedposttags ), esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), get_the_author() ); ?></h2>
				</header>

				<figure class="author-avatar">
					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
						<?php echo get_avatar( get_the_author_meta( 'email' ), 70 ); ?>
					</a>
				</figure>
			</div>

			<div class="g1-column g1-column-1of2">
				<div itemprop="description" class="author-bio">
					<?php echo wp_kses_post( wpautop( get_the_author_meta( 'description' ) ) ); ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
