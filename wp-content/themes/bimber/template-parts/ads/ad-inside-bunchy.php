<?php
/**
 * The Template for displaying ad inside collection (list).
 *
 * @package Bimber_Theme 5.0
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}
?>
<li class="g1-collection-item">
	<?php if ( bimber_can_use_plugin( 'quick-adsense-reloaded/quick-adsense-reloaded.php' ) ) : ?>

		<?php if ( quads_has_ad( 'bimber_inside_stream' ) ) : ?>

			<div class="g1-advertisement g1-advertisement-inside-stream">

				<?php quads_ad( array( 'location' => 'bimber_inside_stream' ) ); ?>

			</div>

		<?php else : ?>

			<?php get_template_part( 'template-parts/ads/notice-not-allowed' ); ?>

		<?php endif; ?>

	<?php else : ?>

		<?php get_template_part( 'template-parts/ads/notice-plugin-required' ); ?>

	<?php endif; ?>
</li>
