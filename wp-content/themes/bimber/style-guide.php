<?php
/**
 * Style guide
 * This is a template part. It must be used within The Loop.
 *
 * @package Bimber_Theme 5.0
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

get_header();
?>

	<div class="g1-row g1-row-layout-page g1-row-padding-m">
		<div class="g1-row-background">
		</div>
		<div class="g1-row-inner">

			<div class="g1-column" id="primary">
				<div id="content" role="main">

				<article <?php post_class( 'entry-tpl-classic' ); ?>>
					<div class="g1-content-narrow g1-typography-xl entry-content" itemprop="articleBody">

<p><span class="g1-button-solid g1-button g1-button-xs">  solid xs</span></p>
<p><span class="g1-button-solid g1-button g1-button-s">   solid s</span></p>
<p><span class="g1-button-solid g1-button g1-button-m">   solid m</span></p>
<p><span class="g1-button-solid g1-button g1-button-l">   solid l</span></p>
<p><span class="g1-button-solid g1-button g1-button-xl">  solid xl</span></p>

<p><span class="g1-button-wide g1-button-solid g1-button g1-button-xs">  wide solid xs</span></p>
<p><span class="g1-button-wide g1-button-solid g1-button g1-button-s">   wide solid s</span></p>
<p><span class="g1-button-wide g1-button-solid g1-button g1-button-m">   wide solid m</span></p>
<p><span class="g1-button-wide g1-button-solid g1-button g1-button-l">   wide solid l</span></p>
<p><span class="g1-button-wide g1-button-solid g1-button g1-button-xl">  wide solid xl</span></p>

<p><span class="g1-button-simple g1-button g1-button-xs">  simple xs</span></p>
<p><span class="g1-button-simple g1-button g1-button-s">   simple s</span></p>
<p><span class="g1-button-simple g1-button g1-button-m">   simple m</span></p>
<p><span class="g1-button-simple g1-button g1-button-l">   simple l</span></p>
<p><span class="g1-button-simple g1-button g1-button-xl">  simple xl</span></p>

<p><span class="g1-button-wide g1-button-simple g1-button g1-button-xs">  wide simple xs</span></p>
<p><span class="g1-button-wide g1-button-simple g1-button g1-button-s">   wide simple s</span></p>
<p><span class="g1-button-wide g1-button-simple g1-button g1-button-m">   wide simple m</span></p>
<p><span class="g1-button-wide g1-button-simple g1-button g1-button-l">   wide simple l</span></p>
<p><span class="g1-button-wide g1-button-simple g1-button g1-button-xl">  wide simple xl</span></p>

<p><span class="g1-button-subtle g1-button g1-button-xs">  subtle xs</span></p>
<p><span class="g1-button-subtle g1-button g1-button-s">   subtle s</span></p>
<p><span class="g1-button-subtle g1-button g1-button-m">   subtle m</span></p>
<p><span class="g1-button-subtle g1-button g1-button-l">   subtle l</span></p>
<p><span class="g1-button-subtle g1-button g1-button-xl">  subtle xl</span></p>

<p><span class="g1-button-wide g1-button-subtle g1-button g1-button-xs">  wide subtle xs</span></p>
<p><span class="g1-button-wide g1-button-subtle g1-button g1-button-s">   wide subtle s</span></p>
<p><span class="g1-button-wide g1-button-subtle g1-button g1-button-m">   wide subtle m</span></p>
<p><span class="g1-button-wide g1-button-subtle g1-button g1-button-l">   wide subtle l</span></p>
<p><span class="g1-button-wide g1-button-subtle g1-button g1-button-xl">  wide subtle xl</span></p>


					</div>
				</article>

				</div><!-- #content -->
			</div><!-- #primary -->
		</div>
	</div><!-- .g1-row -->


<?php get_footer();