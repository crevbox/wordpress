<?php 

/*
*   This is a Child Theme for Trendy Pro WordPress Theme
*   -----------------------------------------------------
*	Thanks for using our Trendy Pro WordPress Theme
*	Our Portfolio: http://themeforest.net/user/uipro/portfolio
*/

function trendy_child_enqueue_styles() {
    $parent_style = 'parent-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ) );
}
add_action( 'wp_enqueue_scripts', 'trendy_child_enqueue_styles', PHP_INT_MAX );


/* ------------------------------------------------
   *  add your custom functionality here
--------------------------------------------------- */





?>