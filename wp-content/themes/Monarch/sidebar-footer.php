<?php
/**
 * The footer widget area is triggered if any of the areas
 * have widgets.
 *
 * If none of the sidebars have widgets, bail early.
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 2.0
 */

if ( ! is_active_sidebar( 'first-footer-widget-area'  )
  && ! is_active_sidebar( 'second-footer-widget-area' )
  && ! is_active_sidebar( 'third-footer-widget-area'  )
  && ! is_active_sidebar( 'fourth-footer-widget-area' )
)
return; ?>

<div class="footer-line-one" id="footer-widget-area" role="complementary">

  <div class="container widget-panel">
    <div class="row">

      <?php if ( is_active_sidebar( 'first-footer-widget-area' ) ) : ?>
        <div id="first" class="col-md-3">
          <?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
        </div>
      <?php endif; ?><!-- #first .widget-area -->

      <?php if ( is_active_sidebar( 'second-footer-widget-area' ) ) : ?>
        <div id="second" class="col-md-3">
          <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
        </div>
      <?php endif; ?><!-- #second .widget-area -->

      <?php if ( is_active_sidebar( 'third-footer-widget-area' ) ) : ?>
        <div id="third" class="col-md-3">
          <?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
        </div>
      <?php endif; ?><!-- #third .widget-area -->

      <?php if ( is_active_sidebar( 'fourth-footer-widget-area' ) ) : ?>
        <div id="fourth" class="col-md-3">
          <?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
        </div>
      <?php endif; ?><!-- #fourth .widget-area -->

    </div>
  </div>

</div>