<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.0
 */

?>

<!-- Modal Search -->
<div class="modal fade modal-theme" id="modal-search" tabindex="-1" role="dialog" aria-labelledby="modal-theme-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-theme-label"><strong><?php echo get_theme_mod( 'monarch_translation_text_1', 'What you are looking for?' ); ?></strong></h4>
      </div>

      <div class="modal-body">

        <div class="search-box">

          <div class="icon-box ion-home">
              <!-- Site Search -->
              <?php get_search_form(); ?>
          </div>

          <!-- If BuddyPress Members Component activated  -->
          <?php if( function_exists( 'is_buddypress' ) && bp_is_active( 'members' ) ) : ?>
            <div class="icon-box ion-person-stalker">
                <div id="members-dir-search" class="dir-search" role="search">
                  <?php bp_directory_members_search_form(); ?>
                </div><!-- #members-dir-search -->
            </div>
          <?php endif; ?>

          <!-- If BuddyPress Groups Component activated -->
          <?php if( function_exists( 'is_buddypress' ) && bp_is_active( 'groups' ) ) : ?>
            <div class="icon-box ion-folder">
                <div id="group-dir-search" class="dir-search" role="search">
                  <?php bp_directory_groups_search_form(); ?>
                </div><!-- #group-dir-search -->
            </div>
          <?php endif; ?>

          <!-- If bbPress activated -->
          <?php if( class_exists('bbPress') ) : ?>
            <div class="icon-box ion-chatbox-working">
                <?php bbp_get_template_part( 'form', 'search' ); ?>
            </div>
          <?php endif; ?>

        </div>

      </div>

    </div>
  </div>
</div>

<!-- Modal Login -->
<?php if ( !is_user_logged_in() ) : ?>
<div class="modal fade modal-theme" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-theme-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-theme-label"><strong><?php echo get_theme_mod( 'monarch_translation_text_2', 'Login' ); ?></strong></h4>
      </div>

      <div class="modal-body">
        <div class="loginform-action">
          <?php do_action( 'login_form' ); ?>
        </div>
        <?php wp_login_form(); ?>
      </div>

      <div class="modal-footer">
        <p>
          <?php login_footer_links(); ?>
        </p>
      </div>
      
    </div>
  </div>
</div>
<?php endif; ?>

</div><!-- Wrapper -->

<?php if ( get_theme_mod( 'monarch_css_grid', 'social' ) == 'blog' ) : ?>
  <?php get_template_part( 'footer-sidebar' ); ?>
<?php endif; ?>

</div><!-- Body Background -->

<?php wp_footer(); ?>
</body>
</html>