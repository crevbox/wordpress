<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 2.0
 */

?>

<?php get_sidebar( 'footer' ); ?>

<div class="footer-line-two">
  <div class="container">
    <div class="line social">

      <!-- WordPress Copyright -->
      <div class="copyright">
        <?php
          /**
           * Fires before the monarch footer text for footer customization.
           *
           * @since Monarch 1.0
           */
          do_action( 'monarch_credits' );
          ?>
        <?php
          /**
           * Footer copyright text
           *
           * @since Monarch 1.0
           */
           echo get_theme_mod( 'monarch_footer_copyright', 'Proudly powered by <strong>WordPress</strong>' );
          ?>
      </div>

      <!-- Social Links Menu -->
      <?php if ( has_nav_menu( 'social' ) ) : ?>
      <nav role="navigation" class="social">
        <?php
          wp_nav_menu( array(
             'menu_class'     => 'nav nav-social',
             'theme_location' => 'social',
             'depth'          => 1,
             'link_before'    => '<span class="hidden">',
             'link_after'     => '</span>',
          ) );
          ?>
      </nav>
      <?php endif; ?>

    </div>
  </div>
</div>







