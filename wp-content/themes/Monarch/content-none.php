<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.0
 */
?>

<section class="error-404 no-results not-found">

   <header class="page-header">

      <div class="timeline-badge"><i class="ion-pin"></i></div>
      
      <div class="page-header-content">

         <h1 class="page-title"><span><?php echo get_theme_mod( 'monarch_translation_text_21', 'Nothing Found' ); ?></span></h1>

         <div class="taxonomy-description">
            <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
               <p><?php echo get_theme_mod( 'monarch_translation_text_38', 'Ready to publish your first post?' ) . ' <a href="' . esc_url( admin_url( 'post-new.php' ) ) . '">' . get_theme_mod( 'monarch_translation_text_37', 'Get started here' ) . '</a>'; ?></p>
            <?php elseif ( is_search() ) : ?>
               <p><?php echo get_theme_mod( 'monarch_translation_text_23', 'Sorry, but nothing matched your search terms. Please try again with some different keywords.' ); ?></p>
               <?php get_search_form(); ?>
            <?php else : ?>
               <p><?php echo get_theme_mod( 'monarch_translation_text_22', 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.' ); ?></p>
               <?php get_search_form(); ?>
            <?php endif; ?>
         </div>
         
      </div>

   </header>
   <!-- .page-header -->

</section>

<!-- .no-results -->
<div class="timeline"></div>