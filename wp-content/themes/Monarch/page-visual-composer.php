<?php

/*
* Template Name: Page Visual Composer
*/

get_header(); ?>

<!-- Content -->
<div class="content fullwidth clearfix">

	<!-- Main -->
	<main class="main col-xs-12 col-sm-12 col-md-12 col-lg-8 col-bg-8" role="main">
		<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div class="post-content clearfix">
						<?php 
							the_content();
							wp_link_pages_monarch();
						?>
						<div class="vc-edit-links">		
							<?php edit_post_link( get_theme_mod( 'monarch_translation_text_6', 'Edit' ) ); ?>
						</div>
					</div>
					
				</article>

				<?php

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			
			// End the loop.
			endwhile;
		?>
	</main>

</div>

<?php get_footer(); ?>
