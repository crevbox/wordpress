<?php

/*
* Template Name: Page One Sidebar
*/

get_header(); ?>

<!-- Content -->
<div class="content with-sb buddypress clearfix">

	<!-- Main -->
	<main class="main col-xs-12 col-sm-12 col-md-12 col-lg-8 col-bg-8" role="main">
		<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
			
				// Include the page content template.
				get_template_part( 'content', 'page' );
			
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			
			// End the loop.
			endwhile;
		?>
	</main>

	<!-- Sidebar BuddyPress -->
	<?php if ( is_active_sidebar( 'sidebar-bp' ) ) : ?>

	<aside class="sidebar widget-area col-xs-12 col-sm-12 col-md-12 col-lg-4 col-bg-4" id="widget-area" role="complementary">

		<div class="masonry">
		     <?php if ( is_active_sidebar( 'sidebar-bp' ) ) : ?>
			        <?php dynamic_sidebar( 'sidebar-bp' ); ?>
		     <?php endif; ?>
		</div>

	</aside>

	<?php endif; ?>

</div>

<?php get_footer(); ?>
