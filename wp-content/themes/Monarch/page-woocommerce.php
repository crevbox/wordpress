<?php

/*
* Template Name: Page WooCommerce
*/

get_header();

?>

<!-- Content -->
<div class="content with-sb woocommerce buddypress clearfix">

	<!-- Main -->
	<main class="main col-xs-12 col-sm-12 col-md-12 col-lg-8 col-bg-8" role="main">
		<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
		?>
		
		<!-- Post Wrapper -->
		<div class="post-wrap">

			<!-- Left Line -->
			<div class="timeline"></div>

			<div id="buddypress">

				<div class="timeline-badge"><i class="ion-ios-cart"></i></div>

					<header class="page-header buddypress">
						<h1 class="page-title"><?php the_title( '<h1 class="post-title">', '</h1>' ); ?></h1>
					</header>

					<div class="woocommerce-page clearfix">
						<?php 
							the_content();
							wp_link_pages_monarch();
							edit_post_link( get_theme_mod( 'monarch_translation_text_6', 'Edit' ) );
						?>
					</div>
				
			</div>

		</div>

		<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			
			// End the loop.
			endwhile;
		?>
	</main>

	<!-- Sidebar BuddyPress -->
	<?php if ( is_active_sidebar( 'sidebar-woo' ) ) : ?>

	<aside class="sidebar widget-area col-xs-12 col-sm-12 col-md-12 col-lg-4 col-bg-4" id="widget-area" role="complementary">

		<div class="masonry">
		     <?php if ( is_active_sidebar( 'sidebar-woo' ) ) : ?>
			        <?php dynamic_sidebar( 'sidebar-woo' ); ?>
		     <?php endif; ?>
		</div>

	</aside>

	<?php endif; ?>

</div>

<?php get_footer(); ?>
