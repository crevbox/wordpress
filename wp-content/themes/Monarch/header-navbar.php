<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 2.0
 */
?>

<nav class="navbar navbar-inverse navbar-color">
  <div class="container">

    <!-- Logo -->
    <header id="masthead" class="site-header navbar-header" role="banner">

      <div class="site-branding navbar-brand">
        <h1 class="site-title clearfix"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php echo get_bloginfo( 'description', 'display' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
      </div>

      <!-- <button type="button" class="header-panel-toggle mobile"><span class="ion-android-more-horizontal"></span></button> -->

    </header>

    <div id="navbar" class="navbar-collapse collapse">

    <?php if ( is_user_logged_in() ) : ?>

      <!-- If BuddyPress activated -->
      <?php if ( function_exists( 'is_buddypress' ) ) : ?>

        <!-- Avatar -->
        <ul class="nav navbar-nav navbar-right nav-buddy">
          <li class="dropdown avatar">

              <a href="<?php echo bp_loggedin_user_domain(); ?>" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <?php bp_loggedin_user_avatar( 'type=full', 'class=img-circle' ); ?>
                <?php echo bp_core_get_user_displayname( bp_loggedin_user_id() ); ?>
                <span class="caret"></span>
              </a>

              <?php monarch_notification(); ?>

              <!-- Logged-in Menu -->
              <ul class="dropdown-menu" role="menu">

                <!-- <li><a href="<?php echo bp_loggedin_user_domain(); ?>activity"><?php esc_html_e('Activity', 'monarch'); ?></a></li> -->
                <li><a href="<?php echo bp_loggedin_user_domain(); ?>friends"><?php esc_html_e('Friends', 'monarch'); ?></a></li>
                <li><a href="<?php echo bp_loggedin_user_domain(); ?>groups"><?php esc_html_e('Groups', 'monarch'); ?></a></li>
                <li><a href="<?php echo bp_loggedin_user_domain(); ?>messages"><?php esc_html_e('Messages', 'monarch'); ?></a></li>
                <!-- <li><a href="<?php echo bp_loggedin_user_domain(); ?>profile"><?php esc_html_e('Notifications', 'monarch'); ?></a></li> -->
                <!-- <li><a href="<?php echo bp_loggedin_user_domain(); ?>forums"><?php esc_html_e('Forums', 'monarch'); ?></a></li> -->
                <li><a href="<?php echo bp_loggedin_user_domain(); ?>profile"><?php esc_html_e('Profile', 'monarch'); ?></a></li>
                <li><a href="<?php echo bp_loggedin_user_domain(); ?>settings"><?php esc_html_e('Settings', 'monarch'); ?></a></li>
                <li class="last"><a href="<?php echo wp_logout_url( bp_get_requested_url() ); ?>"><?php esc_html_e('Log Out', 'monarch'); ?></a></li>

              </ul>
            </li>
        </ul>

    <?php endif; ?>
<?php else : ?>

    <!-- If BuddyPress activated -->
    <?php if ( function_exists( 'is_buddypress' ) ) : ?>

    <!-- Logged-out Menu -->

    <ul class="nav navbar-nav navbar-right nav-buddy">
      <li class="login"><a href="#" data-toggle="modal" data-target="#modal-login"><?php echo get_theme_mod( 'monarch_translation_text_2', 'Login' ); ?></a></li>
      <?php if ( get_option( 'users_can_register' ) ) : ?>
      <li class="register"><?php printf( wp_kses( __( '<a href="%s">Register</a>', 'monarch' ), array(  'a' => array( 'href' => array() ) ) ), bp_get_signup_page() ) ?></li>
      <?php endif; ?>
      </ul>

    <?php endif; ?>
    <!-- End is_buddypress -->

    <?php endif; ?>
    <!-- End is_user_logged_in -->

    </div>

  </div>
</nav>

<nav class="navbar navbar-inverse navbar-second">
  <div class="container">

    <div id="navbar" class="navbar-collapse collapse">

      <!-- BuddyPress Navigation -->
      <?php if ( has_nav_menu( 'buddy' ) ) : ?>
      <nav role="navigation">
        <?php
          wp_nav_menu( array(
             'menu_class'     => 'nav nav-buddy navbar-nav',
             'theme_location' => 'buddy',
             'depth' => 1,
          ) );
          ?>
      </nav>
      <?php endif; ?>

      <ul class="nav navbar-nav navbar-right nav-buddy">
        <li class="">
          <a href="#" data-toggle="modal" data-target="#modal-search" class="ion-search"><?php echo get_theme_mod( 'monarch_translation_text_54', 'Search' ); ?></a>
        </li>
        <li class="">
          <a href="#" class="ion-navicon-round layout-button"><?php echo get_theme_mod( 'monarch_translation_text_55', 'Panel' ); ?></a>
        </li>
      </ul>

    </div>
  </div>
</nav>