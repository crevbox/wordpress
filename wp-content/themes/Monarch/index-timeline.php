<?php

   /**
    * The main template file
    *
    * Timeline Posts
    *
    * This is the most generic template file in a WordPress theme
    * and one of the two required files for a theme (the other being style.css).
    * It is used to display a page when nothing more specific matches a query.
    * e.g., it puts together the home page when no home.php file exists.
    *
    * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
    *
    * @package WordPress
    * @subpackage Monarch
    * @since Monarch 1.0
    */
   
?>

<!-- Content -->
<div class="content fullwidth clearfix">

<!-- Main -->
<main class="main col-xs-12 col-sm-12 col-md-12 col-lg-12 col-bg-12 timeline-blog" role="main">

  <?php if ( have_posts() ) : ?>
  <div class="masonry-posts masonry" id="jp-scroll">
    <?php while ( have_posts() ) : the_post();
      get_template_part( 'content', get_post_format() );
    endwhile; ?>
  </div>
  
  <div class="post-wrap pagination">
    <div class="timeline"></div>
    <?php monarch_the_posts_pagination(); ?>
  </div>

  <?php else :
    get_template_part( 'content', 'none' );
    endif;
  ?>

</main>

</div>