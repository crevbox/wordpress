<?php

/*
* Template Name: Page Masonry
*/

get_header();

?>

<!-- Content -->
<div class="content fullwidth clearfix">

<!-- Main -->
<main class="main col-xs-12 col-sm-12 col-md-12 col-lg-12 col-bg-12 masonry-blog" role="main">

<?php query_posts('post_type=post&post_status=publish&posts_per_page=10&paged='. get_query_var('paged')); ?>

  <?php if ( have_posts() ) : ?>
    
  <div class="masonry-posts masonry" id="jp-scroll">
    <?php while ( have_posts() ) : the_post();
      get_template_part( 'content', get_post_format() );
    endwhile; ?>
  </div>

  <div class="post-wrap pagination">
    <?php monarch_the_posts_pagination(); ?>
  </div>

  <?php else :
    get_template_part( 'content', 'none' );
  endif; wp_reset_query(); ?>

</main>

</div>

<?php get_footer(); ?>

