<?php

/*
* Template Name: Page Widgets
*/

get_header();

?>

<!-- Content -->
<div class="content fullwidth clearfix">

<div class="row">

	<!-- Main -->
	<main class="main widgets-page col-xs-12 col-sm-12 col-md-12 col-lg-7 col-bg-6" role="main">

		<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

			?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="post-content clearfix">
					<?php 
						the_content();
						wp_link_pages_monarch();
					?>
				</div>
				
			</article>

			<?php

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			
			// End the loop.
			endwhile;
		?>

		<div class="masonry widget-area" id="widget-area" role="complementary">
			<div class="row">
				<?php if ( is_active_sidebar( 'widgets-page' ) ) : ?>
					<?php dynamic_sidebar( 'widgets-page' ); ?>
				<?php endif; ?>
			</div>
		</div>

		<?php edit_post_link( get_theme_mod( 'monarch_translation_text_6', 'Edit' ) ); ?>

	</main>

</div>

</div>

<?php get_footer(); ?>