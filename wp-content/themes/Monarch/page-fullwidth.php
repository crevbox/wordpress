<?php

/*
* Template Name: Page Fullwidth
*/

get_header(); ?>

<!-- Content -->
<div class="content fullwidth clearfix">

	<!-- Main -->
	<main class="main col-xs-12 col-sm-12 col-md-12 col-lg-8 col-bg-8" role="main">
		<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
			
				// Include the page content template.
				get_template_part( 'content', 'page' );
			
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			
			// End the loop.
			endwhile;
		?>
	</main>

</div>

<?php get_footer(); ?>
