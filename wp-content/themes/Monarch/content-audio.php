<?php
/**
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.0
 */
?>

<!-- Post Wrapper -->
<div class="post-wrap elem <?php echo monarch_post_avatar(); ?>">

  <div class="timeline"></div>

  <article id="post-<?php the_ID(); ?>" <?php post_class( array( 'ShowOnScroll' ) ); ?> >

    <span class="post-date"><?php monarch_post_date(); ?></span>

    <header class="post-header without-post-thumbnail">

      <div class="post-format-bg"></div>

    <?php if ( ! get_theme_mod( 'monarch_post_avatar' ) ) : ?>

        <div class="avatar-wrapper">
          <a class="avatar-link" href="<?php echo esc_url( get_author_posts_url(get_the_author_meta( 'ID' )) ); ?>">
            <?php
              $author_bio_avatar_size = apply_filters( 'monarch_author_bio_avatar_size', 56 );
              echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
            ?>
          </a>
        </div>
      <?php endif; ?>

      <?php monarch_post_format_header(); ?>

      <?php if ( is_sticky() && ! is_paged() ) { printf( '<span class="sticky-post"><i class="ion-star"></i></span>' ); }; ?>

      <div class="titles">
        <?php the_category(''); ?>
        <?php
          if ( is_single() ) :
            the_title( '<h1 class="post-title">', '</h1>' );
          else :
            the_title( sprintf( '<h2 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
          endif;
        ?>
      </div>

    </header>

    <div class="post-content clearfix">
      <?php
        the_content();
        wp_link_pages_monarch();
      ?>
    </div>

    <?php
      if ( is_single() ) :
        echo '<div class="post-tags clearfix">';
            the_tags( '', '', '' );
            edit_post_link( get_theme_mod( 'monarch_translation_text_6', 'Edit' ), '', '' );
        echo '</div>';
      endif;
    ?>

    <footer class="post-footer">
      <ul>
        <li class="author"><?php the_author_posts_link(); ?></li>
        <?php monarch_attachments() ?>
        <?php monarch_post_format_footer(); ?>
        <?php monarch_share_buttons(); ?>
        <li class="comments pull-right"><?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) { comments_popup_link( esc_html__( '0', 'monarch' ), esc_html__( '1', 'monarch' ), esc_html__( '%', 'monarch' ) ); } ?></li>
      </ul>
    </footer>

  </article>

</div>

<?php
    //  Share Buttons
    get_template_part('inc/widgets/modal-share');
?>

<?php
  if ( is_single() && get_the_author_meta( 'description' ) && !get_theme_mod('monarch_author_bio') ) :
    //  Author Bio
    get_template_part( 'author-bio' );
  endif;
?>
  
<?php 
  if( is_single() && !get_theme_mod('monarch_relatedposts') ) :
    //  Related Posts
    get_template_part('inc/widgets/relatedposts');
  endif; 
?>