<?php

/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.0
 */
   
get_header(); ?>

<!-- Content -->
<div class="content with-sb woocommerce buddypress clearfix">

	<!-- Main -->
	<main class="main col-xs-12 col-sm-12 col-md-12 col-lg-8 col-bg-8" role="main">
	<div class="post-wrap">
		<div class="timeline"></div>

		<div id="buddypress">
			<div class="timeline-badge"><i class="ion-ios-cart"></i></div>
			<?php woocommerce_content(); ?>
			<div class="timeline-badge bottom"><i class="ion-ios-cart"></i></div>
		</div>

	</div>
	</main>

	<!-- Sidebar WooCommerce -->
	<?php if ( is_active_sidebar( 'sidebar-woo' ) ) : ?>

	<aside class="sidebar widget-area col-xs-12 col-sm-12 col-md-12 col-lg-4 col-bg-4" id="widget-area" role="complementary">

		<div class="masonry">
		     <?php if ( is_active_sidebar( 'sidebar-woo' ) ) : ?>
			        <?php dynamic_sidebar( 'sidebar-woo' ); ?>
		     <?php endif; ?>
		</div>

	</aside>

	<?php endif; ?>

</div>

<?php get_footer(); ?>
