<?php
/**
 * Monarch Customizer functionality
 *
 * @package WordPress
 * @subpackage Monarch_Theme
 * @since Monarch 1.0
 */

// This code is recommended for viewing on the screen 1920px +
// Uncheck "View -> Word Wrap" in the Editor

/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since Monarch 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function monarch_customize_register( $wp_customize ) {

$color_scheme = monarch_get_color_scheme();
$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';

//-----------------------------------------------------
// Add Settings
//-----------------------------------------------------

// Colors
$wp_customize->add_setting( 'main_color',                          array( 'default' => $color_scheme[2],   'sanitize_callback' => 'sanitize_hex_color', ) );
$wp_customize->add_setting( 'second_main_color',                   array( 'default' => $color_scheme[3],   'sanitize_callback' => 'sanitize_hex_color', ) );
$wp_customize->add_setting( 'panels_background_color',             array( 'default' => $color_scheme[1],   'sanitize_callback' => 'sanitize_hex_color', ) );
$wp_customize->add_setting( 'border_color',                        array( 'default' => $color_scheme[4],   'sanitize_callback' => 'sanitize_hex_color', ) );

// Color Scheme
$wp_customize->add_setting( 'color_scheme',                        array( 'default' => 'default',          'sanitize_callback' => 'monarch_sanitize_color_scheme', ) );

// Customizer Text
$wp_customize->add_setting( 'monarch_description_text_1',          array( 'default' => false, 'sanitize_callback' => false, ) );
$wp_customize->add_setting( 'monarch_description_text_2',          array( 'default' => false, 'sanitize_callback' => false, ) );
$wp_customize->add_setting( 'monarch_description_text_3',          array( 'default' => false, 'sanitize_callback' => false, ) );
$wp_customize->add_setting( 'monarch_description_text_4',          array( 'default' => false, 'sanitize_callback' => false, ) );
$wp_customize->add_setting( 'monarch_description_text_5',          array( 'default' => false, 'sanitize_callback' => false, ) );

$wp_customize->add_setting( 'monarch_font_secondary',              array( 'default' => 'Playfair Display', 'sanitize_callback' => 'monarch_font_secondary_sanitize', ) );
$wp_customize->add_setting( 'monarch_font_primary',                array( 'default' => 'Merriweather',     'sanitize_callback' => 'monarch_font_primary_sanitize', ) );

// Select Options
$wp_customize->add_setting( 'monarch_css_grid',                    array( 'default' => 'social',           'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_ui_kit',                      array( 'default' => 'monarch',          'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_css_image_outlines',          array( 'default' => 'monarch',          'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_css_widget_title',            array( 'default' => 'monarch',          'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_css_box_style',               array( 'default' => 'monarch',          'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_css_alerts',                  array( 'default' => 'monarch',          'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_css_select',                  array( 'default' => 'monarch',          'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_css_buttons',                 array( 'default' => 'monarch',          'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_font_weight_logo',            array( 'default' => 'normal',           'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_font_logo',                   array( 'default' => 'Elsie',            'sanitize_callback' => 'monarch_sanitize_options', ) );
$wp_customize->add_setting( 'monarch_main_style',                  array( 'default' => 'default',          'sanitize_callback' => 'monarch_sanitize_options', ) );

// Checkboxes
$wp_customize->add_setting( 'monarch_generate_static_css',         array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_fonts_activate',              array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_font_greek',                  array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_font_cyrillic',               array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_css_custom_ui_activate',      array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_css_box_corner',              array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_profile_covers',              array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_css_box_border_radius',       array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_css_letter_spacing',          array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_font_devanagari',             array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_font_vietnamese',             array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_font_medium',                 array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_font_semi_bold',              array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_relatedposts',                array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_author_bio',                  array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_hide_post_meta',              array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_post_avatar',                 array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_show_adminbar',               array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_hide_share_buttons',          array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_admin_button',                array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_private_site',                array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_fixed_panel',                 array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_content_excerpt',             array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_responsive_bp_groups',        array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_responsive_monarch_comments', array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_responsive_monarch_posts',    array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_responsive_calendar',         array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_responsive_bp_members',       array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_responsive_tag_cloud',        array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );
$wp_customize->add_setting( 'monarch_responsive_archives',         array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_checkbox', ) );

// Number
$wp_customize->add_setting( 'monarch_font_size_logo',              array( 'default' => 29,    'sanitize_callback' => 'monarch_sanitize_number', ) );
$wp_customize->add_setting( 'monarch_font_letter_spacing_logo',    array( 'default' => 8,     'sanitize_callback' => 'monarch_sanitize_number', ) );
$wp_customize->add_setting( 'monarch_logo_background_position',    array( 'default' => 0,     'sanitize_callback' => 'monarch_sanitize_number', ) );

// Image
$wp_customize->add_setting( 'monarch_logo_collapsed_panel',        array( 'default' => '',    'sanitize_callback' => 'monarch_sanitize_image', ) );

// Pages
$wp_customize->add_setting( 'monarch_login_page',                  array( 'default' => false, 'sanitize_callback' => 'monarch_sanitize_dropdown_pages', 'description' => esc_html__( 'Required for Private Site option', 'monarch' ) ) );

// HTML
$wp_customize->add_setting( 'monarch_footer_copyright',            array( 'default' => wp_kses( __( 'Proudly powered by <strong>WordPress</strong>', 'monarch' ), esc_html( get_bloginfo( 'name' ) ) ), 'sanitize_callback' => 'monarch_sanitize_html', ) );

//======================================================================
// Add Monarch Theme Setting and Control
//======================================================================

$wp_customize->add_section( 'monarch_section_theme' , array('title' => esc_html__( 'Theme Settings', 'monarch' ), 'priority' => 30, ) );

// Style of the main page
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_main_style',         array( 'priority' => 1,  'section' => 'monarch_section_theme', 'settings' => 'monarch_main_style',         'type' => 'select', 'choices' => array( 'default' => esc_html__( 'Default', 'monarch' ), 'timeline' => esc_html__( 'Timeline', 'monarch' ), 'masonry' => esc_html__( 'Masonry', 'monarch' ), ), 'label' => esc_html__( 'Style of the main page', 'monarch' ) ) ) );

// Grid
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_grid',           array( 'priority' => 2,  'section' => 'monarch_section_theme', 'settings' => 'monarch_css_grid',           'type' => 'select', 'choices' =>  array( 'social'  => esc_html__( 'Social', 'monarch' ),  'blog' => esc_html__( 'Blog', 'monarch' ) ), 'label' => esc_html__( 'Grid (Alpha, hidden setting)', 'monarch' ) ) ) );

// Hide:
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_relatedposts',       array( 'priority' => 3,  'section' => 'monarch_section_theme', 'settings' => 'monarch_relatedposts',       'type' => 'checkbox', 'label' => esc_html__( 'Hide: Post Related Posts', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_author_bio',         array( 'priority' => 10, 'section' => 'monarch_section_theme', 'settings' => 'monarch_author_bio',         'type' => 'checkbox', 'label' => esc_html__( 'Hide: Post Author Bio', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_hide_post_meta',     array( 'priority' => 11, 'section' => 'monarch_section_theme', 'settings' => 'monarch_hide_post_meta',     'type' => 'checkbox', 'label' => esc_html__( 'Hide: Post Meta', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_show_adminbar',      array( 'priority' => 14, 'section' => 'monarch_section_theme', 'settings' => 'monarch_show_adminbar',      'type' => 'checkbox', 'label' => esc_html__( 'Hide: WordPress Toolbar for all users', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_hide_share_buttons', array( 'priority' => 15, 'section' => 'monarch_section_theme', 'settings' => 'monarch_hide_share_buttons', 'type' => 'checkbox', 'label' => esc_html__( 'Hide: Share Buttons', 'monarch' ), 'description' => esc_html__( 'Hidden when the Private Site is activated', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_admin_button',       array( 'priority' => 16, 'section' => 'monarch_section_theme', 'settings' => 'monarch_admin_button',       'type' => 'checkbox', 'label' => esc_html__( 'Hide: Dashboard Button', 'monarch' ), 'description' => esc_html__( 'Will be visible only for admins', 'monarch' ) ) ) );

$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_post_avatar',        array( 'priority' => 17, 'section' => 'monarch_section_theme', 'settings' => 'monarch_post_avatar',        'type' => 'checkbox', 'label' => esc_html__( 'Alternative Posts Without Avatars', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_profile_covers',     array( 'priority' => 18, 'section' => 'monarch_section_theme', 'settings' => 'monarch_profile_covers',     'type' => 'checkbox', 'label' => esc_html__( 'Covers in the directory of Members and Groups', 'monarch' ) ) ) );

// Footer text
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_footer_copyright',   array( 'priority' => 20, 'section' => 'monarch_section_theme', 'settings' => 'monarch_footer_copyright',   'type' => 'textarea', 'label' => esc_html__( 'Copyright or other text to be displayed in the site footer', 'monarch' ) ) ) );

// Private site, Login page, Fixed panel
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_private_site',       array( 'priority' => 21, 'section' => 'monarch_section_theme', 'settings' => 'monarch_private_site',       'type' => 'checkbox', 'label' => esc_html__( 'Private Site: hide the site for logged out users', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_login_page',         array( 'priority' => 22, 'section' => 'monarch_section_theme', 'settings' => 'monarch_login_page',         'type' => 'dropdown-pages', 'label' => esc_html__( 'Login Page', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_fixed_panel',        array( 'priority' => 23, 'section' => 'monarch_section_theme', 'settings' => 'monarch_fixed_panel',        'type' => 'checkbox', 'label' => esc_html__( 'Fixed Header & Footer: enable if you are using only one widget on the Header Panel', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_content_excerpt',    array( 'priority' => 24, 'section' => 'monarch_section_theme', 'settings' => 'monarch_content_excerpt',    'type' => 'checkbox', 'label' => esc_html__( 'Excerpt Content: automatically reduce the text of the posts on the main pages', 'monarch' ) ) ) );

//======================================================================
// Responsive Settings
//======================================================================

$wp_customize->add_section( 'monarch_section_responsive', array( 'priority' => 121, 'title' => esc_html__( 'Responsive Settings', 'monarch' ), 'description'    => esc_html__( 'You can hide some widgets (1 - 2) on the resolution from 1200px to 1670px for the pages with two sidebars: category, single, blog. How this works can be seen on the demo site.', 'monarch' ) ) );

// Hide:
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_responsive_bp_groups',        array( 'priority' => 1, 'section'  => 'monarch_section_responsive', 'settings' => 'monarch_responsive_bp_groups',        'type' => 'checkbox', 'label' => esc_html__( 'BuddyPress Groups', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_responsive_bp_members',       array( 'priority' => 2, 'section'  => 'monarch_section_responsive', 'settings' => 'monarch_responsive_bp_members',       'type' => 'checkbox', 'label' => esc_html__( 'BuddyPress Members', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_responsive_tag_cloud',        array( 'priority' => 3, 'section'  => 'monarch_section_responsive', 'settings' => 'monarch_responsive_tag_cloud',        'type' => 'checkbox', 'label' => esc_html__( 'Tag Cloud', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_responsive_archives',         array( 'priority' => 4, 'section'  => 'monarch_section_responsive', 'settings' => 'monarch_responsive_archives',         'type' => 'checkbox', 'label' => esc_html__( 'Archives', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_responsive_calendar',         array( 'priority' => 5, 'section'  => 'monarch_section_responsive', 'settings' => 'monarch_responsive_calendar',         'type' => 'checkbox', 'label' => esc_html__( 'Calendar', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_responsive_monarch_posts',    array( 'priority' => 6, 'section'  => 'monarch_section_responsive', 'settings' => 'monarch_responsive_monarch_posts',    'type' => 'checkbox', 'label' => esc_html__( 'Monarch Posts', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_responsive_monarch_comments', array( 'priority' => 7, 'section'  => 'monarch_section_responsive', 'settings' => 'monarch_responsive_monarch_comments', 'type' => 'checkbox', 'label' => esc_html__( 'Monarch Comments', 'monarch' ) ) ) );

//======================================================================
// Logo Settings
//======================================================================

$wp_customize->get_section( 'header_image' )->title = esc_html__( 'Logo', 'monarch' );

// Info
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_description_text_5',                 array( 'priority' => 15, 'section' => 'header_image', 'settings' => 'monarch_description_text_5',       'type' => 'hidden',                                                                   'label' => esc_html__( 'Logo Font', 'monarch' ), 'description' => sprintf( wp_kses( esc_html__( 'Not all fonts provide all styles. Please visit the %s to see which styles are available for each font.', 'monarch' ), array( 'a' => array( 'href' => array() ) ) ), sprintf( '<a href="%1$s" target="_blank">%2$s</a>', esc_url( 'https://www.google.com/fonts' ), esc_html__( 'Google Fonts website', 'monarch' ) ) ) ) ) );

// Font style
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_logo',                          array( 'priority' => 16, 'section' => 'header_image', 'settings' => 'monarch_font_logo',                'type' => 'select', 'choices' => monarch_get_font_choices(),                          'label' => esc_html__( 'Font family', 'monarch' ), 'description' => esc_html__( 'Default: Elsie', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_size_logo',                     array( 'priority' => 17, 'section' => 'header_image', 'settings' => 'monarch_font_size_logo',           'type' => 'number', 'input_attrs' => array( 'min' => 12, 'max' => 90, 'step'	=> 1 ),  'label' => esc_html__( 'Font size (px)', 'monarch' ), 'description' => esc_html__( 'Default: 26px', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_letter_spacing_logo',           array( 'priority' => 18, 'section' => 'header_image', 'settings' => 'monarch_font_letter_spacing_logo', 'type' => 'number', 'input_attrs' => array( 'min' => 0, 'max' => 90, 'step' => 1 ),   'label' => esc_html__( 'Letter spacing (px)', 'monarch' ), 'description' => esc_html__( 'Default: 4px', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_weight_logo',                   array( 'priority' => 19, 'section' => 'header_image', 'settings' => 'monarch_font_weight_logo',         'type' => 'radio', 'choices' => array( 'normal' => esc_html__( 'Normal', 'monarch' ), 'bold' => esc_html__( 'Bold', 'monarch' ), ), 'label' => esc_html__( 'Font weight', 'monarch' ) ) ) );

// Logo position
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_logo_background_position',           array( 'priority' => 20, 'section' => 'header_image', 'settings' => 'monarch_logo_background_position', 'type' => 'number',                                                                   'label' => esc_html__( 'Position for logo: left, right (px)', 'monarch' ) ) ) );

// Logo for Collapsed Left Panel
$wp_customize->add_control( new WP_Customize_Cropped_Image_Control( $wp_customize, 'monarch_logo_collapsed_panel', array( 'priority' => 21, 'section' => 'header_image', 'settings' => 'monarch_logo_collapsed_panel', 'width' => 100, 'height'  => 200,                                                         'label' => esc_html__( 'Logo for Collapsed Left Panel', 'monarch' ) ) ) );

//======================================================================
// Theme Settings
//======================================================================

// User Interface Kit:
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_ui_kit',                  array( 'priority' => 1,  'section' => 'colors', 'settings' => 'monarch_ui_kit',                 'type' => 'select',   'choices'  =>  array( 'monarch' => esc_html__( 'Monarch', 'monarch' ), 'atlass' => esc_html__( 'Atlass', 'monarch' ), /*'theaxis' => esc_html__( 'TheAxiS (new, beta)', 'monarch' )*/ ), 'label' => esc_html__( 'User Interface Kit', 'monarch' ) ) ) );

// Base Color Scheme:
$wp_customize->add_control( 'color_scheme',                                                             array( 'label' => esc_html__( 'Base Color Scheme', 'monarch' ),       'section' => 'colors', 'type' => 'select', 'choices' => monarch_get_color_scheme_choices(), 'priority' => 2, ) );

// Colors
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_color',                array( 'label' => esc_html__( 'Main color', 'monarch' ),              'section' => 'colors', ) ) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'second_main_color',         array( 'label' => esc_html__( 'Second color', 'monarch' ),            'section' => 'colors', ) ) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'panels_background_color',   array( 'label' => esc_html__( 'Panels Background Color', 'monarch' ), 'section' => 'colors', ) ) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'border_color',              array( 'label' => esc_html__( 'Box Border Color', 'monarch' ),        'section' => 'colors', ) ) );

// Activate the Custom Fonts
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_description_text_1',      array( 'priority' => 20, 'section' => 'colors', 'settings' => 'monarch_description_text_1',     'type' => 'hidden',   'label' => esc_html__( 'Activate the custom fonts:', 'monarch' ), 'description' => sprintf( wp_kses( esc_html__( 'Not all fonts support each feature. Please visit the %s to see which styles are available for each font.', 'monarch' ),  array( 'a' =>  array( 'href' =>  array() ) ) ), sprintf( '<a href="%1$s" target="_blank">%2$s</a>', esc_url( 'https://www.google.com/fonts' ), esc_html__( 'Google Fonts website', 'monarch' ) ) ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_fonts_activate',          array( 'priority' => 21, 'section' => 'colors', 'settings' => 'monarch_fonts_activate',         'type' => 'checkbox', 'label' => esc_html__( 'If not checked: Monarch UI - Merriweather & Playfair Display, Atlass UI - Exo 2.', 'monarch' ) ) ) );

$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_letter_spacing',      array( 'priority' => 22, 'section' => 'colors', 'settings' => 'monarch_css_letter_spacing',     'type' => 'checkbox', 'label' => esc_html__( 'Letter Spacing', 'monarch' ) ) ) );

// Primary & Secondary Fonts
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_primary',            array( 'priority' => 23, 'section' => 'colors', 'settings' => 'monarch_font_primary',           'type' => 'select',   'choices'  => monarch_get_font_choices(),  'label' => esc_html__( 'Primary font:', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_secondary',          array( 'priority' => 24, 'section' => 'colors', 'settings' => 'monarch_font_secondary',         'type' => 'select',   'choices'  => monarch_get_font_choices(),  'label' => esc_html__( 'Secondary font:', 'monarch' ) ) ) );

// Font Characters Sets
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_description_text_2',      array( 'priority' => 30, 'section' => 'colors', 'settings' => 'monarch_description_text_2',     'type' => 'hidden',   'label' => esc_html__( 'Font characters sets:', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_greek',              array( 'priority' => 31, 'section' => 'colors', 'settings' => 'monarch_font_greek',             'type' => 'checkbox', 'label' => esc_html__( 'Greek', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_cyrillic',           array( 'priority' => 32, 'section' => 'colors', 'settings' => 'monarch_font_cyrillic',          'type' => 'checkbox', 'label' => esc_html__( 'Cyrillic', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_devanagari',         array( 'priority' => 33, 'section' => 'colors', 'settings' => 'monarch_font_devanagari',        'type' => 'checkbox', 'label' => esc_html__( 'Devanagari', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_vietnamese',         array( 'priority' => 34, 'section' => 'colors', 'settings' => 'monarch_font_vietnamese',        'type' => 'checkbox', 'label' => esc_html__( 'Vietnamese', 'monarch' ) ) ) );

// Font weight
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_description_text_3',      array( 'priority' => 35, 'section' => 'colors', 'settings' => 'monarch_description_text_3',     'type' => 'hidden',   'label' => esc_html__( 'Font weight:', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_medium',             array( 'priority' => 36, 'section' => 'colors', 'settings' => 'monarch_font_medium',            'type' => 'checkbox', 'label' => esc_html__( 'Medium 500', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_font_semi_bold',          array( 'priority' => 37, 'section' => 'colors', 'settings' => 'monarch_font_semi_bold',         'type' => 'checkbox', 'label' => esc_html__( 'Semi-bold 600', 'monarch' ) ) ) );

// Custom UI Kit
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_description_text_4',      array( 'priority' => 40, 'section' => 'colors', 'settings' => 'monarch_description_text_4',     'type' => 'hidden',   'label' => esc_html__( 'Activate the custom UI Kit (beta):', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_custom_ui_activate',  array( 'priority' => 41, 'section' => 'colors', 'settings' => 'monarch_css_custom_ui_activate', 'type' => 'checkbox', 'label' => esc_html__( 'If not checked: the default settings for UI Atlass & UI Monarch.', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_box_corner',          array( 'priority' => 42, 'section' => 'colors', 'settings' => 'monarch_css_box_corner',         'type' => 'checkbox', 'label' => esc_html__( 'Box Corner', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_box_border_radius',   array( 'priority' => 44, 'section' => 'colors', 'settings' => 'monarch_css_box_border_radius',  'type' => 'checkbox', 'label' => esc_html__( 'Box Border Radius', 'monarch' ) ) ) );

// Select Options
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_image_outlines',      array( 'priority' => 51, 'section' => 'colors', 'settings' => 'monarch_css_image_outlines',     'type' => 'select',   'choices' =>  array( 'none'    => esc_html__( 'None', 'monarch' ),    'monarch' => esc_html__( 'Monarch', 'monarch' ), 'atlass' => esc_html__( 'Atlass', 'monarch' ), ),                                                'label' => esc_html__( 'Image Outlines', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_widget_title',        array( 'priority' => 52, 'section' => 'colors', 'settings' => 'monarch_css_widget_title',       'type' => 'select',   'choices' =>  array( 'monarch' => esc_html__( 'Monarch', 'monarch' ), 'atlass' => esc_html__( 'Atlass', 'monarch' ), ),                                                                                                 'label' => esc_html__( 'Widgets, footers titles', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_box_style',           array( 'priority' => 53, 'section' => 'colors', 'settings' => 'monarch_css_box_style',          'type' => 'select',   'choices' =>  array( 'none'    => esc_html__( 'None', 'monarch' ),    'monarch' => esc_html__( 'Monarch', 'monarch' ), 'border' => esc_html__( 'Border', 'monarch' ), 'shadow' => esc_html__( 'Shadow', 'monarch' ), ), 'label' => esc_html__( 'Box style', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_alerts',              array( 'priority' => 54, 'section' => 'colors', 'settings' => 'monarch_css_alerts',             'type' => 'select',   'choices' =>  array( 'monarch' => esc_html__( 'Monarch', 'monarch' ), 'atlass' => esc_html__( 'Atlass', 'monarch' ), ),                                                                                                 'label' => esc_html__( 'Alerts', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_buttons',             array( 'priority' => 55, 'section' => 'colors', 'settings' => 'monarch_css_buttons',            'type' => 'select',   'choices' =>  array( 'monarch' => esc_html__( 'Monarch', 'monarch' ), 'atlass' => esc_html__( 'Atlass', 'monarch' ), ),                                                                                                 'label' => esc_html__( 'Buttons', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_css_select',              array( 'priority' => 56, 'section' => 'colors', 'settings' => 'monarch_css_select',             'type' => 'select',   'choices' =>  array( 'monarch' => esc_html__( 'Monarch', 'monarch' ), 'atlass' => esc_html__( 'Atlass', 'monarch' ), ),                                                                                                 'label' => esc_html__( 'Select, checkbox, radio buttons', 'monarch' ) ) ) );

// Generate CSS
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_generate_static_css',     array( 'priority' => 60, 'section' => 'colors', 'settings' => 'monarch_generate_static_css',    'type' => 'checkbox', 'label' => esc_html__( 'Generate static css file (beta)', 'monarch' ), 'description' => esc_html__( 'If you do not see the changes try to refresh the page or open from a different browser (Browser Caching).', 'monarch' ) ) ) );

//======================================================================
// Frontend Translation
//======================================================================

$wp_customize->add_section( 'monarch_section_translation', array( 'title' => esc_html__( 'Frontend Translation', 'monarch' ), 'priority' => 121, ) );

$wp_customize->add_setting( 'monarch_translation_text_1',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'What you are looking for?', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_2',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Login', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_3',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( '&larr; Previous page', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_4',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Next page &rarr;', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_5',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Page', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_6',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Edit', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_7',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Next', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_8',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Next post:', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_9',  array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Previous', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_10', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Previous post:', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_11', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Previous Image', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_12', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Next Image', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_13', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Parent post link', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_14', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Oops! That page can&rsquo;t be found.', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_15', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'It looks like nothing was found at this location. Maybe try a search?', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_16', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'View all posts', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_17', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Comments are closed.', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_18', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'One thought on', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_19', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'thoughts on', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_20', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Published in', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_21', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Nothing Found', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_22', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_23', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_24', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Submit', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_25', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Comment', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_26', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Website', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_27', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Email', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_28', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Name', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_29', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Open/Close Menu', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_31', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'View More', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_30', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Menu', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_32', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'More', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_33', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Password', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_34', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Username', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_35', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Collapse child menu', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_36', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Expand child menu', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_37', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Get started here', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_38', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Ready to publish your first post?', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_39', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Share', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_40', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Continue reading &raquo;', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_41', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Password Lost and Found', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_42', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Lost your password?', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_43', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Register', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_44', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Search Results for:', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_45', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Newer Comments', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_46', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Older Comments', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_47', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Comment navigation', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_48', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Pages:', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_49', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Lost Password', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_50', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_51', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Username or email', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_52', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Reset Password', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_53', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Great to have you back!', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_54', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Search', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_55', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Panel', 'monarch' ) ) );
$wp_customize->add_setting( 'monarch_translation_text_56', array( 'sanitize_callback' => 'monarch_sanitize_html', 'default' => esc_html__( 'Post', 'monarch' ) ) );

$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_1',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_1',  'type' => 'textarea', 'priority' => 1,  'label' => esc_html__( 'What you are looking for?', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_2',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_2',  'type' => 'textarea', 'priority' => 2,  'label' => esc_html__( 'Login', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_3',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_3',  'type' => 'textarea', 'priority' => 3,  'label' => esc_html__( '&larr; Previous page', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_4',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_4',  'type' => 'textarea', 'priority' => 4,  'label' => esc_html__( 'Next page &rarr;', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_5',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_5',  'type' => 'textarea', 'priority' => 5,  'label' => esc_html__( 'Page', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_6',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_6',  'type' => 'textarea', 'priority' => 6,  'label' => esc_html__( 'Edit', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_7',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_7',  'type' => 'textarea', 'priority' => 7,  'label' => esc_html__( 'Next', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_8',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_8',  'type' => 'textarea', 'priority' => 8,  'label' => esc_html__( 'Next post:', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_9',  array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_9',  'type' => 'textarea', 'priority' => 9,  'label' => esc_html__( 'Previous', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_10', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_10', 'type' => 'textarea', 'priority' => 10, 'label' => esc_html__( 'Previous post:', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_11', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_11', 'type' => 'textarea', 'priority' => 11, 'label' => esc_html__( 'Previous Image', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_12', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_12', 'type' => 'textarea', 'priority' => 12, 'label' => esc_html__( 'Next Image', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_13', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_13', 'type' => 'textarea', 'priority' => 13, 'label' => esc_html__( 'Parent post link', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_14', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_14', 'type' => 'textarea', 'priority' => 14, 'label' => esc_html__( 'Oops! That page can&rsquo;t be found.', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_15', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_15', 'type' => 'textarea', 'priority' => 15, 'label' => esc_html__( 'It looks like nothing was found at this location. Maybe try a search?', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_16', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_16', 'type' => 'textarea', 'priority' => 16, 'label' => esc_html__( 'View all posts', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_17', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_17', 'type' => 'textarea', 'priority' => 17, 'label' => esc_html__( 'Comments are closed.', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_18', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_18', 'type' => 'textarea', 'priority' => 18, 'label' => esc_html__( 'One thought on', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_19', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_19', 'type' => 'textarea', 'priority' => 19, 'label' => esc_html__( 'thoughts on', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_20', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_20', 'type' => 'textarea', 'priority' => 20, 'label' => esc_html__( 'Published in', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_21', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_21', 'type' => 'textarea', 'priority' => 21, 'label' => esc_html__( 'Nothing Found', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_22', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_22', 'type' => 'textarea', 'priority' => 22, 'label' => esc_html__( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_23', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_23', 'type' => 'textarea', 'priority' => 23, 'label' => esc_html__( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_24', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_24', 'type' => 'textarea', 'priority' => 24, 'label' => esc_html__( 'Submit', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_25', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_25', 'type' => 'textarea', 'priority' => 25, 'label' => esc_html__( 'Comment', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_26', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_26', 'type' => 'textarea', 'priority' => 26, 'label' => esc_html__( 'Website', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_27', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_27', 'type' => 'textarea', 'priority' => 27, 'label' => esc_html__( 'Email', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_28', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_28', 'type' => 'textarea', 'priority' => 28, 'label' => esc_html__( 'Name', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_29', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_29', 'type' => 'textarea', 'priority' => 29, 'label' => esc_html__( 'Open/Close Menu', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_30', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_30', 'type' => 'textarea', 'priority' => 30, 'label' => esc_html__( 'Menu', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_31', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_31', 'type' => 'textarea', 'priority' => 31, 'label' => esc_html__( 'View More', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_32', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_32', 'type' => 'textarea', 'priority' => 32, 'label' => esc_html__( 'More', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_33', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_33', 'type' => 'textarea', 'priority' => 33, 'label' => esc_html__( 'Password', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_34', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_34', 'type' => 'textarea', 'priority' => 34, 'label' => esc_html__( 'Username', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_35', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_35', 'type' => 'textarea', 'priority' => 35, 'label' => esc_html__( 'Collapse child menu', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_36', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_36', 'type' => 'textarea', 'priority' => 36, 'label' => esc_html__( 'Expand child menu', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_37', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_37', 'type' => 'textarea', 'priority' => 37, 'label' => esc_html__( 'Get started here', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_38', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_38', 'type' => 'textarea', 'priority' => 38, 'label' => esc_html__( 'Ready to publish your first post?', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_39', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_39', 'type' => 'textarea', 'priority' => 39, 'label' => esc_html__( 'Share', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_40', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_40', 'type' => 'textarea', 'priority' => 40, 'label' => esc_html__( 'Continue reading &raquo;', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_41', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_41', 'type' => 'textarea', 'priority' => 41, 'label' => esc_html__( 'Password Lost and Found', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_42', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_42', 'type' => 'textarea', 'priority' => 42, 'label' => esc_html__( 'Lost your password?', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_43', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_43', 'type' => 'textarea', 'priority' => 43, 'label' => esc_html__( 'Register', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_44', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_44', 'type' => 'textarea', 'priority' => 44, 'label' => esc_html__( 'Search Results for:', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_45', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_45', 'type' => 'textarea', 'priority' => 45, 'label' => esc_html__( 'Newer Comments', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_46', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_46', 'type' => 'textarea', 'priority' => 46, 'label' => esc_html__( 'Older Comments', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_47', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_47', 'type' => 'textarea', 'priority' => 47, 'label' => esc_html__( 'Comment navigation', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_48', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_48', 'type' => 'textarea', 'priority' => 48, 'label' => esc_html__( 'Pages:', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_49', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_49', 'type' => 'textarea', 'priority' => 49, 'label' => esc_html__( 'Lost Password', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_50', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_50', 'type' => 'textarea', 'priority' => 50, 'label' => esc_html__( 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_51', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_51', 'type' => 'textarea', 'priority' => 51, 'label' => esc_html__( 'Username or email', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_52', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_52', 'type' => 'textarea', 'priority' => 52, 'label' => esc_html__( 'Reset Password', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_53', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_53', 'type' => 'textarea', 'priority' => 53, 'label' => esc_html__( 'Great to have you back!', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_54', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_54', 'type' => 'textarea', 'priority' => 54, 'label' => esc_html__( 'Search', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_55', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_55', 'type' => 'textarea', 'priority' => 55, 'label' => esc_html__( 'Panel', 'monarch' ) ) ) );
$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'monarch_translation_text_56', array( 'section' => 'monarch_section_translation', 'settings' => 'monarch_translation_text_56', 'type' => 'textarea', 'priority' => 56, 'label' => esc_html__( 'Post', 'monarch' ) ) ) );

// Remove the core header textcolor control, as it shares the sidebar text color
$wp_customize->remove_control( 'header_textcolor' );
$wp_customize->remove_control( 'display_header_text' );

$wp_customize->get_section( 'colors' )->title = esc_html__( 'Colors & UI Kits', 'monarch' );
$wp_customize->get_section( 'background_image' )->description = esc_html__( ' The background image is loaded for pages: register, login, activate.', 'monarch' );

}
add_action( 'customize_register', 'monarch_customize_register', 11 );