<?php

//======================================================================
// Sanitization
//======================================================================

if ( ! function_exists( 'monarch_sanitize_image' ) ) :
/**
 * Check Image.
 *
 * @since Monarch 2.0
 *
 * @return array Array of color schemes.
 */
function monarch_sanitize_image( $input ) {
 
    /* default output */
    $output = '';
 
    /* check file type */
    $filetype = wp_check_filetype( $input );
    $mime_type = $filetype['type'];
 
    /* only mime type "image" allowed */
    if ( strpos( $mime_type, 'image' ) !== false ) {
        $output = $input;
    }
 
    return $output;
}
endif; // monarch_sanitize_image

if ( ! function_exists( 'monarch_sanitize_number' ) ) :
/**
 * Number sanitization callback.
 *
 * @since Monarch 1.1
 *
 */
function monarch_sanitize_number( $number, $setting ) {
    if (is_numeric($number)) {
        return ( $number ? $number : $setting->default );
    } else {
        return ( $setting->default );
    }
}
endif; // monarch_sanitize_number

if ( ! function_exists( 'monarch_sanitize_html' ) ) :
/**
 * HTML sanitization callback.
 *
 * @since Monarch 1.0
 *
 */
function monarch_sanitize_html( $html ) {
	return wp_kses( $html, array( 'a' => array( 'href' => array(), 'title' => array() ), 'br' => array(), 'em' => array(), 'strong' => array(), ) );
}
endif; // monarch_sanitize_html

if ( ! function_exists( 'monarch_sanitize_checkbox' ) ) :
/**
 * Checkbox sanitization callback.
 *
 * @since Monarch 1.0
 *
 */
function monarch_sanitize_checkbox( $checked ) {
	// Boolean check.
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
}
endif; // monarch_sanitize_checkbox

if ( ! function_exists( 'monarch_sanitize_color_scheme' ) ) :
/**
 * Sanitization callback for color schemes.
 *
 * @since Monarch 1.0
 *
 * @param string $value Color scheme name value.
 * @return string Color scheme name.
 */
function monarch_sanitize_color_scheme( $value ) {
	$color_schemes = monarch_get_color_scheme_choices();

	if ( ! array_key_exists( $value, $color_schemes ) ) {
		$value = 'default';
	}

	return $value;
}
endif; // monarch_sanitize_color_scheme

if ( ! function_exists( 'monarch_sanitize_options' ) ) :
/**
 * Sanitize options.
 *
 * @since Monarch 2.0
 *
 * @return option.
 */
function monarch_sanitize_options( $input, $setting ) {

	// Ensure input is a slug.
	$input = sanitize_key( $input );

	// Get list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// If the input is a valid key, return it; otherwise, return the default.
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

}
endif; // monarch_sanitize_sanitize_select

if ( ! function_exists( 'monarch_sanitize_dropdown_pages' ) ) :
/**
 * Dropdown Pages Sanitization.
 *
 * @since Monarch 1.8.9
 *
 * @return page.
 */
function monarch_sanitize_dropdown_pages( $page_id, $setting ) {
	// Ensure $input is an absolute integer.
	$page_id = absint( $page_id );

	// If $page_id is an ID of a published page, return it; otherwise, return the default.
	return ( 'publish' === get_post_status( $page_id ) ? $page_id : $setting->default );
}
endif; // monarch_sanitize_dropdown_pages

//======================================================================
// Color
//======================================================================

/**
 * Register color schemes for Monarch.
 *
 * Can be filtered with {@see 'monarch_color_schemes'}.
 *
 * The order of colors in a colors array:
 * 1. Main Background Color.
 * 2. Sidebar Background Color.
 * 3. Box Background Color.
 * 4. Main Text and Link Color.
 * 5. Sidebar Text and Link Color.
 * 6. Meta Box Background Color.
 *
 * @since Monarch 1.0
 *
 * @return array An associative array of color scheme options.
 */
function monarch_get_color_schemes() {
	/**
	 * Filter the color schemes registered for use with Monarch.
	 *
	 * @since Monarch 1.0
	 *
	 * @param array $schemes {
	 *     Associative array of color schemes data.
	 *
	 *     @type array $slug {
	 *         Associative array of information for setting up the color scheme.
	 *
	 *         @type string $label  Color scheme label.
	 *         @type array  $colors HEX codes for default colors prepended with a hash symbol ('#').
	 *                              Colors are defined in the following order: Main background, sidebar
	 *                              background, box background, main text and link, sidebar text and link,
	 *                              meta box background.
	 *     }
	 * }
	 */
	return apply_filters( 'monarch_color_schemes', array(
		'default'       => array('label' => esc_html__( 'Chocolate I', 'monarch' ),     'colors' => array('#f2f2f2', '#323232', '#b5967f', '#714d4d', '#dddddd', ), ),
		'chocolate2'    => array('label' => esc_html__( 'Chocolate II', 'monarch' ),    'colors' => array('#f2f2f2', '#181818', '#b5967f', '#714d4d', '#dddddd', ), ),
		'chocolate3'    => array('label' => esc_html__( 'Chocolate III', 'monarch' ),   'colors' => array('#f2f2f2', '#181818', '#986d5b', '#714d4d', '#dddddd', ), ),
		'chocolate4'    => array('label' => esc_html__( 'Chocolate IV', 'monarch' ),    'colors' => array('#f2f2f2', '#292320', '#a57c6c', '#714d4d', '#dddddd', ), ),
		'chocolate5'    => array('label' => esc_html__( 'Chocolate V', 'monarch' ),     'colors' => array('#eeeeee', '#292623', '#8a7765', '#b5967f', '#dddddd', ), ),
		'chocolate6'    => array('label' => esc_html__( 'Chocolate VI', 'monarch' ),    'colors' => array('#eeeeee', '#22171b', '#714d4d', '#986d5b', '#dddddd', ), ),
		'chocolate7'    => array('label' => esc_html__( 'Chocolate VII', 'monarch' ),   'colors' => array('#f0f0f0', '#333333', '#986d5b', '#b5967f', '#dddddd', ), ),
		'cake'          => array('label' => esc_html__( 'Cake I', 'monarch' ),          'colors' => array('#f4f4f4', '#232323', '#3ea59d', '#2b8780', '#dddddd', ), ),
		'cake2'         => array('label' => esc_html__( 'Cake II', 'monarch' ),         'colors' => array('#f0f0f0', '#3d3c3c', '#53b9a3', '#2b8780', '#dddddd', ), ),
		'cake3'         => array('label' => esc_html__( 'Cake III', 'monarch' ),        'colors' => array('#f4f4f4', '#393939', '#84bfa4', '#2b8780', '#dddddd', ), ),
		'cake4'         => array('label' => esc_html__( 'Cake IV', 'monarch' ),         'colors' => array('#eeeeee', '#333333', '#34a3b1', '#2b8780', '#dddddd', ), ),
		'cake5'         => array('label' => esc_html__( 'Cake V', 'monarch' ),          'colors' => array('#f0f0f0', '#2b2b2b', '#3ca09d', '#2b8780', '#dddddd', ), ),
		'red'           => array('label' => esc_html__( 'Red I', 'monarch' ),           'colors' => array('#f2f2f2', '#222222', '#a73a3a', '#8C73A5', '#dddddd', ), ),
		'red2'          => array('label' => esc_html__( 'Red II', 'monarch' ),          'colors' => array('#f0f0f0', '#262627', '#af4b4b', '#8C73A5', '#dddddd', ), ),
		'red3'          => array('label' => esc_html__( 'Red III', 'monarch' ),         'colors' => array('#f2f2f2', '#313131', '#a76262', '#8C73A5', '#dddddd', ), ),
		'red4'          => array('label' => esc_html__( 'Red IV', 'monarch' ),          'colors' => array('#f2f2f2', '#313231', '#da3641', '#8C73A5', '#dddddd', ), ),
		'gold'          => array('label' => esc_html__( 'Gold I', 'monarch' ),          'colors' => array('#f1f1f1', '#323232', '#bfa885', '#8e85bf', '#dddddd', ), ),
		'gold2'         => array('label' => esc_html__( 'Gold II', 'monarch' ),         'colors' => array('#eeeeee', '#333333', '#c9af7e', '#8e85bf', '#dddddd', ), ),
		'gold3'         => array('label' => esc_html__( 'Gold III', 'monarch' ),        'colors' => array('#f2f2f2', '#252525', '#adaf7f', '#8e85bf', '#dddddd', ), ),
		'gold4'         => array('label' => esc_html__( 'Gold IV', 'monarch' ),         'colors' => array('#f2f1ec', '#353533', '#c1b47f', '#8e85bf', '#dddddd', ), ),
		'gold5'         => array('label' => esc_html__( 'Gold V', 'monarch' ),          'colors' => array('#f2f2f2', '#3c3934', '#dec18c', '#8e85bf', '#dddddd', ), ),
		'gold6'         => array('label' => esc_html__( 'Gold VI', 'monarch' ),         'colors' => array('#f2f2f2', '#333333', '#98957a', '#8e85bf', '#dddddd', ), ),
		'violet'        => array('label' => esc_html__( 'Violet I', 'monarch' ),        'colors' => array('#f2f2f2', '#252525', '#b57fb3', '#967fb5', '#dddddd', ), ),
		'violet2'       => array('label' => esc_html__( 'Violet II', 'monarch' ),       'colors' => array('#f0f0f0', '#262627', '#ba68c8', '#967fb5', '#dddddd', ), ),
		'violet3'       => array('label' => esc_html__( 'Violet III', 'monarch' ),      'colors' => array('#f2f2f2', '#2b261f', '#97659b', '#967fb5', '#dddddd', ), ),
		'violet4'       => array('label' => esc_html__( 'Violet IV', 'monarch' ),       'colors' => array('#f2f2f2', '#292124', '#b57f97', '#967fb5', '#dddddd', ), ),
		'violet5'       => array('label' => esc_html__( 'Violet V', 'monarch' ),        'colors' => array('#f2f2f2', '#222222', '#8c73a5', '#b57fb3', '#dddddd', ), ),
		'violet6'       => array('label' => esc_html__( 'Violet VI', 'monarch' ),       'colors' => array('#f2f2f2', '#222222', '#71588a', '#b57fb3', '#dddddd', ), ),
		'green'         => array('label' => esc_html__( 'Green I', 'monarch' ),         'colors' => array('#efefef', '#323232', '#669669', '#3ca390', '#dddddd', ), ),
		'green2'        => array('label' => esc_html__( 'Green II', 'monarch' ),        'colors' => array('#eeeeee', '#2e2f36', '#3ca376', '#3ca390', '#dddddd', ), ),
		'green3'        => array('label' => esc_html__( 'Green III', 'monarch' ),       'colors' => array('#efefe9', '#323232', '#009276', '#3ca390', '#dddddd', ), ),
		'green4'        => array('label' => esc_html__( 'Green IV', 'monarch' ),        'colors' => array('#f1f1f1', '#262626', '#97b573', '#3ca390', '#dddddd', ), ),
		'darksea'       => array('label' => esc_html__( 'Dark Sea I', 'monarch' ),      'colors' => array('#ececec', '#2c2F33', '#1d7e84', '#0E8AC9', '#dddddd', ), ),
		'darksea2'      => array('label' => esc_html__( 'Dark Sea II', 'monarch' ),     'colors' => array('#e7ebf2', '#1a1a25', '#007f85', '#0E8AC9', '#dddddd', ), ),
		'darksea3'      => array('label' => esc_html__( 'Dark Sea III', 'monarch' ),    'colors' => array('#efefef', '#3b3b42', '#689598', '#687798', '#dddddd', ), ),
		'darkviolet'    => array('label' => esc_html__( 'Dark Violet I', 'monarch' ),   'colors' => array('#eeeeee', '#251f2c', '#6964a7', '#A764A2', '#dddddd', ), ),
		'darkviolet2'   => array('label' => esc_html__( 'Dark Violet II', 'monarch' ),  'colors' => array('#efefef', '#242729', '#666699', '#966699', '#dddddd', ), ),
		'darkslate'     => array('label' => esc_html__( 'Dark Slate', 'monarch' ),      'colors' => array('#f0f0f0', '#3a3a3a', '#8f9e8b', '#74768e', '#dddddd', ), ),
		'darkslate2'    => array('label' => esc_html__( 'Dark Slate II', 'monarch' ),   'colors' => array('#f2f2f2', '#353535', '#74858e', '#74768e', '#dddddd', ), ),
	) );
}

if ( ! function_exists( 'monarch_get_color_scheme' ) ) :
/**
 * Get the current Monarch color scheme.
 *
 * @since Monarch 1.0
 *
 * @return array An associative array of either the current or default color scheme hex values.
 */
function monarch_get_color_scheme() {
	$color_scheme_option = get_theme_mod( 'color_scheme', 'default' );
	$color_schemes       = monarch_get_color_schemes();

	if ( array_key_exists( $color_scheme_option, $color_schemes ) ) {
		return $color_schemes[ $color_scheme_option ]['colors'];
	}

	return $color_schemes['default']['colors'];
}
endif; // monarch_get_color_scheme

if ( ! function_exists( 'monarch_get_color_scheme_choices' ) ) :
/**
 * Returns an array of color scheme choices registered for Monarch.
 *
 * @since Monarch 1.0
 *
 * @return array Array of color schemes.
 */
function monarch_get_color_scheme_choices() {
	$color_schemes                = monarch_get_color_schemes();
	$color_scheme_control_options = array();

	foreach ( $color_schemes as $color_scheme => $value ) {
		$color_scheme_control_options[ $color_scheme ] = $value['label'];
	}

	return $color_scheme_control_options;
}
endif; // monarch_get_color_scheme_choices

/**
 * Binds JS listener to make Customizer color_scheme control.
 *
 * Passes color scheme data as colorScheme global.
 *
 * @since Monarch 1.0
 */
function monarch_customize_control_js() {
	wp_enqueue_script( 'color-scheme-control', get_template_directory_uri() . '/js/color-scheme-control.js', array( 'customize-controls', 'iris', 'underscore', 'wp-util' ), '20141216', true );
	wp_localize_script( 'color-scheme-control', 'colorScheme', monarch_get_color_schemes() );
}
add_action( 'customize_controls_enqueue_scripts', 'monarch_customize_control_js' );

/**
 * Binds JS handlers to make the Customizer preview reload changes asynchronously.
 *
 * @since Monarch 1.0
 */
function monarch_customize_preview_js() {
	wp_enqueue_script( 'monarch-customize-preview', get_template_directory_uri() . '/js/customize-preview.js', array( 'customize-preview' ), '20141216', true );
}
add_action( 'customize_preview_init', 'monarch_customize_preview_js' );

/**
 * Output an Underscore template for generating CSS for the color scheme.
 *
 * The template generates the css dynamically for instant display in the Customizer
 * preview.
 *
 * @since Monarch 1.0
 */
function monarch_color_scheme_css_template() {
	$colors = array(
		'background_color'            => '{{ data.background_color }}',
		'panels_background_color'     => '{{ data.panels_background_color }}',
		'main_color'                  => '{{ data.main_color }}',
		'second_main_color'           => '{{ data.second_main_color }}',
		'border_color'                => '{{ data.border_color }}',
		'light_main_color'            => '{{ data.light_main_color }}',
		'light_panels_bg_color'       => '{{ data.light_panels_bg_color }}',
	);
	?>
	<script type="text/html" id="tmpl-monarch-color-scheme">
		<?php echo monarch_get_color_scheme_css( $colors ); ?>
	</script>
	<?php
}
add_action( 'customize_controls_print_footer_scripts', 'monarch_color_scheme_css_template' );

//======================================================================
// Font
//======================================================================

if ( ! function_exists( 'monarch_get_font_choices' ) ) :
/**
 * Returns an array of Google Fonts registered for Monarch.
 *
 * @since Monarch 1.1
 *
 * @return array Array of color schemes.
 */
function monarch_get_font_choices() {
	$fonts                = monarch_get_google_fonts();
	$font_control_options = array();

	foreach ( $fonts as $font => $value ) {
		$font_control_options[ $font ] = $value['label'];
	}

	return $font_control_options;
}
endif; // monarch_get_font_choices

if ( ! function_exists( 'monarch_font_primary_sanitize' ) ) :
/**
 * Sanitization callback for primary font.
 *
 * @since Monarch 1.1
 */
function monarch_font_primary_sanitize( $value ) {
	$fonts = monarch_get_google_fonts();

	if ( ! array_key_exists( $value, $fonts ) ) {
		$value = 'Merriweather';
	}

	return $value;
}
endif; // monarch_font_primary_sanitize

if ( ! function_exists( 'monarch_font_secondary_sanitize' ) ) :
/**
 * Sanitization callback for secondary font.
 *
 * @since Monarch 1.1
 */
function monarch_font_secondary_sanitize( $value ) {
	$fonts = monarch_get_google_fonts();

	if ( ! array_key_exists( $value, $fonts ) ) {
		$value = 'Playfair Display';
	}

	return $value;
}
endif; // monarch_font_secondary_sanitize
