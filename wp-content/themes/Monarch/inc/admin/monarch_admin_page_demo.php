<?php
/**
 * Monarch Admin Page Help
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.2
 */

function monarch_admin_page_demo() {
?>

<div class="wrap about-wrap monarch-theme">

	<?php monarch_admin_page_header(); ?>

	<?php monarch_admin_page_links( 'demo' ) ?>

</div>

<div class="monarch-demos">

		<div class="update-nag">
		<?php printf( wp_kses( esc_html__( '%s', 'monarch' ), array( 'a' => array( 'href' => array() ) ) ), sprintf( 'Please activate plugin "<a href="%1$s">%2$s</a>" to install the demo content.', admin_url('admin.php?page=tgmpa-install-plugins'), esc_html__( 'One Click Demo Import', 'monarch' ) ) ); ?>
		</div>

		<div class="clearfix"></div>

		<br>

		<div class="wrap">

			<div class="theme-browser">
				<div class="themes wp-clearfix">

					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-main-monarch.jpg">
						</div>
						<h2 class="theme-name"><?php esc_html_e( 'Posts Monarch', 'monarch' ); ?></h2>
					</div>

					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-posts-atlass.jpg">
						</div>
						<h2 class="theme-name"><?php esc_html_e( 'Posts Atlass', 'monarch' ); ?></h2>
					</div>

					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-widgets-monarch.jpg">
							</div>
						<h2 class="theme-name"><?php esc_html_e( 'Widgets Monarch', 'monarch' ); ?></h2>
					</div>

					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-widgets-atlass.jpg">
						</div>
						<h2 class="theme-name"><?php esc_html_e( 'Widgets Atlass', 'monarch' ); ?></h2>
					</div>

					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-pages.jpg">
						</div>
						<h2 class="theme-name"><?php esc_html_e( 'Pages', 'monarch' ); ?></h2>
					</div>

					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-nav.jpg">
						</div>
						<h2 class="theme-name"><?php esc_html_e( 'Navigation Menu Items', 'monarch' ); ?></h2>
					</div>





					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-woo.jpg">
						</div>
						<h2 class="theme-name"><?php esc_html_e( 'WooCommerce', 'monarch' ); ?></h2>
					</div>

					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-shop.jpg">
						</div>
						<h2 class="theme-name"><?php esc_html_e( 'Page Home Shop', 'monarch' ); ?></h2>
					</div>
					
					<div class="theme">
						<div class="theme-screenshot">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/demo-content-bbpress.jpg">
						</div>
						<h2 class="theme-name"><?php esc_html_e( 'bbPress Dummy Data', 'monarch' ); ?></h2>
					</div>

				</div>
			</div>

		</div>

</div>

<?php
}

?>