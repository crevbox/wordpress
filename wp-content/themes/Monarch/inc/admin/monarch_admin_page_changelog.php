<?php
/**
 * Monarch Admin Page Changelog
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.2
 */

function monarch_admin_page_changelog() {
?>

<div class="wrap about-wrap monarch-theme">

	<?php monarch_admin_page_header(); ?>

	<?php monarch_admin_page_links( 'changelog' ); ?>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 2.0.0 - 27 June 17', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Big back-end update', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Customizer section "Frontend Translation"', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New widget "Monarch Panel: Add Content"', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Covers in the directory of Members and Groups (setting)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.9.9 - 11 March 17', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New customization settings', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.9 - 11 Jan 17', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Visual Composer - plugin update', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Demo Content', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New Settings: WP Dashboard => Appearance => Customize => Logo -> Logo Collapsed Panel', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New Settings: WP Dashboard => Appearance => Customize => Theme Settings -> Login Page', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New Page: Home Shop Page (Visual Composer)  - http://themekitten.com/demo/atlass2/home-shop/ http://themekitten.com/demo/monarch/home-shop/ ', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some improvements', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>

	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.8 - 11 Nov 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Visual Composer - plugin update', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Hide: Share Button - new option', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some improvements', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>

	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.7 - 8 Nov 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Visual Composer - plugin compatibility', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; JavaScript cookie for the "layout button"', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>

	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.6 - 31 Oct 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Added: Visual Composer: Page Builder for WordPress (34$) - compatibility soon (important)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Page Visual Composer', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Demo content for the plugin "One Click Demo Import" ', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some improvements', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>

	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.5 - 26 Oct 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Social Articles - plugin compatibility', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: BuddyPress Activity Plus - plugin compatibility', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some fixes', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.4 - 1 Oct 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New option: Position for logo: left, right (px)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some improvements', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Page Fullwidth', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php printf( wp_kses(esc_html__( '&mdash; How to change the icon in the BuddyPress menu - %s', 'monarch' ), array( 'a' => array( 'href' => array() ) ) ), sprintf( '<a href="%1$s" target="_blank">%2$s</a>', esc_url( 'http://themekitten.com/demo/monarch/documentation/#ChangeIcons' ), esc_html__( 'documentation', 'monarch' ) ) ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Updated: Monarch Package', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.3 - 25 Sep 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; jQuery iCheck for BuddyPress: Selecting/Deselecting all notifications & Selecting/Deselecting all messages (added)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some improvements', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.2 - 18 Sep 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Monarch "Atlass" UI demo - http://themekitten.com/demo/atlass2/', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Fixes: UI "Atlass"', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Fixes: Header Panel', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.1 - 10 Sep 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Fixed: delete account (BuddyPress)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Widget "Monarch Panel Menu": new settings', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some fixes (/monarch/css/main.css)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.8.0 - 5 Sep 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New feature: Post Avatar', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New feature: Excerpt Content', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: button minimize Header Panel', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New feature: UI Kits', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Page One Sidebar', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: UI Kit "Atlass"', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Widget Area on Header Panel', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Improvement: Monarch Customizer', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Changed: menu "Categories Menu" => widget "Monarch Panel Menu"', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><strong><?php esc_html_e( 'Old style Header Panel:', 'monarch' ); ?></strong></p>
						<p><?php esc_html_e( '1. WP Dashboard -> Appearance -> Widgets', 'monarch' ); ?></p>
						<p><img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAb4AAACVCAMAAAAQT2+aAAADAFBMVEX////6+vrl5eX9/f34+fj//9YjKC1yd3yIv+vT////6cHpv5C86f/SoXzo//+ed5D//+pyodb/1KQjKGiId6Sed6Tx//////JgreirYy36//////qs6v/7+82BKC2e1P/8+uX//96DzfyIjMHlrGZyd4Gv4f/js4z/z4l2d3xfKC4qKC3O//////fxx5jrr2j/880jKYjaqoTd////3KwjKDTL+vp3jKwjY6zpyqD3//96qtv75q0jKIXM+f9yntAjg8ynf5JxfIjO7//v+f57oNGEKS3/+tEkhtF6kMJzepN9e33Kgi7B9P+HtOX86rj80Z+8jJCqjIH/9dueeHyz5/7G8fyAs+HOrZLQooWHe3zX9//l+vqn1O7+4bPZr5KahIBJKS3l+v/g+P7K5/pPpt7548N2hJ9UXWep5vrHycv+8sRyjcJ7iLWMgawkUqeFjJD5x4Sm3f+cz/H97dByfKOvh4u2iYiZzvq+4PWt2/SZweP00aN/gYPSmUmXQi98Ki2k2P7//+51lskld79zjLIkLoEjKEhhTzY7Ki2Qx/L9+vBsufCJtNF6m8aAm8GKeJQjRZPDl4eWfofInYbNnm4kNl8jKlWjVS3e7/X68d/y3bKylX5+WDpSMy648P7T8vx8w/f/+MN5eqjixaSWep/ivILFlH2KeH0uSW/dpVVsKS63bi3v8vGoy+Tu49Ngos2yz8urq8v23bt4l7btz7J0g6i6jn8jLXSX1f5AmdWIqNSBnsxFfruGhrYjabZvp7PPxKTgv6BAap9VcZKefIYxX4amgIM3VX2/fy1DNy206ubCrsSHrMKswbLK07Gtm46Wj4jxuHKdcFIoP02e1fqUx+6e0cWKtbjHrIo6d4dnXGlaLGmsgV9ST0p/Z0mFRi7Ohi3HzujF7tCviXaJhXO5pGPOhjPp/OKSx9XZxtGxs5y3x5OMi3+Vg3yIemGbZ0BiOzHl98pSisavjq2ZjKDDn57v0JQ9KFy9fU1/Lk3Ihi3K+tPX6cHBtal6fS2SugV1AAAQt0lEQVR42uyYSWgTYRTH38F8QZEgDkwqxkODS3QcXOqgBjcUYw6KQhURQRiwIhVM3A6KtD1EE7EecjDxYOKCCI0bLkjtpYqluOCCC2Ld14Ii4lVB/H/fWCYRCUlFpPJ+UPIyvPSD7zfvfW+GGIZhGIZhGIZhGIZhGIZhGIZhGIb5fzkVJubfUn+u3w42GQmdFJ7IlIn05wwboQWIqYKRU8dsocoIGZ0+KU3E6vDRJFqH2x3h8vrMY09WkMMkAdr26RXrK4jdc4gpz87x3m0V7tLRltlL8NErtIC70+X1uVnQ1x1/d0Mk9Ar1eZqFOEJMWeqXe73ehZW1T/9JKyi31cK2wmWsrkp9NUSTX2Y3V6gvZPRM6NSJKYO5ygvGPKRKgIEGomn2fDsqd7eDQgIBzUyJtkfQVxzVP9tjXduCBKAFXH30TQSH7U8J6+padeX4FxXR+RdGsmttsT5zcfbmAfUNq+zMR2nY/rxoWxkmc1zKsK7tIEa1Tsm21VQBMIfWN0m72YzCK1hBR989IxN/lzcgzY0WvczcupJLBod8+pLs2rjA1ScLeNbTC43PjJgPV9Zfi+cESixkd599nE/PcfWhujv1e6JB6etpEdHaEdnLd+5bNeT5evHsQZuPxerxNM9e4j8Zq+vVAv6TOAehD9e0sUTL7CkTnSiEiArSwtITCf2X5umfaqcH79WlRm0FrsTq8Pv2Q7XNM3y4D0Srmy6/0YYTSMAqVk2Y7iVV404PNgfJuQk3D1MtvclLR1s6MHu2jop0+pS+NRMSYefsc6PaA0Ixw1eiT9KOQfTM3RfP0VKdXmxe1wLTbKGo6UuXflFf5uLkJeiDZaQJBe6NrXc/PzcE66saeGsIGUFaOiG6bE8DSX3qz5HmRp6I9qgRvA7/MnluPK0jMNouNELaz9PwlRYIiRmNkrVu+qy8UHQ4q+BUzO5T/1IP2ZnpZ9+zvupBN0s0tR+i2gPp905dRGnZnnl9+gwVqepD0m8nT9WBUUFmiT51ppamF0RXPB7/lEcNylXwAyzolKVMKbC+/r0VScljqimbggO5sRgxpKpNGFiOtuxe4kTUq2aOdQt+qy+SHoxfFevzROTjhPnB7bXylCUpzTri6EOybM1DtiNF3j+srz8UhDMMGthMpQ8qRSb++QEGFjdCd7W6bj+DCnOx6IkPLdGH4fPqxycpV5+MjOyu7zn3qERNJ3SnWyd0tQqGWdF97q0dxCrdt3PPWV8/wLbKHobiwVz4U59//w3r6hu0zKKItt43nBdkQ3KivVQfLhmZLd+K9aknRqtnB/Xpg/XWn91aO6RWUY+SIjPXR/X3jczK0azv76DexjADldEYFpmBSBOGxRdGmt+HDEyOpwzxYNdhYpgf7FtLTFNBFL0Lm1daWkojbfLeghLTNk2kadpGg00gVkhDCYgo8gtq1MhCJAYFUWSBxA0GNcBGcWGCRjdq/LLQEDUxLvwv/K11ofGzUteemc60IIgWqxvnLCjzZu6ZM/fMr+ShoKCgoKCgoKCgoKCgoKCgoKCgoKCgkAXWN2iDNBOmgnxaHCxL83LQLHdwVrtoISQKahc1clPRzwdgNdNfhVXTtOTI1kw5ptOcQWw6Zhclx9Rh/mtigzkbX0wbNC3ery/UzKn18aK/Jxs39z4NaCvXiEIIecwSlq42pwZtj07z4j+0zwgP059B8p983dOWKc8zCGdn2r59A4M8yQPZ2YcR7h3rXtC+OF8cibHJLDzYdPziKuPqxUWvY6PlOTkLiTzNyfx/bB+Fzpbnxj4qbakj9+ZAvH+VTdN68s60aofbyLLMTo51+RgEW6HFm3bBQjz4UKEzxR8Qt2OLhhCyjYxqN9rI3VzjvWUn26OGTvvehkCy0bL0Sau3Tk+P0G82BDOrMPONumN/2r7bvT58BF+haUqLqeDyZ28M/MXEcgygogaLDSEfa/CQSlYjBOC8fBmZBSnTdq3ZzGpQhBOPW83WQv4Y8XsbtORGAnZ2RQS1v0KHkEPNAe8LDGg8kDzA7EsRO3ox8SzTEYLkp4H4C06TmmUIiL8syuP86b4uo+9tUoLUbOsj97lhSN4o8kXh4RzZR84hl7+qcn1DNys7Hp5wt8SkfXL1SfsesCwHY2gXKmjXTzb0ka1nI2tvurLVMzWM0hpaf3+PvmO75Xyda+3x/Bn2SWZesdHRu0ef6IpI+wrrK3SM7g2aprSYNhytDN3rnmmfv3qrUbbLDgKdgGg1NAGSF0yCNFFwgNaeN9PBlaeNiaXlpg1VLoJ9eGyUDS0Z3UMpBAsltYULCaIZkS3ZaBychCf5gtgaQ6LqCLjarkN5yepGQQClR1p68hh/pq9rW48gSEiQmuvR+1gVy6rIFzkrcmXf2mXvVtei0CTKGNOP9nGwB8EYclSLdhg706Db6ngiBZutiQhG8L1sOSZcsbDPuAonObOsiFa7sBn70vaZIKE+hqYlKS2mAiQpbM7YB3NBwUPKpUoOyQsmQcq1+c28e0x80z0fMfugHVpW2JqYS7yFJEG3CK/H3kB8CI6pboxcEO+cjmDgmRQ4el/olBaUwOoD/6y+MBuEBKk51BVxviqK1DeRyBeSbs/V6qvA5QIo5OXrzwa0n9uXKCqHPWjHQ9lsMvOP0jMIQ7LNkpT7gtwDYMe+IphlhVNjKE7bB8ccvT7kUWjh6bNK+zKnjE2GUHRI2JDhFaQIY7GQyz9TVOhBA5L5nlG26TOqtH1rp+0IL71bc2wbnvJeECSI3aM+yzLelefbQE0yH7tvsl0I4j9Y01l94ZmQIDWjerNv3NfSTSJfZMmRfZg4faaiclmOrtxeyebytJ1KeufaR/6RdT60w1ROrT4hJ3j0RKVV2BebZZ/Mu2CWFVGsUSBjX+Lsk6pVaCq0zLSvXqy+WpILliGB9fkDLycVCsJm9+ggXxFp+5rS34/6Zq++cB0Px0nFtxOMvBFBgpjqm6zsoIKP/Rc83KadYz4usRa/LuX2ZfqCKBw1QoLUTP73XyLB91/ycm2fcelU1SoKV1WKcnDI5Rhn50S74U9y+6K7IkyQsC90vtPF2oXG2Nk3KOWEm/STp7h9rAZn3xz7JLOoKMEJOcs+CnsHWVOhRdrnH6oM3UctED6Kc6TaLkJQ27Ffx81T8LK7nCQ93kgTYxgLO4+62qR9ITzmcLcMZ84+dveutgshB4dcOJwMf6cdQYKYEl+/shkF9gPGBD8EPPu6+c0VZ9/rSW6f7AvnYKUHiRASpGaK7r5Jlt23dJmv3Jx9Gra1dh3GjAe0jjUsXZ5T2o070FxWE3/PN08cBNpgxj7a/BxxqZsn9hApJ8QvWqzEazrm2ieZZYVli+a95Zph304MCk2FFmkf7pKHESUuel7sbiIEuP5JwwPBi38LNwtSut7qvbnZTKV3+bVP2kcTrVq87tA5zXvTRcSvk3y7TfajCNZgAK3Z5VkEyVQYNr6oYW6NdwQ70n125RYjSl5ObZ6Zvh63cvoy3MQhQWjGAVJMJW+H0/liN89oISn8BAbu6r9A6fhzmgPkd267QVoY2X9dTGCv8BeTwvwwyibL6VfADfF37Ns0Hcm1fQYWn/tbHinMgxC20JXbKQssYJ9jqgNU2dinoKCgoKCgoKCg8P/ib96IcVdXWASscR8Bhq3TnoV98778wF+pkJj9+kNW9h1pbmV/jMsGoaL/9PuvdSDGUz9QnYV987/84PxhBmRef8jKvpLv7Jy7S2NBFMan27vMbmBhuQtJsUpQSSGIBiwsxBdCxAjGB77FVk3hA3w2io2lpY2FWFpqk0YFOwtLa0v/B8FzzsxH7oF0NuKdDyaZSc7kwnzck3DOj4zX5s35UwbrL4U7fTXZF0EuOl/yGcAPtnepUPoF+MHenW3/bDor9G8yvECvJLrnAAGEMRCkQmYcofEHBAKT4GL3nmMe2h9mfwzFDBD40kbntAEYgQpw/wKVzzcN15JberaWW4ZizVMIJsEV4x6TMtmorUzJp/qezwB+sKW9XNtx7OAHWk1S1agcV14FXpj4n7APIIAwBnL38Uyk8QcEekzC3OxmDy67hHmwJ3P/3mIBCOoddRK2MIdwcFiLKd78vh3oeOweqDxuaJ7CdfrWV0zqZKPVKp3uKXUwAD/YXvLTZSPuqZMFrlkEeKEOPwAEEMZA7OOZSOMPCNT8BDMPzXKHAiBIcFvYwld140KiZESapxD7Fv9OmdTJRrmZHepdkH2AH+wfOUQPP1jQPGifJuAHgADCGIh9bkbS+AMC0enbej4sbDNcArfQA+OrOBewha+KQVF+aJ5C7KMWH+XalMlG1A25n+azA/zg7fPwA6+Ka7GyD4cOEMAxBmyfm9GTxh8QiD5738K1Y4MYN9D25ej2Sn52Y/s8T6HsM5xrTbpkI8pmpVE+O8AP3j4HP9CKGYH9bOVV25eEFxxjwEiFm9Gjxh8Q6O3ruJqibpzYx+/Qd1/dPsII9wf5l6ff0tg+zVMAeR2rpu2/19iOYi3LZ+fhB9jn4AdZCdfaqu1LwgvCGAhSIbMjIio0/oBAb19uSeBWXsk7ZeBOAN8pM2NLY/s0TyGYRHGEdwV9WsP5tOWwb6ViqIAFBQUFBQUFBQUFBQUFBX2wR8dEAIBADMDg698zK0gol1gIAAAA8KWhynpkNkUm+orpq6av2tsXfV0mufb0ldF32LGDkLTiOIDj/CIenfcuBnsi4akOtYNisEBGtItgMHCHajDMmHgQcZd2MHexRHfwYNrByougzRR3qUAKGrEpO9kOW27rsEWHHVo7rI3B9nvP954PCZmyDf7x+xJY/v6+w/vw/7+Q6YiP6YiP6YiP6YiP6YiP6YiP6YiP6YiP6YiP6YiP6f4qn6DXwZ90c3AAqP/CZ+S8TnzxBHl3R3xGjjO9eNSOD5eb0oDlOCtQ/4pvcRtfhg8vOuQLVQu1rbX2fNzuOMDthQrxte/b0QOx5+ud84V+WQEMySPcfZH4qrijBP3e2ZB3HeY+DJnKTrAd1Hj3nXdhk0vQF3HgaHxuzAz2jaiyxnISXvYD3EtxeAGV71bdDeDLLlmbk1cpXG/otzauQElF4hKfHzrnG/sccsC1+hvkC/CbPd+31gR9dn500AXPNvOZcBRsi31g6M/OFEqCnp8ZHdxR+TzBqLrmbiHpnbJvFPPxkEPlKyZdEHlZRCx1YpnNhNPE19JIDfWK493w/VzQQWDHx7uFCReIO0rQu9DLqhyXNu8UTF68xr/kgcIXifPXlTUWp/iO8YYZ0E7l0w3HzPZ6FT+iTtLiVYjvkuMTj85u+Mw5y/mPbeTDeyvdc/xBJcv0w4/vK5xIMy3famUg/+vCJfpAu4Z32zgxDZ8nOJDbPUWs5gTfthLfJcenH7rjs9c/eZ3N3ZeWlU6T5RX8VaLxJR638oWq+V4waNYgnzEmiTT5IPD2WDoq5YmWL0B8mjyl3i75Ik9x//iUZ1/MISudT5R7MvcbNCPHB/OFJxo+ZT9q1hjF/ft1pVCa1vBNhmNmEUszQT6w4UP0C/G11B0f+EIOic8Qr5j2Z0FRyqwm9uSdBXNnQ8u6Vj5Mswb5YHSJOyz2avgMSflxKU9kPk/KtH9CfPSl2dWO+JiO+JiO+JiO+JiO+JiO+JiO+JiO+JiO+JiO+JiO+JiO+JiO+H63c8c2AIAADMMA8f/NMNCdtZL9QvZUk6/azecs0Sv50m9R5dVLvrkpMpMv/agyAAAAPh2gnWmpMSgVgAAAAABJRU5ErkJggg=="></p>
						<p><?php esc_html_e( '2. WP Dashboard -> Appearance -> Customize -> Theme Settings', 'monarch' ); ?></p>
						<p><img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABRCAMAAABfaGvsAAAA4VBMVEXu7u6rq6uTk5PR0dHf39/d3d1VVVX////m5ubr6+vCwsJ3d3fk5OS0tLSBgYG0ub6Kiort7e1XV1fn5+eQkJDZ2dmioqJcXFz39/eAgIBzc3NgYGDW1ta7u7uEhIR9fX1mZmbh4eHq6urMzMzIyMjGxsabm5vLy8tvb29ra2uvr6+lpaWJiYl2dnbp6enExMSHh4e9vb2NjY16enqWlpadnZ2YmJjhTUPe3t6pqanPz8/40c7hUUj/+/q3t7efn5/lZl22trbwo57si4X79PP75OO/v7/jWlH1wb3odm786umf+RwqAAAHf0lEQVR42u2ZZ3faMBSGr2SbYiPTeFKzVxhhhwQyunf7/39QJfUaDG7aKuk6PXo+JCcaV+KxMOE1aH4aCzRaFkfLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDLUkDL+lvkZT0+AjTfkWVDBlvL+q4seJIBtKzvynryKOXZ69snR7KK5VPIQQjkUBnxcAgFCc2uY63rcJX4iz8i69mb7Zu3O1lOgeN8R0XxxAAAs2z+nCyjWqgau35eu2bAIXO6uL8s5Gxj/5mT9Xr78t2TvSzczy+SNT9p2M83d/XjKXbUZeUX/t2ynglXH58+/fjoWJZZMtxp4F0Sz0r86hBgWPWbgyNZQVypnXVtt+0nV+kIbDRLkzaRss6H0IjxxeAvPqPAa4qhfisyx4VCuZiZ9mKCR5J3glF61a6cebgICfmwhidk2W6tchnIgSWTFgpjE+CmxQBIDMbM58XkasZJETg3a7+9AFkeJ6rKun15++jR7Yftm2ffklVs9o2kw8rP7dOkY5VO7V77SNYkZFa5XyRWQAeAI7DRHF3gfm5GrUFwKKtRiuzFzPIm007XTZg4WdlpUpas554HRm0S9EZ1XIRUVnavWheyVonFQhdlYWG+ZSgub6Lqe9tqrbKyGn27UWKiPE5UlcXvVR9u373cvvz8KCtL3la4LBg2pw70pwFfv+dSDyD+uqNyQTA2WVIX1xEN4whsNKs9kFw143g09Db93T1rHPGy4IUNlhji9fWErMw0xB14YLYiY3YF86WDi5BzGzzqclnzk1Pewv/MygI3Fnb4VmRfRtbXyyvKpxNVTxYX9fTp9ik/Xt86WdA9aQbcncAhJH/PMseii3pOq+1TwBHYKOYLbLoCuKk+P2F4srCAHCpve9QRsg6moVd51bgJMSZdRGzumnBZXy9ZuXgoq75kxBV/SEsZWVE4roxNUT6dqHzPun263W5fP7pD1k251YdFKK/B2cAD7+ie1Uki4PRbdebQdAQ2pq+aNcXv97VT2MkS1xZPVn13sg6nyRVjEKAsXISEHtjnDpcVTPEMHsiy6eK8zg8lnqxrgN5SaGHJqmOVpax0oqIs+UG4/fT2Dlms2e8nrLNuyKM+6tmno0NZ3qW8L7nTgIU0HYGNu5MVXgRedD6+COydLHDX/J7VuvIuz/k9a1mcL59DZhres+rVnreXhYuQ9qndmFlcFrjNTl4WrJobG+ojx7b4FVitLdaUZ6g+q3dfVaWsdKK6rLefPtw+ukPW2ca2L4hnVAsVOvcaNf9icigLWOj7s6iz9GeEAo7Axt0RMcOKX13xxrO9rO5ZTXzQQXBZ8UOTL1lpBjhtLwsW7UJlksrCRcgyFJ/NQlY3rhTGw2NZ1ngF4oO50F55YisjV74NbVKpka8nCycqy8rw5D/5uhPhsfkhWhbY8cTTEc3PYdRCpsM/BbSsH6JlKaBlKaBlKaBlKaBlKfAvyOLx9k/Fl0qQDaRfmxRQXIcQtdIPkaW8kjp5WdHShDuYnKXr5I1Q54eyMPPxy9Z/I8so3SmLkofLIhBMpsEvlSUjOeqIb+6zDt++WXJH/qArooJKvDSyGTqNQ39t8ZVY0gcwmizXt0/KPdqA+dIFtqxTsisWtUSYgHm7yBlJvj6Gs2OThLHf7uPgA1m4DCbt+DQgbaSbVrmIg68SM4hrfsgO9/dQWSwRGZmQNQpZVO2L6LpL2kYmQwc6qstgnAIZAJBJvi8TeDeoF80uPJ5cUpIWEwF/NKNp3i5PVr4GniwZZpFyEQdnZeEymLTj04C0kVb3ZqOW2blmLHEP9/dwWa79VdaMCxqQ+dRJc885ZuhArwH6zSKhIs4tnnyjr7MPvOtl5lxOWSMGuivGJwuLad4uZeXroywZk9bXJg7G25DA2eXqcvv4NCBtpDGkstjFBjPIo/09UBZErfHKk29DQ1SXLsQP+RMXlBm3kBWc9IwwyPeZaeAte+IhHW76QHfFHCrTxmzenq+xl0Xl7nBw9mSlufrXpB2fBqSNYiqaFe+4rltqF473d29ZyZWIuWUWe7OXFSz7AJ2WkcnQM7KgERMX8n1mJvCOrzfMvQ5NoLzYqSy2mM4BXJrJ23M18rJwcFYW5uqYtOPTAGzkU7ODwT2PGCFH+7uvLNZy7fcVIYuV+3tZQMqd4HpsZDL0rCyrubTgG32ZwLtfnYCxvrCB7opZs4ZdX9M0b6/PolwNlDWIvVQWDs7Kwlwdk3Z8GiAb87IGL2wrIZn93VcWZt9+HDqdUoE/1M3ICgZ++9XUyGToWVl2GNrwjb5M4G2OHGBrV3YFF37N5cWGI//EpWne3t34Z8c1UJbRHnVQFg7OysJcHZN2fBqAjceyopm/vCSH+/st/2dF6+iOnm54CvckjuEf5MGyWHp88gzLDO6FvRjfwD/Iw2Q1KoXK5g4jxfLIuJf9xPerCw/+QXTqoGVJtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtCwFtKy/ifUF+evLFD6AyXUAAAAASUVORK5CYII="></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Fixes and improvements', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Coming Soon: Monarch "Atlass" demo and more', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.3.3 - 20 Aug 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Important: WordPress Masonry update', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; WooCommerce: added styles for MyAccount navigation', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; TGM Plugin Activation update', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.3.2 - 21 Jun 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: WooCommerce - plugin compatibility (beta)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some fixes and improvements: profile fields (registration page)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.3.1 - 6 Jun 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: iFlyChat - plugin compatibility (community chat, dark theme)', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: BuddyDrive - plugin compatibility', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some fixes: 1.3 update (rtMedia): masonry, animations, create a group, comments widget', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.3.0 - 26 May 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: rtMedia - plugin compatibility', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Flexmenu for profile', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Customizer - Hide post meta', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Customizer - Private site', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Update: WordPress 4.5', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some fixes and improvements', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.2.0 - 28 April 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: WP-Polls - plugin compatibility', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New:  Monarch WP Dashboard - Welcome, Changelog, Help, Plugins pages', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Customizer - Responsive Settings', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Envato Wordpress Toolkit', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Documentation - theme', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Documentation - pages', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Support Forum', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Upgrade: BuddyPress 2.5.2', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some fixes', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.1.1 - 20 April 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; PHP 5.4 fix', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.1.0 - 7 April 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: TGM Plugin Activation', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Customizer - New options', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Customizer - Typography settings', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Customizer - Logo Font settings', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; New: Login Page', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Some fixes', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<br>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope="col" class="manage-column column-name column-primary"><strong><?php esc_html_e( 'Version 1.0.0 - 30 March 16', 'monarch' ); ?></strong></th>
			</tr>
		</thead>
		<tbody>
			<tr class="inactive" data-slug="multi-device-switcher">
				<td class="column-description desc">
					<div class="plugin-description">
						<p><?php esc_html_e( '&mdash; Initial release', 'monarch' ); ?></p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

</div>

<?php	
}
?>