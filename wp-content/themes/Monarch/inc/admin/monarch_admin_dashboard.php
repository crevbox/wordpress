<?php
/**
 * Monarch Admin Page Welcome
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.2
 */

function monarch_admin_dashboard() {
?>

<div class="wrap about-wrap monarch-theme">

	<?php monarch_admin_page_header(); ?>

	<?php monarch_admin_page_links( 'welcome' ) ?>

	<h2 class="monarch-title"><?php esc_html_e( 'What\'s New', 'monarch' ); ?></h2>
	<div class="vc_feature-section-teaser">
		<div>
			<img class="vc-featured-img" src="<?php echo esc_url( get_template_directory_uri() ); ?>/inc/admin/img/screenshot.png">

			<h3><?php esc_html_e( 'Visual Composer: Page Builder for WordPress', 'monarch' ); ?> <span><?php esc_html_e( '(save 34$)', 'monarch' ); ?></span> </h3>

			<p><?php esc_html_e( 'Visual Composer is an easy to use drag and drop page builder that will help you to create any layout you can imagine fast and easy - ', 'monarch' ); ?><strong><?php esc_html_e( 'no programming knowledge required!', 'monarch' ); ?></strong></p>
			<p><?php esc_html_e( 'Visual Composer page builder comes with 2 types of intuitive content editors - Frontend and Backend. Create your WordPress website with backend style editor or move your page building process to frontend and instantly see any changes you make.', 'monarch' ); ?></p>
			<p><?php printf( wp_kses( esc_html__( '%s', 'monarch' ), array( 'a' => array( 'href' => array() ) ) ), sprintf( '<a href="%1$s" class="button button-primary monarch" target="_blank">%2$s</a>', esc_url( 'https://vc.wpbakery.com/' ), esc_html__( 'Open Website', 'monarch' ) ) ); ?></p>
		</div>
	</div>

	<div class="monarch-plugins">
		<div class="under-the-hood two-col">
			<div class="col">
				<span class="dashicons alt dashicons-welcome-write-blog" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'Social Articles', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'With social Articles you can create posts, attach the featured image, set categories and tags, and more! ', 'monarch' ); ?></p>
			</div>
			<div class="col">
				<span class="dashicons alt dashicons-camera" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'BuddyPress Activity Plus', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'Add buttons to the BuddyPress activity feed that make it super easy to share images, videos, and links from around the web.', 'monarch' ); ?></p>
			</div>
		</div>
<!-- 		<div class="under-the-hood two-col">
			<div class="col">
				<span class="dashicons alt dashicons-analytics" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'One Click Demo Import', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'Import the content, widgets and theme settings with one click. This plugin will create a submenu page under Appearance with the title Import demo data.', 'monarch' ); ?></p>
			</div>
		</div> -->
	</div>

	<h2 class="monarch-title"><?php esc_html_e( 'Plugins', 'monarch' ); ?></h2>

	<div class="monarch-plugins">
		<div class="under-the-hood two-col">
			<div class="col">
				<span class="dashicons dashicons-groups" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'BuddyPress', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'BuddyPress helps you build any type of community website using WordPress, with member profiles, activity streams, user groups, messaging, and more.', 'monarch' ); ?></p>
			</div>
			<div class="col">
				<span class="dashicons dashicons-cart" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'WooCommerce', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'WooCommerce is a powerful, extendable eCommerce plugin that helps you sell anything. Beautifully.', 'monarch' ); ?></p>
			</div>
		</div>
		<div class="under-the-hood two-col">
			<div class="col">
				<span class="dashicons dashicons-format-chat" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'bbPress', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'bbPress is forum software, made the WordPress way.', 'monarch' ); ?></p>
			</div>
			<div class="col">
				<span class="dashicons dashicons-images-alt" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'rtMedia', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'Add albums, photo, audio/video upload, privacy, sharing, front-end uploads & more. All this works on mobile/tablets devices.', 'monarch' ); ?></p>
			</div>
		</div>
		<div class="under-the-hood two-col">
			<div class="col">
				<span class="dashicons dashicons-admin-plugins" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'Envato WordPress Toolkit', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'WordPress toolkit for Envato Marketplace hosted items. Currently supports the following theme functionality: install, upgrade, & backups during upgrade.', 'monarch' ); ?></p>
			</div>
			<div class="col">
				<span class="dashicons dashicons-chart-pie" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'WP-Polls', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'WP-Polls is extremely customizable via templates and css styles and there are tons of options for you to choose to ensure that WP-Polls runs the way you wanted. It now supports multiple selection of answers.', 'monarch' ); ?></p>
			</div>
		</div>
		<div class="under-the-hood two-col">
			<div class="col">
				<span class="dashicons dashicons-thumbs-up" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'BuddyPress Like', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'Gives users the ability to "like" content across your BuddyPress enabled site.', 'monarch' ); ?></p>
			</div>
			<div class="col">
				<span class="dashicons dashicons-portfolio" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'BuddyDrive', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'BuddyDrive is a BuddyPress plugin that uses the BP Attachment API to allow the members of a community to share files or folders.', 'monarch' ); ?></p>
			</div>
		</div>
		<div class="under-the-hood two-col">
			<div class="col">
				<span class="dashicons dashicons-twitter" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'WordPress Social Login', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'WordPress Social Login allow your visitors to comment and login with social networks such as Twitter, Facebook, Google, Yahoo and more.', 'monarch' ); ?></p>
			</div>
			<div class="col">
				<span class="dashicons dashicons-visibility" aria-hidden="true"></span>
				<h3><?php esc_html_e( 'Simple Post Views Counter', 'monarch' ); ?></h3>
				<p><?php esc_html_e( 'This plugin will enable you to display how many times a post has been viewed in total. The total views are displayed in entry meta of each post.', 'monarch' ); ?></p>
			</div>
		</div>
		<div class="button-wrap">
			<?php printf( wp_kses( esc_html__( '%s', 'monarch' ), array( 'a' => array( 'href' => array() ) ) ), sprintf( '<a href="%1$s" class="button button-primary monarch" target="_blank">%2$s</a>', esc_url( 'http://themekitten.com/demo/monarch/plugins/' ), esc_html__( 'Documentation - Plugins', 'monarch' ) ) ); ?>
		</div>
	</div>

	<h2 class="monarch-title"><?php esc_html_e( 'Scripts & FrameWorks', 'monarch' ); ?></h2>

	<div class="under-the-hood three-col">
		<div class="col">
			<h3><?php esc_html_e( 'Bootstrap', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.', 'monarch' ); ?></p>
		</div>
		<div class="col">
			<h3><?php esc_html_e( 'flexMenu', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'flexMenu is a jQuery plugin that lets you create responsive menus that automatically collapse into a "more" drop-down when they run out of space. When there is only space to display one or two items, all the items collapse into a "menu" drop-down.', 'monarch' ); ?></p>
		</div>
		<div class="col">
			<h3><?php esc_html_e( 'Masonry', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'Masonry is a JavaScript grid layout library. It works by placing elements in optimal position based on available vertical space, sort of like a mason fitting stones in a wall. You"ve probably seen it in use all over the Internet.', 'monarch' ); ?></p>
		</div>
	</div>
	<div class="under-the-hood three-col">
		<div class="col">
			<h3><?php esc_html_e( 'HC-Sticky', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'Cross-browser jQuery plugin that makes any element attached to the page and always visible while you scroll.', 'monarch' ); ?></p>
		</div>
		<div class="col">
			<h3><?php esc_html_e( 'iCheck', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'Highly customizable checkboxes and radio buttons for jQuery and Zepto.', 'monarch' ); ?></p>
		</div>
		<div class="col">
			<h3><?php esc_html_e( 'imagesLoaded', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'JavaScript is all like "You images done yet or what?"', 'monarch' ); ?></p>
		</div>
	</div>
	<div class="under-the-hood three-col">
		<div class="col">
			<h3><?php esc_html_e( 'jQuery Scrollbar', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'Cross-browser CSS customizable scrollbar.', 'monarch' ); ?></p>
		</div>
		<div class="col">
			<h3><?php esc_html_e( 'Modernizr', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'Modernizr is a JavaScript library that detects HTML5 and CSS3 features in the user"s browser.', 'monarch' ); ?></p>
		</div>
		<div class="col">
			<h3><?php esc_html_e( 'Nanobar', 'monarch' ); ?></h3>
			<p><?php esc_html_e( 'Very lightweight progress bars.', 'monarch' ); ?></p>
		</div>
	</div>

	<div class="thanks-wrap">

		<hr>

		<h2 class="monarch-title"><?php esc_html_e( 'Thanks', 'monarch' ); ?></h2>

		<i class="dashicons dashicons-heart"></i>

		<i class="dashicons dashicons-star-filled" aria-hidden="true"></i>
		<i class="dashicons dashicons-star-filled" aria-hidden="true"></i>
		<i class="dashicons dashicons-star-filled" aria-hidden="true"></i>
		<i class="dashicons dashicons-star-filled" aria-hidden="true"></i>
		<i class="dashicons dashicons-star-filled" aria-hidden="true"></i>	

		<h3><?php esc_html_e( 'Thanks for purchasing our awesome theme.', 'monarch' ); ?></h3>
		<h3><?php printf( wp_kses( esc_html__( 'If you really love our theme don\'t forget to rate it on %s', 'monarch' ), array( 'a' => array( 'href' => array() ) ) ), sprintf( '<a href="%1$s" target="_blank">%2$s</a>', esc_url( 'https://themeforest.net/downloads' ), esc_html__( 'themeforest', 'monarch' ) ) ); ?></h3>

	</div>

	<hr>

</div>

<?php
}

?>
