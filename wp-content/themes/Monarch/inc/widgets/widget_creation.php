<?php 
/**
 * Monarch Widget Creation
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.0
 */

class widget_creation extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'creation', // Base ID
			esc_html__( 'Monarch Panel: Add Content', 'monarch' ), // Name
			array( 'description' => esc_html__( 'A widget that displays links to create content on Front-end', 'monarch' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
    	extract($args);
		//Our variables from the widget settings.
	    $title = apply_filters('widget_title', $instance['title']);
	    if ( is_user_logged_in() ) {
			echo $args['before_widget'];
			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
			}
		?>

<div class="creationwidget">

	<ul class="user-menu-create">

      	<?php if( function_exists( 'is_buddypress' ) && function_exists( 'social_articles' ) ) : ?>
			<li><a href="<?php echo bp_loggedin_user_domain(); ?>articles/new"><i class="ion-ios-book"></i><?php echo get_theme_mod( 'monarch_translation_text_55', 'Post' ); ?></a></li>
        <?php endif; ?>
		
       	<?php if( function_exists( 'is_buddypress' ) && bp_is_active( 'activity' ) ) : ?>
			<li><a href="<?php echo bp_get_activity_root_slug(); ?>"><i class="ion-ios-pulse-strong"></i><?php esc_html_e( 'Activity', 'buddypress' ); ?></a></li>
        <?php endif; ?>

      	<?php if( class_exists( 'RTMedia' ) ) : ?>
			<li><a href="<?php echo bp_loggedin_user_domain(); ?>media"><i class="ion-android-playstore"></i><?php esc_html_e( 'Photo', 'buddypress-media' ); ?></a></li>
        <?php endif; ?>

      	<?php if( function_exists( 'is_buddypress' ) && bp_is_active( 'groups' ) ) : ?>
			<li><a href="<?php echo bp_groups_directory_permalink(); ?>create/step/group-details"><i class="ion-ios-briefcase"></i><?php esc_html_e( 'Group', 'buddypress' ); ?></a></li>
        <?php endif; ?>

        <?php if( function_exists( 'is_buddypress' ) && bp_is_active( 'messages' ) ) : ?>
			<li><a href="<?php echo bp_loggedin_user_domain(); ?>messages/compose"><i class="ion-email"></i><?php esc_html_e( 'Message', 'buddypress' ); ?></a></li>
        <?php endif; ?>

      	<?php if( function_exists('is_woocommerce') ) : ?>
			<li><a href="<?php global $woocommerce; $cart_url = $woocommerce->cart->get_cart_url(); echo $cart_url; ?>"><i class="ion-ios-cart"></i><?php esc_html_e( 'Cart', 'woocommerce' ); ?></a></li>
        <?php endif; ?>

      	<?php if( class_exists( 'bbPress' ) ) : ?>
			<li><a href="<?php echo bbp_get_forums_url(); ?>"><i class="ion-chatbox-working"></i><?php esc_html_e( 'Topic', 'bbpress' ); ?></a></li>
        <?php endif; ?>

      	<?php if( function_exists( 'is_buddypress' ) && function_exists( 'buddydrive' ) ) : ?>
			<li><a href="<?php echo bp_loggedin_user_domain() . buddydrive_get_slug(); ?>"><i class="ion-android-archive"></i><?php esc_html_e( 'File', 'buddydrive' ); ?></a></li>
        <?php endif; ?>

	</ul>

</div>

		<?php
			echo $args['after_widget'];
		}
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		    //Set up some default widget settings.
		    $defaults = array(
		        'title' => 'Add Content'
		    );
		    $instance = wp_parse_args((array) $instance, $defaults);
		?>
	    <p>
	        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'monarch'); ?></label>
	        <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
	    </p>
	    <p>
	        <?php esc_html_e('Buttons: New Article (Social Articles), Activity (BuddyPress), Photo (RTMedia), Group (BuddyPress), Message (BuddyPRess), Cart (WooCommerce), File (BuddyDrive), Topic (bbPress)', 'monarch'); ?>
	    </p>
		<p class="no-options-widget">
			<strong><?php esc_html_e('Only for "Panel Widgets"', 'monarch'); ?></strong>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class widget_creation
?>