<?php if ( ! ( get_theme_mod('monarch_private_site') || get_theme_mod('monarch_hide_share_buttons') ) ) : ?>
	<!-- Modal Post Share -->
	<div class="modal modal-vcenter fade modal-share" id="modal-share-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-body">
	        <div class="shareinit" data-url="<?php the_permalink() ?>" data-title="<?php the_title() ?>"></div>
	      </div>
	    </div>
	  </div>
	</div>
<?php endif; ?>