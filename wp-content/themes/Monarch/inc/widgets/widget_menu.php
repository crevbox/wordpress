<?php 
/**
 * Monarch Widget Panel Menu
 *
 * @package WordPress
 * @subpackage Monarch
 * @since Monarch 1.4
 */

class widget_menu extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */

	function __construct() {
		parent::__construct(
			'widget_menu', // Base ID
			esc_html__( 'Monarch Panel: Menu', 'monarch' ), // Name
			array( 'description' => esc_html__( 'A widget that displays menu "Widget Pagel Menu"', 'monarch' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract($args);
		//Our variables from the widget settings.
		$textAlign = $instance['textAlign'];
		$showDesc  = $instance['showDesc'];
?>

<div class="scrollbar-inner monarch_menu <?php if( $textAlign ) { echo 'left'; }; if( $showDesc ) { echo ' showDesc'; }; ?>">

	<!-- Primary Menu -->
	<?php if ( has_nav_menu( 'primary' ) ) : ?>
	<nav id="site-navigation" class="main-navigation" role="navigation">
	   <?php
	    	wp_nav_menu( array(
	        	'menu_class'     => 'nav nav-primary',
	        	'theme_location' => 'primary',
	    	) );
	    ?>
	</nav>
	<?php endif; ?>

</div>

<?php
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		//Set up some default widget settings
		$defaults = array(
			'textAlign' => false,
			'showDesc'  => false
		);
		$instance = wp_parse_args((array) $instance, $defaults);
		?>
		<p class="no-options-widget">
			<a href="<?php echo admin_url('/nav-menus.php?action=locations'); ?>"><?php esc_html_e( 'Setting this menu: ', 'monarch' ); ?></a>
			<?php esc_html_e('WordPress Dashboard -> Appearance -> Menus -> Manage Locations -> Widget Pagel Menu -> create or select menu.', 'monarch'); ?>
		</p>
		<p class="no-options-widget">
			<strong><?php esc_html_e('Only for "Panel Widgets"', 'monarch'); ?></strong>
		</p>
		<p>
			<input type="checkbox" id="<?php echo $this->get_field_id( 'textAlign' ); ?>" name="<?php echo $this->get_field_name( 'textAlign' ); ?>" <?php checked( (bool) $instance['textAlign'], true ); ?> />
			<label for="<?php echo $this->get_field_id( 'textAlign' ); ?>"><?php esc_html_e('Text-align: left', 'monarch'); ?></label>
		</p>
		<p>
			<input type="checkbox" id="<?php echo $this->get_field_id( 'showDesc' ); ?>" name="<?php echo $this->get_field_name( 'showDesc' ); ?>" <?php checked( (bool) $instance['showDesc'], true ); ?> />
			<label for="<?php echo $this->get_field_id( 'showDesc' ); ?>"><?php esc_html_e('Show descriptions', 'monarch'); ?></label>
		</p>
		<?php 
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = array();
		$instance['textAlign'] = ( ! empty( $new_instance['textAlign'] ) ) ? strip_tags( $new_instance['textAlign'] ) : '';
		$instance['showDesc']  = ( ! empty( $new_instance['showDesc'] ) ) ? strip_tags( $new_instance['showDesc'] ) : '';

		return $instance;
	}

}