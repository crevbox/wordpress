<?php
/**
 * Enqueues front-end CSS for color scheme.
 *
 * @package WordPress
 * @subpackage Monarch_Theme
 * @since Monarch 2.0
 */

if ( get_theme_mod( 'monarch_generate_static_css' ) ) {

require get_template_directory() . '/inc/admin/cs-framework/cs-framework.php';

//
// set up cache folder
// -------------------
$upload_dir = wp_upload_dir();
$cache_name = 'monarch-temp';
$cache_dir  = trailingslashit( $upload_dir['basedir'] ) . $cache_name;
$cache_url  = trailingslashit( $upload_dir['baseurl'] ) . $cache_name;

defined( 'MONARCH_CACHE_DIR' ) or define( 'MONARCH_CACHE_DIR', $cache_dir );
defined( 'MONARCH_CACHE_URL' ) or define( 'MONARCH_CACHE_URL', $cache_url );

//
// Enqueue custom styles
// ---------------------
function monarch_wp_enqueue_styles() {

	if( ! is_dir( MONARCH_CACHE_DIR ) ) {

		if( ! function_exists( 'WP_Filesystem' ) ) {
  		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
	
	  WP_Filesystem();

	  global $wp_filesystem;
	
		$wp_filesystem->mkdir( MONARCH_CACHE_DIR );

	}

	// 1. checking is cache folder writable
	// 2. if user in customizer passed custom.css for avoid conflicts
	if( is_writable( MONARCH_CACHE_DIR ) && ! isset( $_POST['wp_customize'] ) ) {

    $has_cached = get_option( 'monarch-has-cached' );

    if( ! $has_cached ) {
      monarch_cache_css_file();
    }

    // check for multisite
    global $blog_id;
    $is_multisite = ( is_multisite() ) ? '-'. $blog_id : '';

    wp_enqueue_style( 'monarch-customizer', MONARCH_CACHE_URL .'/customizer-style'. $is_multisite .'.css', array(), null );

  } else {

  	add_action( 'wp_head', function(){ 	echo '<style>'. monarch_genearet_css() .'</style>'; }, 99 );

  }

}
add_action( 'wp_enqueue_scripts', 'monarch_wp_enqueue_styles', 999 );

//
// Generate cache css file
// -----------------------
function monarch_cache_css_file() {

  if( is_multisite() ) {
    global $blog_id;
    $css_file = MONARCH_CACHE_DIR . '/customizer-style-'. $blog_id .'.css';
  } else {
    $css_file = MONARCH_CACHE_DIR . '/customizer-style.css';
  }

  $css  = "/**\n";
  $css .= " * Do not touch this file! This file created by PHP\n";
  $css .= " * Last modifiyed time: ". date( 'M d Y, h:s:i' ) ."\n";
  $css .= " */\n\n\n";
  $css .= monarch_genearet_css();

	if( ! function_exists( 'WP_Filesystem' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}

  WP_Filesystem();

  global $wp_filesystem;

  if ( ! $wp_filesystem->put_contents( $css_file, $css, FS_CHMOD_FILE ) ) {
    update_option( 'monarch-has-cached', false );
  } else {
    update_option( 'monarch-has-cached', true );
  }

}

//
// Generate cache css
// -----------------------
function monarch_genearet_css() {

	$color_scheme_option  = get_theme_mod( 'color_scheme', 'default' );
	$color_scheme         = monarch_get_color_scheme();
	$color_main_color_rgb = monarch_hex2rgb( $color_scheme[2] );
	$color_panels_bg_rgb  = monarch_hex2rgb( $color_scheme[1] );

	$colors = array(
		'background_color'            => "#" . get_background_color(),
		'panels_background_color'     => $color_scheme[1],
		'main_color'                  => $color_scheme[2],
		'second_main_color'           => $color_scheme[3],
		'border_color'                => $color_scheme[4],
		'light_main_color'            => vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.3)', $color_main_color_rgb ),
		'light_panels_bg_color'       => vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.3)', $color_panels_bg_rgb ),
	);

	$monarch_dynamic_style = monarch_get_color_scheme_css( $colors );

	return $monarch_dynamic_style;

}

//
// Reseting cache for regenerate
// -----------------------
function monarch_reset_cache() {
	update_option( 'monarch-has-cached', false );
}
// update your classes/framework.class.php because this filter i added now.
add_action( 'cs_validate_save_after', 'monarch_reset_cache' );
add_action( 'customize_save_after', 'monarch_reset_cache' );

} else {

	function monarch_color_scheme_css() {
		$color_scheme_option = get_theme_mod( 'color_scheme', 'default' );

		$color_scheme = monarch_get_color_scheme();

		// Convert main and sidebar text hex color to rgba.
		$color_main_color_rgb  = monarch_hex2rgb( $color_scheme[2] );
		$color_panels_bg_rgb = monarch_hex2rgb( $color_scheme[1] );

		$colors = array(
			'background_color'            => "#" . get_background_color(),
			'panels_background_color'     => $color_scheme[1],
			'main_color'                  => $color_scheme[2],
			'second_main_color'           => $color_scheme[3],
			'border_color'                => $color_scheme[4],
			'light_main_color'            => vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.3)', $color_main_color_rgb ),
			'light_panels_bg_color'       => vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.3)', $color_panels_bg_rgb ),
		);

		$color_scheme_css = monarch_get_color_scheme_css( $colors );

		wp_add_inline_style( 'monarch-style', $color_scheme_css );
	}
	add_action( 'wp_enqueue_scripts', 'monarch_color_scheme_css' );

}