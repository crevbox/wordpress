<?php
/**
 * Monarch Customizer functionality
 *
 * @package WordPress
 * @subpackage Monarch_Theme
 * @since Monarch 1.3
 */

/**
 * Enqueues front-end CSS for font scheme.
 *
 * @since Monarch 1.1
 *
 * @see wp_add_inline_style()
 */
function monarch_get_color_scheme_css( $colors ) {

// Customizer Settings
$monarch_ui_kit                   = get_theme_mod( 'monarch_ui_kit', 'monarch' );
$monarch_css_custom_ui_activate   = get_theme_mod( 'monarch_css_custom_ui_activate', false );
$monarch_css_box_corner           = get_theme_mod( 'monarch_css_box_corner', false );
$monarch_css_box_style            = get_theme_mod( 'monarch_css_box_style', 'monarch' );
$monarch_css_select               = get_theme_mod( 'monarch_css_select', 'monarch' );
$monarch_css_widget_title         = get_theme_mod( 'monarch_css_widget_title', 'monarch' );
$monarch_css_buttons              = get_theme_mod( 'monarch_css_buttons', 'monarch' );
$monarch_css_alerts               = get_theme_mod( 'monarch_css_alerts', 'monarch' );
$monarch_css_image_outlines       = get_theme_mod( 'monarch_css_image_outlines', 'monarch' );
$monarch_css_letter_spacing       = get_theme_mod( 'monarch_css_letter_spacing', false );
$monarch_font_semi_bold           = get_theme_mod( 'monarch_font_semi_bold', false );
$monarch_fonts_activate           = get_theme_mod( 'monarch_fonts_activate', false );
$monarch_font_medium              = get_theme_mod( 'monarch_font_medium', false );
$monarch_css_box_border_radius    = get_theme_mod( 'monarch_css_box_border_radius', false );


$monarch_font_primary             = get_theme_mod( 'monarch_font_primary', 'Merriweather' );
$monarch_font_secondary           = get_theme_mod( 'monarch_font_secondary', 'Playfair Display' );

if ( $monarch_fonts_activate == false ) {
	if ( $monarch_ui_kit == 'monarch' ) {
		// MONARCH UI FONTS
		$monarch_font_primary        = 'Merriweather';
		$monarch_font_secondary      = 'Playfair Display';
		$monarch_font_medium         = false;
		$monarch_font_semi_bold      = false;
		$monarch_css_letter_spacing  = true;
	} if ( $monarch_ui_kit == 'atlass' ) {
		// ATLASS UI FONTS
		$monarch_font_primary        = 'Exo 2';
		$monarch_font_secondary      = 'Exo 2';
		$monarch_font_medium         = true;
		$monarch_font_semi_bold      = true;
		$monarch_css_letter_spacing  = false;
	} if ( $monarch_ui_kit == 'theaxis' ) {
		// THEAXIS UI FONTS
		$monarch_font_primary        = 'Roboto';
		$monarch_font_secondary      = 'Roboto';
		$monarch_font_medium         = true;
		$monarch_font_semi_bold      = true;
		$monarch_css_letter_spacing  = false;
	}
}

if ( $monarch_css_custom_ui_activate == false ) {
	if ( $monarch_ui_kit == 'monarch' ) {
		// MONARCH UI KIT
		$monarch_css_box_corner           = true;
		$monarch_css_box_style            = 'monarch';
		$monarch_css_select               = 'monarch';
		$monarch_css_widget_title         = 'monarch';
		$monarch_css_buttons              = 'monarch';
		$monarch_css_alerts               = 'monarch';
		$monarch_css_image_outlines       = 'monarch';
		$monarch_css_box_border_radius    = false;
	} if ( $monarch_ui_kit == 'atlass' ) {
		// ATLASS UI KIT
		$monarch_css_box_corner           = true;
		$monarch_css_box_style            = 'atlass';
		$monarch_css_select               = 'atlass';
		$monarch_css_widget_title         = 'atlass';
		$monarch_css_buttons              = 'atlass';
		$monarch_css_alerts               = 'atlass';
		$monarch_css_image_outlines       = 'atlass';
		$monarch_css_box_border_radius    = true;
	}  if ( $monarch_ui_kit == 'theaxis' ) {
		// THEAXIS UI KIT
		$monarch_css_box_corner           = true;
		$monarch_css_box_style            = 'atlass';
		$monarch_css_select               = 'atlass';
		$monarch_css_widget_title         = 'atlass';
		$monarch_css_buttons              = 'atlass';
		$monarch_css_alerts               = 'atlass';
		$monarch_css_image_outlines       = 'none';
		$monarch_css_box_border_radius    = false;
	}
}

$monarch_font_logo                = get_theme_mod( 'monarch_font_logo', 'Elsie' );
$monarch_font_size_logo           = get_theme_mod( 'monarch_font_size_logo', '26' );
$monarch_font_letter_spacing_logo = get_theme_mod( 'monarch_font_letter_spacing_logo', '4' );
$monarch_font_weight_logo         = get_theme_mod( 'monarch_font_weight_logo', 'normal' );
$monarch_logo_background_position = get_theme_mod( 'monarch_logo_background_position', '0' );

$bp_loggedin_user_domain             = '';
$bp_get_activity_directory_permalink = '';
$bp_get_groups_directory_permalink   = '';
$bp_get_members_directory_permalink  = '';
$bbp_get_forums_url                  = '';

if(function_exists('bp_loggedin_user_domain')){
	$bp_loggedin_user_domain             = bp_loggedin_user_domain();
}

if(function_exists('bp_get_activity_directory_permalink')){
	$bp_get_activity_directory_permalink = bp_get_activity_directory_permalink();
}

if(function_exists('bp_get_groups_directory_permalink')){
	$bp_get_groups_directory_permalink   = bp_get_groups_directory_permalink();
}

if(function_exists('bp_get_members_directory_permalink')){
 	$bp_get_members_directory_permalink  = bp_get_members_directory_permalink();
}

if(function_exists('bbp_get_forums_url')){
 	$bbp_get_forums_url                  = bbp_get_forums_url();
}

$colors = wp_parse_args( $colors, array(
	'background_color'         => '',
	'panels_background_color'  => '',
	'main_color'               => '',
	'second_main_color'        => '',
	'border_color'             => '',
	'light_main_color'         => '',
	'light_panels_bg_color'    => '',
) );

$color_scheme                  = monarch_get_color_scheme();

$monarch_fonts_url             = monarch_fonts_url();

$panels_background_default     = $color_scheme[1];
$main_color_default            = $color_scheme[2];
$second_main_color_default     = $color_scheme[3];
$border_color_default          = $color_scheme[4];

$background_color              = "#" . get_background_color();
$main_color                    = get_theme_mod( 'main_color', $main_color_default );
$second_main_color             = get_theme_mod( 'second_main_color', $second_main_color_default );
$panels_background_color       = get_theme_mod( 'panels_background_color', $panels_background_default );
$border_color                  = get_theme_mod( 'border_color', $border_color_default );

$main_color_rgb                = monarch_hex2rgb( $main_color );
$light_main_color              = vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.3)', $main_color_rgb );

$panels_background_color_rgb   = monarch_hex2rgb( $panels_background_color );
$light_panels_bg_color         = vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.3)', $panels_background_color_rgb );

$colors = array(
	'background_color'         => $background_color,
	'border_color_default'     => $border_color_default,
	'panels_background_color'  => $panels_background_color,
	'main_color'               => $main_color,
	'border_color'             => $border_color,
	'second_main_color'        => $second_main_color,
	'light_main_color'         => $light_main_color,
	'light_panels_bg_color'    => $light_panels_bg_color,
);


$css = '';

// Panels
$classCSSpanels ='
body .sa-upload-image-container,
body .buttons-container,
#buddypress .activity-list .activity-avatar a,
#buddypress #whats-new-form #whats-new-avatar a,
#add_payment_method #payment,
#bbpress-forums #bbp-search-form,
#bbpress-forums div.hentry,
#bbpress-forums fieldset.bbp-form,
#bbpress-forums li.bbp-body ul.forum,
#bbpress-forums li.bbp-body ul.topic,
#bbpress-forums li.bbp-footer,
#bbpress-forums li.bbp-header,
#buddypress #friend-list li,
#buddypress #item-body div.profile,
#buddypress #member-list li,
#buddypress #message-thread .message-box,
#buddypress .activity-comments .acomment-content,
#buddypress .activity-comments .acomment-meta img.avatar,
#buddypress .activity-list .activity-header img.avatar,
#buddypress .activity-list .activity-inner,
#buddypress .item-list-tabs,
#buddypress .masonry li .padding,
#buddypress .notifications,
#buddypress .notifications,
#buddypress .standard-form,
#buddypress .standard-form,
#buddypress div.dir-search,
#buddypress div.item-list-tabs#subnav ul li.groups-members-search input[type="submit"],
.author-info,
.comments-area .no-comments,
.comments-area .pingback .comment-body,
.comments-area article .avatar,
.comments-area article .comment-content,
.cover,
.notifications-options-nav,
.page-header .page-header-content,
.page-numbers,
.pagination-links a,
.pagination-links span,
.panel,
.post-navigation .nav-links .nav-next,
.post-navigation .nav-links .nav-previous,
.post-wrap .hentry,
.post-wrap .post-date,
.relatedposts .relatedpost,
.search-form input[type="submit"],
.timeline-badge,
.widget-area .widget,
.widget_display_search input[type="submit"],
.woocommerce .term-description,
.woocommerce .track_order,
.woocommerce .woocommerce-billing-fields,
.woocommerce .woocommerce-shipping-fields,
.woocommerce div.product .woocommerce-tabs .panel,
.woocommerce table.shop_table,
.woocommerce ul.products li.product,
.woocommerce-checkout #payment,
.woocommerce-page ul.products li.product,
.woocommerce-product-search input[type="submit"],
body #buddypress #item-body .rtmedia-container ul.rtmedia-list li .rtmedia-list-item-a,
body #buddypress #item-body .rtmedia-container ul.rtmedia-list.rtmedia-album-list li a,
body #item-body .rtmedia-container .rtm-lightbox-container .rtm-single-media,
body .rtm-comment-list .rtmedia-comment-content,
body .rtm-comment-list .rtmedia-comment-user-pic a,
body .rtm-pagination .rtm-paginate a,
body .rtm-pagination .rtm-paginate a.rtmedia-page-link,
body .rtm-pagination .rtm-paginate span,
body .rtmedia-container #rtmedia-uploader-form .rtm-plupload-list li,
body .rtmedia-container #rtmedia_uploader_filelist li,
body .rtmedia-container .rtm-comment-list.rtm-comment-list li.rtmedia-no-comments,
body .rtmedia-container .rtm-media-gallery-uploader,
body .rtmedia-container .rtm-tabs-content .content,
body .rtmedia-container .rtmedia-like-info, 
body .rtmedia-container .rtmedia-media-description,
body.buddypress div#buddydrive-main #buddydrive-actions-form,
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback,
body.buddypress div#buddydrive-main #buddydrive-uploader,
body.buddypress div#buddydrive-main .buddydrive-toolbar,
body.buddypress div#buddydrive-main article#buddydrive-item-content,
body.buddypress div#buddydrive-main header#buddydrive-item-header,
body.buddypress div#buddydrive-main ul#buddydrive-browser li .buddydrive-content,
body.buddypress div#buddydrive-main ul#buddydrive-browser li .buddydrive-icon,
div.bbp-breadcrumb,
div.bbp-template-notice.info,
div.bbp-topic-tags,
.messages-options-nav
';

$classCSScorner = '
.item-list .elem .padding::after,
.hentry.course::after,
.author-info::after,
.page-header .page-header-content::after,
.panel::after,
.post-wrap .hentry.format-audio::after,
.post-wrap .hentry.format-gallery::after,
.post-wrap .hentry.format-standard::after,
.post-wrap .hentry.format-video::after,
.post-wrap .hentry.page.type-page::after,
.post-wrap .hentry.type-attachment::after,
.relatedposts .relatedpost::after,
body #item-body .rtm-lightbox-container .rtm-single-media::after,
.widget-area .widget::after
';

$classCSScornerShadow = '
.item-list .elem .padding::before,
.hentry.course::before,
.author-info::before,
.panel::before,
.post-wrap .hentry.format-audio::before,
.post-wrap .hentry.format-gallery::before,
.post-wrap .hentry.format-standard::before,
.post-wrap .hentry.format-video::before,
.post-wrap .hentry.page.type-page::before,
.post-wrap .hentry.type-attachment::before,
.relatedposts .relatedpost::before,
body #item-body .rtm-lightbox-container .rtm-single-media::before,
.page-header .page-header-content::before,
.widget-area .widget::before
';

$css = <<<CSS

/*
  ========================================
  Add custom fonts, used in the main stylesheet
  ========================================
*/

@import url($monarch_fonts_url);

/*
  ========================================
  GOOGLE FONT
  ========================================
*/

#buddypress .standard-form .radio ul,
#settings-form td,
.bbp-forum-description .bbp-author-name,
.bbp-topic-description .bbp-author-name,
.bbp-topic-description, .bbp-forum-description,
.gallery-caption,
.rtmedia-popup label,
.tooltip,
.wp-polls label,
body {
	font-family: '{$monarch_font_primary}', sans-serif;
}

body .learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab,
body .learn-press-message,
body .article-footer,
body .excerpt,
body .select2,
body .titlelabel,
body .article-status,
#bbpress-forums ul li.bbp-forum-info .bbp-forums-list li a,
#buddypress #message-thread .activity,
#buddypress #message-thread strong,
#buddypress div#message-thread strong a,
#buddypress div.error,
#buddypress p.success,
#buddypress p.updated,
#buddypress p.warning,
#latest-update,
#link-modal-title,
#message,
#settings-form,
.aboutwidget,
.ac-reply-cancel,
.acomment-meta,
.activity-greeting,
.activity-header,
.bbp-author-name,
.bbp-breadcrumb,
.bbp-footer,
.bbp-form legend,
.bbp-forum-title,
.bbp-header,
.bbp-pagination,
.bbp-reply-header,
.bbp-topic-meta,
.bbp-topic-permalink,
.bbp-topic-tags,
.bp-avatar-nav,
.bp-login-widget-user-link,
.button,
.comment-meta,
.drag-drop-info,
.forum-titles,
.item-list-tabs,
.item-options,
.item-title,
.load-more,
.nav-links,
.page-numbers,
.pager,
.pagination,
.popover,
.post-categories,
.post-category,
.post-date,
.post-footer,
.post-format,
.post-navigation .nav-links .meta-nav,
.post-tags,
.relatedposts .relatedpost .post-info .category,
.reply a,
.rtm-album-privacy label,
.rtm-comment-wrap .rtmedia-comment-author,
.rtm-comment-wrap .rtmedia-comment-date,
.rtm-ltb-action-container,
.rtm-options,
.rtm-user-meta-details,
.rtmedia-gallery-item-actions a,
.rtmedia-success,
.rtmedia-uploader .drag-drop .drag-drop-info,
.rtmedia-warning,
.standard-form .error,
.sticky-post,
.subscription-toggle,
.tagcloud,
.user-panel,
.users-who-like,
.widget.widget_comments ul li a .commauth,
.widget.widget_display_replies,
.widget.widget_display_stats,
.widget.widget_display_views,
.widget_polls-widget a,
.woocommerce #reviews #comments ol.commentlist li .comment-text p.meta,
.bpfb_controls_container .qq-upload-button,
.woocommerce .price,
.woocommerce .product_list_widget,
.woocommerce .tabs,
.woocommerce .total strong,
.woocommerce .woocommerce-error,
.sa-error-container,
.woocommerce .woocommerce-info,
.woocommerce .woocommerce-message,
.woocommerce .woocommerce-ordering,
.woocommerce .woocommerce-product-rating,
.woocommerce .woocommerce-result-count,
.wp-caption-text,
.wp-polls a,
body .rtm-media-options .rtm-media-options-list .rtmedia-action-buttons,
body .rtm-pagination,
body .rtm-tabs li a,
body .rtmedia-actions-before-comments .rtmedia-comment-link,
body .rtmedia-container .rtm-load-more a,
body .rtmedia-success,
body .rtmedia-upload-media-link,
body .rtmedia-warning,
body a.rtmedia-upload-media-link,
body.buddypress #buddydrive-main .subsubsub,
body.buddypress .error,
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback p.info,
body.buddypress div#buddydrive-main form.buddydrive-item-details #buddydrive-object-selection .buddydrive-item,
body.buddypress div#buddydrive-main ul#buddydrive-browser li .buddydrive-content .buddydrive-title,
body.buddypress div#buddydrive-main ul#buddydrive-browser li.buddydrive-item .buddydrive-share-content p.description,
h1,
h2,
h3,
h4,
h5,
h6,
input,
input.ed_button,
label,
select,
table,
.header-panel {
	font-family: '{$monarch_font_secondary}', sans-serif;
}

.site-branding h1.site-title {
	font-family: '{$monarch_font_logo}', sans-serif;
}

.site-header h1 a {
	font-size: {$monarch_font_size_logo}px;
	letter-spacing: {$monarch_font_letter_spacing_logo}px;
	font-weight: {$monarch_font_weight_logo};
}

@media only screen and (min-width: 768px) {
	body .site-header h1 a {
		background-position-x: {$monarch_logo_background_position}px;
	}
}

/* iFlyChat */
.ifc .ifc-reset a, .ifc .ifc-reset abbr, .ifc .ifc-reset acronym, .ifc .ifc-reset address, .ifc .ifc-reset applet, .ifc .ifc-reset article, .ifc .ifc-reset aside, .ifc .ifc-reset audio, .ifc .ifc-reset b, .ifc .ifc-reset big, .ifc .ifc-reset blockquote, .ifc .ifc-reset button, .ifc .ifc-reset canvas, .ifc .ifc-reset caption, .ifc .ifc-reset center, .ifc .ifc-reset cite, .ifc .ifc-reset code, .ifc .ifc-reset dd, .ifc .ifc-reset del, .ifc .ifc-reset details, .ifc .ifc-reset dfn, .ifc .ifc-reset div, .ifc .ifc-reset div.form, .ifc .ifc-reset dl, .ifc .ifc-reset dt, .ifc .ifc-reset em, .ifc .ifc-reset fieldset, .ifc .ifc-reset figcaption, .ifc .ifc-reset figure, .ifc .ifc-reset footer, .ifc .ifc-reset form, .ifc .ifc-reset h1, .ifc .ifc-reset h2, .ifc .ifc-reset h3, .ifc .ifc-reset h4, .ifc .ifc-reset h5, .ifc .ifc-reset h6, .ifc .ifc-reset header, .ifc .ifc-reset hgroup, .ifc .ifc-reset i, .ifc .ifc-reset iframe, .ifc .ifc-reset img, .ifc .ifc-reset input, .ifc .ifc-reset input[type], .ifc .ifc-reset ins, .ifc .ifc-reset kbd, .ifc .ifc-reset label, .ifc .ifc-reset legend, .ifc .ifc-reset li, .ifc .ifc-reset mark, .ifc .ifc-reset menu, .ifc .ifc-reset nav, .ifc .ifc-reset object, .ifc .ifc-reset ol, .ifc .ifc-reset p, .ifc .ifc-reset pre, .ifc .ifc-reset q, .ifc .ifc-reset s, .ifc .ifc-reset samp, .ifc .ifc-reset section, .ifc .ifc-reset small, .ifc .ifc-reset span, .ifc .ifc-reset strike, .ifc .ifc-reset strong, .ifc .ifc-reset sub, .ifc .ifc-reset summary, .ifc .ifc-reset sup, .ifc .ifc-reset table, .ifc .ifc-reset tbody, .ifc .ifc-reset td, .ifc .ifc-reset textarea, .ifc .ifc-reset tfoot, .ifc .ifc-reset th, .ifc .ifc-reset thead, .ifc .ifc-reset time, .ifc .ifc-reset tr, .ifc .ifc-reset tt, .ifc .ifc-reset u, .ifc .ifc-reset ul, .ifc .ifc-reset var, .ifc .ifc-reset video { font-family: '{$monarch_font_secondary}', sans-serif!important; }

/* User Panel Icons */
.nav-buddy li a[href*='{$bp_get_activity_directory_permalink}']::before  { content: '\\f2d4'; }
.nav-buddy li a[href*='{$bp_get_groups_directory_permalink}']::before    { content: '\\f26c'; }
.nav-buddy li a[href*='{$bp_get_members_directory_permalink}']::before   { content: '\\f212'; }
.nav-buddy li a[href*='{$bbp_get_forums_url}']::before                   { content: '\\f11a'; }
.nav-buddy li a[href*='widgets']::before                                 { content: '\\f111'; }

.nav-buddy li a[href*='{$bp_loggedin_user_domain}activity']::before      { content: '\\f389'; }
.nav-buddy li a[href*='{$bp_loggedin_user_domain}friends']::before       { content: '\\f47a'; }
.nav-buddy li a[href*='{$bp_loggedin_user_domain}groups']::before        { content: '\\f3ea'; }
.nav-buddy li a[href*='{$bp_loggedin_user_domain}messages']::before      { content: '\\f132'; }
.nav-buddy li a[href*='{$bp_loggedin_user_domain}notifications']         { padding: 15px 10px; }
.nav-buddy li a[href*='{$bp_loggedin_user_domain}notifications']::before { content: '\\f3e2'; }
.nav-buddy li a[href*='{$bp_loggedin_user_domain}profile']::before       { content: '\\f3a0'; }
.nav-buddy li a[href*='{$bp_loggedin_user_domain}settings']::before      { content: '\\f13e'; }
.nav-buddy li a[href*='logout']::before                                  { content: '\\f254'; }

/* COLOR SCHEME */
.comments-area article .comment-author.vcard::before,
body .rtm-media-single-comments,
body .mfp-content .rtm-single-meta,
#buddypress .activity-list .activity-avatar a:before,
#buddypress #whats-new-form #whats-new-avatar:before,
body,
.wrapper {
	background-color: {$colors['background_color']};
}

.toolbar-scrollup .layout,
.toolbar-scrollup .wp-admin,
.toolbar-scrollup .scrollup,
.monarch_menu .nav-primary .current-menu-item > a::before {
	border-color: {$colors['background_color']};
}

#share,
.activation .wrapper,
.bbpress #wp-link-backdrop,
.bbpress #wp-link-wrap,
.cover .item-list-tabs ul li ul.flexMenu-popup li,
.error404 .page-header .page-title > span,
.error404 .wrapper,
.header-panel .hp-footer,
.header-panel,
.modal-backdrop.in,
.modal-theme .modal-content,
.modal-theme .modal-dialog,
.modal-search .search-form123,
.post-format-bg,
.post-wrap .hentry .post-thumbnail,
.post-wrap .post-front-block .post-header,
.registration .wrapper,
.toolbar-scrollup .item-wrap,
.user-panel,
.body-bg::before,
.wrapper::after,
.widget.widget_bp_core_friends_widget .item-list li:hover .item,
.widget.widget_bp_core_members_widget .item-list li:hover .item,
.widget.widget_bp_groups_widget .item-list li:hover .item,
body #drupalchat .chatboxcontent,
body #drupalchat .chatboxinput,
body #drupalchat .subpanel .chat_options,
body #drupalchat .subpanel .drupalchat_search .drupalchat_searchinput,
body #drupalchat .subpanel .drupalchat_search,
body #drupalchat .subpanel li,
body #drupalchat ul li a.active,
body .mfp-bg,
body .rtmedia-popup,
body.buddypress div#buddydrive-main ul#buddydrive-browser li.buddydrive-item .buddydrive-share-dialog,
.body-bg {
	background-color: {$colors['panels_background_color']};
}

.widget-panel.widget.widget_comments ul li a .post::after,
.post-wrap .post-format-bg {
	border-bottom-color: {$colors['panels_background_color']};
}

@media only screen and (max-width: 1530px) {
	.cover .item-list-tabs {
		background: {$colors['panels_background_color']};
	}
	.cover .item-list-tabs ul li ul.flexMenu-popup {
		background: {$colors['panels_background_color']};
	}
}

body .rtmedia-popup,
.bbpress #wp-link-wrap,
.modal-theme .modal-dialog {
	-webkit-box-shadow: 1px 1px rgba(255, 255, 255, 0.1), 2px 2px {$colors['light_panels_bg_color']}, 3px 3px rgba(255, 255, 255, 0.1), 4px 4px {$colors['light_panels_bg_color']};
	box-shadow: 1px 1px rgba(255, 255, 255, 0.1), 2px 2px {$colors['light_panels_bg_color']}, 3px 3px rgba(255, 255, 255, 0.1), 4px 4px {$colors['light_panels_bg_color']};
}

.activation #buddypress input[type="submit"],
.header-panel .nav-info > li > a:hover,
.header-panel .nav-social li a:hover,
.monarch_menu .nav-primary .sub-menu a:focus,
.monarch_menu .nav-primary .sub-menu a:hover,
.monarch_menu .nav-primary > li > a:hover,
.monarch_menu .nav-primary li a:focus .menu-item-description,
.monarch_menu .nav-primary li a:hover .menu-item-description,
.navbar-monarch .navbar-nav > li > a:hover,
.navbar-monarch .navbar-nav > li.dropdown .dropdown-menu a:hover,
.post-wrap .format-quote .post-title blockquote cite a,
.post-wrap .format-quote .post-title blockquote cite a:hover,
.post-wrap .format-quote .post-title blockquote cite,
.relatedposts h3 a:hover,
.widget-panel.widget 
.widget-panel.widget .button,
.widget.widget_comments ul li a:hover .post,
.widget.widget_posts ul li h4 a:hover,
a:focus,
a:hover,
body #TB_closeWindowButton:focus .tb-close-icon,
body #TB_closeWindowButton:hover .tb-close-icon,
body .bpfb_actions_container.bpfb-theme-new .bpfb_toolbarItem,
body .bpfb_actions_container.bpfb-theme-new .bpfb_toolbarItem.bpfb_active,
body .bpfb_actions_container.bpfb-theme-new .bpfb_toolbarItem:active,
body .bpfb_actions_container.bpfb-theme-new .bpfb_toolbarItem:hover,
body .bpfb_actions_container.bpfb-theme-new .bpfb_toolbarItem:visited,
body .plupload_file_action .dashicons,
body .plupload_file_name .dashicons,
body .plupload_file_name .dashicons-yes,
a {
	color: {$colors['main_color']};
}

body .learn-press-message::before,
#bp-uploader-warning::before,
#buddypress #pass-strength-result::before,
#buddypress div.error::before,
#buddypress p.success::before,
#buddypress p.updated::before,
#buddypress p.warning::before,
#message::before,
#sitewide-notice::before,
.activation #buddypress input[type="submit"]:active,
.activation #buddypress input[type="submit"]:focus,
.activation #buddypress input[type="submit"]:hover,
.bbp-forum-content ul.sticky::after,
.bbp-topics ul.sticky::after,
.bbp-topics ul.super-sticky::after,
.bbp-topics-front ul.super-sticky::after,
.cover-image-container #item-buttons .generic-button a:hover,
.icheckbox.checked::before,
.indicator-hint::before,
.iradio.checked::before,
.registration #buddypress input[type="submit"]:active,
.registration #buddypress input[type="submit"]:focus,
.registration #buddypress input[type="submit"]:hover,
.sa-error-container::before,
.user-panel li.current-menu-item a:focus,
.user-panel li.current-menu-item a:hover,
.widget-panel.widget .bp-login-widget-user-logout .logout:active,
.widget-panel.widget .bp-login-widget-user-logout .logout:focus,
.widget-panel.widget .bp-login-widget-user-logout .logout:hover,
.widget-panel.widget .button:active,
.widget-panel.widget .button:focus,
.widget-panel.widget .button:hover,
.woocommerce .woocommerce-error::before,
.woocommerce .woocommerce-info::before,
.woocommerce .woocommerce-message::before,
body #buddypress #rtm-media-options-list .rtm-options .button:focus,
body #buddypress #rtmedia-single-media-container.rtmedia-single-media .rtm-options .button:hover,
body .plupload_file_progress,
body .rtm-media-options .rtmedia-upload-media-link:focus,
body .rtm-media-options .rtmedia-upload-media-link:hover,
body .rtmedia-no-media-found::before,
body .rtmedia-success::before,
body .rtmedia-warning::before,
body .select2-container--default .select2-results__option--highlighted[aria-selected],
body.buddypress .error::before,
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback p.info::before,

#bbpress-forums div.bbp-forum-author .bbp-author-role,
#bbpress-forums div.bbp-reply-author .bbp-author-role,
#bbpress-forums div.bbp-topic-author .bbp-author-role,
#buddypress .item-list-tabs ul li a span,
.cover-image-container,
.site-header h1,
.mini-panel .header-panel button.top,
.post-wrap .post-format,
.post-wrap .sticky-post,
.post-wrap article .post-categories li a,
.relatedposts .relatedpost .post-info .category,
.scrollbar-inner > .scroll-element .scroll-bar,
.tooltip-inner,
.user-panel .buddy-avatar .notifications,
.user-panel li.current-menu-item a,
.widget-area .widget-title span,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
div.bp-avatar-status .bp-bar,
div.bp-cover-image-status .bp-bar {
	background-color: {$colors['main_color']};
}

body .learn-press-message,
#bp-uploader-warning,
#buddypress div.error,
#buddypress p.success,
#buddypress p.updated,
#buddypress p.warning,
#message,
#pass-strength-result,
#sitewide-notice,
.activation .content,
.bbp-forum-content ul.sticky::before,
.bbp-topics ul.sticky::before,
.bbp-topics ul.super-sticky::before,
.bbp-topics-front ul.super-sticky::before,
.bbpress #wp-link .query-notice .query-notice-default,
.bbpress #wp-link .query-notice .query-notice-hint,
.error404 .wrapper,
.indicator-hint,
.post-wrap .post-format::after,
.post-wrap .sticky-post::after,
.registration .content,
.sa-error-container,
.tooltip.bottom .tooltip-arrow,
.tooltip.left .tooltip-arrow,
.tooltip.right .tooltip-arrow,
.tooltip.top .tooltip-arrow,
.widget-area .widget-title,
.woocommerce .woocommerce-error,
.woocommerce .woocommerce-info,
.woocommerce .woocommerce-message,
body .rtmedia-no-media-found,
body .rtmedia-success,
body.buddypress .error,
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback p.info,
body .rtmedia-warning {
	border-color: {$colors['main_color']};
}

.post-wrap .post-format-bg {
	border-top-color: {$colors['main_color']};
}

body .woocommerce div.product form.cart .button,
.activation #buddypress input[type="submit"],
.icheckbox,
.registration #buddypress input[type="submit"],
.widget-panel.widget 
.widget-panel.widget .button,
body .wp-polls .Buttons,
body.buddypress div#buddydrive-main ul#buddydrive-browser.bulk-select li.buddydrive-item.bulk-selected .buddydrive-content,
body.buddypress div#buddydrive-main ul#buddydrive-browser.bulk-select li.buddydrive-item.bulk-selected .buddydrive-icon,
#buddypress input[type=submit],
input[type="submit"] {
	border-color: {$colors['light_main_color']};
}

body #drupalchat .subpanel li:hover,
.icheckbox::before,
.infinite-loader .spinner,
.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content,
.infinite-loader .spinner::before,
.infinite-loader .spinner::after {
	background: {$colors['light_main_color']};
}

@media only screen and (min-width: 1200px) and (max-width: 1360px), (max-width: 768px) {
	.user-panel .nav-buddy li.current-menu-item a,
	.user-panel .nav-buddy li.current-menu-item a::before {
		background-color: {$colors['main_color']};
	}

	.user-panel .nav-buddy li a:focus,
	.user-panel .nav-buddy li a:hover {
		background: {$colors['main_color']};
	}
}

::-webkit-scrollbar-thumb,
::-webkit-scrollbar-thumb:window-inactive,
::selection {
	background-color: {$colors['main_color']};
}

CSS;

if ( $monarch_css_box_corner == true ) {

$css .= <<<CSS

/*
  ========================================
  Box Corner
  ========================================
*/

$classCSScorner {
	position: absolute;
	content: "";
	bottom: -1px;
	right: 0px;
	width: 25px;
	height: 25px;
	z-index: 1;
	background: -webkit-linear-gradient(135deg, {$colors['background_color']} 45%, {$colors['border_color']} 50%, {$colors['border_color']} 56%, {$colors['border_color']} 80%); 
	background: linear-gradient(315deg, {$colors['background_color']} 45%, {$colors['border_color']} 50%, {$colors['border_color']} 56%, {$colors['border_color']} 80%);
}

$classCSScornerShadow {
	position: absolute;
	content: "";
	bottom: 0;
	right: 25px;
	background: -webkit-linear-gradient(135deg, rgba(0, 0, 0, 0.04) 45%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0) 56%, rgba(255, 255, 255, 0) 80%);
	background: linear-gradient(315deg, rgba(0, 0, 0, 0.02) 45%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0) 56%, rgba(255, 255, 255, 0) 80%); width: 20px; height: 20px; z-index: 2;
}

CSS;

}

if ( $monarch_css_box_style == 'monarch' ) {

$css .= <<<CSS

/*
  ========================================
  Box Style Monarch
  ========================================
*/

{$classCSSpanels} {
	border-bottom: 1px solid {$colors['border_color']};
}

CSS;

}

if ( $monarch_css_box_style == 'border' ) {

$css .= <<<CSS

/*
  ========================================
  Box Style Border
  ========================================
*/

{$classCSSpanels} {
	border-bottom: 1px solid rgba(0, 0, 0, 0.07);
}

.woocommerce div.product .woocommerce-tabs ul.tabs li.active,
.timeline,
{$classCSSpanels} {
	border: 1px solid {$colors['border_color']};
}

.timeline {
	border-top: 0;
	border-bottom: 0;
}

.woocommerce div.product .woocommerce-tabs ul.tabs li.active {
	border-bottom: 0;
}

body .rtm-comment-list .rtmedia-comment-content::before,
.comments-area article .comment-content::after {
    z-index: 0;
    content: " ";
    margin-top: -6px;
    position: absolute;
    top: 31px;
    left: -14px;
    border: 6px solid {$colors['border_color']};
    border-top: 6px solid transparent;
    border-bottom: 6px solid transparent;
    border-left: 6px solid transparent;
}

.cover {
	border-width: 0 0 1px 0;
}

{$classCSScorner} {
	right: -1px;
}

{$classCSScornerShadow} {
	right: 24px;
}

CSS;

}

if ( $monarch_css_box_style == 'shadow' ) {

$css .= <<<CSS

/*
  ========================================
  Box Style Shadow
  ========================================
*/

{$classCSSpanels} {
	box-shadow: 0 1px 2px rgba(0,0,0,.08);
}

body .rtm-comment-list .rtmedia-comment-content::before,
.comments-area article .comment-content::after {
    z-index: 0;
    content: " ";
    margin-top: -6px;
    position: absolute;
    top: 31px;
    left: -14px;
    border: 6px solid {$colors['border_color']};
    border-top: 6px solid transparent;
    border-bottom: 6px solid transparent;
    border-left: 6px solid transparent;
}

{$classCSScorner} {
	right: -1px;
	bottom: -2px;
}

{$classCSScornerShadow} {
	right: 24px;
	bottom: -2px;
}

CSS;

}

if ( $monarch_css_select == 'monarch' ) {

$css .= <<<CSS

/*
  ========================================
  Select Monarch
  ========================================
*/

.icheckbox.checked::before,
.iradio.checked::before                                             { opacity: 0.5; }

body .select2-container--default .select2-selection--single .select2-selection__rendered,
body .select2-container--default .select2-selection--multiple .select2-selection__rendered { -webkit-box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.02); box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.02); }

CSS;

}

if ( $monarch_css_select == 'atlass' ) {

$css .= <<<CSS

/*
  ========================================
  Radio Atlass
  ========================================
*/

.iradio                 { width: 14px; height: 14px; }

.iradio                 { background: #fff; box-shadow: 0 0 0 2px #dcdcdc; border: 1px solid #fff; }
.iradio.checked         { box-shadow: none; }

.iradio::before         { background: #fff; border: 3px solid #fff; }
.iradio.checked::before { border: 3px solid #fff; box-shadow: 0 0 0 3px {$colors['main_color']}; }

/*
  ========================================
  Switches Atlass
  ========================================
*/

.icheckbox                 { background: #BDC3C7; box-shadow: none!important; }
.icheckbox.checked         { border: 1px solid #fff; background: #bfa885; }

.icheckbox::before         { background: #fff; opacity: 1; height: 22px; width: 22px; top: 1px; -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4); box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4); }
.icheckbox.checked::before { background: #fff; }

/*
  ========================================
  Select Atlass
  ========================================
*/

select                                                              { border-color: #eee; -webkit-box-shadow: none; box-shadow: none; color: #696969; }
select:focus                                                        { -webkit-box-shadow: none; box-shadow: none; border-color: #eee; color: #777; }

.message-search form,
.widget_display_search form,
#buddypress div.item-list-tabs#subnav ul li.groups-members-search form,
.search-form,
input[type=search]                                                  { -webkit-box-shadow: none; box-shadow: none; }

input,
.form-control                                                       { box-shadow: none; -webkit-box-shadow: none; }

.icheckbox                                                          { border: 1px solid #ddd; -webkit-box-shadow: 0 0 0 3px #eee; box-shadow: 0 0 0 3px #eee; height: 26px; margin-right: 10px; }

#buddypress div.dir-search input[type="submit"]                     { margin-top: 0; }
.iradio                                                             { border-color: #dcdcdc; }
.modal-search .search-form123                                          { border: 5px solid rgba(255, 255, 255, 0.08); border-radius: 10px; }
.generic-button a                                                   { box-shadow: 0 0 0 4px #eee; -webkit-box-shadow: 0 0 0 4px #eee; border: 0; }

body .select2-container--default .select2-selection--single .select2-selection__rendered,
body .select2-container--default .select2-selection--multiple .select2-selection__rendered { -webkit-box-shadow: none; box-shadow: none; }

CSS;

}

if ( $monarch_css_widget_title == 'monarch' ) {

$css .= <<<CSS

/*
  ========================================
  Widgets Monarch
  ========================================
*/

.widget-area .widget-title                                          { border-bottom-width: 2px; border-bottom-style: solid; margin: 0 0 15px 0; }
.widget-area .widget-title span                                     { position: relative; display: inline-block; height: 22px; color: #fff; text-transform: uppercase; font-size: 12px; line-height: 24px; font-weight: normal; padding: 0 23px 0 10px; overflow: hidden; float: left; }
.widget-area .widget-title span::before                             { content: ""; position: absolute; top: -9px; width: 26px; height: 26px; background: #fff; -moz-transform: rotate(60deg); -ms-transform: rotate(60deg); -webkit-transform: rotate(60deg); -o-transform: rotate(60deg); transform: rotate(60deg); right: -17px; }

.post-wrap .post-footer                                             { border-top: 1px dashed #e4e4e4; padding: 25px 0 40px; margin: 0 50px; }
.post-wrap .post-footer ul                                          { list-style-type: none; padding: 0; margin: 0 -15px; }
.post-wrap .post-footer li                                          { display: inline-block; font-size: 12px; color: #b3b3b3; font-weight: bold; letter-spacing: 1px; padding: 0 20px; text-transform: capitalize; position: relative; height: 24px; }
.post-wrap .post-footer li i                                        { margin-right: 5px; font-size: 17px; position: relative; top: 2px; }
.post-wrap .post-footer li a                                        { color: #b3b3b3; }

CSS;

}

if ( $monarch_css_widget_title == 'atlass' ) {

$css .= <<<CSS

/*
  ========================================
  Widgets Atlass
  ========================================
*/

.widget.widget_about .aboutwidget                                   { color: #363A46; }

.widget.widget_calendar td,
.widget.widget_calendar th,
.widget.widget_calendar caption                                     { color: #63666f; }

.widget.widget_comments ul li a .post::before                       { top: -17px; }

.widget-area .widget                                                { padding-top: 60px; }
.widget-area .widget .widget-title                                  { position: absolute; left: 0; right: 0; top: 0; margin: 0; display: block; }
.widget-area .widget .widget-title span                             { color: #fff; display: block; padding: 15px 0 15px 35px; font-size: 12px; }

.widget-panel .widget-title                                         { text-transform: none; font-size: 15px; }

.widget-area .widget.widget_comments ul li a .post                  { border-style: solid; background: #eee; }
.widget-area .widget.widget_comments ul li a .post::after           { border-bottom-color: #eee; }

.post-wrap .post-tags a.post-edit-link,
.post-wrap .post-tags a                                             { color: #555; background: #eee; font-size: 10px; font-weight: bold; }
.widget-area .widget .tagcloud a                                    { font-size: 10px!important; color: #333; border-color: #eee; font-weight: 600; border-style: solid; }
.widget-area .widget .tagcloud a:hover                              { color: #fff; border-color: transparent; background-color: {$colors['main_color']}; }

.post-wrap .post-tags a.post-edit-link:hover,
.post-wrap .post-tags a:hover                                       { text-decoration: underline; }

.widget.widget_bp_core_friends_widget .item-list li:hover .item, 
.widget.widget_bp_core_members_widget .item-list li:hover .item, 
.widget.widget_bp_groups_widget .item-list li:hover .item           { border-width: 3px; }

.widget.widget_bp_core_friends_widget .item-list li .item-title a, 
.widget.widget_bp_core_members_widget .item-list li .item-title a, 
.widget.widget_bp_groups_widget .item-list li .item-title a         { color: #D9DDE7; }

/* BORDER */
.woocommerce .widget_shopping_cart .total,
.woocommerce.widget_shopping_cart .total,
.woocommerce .widget-panel.widget_shopping_cart .total,
.widget-panel.woocommerce.widget_shopping_cart .total,
.widget-panel.widget.widget_display_stats dd,
.widget-panel.widget.widget_display_stats dt,
.widget-panel.widget.widget_nav_menu ul ul,
.widget-panel.widget.widget_product_categories ul ul,
.widget-panel.widget.widget_categories ul ul,
.widget-panel.widget.widget_comments ul li,
.widget-panel.widget.widget_rss ul li,
.widget-panel.widget.widget_display_topics ul li,
.widget-panel.widget.widget_display_views ul li,
.widget-panel.widget.widget_display_forums ul li,
.widget-panel.widget.widget_display_replies ul li,
.widget-panel.widget.widget_nav_menu ul li,
.widget-panel.widget.widget_archive ul li,
.widget-panel.widget.widget_recent_comments ul li,
.widget-panel.widget.widget_categories ul li,
.widget-panel.widget.widget_product_categories ul li,
.widget-panel.widget.widget_pages ul li,
.widget-panel.widget.widget_recent_entries ul li,
.widget-panel.widget.widget_meta ul li,
.widget-panel.widget.widget_posts ul li,
.widget-panel.widget .tagcloud a,
.widget-panel.widget.widget_comments ul li a .post                  { border-style: solid; }

.widget-panel select                                                { font-weight: normal; color: #999; }
.widget-panel select:focus                                          { color: rgba(255, 255, 255, 0.7); border-color: rgba(255, 255, 255, 0.08); }

.woocommerce .widget_shopping_cart .total,
.woocommerce.widget_shopping_cart .total                            { border-width: 1px 0 0 0; }

/*
  ========================================
  Social Articles
  ========================================
*/

body .article-container .excerpt                                    { font-size: 13px; line-height: 2; }

body .article-container .article-footer a:hover,
body .article-container .article-footer a:focus,
body .article-container .article-footer a                           { color: #fff; }
body .article-container .article-footer                             { color: #fff; font-size: 12px; padding: 5px 25px; border: 0; margin: 0; background-color: {$colors['main_color']}; }

body .article-container .article-image::before                      { content: ""; position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: rgba(0, 0, 0, 0.4); z-index: 2; }

/*
  ========================================
  Post
  ========================================
*/

.post-wrap .post-content                                            { line-height: 2.1 }
.post-wrap .post-date                                               { line-height: 1.5; padding: 10px 0; font-weight: 600; font-size: 12px; }
.post-wrap .post-footer                                             { background-color: {$colors['main_color']}; }
.post-wrap .post-footer li                                          { -webkit-transition: all .15s ease-in-out; -moz-transition: all .15s ease-in-out; -o-transition: all .15s ease-in-out; transition: all .15s ease-in-out; }

.post-wrap .post-footer li a i::before,
.post-wrap .post-footer li a::before                                { font-size: 15px; }
.post-wrap .post-footer                                             { padding: 7px 45px 7px 40px; margin: 10px 0 0 0!important; }
.post-wrap .post-footer ul                                          { list-style-type: none; padding: 0; margin: 0; }
.post-wrap .post-footer li                                          { display: inline-block; font-size: 12px; font-weight: bold; color: #b3b3b3; padding: 0 10px; position: relative; line-height: 2.4; }
.post-wrap .post-footer li:hover                                    { background: rgba(0, 0, 0, 0.2) }
.post-wrap .post-footer li i                                        { margin-right: 5px; font-size: 17px; position: relative; top: 2px; }
.post-wrap .post-footer li a                                        { color: #fff; }
.post-wrap .post-footer li a:focus,
.post-wrap .post-footer li a:hover                                  { text-decoration: none }
.post-wrap .post-footer li.viewcount                                { color: #fff; }

.post-wrap .hentry .post-thumbnail                                  { overflow: hidden; }

/*
  ========================================
  Header Panel
  ========================================
*/

.cover .item-list-tabs ul li a,
.user-menu-create li a,
.nav-buddy li a                                                     { color: rgba(255,255,255,0.75); text-transform: none; font-size: 11px; letter-spacing: 0; }
.user-panel li.current-menu-item a                                  { color: #fff; }

.header-panel .copyright,
.header-panel .nav-info li a                                        { color: rgba(255, 255, 255, 0.4); }
.header-panel .widget.widget_posts .image a::after                  { top: 0; left: 0; right: 0; bottom: 0; }

.toolbar-scrollup .item-wrap                                        { border: 0; width: 26px; height: 26px; margin-left: 3px; margin-right: 3px; }
.toolbar-scrollup .item                                             { background: rgba(255,255,255,0.1); height: 100%; padding: 5px 0; font-size: 16px; line-height: 1; }
.toolbar-scrollup .item:focus,
.toolbar-scrollup .item:hover                                       { opacity: 1; background-color: {$colors['main_color']}; }

CSS;

}

if ( $monarch_css_box_border_radius == 'atlass' ) {

$css .= <<<CSS

/*
  ========================================
  Box Border Radius
  ========================================
*/

.modal-theme .search-form input,
#add_payment_method #payment,
#bbpress-forums #bbp-search-form,
#bbpress-forums div.bbp-the-content-wrapper,
#bbpress-forums div.hentry,
#bbpress-forums div.wp-editor-container,
#bbpress-forums fieldset.bbp-form,
#bbpress-forums li.bbp-body ul.forum,
#bbpress-forums li.bbp-body ul.topic,
#bbpress-forums li.bbp-footer,
#bbpress-forums li.bbp-header,
#bp-uploader-warning,
#buddypress #friend-list li img.avatar,
#buddypress #friend-list li,
#buddypress #item-body div.profile,
#buddypress #member-list li img.avatar,
#buddypress #member-list li,
#buddypress #message-thread .message-box,
#buddypress #message-thread img.avatar,
#buddypress .activity-comments .acomment-content,
#buddypress .activity-comments .acomment-meta img.avatar,
#buddypress .activity-list .activity-inner,
#buddypress .item-list-tabs ul li > span,
#buddypress .item-list-tabs ul li a span,
#buddypress .item-list-tabs ul li a,
#buddypress .item-list-tabs,
#buddypress .masonry li .item-avatar .avatar,
#buddypress .masonry li .item-avatar a,
#buddypress .masonry li .item-avatar,
#buddypress .masonry li .padding,
#buddypress .notifications,
#buddypress .standard-form,
#buddypress div.dir-search,
#buddypress div.error,
#buddypress div.item-list-tabs#subnav ul li.groups-members-search input[type="submit"],
#buddypress p.success,
#buddypress p.updated,
#buddypress p.warning,
body .learn-press-message,
#message,
#pass-strength-result,
#sitewide-notice,
.author-info,
.bbp-logged-in .user-submit::after,
.bbp-logged-in img.avatar,
.comments-area .no-comments,
.comments-area .pingback .comment-body,
.comments-area article .comment-content img,
.comments-area article .comment-content,
.cover .item-list-tabs ul li a,
.cover,
.cover-image-container #item-buttons .generic-button a,
.cover-image-container #item-header-avatar a,
.error404 .search-form,
.form-control,
.indicator-hint,
#buddypress .message-search input[type="submit"],
.messages-options-nav,
.notifications-options-nav,
.page-header .page-header-content,
.page-numbers,
.pagination-links a,
.pagination-links span,
.panel,
.post-navigation .nav-links .nav-next,
.post-navigation .nav-links .nav-next::before,
.post-navigation .nav-links .nav-previous,
.post-navigation .nav-links .nav-previous::before,
.post-navigation .nav-links a,
.post-wrap .hentry,
.post-wrap .post-date,
.post-wrap .post-footer li,
.post-wrap .post-front-block .post-header,
.post-wrap .post-front-block .post-thumbnail,
.post-wrap .post-tags a,
.post-wrap article .post-categories li a,
.post-wrap article .post-content .nav-links a,
.post-wrap article .post-content img,
.profile .wp-editor-container,
.relatedposts .image a::after, 
.relatedposts .relatedpost .image img,
.relatedposts .relatedpost,
.sa-error-container,
.search-form input[type="submit"],
.timeline-badge,
.toolbar-scrollup .item,
.toolbar-scrollup .item-wrap,
.tooltip-inner,
.widget .tagcloud a,
.widget-area .widget,
.widget-area .widget.widget_comments ul li a .post,
.widget-panel form[role="search"],
.widget.buddypress .bp-login-widget-user-avatar a::after,
.widget.buddypress .bp-login-widget-user-avatar img.avatar,
.widget.widget_about .aboutwidget .image::after,
.widget.widget_about .aboutwidget img,
.widget.widget_archive .count, 
.widget.widget_banner img,
.widget.widget_bp_core_friends_widget .item-list .item-avatar img.avatar,
.widget.widget_bp_core_friends_widget .item-list li .item-avatar a::after,
.widget.widget_bp_core_friends_widget .item-options a, 
.widget.widget_bp_core_members_widget .item-list .item-avatar img.avatar,
.widget.widget_bp_core_members_widget .item-list li .item-avatar a::after, 
.widget.widget_bp_core_members_widget .item-options a,
.widget.widget_bp_core_recently_active_widget .item-avatar a::after, 
.widget.widget_bp_core_recently_active_widget .item-avatar img.avatar,
.widget.widget_bp_core_whos_online_widget .item-avatar a::after, 
.widget.widget_bp_core_whos_online_widget .item-avatar img.avatar,
.widget.widget_bp_groups_widget .item-list img.avatar,
.widget.widget_bp_groups_widget .item-list li .item-avatar a::after,
.widget.widget_bp_groups_widget .item-options a, 
.widget.widget_categories .count,
.widget.widget_comments ul li a .image::after, 
.widget.widget_dribbble ul li a::after, 
.widget.widget_flickr ul li a::after, 
.widget.widget_posts .image a img,
.widget.widget_posts .image a::after, 
.widget.widget_product_categories .count, 
.widget_display_search input[type="submit"],
.woocommerce .term-description,
.woocommerce .track_order,
.woocommerce .woocommerce-billing-fields,
.woocommerce .woocommerce-error,
.woocommerce .woocommerce-info, 
.woocommerce .woocommerce-message, 
.woocommerce .woocommerce-shipping-fields,
.woocommerce div.product .woocommerce-tabs .panel,
.woocommerce table.shop_table,
.woocommerce ul.products li.product,
.woocommerce-checkout #payment,
.woocommerce-page ul.products li.product,
.woocommerce-product-search input[type="submit"],
.wp-editor-wrap,
body #buddypress #item-body .rtmedia-container ul.rtmedia-list li .rtmedia-list-item-a,
body #buddypress #item-body .rtmedia-container ul.rtmedia-list.rtmedia-album-list li a,
body #item-body .rtmedia-container .rtm-lightbox-container .rtm-single-media,
body .article-container .article-image,
body .article-container .article-image::after,
body .article-container .article-image::before,
body .article-content,
body .buttons-container,
body .post-navigation .nav-next a::before,
body .post-navigation .nav-previous a::before,
body .rtm-comment-list .rtmedia-comment-content,
body .rtm-comment-list .rtmedia-comment-user-pic a,
body .rtm-pagination .rtm-paginate a,
body .rtm-pagination .rtm-paginate a.rtmedia-page-link,
body .rtm-pagination .rtm-paginate span,
body .rtmedia-container #rtmedia-uploader-form .rtm-plupload-list li,
body .rtmedia-container #rtmedia_uploader_filelist li,
body .rtmedia-container .rtm-comment-list.rtm-comment-list li.rtmedia-no-comments,
body .rtmedia-container .rtm-media-gallery-uploader,
body .rtmedia-container .rtm-tabs-content .content,
body .rtmedia-container .rtmedia-like-info,
body .rtmedia-container .rtmedia-media-description,
body .rtmedia-container ul.rtmedia-list li.rtmedia-list-item div.rtmedia-item-thumbnail img,
body .rtmedia-no-media-found,
body .rtmedia-success, body .rtmedia-warning, 
body .sa-upload-image-container,
body .select2-container--default .select2-selection--multiple .select2-selection__rendered,
body .select2-container--default .select2-selection--single .select2-selection__rendered,
body .userprofile.rtm-user-avatar::after,
body.buddypress .error, 
body.buddypress div#buddydrive-main #buddydrive-actions-form,
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback p.info, 
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback,
body.buddypress div#buddydrive-main #buddydrive-uploader,
body.buddypress div#buddydrive-main .buddydrive-toolbar,
body.buddypress div#buddydrive-main article#buddydrive-item-content,
body.buddypress div#buddydrive-main header#buddydrive-item-header,
body.buddypress div#buddydrive-main ul#buddydrive-browser li .buddydrive-content,
body.buddypress div#buddydrive-main ul#buddydrive-browser li .buddydrive-icon,
div.bbp-breadcrumb,
div.bbp-template-notice.info,
div.bbp-topic-tags,
input,
select,
textarea,
textarea.form-control,
.widget                                                             { border-radius: 4px; }

#bbpress-forums .bbp-topic-title p.bbp-topic-meta .bbp-author-avatar::after,
#bbpress-forums div.bbp-forum-author img.avatar,
#bbpress-forums div.bbp-reply-author .bbp-author-avatar::after,
#bbpress-forums div.bbp-reply-author img.avatar,
#bbpress-forums div.bbp-topic-author img.avatar,
#bbpress-forums p.bbp-topic-meta img.avatar,
#bbpress-forums p.bbp-topic-meta span.bbp-topic-freshness-author .bbp-author-avatar::after,
#buddypress #whats-new-form #whats-new-avatar a,
#buddypress #whats-new-form #whats-new-avatar img,
#buddypress .activity-comments .acomment-avatar a,
#buddypress .activity-list .activity-avatar a,
#buddypress .activity-list .activity-header img.avatar,
#buddypress .activity-list li img.avatar,
.author-info .author-avatar img.avatar,
.author-info .author-avatar::after,
.avatar-link img,
.avatar-link,
.comments-area article .avatar,
.comments-area article .comment-author.vcard::after,
.post-wrap.with-avatar .without-post-thumbnail .avatar-link::after,
.post-wrap.with-avatar .without-post-thumbnail .avatar-link::after,
.post-wrap.with-avatar .without-post-thumbnail .avatar-link::after,
.rtmedia-comment-user-pic img,
.userprofile img,
.widget-area .widget.widget_comments ul li a .image img,
.widget-area .widget.widget_comments ul li a .image,
.widget-area .widget.widget_comments ul li a .image::after,
.widget-panel.widget.widget_comments ul li a .image img,
.widget-panel.widget.widget_comments ul li a .image,
body .rtm-comment-list .rtmedia-comment-user-pic a,
body .rtmedia-container .userprofile.rtm-user-avatar::after        { border-radius: 50%; }

body.buddypress div#buddydrive-main ul#buddydrive-browser.bulk-select li.buddydrive-item.bulk-selected .buddydrive-content,
.search-form input[type="submit"]                                  { border-radius: 0 4px 4px 0; }

body.buddypress div#buddydrive-main ul#buddydrive-browser.bulk-select li.buddydrive-item.bulk-selected .buddydrive-icon,
.search-form input                                                 { border-radius: 4px 0 0 4px; }

#bbpress-forums div.bbp-the-content-wrapper textarea.bbp-the-content,
body .article-container .article-footer,
.post-wrap .post-footer                                            { border-radius: 0 0 4px 4px; }

.elem .cover-loop,
.elem .cover-loop a::after,
body .article-container .article-metadata,
#buddypress .masonry li .item-avatar a::after, 
.mce-container *,
.mce-container,
.mce-widget *,
.mce-widget,
.quicktags-toolbar,
.widget-area .widget .widget-title span,
.post-wrap .hentry .post-thumbnail,
.post-wrap .post-front-block .post-header::after,
.post-wrap .post-front-block .post-thumbnail,
.post-wrap article.has-post-thumbnail.format-gallery .post-thumbnail::after, 
.post-wrap article.has-post-thumbnail.format-standard .post-thumbnail::after, 
.post-wrap article.has-post-thumbnail.type-page .post-thumbnail::after, 
.post-wrap.with-avatar .avatar-link::after, 
.post-wrap .hentry .post-thumbnail img                              { border-radius: 4px 4px 0 0; }

#share span a,
body #rtmedia_create_new_album,
body input.rtmedia-merge-selected,
body.bbpress #wp-link-cancel .button,
body.bbpress #wp-link-submit,
.modal-theme input[type="submit"]                                   { border-radius: 5px; }

.widget.widget_bp_core_friends_widget .item-list li:hover .item, 
.widget.widget_bp_core_members_widget .item-list li:hover .item, 
.widget.widget_bp_groups_widget .item-list li:hover .item           { border-radius: 6px; }

body #buddypress .rtmedia-container #rtm-media-options-list .rtm-options .button,
body #buddypress #rtm-media-options-list .rtm-options.rtm-options .rtmedia-delete-album,
body #buddypress .rtmedia-container #rtmedia-single-media-container.rtmedia-single-media .rtm-options .button,
body .rtmedia-container .rtm-options.rtm-options a                  { border-radius: 8px; }

#rtm-modal-container,
.modal-theme .modal-content                                         { border-radius: 10px; }

body .rtmedia-popup,
.bbpress #wp-link-wrap,
.modal-theme .modal-dialog                                          { border-radius: 15px; }

CSS;

}

if ( $monarch_font_medium == true ) {

$css .= <<<CSS

/*
  ========================================
  Font Weight 500
  ========================================
*/

body                                                                { font-size: 12px; font-weight: 500; -webkit-font-smoothing: antialiased; }

CSS;

}

if ( $monarch_font_semi_bold == true ) {

$css .= <<<CSS

/*
  ========================================
  Font Weight 600
  ========================================
*/

b, strong,

/* BuddyPress */
#buddypress #friend-list li .action,
#buddypress #friend-list li .item-title a,
#buddypress #friend-list li h4 a,
#buddypress #member-list li .action,
#buddypress #member-list li h5 a,
#buddypress #whats-new-form p.activity-greeting,
#buddypress .activity-comments .ac-reply-content a,
#buddypress .activity-comments .ac-reply-content a,
#buddypress .activity-comments .acomment-meta a,
#buddypress .activity-list .activity-header p a,
#buddypress .item-list-tabs ul li > span,
#buddypress .item-list-tabs ul li a,
#buddypress .masonry li .item-title a,
#buddypress .masonry li .item-title a,
#buddypress .standard-form h5 > a,
#buddypress .standard-form label,
#buddypress .standard-form span.label,
#buddypress .standard-form#settings-form,
#buddypress li span.unread-count,
#buddypress span.user-nicename,
#buddypress table#message-threads tr.unread td,
#buddypress table.forum tr td.label,
#buddypress table.messages-notices tr td.label,
#buddypress table.notifications tr td.label,
#buddypress table.notifications-settings tr td.label,
#buddypress table.profile-fields tr td.label,
#buddypress table.wp-profile-fields tr td.label,
#buddypress tr.unread span.unread-count,
#buddypress ul.button-nav li.current a,
.bp-avatar-nav,
.cover-image-container #item-header-content #latest-update a,
.current-visibility-level,
.drag-drop .drag-drop-inside p.drag-drop-info,
.widget.buddypress .bp-login-widget-user-link a,
.widget.widget_bp_core_friends_widget .item-list li .item-title a,
.widget.widget_bp_core_friends_widget .item-options a,
.widget.widget_bp_core_members_widget .item-list li .item-title a,
.widget.widget_bp_core_members_widget .item-options a,
.widget.widget_bp_groups_widget .item-list li .item-title a,
.widget.widget_bp_groups_widget .item-options a,
body .rtmedia-container .drag-drop-inside p.drag-drop-info,
body .rtmedia-container .rtm-album-privacy label,
body .rtmedia-container .rtm-album-privacy label,
body .rtmedia-container .rtm-user-meta-details .username,
body .rtmedia-container .rtmedia-uploader .drag-drop .drag-drop-info,
body.buddypress div#buddydrive-main #buddydrive-uploader .drag-drop-inside p.drag-drop-info,
body.buddypress div#buddydrive-main ul.subsubsub li a,
#bbpress-forums ul li.bbp-forum-info .bbp-forums-list li a,
body .article-container .article-footer,
body .article-container .article-data h3 a,

/* bbPress */
#bbpress-forums div.bbp-forum-header,
#bbpress-forums div.bbp-reply-author a.bbp-author-name,
#bbpress-forums div.bbp-reply-content #subscription-toggle a,
#bbpress-forums div.bbp-reply-header,
#bbpress-forums div.bbp-the-content-wrapper input[type="button"],
#bbpress-forums div.bbp-topic-author a.bbp-author-name,
#bbpress-forums div.bbp-topic-content #subscription-toggle a,
#bbpress-forums div.bbp-topic-header,
#bbpress-forums fieldset.bbp-form legend,
#bbpress-forums li.bbp-forum-freshness .bbp-author-name,
#bbpress-forums li.bbp-topic-freshness .bbp-author-name,
#bbpress-forums ul li.bbp-forum-info a,
#bbpress-forums ul li.bbp-topic-title > a,
.bbp-row-actions #subscription-toggle a,
div.bbp-breadcrumb,
div.bbp-topic-tags,
span.bbp-author-ip,

/* Main */
#bbpress-forums > h3,
#bp-uploader-warning,
#buddypress div.error,
#buddypress p.success,
#buddypress p.updated,
#buddypress p.warning,
#message,
#pass-strength-result,
#sitewide-notice,
.bbpress #wp-link-close,
.comments-area .comment-respond .comment-reply-title small a,
.comments-area .comment-respond .comment-reply-title,
.comments-area article .comment-content th,
.comments-area article .comment-metadata a time,
.comments-area article .edit-link a,
.comments-area article .reply a,
.comments-title,
.imgedit-group-top h2,
.indicator-hint,
.modal-share .modal-title,
.modal-theme .close,
.monarch_menu .nav-primary li a,
.nav-buddy li a,
.page-header .page-title,
.page-numbers,
.pagination-links a,
.pagination-links span,
.post-navigation .nav-links .nav-next,
.post-navigation .nav-links .nav-previous,
.post-wrap .post-date,
.post-wrap .post-format,
.post-wrap .post-tags a,
.post-wrap article .post-content .image-navigation .nav-links a,
.post-wrap article .post-content .nav-links .numbers,
.post-wrap article .post-content .wp-caption-text,
.post-wrap article .post-content th,
.registration legend,
.rtm-gallery-title,
.rtmedia-media-edit > h2,
.rtmedia-title,
.sa-error-container,
.sa-error-container,
.timeline-badge,
.user-menu-create li a,
.users-who-like,
.widget-panel.widget.widget_comments ul li a .commauth,
.widget.widget_calendar tbody a,
.widget.widget_comments ul li a .commauth,
.widget.widget_posts ul li h4,
.widget_polls-widget a,
.woocommerce .woocommerce-error,
.woocommerce .woocommerce-info,
.woocommerce .woocommerce-message,
.wp-polls,
.wpcf7-form p,
body .learn-press-message,
body .rtm-comment-list .rtm-comment-wrap .rtmedia-comment-date,
body .rtm-pagination .rtm-paginate a,
body .rtm-pagination .rtm-paginate a.rtmedia-page-link,
body .rtm-pagination .rtm-paginate span,
body .rtmedia-no-media-found,
body .rtmedia-success,
body .rtmedia-warning,
body button.mfp-arrow,
body button.mfp-close,
body.buddypress .error,
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback p.info,
body.buddypress div#buddydrive-main ul.subsubsub li.current,
body.woocommerce .page-title,
h1, h2, h3, h4, h5, h6,

.widget.widget_display_topics ul li a,
.widget.widget_display_views ul li a,
.widget.widget_display_forums ul li a,
.widget.widget_display_replies ul li a,
.widget.widget_archive ul li a,
.widget.widget_nav_menu ul li a,
.widget.widget_pages ul li a,
.widget.widget_categories ul li a,
.widget.widget_product_categories ul li a,
.widget.widget_rss ul li a,
.widget.widget_recent_entries ul li a,
.widget.widget_recent_comments ul li a,
.widget.widget_meta ul li a,

.widget.widget_display_stats dt                                      { font-weight: bold; font-weight: 600; }

CSS;

}

if ( $monarch_css_letter_spacing == true ) {

$css .= <<<CSS

/*
  ========================================
  Letter Spacing
  ========================================
*/

/* Page Header */
body.woocommerce .page-title,
body.buddypress div#buddydrive-main ul.subsubsub li.current,
.imgedit-group-top h2,
.rtmedia-title,
#bbpress-forums > h3,
.rtm-gallery-title,
.rtmedia-media-edit > h2,
.page-header .page-title,

/* Comments */
body .rtm-comment-list .rtm-comment-wrap .rtmedia-comment-author a,
.comments-area article .comment-author a,

.comments-area .comment-respond .comment-reply-title small a,

/* Widgets */
.widget .tagcloud a,
.comments-area article .reply a,
.widget.widget_rss .rss-date,
.widget.widget_recent_entries .post-date,

.nav-buddy li a,
.user-menu-create li a,
.post-wrap .post-format,
.header-panel .hp-footer                                             { letter-spacing: 1px; }

.bbpress #link-modal-title,
.rtmedia-popup .rtm-modal-title,
.modal-theme .modal-header,

.monarch_menu .nav-primary .sub-menu a,
.monarch_menu .nav-primary li a                                      { letter-spacing: 2px; }

.widget.widget_posts ul li h4                                        { letter-spacing: 0.5px; }
.comments-title                                                      { letter-spacing: 3px; }

.post-wrap .post-tags a,
.comments-area .comment-respond .comment-reply-title                 { letter-spacing: 1.5px; }

.post-navigation .nav-links .meta-nav                                { letter-spacing: 0.04em; }

/* bbPress */
#bbpress-forums li.bbp-header,
#bbpress-forums li.bbp-footer,
#bbpress-forums fieldset.bbp-form legend                             { letter-spacing: 0.5px; }

#bbpress-forums div.bbp-reply-author a.bbp-author-name,
#bbpress-forums div.bbp-topic-author a.bbp-author-name,
#bbpress-forums h2.entry-title,
#bbpress-forums ul li.bbp-forum-info a,
div.bbp-breadcrumb,
div.bbp-topic-tags                                                   { letter-spacing: 1px; }

/* BuddyPress */
#buddypress #friend-list li .item-title a,
#buddypress #friend-list li h4 a,
#buddypress #member-list li h5 a,
#buddypress #whats-new-form p.activity-greeting,
#buddypress .activity-comments .ac-reply-content a,
#buddypress .masonry li .item-title a,
#buddypress .standard-form h5 > a,
.bp-avatar-nav ul,
.widget.widget_bp_core_friends_widget .item-list li .item-title a,
.widget.widget_bp_core_members_widget .item-list li .item-title a,
.widget.widget_bp_groups_widget .item-list li .item-title a          { letter-spacing: 1px; }

#buddypress .activity-comments .acomment-meta a,
#buddypress .activity-list .activity-header p a,
#buddypress .activity-list .bbpress .activity-header p .time-since,
#buddypress .standard-form label,
#buddypress .standard-form span.label,
#buddypress .standard-form#settings-form,
.drag-drop .drag-drop-inside p.drag-drop-info                        { letter-spacing: 0.5px; }

/* rtMedia */
body.buddypress div#buddydrive-main #buddydrive-uploader .drag-drop-inside p.drag-drop-info,
body .rtmedia-container .rtmedia-uploader .drag-drop .drag-drop-info,
body .rtmedia-container .rtm-user-meta-details .username a,
body .rtmedia-container .drag-drop-inside p.drag-drop-info,
body .rtmedia-container .rtm-album-privacy label                     { letter-spacing: 0.5px; }

CSS;

}

if ( $monarch_css_buttons == 'monarch' ) {

$css .= <<<CSS

/*
  ========================================
  Buttons Monarch
  ========================================
*/

/* Button Default */
body .woocommerce div.product form.cart .button,
.page .post-edit-link,
.page #vc_load-inline-editor,
body .delete-logo,
body input.bpfb_primary_button,
body .bpfb_controls_container .qq-upload-button,
#rtmedia_create_new_album,
#subscription-toggle a,
.activity-comments li.show-all a,
.activity-read-more a,
.bbpress #wp-link-cancel .button,
.bbpress #wp-link-submit,
.bp-title-button,
.btn,
.btn.btn-default,
.btn.btn-primary,
.button-nav li a,
.wp-social-login-widget a,
body #whats-new-submit #aw-whats-new-submit,
body.buddypress div#buddydrive-main .button-primary,
body.buddypress div#buddydrive-main #buddydrive-uploader #bp-upload-ui:not(.drag-drop) #bp-browse-button,
body .rtmedia-add-media-button,
body button.rtmedia-comment-media-upload,
body.buddypress div#buddydrive-main .buddydrive-actions a,
body.buddypress div#buddydrive-main #buddydrive-load-more,
body .rtmedia-container .rtm-load-more .rtmedia-page-link.button,
body .rtmedia-gallery-item-actions a,
body .rtmedia-single-container button.rtmedia-like,
#buddypress input[type=submit],
button[type="submit"],
input[type="button"],
input[type="submit"]                                               { font-size: 11px; color: #505050; padding: 12px 15px; line-height: 1; position: relative; height: 40px; letter-spacing: 1px; font-weight: bold; border: 3px double; -webkit-box-shadow: none; box-shadow: none; text-transform: uppercase; border-radius: 0; -webkit-transition: all 0.3s; -moz-transition: all 0.3s; transition: all 0.3s; display: inline-block; z-index: 1; background: #fff; text-shadow: none; border-color: {$colors['light_main_color']}; font-family: '{$monarch_font_secondary}', sans-serif; }

/* Button Default Focus & Hover & Active */
.page .post-edit-link:active,
.page #vc_load-inline-editor:active,
body .delete-logo:active,
body input.bpfb_primary_button:active,
body .bpfb_controls_container .qq-upload-button:active,
#rtmedia_create_new_album:active,
#subscription-toggle a:active,
.activity-comments li.show-all a:active,
.activity-read-more a:active,
.bbpress #wp-link-cancel .button:active,
.bbpress #wp-link-submit:active,
.bp-title-button:active,
.btn.btn-default:active,
.btn.btn-primary:active,
.btn:active,
.button-nav li a:active,
.wp-social-login-widget a:active,
body #whats-new-submit #aw-whats-new-submit:active,
body.buddypress div#buddydrive-main .button-primary:active,
body.buddypress div#buddydrive-main #buddydrive-load-more:active,
body.buddypress div#buddydrive-main #buddydrive-uploader #bp-upload-ui:not(.drag-drop) #bp-browse-button:active,
body .rtmedia-add-media-button:active,
body button.rtmedia-comment-media-upload:active,
body .rtmedia-container .rtm-load-more .rtmedia-page-link.button:active,
body .rtmedia-gallery-item-actions a:active,
body.buddypress div#buddydrive-main .buddydrive-actions a:active,
body .rtmedia-single-container button.rtmedia-like:active,
body .wp-polls .Buttons:active, 
#buddypress input[type=submit]:active,
button[type="submit"]:active,
input[type="button"]:active,
input[type="submit"]:active,
body .woocommerce div.product form.cart .button:active,

body .woocommerce div.product form.cart .button:focus,
.page .post-edit-link:focus,
.page #vc_load-inline-editor:focus,
body .delete-logo:focus,
body input.bpfb_primary_button:focus,
body .bpfb_controls_container .qq-upload-button:focus,
#rtmedia_create_new_album:focus,
#subscription-toggle a:focus,
.activity-comments li.show-all a:focus,
.activity-read-more a:focus,
.bbpress #wp-link-cancel .button:focus,
.bbpress #wp-link-submit:focus,
.bp-title-button:focus,
.btn.btn-default:focus,
.btn.btn-primary:focus,
.btn:focus,
.button-nav li a:focus,
.wp-social-login-widget a:focus,
body #whats-new-submit #aw-whats-new-submit:focus,
body.buddypress div#buddydrive-main .button-primary:focus,
body.buddypress div#buddydrive-main #buddydrive-load-more:focus,
body.buddypress div#buddydrive-main #buddydrive-uploader #bp-upload-ui:not(.drag-drop) #bp-browse-button:focus,
body .rtmedia-add-media-button:focus,
body button.rtmedia-comment-media-upload:focus,
body .rtmedia-container .rtm-load-more .rtmedia-page-link.button:focus,
body .rtmedia-gallery-item-actions a:focus,
body.buddypress div#buddydrive-main .buddydrive-actions a:focus,
body .rtmedia-single-container button.rtmedia-like:focus,
body .wp-polls .Buttons:focus, 
button[type="submit"]:focus,
input[type="button"]:focus,
#buddypress input[type=submit]:focus,
input[type="submit"]:focus,

body .woocommerce div.product form.cart .button:hover,
.page .post-edit-link:hover,
.page #vc_load-inline-editor:hover,
body .delete-logo:hover,
body input.bpfb_primary_button:hover,
body .bpfb_controls_container .qq-upload-button:hover,
#rtmedia_create_new_album:hover,
#subscription-toggle a:hover,
.activity-comments li.show-all a:hover,
.activity-read-more a:hover,
.bbpress #wp-link-cancel .button:hover,
.bbpress #wp-link-submit:hover,
.bp-title-button:hover,
.btn.btn-default:hover,
.btn.btn-primary:hover,
.btn:hover,
.button-nav li a:hover,
.wp-social-login-widget a:hover,
body #whats-new-submit #aw-whats-new-submit:hover,
body.buddypress div#buddydrive-main .button-primary:hover,
body.buddypress div#buddydrive-main #buddydrive-load-more:hover,
body.buddypress div#buddydrive-main #buddydrive-uploader #bp-upload-ui:not(.drag-drop) #bp-browse-button:hover,
body .rtmedia-add-media-button:hover,
body button.rtmedia-comment-media-upload:hover,
body .rtmedia-container .rtm-load-more .rtmedia-page-link.button:hover,
body .rtmedia-gallery-item-actions a:hover,
body.buddypress div#buddydrive-main .buddydrive-actions a:hover,
body .rtmedia-single-container button.rtmedia-like:hover,
body .wp-polls .Buttons:hover,
button[type="submit"]:hover,
input[type="button"]:hover,
#buddypress input[type=submit]:hover,
input[type="submit"]:hover                                           { color: #fff; border-width: 3px; border-style: double; border-color: #fff; text-decoration: none; background-color: {$colors['main_color']}; }

/* Button Secondary */
.woocommerce #respond input#submit,
.woocommerce a.button,
.woocommerce button.button,
.woocommerce input.button,
.acomment-options a,
.bp-login-widget-user-logout .logout,
.generic-button a,
.load-more a,
.load-newest a,
.notifications a.delete,
.notifications a.mark-read,
.notifications a.mark-unread,
.thread-options a.delete,
.thread-options a.read,
.thread-options a.unread,
body #buddypress #rtm-media-options-list .rtm-options .button,
body #buddypress .rtmedia-actions-before-comments .rtmedia-like,
body.buddypress div#buddydrive-main ul#buddydrive-manage-actions li a,
body #rtm-media-options-list .rtm-options.rtm-options .rtmedia-delete-album,
body .imgedit-group .dashicons.imgedit-help-toggle,
body .rtm-media-options .rtm-media-options-list .rtmedia-action-buttons,
body .rtm-options.rtm-options a,
body .rtm-tabs a,
body .rtmedia-actions-before-comments .rtmedia-comment-link,
body .rtmedia-container .rtm-load-more a#rtMedia-galary-next,
body .rtmedia-edit-media-tabs .rtm-tabs a,
body .rtmedia-upload-media-link,
body a.rtmedia-upload-media-link,
.button                                                             { color: #aaa; vertical-align: middle; position: relative; z-index: 25; display: inline-block; line-height: 1.3; border: 3px double rgba(0, 0, 0, 0.1); background-color: #fff; letter-spacing: 0.5px; -webkit-box-shadow: none; box-shadow: none; padding: 8px 14px; font-size: 11px; font-family: '{$monarch_font_secondary}', sans-serif; height: auto; font-weight: bold; min-height: 36px; }

/* Button Secondary Focus & Hover*/
.woocommerce #respond input#submit:focus,
.woocommerce a.button:focus,
.woocommerce button.button:focus,
.woocommerce input.button:focus,
.acomment-options a:focus,
.bp-login-widget-user-logout .logout:focus,
.button:focus,
.generic-button a:focus,
.load-more a:focus,
.load-newest a:focus,
.notifications a.delete:focus,
.notifications a.mark-read:focus,
.notifications a.mark-unread:focus,
.thread-options a.delete:focus,
.thread-options a.read:focus,
.thread-options a.unread:focus,
body #buddypress #rtm-media-options-list .rtm-options .button:focus,
body #buddypress .rtmedia-actions-before-comments .rtmedia-like:focus,
body.buddypress div#buddydrive-main ul#buddydrive-manage-actions li a:focus,
body #rtm-media-options-list .rtm-options.rtm-options .rtmedia-delete-album:focus,
body .imgedit-group .dashicons.imgedit-help-toggle:focus,
body .rtm-media-options .rtm-media-options-list .rtmedia-action-buttons:focus,
body .rtm-options.rtm-options a:focus,
body .rtm-tabs a:focus,
body .rtmedia-actions-before-comments .rtmedia-comment-link:focus,
body .rtmedia-container .rtm-load-more a#rtMedia-galary-next:focus,
body .rtmedia-edit-media-tabs .rtm-tabs a:focus,
body .rtmedia-upload-media-link:focus,
body a.rtmedia-upload-media-link:focus,

.woocommerce #respond input#submit:hover,
.woocommerce a.button:hover,
.woocommerce button.button:hover,
.woocommerce input.button:hover,
.acomment-options a:hover,
.bp-login-widget-user-logout .logout:hover,
.generic-button a:hover,
.load-more a:hover,
.load-newest a:hover,
.notifications a.delete:hover,
.notifications a.mark-read:hover,
.notifications a.mark-unread:hover,
.thread-options a.delete:hover,
.thread-options a.read:hover,
.thread-options a.unread:hover,
body #buddypress #rtm-media-options-list .rtm-options .button:hover,
body #buddypress .rtmedia-actions-before-comments .rtmedia-like:hover,
body.buddypress div#buddydrive-main ul#buddydrive-manage-actions li a:hover,
body #rtm-media-options-list .rtm-options.rtm-options .rtmedia-delete-album:hover,
body .imgedit-group .dashicons.imgedit-help-toggle:hover,
body .rtm-media-options .rtm-media-options-list .rtmedia-action-buttons:hover,
body .rtm-options.rtm-options a:hover,
body .rtm-tabs a:hover,
body .rtmedia-actions-before-comments .rtmedia-comment-link:hover,
body .rtmedia-container .rtm-load-more a#rtMedia-galary-next:hover,
body .rtmedia-edit-media-tabs .rtm-tabs a:hover,
body .rtmedia-upload-media-link:hover,
body a.rtmedia-upload-media-link:hover,
.button:hover                                                       { text-decoration: none; -webkit-box-shadow: 0 0 0 3px rgba(216, 216, 216, 0); box-shadow: 0 0 0 3px rgba(216, 216, 216, 0); color: #fff; border-color: #fff; background-color: {$colors['main_color']}; }

/* Button Counter */
#buddypress a.bp-primary-action span,
#buddypress #reply-title small a span                               { background: #D8D8D8; padding: 0px 5px 3px; color: #fff; -webkit-transition: all 0.3s; -moz-transition: all 0.3s; transition: all 0.3s; text-align: center; }

/* Button Hover Counter */
#buddypress a.bp-primary-action:focus span,
#buddypress #reply-title small a:focus span,
#buddypress a.bp-primary-action:hover span,
#buddypress #reply-title small a:hover span                         { background: rgb(255, 255, 255); color: #757575; }

/* Button Load More */
body.buddypress div#buddydrive-main #buddydrive-load-more,
body .rtmedia-container .rtm-load-more a#rtMedia-galary-next,
.load-more a,
.load-newest a                                                      { display: block; width: 100%; height: 55px; padding: 18px; font-family: '{$monarch_font_secondary}', sans-serif; }

input[type="button"],
input[type="submit"]                                                { width: auto; }

/* Button Disabled */
input[type="submit"].pending,
input[type="button"].pending,
input[type="reset"].pending,
input[type="submit"].disabled,
input[type="button"].disabled,
input[type="reset"].disabled,
input[type="submit"][disabled=disabled],
input[type="submit"][disabled],
button.pending,
button.disabled,
div.pending a,
a.disabled                                                          { border-color: #ddd; color: #bbb; cursor: default; -webkit-box-shadow: none; box-shadow: none; }

/* Button Disabled Hover */
input[type="submit"].pending:hover,
input[type="button"].pending:hover,
input[type="reset"].pending:hover,
input[type="submit"].disabled:hover,
input[type="button"].disabled:hover,
input[type="reset"].disabled:hover,
input[type="submit"][disabled=disabled]:hover,
input[type="submit"][disabled]:hover,
button.pending:hover,
button.disabled:hover,
div.pending a:hover,
a.disabled:hover                                                    { border-color: #eee; color: #fff; }

/* Button Animation */
body .rtmedia-single-container button.rtmedia-like[disabled=disabled],
body .rtmedia-actions-before-comments .rtmedia-comment-link[disabled=disabled],
body #buddypress .rtmedia-actions-before-comments .rtmedia-like[disabled=disabled],
.button.loading,
.load-more.loading a                                                { background-repeat: repeat; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMAQMAAABsu86kAAAABlBMVEX+/v4AAAAbQk4OAAAAAnRSTlNAAIbqgj0AAAAcSURBVAjXY+gzYJApYLB4wFB4gOFxA8NxAUwRAMfUC5sqMpN5AAAAAElFTkSuQmCC); -webkit-animation: loading 2s infinite; -o-animation: loading 2s infinite; animation: loading 2s infinite; }

*[disabled],
form *[disabled="disabled"]                                         { cursor: default; opacity: 0.4; }

.toolbar-scrollup .item .ion-navicon-round::before                  { content: '\\f107'; }

CSS;

}

if ( $monarch_css_image_outlines == 'atlass' || $monarch_css_image_outlines == 'monarch' ) {

$css .= <<<CSS

/*
  ========================================
  Images Outlines Monarch
  ========================================
*/

body .userprofile.rtm-user-avatar::after,
#bbpress-forums .bbp-topic-title p.bbp-topic-meta .bbp-author-avatar::after,
.bbp-logged-in .user-submit::after,
.widget.widget_flickr ul li a::after,
.widget.widget_dribbble ul li a::after,
.widget.widget_comments ul li a .image::after,
.widget.widget_bp_core_friends_widget .item-list li .item-avatar a::after,
.widget.widget_about .aboutwidget .image::after,
.widget.buddypress .bp-login-widget-user-avatar a::after,
.widget.widget_bp_core_whos_online_widget .item-avatar a::after,
.widget.widget_bp_core_recently_active_widget .item-avatar a::after,
.widget.widget_bp_core_members_widget .item-list li .item-avatar a::after,
.widget.widget_bp_groups_widget .item-list li .item-avatar a::after { content: ""; position: absolute; top: 1px; left: 1px; right: 1px; bottom: 1px; border: 1px solid rgba(255, 255, 255, 0.25); -webkit-box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1); box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1); z-index: 1; }

.article-image::after,
.relatedposts .image a::after,
#bbpress-forums p.bbp-topic-meta span.bbp-topic-freshness-author .bbp-author-avatar::after,
#bbpress-forums div.bbp-reply-author .bbp-author-avatar::after,
.widget.widget_posts .image a::after,
.author-info .author-avatar::after                                  { content: ""; position: absolute; top: 3px; left: 3px; right: 3px; bottom: 3px; border: 1px solid rgba(255, 255, 255, 0.25); -webkit-box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1); box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1); z-index: 1; }

.elem .cover-loop a::after,
#buddypress .masonry li .item-avatar a::after,
.post-wrap article.has-post-thumbnail.format-standard .post-thumbnail::after,
.post-wrap article.has-post-thumbnail.format-gallery .post-thumbnail::after,
.post-wrap.with-avatar .avatar-link::after,
.post-wrap article.has-post-thumbnail.type-page .post-thumbnail::after,
.post-wrap .post-front-block .post-header::after                    { content: ""; position: absolute; top: 5px; left: 5px; right: 5px; bottom: 5px; border: 1px solid rgba(255, 255, 255, 0.25); -webkit-box-shadow: 0 0 0 5px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1); box-shadow: 0 0 0 5px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1); z-index: 1; }

CSS;

}

if ( $monarch_css_image_outlines == 'atlass' ) {

$css .= <<<CSS

/*
  ========================================
  Images Outlines Atlass
  ========================================
*/

.post-wrap .hentry.page.type-page::before                           { background: -webkit-linear-gradient(135deg, rgba(0, 0, 0, 0.04) 45%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0) 56%, rgba(255, 255, 255, 0) 80%); background: linear-gradient(315deg, rgba(0, 0, 0, 0.06) 45%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0) 56%, rgba(255, 255, 255, 0) 80%); }

.elem .cover-loop a::after,
#buddypress .masonry li .item-avatar a::after, 
.post-wrap article.has-post-thumbnail.format-standard .post-thumbnail::after, 
.post-wrap article.has-post-thumbnail.format-gallery .post-thumbnail::after, 
.post-wrap.with-avatar .avatar-link::after, 
.post-wrap article.has-post-thumbnail.type-page .post-thumbnail::after, 
.post-wrap .post-front-block .post-header::after                    { top: 1px; left: 1px; bottom: 1px; right: 1px; }

body .article-image::after,
.post-wrap.with-avatar .without-post-thumbnail .avatar-link::after,
.relatedposts .image a::after, 
#bbpress-forums p.bbp-topic-meta span.bbp-topic-freshness-author .bbp-author-avatar::after, 
#bbpress-forums div.bbp-reply-author .bbp-author-avatar::after, 
.widget.widget_posts .image a::after, 
.author-info .author-avatar::after                                  { top: 1px; left: 1px; right: 1px; bottom: 1px; border: 1px solid rgba(255, 255, 255, 0.25); -webkit-box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1); box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1), 0 0 0 1px rgba(0, 0, 0, 0.1); }

.post-wrap.with-avatar .without-post-thumbnail .avatar-link::after,
.post-wrap.with-avatar .without-post-thumbnail .avatar-link::after  { margin: 5px; }
.post-wrap.with-avatar .without-post-thumbnail .avatar-link         { padding: 5px; }

.post-navigation .nav-next a::before,
.post-navigation .nav-previous a::before                            { top: 1px!important; left: 1px!important; right: 1px!important; bottom: 1px!important; }

CSS;

}

if ( $monarch_css_alerts == 'atlass' ) {

$css .= <<<CSS

/*
  ========================================
  Alerts Atlass
  ========================================
*/

body .learn-press-message,
.sa-error-container,
#bp-uploader-warning,
#buddypress div.error,
#buddypress p.success,
#buddypress p.updated,
#buddypress p.warning,
#message,
#sitewide-notice,
#pass-strength-result,
.sa-error-container,
.woocommerce .woocommerce-error,
.woocommerce .woocommerce-info,
.woocommerce .woocommerce-message,
body .rtmedia-no-media-found,
body .rtmedia-success,
body .rtmedia-warning,
body.buddypress .error,
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback p.info,
.indicator-hint {
	background-color: {$colors['second_main_color']};
	border-color: {$colors['second_main_color']};
	color: #fff;
	opacity: 1;
}

body .learn-press-message::before,
.sa-error-container::before,
#bp-uploader-warning::before,
#buddypress #pass-strength-result::before,
#buddypress div.error::before,
#buddypress p.success::before,
#buddypress p.updated::before,
#buddypress p.warning::before,
#message::before,
#sitewide-notice::before,
.woocommerce .woocommerce-error::before,
.sa-error-container::before,
.woocommerce .woocommerce-info::before,
.woocommerce .woocommerce-message::before,
body .rtmedia-no-media-found::before,
body .rtmedia-success::before,
body .rtmedia-warning::before,
body.buddypress .error::before,
body.buddypress div#buddydrive-main #buddydrive-status div.buddydrive-feedback p.info::before,
.indicator-hint::before {
	background-color: {$colors['second_main_color']};
}

CSS;

}

if ( $monarch_css_buttons == 'atlass' ) {

$css .= <<<CSS

/*
  ========================================
  Buttons Atlass
  ========================================
*/

/* Button Default */
body .woocommerce div.product form.cart .button,
.page .post-edit-link,
.page #vc_load-inline-editor,
body .delete-logo,
body input.bpfb_primary_button,
body .bpfb_controls_container .qq-upload-button,
#rtmedia_create_new_album,
#subscription-toggle a,
.activity-comments li.show-all a,
.activity-read-more a,
.bbpress #wp-link-cancel .button,
.bbpress #wp-link-submit,
.bp-title-button,
.btn,
.btn.btn-default,
.btn.btn-primary,
.button-nav li a,
.widget_polls-widget .Buttons,
.wp-polls .Buttons,
.wp-social-login-widget a,
body #whats-new-submit #aw-whats-new-submit,
body .rtmedia-add-media-button,
body button.rtmedia-comment-media-upload,
body.buddypress div#buddydrive-main #buddydrive-uploader #bp-upload-ui:not(.drag-drop) #bp-browse-button,
body.buddypress div#buddydrive-main .buddydrive-actions a,
body.buddypress div#buddydrive-main .button-primary,
body.buddypress div#buddydrive-main #buddydrive-load-more,
body .rtmedia-container .rtm-load-more .rtmedia-page-link.button,
body .rtmedia-gallery-item-actions a,
body .rtmedia-single-container button.rtmedia-like,
button[type="submit"],
input[type="button"],
#buddypress input[type=submit],
input[type="submit"]                                               { font-size: 11px; color: #fff; padding: 10px 12px; line-height: 1; position: relative; height: 32px; font-weight: bold; font-weight: 600; border: 0; -webkit-box-shadow: none; box-shadow: none; -webkit-transition: all 0.3s; -moz-transition: all 0.3s; transition: all 0.3s; display: inline-block; z-index: 1; text-shadow: none; border-radius: 5px; margin: 4px; font-family: '{$monarch_font_secondary}', sans-serif; background-color: {$colors['second_main_color']}; }

/* Button Default Focus & Hover */
body .woocommerce div.product form.cart .button:focus,
.page .post-edit-link:focus,
.page #vc_load-inline-editor:focus,
body .delete-logo:focus,
body input.bpfb_primary_button:focus,
body .bpfb_controls_container .qq-upload-button:focus,
#rtmedia_create_new_album:focus,
#subscription-toggle a:focus,
.activity-comments li.show-all a:focus,
.activity-read-more a:focus,
.bbpress #wp-link-cancel .button:focus,
.bbpress #wp-link-submit:focus,
.bp-title-button:focus,
.btn.btn-default:focus,
.btn.btn-primary:focus,
.btn:focus,
.button-nav li a:focus,
.widget_polls-widget .Buttons:focus,
.wp-polls .Buttons:focus,
.wp-social-login-widget a:focus,
body #whats-new-submit #aw-whats-new-submit:focus,
body.buddypress div#buddydrive-main .button-primary:focus,
body.buddypress div#buddydrive-main #buddydrive-load-more:focus,
body.buddypress div#buddydrive-main #buddydrive-uploader #bp-upload-ui:not(.drag-drop) #bp-browse-button:focus,
body .rtmedia-add-media-button:focus,
body button.rtmedia-comment-media-upload:focus,
body .rtmedia-container .rtm-load-more .rtmedia-page-link.button:focus,
body .rtmedia-gallery-item-actions a:focus,
body.buddypress div#buddydrive-main .buddydrive-actions a:focus,
body .rtmedia-single-container button.rtmedia-like:focus,
body .wp-polls .Buttons:focus, 
button[type="submit"]:focus,
input[type="button"]:focus,
#buddypress input[type=submit]:focus,
input[type="submit"]:focus,

body .woocommerce div.product form.cart .button:hover,
.page .post-edit-link:hover,
.page #vc_load-inline-editor:hover,
body .delete-logo:hover,
body input.bpfb_primary_button:hover,
body .bpfb_controls_container .qq-upload-button:hover,
#rtmedia_create_new_album:hover,
#subscription-toggle a:hover,
.activity-comments li.show-all a:hover,
.activity-read-more a:hover,
.bbpress #wp-link-cancel .button:hover,
.bbpress #wp-link-submit:hover,
.bp-title-button:hover,
.btn.btn-default:hover,
.btn.btn-primary:hover,
.btn:hover,
.button-nav li a:hover,
.widget_polls-widget .Buttons:hover,
.wp-polls .Buttons:hover,
.wp-social-login-widget a:hover,
body #whats-new-submit #aw-whats-new-submit:hover,
body.buddypress div#buddydrive-main .button-primary:hover,
body.buddypress div#buddydrive-main #buddydrive-load-more:hover,
body.buddypress div#buddydrive-main #buddydrive-uploader #bp-upload-ui:not(.drag-drop) #bp-browse-button:hover,
body .rtmedia-add-media-button:hover,
body button.rtmedia-comment-media-upload:hover,
body .rtmedia-container .rtm-load-more .rtmedia-page-link.button:hover,
body .rtmedia-gallery-item-actions a:hover,
body.buddypress div#buddydrive-main .buddydrive-actions a:hover,
body .rtmedia-single-container button.rtmedia-like:hover,
body .wp-polls .Buttons:hover, 
button[type="submit"]:hover,
input[type="button"]:hover,
#buddypress input[type=submit]:hover,
input[type="submit"]:hover                                           { color: #fff; text-decoration: none; opacity: 0.8; }

/* Button Secondary */
.woocommerce #respond input#submit,
.woocommerce a.button,
.woocommerce button.button,
.woocommerce input.button,
.acomment-options a,
.bp-login-widget-user-logout .logout,
.generic-button a,
.load-more a,
.load-newest a,
.notifications a.delete,
.notifications a.mark-read,
.notifications a.mark-unread,
.thread-options a.delete,
.thread-options a.read,
.thread-options a.unread,
body #buddypress #rtm-media-options-list .rtm-options .button,
body #buddypress .rtmedia-actions-before-comments .rtmedia-like,
body.buddypress div#buddydrive-main ul#buddydrive-manage-actions li a,
body #rtm-media-options-list .rtm-options.rtm-options .rtmedia-delete-album,
body .imgedit-group .dashicons.imgedit-help-toggle,
body .rtm-media-options .rtm-media-options-list .rtmedia-action-buttons,
body .rtm-options.rtm-options a,
body .rtm-tabs a,
body .rtmedia-actions-before-comments .rtmedia-comment-link,
body .rtmedia-container .rtm-load-more a#rtMedia-galary-next,
body .rtmedia-edit-media-tabs .rtm-tabs a,
body .rtmedia-upload-media-link,
body a.rtmedia-upload-media-link,
.button                                                             { color: #fff; vertical-align: middle; position: relative; z-index: 25; display: inline-block; line-height: 1.3; border: 2px solid #eee; -webkit-box-shadow: none; box-shadow: none; padding: 4px 10px; font-size: 10px; height: auto; font-weight: bold; font-weight: 600; min-height: 25px; text-transform: uppercase; border-radius: 12px; margin: 0; background-color: {$colors['second_main_color']}; }

/* Activity Buttons */
.activity-list .acomment-options a,
.activity-list .button                                              { border-color: #fff; }

/* Button Secondary Focus & Hover */
.acomment-options a:focus,
.bp-login-widget-user-logout .logout:focus,
.woocommerce #respond input#submit:focus,
.woocommerce a.button:focus,
.woocommerce button.button:focus,
.woocommerce input.button:focus,
.button:focus,
.generic-button a:focus,
.load-more a:focus,
.load-newest a:focus,
.notifications a.delete:focus,
.notifications a.mark-read:focus,
.notifications a.mark-unread:focus,
.thread-options a.delete:focus,
.thread-options a.read:focus,
.thread-options a.unread:focus,
body #buddypress #rtm-media-options-list .rtm-options .button:focus,
body #buddypress .rtmedia-actions-before-comments .rtmedia-like:focus,
body #rtm-media-options-list .rtm-options.rtm-options .rtmedia-delete-album:focus,
body .imgedit-group .dashicons.imgedit-help-toggle:focus,
body .rtm-media-options .rtm-media-options-list .rtmedia-action-buttons:focus,
body .rtm-options.rtm-options a:focus,
body .rtm-tabs a:focus,
body .rtmedia-actions-before-comments .rtmedia-comment-link:focus,
body .rtmedia-container .rtm-load-more a#rtMedia-galary-next:focus,
body .rtmedia-edit-media-tabs .rtm-tabs a:focus,
body .rtmedia-upload-media-link:focus,
body a.rtmedia-upload-media-link:focus,
body.buddypress div#buddydrive-main ul#buddydrive-manage-actions li a:focus,

.woocommerce #respond input#submit:hover,
.woocommerce a.button:hover,
.woocommerce button.button:hover,
.woocommerce input.button:hover,
.acomment-options a:hover,
.bp-login-widget-user-logout .logout:hover,
.button:hover,
.generic-button a:hover,
.load-more a:hover,
.load-newest a:hover,
.notifications a.delete:hover,
.notifications a.mark-read:hover,
.notifications a.mark-unread:hover,
.thread-options a.delete:hover,
.thread-options a.read:hover,
.thread-options a.unread:hover,
body #buddypress #rtm-media-options-list .rtm-options .button:hover,
body #buddypress .rtmedia-actions-before-comments .rtmedia-like:hover,
body #rtm-media-options-list .rtm-options.rtm-options .rtmedia-delete-album:hover,
body .imgedit-group .dashicons.imgedit-help-toggle:hover,
body .rtm-media-options .rtm-media-options-list .rtmedia-action-buttons:hover,
body .rtm-options.rtm-options a:hover,
body .rtm-tabs a:hover,
body .rtmedia-actions-before-comments .rtmedia-comment-link:hover,
body .rtmedia-container .rtm-load-more a#rtMedia-galary-next:hover,
body .rtmedia-edit-media-tabs .rtm-tabs a:hover,
body .rtmedia-upload-media-link:hover,
body a.rtmedia-upload-media-link:hover,
body.buddypress div#buddydrive-main ul#buddydrive-manage-actions li a:hover { text-decoration: none; color: #fff; opacity: 0.8; background-color: {$colors['second_main_color']}; }

/* Button Counter */
#buddypress a.bp-primary-action span,
#buddypress #reply-title small a span                               { background: rgba(255, 255, 255, 0.4); border-radius: 50%; padding: 1px 5px 2px; color: #fff; -webkit-transition: all 0.3s; -moz-transition: all 0.3s; transition: all 0.3s; text-align: center; }

/* Button Hover Counter */
#buddypress a.bp-primary-action:focus span,
#buddypress #reply-title small a:focus span,
#buddypress a.bp-primary-action:hover span,
#buddypress #reply-title small a:hover span                         { background: rgba(255, 255, 255, 0.4); }

/* Button Load More */
body.buddypress div#buddydrive-main #buddydrive-load-more,
body .rtmedia-container .rtm-load-more a#rtMedia-galary-next,
.load-more a,
.load-newest a                                                      { display: block; width: 100%; height: 55px; padding: 18px; font-size: 12px; text-transform: none; border-radius: 5px; border: 2px solid #fff!important; }

input[type="button"],
input[type="submit"]                                                { width: auto; }

/* Button Disabled */
input[type="submit"].pending,
input[type="button"].pending,
input[type="reset"].pending,
input[type="submit"].disabled,
input[type="button"].disabled,
input[type="reset"].disabled,
input[type="submit"][disabled=disabled],
input[type="submit"][disabled],
button.pending,
button.disabled,
div.pending a,
a.disabled                                                          { border-color: #ddd; color: #bbb; cursor: default; -webkit-box-shadow: none; box-shadow: none; }

/* Button Disabled Hover */
input[type="submit"].pending:hover,
input[type="button"].pending:hover,
input[type="reset"].pending:hover,
input[type="submit"].disabled:hover,
input[type="button"].disabled:hover,
input[type="reset"].disabled:hover,
input[type="submit"][disabled=disabled]:hover,
input[type="submit"][disabled]:hover,
button.pending:hover,
button.disabled:hover,
div.pending a:hover,
a.disabled:hover                                                    { border-color: #eee; color: #fff; }

/* Button Animation */
body .rtmedia-single-container button.rtmedia-like[disabled=disabled],
body .rtmedia-actions-before-comments .rtmedia-comment-link[disabled=disabled],
body #buddypress .rtmedia-actions-before-comments .rtmedia-like[disabled=disabled],
.button.loading,
.load-more.loading a                                                { background-repeat: repeat; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMAQMAAABsu86kAAAABlBMVEX+/v4AAAAbQk4OAAAAAnRSTlNAAIbqgj0AAAAcSURBVAjXY+gzYJApYLB4wFB4gOFxA8NxAUwRAMfUC5sqMpN5AAAAAElFTkSuQmCC); -webkit-animation: loading 2s infinite; -o-animation: loading 2s infinite; animation: loading 2s infinite; }

*[disabled],
form *[disabled="disabled"]                                         { cursor: default; opacity: 0.4; }

body button#rtmedia-add-media-button-post-update                    { margin-top: 18px; }

.button,
.imgedit-menu .button,
#buddypress .masonry li .action a,
#buddypress .item-list-tabs ul li a span,
.wp-polls .Buttons,
.widget_polls-widget .Buttons {
	background-color: {$colors['second_main_color']};
}

/*
  ========================================
  Share Buttons
  ========================================
*/

#share                                                              { background: none!important; }
#share span                                                         { padding: 0 10px; width: 130px; margin-bottom: 25px; } 
#share span a                                                       { width: 110px; } 
#share span a[data-count="fb"]                                      { background-color: #3b5998; box-shadow: inset 0 1px 0 #3B5FAA,0 5px 0 0 #2D4881,0 10px 5px #222; -webkit-box-shadow: inset 0 1px 0 #3B5FAA,0 5px 0 0 #2D4881,0 10px 5px #222; text-shadow: 1px 1px 0 #2D4881; } 
#share span a[data-count="gplus"]                                   { background-color: #DB4A39; box-shadow: inset 0 1px 0 #E96555,0 5px 0 0 #B43728,0 10px 5px #222; -webkit-box-shadow: inset 0 1px 0 #E96555,0 5px 0 0 #B43728,0 10px 5px #222; text-shadow: 1px 1px 0 #B43728; } 
#share span a[data-count="twi"]                                     { background: #109bce; box-shadow: inset 0 1px 0 #2ab7ec,0 5px 0 0 #07526e,0 10px 5px #222; -webkit-box-shadow: inset 0 1px 0 #2ab7ec,0 5px 0 0 #07526e,0 10px 5px #222; text-shadow: 1px 1px 0 #07526e; } 
#share span a[data-count="pin"]                                     { background: #da0c23; box-shadow: inset 0 1px 0 #bd081c,0 5px 0 0 #980011,0 10px 5px #222; -webkit-box-shadow: inset 0 1px 0 #bd081c,0 5px 0 0 #980011,0 10px 5px #222; text-shadow: 1px 1px 0 #82000e; } 
#share span a[data-count="tumb"]                                    { background: #455875; box-shadow: inset 0 1px 0 #212d40,0 5px 0 0 #28364a,0 10px 5px #222; -webkit-box-shadow: inset 0 1px 0 #212d40,0 5px 0 0 #28364a,0 10px 5px #222; text-shadow: 1px 1px 0 #37465D; }

#share span a[data-count*="fb"]:hover                               { background-color: #314d91; }
#share span a[data-count*="gplus"]:hover                            { background-color: #bf4132; }
#share span a[data-count*="pin"]:hover                              { background-color: #af081a; }
#share span a[data-count*="twi"]:hover                              { background-color: #0d89b7; }
#share span a[data-count*="tumb"]:hover                             { background-color: #37465D; }

#share span a:active                                                { top: 3px; box-shadow: inset 0 1px 0 #666, 0 2px 0 0 #444, 0 5px 3px #222; -webkit-box-shadow: inset 0 1px 0 #666, 0 2px 0 0 #444, 0 5px 3px #222; }

/*
  ========================================
  Dark Inputs
  ========================================
*/

.loginform-action .icheckbox,
#register-page .icheckbox,
#loginform .icheckbox                                               { border: 1px solid rgba(0, 0, 0, 0.2); -webkit-box-shadow: 0 0 0 3px rgba(255, 255, 255, 0.1); box-shadow: 0 0 0 3px rgba(255, 255, 255, 0.1); }

#register-page .iradio::before,
#loginform .iradio::before                                          { border-color: #333; }

body .rtmedia-popup,
.bbpress #wp-link-wrap,
.modal-theme .modal-dialog                                          { border-width: 5px; border-style: solid; -webkit-box-shadow: none!important; box-shadow: none!important; padding: 0; }

body .rtmedia-popup                                                 { background-color: {$colors['panels_background_color']}; }

.wp-social-login-widget .wp-social-login-provider-list a            { width: 37px; height: 37px; }
.standard-form .wp-social-login-widget .wp-social-login-provider-list a,
.modal .wp-social-login-widget .wp-social-login-provider-list a     { background-color: rgba(0, 0, 0, 0.08)!important; color: rgba(255, 255, 255, 0.4); border: 1px solid rgba(255, 255, 255, 0.1); }
.standard-form .wp-social-login-widget .wp-social-login-provider-list a:hover,
.modal .wp-social-login-widget .wp-social-login-provider-list a:hover{ opacity: 0.9; }

body input.rtmedia-merge-selected,
body #rtmedia_create_new_album,
body.bbpress #wp-link-cancel .button,
body.bbpress #wp-link-submit,
.modal-theme input[type="submit"]                                   { border: 1px solid rgba(255, 255, 255, 0.08); }

.registration #buddypress .standard-form input[type="submit"]       { border-style: solid; border-width: 1px; color: rgba(255, 255, 255, 0.6); }
.registration #buddypress .standard-form input[type="submit"]:hover,
.registration #buddypress .standard-form input[type="submit"]:focus { color: #fff; }

body #rtmedia_create_new_album,
body input.rtmedia-merge-selected,
body.bbpress #wp-link-cancel .button,
body.bbpress #wp-link-submit,
.modal-theme input[type="submit"]                                   { background-color: rgba(0, 0, 0, 0.08)!important; border-color: rgba(255, 255, 255, 0.07); color: rgba(255, 255, 255, 0.4); }
.modal-theme input[type="submit"]:hover,
body #rtmedia_create_new_album:focus,
body #rtmedia_create_new_album:hover,
body input.rtmedia-merge-selected:focus,
body input.rtmedia-merge-selected:hover,
body.bbpress #wp-link-cancel .button:focus,
body.bbpress #wp-link-cancel .button:hover,
body.bbpress #wp-link-submit:focus,
body.bbpress #wp-link-submit:hover,
.modal-theme input[type="submit"]:focus                             { color: rgba(255, 255, 255, 0.6); border-color: rgba(255, 255, 255, 0.15); background-color: rgba(255, 255, 255, 0.05); }

body input.rtmedia-merge-selected,
body #rtmedia_create_new_album,
body.bbpress #wp-link-cancel .button,
body.bbpress #wp-link-submit,
.modal-theme input[type="submit"]                                   { background-color: rgba(255, 255, 255, 0.03); border-radius: 0; border-color: rgba(255, 255, 255, 0.07); color: rgba(255, 255, 255, 0.4); }
body #rtmedia_create_new_album:hover,
body #rtmedia_create_new_album:focus,
body input.rtmedia-merge-selected:focus,
body.bbpress #wp-link-cancel .button:hover,
body.bbpress #wp-link-cancel .button:focus,
body.bbpress #wp-link-submit:hover,
body.bbpress #wp-link-submit:focus,
body input.rtmedia-merge-selected:hover,
.modal-theme input[type="submit"]:hover,
.modal-theme input[type="submit"]:focus                             { color: rgba(255, 255, 255, 0.6); border-color: rgba(255, 255, 255, 0.15); background-color: rgba(255, 255, 255, 0.05); }

/*
  ========================================
  rtMedia
  ========================================
*/

body .rtmedia-container .rtm-media-options .rtm-media-options-list .rtmedia-action-buttons,
body .rtmedia-container .rtmedia-upload-media-link,
body .rtmedia-container .rtm-tabs li a                              { padding: 4px 10px; }

body .rtmedia-container .rtm-tabs li a .dashicons,
body .rtmedia-container .rtm-options .dashicons,
body .rtm-media-options .dashicons                                  { line-height: 1.2; width: 15px; height: 11px; }

body .rtm-media-options .rtmedia-upload-media-link                  { border-right: 2px solid #fff; }

body #buddypress .rtmedia-container #rtmedia-single-media-container.rtmedia-single-media .rtm-options .button,
body #buddypress #rtm-media-options-list .rtm-options.rtm-options .rtmedia-delete-album,
body #buddypress .rtmedia-container #rtm-media-options-list .rtm-options .button,
body .rtmedia-container .rtm-options.rtm-options a                  { color: #fff; padding: 10px 10px; }

body .rtmedia-container .rtmedia-gallery-item-actions a             { color: #454545; }
body .rtmedia-container .rtmedia-gallery-item-actions a:focus,
body .rtmedia-container .rtmedia-gallery-item-actions a:hover       { color: #fff; }

/*
  ========================================
  BuddyDrive
  ========================================
*/

body.buddypress div#buddydrive-main ul#buddydrive-browser.bulk-select li.buddydrive-item.bulk-selected .buddydrive-icon,
body.buddypress div#buddydrive-main ul#buddydrive-browser.bulk-select li.buddydrive-item.bulk-selected .buddydrive-content { border-style: solid; }

body.buddypress div#buddydrive-main .buddydrive-actions a.buddydrive-edit:before,
body.buddypress div#buddydrive-main .buddydrive-actions a.buddydrive-share:before { font-size: 16px; }

/*
  ========================================
  Base
  ========================================
*/

#buddypress .activity-list .activity-avatar a img                   { z-index: 2; }
#buddypress .activity-list .activity-avatar a::before               { content: ""; position: absolute; display: block; width: 50px; height: 85px; background: {$colors['background_color']}; top: -15px; left: 0; z-index: 1; }

body .drag-drop #drag-drop-area                                     { border-style: dotted; }

.author-info .author-link                                           { font-style: italic; }
.author-info .author-description .author-bio                        { color: #737a90; }
#buddypress .activity-comments .acomment-meta .activity-time-since,
#buddypress .activity-list .activity-header p .time-since           { color: #706a88; }

.pagination .pag-count,
#buddypress #whats-new-form p.activity-greeting,
.users-who-like,
#buddypress .activity-comments .acomment-meta a,
#buddypress .activity-comments .acomment-meta,
#buddypress .activity-list .activity-header p a,
#buddypress .activity-list .activity-header,
.comments-area .comment-respond .comment-reply-title small a,
.comments-area article .comment-author a,
.comments-area article .comment-metadata a time,
.comments-area article .edit-link a::before,
.comments-area article .reply a,
body .rtm-comment-list .rtm-comment-wrap .rtmedia-comment-author a,
body .rtm-comment-list .rtm-comment-wrap .rtmedia-comment-date      { color: #4C5264; }

.author-info .author-description .author-bio,
.author-info .author-link,
.comments-area article .comment-content,
#buddypress .activity-comments .acomment-content,
#buddypress .activity-list .activity-inner                          { font-size: 13px; }
#bbpress-forums ul li.bbp-forum-info .bbp-forums-list li a          { font-size: 11px; }

CSS;

}

if ( get_theme_mod( 'monarch_css_grid', 'social' ) == 'blog' ) {

$css .= <<<CSS

/*
  ========================================
  GRID: BLOG (ALPHA, HIDDEN SETTING)
  ========================================
*/

.body-bg.blog .wrapper                         { margin: 0!important; }

.body-bg.blog::after,
.body-bg.blog::before,
.body-bg.blog .wrapper::after,
.body-bg.blog .wrapper::before                 { content: none; }

::-webkit-scrollbar              { width: 12px; }
::-webkit-scrollbar-track        { -webkit-box-shadow: inset 0 0 0px rgba(0,0,0,0.3); -webkit-border-radius: 0px; box-shadow: inset 0 0 0px rgba(0,0,0,0.3); border-radius: 0px; background: #f2f2f2; }

::-webkit-scrollbar-thumb,
::-webkit-scrollbar-thumb:window-inactive,
::selection                      { background-color: #555!important; }

.body-bg.blog .container,
.body-bg.blog .content                         { max-width: 1250px!important; margin: 0 auto!important; width: 1250px; }

.body-bg.blog .wrapper-sticky-header-panel,
.body-bg.blog .wrapper-sticky-user-panel       { display: none!important; }

.body-bg.blog .header-panel .site-header       { display: none; }

.body-bg.blog .navbar .avatar img { border-radius: 50%; width: 35px; margin-right: 5px; }

.body-bg.blog .wrapper-sticky-header-panel                          { display: block!important; }
.body-bg.blog .wrapper::before                                      { content: ""; }

.sidebar-open .body-bg.blog .wrapper-sticky-header-panel            { left: 0!important; -khtml-transition: all .2s; transition: all .2s; }
.sidebar-open .body-bg.blog .wrapper::before                        { left: 255px!important; }
.sidebar-open .body-bg.blog .toolbar-scrollup                       { left: 243px!important; }

.body-bg.blog .toolbar-scrollup,
.body-bg.blog .wrapper::before,
.body-bg.blog .wrapper-sticky-header-panel                          { -webkit-transition: all .2s; -moz-transition: all .2s; -ms-transition: all .2s; -o-transition: all .2s; }
.body-bg.blog .toolbar-scrollup,
.body-bg.blog .wrapper::before,
.body-bg.blog .wrapper-sticky-header-panel                          { left: -260px!important; }
.body-bg.blog .wrapper-sticky-header-panel                          { margin-top: 40px; }
.body-bg.blog .wrapper-sticky-header-panel                          { top: 176px!important; }

.body-bg.blog .navbar.navbar-inverse                                { z-index: 101; }

.body-bg.blog .footer-line-one         { background: {$colors['panels_background_color']}; padding: 0 0 20px; color: #fff; position: relative; }
.body-bg.blog .footer-line-one::before { content: ""; position: absolute; height: 60px; width: 100%; background: rgba(255, 255, 255, 0.1); display: block; }

.body-bg.blog .footer-line-one .widget-panel .widget-title { background: none; }

.body-bg.blog .footer-line-two         { background: {$colors['main_color']}; padding: 25px 0 10px; color: rgba(255, 255, 255, 0.8); font-weight: 600; border-top: 5px solid rgba(255, 255, 255, 0.1); }
.body-bg.blog .footer-line-two a       { color: rgba(255, 255, 255, 0.8); text-decoration: underline; }

.body-bg.blog .copyright               { display: inline-block; padding: 15px 0; }
.body-bg.blog .line.social .social     { display: inline-block; float: right; }

body .body-bg.blog .site-branding h1.site-title a { font-family: 'Exo 2', sans-serif; font-weight: 600!important; opacity: 0.9; text-transform: uppercase1; }

/*
  ========================================
  Navbar Blog
  ========================================
*/

.body-bg.blog .navbar.navbar-inverse .navbar-nav > li.dropdown > a::before { content: none; }
body .body-bg.blog .nav-buddy li a span.caret                              { padding: 0; position: static; right: auto; }

.body-bg.blog .navbar.navbar-inverse.navbar-color,
.body-bg.blog .navbar.navbar-inverse.navbar-second                     { margin: 0; border: 0; border-radius: 0; }

.body-bg.blog .navbar.navbar-inverse.navbar-color                      { background: {$colors['main_color']}; z-index: 102; border-top: 5px solid rgba(255, 255, 255, 0.1); }
.body-bg.blog .navbar.navbar-inverse.navbar-second                     { background: {$colors['panels_background_color']}; border-bottom: 5px solid rgba(255, 255, 255, 0.1); border-top: 5px solid rgba(255, 255, 255, 0.1); }

.body-bg.blog .navbar.navbar-inverse.navbar-color .navbar-nav > li > a { padding: 30px 25px; }

.body-bg.blog .navbar.navbar-inverse .navbar-brand                     { height: 95px; padding: 0; }
.body-bg.blog .navbar.navbar-inverse .site-header h1 a                 { border: 0; height: 95px; line-height: 95px; }
.body-bg.blog .navbar.navbar-inverse .site-header h1                   { background-color: transparent; }

.body-bg.blog .navbar.navbar-inverse .navbar-nav > li > a              { border-right: 1px solid rgba(255, 255, 255, 0.05); padding: 14px 25px 12px; font-weight: 600; border-bottom: 0; text-transform: none; color: rgba(255,255,255,0.75); max-height: 95px; }
.body-bg.blog .navbar.navbar-inverse .navbar-nav > li:first-child > a  { border-left: 1px solid rgba(255, 255, 255, 0.05); }
.body-bg.blog .navbar.navbar-inverse .navbar-nav > li::before          { content: none; }

.body-bg.blog .navbar.navbar-inverse .navbar-nav > .active > a,
.body-bg.blog .navbar.navbar-inverse .navbar-nav > .active > a:focus,
.body-bg.blog .navbar.navbar-inverse .navbar-nav > .active > a:hover,

.body-bg.blog .navbar.navbar-inverse .navbar-nav > li > a:hover,
.body-bg.blog .navbar.navbar-inverse .navbar-nav > .open > a,
.body-bg.blog .navbar.navbar-inverse .navbar-nav > .open > a:focus,
.body-bg.blog .navbar.navbar-inverse .navbar-nav > .open > a:hover     { color: #eee; background-color: rgba(255, 255, 255, 0.1); }

.body-bg.blog .navbar.navbar-inverse .navbar-nav > li > .dropdown-menu { border: 5px solid rgba(255, 255, 255, 0.1); border-top: 0; margin-top: 5px; border-radius: 0; padding: 0; background: {$colors['panels_background_color']}; }

.body-bg.blog .navbar.navbar-inverse .navbar-nav > li > a.notifications,
.body-bg.blog .user-panel .buddy-avatar .notifications                 { display: inline-block; position: absolute; right: 15px; font-size: 10px; padding: 3px 6px 4px; bottom: 15px; background: rgba(0, 0, 0, 0.3); border: 0; }

/*
  ========================================
  Navbar Default
  ========================================
*/

.body-bg.blog .navbar.navbar-default                              { margin: 0; border: 0; border-radius: 0; background-color: #f2f2f2; }
.body-bg.blog .navbar.navbar-default .navbar-brand                { height: 95px; padding: 0; }
.body-bg.blog .navbar.navbar-default .navbar-right                { display: none; }
.body-bg.blog .navbar.navbar-default .site-branding h1.site-title { background-color: transparent; }
.body-bg.blog .navbar.navbar-default .site-branding h1 a          { background-repeat: no-repeat; background-position: 50% 50%; text-indent: -99999px; color: transparent; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: contain; background-size: 50%; }

CSS;

}

$css .= <<<CSS

@-webkit-keyframes infload {
	0%, 80%, 100% {
		-webkit-box-shadow: 0 0 {$colors['light_main_color']};
		height: 4em;
	} 40% {
		-webkit-box-shadow: 0 -2em {$colors['light_main_color']};
		height: 5em;
	}
}

@-moz-keyframes infload {
	0%, 80%, 100% {
		-moz-box-shadow: 0 0 {$colors['light_main_color']};
		height: 4em;
	} 40% {
		-moz-box-shadow: 0 -2em {$colors['light_main_color']};
		height: 5em;
	}
}

@-ms-keyframes infload {
	0%, 80%, 100% {
		box-shadow: 0 0 {$colors['light_main_color']};
		height: 4em;
	} 40% {
		box-shadow: 0 -2em {$colors['light_main_color']};
		height: 5em;
	}
}

@-o-keyframes infload {
	0%, 80%, 100% {
		box-shadow: 0 0 {$colors['light_main_color']};
		height: 4em;
	} 40% {
		box-shadow: 0 -2em {$colors['light_main_color']};
		height: 5em;
	}
}

@keyframes infload {
	0%, 80%, 100% {
		box-shadow: 0 0 {$colors['light_main_color']};
		height: 4em;
	} 40% {
		box-shadow: 0 -2em {$colors['light_main_color']};
		height: 5em;
	}
}

CSS;

	return $css;
}