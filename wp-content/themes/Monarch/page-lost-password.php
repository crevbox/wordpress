<?php

/*
*  Page Lost Password
*/

add_filter( 'body_class', 'specific_body_class_registration' );

get_header();

?>

<!-- Content -->
<div class="content with-sb buddypress clearfix">

  <!-- Main -->
  <main class="main col-xs-12 col-sm-12 col-md-12 col-lg-8 col-bg-8" role="main">

    <section class="section">

    <header id="masthead" class="site-header" role="banner">
      <div class="site-branding">
        <h1 class="site-title clearfix"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php echo get_bloginfo( 'description', 'display' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
      </div>
    </header>

    <div id="buddypress">
      <div class="standard-form">
        <h6 class="text-center">
          <?php echo get_theme_mod( 'monarch_translation_text_49', 'Lost Password' ); ?>
        </h6>
        <div class="loginform-action">
          <?php do_action( 'login_form' ); ?>
        </div>
        <p><?php echo get_theme_mod( 'monarch_translation_text_50', 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.' ); ?></p>
        <form id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
          <div id="basic-details-section">
            <p class="signup_email">
              <input type="text" placeholder="<?php echo get_theme_mod( 'monarch_translation_text_51', 'Username or email' ); ?>" name="user_login" class="login-password" id="signup_email">
            </p>
            <p class="lostpassword-submit">
              <input type="submit" name="submit" class="lostpassword-button" value="<?php echo get_theme_mod( 'monarch_translation_text_52', 'Reset Password' ); ?>"/>
            </p>
          </div>
        </form>
        <div class="clearfix"></div>
      </div>
      <br>
      <p class="text-center">
        <?php login_footer_links(); ?>
      </p>
    </div>

    </section>

  </main>

</div>

<?php get_footer(); ?>