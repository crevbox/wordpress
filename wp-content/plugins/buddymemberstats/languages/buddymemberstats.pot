# Copyright (C) 2015 Buddy Member Stats
# This file is distributed under the same license as the Buddy Member Stats package.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Buddy Member Stats 1.0\n"
"POT-Creation-Date: 2015-10-18 16:19+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _e;_x:1,2c;__;esc_html__;esc_html_e;esc_html_x:1,2c;"
"esc_attr__;esc_attr_e;esc_attr_x:1,2c;_ex:1,2c;_n:1,2,4d\n"
"X-Poedit-Basepath: ..\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"X-Generator: Poedit 1.8.5\n"
"X-Poedit-SearchPath-0: .\n"

#: buddymemberstats.php:331
msgid "Settings"
msgstr ""

#: buddymemberstats.php:332
msgid "About"
msgstr ""

#: includes/admin-includes/settings.php:17
msgid "Main Settings"
msgstr ""

#: includes/admin-includes/settings.php:46
msgctxt "Admin setup"
msgid "Stats view menu"
msgstr ""

#: includes/admin-includes/settings.php:59
msgctxt "Admin setup"
msgid "Stats options menu"
msgstr ""

#: includes/admin-includes/settings.php:176
msgid "Buddy Member Stats Settings"
msgstr ""

#: includes/admin-includes/settings.php:188
#: includes/component-includes/screens.php:410
msgid "Save Changes"
msgstr ""

#: includes/admin-includes/settings.php:245
msgid "Settings saved"
msgstr ""

#: includes/admin-includes/settings.php:281
msgctxt "Help tab title"
msgid "Member Stats Help"
msgstr ""

#: includes/admin-includes/settings.php:283
msgctxt "Help tab title"
msgid "User navigation - item names"
msgstr ""

#: includes/admin-includes/settings.php:286
msgctxt "Help tab instruction"
msgid "Stats view menu: the nav item name to view the stats."
msgstr ""

#: includes/admin-includes/settings.php:287
msgctxt "Help tab instruction"
msgid "Stats options menu: the nav item name to select stats options."
msgstr ""

#: includes/admin-includes/settings.php:290
msgctxt "Help tab instruction"
msgid ""
"You must click the Save Changes button at the bottom of the screen for new "
"settings to take effect."
msgstr ""

#: includes/admin-includes/settings.php:301
msgid "Click the help button in the right corner for additionnal informations."
msgstr ""

#: includes/admin.php:117
msgid ""
"BuddyPress is activated on the network, please deactivate Buddy Member Stats from "
"this site and make sure to activate Buddy Member Stats on the network."
msgstr ""

#: includes/admin.php:119
msgid ""
"Buddy Member Stats has been activated on a site where BuddyPress is not, please "
"deactivate BuddyMemberStats from this site and activate it on the same site where "
"BuddyPress is activated."
msgstr ""

#: includes/admin.php:140
#, php-format
msgid ""
"Buddy Member Stats requires at least <strong>BuddyPress %s</strong>, please "
"upgrade."
msgstr ""

#: includes/admin.php:169 includes/admin.php:170
msgctxt "Admin settings item"
msgid "Buddy Member Stats"
msgstr ""

#: includes/admin.php:179 includes/admin.php:180
msgid "Welcome to Buddy Member Stats"
msgstr ""

#: includes/admin.php:209
msgctxt "Settings error"
msgid "Please fill the empty field:"
msgstr ""

#: includes/admin.php:276
msgctxt "Welcome screen title"
msgid "Welcome to Buddy Member Stats"
msgstr ""

#: includes/admin.php:277
msgctxt "Welcome screen sub title"
msgid "Thank you for using Buddy Member Stats"
msgstr ""

#: includes/admin.php:281
msgctxt "Welcome screen"
msgid "Presentation"
msgstr ""

#: includes/admin.php:285
msgctxt "Welcome screen"
msgid "Buddy Member Stats at a glance"
msgstr ""

#: includes/admin.php:286
msgctxt "Welcome screen"
msgid ""
"Buddy Member Stats is the ideal tool to see at a glance the involvement and level "
"of contribution of each member of your site. By consolidating on each profile the "
"many figures released by BuddyPress or other plugins, Buddy Member Stats offers "
"administrators and members a powerfull performance board. Unlike BuddyPress who "
"informs you, in exemple, that you received 2 new messages, Buddy Member Stats "
"indicates the amount of all your messages, regardless of their status. It is the "
"same for all other counts of activities identified by Buddy Member Stats. In "
"doing so, Buddy Member Stats becomes the ideal results panel of your community."
msgstr ""

#: includes/admin.php:289
msgctxt "Welcome screen img alt"
msgid "The Buddy Member Stats view"
msgstr ""

#: includes/admin.php:294
msgctxt "Welcome screen"
msgid "Main view"
msgstr ""

#: includes/admin.php:295
msgctxt "Welcome screen"
msgid ""
"The main view displays up to five sections, where stats are grouped by activity "
"relevancy: personnal, community, blogging, groups and forums. "
msgstr ""

#: includes/admin.php:299
msgctxt "Welcome screen"
msgid "Section"
msgstr ""

#: includes/admin.php:300
msgctxt "Welcome screen"
msgid ""
"Each section is compiling totals coming from BuddyPress components or third "
"parties plugins like bbPress."
msgstr ""

#: includes/admin.php:307
msgctxt "Welcome screen"
msgid "Administration"
msgstr ""

#: includes/admin.php:311
msgctxt "Welcome screen"
msgid "Backend"
msgstr ""

#: includes/admin.php:312
msgctxt "Welcome screen"
msgid ""
"Once activated, Buddy Member Stats admins can change menu items to their need. No "
"other settings are avaible in backend"
msgstr ""

#: includes/admin.php:316
msgctxt "Welcome screen"
msgid "Stats display"
msgstr ""

#: includes/admin.php:317
msgctxt "Welcome screen"
msgid ""
"Users can change their stat display on profile in two ways: by global privacy or "
"just by sections. Stats can be public, for members only or private. Sections can "
"be hiden individually."
msgstr ""

#: includes/admin.php:324
msgctxt "Welcome screen"
msgid "Note"
msgstr ""

#: includes/admin.php:325
msgctxt "Welcome screen"
msgid ""
"Keep in mind that Buddy Member Stats displays only totals. Eg. My notifications "
"shows a total of read and unread messages. That's what statistics generally do: "
"totalize."
msgstr ""

#: includes/admin.php:329
msgctxt "Welcome screen"
msgid "Third parties integration"
msgstr ""

#: includes/admin.php:330
msgctxt "Welcome screen"
msgid ""
"Buddy Member Stats plays well with BuddyPress figures, but integrates also some "
"third parties counting fetched in:"
msgstr ""

#: includes/admin.php:338
msgctxt "Welcome screen"
msgid ""
"To get these third party figures, the respective plugin should be activated on "
"your site."
msgstr ""

#: includes/admin.php:343
msgctxt "Welcome screen"
msgid "Customization"
msgstr ""

#: includes/admin.php:344
msgctxt "Welcome screen"
msgid ""
"Buddy Member Stats Screen is fully customisable. It comes with several "
"placeholders where you can hook in. You can use the plugin's child-theme "
"compliance to style the output to your need, by using:"
msgstr ""

#: includes/admin.php:346
msgctxt "Welcome screen"
msgid "Enjoy and have fun !"
msgstr ""

#: includes/admin.php:352
msgctxt "Welcome screen"
msgid "Go to Buddy Member Stats settings"
msgstr ""

#: includes/component-includes/screens.php:95
msgid "Sorry, my stats are private."
msgstr ""

#: includes/component-includes/screens.php:102
msgid "Sorry, only members can view my stats."
msgstr ""

#: includes/component-includes/screens.php:118
msgid ""
"Please, go to your Stats settings to register this option and find some others."
msgstr ""

#: includes/component-includes/screens.php:127
#: includes/component-includes/screens.php:364
msgid "Personnal Stats"
msgstr ""

#: includes/component-includes/screens.php:133
msgid "My profile views:"
msgstr ""

#: includes/component-includes/screens.php:137
msgid "My favorites:"
msgstr ""

#: includes/component-includes/screens.php:141
msgid "My mentions:"
msgstr ""

#: includes/component-includes/screens.php:142
msgid "My notifications:"
msgstr ""

#: includes/component-includes/screens.php:146
msgid "My received messages:"
msgstr ""

#: includes/component-includes/screens.php:147
msgid "My starred messages:"
msgstr ""

#: includes/component-includes/screens.php:151
msgid "My friends:"
msgstr ""

#: includes/component-includes/screens.php:156
msgid "My followers:"
msgstr ""

#: includes/component-includes/screens.php:179
#: includes/component-includes/screens.php:372
msgid "Community Stats"
msgstr ""

#: includes/component-includes/screens.php:182
msgid "Member since"
msgstr ""

#: includes/component-includes/screens.php:184
msgid "Activity Ratio:"
msgstr ""

#: includes/component-includes/screens.php:184
msgid "My activities compared to total site activities"
msgstr ""

#: includes/component-includes/screens.php:185
msgid "Posted Attachments:"
msgstr ""

#: includes/component-includes/screens.php:189
msgid "Received Compliments:"
msgstr ""

#: includes/component-includes/screens.php:195
msgid "BuddyDrive files:"
msgstr ""

#: includes/component-includes/screens.php:199
msgid "BuddyDrive downloads:"
msgstr ""

#: includes/component-includes/screens.php:207
msgid "Followed members:"
msgstr ""

#: includes/component-includes/screens.php:228
#: includes/component-includes/screens.php:380
msgid "Blogging Stats"
msgstr ""

#: includes/component-includes/screens.php:233
msgid "My blogs:"
msgstr ""

#: includes/component-includes/screens.php:239
msgid "My posts:"
msgstr ""

#: includes/component-includes/screens.php:243
msgid "My comments:"
msgstr ""

#: includes/component-includes/screens.php:266
#: includes/component-includes/screens.php:389
msgid "Group Stats"
msgstr ""

#: includes/component-includes/screens.php:269
msgid "My groups:"
msgstr ""

#: includes/component-includes/screens.php:270
msgid "My updates:"
msgstr ""

#: includes/component-includes/screens.php:294
#: includes/component-includes/screens.php:400
msgid "Forum Stats"
msgstr ""

#: includes/component-includes/screens.php:298
msgid "Topics:"
msgstr ""

#: includes/component-includes/screens.php:299
msgid "Replies:"
msgstr ""

#: includes/component-includes/screens.php:343
msgid "Stats Display"
msgstr ""

#: includes/component-includes/screens.php:348
msgid "Who can see my stats page ?"
msgstr ""

#: includes/component-includes/screens.php:351
msgid "Anybody"
msgstr ""

#: includes/component-includes/screens.php:352
msgid "Just me"
msgstr ""

#: includes/component-includes/screens.php:353
msgid "Members"
msgstr ""

#: includes/component-includes/screens.php:359
msgid "Show/hide sections"
msgstr ""

#: includes/component-includes/screens.php:410
msgid "Clear"
msgstr ""

#: includes/component.php:86
msgctxt "Main nav item"
msgid "Member Stats"
msgstr ""

#: includes/functions.php:520
msgctxt "stat list title"
msgid "Forum:"
msgstr ""

#: includes/functions.php:525 includes/functions.php:528 includes/functions.php:531
#: includes/functions.php:534 includes/functions.php:537
msgctxt "stat list title"
msgid "Site:"
msgstr ""

#: includes/functions.php:525
msgctxt "role on list"
msgid "Site admin"
msgstr ""

#: includes/functions.php:528
msgctxt "role on list"
msgid "Editor"
msgstr ""

#: includes/functions.php:531
msgctxt "role on list"
msgid "Author"
msgstr ""

#: includes/functions.php:534
msgctxt "role on list"
msgid "Contributor"
msgstr ""

#: includes/functions.php:537
msgctxt "role on list"
msgid "Member"
msgstr ""

#: includes/functions.php:641
#, php-format
msgid "<a href=\"%s\">Back to my stats</a>"
msgstr ""

#: includes/functions.php:651
msgid "Yes"
msgstr ""

#: includes/functions.php:655
msgid "No"
msgstr ""
