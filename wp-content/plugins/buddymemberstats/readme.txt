Buddy Member Stats
==================
Contributors: danbp
Tags: stats, counter, BuddyPress
Requires at least: PHP 5.4, WordPress 4.0, BuddyPress 2.2
Tested up to: WordPress 4.6, BuddyPress 2.6.2
Stable tag: 1.0.1

== Description ==

A BuddyPress plugin that totals all kind of activity figures on member's profile.


== Installation ==

After you've downloaded and extracted the files:

1. Upload the complete 'buddymemberstats' folder to the '/wp-content/plugins/' directory OR install via the plugin installer
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to Buddy Member Stats settings page and give a name to each menu.

== Screenshots ==

1. User's Buddy Member Stats
2. Option Settings

== Changelog ==

* Version 1.0.1 
* Translation is now available on translate.wordpress.org
* Requires BuddyPress 2.2
* Tested Up to WordPress 4.5

* Version 1.0: Initial release