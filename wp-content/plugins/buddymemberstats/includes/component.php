<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'BuddyMemberStats_Component' ) ) :
/**
 * Main BuddyMemberStats Component Class
 *
 * @see http://codex.buddypress.org/developer/bp_component/
 */
class Buddy_Member_Stats_Component extends BP_Component {

	/**
	 * Constructor method
	 *
	 * @package BuddyMemberStats
	 * @subpackage Component
	 * @since 1.0
	 *
	 * @uses buddymemberstats_get_component_id() to get the id of the component
	 * @uses buddymemberstats_get_component_name() to get the name of the component
	 * @uses buddymemberstats_get_component_includes_dir() to get component's include dir
	 * @uses buddypress() to get BuddyPress main instance
	 * @uses bp_is_active() to check if settings component is active
	 */
	function __construct() {

		parent::start(
			buddymemberstats_get_component_id(),
			buddymemberstats_get_component_name(),
			buddymemberstats_get_component_includes_dir()
		);

	 	$this->includes();
		
		buddypress()->active_components[$this->id] = '1';

		if( bp_is_active( 'settings' ) ) {
			add_action( 'bp_settings_setup_nav',       array( $this, 'setup_settings_bp_nav' ) );
			add_action( 'bp_settings_setup_admin_bar', array( $this, 'setup_settings_wp_nav' ) );
		}
	}

	/**
	 * Includes the needed files
	 *
	 * @package BuddyMemberStats
	 * @subpackage Component
	 * @since 1.0
	 *
	 * @param $args array
	 * @uses buddymemberstats_get_component_slug() to get the slug of the component
	 * @uses buddypress() to get BuddyPress main instance
	 */
	public function includes( $includes = array() ) {

		// Files to include
		$includes = array(
			'functions.php',
			'screens.php',
		);

		parent::includes( $includes );
	}

	/**
	 * Builds the BuddyPress user nav
	 * 
	 * @package BuddyMemberStats
	 * @subpackage Component
	 * @since 1.0
	 * 
	 * @param $main_nav array
	 * @param $sub_nav array
	 * @uses  buddymemberstats_get_component_primary_subnav_slug() to get the slug for subnav
	 * @uses  bp_displayed_user_domain() to get the displayed user profile url
	 * @uses  bp_loggedin_user_domain() to get the loggedin user profil url
	 * @uses  buddymemberstats_get_component_primary_subnav_name() to get the primary nav name
	 * @uses  buddymemberstats_get_component_primary_subnav_slug() to get the primary nav slug
	 * @uses  buddymemberstats_get_component_options_subnav_name() to get the primary nav name
	 * @uses  buddymemberstats_get_component_options_subnav_slug() to get the options nav slug
	 */
	public function setup_nav( $main_nav = array(), $sub_nav = array() ) {
		
		$main_nav = array(
			'name'                => _x( 'Member Stats', 'Main nav item', 'buddymemberstats' ),
			'slug'                => $this->slug,
			'position'            => 90,
			'screen_function'     => 'buddymemberstats_screen_main_nav',
			'default_subnav_slug' => buddymemberstats_get_component_primary_subnav_slug(),
			'item_css_id'         => $this->id
		);

		// Determine user to use
		if ( bp_displayed_user_domain() ) {
			$user_domain = bp_displayed_user_domain();
		} elseif ( bp_loggedin_user_domain() ) {
			$user_domain = bp_loggedin_user_domain();
		} else {
			return;
		}

		$buddymemberstats_link = trailingslashit( $user_domain . $this->slug );

		// Add the subnav item
		$sub_nav[] = array(
			'name'            => buddymemberstats_get_component_primary_subnav_name(),
			'slug'            => buddymemberstats_get_component_primary_subnav_slug(),
			'parent_url'      => $buddymemberstats_link,
			'parent_slug'     => $this->slug,
			'screen_function' => 'buddymemberstats_screen_main_nav',
			'position'        => 10,
			'item_css_id'     => buddymemberstats_get_component_primary_subnav_slug()
		);

		parent::setup_nav( $main_nav );
	}
	
	/**
	 * Builds the BuddyPress loggedin user nav in WP Admin Bar
	 * 
	 * @package BuddyMemberStats
	 * @subpackage Component
	 * @since 1.0
	 * 
	 * @uses  bp_core_new_subnav_item() to build a new subnav item
	 * @uses  buddymemberstats_get_component_user_settings_nav_name() to get the BuddyMemberStats settings nav name
	 * @uses  bp_get_settings_slug() to get the settings slug
	 * @uses  bp_is_my_profile() to be sure it shows to loggedin user only
	 */
	public function setup_settings_bp_nav() {
	
		bp_core_new_subnav_item( array(
			'name' 		      => buddymemberstats_get_component_user_settings_nav_name(),		
			'slug' 		      => $this->slug,
			'parent_slug'     => bp_get_settings_slug(),
			'parent_url' 	  => trailingslashit( bp_loggedin_user_domain() . bp_get_settings_slug() ),
			'screen_function' => 'buddymemberstats_screen_user_settings',
			'position' 	      => 90,
			'user_has_access' => bp_is_my_profile() // Only the logged in user can access this on his/her profile
		) );
	}

	/**
	 * Builds a sub menu for the settings in My Profile on WP Admin Bar
	 * 
	 * @package BuddyMemberStats
	 * @subpackage Component
	 * @since 1.0
	 * 
	 * @global $wp_admin_bar object
	 * @uses  buddymemberstats_get_component_user_settings_nav_name() to get the custom nav name
	 * @uses  bp_loggedin_user_domain() to get the loggedin user profil url
	 * @uses  bp_get_settings_slug() to get the settings slug
	 */
	public function setup_settings_wp_nav() {
		global $wp_admin_bar;

		// Prevent debug notices
		$wp_admin_nav = array();

		// Menus for logged in user
		if ( is_user_logged_in() ) { 

		$wp_admin_bar->add_menu( array(
		'parent' => 'my-account-xprofile',
		'id'     => 'my-account-xprofile-stat',
		'title' => buddymemberstats_get_component_primary_subnav_name(),
		'href'   => trailingslashit( bp_loggedin_user_domain(). '/' . $this->slug ),
			'meta'   => array(
			'class'  => 'ab-item'
			),
		) );

 		$settings_menu = array(
			'parent' => 'my-account-settings',
			'id'     => 'my-account-settings-' . $this->slug,
			'title'  => buddymemberstats_get_component_user_settings_nav_name(),
			'href'   => trailingslashit( bp_loggedin_user_domain() . bp_get_settings_slug() . '/' . $this->slug ),
		);			
		$wp_admin_bar->add_menu( $settings_menu );
		}
	} 
}

/**
 * Loading component into the main BuddyPress instance
 *
 * @uses buddypress()
 */
function buddymemberstats_component() {
	buddypress()->{buddymemberstats_get_component_id()} = new Buddy_Member_Stats_Component;
}
add_action( 'bp_loaded', 'buddymemberstats_component', 11 );

endif;