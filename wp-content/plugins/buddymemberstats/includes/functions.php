<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Returns plugin version
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_plugin_version() {
	return buddymemberstats()->version;
}

/**
 * Returns plugin's dir
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_plugin_dir() {
	return buddymemberstats()->plugin_dir;
}

/**
 * Returns plugin's includes dir
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_includes_dir() {
	return buddymemberstats()->includes_dir;
}

/**
 * Returns plugin's includes url
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_includes_url() {
	return buddymemberstats()->includes_url;
}

/**
 * Returns plugin's js url
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_js_url() {
	return buddymemberstats()->plugin_js;
}

/**
 * Returns plugin's js url
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_css_url() {
	return buddymemberstats()->plugin_css;
}

/**
 * Returns plugin's component includes dir
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_component_includes_dir() {
	return buddymemberstats()->component_includes_dir;
}

/**
 * Returns plugin's component id
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_component_id() {
	return buddymemberstats()->component_id;
}

/**
 * Returns plugin's component slug
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddypress() BuddyPress main instance
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_component_slug() {
	$slug = !empty( buddypress()->{buddymemberstats_get_component_id()}->slug ) ? buddypress()->{buddymemberstats_get_component_id()}->slug : buddymemberstats()->component_slug;
	return $slug;
}

/**
 * Returns plugin's component primary nav slug
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_component_primary_subnav_slug() {
	return buddymemberstats()->component_primary_subnav_slug;
}


/**
 * Returns plugin's component name
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_component_name() {
	return buddymemberstats()->component_name;
}


/**
 * Displays nav name defined by site admin
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats_get_component_user_settings_nav_name() to get it
 */
function buddymemberstats_component_user_settings_nav_name() {
	echo buddymemberstats_get_component_user_settings_nav_name();
}

/**
 * Returns nav name defined by site admin
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_component_user_settings_nav_name() {
	return sanitize_text_field( bp_get_option( 'buddymemberstats_user_settings_nav_name', buddymemberstats()->component_user_settings_nav_name ) );
}

/**
 * Displays plugin's primary nav name
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats_get_component_primary_subnav_name() to get it
 */
function buddymemberstats_component_primary_subnav_name() {
	echo buddymemberstats_get_component_primary_subnav_name();
}

/**
 * Returns plugin's primary nav name
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats() plugin's main instance
 */
function buddymemberstats_get_component_primary_subnav_name() {
	return sanitize_text_field( bp_get_option( 'buddymemberstats_primary_subnav_name', buddymemberstats()->component_primary_subnav_name ) );
}

/**
 * Checks if a particular user has a role. 
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * Returns true if a match was found.
 *
 * @param string $role Role name.
 * @param int $user_id The ID of a user. Defaults to the current user.
 * @return bool
 */
function bms_check_user_role( $role, $user_id = null ) {
global $bp;
$user_id = bp_displayed_user_id(); 
 
    if ( is_numeric( $user_id ) )
		$user = get_userdata( $user_id );
    else
        $user = wp_get_current_user();
 
    if ( empty( $user ) )
	return false;
 
    return in_array( $role, (array) $user->roles );
}

/**
 * User's options handler
 * 
 * @use WP usermeta table
 * @package BuddyMemberStats
 * @since 1.0
 * 
 */
function bms_front_options() {
global $bp;

$current_user = wp_get_current_user();
$user_id = $current_user->ID;

	// privacy option
	if ( isset( $_POST['bms_submit'] ) && isset( $_POST['bms_stat_privacy'] ) ) {

	update_user_meta( $user_id, 'bms_stat_privacy',  $_POST['bms_stat_privacy']  );
	}	

	// show/hide option for personnal section
	if ( isset( $_POST['bms_submit'] ) && isset( $_POST['bms_show_personnal_section'] ) ) {

		$checked = ( $_POST['bms_show_personnal_section'] !== 'no' )? 'yes' : 'no';	

	update_user_meta( $user_id, 'bms_show_personnal_section', $checked );
	}

	// show/hide option for community section
	if ( isset( $_POST['bms_submit'] ) && isset( $_POST['bms_show_community_section'] ) ) {
		
		$checked = ( $_POST['bms_show_community_section'] == 'yes' )? 'yes': 'no';	
		
	update_user_meta( $user_id, 'bms_show_community_section', $checked );
	}

	// show/hide option for blogging section
	if ( isset( $_POST['bms_submit'] ) && isset( $_POST['bms_show_blogging_section'] ) ) {
		
		$checked = ( $_POST['bms_show_blogging_section'] == 'yes' )? 'yes' : 'no';	
		
	update_user_meta( $user_id, 'bms_show_blogging_section', $checked );
	}

	// show/hide option for group section
	if ( bp_is_active( 'groups' ) ) {
		if ( isset( $_POST['bms_submit'] ) && isset( $_POST['bms_show_group_section'] ) ) {

			$checked = ( $_POST['bms_show_group_section'] == 'yes' )? 'yes' : 'no';	

		update_user_meta( $user_id, 'bms_show_group_section', $checked );
		}
	}

	// show/hide option for forum section
	if( class_exists ( 'bbPress' ) ) {
		if ( isset( $_POST['bms_submit'] ) && isset( $_POST['bms_show_forum_section'] ) ) {

			$checked = ( $_POST['bms_show_forum_section'] == 'yes' )? 'yes' : 'no';	

		update_user_meta( $user_id, 'bms_show_forum_section', $checked );
		}
	}
}

/**
 * Get stats
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
*/

function bms_get_total_notifications_count(){
global $wpdb, $bp;
$user_id = bp_displayed_user_id(); 

	if ( bp_is_active( 'notifications' ) ) :

	$total_notifications = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(user_id) FROM {$wpdb->prefix}bp_notifications 
			WHERE user_id = '%s' ", $user_id ) ); 

	echo $total_notifications;

	endif;
}

function bms_get_total_mention_count(){
global $wpdb, $bp;
$user_id = bp_displayed_user_id(); 

	if ( bp_is_active( 'notifications' ) ) :

	// DB query 
	$total_atmention = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}bp_notifications 
			WHERE user_id = '%s' 
			AND component_action = 'new_at_mention' ", $user_id ) );
									  
	echo $total_atmention;

	endif;
}

function bms_get_total_group_updates_count(){
global $wpdb, $bp;
$user_id = bp_displayed_user_id(); 

	if ( bp_is_active( 'groups' ) ) :

	// DB query 
	$total_updates = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}bp_activity 
			WHERE component = 'groups' 
			AND   type = 'activity_update'
			AND   user_id = '%s' ", $user_id ) );
									  
	echo $total_updates;

	endif;
}

function bms_get_total_starred_message_count(){
global $wpdb, $bp;
$user_id = bp_displayed_user_id(); 

	if ( bp_is_active( 'messages' ) ) :

	// DB query 
	$total_stars = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}bp_messages_meta 
			WHERE meta_key = 'starred_by_user' 
			AND   meta_value = '%s' ", $user_id) );
									  
	echo $total_stars;

	endif;
}

function bms_get_received_messages_count() {
global $wpdb, $bp;
$user_id = bp_displayed_user_id(); 

if ( bp_is_active( 'messages' ) ) :

	// DB query 
	$total_msg = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}bp_messages_recipients
			WHERE user_id = '%s' ", $user_id ) );
									  
	echo $total_msg;

	endif;
}

function bms_get_total_post_count() {
global $wpdb, $bp;
$user_id = bp_displayed_user_id(); 

   if ( bms_check_user_role( 'subscriber' ) ) { 
		// we do nothing
	} else {   

		// DB query
		$total_post = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}bp_activity 
			WHERE user_id = '%s'
			AND   component = 'blogs'
			AND   type = 'new_blog_post'
			", $user_id ) );
										  
		echo $total_post;
	}
}

function bms_get_total_comments_count() {
global $wpdb, $bp;
$user_id = bp_displayed_user_id(); 

	// are we on a single install ?
	if ( !is_multisite() ) {

		$args = array(
			'user_id'	=> $user_id, // we use user_id and...
			'count'		=> true 	 // return only the count
		);
	$my_comments = get_comments($args);
    return $my_comments;

	} else { 
	 
	/* *
	 * hey, it's a network !
	 * DB query
    */
		$total_com = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}bp_activity 
			WHERE component = 'bp_plugin' 
			AND   type = 'new_blog_comment'
			AND   user_id = '' 
			", $user_id ) );										  
	    echo $total_com;
	} 
}

function bms_get_attachment_stat() {
global $bp, $wpdb;
$user_id = bp_displayed_user_id();

	// Check if plugin is loaded
	if ( class_exists( 'BP_Attachment' ) ) :

	// DB query
	$total_bp_attachment = $wpdb->get_var( $wpdb->prepare(" SELECT COUNT(post_id) 
		AS at_count
		FROM	{$wpdb->prefix}posts AS posts LEFT JOIN {$wpdb->prefix}postmeta AS meta
		ON		posts.ID = meta.post_id
		WHERE	posts.post_author = '%s'
		AND		posts.post_type = 'bp_attachment'
		AND		meta.meta_key = '_wp_attachment_context'
		AND     meta.meta_value = 'buddypress'
	", $user_id ) );
	echo $total_bp_attachment;
	endif;
}

/**
 * Average of user's activities 
 *
 * @use bp_activity
 * Get site activities vs. user activities
 * to calculate  a percentage ratio
*/

function bms_get_total_site_activity(){
global $wpdb, $bp;
$item_id = null;

	if ( bp_is_active( 'activity' ) ) :

	// DB query 
	$total_site_activities = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(id) FROM {$wpdb->prefix}bp_activity 
			WHERE item_id = '%s' ", $item_id ) );
							  
	return $total_site_activities;

	endif;
}

function bms_get_total_user_activity(){
global $wpdb, $bp;
$user_id = bp_displayed_user_id(); 

	if ( bp_is_active( 'activity' ) ) :

	// DB query 
	$total_user_activities = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(id) FROM {$wpdb->prefix}bp_activity 
			WHERE user_id = '%s' ", $user_id ) );
									  
	return $total_user_activities;
	
	endif;
}

	function bms_get_ratio() {
		if ( bp_is_active( 'activity' ) ) :
			if ( function_exists( 'bms_get_total_user_activity' ) && function_exists( 'bms_get_total_site_activity' ) ) {
				$a = bms_get_total_site_activity(); // site
				$b = bms_get_total_user_activity();	// user			
				$p = 100;			// percentage                           
				$r = ($a/$b) * $p;	// result
					return round($r); 
			}
		endif;
	}

/**
 * Show user's register date
*/

function bms_get_member_since() {
global $bp;

	$user_id = bp_displayed_user_id(); 
	$bms_date = get_userdata( $user_id );
	$registered = $bms_date->user_registered;
	$joined =  date_i18n( "j F Y", strtotime( $registered ) );
	
return  $joined;
}

/**
 * Show user's role
*/

function bms_get_roles() {
global $bp, $wp;

	$user = new WP_User( bp_displayed_user_id() );

	if ( class_exists( 'bbPress' ) ) {
		$abc_role = bbp_get_user_display_role( bp_displayed_user_id() );
	}

	if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
		foreach ( $user->roles as $role ) {

			if ($role == 'bbp_keymaster' || $role == 'bbp_moderator' || $role == 'bbp_participant') {
			// is bbPress active ?
				if ( class_exists( 'bbPress' ) ) :
					echo '<li>'. _x( 'Forum:', 'stat list title', 'buddymemberstats' ) .'&nbsp; '. esc_html( ucfirst ( $abc_role ) ) .'</li>';	
				endif;
			}
			// default WP roles
			elseif ($role == 'administrator') {
				echo '<li>'. _x( 'Site:', 'stat list title', 'buddymemberstats' ) .'&nbsp;'. _x( 'Site admin', 'role on list', 'buddymemberstats' ) .'</li>';
			}
			elseif ($role == 'editor') {
				echo '<li>'. _x( 'Site:', 'stat list title', 'buddymemberstats' ) .'&nbsp;'. _x( 'Editor', 'role on list', 'buddymemberstats' ) .'</li>';
			}
			elseif ($role == 'author') {
				echo '<li>'. _x( 'Site:', 'stat list title', 'buddymemberstats' ) .'&nbsp;'. _x( 'Author', 'role on list', 'stat list role', 'buddymemberstats' ) .'</li>';
			}
			elseif ($role == 'contributor') {
				echo '<li>'. _x( 'Site:', 'stat list title', 'buddymemberstats' ) .'&nbsp;'. _x( 'Contributor', 'role on list', 'buddymemberstats' ) .'</li>';
			} 
			else {
				echo '<li>'. _x( 'Site:', 'stat list title', 'buddymemberstats' ) .'&nbsp;'. _x( 'Member', 'role on list', 'buddymemberstats' ) .'</li>';
			}
		}
	}
} 

/**
 * Stats from third parties 
 * @use BuddyPress Compliments
 * @use BuddyDrive
 * @use Buddy Views
 * BuddyPress Followers
*/

function bms_get_compliment_stats() {
global $bp;
$user_id = bp_displayed_user_id();

if ( function_exists( 'bp_compliments_total_counts' ) ) {
	$counts  = bp_compliments_total_counts( array( 'user_id' => $user_id ) );
	$bms_comp_count = $counts['received'];
		return $bms_comp_count;
	}
}

function bms_get_buddydrive_stat() {
global $bp;
$user_id = bp_displayed_user_id();
	if ( function_exists( 'count_user_posts' ) ) {
		return count_user_posts( $user_id, "buddydrive-file" );
	}
}

function bms_get_bp_followers() {
global $bp;
$user_id = bp_displayed_user_id();

if ( function_exists( 'bp_follow_total_follow_counts' ) ) :
    $count = bp_follow_total_follow_counts( array(
        
        'user_id' => $user_id
    ) );

    echo $count['followers'];

endif;
}

function bms_get_bp_following() {
global $bp;
$user_id = bp_displayed_user_id();

if ( function_exists( 'bp_follow_total_follow_counts' ) ) :
    $count = bp_follow_total_follow_counts( array(
        
        'user_id' => $user_id
    ) );

   echo $count['following'];

endif;
}

function bms_get_buddydrive_stat_dl() {
global $bp, $wpdb;
$user_id = bp_displayed_user_id();

	// Check if plugin is loaded
	if ( class_exists( 'buddydrive' ) ) 

	// DB query
	$total_drive_dl = $wpdb->get_var( $wpdb->prepare("SELECT SUM(meta.meta_value) 
		AS dl_count
		FROM	{$wpdb->prefix}posts AS posts LEFT JOIN {$wpdb->prefix}postmeta AS meta
		ON		posts.ID = meta.post_id
		WHERE	posts.post_author = '%s'
		AND		posts.post_type = 'buddydrive-file'
		AND		meta.meta_key = '_buddydrive_downloads'
	", $user_id ) );
	return $total_drive_dl;
}

function bms_buddy_views_count() {
global $bp, $bv_views_log_model;
$user_id = bp_displayed_user_id();

	if ( class_exists( 'Buddy_Views' ) ) {
		$where = array(
			'member_id' => $user_id,
		);

		$views = $bv_views_log_model->get( $where );

			if ( ! empty( $views ) ) {
				$views_count = count( $views );
			echo $views_count;
			}
		}
}

function bms_back_button() {

	if ( is_user_logged_in() ) {
		$url = bp_get_loggedin_user_link() .'buddymemberstats/';
		$link = sprintf( wp_kses( __( '<a href="%s">Back to my stats</a>', 'buddymemberstats' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $url ) );
	
	}?>
	<span class="bms_back_button"><?php echo $link; ?></span>
<?php }

/**
 * let's i18n radio button options only once !
*/
function bms_yes_box() {
		_e( 'Yes', 'buddymemberstats' );   
}

function bms_no_box() {
		_e( 'No', 'buddymemberstats' );   
}