<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Main settings section
 *
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since 1.0
 *
 * @return array
 */
function buddymemberstats_admin_get_settings_sections() {
	return (array) apply_filters( 'buddymemberstats_admin_get_settings_sections', array(
		'buddymemberstats_main' => array(
			'title'    => __( 'Main Settings', 'buddymemberstats' ),
			'callback' => 'buddymemberstats_admin_setting_callback_main',
			'page'     => 'buddymemberstats',
		)
	) );
}

/**
 * The different fields for the main settings
 * 
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since 1.0
 *
 * By default, WP's dashicons are only available in the backend. 
 * However, as the dashicons can be loaded by simply enqueueing 
 * the dashicons stylesheet, we do that here.
 *
 * @uses bp_is_active() to check if the settings component is active
 * @return array
 */
function buddymemberstats_admin_get_settings_fields() {
	$settings_fields = array(

		/** Main Section ******************************************************/

		'buddymemberstats_main' => array(
			// Primary subnav name
			'buddymemberstats_primary_subnav_name' => array(
				'title'             => _x( 'Stats view menu', 'Admin setup', 'buddymemberstats' ),
				'callback'          => 'buddymemberstats_setting_callback_names',
				'sanitize_callback' => 'buddymemberstats_sanitize_custom_names',
				'args'              => array( 
					'option' => 'buddymemberstats_primary_subnav_name', 
					'default' => buddymemberstats()->component_primary_subnav_name
				)
			),
		)
	);

	if( bp_is_active( 'settings' ) ) {
		$settings_fields[ 'buddymemberstats_main' ]['buddymemberstats_user_settings_nav_name'] = array(
			'title'             => _x( 'Stats options menu', 'Admin setup', 'buddymemberstats' ),
			'callback'          => 'buddymemberstats_setting_callback_names',
			'sanitize_callback' => 'buddymemberstats_sanitize_custom_names',
			'args'              => array( 
				'option' => 'buddymemberstats_user_settings_nav_name', 
				'default' => buddymemberstats()->component_user_settings_nav_name
			)
		);
	}

	return (array) apply_filters( 'buddydrive_admin_get_settings_fields', $settings_fields );

}


/**
 * Gives the setting fields for section
 *
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since 1.0
 * 
 * @param  string $section_id 
 * @uses  buddymemberstats_admin_get_settings_fields() to get the setting fields
 * @return array  the fields
 */
function buddymemberstats_admin_get_settings_fields_for_section( $section_id = '' ) {

	// Bail if section is empty
	if ( empty( $section_id ) )
		return false;

	$fields = buddymemberstats_admin_get_settings_fields();
	$retval = isset( $fields[$section_id] ) ? $fields[$section_id] : false;

	return (array) apply_filters( 'buddymemberstats_admin_get_settings_fields_for_section', $retval, $section_id );
}

/**
 * No text for the settings section
 *
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since 1.0
 * 
 */
function buddymemberstats_admin_setting_callback_main() { ?>
	<div id="bms-help-intro">
		<?php _e( 'Click the help button in the right corner for additionnal informations.', 'buddymemberstats' ); ?>
	</div>
<?php
}


/**
 * Let the admin customize the name of the main user's subnav
 *
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since 1.0
 *
 * @param $args array
 * @uses bp_get_option() to get the user's subnav
 * @uses sanitize_title() to sanitize user's subnav name
 * @return string html
 */
function buddymemberstats_setting_callback_names( $args = array() ) {
	$name = bp_get_option( $args['option'], $args['default'] );
	$name = sanitize_text_field( $name );
	?>

	<input name="<?php echo esc_attr( $args['option'] );?>" type="text" id="<?php echo esc_attr( $args['option'] );?>" value="<?php echo $name;?>" class="regular-text code" />

	<?php
}

/**
 * Sanitizes the names
 *
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since 1.0
 *
 * @param string $option 
 * @uses sanitize_text_field() to sanitize the name
 * @return string the slug
 */
function buddymemberstats_sanitize_custom_names( $option ) {
	$option = sanitize_text_field( $option );
	
	return $option;
}

/**
 * Displays the settings page
 *
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since 1.0
 * 
 * @uses bp_core_do_network_admin() to check if BuddyPress is activated on the network
 * @uses add_query_arg() to add arguments to query in case of multisite
 * @uses bp_get_admin_url to build the settings url in case of multisite
 * @uses screen_icon() to show options icon
 * @uses settings_fields()
 * @uses do_settings_sections()
 * @uses wp_nonce_field() for security reason in case of multisite
 */
function buddymemberstats_admin_settings() {
	$form_action = 'options.php';
	
	if( bp_core_do_network_admin() ) {
		do_action( 'buddymemberstats_network_options' );
		
		$form_action = add_query_arg( 'page', 'buddymemberstats', bp_get_admin_url( 'settings.php' ) );
	}
?>

	<div class="wrap">

		<h2><?php _e( 'Buddy Member Stats Settings', 'buddymemberstats' ) ?></h2>

		<form action="<?php echo $form_action;?>" method="post" id="buddymemberstats-form-settings">

			<?php settings_fields( 'buddymemberstats' ); ?>

			<?php do_settings_sections( 'buddymemberstats' ); ?>

			<p class="submit">
				<?php if( bp_core_do_network_admin() ) :?>
					<?php wp_nonce_field( 'buddymemberstats_settings', '_wpnonce_buddymemberstats_setting' ); ?>
				<?php endif;?>
				<input type="submit" name="submit" class="button-primary" value="<?php _e( 'Save Changes', 'buddymemberstats' ); ?>" />
			</p>
		</form>
	</div>

<?php
}

/**
 * Add backend stylesheet
 *
 * @package Buddy Member Stats
 * @since version 1.0
*/

function bms_enqueue_admin_css() {
   wp_enqueue_style( 'bms-style-admin', plugin_dir_url( __FILE__ ) . 'css/buddymemberstats-admin.css' );
}
add_action( 'admin_enqueue_scripts', 'bms_enqueue_admin_css' );


/**
 * Save settings in case of a multisite config
 *
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since 1.0
 *
 * @uses check_admin_referer() to check the nonce
 * @uses buddymemberstats_sanitize_custom_names() to sanitize names
 * @uses bp_update_option() to save the options in root blog
 */
function buddymemberstats_handle_network_settings() {
	
	if ( 'POST' !== strtoupper( $_SERVER['REQUEST_METHOD'] ) )
		return;
	
	check_admin_referer( 'buddymemberstats_settings', '_wpnonce_buddymemberstats_setting' );

	foreach( $_POST as $key => $val ) {
		if( strpos( $key, 'buddymemberstats' ) === 0 ) {
			$name = buddymemberstats_sanitize_custom_names( $val );

			if( ! empty( $name ) )
				bp_update_option( $key, $name );

		}
			
	}
	?>
	<div id="message" class="updated"><p><?php _e( 'Settings saved', 'buddymemberstats' );?></p></div>
	<?php
	
}

add_action( 'buddymemberstats_network_options', 'buddymemberstats_handle_network_settings', 0 );


/**
 * add a help tab on settings page
*/
function bms_admin_help_tab( $contextual_help, $screen_id, $screen ) {
global $settings_page_buddymemberstats, $bms_reminder;

if ( ! method_exists( $screen, 'add_help_tab' ) )
        return $contextual_help;	

	if ($screen_id == 'settings_page_buddymemberstats-network' || $screen_id == 'settings_page_buddymemberstats' ) {

		// display help panel
		$screen->add_help_tab( array(
			'id'      => 'bms-help',
			'title'	=> _x( 'Member Stats Help', 'Help tab title', 'buddymemberstats' ),
			'function' => $bms_reminder,
			'content' => '<p><strong>' . _x( 'User navigation - item names', 'Help tab title', 'buddymemberstats' ) . '</strong></p>' .
						 '<p>' .
							'<ul>' .
								'<li>' . _x( 'Stats view menu: the nav item name to view the stats.', 'Help tab instruction', 'buddymemberstats' ) . '</li>' .
								'<li>' . _x( 'Stats options menu: the nav item name to select stats options.', 'Help tab instruction',	'buddymemberstats' ) . '</li>' .						
							'</ul>' .
						'</p>' .
						'<p>' . _x( 'You must click the Save Changes button at the bottom of the screen for new settings to take effect.', 'Help tab instruction', 'buddymemberstats' ) . '</p>'
		
		) );

		return $contextual_help;
	}
}
add_action( 'contextual_help', 'bms_admin_help_tab', 10, 3 );