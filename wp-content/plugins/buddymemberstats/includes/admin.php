<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'BuddyMemberStats_Admin' ) ) :
/**
 * Loads BuddyMemberStats plugin admin area
 * 
 * @package BuddyMemberStats
 * @subpackage Admin
 * @since version 1.0.0
 */
class BuddyMemberStats_Admin {
	
	/**
	 * @var the BuddyMemberStats settings page for admin or network admin
	 */
	public $settings_page ='';
	
	/**
	 * @var the notice hook depending on config (multisite or not)
	 */
	public $notice_hook = '';

	/**
	 * @var the BuddyMemberStats hook_suffixes to eventually load script
	 */
	public $hook_suffixes = array();

	/**
	 * The constructor
	 *
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 */
	public function __construct() {
		$this->setup_globals();
		$this->includes();
		$this->setup_actions();
	}

	/**
	 * Admin globals
	 *
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 *
	 * @uses bp_core_do_network_admin() to define the best menu (network or not)
	 */
	private function setup_globals() {
		$this->settings_page = bp_core_do_network_admin() ? 'settings.php' : 'options-general.php';
		$this->about_url	 = bp_core_do_network_admin() ? 'index.php' : 'options-general.php';
		$this->notice_hook   = bp_core_do_network_admin() ? 'network_admin_notices' : 'admin_notices' ;
		$this->includes_dir  = trailingslashit( buddymemberstats_get_includes_dir() . 'admin-includes' );

	}

	/**
	 * Includes the needed files
	 *
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 */
	public function includes() {
		require( $this->includes_dir . 'settings.php' );
	}

	/**
	 * Setup the admin hooks, actions and filters
	 *
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 * 
	 * @uses bp_core_admin_hook() to hook the right menu (network or not)
	 * @uses buddymemberstats() to get plugin's main instance
	 *
	 * add settings & about link on plugin directory
	 */
	private function setup_actions() {
		$buddymemberstats = buddymemberstats();

		// Current blog is not the one where BuddyPress is activated so let's warn the administrator
		if( ! $buddymemberstats::buddypress_site_check() ) {
			add_action( 'admin_notices',              array( $this, 'warning_notice' ) );
		} else {
			add_action( $this->notice_hook,           array( $this, 'activation_notice' ) );
			add_action( bp_core_admin_hook(),         array( $this, 'admin_menus'       ) );
			add_action( 'bp_admin_enqueue_scripts',   array( $this, 'enqueue_scripts'   ), 10, 1 );
			add_action( 'bp_register_admin_settings', array( $this, 'register_settings' ) );
		}	
		// Modify BuddyDrive's admin links
		add_filter( 'plugin_action_links',               array( $this, 'bms_modify_plugin_action_links' ), 10, 2 );
		add_filter( 'network_admin_plugin_action_links', array( $this, 'bms_modify_plugin_action_links' ), 10, 2 );	
	}

	/**
	 * Prints a warning notice if the Community administrator activated the plugin on the wrong site
	 *
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 * 
	 * @uses is_plugin_active_for_network() to check if the plugin is activated on the network
	 * @uses buddymemberstats() to get plugin's main instance
	 * @uses bp_core_do_network_admin() to check if BuddyPress has been activated on the network
	 */
	public function warning_notice() {
		if( is_plugin_active_for_network( buddymemberstats()->basename ) )
			return;
		?>
		<div id="message" class="updated fade">
			<?php if( bp_core_do_network_admin() ) :?>
				<p><?php _e( 'BuddyPress is activated on the network, please deactivate Buddy Member Stats from this site and make sure to activate Buddy Member Stats on the network.', 'buddymemberstats' );?></p>
			<?php else:?>
				<p><?php _e( 'Buddy Member Stats has been activated on a site where BuddyPress is not, please deactivate BuddyMemberStats from this site and activate it on the same site where BuddyPress is activated.', 'buddymemberstats' );?></p>
			<?php endif;?>
		</div>
		<?php
	}

	/**
	 * Displays a warning if BuddyPress version is outdated for the plugin
	 * 
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 *
	 * @uses  buddymemberstats() to get plugin's main instance
	 */
	public function activation_notice() {
		$buddymemberstats = buddymemberstats();

		if( ! $buddymemberstats::buddypress_version_check() ) {
			?>
			<div id="message" class="updated fade">
				<p><?php printf( __( 'Buddy Member Stats requires at least <strong>BuddyPress %s</strong>, please upgrade.', 'buddymemberstats' ), $buddymemberstats::$init_vars['bp_version_required'] );?></p>
			</div>
			<?php
		}
	}
	
	/**
	 * Builds BuddyMemberStats admin menus
	 * 
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 *
	 * @uses  buddymemberstats() to get plugin's main instance
	 * @uses  bp_current_user_can() to check for user's capability
	 * @uses  add_submenu_page() to add the settings page
	 * @uses  buddymemberstats_get_plugin_version() to get plugin's version
	 * @uses  bp_get_option() to get plugin's db version
	 * @uses  bp_update_option() to update plugin's db version
	 */
	public function admin_menus() {
		$buddymemberstats = buddymemberstats();

		// Bail if user cannot manage options
		if ( ! bp_current_user_can( 'manage_options' ) )
			return;

			$this->hook_suffixes[] = add_submenu_page(
			$this->settings_page,
			_x( 'Buddy Member Stats', 'Admin settings item',  'buddymemberstats' ),
			_x( 'Buddy Member Stats', 'Admin settings item',  'buddymemberstats' ),
			'manage_options',
			'buddymemberstats',
			'buddymemberstats_admin_settings',
			30
		);

		// About page
		$this->hook_suffixes[] = add_dashboard_page(
			__( 'Welcome to Buddy Member Stats',  'buddymemberstats' ),
			__( 'Welcome to Buddy Member Stats',  'buddymemberstats' ),
			'manage_options',
			'buddymemberstats-about',
			array( $this, 'about_screen' )
		);

		if( $buddymemberstats::buddypress_version_check() && buddymemberstats_get_plugin_version() != bp_get_option( 'buddymemberstats_db_version' ) )
			bp_update_option( 'buddymemberstats_db_version', buddymemberstats_get_plugin_version() ); 
	}

	/**
	 * Enqueues scripts and styles if needed
	 * 
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 * 
	 * @param  string $hook the WordPress admin page
	 * @uses wp_enqueue_script() to enqueue the script
	 * @uses buddymemberstats_get_js_url() to get the js url for the plugin
	 * @uses buddymemberstats_get_plugin_version() to get plugin's version
	 */
	public function enqueue_scripts( $hook = '' ) {

		if( empty( $hook ) )
			$hook = bp_core_do_network_admin() ? str_replace( '-network', '', get_current_screen()->id ) : get_current_screen()->id;

		if( in_array( $hook, $this->hook_suffixes ) ) {
			wp_enqueue_script	( 'buddymemberstats-admin-js', buddymemberstats_get_js_url() .'admin.js', array( 'jquery' ), buddymemberstats_get_plugin_version(), 1 );
			wp_localize_script	( 'buddymemberstats-admin-js', 'buddymemberstats_admin', array( 'message' => _x( 'Please fill the empty field:', 'Settings error', 'buddymemberstats' ) ) );
		}
			
	}

	/**
	 * Builds the settings fields for the plugin
	 *
	 * @package BuddyMemberStats
	 * @subpackage Admin
	 * @since 1.0
	 * 
	 * @uses buddymemberstats_admin_get_settings_sections() to get plugin's settings sections
	 * @uses bp_current_user_can() to check for user's capacity
	 * @uses buddymemberstats_admin_get_settings_fields_for_section() to get plugin's fields for the section
	 * @uses add_settings_section() to add the settings section
	 * @uses add_settings_field() to add the fields
	 * @uses register_setting() to fianlly register the settings
	 */
	public function register_settings() {
		$sections = buddymemberstats_admin_get_settings_sections();

		// Bail if no sections available
		if ( empty( $sections ) )
			return false;

		// Loop through sections
		foreach ( (array) $sections as $section_id => $section ) {

			// Only proceed if current user can see this section
			if ( ! bp_current_user_can( 'manage_options' ) )
				continue;

			// Only add section and fields if section has fields
			$fields = buddymemberstats_admin_get_settings_fields_for_section( $section_id );
			if ( empty( $fields ) )
				continue;

			// Add the section
			add_settings_section( $section_id, $section['title'], $section['callback'], $section['page'] );

			// Loop through fields for this section
			foreach ( (array) $fields as $field_id => $field ) {

				// Add the field
				add_settings_field( $field_id, $field['title'], $field['callback'], $section['page'], $section_id, $field['args'] );

				// Register the setting
				register_setting( $section['page'], $field_id, $field['sanitize_callback'] );
			}
		}
	}


	public function bms_modify_plugin_action_links( $links, $file ) {

		// Return normal links if not BuddyPress
		if ( plugin_basename( buddymemberstats()->file ) != $file )
			return $links;

		// Add a few links to the existing links array
		return array_merge( $links, array(
			'settings' => '<a href="' . esc_url( add_query_arg( array( 'page' => 'buddymemberstats'       ), bp_get_admin_url( $this->settings_page ) ) ) . '">' . esc_html__( 'Settings', 'buddymemberstats' ) . '</a>',
			'about'    => '<a href="' . esc_url( add_query_arg( array( 'page' => 'buddymemberstats-about' ), bp_get_admin_url( 'index.php'          ) ) ) . '">' . esc_html__( 'About',    'buddymemberstats' ) . '</a>'
		) ); 
	}

	 /**
	 * Displays the Welcome screen
	 *
	 * @uses buddydrive_get_version() to get the current version of the plugin
	 * @uses bp_get_admin_url() to build the url to settings page
	 * @uses add_query_arg() to add args to the url
	 */
	public function about_screen() {
		global $wp_version;
		$display_version = buddymemberstats_get_plugin_version();
		$settings_url = add_query_arg( array( 'page' => 'buddymemberstats'), bp_get_admin_url( $this->settings_page ) ); ?>

	<div class="wrap about-wrap">
			
    <h1><?php _ex( 'Welcome to Buddy Member Stats', 'Welcome screen title', 'buddymemberstats' ); ?></h1>
	<div class="about-text"><?php _ex( 'Thank you for using Buddy Member Stats', 'Welcome screen sub title', 'buddymemberstats' ); ?>&nbsp;<?php echo $display_version; ?></div>
	<div class="badge"><?php echo $display_version; ?></div>

	<h2 class="nav-tab-wrapper">
	<?php echo '<a class="nav-tab nav-tab-active" href="'. bp_get_admin_url( 'index.php?page=buddymemberstats-about' ) .' ">' . esc_html_x( 'Presentation', 'Welcome screen', 'buddymemberstats' ) . '</a>'; ?>		
	</h2>

	<div class="changelog">		
		<h4><?php _ex( 'Buddy Member Stats at a glance', 'Welcome screen', 'buddymemberstats' ); ?></h4>
			<p><?php _ex( 'Buddy Member Stats is the ideal tool to see at a glance the involvement and level of contribution of each member of your site. By consolidating on each profile the many figures released by BuddyPress or other plugins, Buddy Member Stats offers administrators and members a powerfull performance board. Unlike BuddyPress who informs you, in exemple, that you received 2 new messages, Buddy Member Stats indicates the amount of all your messages, regardless of their status. It is the same for all other counts of activities identified by Buddy Member Stats. In doing so, Buddy Member Stats becomes the ideal results panel of your community.', 'Welcome screen', 'buddymemberstats' ); ?></p>
			
		<div class="featured-image">
			<img src="<?php echo esc_url( buddymemberstats_get_images_url() . '/budddymemberstats-view.png' );?>" alt="<?php esc_attr_x( 'The Buddy Member Stats view', 'Welcome screen img alt', 'buddymemberstats' ); ?>">
		</div>

		<div class="feature-section col two-col">
			<div class="last-feature">
				<h4><?php _ex( 'Main view', 'Welcome screen', 'buddymemberstats' ); ?></h4>
				<p><?php _ex( 'The main view displays up to five sections, where stats are grouped by activity relevancy: personnal, community, blogging, groups and forums. ', 'Welcome screen', 'buddymemberstats' ); ?></p>
			</div>

			<div>
				<h4><?php _ex( 'Section', 'Welcome screen', 'buddymemberstats' ); ?></h4>
				<p><?php _ex( 'Each section is compiling totals coming from BuddyPress components or third parties plugins like bbPress.', 'Welcome screen',  'buddymemberstats' ); ?></p>

			</div>
		</div>
	</div>

	<div class="changelog">
		<h3><?php _ex( 'Administration', 'Welcome screen', 'buddymemberstats' ); ?></h3>

		<div class="feature-section col two-col">
			<div class="last-feature">
				<h4><?php _ex( 'Backend', 'Welcome screen', 'buddymemberstats' ); ?></h4>
				<p><?php _ex( 'Once activated, Buddy Member Stats admins can change menu items to their need. No other settings are avaible in backend', 'Welcome screen', 'buddymemberstats' ); ?></p>
			</div>

			<div>
				<h4><?php _ex( 'Stats display', 'Welcome screen', 'buddymemberstats' ); ?></h4>
				<p><?php _ex( 'Users can change their stat display on profile in two ways: by global privacy or just by sections. Stats can be public, for members only or private. Sections can be hiden individually.', 'Welcome screen', 'buddymemberstats' ); ?></p>
			</div>
		</div>

		<div class="feature-section col three-col">

			<div>
				<h4><?php _ex( 'Note', 'Welcome screen', 'buddymemberstats' ); ?></h4>
				<p><?php _ex( 'Keep in mind that Buddy Member Stats displays only totals. Eg. My notifications shows a total of read and unread messages. That\'s what statistics generally do: totalize.', 'Welcome screen', 'buddymemberstats' ); ?></p>
			</div>

			<div class="last-feature">
				<h4><?php _ex( 'Third parties integration', 'Welcome screen', 'Welcome screen', 'buddymemberstats' ); ?></h4> </p>
				<p><?php _ex( 'Buddy Member Stats plays well with BuddyPress figures, but integrates also some third parties counting fetched in:', 'Welcome screen', 'buddymemberstats' ); ?></p>
					<ol>
					<li><a href="https://wordpress.org/plugins/bbpress/">bbPress</a></li>
					<li><a href="https://wordpress.org/plugins/buddypress-compliments/">BuddyPress Compliments</a></li>
					<li><a href="https://wordpress.org/plugins/buddydrive/">BuddyDrive</a></li>
					<li><a href="https://wordpress.org/plugins/buddypress-followers/">BuddyPress Follow</a></li>
					<li><a href="https://wordpress.org/plugins/buddy-views/">Buddy Views</a></li>
					</ol>
				<p><?php _ex('To get these third party figures, the respective plugin should be activated on your site.', 'Welcome screen', 'buddymemberstats' ); ?></p>

			</div>

			<div>
				<h4><?php _ex( 'Customization', 'Welcome screen', 'buddymemberstats' ); ?></h4>
				<p><?php _ex( 'Buddy Member Stats Screen is fully customisable. It comes with several placeholders where you can hook in. You can use the plugin\'s child-theme compliance to style the output to your need, by using:', 'Welcome screen', 'buddymemberstats' ); ?> <code>/child-theme/css/buddymemberstats.css</code></p>
 
			<h3><?php _ex( 'Enjoy and have fun !', 'Welcome screen', 'buddymemberstats' ); ?></h3>
			</div>
		</div>
	</div>

	<div class="return-to-dashboard">
		 <a href="<?php echo $settings_url ?>"><?php echo _x( 'Go to Buddy Member Stats settings', 'Welcome screen', 'buddymemberstats' ); ?></a>
	</div>

	</div>

	<?php
	}

// end of class	
} 

/**
 * Remove About page from admin menu
 * 
 * @package BuddyMemberStats
 * @since 1.0
*/
function bms_remove_about_menu() {
	
	remove_submenu_page( 'index.php', 'buddymemberstats-about'   );
	$version = buddymemberstats_get_plugin_version();		
}	
if( ! is_multisite() ) {	
	add_action( 'admin_menu', 'bms_remove_about_menu', 88 );
}		
if( is_multisite() ) {	
	add_action( 'network_admin_menu', 'bms_remove_about_menu', 88 );
}


/**
 * Launches the admin
 * 
 * @package BuddyMemberStats
 * @since 1.0
 * 
 * @uses buddymemberstats()
 */
function buddymemberstats_admin() {
	buddymemberstats()->admin = new BuddyMemberStats_Admin();
}

add_action( 'bp_loaded', 'buddymemberstats_admin' );

endif;