<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/* screen functions */

/**
 * Adds an action to the template hooks and loads the default member's template
 *
 * @package BuddyMemberStats
 * @subpackage BuddyMemberStats_Component
 * @since 1.0
 * 
 * @uses      bp_core_load_template() to ask BuddyPress to load the template
 */
function buddymemberstats_screen_main_nav() {
	add_action( 'bp_template_title',   'buddymemberstats_main_page_title' );
    add_action( 'bp_template_content', 'buddymemberstats_main_page_content' );
	
	bp_core_load_template( apply_filters( 'buddymemberstats_screen_main_nav', 'members/single/plugins' ) );
}

/**
 * Adds an action to the template hooks and loads the default member's template
 *
 * @package BuddyMemberStats
 * @subpackage BuddyMemberStats_Component
 * @since 1.0
 * 
 * @uses      bp_core_load_template() to ask BuddyPress to load the template
 */
function buddymemberstats_screen_options_subnav() {
	add_action( 'bp_template_title',   'buddymemberstats_options_page_title' );
    add_action( 'bp_template_content', 'buddymemberstats_options_page_content' );	
	bp_core_load_template( apply_filters( 'buddymemberstats_screen_options_subnav', 'members/single/plugins' ) );
}

/**
 * Adds an action to the template hooks and loads the default member's template
 *
 * @package BuddyMemberStats
 * @subpackage BuddyMemberStats_Component
 * @since 1.0
 * 
 * @uses      bp_core_load_template() to ask BuddyPress to load the template
 */
function buddymemberstats_screen_user_settings() {
	add_action( 'bp_template_title', 'buddymemberstats_screen_user_settings_title' );
	add_action( 'bp_template_content', 'buddymemberstats_screen_user_settings_content' );
	bp_core_load_template( apply_filters( 'buddymemberstats_screen_user_settings', 'members/single/plugins' ) );
}

/**
 * Displays the title of main nav
 *
 * @package BuddyMemberStats
 * @subpackage BuddyMemberStats_Component
 * @since 1.0
 * 
 * @uses      buddymemberstats_component_primary_subnav_name() as an example
 */
function buddymemberstats_main_page_title() {
	buddymemberstats_component_primary_subnav_name();
}

/**
 * Displays the content of main nav
 *
 * @package BuddyMemberStats
 * @subpackage BuddyMemberStats_Component
 * @since 1.0
 * 
 */

function buddymemberstats_main_page_content() { 
global $bp;
$user_id = bp_displayed_user_id(); 

$show_personnal		= get_user_meta( bp_displayed_user_id(), 'bms_show_personnal_section', 'yes' );
$show_community		= get_user_meta( bp_displayed_user_id(), 'bms_show_community_section', 'yes' );
$show_blogging		= get_user_meta( bp_displayed_user_id(), 'bms_show_blogging_section', 'yes' );
$show_group			= get_user_meta( bp_displayed_user_id(), 'bms_show_group_section', 'yes' );
$show_forum			= get_user_meta( bp_displayed_user_id(), 'bms_show_forum_section', 'yes' );
$privacy			= get_user_meta( bp_displayed_user_id(), 'bms_stat_privacy', 'justme' );
$privacy2			= get_user_meta( bp_displayed_user_id(), 'bms_stat_privacy', 'members' );

	if ( $privacy == 'justme' ) { 
		    // The user ID of the currently logged in user
    $current_user_id = (int) trim($bp->loggedin_user->id);

    // The author that we are currently viewing
    $author_id  = (int) trim($bp->displayed_user->id);

    if ( $current_user_id !== $author_id ) { ?>
		 <span class="bms_p_error"><i class="icon bms-icon-p-error"></i><?php _e( 'Sorry, my stats are private.', 'buddymemberstats' ); ?></span>
	<?php return; 
		}
	}

	if ( $privacy2 == 'members' ) { 
		if ( ! is_user_logged_in() ) {?>
		 <span class="bms_m_error"><i class="icon bms-icon-m-error"></i><?php _e( 'Sorry, only members can view my stats.', 'buddymemberstats' ); ?></span>
	<?php	return; 
		}
	}

// screen content start here

	do_action( 'bms_stat_list_before' ); 

/**
 * Personnal section
 * This section is displayed by default, we invite the user to check his options.
 * This message will be hidden once an option is checked and registered.
*/

	if( bp_is_my_profile() && ( $show_personnal == '' ) ) { 
		$url = bp_get_loggedin_user_link().'settings/buddymemberstats';
		$link = sprintf( wp_kses( __( 'Please, go to your Stats settings to <a href="%s">register</a> this option and find some others.', 'buddymemberstats' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $url ) ); ?>
		<div class="first-attempt-msg"><p><i class="icon bms-icon-info"></i><?php echo $link; ?><br/><?php _e( 'Permanent access to stats settings is User menu > Profile > Settings > Stats options', 'buddymemberstats' ); ?></p></div>
	<?php }


if ( $show_personnal !== 'no' ) { ?>

	<div id="bms-block-perso">

		<?php do_action( 'bms_personnal_block_before_list' ); ?>	

	<h4 id="bms-personnal-stats"><i class="icon bms-icon-perso"></i><?php _e( 'Personnal Stats', 'buddymemberstats' ); ?></h4>

	<ul id="bms-list-personnal-stat">

	<?php if ( class_exists( 'Buddy_Views' ) ) { ?>

			<li><?php _e( 'My profile views:', 'buddymemberstats' ); ?>&nbsp;<?php bms_buddy_views_count(); ?></li> 
	<?php 	}	

		 if ( bp_is_active( 'activity' ) ) {	?>
			<li><?php _e( 'My favorites:', 'buddymemberstats' ); ?>&nbsp;<?php echo bp_get_total_favorite_count_for_user( $user_id ); ?></li>
		<?php }

		if ( bp_is_active( 'notifications' ) ) {	?>	
			<li><?php _e( 'My mentions:', 'buddymemberstats' ); ?>&nbsp;<?php bms_get_total_mention_count();  ?></li>
			<li><?php _e( 'My notifications:', 'buddymemberstats' ); ?>&nbsp; <?php bms_get_total_notifications_count(); ?> </li>
	<?php	}

		if ( bp_is_active( 'messages' ) ) {	?>
			<li><?php _e( 'My received messages:', 'buddymemberstats' ); ?>&nbsp;<?php bms_get_received_messages_count(); ?></li>
			<li><?php _e( 'My starred messages:', 'buddymemberstats' ); ?>&nbsp;<?php bms_get_total_starred_message_count(); ?></li>
	<?php	}

		if ( bp_is_active( 'friends' ) ) {	?>
			<li><?php _e( 'My friends:', 'buddymemberstats' ); ?>&nbsp;<?php echo friends_get_friend_count_for_user( $user_id ); ?></li>	
	<?php	}	

		if ( function_exists( 'bp_follow_total_follow_counts' ) ) : ?>

			<li><?php _e( 'My followers:', 'buddymemberstats' ); ?>&nbsp;<?php bms_get_bp_followers(); ?></li>

		<?php endif; ?>

		<?php do_action( 'bms_personnal_block_inlist' ); ?>	
		
	</ul>

		<?php do_action( 'bms_personnal_block_outlist' ); ?>

	</div>

<?php } //end of personnal section

// community section
if ( bp_is_active( 'activity' ) )  :

	if( $show_community == 'yes' ) { ?>

		<div id="bms-block-implication">

			<?php do_action( 'bms_implication_block_before_list' ); ?>

		<h4 id="bms-community-stats"><i class="icon bms-icon-community"></i><?php _e( 'Community Stats', 'buddymemberstats' ); ?></h4>

			<ul id="bms-list-implication-stat">
				<li><?php _e( 'Member since', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_member_since(); ?></li>
				<?php echo bms_get_roles(); ?>
				<li><?php _e( 'Activity Ratio:', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_ratio();  ?><span>%</span></li>
				<li><?php _e( 'Posted Attachments:', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_attachment_stat(); ?></li>

		<?php if ( class_exists( 'BP_Compliments' ) ) : ?>

			<li><?php _e( 'Received Compliments:', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_compliment_stats();  ?></li>
						
		<?php endif; ?>

		<?php if ( class_exists( 'buddydrive' ) ) : ?>

				<li><?php _e( 'BuddyDrive files:', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_buddydrive_stat(); ?></li>
		
			<?php if ( bms_get_buddydrive_stat_dl() > 0 ) { ?>

				<li><?php _e( 'BuddyDrive downloads:', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_buddydrive_stat_dl(); ?></li>

			<?php } 

				endif; ?>

		<?php if ( function_exists( 'bp_follow_total_follow_counts' ) ) : ?>

				<li><?php _e( 'Followed members:', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_bp_following(); ?></li>

		<?php endif; ?>

				<?php do_action( 'bms_implication_block_inlist' ); ?>

			</ul>

		<?php do_action( 'bms_implication_block_outlist' ); ?>

		</div>

<?php } 

endif; // end of community section

// blogging section
if ( $show_blogging == 'yes' ) { ?> 

	<div id="bms-block-blogging">		

	<h4 id="bms-site-stats"><i class="icon bms-icon-blog"></i><?php _e( 'Blogging Stats', 'buddymemberstats' ); ?></h4>

		<ul id="bms-list-blog-stat">

		<?php	if ( is_multisite() ) {	?>
				<li><?php _e( 'My blogs:', 'buddymemberstats' ); ?>&nbsp;<?php echo bp_get_total_blog_count_for_user( $user_id ); ?></li>
		<?php	}	?>

		<?php   if ( bms_check_user_role( 'subscriber' ) ) { 					
			// we do nothing		
				} else {   ?>
			<li><?php _e( 'My posts:', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_total_post_count(); ?></li>
				
				<?php } ?>

			<li><?php _e( 'My comments:', 'buddymemberstats' ); ?>&nbsp;<?php echo bms_get_total_comments_count(); ?></li>

			<?php do_action( 'bms_blogging_block_inlist' ); ?>

		</ul>

		<?php do_action( 'bms_blogging_block_outlist' ); ?>

	</div>

<?php } // end of blog section 

// group section
if ( bp_is_active( 'groups' ) ) :  

	if ($show_group == 'yes') { 

		if ( bp_get_total_group_count_for_user( $user_id ) > 0 ) { ?>

		<div id="bms-block-group">

			<?php do_action( 'bms_group_block_before_list' ); ?>

		<h4 id="bms-group-stats"><i class="icon bms-icon-group"></i><?php _e( 'Group Stats', 'buddymemberstats' ); ?></h4>

			<ul id="bms-list-group-stat">
				<li><?php _e( 'My groups:', 'buddymemberstats' ); ?>&nbsp;<?php echo bp_get_total_group_count_for_user( $user_id ); ?></li>
				<li><?php _e( 'My updates:', 'buddymemberstats' ); ?>&nbsp;<?php bms_get_total_group_updates_count(); ?></li>

				<?php do_action( 'bms_group_block_inlist' ); ?>

			</ul>

			<?php do_action( 'bms_group_block_outlist' ); ?>

		</div>

<?php 	} 
	}

endif; // end of group section

// forum section
if ( class_exists( 'bbPress' ) ) :

	if( $show_forum == 'yes' ) { ?>

	<div id="bms-block-forum">

		<?php do_action( 'bms_forum_block_before_list' ); ?>

	<h4 id="bms-forum-stats"><i class="icon bms-icon-forum"></i><?php _e( 'Forum Stats', 'buddymemberstats' ); ?></h4>

		<ul id="bms-list-forum-stat">

			<li><?php _e( 'Topics:', 'buddymemberstats' ); ?>&nbsp;<?php echo bbp_get_user_topic_count_raw( $user_id ); ?></li>
			<li><?php _e( 'Replies:', 'buddymemberstats' ); ?>&nbsp;<?php echo bbp_get_user_reply_count_raw( $user_id ); ?></li>

			<?php do_action( 'bms_forum_block_inlist' ); ?>
		
		</ul>
			<?php do_action( 'bms_forum_block_outlist' ); ?>
	</div>

<?php } endif; // end of forum section

	do_action( 'bms_stat_list_after' ); 
}

/**
 * Displays the title of user settings BuddyMemberStats nav
 *
 * @package BuddyMemberStats
 * @subpackage BuddyMemberStats_Component
 * @since 1.0
 * 
 */
function buddymemberstats_screen_user_settings_title() {
	buddymemberstats_component_user_settings_nav_name();
}

/**
 * Displays the content of BuddyMemberStats user settings
 *
 * @package BuddyMemberStats
 * @subpackage BuddyMemberStats_Component
 * @since 1.0
 * 
 */

function buddymemberstats_screen_user_settings_content() { ?>
<div id="buddymemberstats-content">
<?php if( bp_is_my_profile() ) { ?>

	<form method="post" action=""> 

		<table class="widefat fixed" style="padding:10px;margin-top: 10px;">
			
			<tbody>
			<tr valign="top">
				<th scope="row"><?php  _e( 'Stats Display', 'buddymemberstats' ); ?></th>
				<th></th>
			</tr>

			<tr valign="top">
				<td><?php  _e( 'Who can see my stats page ?', 'buddymemberstats' ); ?><td>
				<td>
					<select id="bms_stat_privacy" name="bms_stat_privacy">
						<option value="all" <?php selected( 'all', get_user_meta( bp_displayed_user_id(), 'bms_stat_privacy', true ) ); ?> ><?php _e( 'Anybody', 'buddymemberstats' ); ?></option>
						<option value="justme" <?php selected( 'justme', get_user_meta( bp_displayed_user_id(), 'bms_stat_privacy', true ) ); ?>><?php _e( 'Just me', 'buddymemberstats' ); ?></option>
						<option value="members" <?php selected( 'members', get_user_meta( bp_displayed_user_id(), 'bms_stat_privacy', true ) ); ?>><?php _e( 'Members', 'buddymemberstats' ); ?></option>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php  _e( 'Show/hide sections', 'buddymemberstats' ); ?></th>	
				<th scope="row"><?php bms_yes_box(); ?>&nbsp;&nbsp;&nbsp;<?php bms_no_box(); ?></th>
			</tr>

			<tr valign="top">
				<td><?php  _e( 'Personnal Stats', 'buddymemberstats' ); ?></td>
				<td>
					<label for="yes"><?php bms_yes_box(); ?><input name="bms_show_personnal_section" type="radio" value="yes" <?php checked( 'yes', get_user_meta( bp_displayed_user_id(), 'bms_show_personnal_section', true ) ); ?> /></label>
					<label for="no"><?php bms_no_box(); ?><input name="bms_show_personnal_section" type="radio" value="no" <?php checked( 'no', get_user_meta( bp_displayed_user_id(), 'bms_show_personnal_section', true ) ); ?> /></label>
				</td> 
			</tr>

			<tr valign="top">
				<td><?php  _e( 'Community Stats', 'buddymemberstats' ); ?></td>
				<td>
					<label for="yes"><?php bms_yes_box(); ?><input name="bms_show_community_section" type="radio" value="yes" <?php checked( 'yes', get_user_meta( bp_displayed_user_id(), 'bms_show_community_section', true ) ); ?> /></label>
					<label for="no"><?php bms_no_box(); ?><input name="bms_show_community_section" type="radio" value="no" <?php checked( 'no', get_user_meta( bp_displayed_user_id(), 'bms_show_community_section', true ) ); ?> /></label>
				</td>
			</tr>

			<tr valign="top">
				<td><?php  _e( 'Blogging Stats', 'buddymemberstats' ); ?></td>
				<td>
					<label for="yes"><?php bms_yes_box(); ?><input name="bms_show_blogging_section" type="radio" value="yes" <?php checked( 'yes', get_user_meta( bp_displayed_user_id(), 'bms_show_blogging_section', true ) ); ?> /></label>
					<label for="no"><?php bms_no_box(); ?><input name="bms_show_blogging_section" type="radio" value="no" <?php checked( 'no', get_user_meta( bp_displayed_user_id(), 'bms_show_blogging_section', true ) ); ?> /></label>
				</td>
			</tr>

			<?php if( bp_is_active( 'groups' ) ) { ?>
			<tr valign="top">
				<td><?php  _e( 'Group Stats', 'buddymemberstats' ); ?></td>
				<td>
					<label for="yes"><?php bms_yes_box(); ?><input name="bms_show_group_section" type="radio" value="yes" <?php checked( 'yes', get_user_meta( bp_displayed_user_id(), 'bms_show_group_section', true ) ); ?> /></label>
					<label for="no"><?php bms_no_box(); ?><input name="bms_show_group_section" type="radio" value="no" <?php checked( 'no', get_user_meta( bp_displayed_user_id(), 'bms_show_group_section', true ) ); ?> /></label>

				</td>
			</tr>
			<?php } ?>

			<?php if( class_exists( 'bbPress' ) ) { ?>
			<tr valign="top">
				<td><?php  echo __( 'Forum Stats', 'buddymemberstats' ); ?></td>
				<td>
					<label for="yes"><?php bms_yes_box(); ?><input name="bms_show_forum_section" type="radio" value="yes" <?php checked( 'yes', get_user_meta( bp_displayed_user_id(), 'bms_show_forum_section', true ) ); ?> /></label>
					<label for="no"><?php bms_no_box(); ?><input name="bms_show_forum_section" type="radio" value="no" <?php checked( 'no', get_user_meta( bp_displayed_user_id(), 'bms_show_forum_section', true ) ); ?> /></label>
				</td>
			</tr>
			<?php } ?>

			<tr valign="top" colspan="3">
				<td>
					<input class="submit" type="submit" name="bms_submit" value="<?php _e('Save Changes', 'buddymemberstats' ); ?>" /></td><td><input type="button" name="bms_clear" value="<?php _e('Clear', 'buddymemberstats' ); ?>" onclick="bmsClearForm(this.form);"></td><td><?php bms_back_button(); ?></td>
					<input type="hidden" name="user_id" id="user_id" value="<?php echo esc_attr( bp_displayed_user_id() ); ?>" />
					<?php wp_nonce_field( 'show-hide-stat-section_' . bp_displayed_user_id() ) ?>
				</td>
			</tr>
		</table>
		<script type="text/javascript">
			function bmsClearForm(cForm) {
			   
			  var elements = cForm.elements;
			   
			  cForm.reset();

				var frm_elements = cForm.elements;
				for (i = 0; i < frm_elements.length; i++)
				{
					field_type = frm_elements[i].type.toLowerCase();
					switch (field_type)
					{
					case "radio":
						if (frm_elements[i].checked)
						{
							frm_elements[i].checked = false;
						}
						break;
					default:
						break;
					}
				}
			}
		</script>
	</form>
</div>
	<?php } 
}