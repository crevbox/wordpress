<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/* conditional functions */

/**
 * Is BuddyMemberStats the current BuddyPress component ?
 * 
 * @package BuddyMemberStats
 * @subpackage Component
 * @since 1.0
 *
 * @uses bp_is_current_component() to get current component
 * @uses buddymemberstats_get_component_id() to get BuddyMemberStats component id
 */
function buddymemberstats_is_current_component() {
	return bp_is_current_component( buddymemberstats_get_component_id() );
}

/**
 * Are we on BuddyMemberStats user settings page ?
 * 
 * @package BuddyMemberStats
 * @subpackage Component
 * @since 1.0
 *
 * @uses bp_is_settings_component() are we in the settings component ?
 * @uses bp_is_current_action() is BuddyMemberStats the current action ?
 */
function buddymemberstats_is_user_settings() {
	if( bp_is_settings_component() && bp_is_current_action( buddymemberstats_get_component_slug() ) )
		// displaying the screen content
		bms_front_options();
}

/**
 * Are we on BuddyMemberStat's component area ?
 * 
 * @package BuddyMemberStats
 * @subpackage Component
 * @since 1.0
 *
 * @uses buddymemberstats_is_current_component() Is BuddyMemberStats the current BuddyPress component ?
 * @uses buddymemberstats_is_user_settings() Are we on BuddyMemberStats user settings page ?
 */
function buddymemberstats_is_component_area() {
	if( buddymemberstats_is_current_component() || buddymemberstats_is_user_settings() )
		return true;

	return false;
}

/**
 * Url to images dir
 *
 * @uses  buddymemberstats()
 * @return string the url
 */
function buddymemberstats_get_images_url() {
	return buddymemberstats()->images_url;
}