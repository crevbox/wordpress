<?php
/**
 * Plugin Name: Buddy Member Stats
 * Description: A plugin to get all BuddyPress related counts on user profile.
 * Version: 1.0.1
 * Author: danbp
 * Author URI:  http://bp-fr.net
 * License: GPLv2
 * Text Domain: buddymemberstats
 * Domain Path: /languages
*/


// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;


if ( !class_exists( 'BuddyMemberStats' ) ) :
/**
 * Main BuddyMemberStats Class
 *
 * @since Buddy Member Stats 1.0
 */
class BuddyMemberStats {
	/**
	 * Instance of this class.
	 *
	 * @package BuddyMemberStats
	 * @since    1.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Some init vars
	 *
	 * @package BuddyMemberStats
	 * @since    1.0
	 *
	 * @var      array
	 */
	public static $init_vars = array(
		'component_id'        => 'buddymemberstats',
		'component_root_slug' => 'buddymemberstats',
		'component_name'      => 'BuddyMemberStats',
		'bp_version_required' => '2.2'
	);

	/**
	 * Initialize the plugin
	 * 
	 * @package BuddyMemberStats
	 * @since 1.0
	 */
	private function __construct() {
		$this->setup_globals();
		$this->includes();
		$this->setup_hooks();
	}

	/**
	 * Return an instance of this class.
	 *
	 * @package BuddyMemberStats
	 * since 1.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Sets some globals for the plugin
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 * 
	 * @global $wp_admin_bar object
	 */
	private function setup_globals() {
		/** BuddyMemberStats globals ********************************************/
		$this->version                = '1.0.1';
		$this->domain                 = 'buddymemberstats';
		$this->file                   = __FILE__;
		$this->basename               = plugin_basename( $this->file );
		$this->plugin_dir             = plugin_dir_path( $this->file );
		$this->plugin_url             = plugin_dir_url( $this->file );
		$this->lang_dir               = trailingslashit( $this->plugin_dir . 'languages' );
		$this->includes_dir           = trailingslashit( $this->plugin_dir . 'includes' );
		$this->includes_url           = trailingslashit( $this->plugin_url . 'includes' );
		$this->component_includes_dir = trailingslashit( $this->includes_dir . 'component-includes' );
		$this->groups_includes_dir    = trailingslashit( $this->includes_dir . 'groups-includes' );
		$this->plugin_js              = trailingslashit( $this->includes_url . 'js' );
		$this->plugin_css             = trailingslashit( $this->includes_url . 'css' );
		$this->images_url             = trailingslashit( $this->includes_url . '/admin-includes/images' );

		/** Component specific globals ********************************************/
		$this->component_id                     = self::$init_vars['component_id'];
		$this->component_slug                   = self::$init_vars['component_root_slug'];
		$this->component_name                   = self::$init_vars['component_name'];
		$this->component_primary_subnav_slug    = 'buddymemberstats-primary';
		$this->component_primary_subnav_name    = 'Buddy Member Stats Primary';		
		$this->component_user_settings_nav_name = 'Buddy Member Stats User Settings';

	}

	/**
	 * Checks BuddyPress version
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 */
	public static function buddypress_version_check() {
		// taking no risk
		if( !defined( 'BP_VERSION' ) )
			return false;

		return version_compare( BP_VERSION, self::$init_vars['bp_version_required'], '>=' );
	}

	/**
	 * Checks if current blog is the one where is activated BuddyPress
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 */
	public static function buddypress_site_check() {
		global $blog_id;

		if( !function_exists( 'bp_get_root_blog_id' ) )
			return false;

		if( $blog_id != bp_get_root_blog_id() )
			return false;
		
		return true;
	}

	/**
	 * Includes the needed files
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 */
	private function includes() {
		require( $this->includes_dir . 'functions.php' );

		if( is_admin() )
			require( $this->includes_dir . 'admin.php' );
	}

	/**
	 * Sets the key hooks to add an action or a filter to
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 */
	private function setup_hooks() {
		// Bail if BuddyPress version is not supported or current blog is not the one where BuddyPress is activated
		if( ! self::buddypress_version_check() || ! self::buddypress_site_check() )
			return;

		//Actions
		// loads the languages..
		add_action( 'bp_init',            array( $this, 'load_textdomain' ), 6 );
		add_action( 'bp_include',         array( $this, 'load_component'  )    );
		add_action( 'bp_enqueue_scripts', array( $this, 'cssjs'           )    );
	}

	/**
	 * Loads the component
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 */
	public function load_component() {
		require( $this->includes_dir . 'component.php' );
	}

	/**
	 * Enqueues the js and css files only if BuddyMemberStats needs it
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 * 
	 * @uses bp_is_active() to check if a BuddyPress component is active
	 * @uses buddymemberstats_is_component_are() to check if we are in a BuddyMemberStats component area
	 * @uses buddymemberstats_is_group_area() to check if we are in a BuddyMemberStats group area
	 * @uses buddymemberstats_is_current_component() are we the current component ?
	 * @uses buddymemberstats_is_directory() are we on a BuddyMemberStats directory page ?
	 * @uses buddymemberstats_is_user_settings() are we on the user's setting BuddyMemberStats page ?
	 * @uses wp_enqueue_style() to safely add our style to WordPress queue
	 * @uses wp_enqueue_script() to safely add our script to WordPress queue
	 * @uses wp_localize_script() to attach some vars to it
	 */
	public function cssjs() {

		if( ( bp_is_active( $this->component_id ) && buddymemberstats_is_component_area() ) ) {

			$localized_script = array();

			if( bp_is_active( $this->component_id ) ) {
				$localized_script = array_merge( $localized_script, array(
						'component'     => buddymemberstats_is_current_component(),						
						'user_settings' => buddymemberstats_is_user_settings()
					)
				);
			}

			// CSS is Theme's territory, so let's help him to easily override our css.
			$css_datas = (array) $this->css_datas();
			wp_enqueue_style( 'dashicons' );
			wp_enqueue_style( $css_datas['handle'], $css_datas['location'], false, $this->version );
			wp_localize_script( 'buddymemberstats-js', 'buddymemberstats_vars', $localized_script );
		}
		
	}

	/**
	 * The theme can override plugin's css
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 */
	public function css_datas() {
		$file = 'css/buddymemberstats.css';
		
		// Check child theme
		if ( file_exists( trailingslashit( get_stylesheet_directory() ) . $file ) ) {
			$location = trailingslashit( get_stylesheet_directory_uri() ) . $file ; 
			$handle   = 'buddymemberstats-child-css';

		// Check parent theme
		} elseif ( file_exists( trailingslashit( get_template_directory() ) . $file ) ) {
			$location = trailingslashit( get_template_directory_uri() ) . $file ;
			$handle   = 'buddymemberstats-parent-css';

		// use our style
		} else {
			$location = $this->includes_url . $file;
			$handle   = 'buddymemberstats-css';
		}

		return array( 'handle' => $handle, 'location' => $location );
	}

	/**
	 * Loads the translation files
	 *
	 * @package BuddyMemberStats
	 * since 1.0
	 * 
	 * @uses get_locale() to get the language of WordPress config
	 * @uses load_texdomain() to load the translation if any is available for the language
	 */

	public function load_textdomain() {
		// try to get locale
		$locale = apply_filters( 'buddymemberstats_load_textdomain_get_locale', get_locale(), $this->domain );
		$mofile = sprintf( '%1$s-%2$s.mo', $this->domain, $locale );

		// Setup paths to a buddymemberstats subfolder in WP LANG DIR
		$mofile_global = WP_LANG_DIR . '/buddymemberstats/' . $mofile;

		// Look in global /wp-content/languages/buddymemberstats folder
		if ( ! load_textdomain( $this->domain, $mofile_global ) ) {

			// Look in local /wp-content/plugins/buddymemberstats/languages/ folder
			// or /wp-content/languages/plugins/
			load_plugin_textdomain( $this->domain, false, basename( $this->plugin_dir ) . '/languages/' );
		}
	}

	/**
	 * Activation method
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 * 
	 */
	public static function activation() {
		// Bail if BuddyPress version is not supported or current blog is not the one where BuddyPress is activated
		if( ! self::buddypress_version_check() || ! self::buddypress_site_check() )
			return;
		do_action( 'buddymemberstats_activation' );
	}

	/**
	 * Deactivation method
	 * 
	 * @package BuddyMemberStats
	 * since 1.0
	 * 
	 */
	public static function deactivation() {
		// Bail if BuddyPress version is not supported or current blog is not the one where BuddyPress is activated
		if( ! self::buddypress_version_check() || ! self::buddypress_site_check() )
			return;
		do_action( 'buddymemberstats_deactivation' );
	}
	
}

// Let's start !
function buddymemberstats() {
	return BuddyMemberStats::get_instance();
}
// Not too early and not too late ! 9 seems ok ;)
add_action( 'bp_include', 'buddymemberstats', 9 );

// Activation
add_action( 'activate_' . plugin_basename( __FILE__ ) , array( 'BuddyMemberStats', 'activation' ) );

// Deactivation
add_action( 'deactivate_' . plugin_basename( __FILE__ ) , array( 'BuddyMemberStats', 'deactivation' ) );

endif;