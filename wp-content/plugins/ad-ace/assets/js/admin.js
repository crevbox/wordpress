/* global jQuery */
/* global document */

(function($) {

	'use strict';

	$(document).ready(function(){
		$('.adace-image-upload').each(function() {
			imageUploadControl($(this));
		});
		hideElements();
		clearSponsorLogoOnPublish();
		manageSlotsToggles();
		initAdMetabox();
		initMultipleSelect();
	});

	var imageUploadControl = function($el) {
		var
		$image = $el.find('.adace-image'),
		$addLink = $el.find('.adace-add-image'),
		$deleteLink = $el.find('.adace-delete-image'),
		$imageId = $el.find('.adace-image-id');

		if ( $imageId.val().length > 0 ) {
			$addLink.hide();
			$deleteLink.show();
		} else {
			$addLink.show();
			$deleteLink.hide();
		}

		$addLink.on('click', function(e) {
			e.preventDefault();
			openMediaLibrary(function(imageObj) {
				var
				thumb = '';
				if( imageObj.sizes.thumbnail !== undefined ){
					thumb = imageObj.sizes.thumbnail;
				} else {
					thumb = imageObj.sizes.full;
				}

				$image.html('<img src="' + thumb.url + '" width="' + thumb.width + '" height="' + thumb.height + '" />');
				$imageId.val(imageObj.id);

				$addLink.hide();
				$deleteLink.show();
			});
		});

		$deleteLink.on('click', function(e) {
			e.preventDefault();

			if ( ! confirm( 'Are you sure?' ) ) {
				return;
			}

			$image.empty();
			$imageId.val('');

			$addLink.show();
			$deleteLink.hide();
		});
	};

	var openMediaLibrary = function(callback) {
		var
		frame = wp.media({
			'title': 'Select an image',
			'multiple': false,
			'library': {
				'type': 'image'
			},
			'button': {
				'text': 'Add Image'
			}
		});

		frame.on('select',function() {
			var objSelected = frame.state().get('selection').first().toJSON();
			callback(objSelected);
		});

		frame.open();
	};

	var hideElements = function() {
		$('#adace_override_hide_elements').on('change', function() {
			var
			option = $(this).val(),
			$dependent = $('#adace-hide-elements-wrapper');

			if ('none' === option) {
				$dependent.hide();
			} else {
				$dependent.show();
			}
		});

		$('.adace-ad-section select:disabled').each(function(){
			$(this).closest('tr').hide();
		});
		$('.adace-ad-section input:disabled').each(function(){
			$(this).closest('tr').hide();
		});
	};

	var clearSponsorLogoOnPublish = function() {
		var $SponsorLogoWrap = $('.form-field.bimber-sponsor-logo-wrap'),
			$image = $SponsorLogoWrap.find('.bimber-image'),
			$addLink = $SponsorLogoWrap.find('.bimber-add-image'),
			$deleteLink = $SponsorLogoWrap.find('.bimber-delete-image'),
			$imageId = $SponsorLogoWrap.find('.bimber-image-id');

		if ($SponsorLogoWrap.lenght == 0) { return; }
		$('#addtag #submit').click(function () {
		    if ($('#addtag .form-invalid').length == 0) {
				$image.empty();
				$imageId.val('');

				$addLink.show();
				$deleteLink.hide();
		    }
		});
	};

	var manageSlotsToggles = function() {
		var SlotsToggles = $('#adace_slots-form .postbox');
		SlotsToggles.each(function(){
			var
			ThisToggle = $(this),
			ClickTargets = ThisToggle.find('.handlediv, .hndle');
			ClickTargets.click(function(e){
				e.preventDefault();
				ThisToggle.toggleClass('closed');
			});
		});
	};

	var initAdMetabox = function() {
		var $adType 		= $('#adace_ad_type');
		var $adsenseSection = $('#adace_ad_meta_box_adsense');
		var $customSection 	= $('#adace_ad_meta_box_custom');
		var $customTab		= $('.adace-nav-tab-custom');
		var $adsenseTab		= $('.adace-nav-tab-adsense');

		var $adSenseType 	= $('#adace_adsense_type');
		var $adSenseFormat 	= $('.adace_adsense_format');

		var $adSize 		= $('.adace_adsense_size');
		var $adSenseWidth 	= $('#adace_adsense_width');
		var $adSenseHeight 	= $('#adace_adsense_height');
		var $adSensePaste 	= $('#adace_adsense_paste');
		var $adSenseSlot 	= $('#adace_adsense_slot');
		var $adSensePub 	= $('#adace_adsense_pub');

		var $desktopCustom		= $('#adace_adsense_use_size_desktop');
		var $desktopSection		= $('.adace_adsense_size_desktop');
		var $landscapeCustom	= $('#adace_adsense_use_size_landscape');
		var $landscapeSection	= $('.adace_adsense_size_landscape');
		var $portraitCustom		= $('#adace_adsense_use_size_portrait');
		var $portraitSection	= $('.adace_adsense_size_portrait');
		var $phoneCustom 		= $('#adace_adsense_use_size_phone');
		var $phoneSection 		= $('.adace_adsense_size_phone');

		var setSectionVisibility = function() {
			if ( $adType.val() === 'adsense' ) {
				$customSection.hide();
				$adsenseSection.show();
			} else {
				$customSection.show();
				$adsenseSection.hide();
			}
		};
		var setSizesVisibility = function() {
			if ( $adSenseType.val() === 'fixed' ) {
				$adSize.show();
				$adSenseFormat.hide();
			} else {
				$adSize.hide();
				$adSenseFormat.show();
			}
			if ( $desktopCustom.is(':checked')) {
				$desktopSection.show();
			} else {
				$desktopSection.hide();
			}
			if ( $landscapeCustom.is(':checked')) {
				$landscapeSection.show();
			} else {
				$landscapeSection.hide();
			}
			if ( $portraitCustom.is(':checked')) {
				$portraitSection.show();
			} else {
				$portraitSection.hide();
			}
			if ( $phoneCustom.is(':checked')) {
				$phoneSection .show();
			} else {
				$phoneSection .hide();
			}
		};

		setSectionVisibility();
		setSizesVisibility();
		$adType.bind( 'change', setSectionVisibility);
		$adSenseType.bind( 'change', setSizesVisibility);
		$desktopCustom.bind( 'change', setSizesVisibility);
		$landscapeCustom.bind( 'change', setSizesVisibility);
		$portraitCustom.bind( 'change', setSizesVisibility);
		$phoneCustom.bind( 'change', setSizesVisibility);

		$customTab.bind('click', function() {
			$adsenseTab.removeClass('nav-tab-active');
			$customTab.addClass('nav-tab-active');
			$adType.val('custom');
			setSectionVisibility();
		});

		$adsenseTab.bind('click', function() {
			$customTab.removeClass('nav-tab-active');
			$adsenseTab.addClass('nav-tab-active');
			$adType.val('adsense');
			setSectionVisibility();
		});

		$adSensePaste.on( 'change', function() {
			if ($(this).val() === ''){
				return;
			}
			var $html 	= $($.parseHTML($(this).val()));
			var $ins 	= $html.filter('ins');
			$adSensePub.val($ins.attr('data-ad-client'));
			$adSenseSlot.val($ins.attr('data-ad-slot'));
			if ( parseInt($ins.css('width'), 10) > 0 ){
				$adSenseWidth.val(parseInt($ins.css('width'), 10));
				$adSenseHeight.val(parseInt($ins.css('height'), 10));
				$adSenseType.val('fixed');
			} else {
				$adSenseWidth.val(0);
				$adSenseHeight.val(0);
				$adSenseType.val('responsive');
			}
			setSizesVisibility();
			$(this).val('');
		});

	};

	var initMultipleSelect = function() {
		$('.adace-multiple-with-none').change( function() {
			if ($('option:first', this).is(':selected')) {
				$('option:not(:first)', this).prop('selected', false);
			}
		});
	};

})(jQuery);
