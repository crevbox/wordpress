<?php
/**
 * Init Sponsor
 *
 * @package AdAce.
 * @subpackage Sponsors.
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

// Load sponsors common parts.
require_once( trailingslashit( adace_get_plugin_dir() ) . 'includes/sponsors/common/register.php' );

// Load sponsors backend parts.
if ( is_admin() ) {
	require_once( trailingslashit( adace_get_plugin_dir() ) . 'includes/sponsors/admin/fields.php' );
}

// Load sponsors front parts.
if ( ! is_admin() ) {
	require_once( trailingslashit( adace_get_plugin_dir() ) . 'includes/sponsors/front/get-sponsor.php' );
}
