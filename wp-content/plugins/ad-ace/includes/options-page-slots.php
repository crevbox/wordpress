<?php
/**
 * Options Page for Slots
 *
 * @package AdAce
 * @subpackage Functions
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Prints out all settings sections for ads slots.
 * This is reworked do_settings_sections();
 *
 * @global $wp_settings_sections Storage array of all settings sections added to admin pages
 * @global $wp_settings_fields Storage array of settings fields and info about their pages/sections
 *
 * @param string $page The slug name of the page whose settings sections you want to output.
 */
function adace_do_slots_settings_sections( $page ) {
	global $wp_settings_sections, $wp_settings_fields;
	if ( ! isset( $wp_settings_sections[ $page ] ) ) {
		return;
	}

	$adace_ad_slots = adace_access_ad_slots();
	$adace_ad_sections = adace_access_ad_sections();

	$adace_ad_sections[] = array(
		'slug' => 'default',
		'label' => __( 'Various', 'adace' ),
	);
	?>

	<?php foreach ( $adace_ad_sections as $ad_section ) : ?>
		<div class="adace-ad-section">
			<h3><?php echo wp_kses_post( $ad_section['label'] ); ?></h3>
			<?php foreach ( (array) $wp_settings_sections[ $page ] as $section ) : ?>
				<?php
					$id = $section['id'];
					$id = str_replace( 'adace_slot_','',$id );
					$id = str_replace( '_section','',$id );
					$slot_section = $adace_ad_slots[ $id ]['section'];
				?>
				<?php if ( $slot_section === $ad_section['slug'] ) : ?>
					<div class="postbox closed">
						<?php if ( $section['title'] ) : ?>
							<button type="button" class="handlediv button-link"><span class="toggle-indicator" aria-hidden="true"></span></button>
							<h2 class="hndle"><?php echo wp_kses_post( $section['title'] ); ?></h2>
						<?php  endif; ?>

						<div class="inside">
							<?php
								if ( $section['callback'] ) :
									call_user_func( $section['callback'], $section );
								endif;

								if ( ! isset( $wp_settings_fields ) || ! isset( $wp_settings_fields[ $page ] ) || ! isset( $wp_settings_fields[ $page ][ $section['id'] ] ) ) {
									continue;
								}
							?>

							<table class="form-table">
								<?php do_settings_fields( $page, $section['id'] ); ?>
							</table>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
	<?php
}

add_action( 'admin_menu', 'adace_add_slots_options_sections_and_fields' );
/**
 * Add options page sections, fields and options.
 */
function adace_add_slots_options_sections_and_fields() {
	// Get registered slots.
	$adace_ad_slots = adace_access_ad_slots();
	// Check if any registed.
	if ( ! empty( $adace_ad_slots ) && is_array( $adace_ad_slots ) ) {
		// Loop through to add sections, settings and fields.
		foreach ( $adace_ad_slots as $adace_ad_slot ) {
			// Add setting.
			add_settings_section(
				'adace_slot_' . $adace_ad_slot['id'] . '_section', // section id
				'<span>' . $adace_ad_slot['name'] . '</span>', // Section title.
				function () use ( $adace_ad_slot ) {
					adace_options_slots_section_renderer_callback( $adace_ad_slot );
				}, // Section renderer callback with args pass.
				'adace_slots' // Page.
			);
			// Array of fields for setting.
			$slots_fields = array(
				'ad_id'             => esc_html__( 'Ad', 'adace' ),
				'is_home'           => esc_html__( 'Display on home', 'adace' ),
				'is_singular'       => esc_html__( 'Display on singular', 'adace' ),
				'is_archive'       	=> esc_html__( 'Display on archive', 'adace' ),
				'is_user_logged_in' => esc_html__( 'Display for logged in users', 'adace' ),
				'is_amp'            => esc_html__( 'Display on AMP', 'adace' ),
				'min_width'         => esc_html__( 'Slot minimal width', 'adace' ),
				'max_width'         => esc_html__( 'Slot maximal width', 'adace' ),
				'alignment'         => esc_html__( 'Alignment', 'adace' ),
				'margin'         	=> esc_html__( 'Margin', 'adace' ),
			);
			// Allows to input custom field based on current $adace_ad_slot in loop.
			$slots_fields = apply_filters(
				'adace_options_slot_fields_filter',
				$slots_fields,
				$adace_ad_slot
			);
			foreach ( $slots_fields as $slots_field_key => $slots_field_name ) {
				add_settings_field(
					'slot_' . $slots_field_key, // Field ID.
					$slots_field_name, // Field title.
					'adace_options_slots_fields_renderer_callback', // Callback.
					'adace_slots', // Page.
					'adace_slot_' . $adace_ad_slot['id'] . '_section', // Section.
					array(
						'field_for' => $slots_field_key,
						'slot' => $adace_ad_slot,
					) // Data for callback.
				);
			}
			// Register setting.
			register_setting(
				'adace_slots', // Option group.
				'adace_slot_' . $adace_ad_slot['id'] . '_options', // Option name.
				'adace_slots_options_save_validator' // Options saving validator.
			);
		}
	}
}

/**
 * Options section renderer.
 *
 * @param array $args Slot arguments.
 */
function adace_options_slots_section_renderer_callback( $args ) {
	echo( isset( $args['desc'] ) ? '<p>' . wp_kses_post( $args['desc'] ) . '</p>' : '');
}

/**
 * Options fields renderer.
 *
 * @param array $args Field arguments.
 */
function adace_options_slots_fields_renderer_callback( $args ) {
	// Get slot options.
	$slot_options = get_option( 'adace_slot_' . $args['slot']['id'] . '_options' );
	// Action to render outside fields. For example other plugins supported fields.
	do_action( 'adace_options_slots_field_renderer_action', $args, $slot_options );
	// Switch field.
	switch ( $args['field_for'] ) {
		case 'ad_id':
			$ad_current = isset( $slot_options['ad_id'] ) ? $slot_options['ad_id'] : '';
			$ads_query_args = array(
				'posts_per_page' => -1,
				'post_type'      => 'adace-ad',
			);
			$ads_query = new WP_Query( $ads_query_args );
			$has_ads = $ads_query -> have_posts();
		?>
		<select <?php if ( ! $has_ads ) { echo esc_attr( 'hidden' );}?> id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[ad_id]" name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[ad_id]">
			<option value="" <?php selected( $ad_current, '' ); ?>><?php esc_html_e( '- Disabled -', 'adace' ); ?></option>
			<option value="-1" <?php selected( $ad_current, '-1' ); ?>><?php esc_html_e( '- Random ad -', 'adace' ); ?></option>
			<?php
			if ( $ads_query -> have_posts() ) :
				while ( $ads_query -> have_posts() ) :
					$ads_query -> the_post();
				?>
					<option value="<?php echo( esc_html( get_the_id() ) ); ?>" <?php selected( $ad_current, get_the_id() ); ?>><?php the_title(); ?></option>
				<?php
				endwhile;
				wp_reset_postdata();
			endif;
			?>
		</select>
		<?php
		if ( ! $has_ads ) : ?>
			<a href="<?php echo esc_url( admin_url( 'post-new.php?post_type=adace-ad' ) ); ?>" class="button button-secondary" href="#"><?php esc_html_e( 'Create Ad', 'adace' ); ?></a>
		<?php
		endif;
		break;
		case 'is_home':
			$is_home_editable = $args['slot']['options']['is_home_editable'];
			if ( $is_home_editable ) {
				$is_home_current = isset( $slot_options['is_home'] ) ? $slot_options['is_home'] : $args['slot']['options']['is_home'];
			} else {
				$is_home_current = $args['slot']['options']['is_home'];
			}
		?>
		<input
			type="checkbox"
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_home]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_home]"
			value="1"
			<?php checked( 1, $is_home_current );?>
			<?php echo( $is_home_editable ? '' : ' disabled' );  ?>
		/>
		<?php
		break;
		case 'is_singular':
			$is_singular_editable = $args['slot']['options']['is_singular_editable'];
			if ( $is_singular_editable ) {
				$is_singular_current = isset( $slot_options['is_singular'] ) ? $slot_options['is_singular'] : $args['slot']['options']['is_singular'];
			} else {
				$is_singular_current = $args['slot']['options']['is_singular'];
			}
			if ( 'default' === $is_singular_current ) {
				$is_singular_current = array_keys( adace_get_supported_post_types() );
			}
			$supported_post_types = adace_get_supported_post_types();
			$supported_post_types = array( 'adace-none' => esc_html__( '- None -', 'adace' ) ) + $supported_post_types;
		?>
		<select
			multiple="multiple"
			class="adace-multiple-with-none"
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_singular][]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_singular][]"
			<?php echo( $is_singular_editable ? '' : ' disabled' );  ?>
		>
			<?php foreach ( $supported_post_types as $post_type_slug => $post_type_name ) : ?>
				<option value="<?php echo( esc_html( $post_type_slug ) ); ?>" <?php echo( in_array( $post_type_slug, $is_singular_current, true ) ? 'selected="selected"' : '' ); ?>><?php echo( esc_html( $post_type_name ) ); ?></option>
			<?php endforeach; ?>
		</select>
		<?php
		break;
		case 'is_archive':
			$is_archive_editable = $args['slot']['options']['is_archive_editable'];
			if ( $is_archive_editable ) {
				$is_archive_current = isset( $slot_options['is_archive'] ) ? $slot_options['is_archive'] : $args['slot']['options']['is_archive'];
			} else {
				$is_archive_current = $args['slot']['options']['is_archive'];
			}
			if ( 'default' === $is_archive_current ) {
				$is_archive_current = array_keys( adace_get_supported_taxonomies() );
			}
			$supported_taxonomies = adace_get_supported_taxonomies();
			$supported_taxonomies = array( 'adace-none' => esc_html__( '- None -', 'adace' ) ) + $supported_taxonomies;
		?>
		<select
			multiple="multiple"
			class="adace-multiple-with-none"
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_archive][]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_archive][]"
			<?php echo( $is_archive_editable ? '' : ' disabled' );  ?>
		>
			<?php foreach ( $supported_taxonomies as $taxonomy_slug => $taxonomy_name ) : ?>
				<option value="<?php echo( esc_html( $taxonomy_slug ) ); ?>" <?php echo( in_array( $taxonomy_slug, $is_archive_current, true ) ? 'selected="selected"' : '' ); ?>><?php echo( esc_html( $taxonomy_name ) ); ?></option>
			<?php endforeach; ?>
		</select>
		<?php
		break;
		case 'is_user_logged_in':
			$is_home_editable = $args['slot']['options']['is_user_logged_in_editable'];
			if ( $is_home_editable ) {
				$is_home_current = isset( $slot_options['is_user_logged_in'] ) ? $slot_options['is_user_logged_in'] : $args['slot']['options']['is_user_logged_in'];
			} else {
				$is_home_current = $args['slot']['options']['is_user_logged_in'];
			}
		?>
		<input
			type="checkbox"
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_user_logged_in]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_user_logged_in]"
			value="1"
			<?php checked( 1, $is_home_current );?>
			<?php echo( $is_home_editable ? '' : ' disabled' );  ?>
		/>
		<?php
		break;
		case 'is_amp':
			$is_amp_editable = $args['slot']['options']['is_amp_editable'];
			if ( $is_amp_editable ) {
				$is_amp_current = isset( $slot_options['is_amp'] ) ? $slot_options['is_amp'] : $args['slot']['options']['is_amp'];
			} else {
				$is_amp_current = $args['slot']['options']['is_amp'];
			}
		?>
		<input
			type="checkbox"
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_amp]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[is_amp]"
			value="1"
			<?php checked( 1, $is_amp_current );?>
			<?php echo( $is_amp_editable ? '' : ' disabled' );  ?>
		/>
		<?php
		break;
		case 'min_width':
			$min_width_editable = $args['slot']['options']['min_width_editable'];
			if ( $min_width_editable ) {
				$min_width_current = isset( $slot_options['min_width'] ) ? $slot_options['min_width'] : $args['slot']['options']['min_width'];
			} else {
				$min_width_current = $args['slot']['options']['min_width'];
			}
		?>
		<input
			class="small-text"
			type="number"
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[min_width]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[min_width]"
			min="0"
			max="10000"
			step="1"
			value="<?php echo( esc_html( $min_width_current ) ); ?>"
			<?php echo( $min_width_editable ? '' : ' disabled' );  ?>
		/>
		<label for="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[min_width]"><?php esc_html_e( 'px', 'adace' ); ?></label>
		<?php
		break;
		case 'max_width':
			$max_width_editable = $args['slot']['options']['max_width_editable'];
			if ( $max_width_editable ) {
				$max_width_current = isset( $slot_options['max_width'] ) ? $slot_options['max_width'] : $args['slot']['options']['max_width'];
			} else {
				$max_width_current = $args['slot']['options']['max_width'];
			}
		?>
		<input
			class="small-text"
			type="number"
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[max_width]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[max_width]"
			min="0"
			max="10000"
			step="1"
			value="<?php echo( esc_html( $max_width_current ) ); ?>"
			<?php echo( $max_width_editable ? '' : ' disabled' );  ?>
		/>
		<label for="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[max_width]"><?php esc_html_e( 'px', 'adace' ); ?></label>
		<?php
		break;
		case 'alignment':
			$alignment_editable = $args['slot']['options']['alignment_editable'];
			if ( $alignment_editable ) {
				$alignment_current = isset( $slot_options['alignment'] ) ? $slot_options['alignment'] : $args['slot']['options']['alignment'];
			} else {
				$alignment_current = $args['slot']['options']['alignment'];
			}
		?>
		<select
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[alignment]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[alignment]"
			<?php echo( $alignment_editable ? '' : ' disabled' );  ?>
		>
			<option value="none" <?php selected( $alignment_current, 'none' ); ?>><?php esc_html_e( 'None', 'adace' ); ?></option>
			<option value="left" <?php selected( $alignment_current, 'left' ); ?>><?php esc_html_e( 'Left', 'adace' ); ?></option>
			<option value="center" <?php selected( $alignment_current, 'center' ); ?>><?php esc_html_e( 'Center', 'adace' ); ?></option>
			<option value="right" <?php selected( $alignment_current, 'right' ); ?>><?php esc_html_e( 'Right', 'adace' ); ?></option>
		</select>
		<?php
		break;
		case 'margin':
			$margin_editable = $args['slot']['options']['margin_editable'];
			if ( $margin_editable ) {
				$margin_current = isset( $slot_options['margin'] ) ? $slot_options['margin'] : $args['slot']['options']['margin'];
			} else {
				$margin_current = $args['slot']['options']['margin'];
			}
		?>
		<input
			class="small-text"
			type="number"
			id="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[margin]"
			name="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[margin]"
			min="0"
			max="10000"
			step="1"
			value="<?php echo( esc_html( $margin_current ) ); ?>"
			<?php echo( $margin_editable ? '' : ' disabled' );  ?>
		/>
		<label for="<?php echo( 'adace_slot_' . esc_attr( $args['slot']['id'] ) . '_options' ); ?>[margin]"><?php esc_html_e( 'px', 'adace' ); ?></label>
		<?php
	}
}

/**
 * Options validator.
 *
 * @param array $input Saved options.
 * @return array Sanitised options for save.
 */
function adace_slots_options_save_validator( $input ) {
	$defaults = adace_get_slot_default_args();
	$input_sanitized = array();
	if ( isset( $input['is_home'] ) ) {
		$input_sanitized['is_home'] = filter_var( $input['is_home'], FILTER_VALIDATE_BOOLEAN );
	} else {
		$input_sanitized['is_home'] = false;
	}
	if ( isset( $input['is_singular'] ) ) {
		$input_sanitized['is_singular'] = filter_var_array( $input['is_singular'], FILTER_SANITIZE_STRING );
	}
	if ( isset( $input['is_archive'] ) ) {
		$input_sanitized['is_archive'] = filter_var_array( $input['is_archive'], FILTER_SANITIZE_STRING );
	}
	if ( isset( $input['is_user_logged_in'] ) ) {
		$input_sanitized['is_user_logged_in'] = filter_var( $input['is_user_logged_in'], FILTER_VALIDATE_BOOLEAN );
	} else {
		$input_sanitized['is_user_logged_in'] = false;
	}
	if ( isset( $input['is_amp'] ) ) {
		$input_sanitized['is_amp'] = filter_var( $input['is_amp'], FILTER_VALIDATE_BOOLEAN );
	} else {
		$input_sanitized['is_amp'] = false;
	}
	if ( isset( $input['min_width'] ) ) {
		$input_sanitized['min_width'] = intval( filter_var( $input['min_width'], FILTER_SANITIZE_NUMBER_INT ) );
	}
	if ( isset( $input['max_width'] ) ) {
		$input_sanitized['max_width'] = intval( filter_var( $input['max_width'], FILTER_SANITIZE_NUMBER_INT ) );
	}
	if ( isset( $input['alignment'] ) ) {
		$input_sanitized['alignment'] = filter_var( $input['alignment'], FILTER_SANITIZE_STRING );
	}
	if ( isset( $input['margin'] ) ) {
		$input_sanitized['margin'] = filter_var( $input['margin'], FILTER_SANITIZE_NUMBER_INT );
	}
	// Parse args.
	$input_sanitized = wp_parse_args( $input_sanitized, $defaults['options'] );
	// ad_id is not in defaults, so its added after parsing.
	if ( isset( $input['ad_id'] ) ) {
		$input_sanitized['ad_id'] = filter_var( $input['ad_id'], FILTER_SANITIZE_NUMBER_INT );
	}
	return apply_filters(
		'adace_slots_options_save_validator_filter',
		$input_sanitized, $input
	);
}
