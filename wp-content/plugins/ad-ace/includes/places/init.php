<?php
/**
 * Init Sponsor
 *
 * @package AdAce.
 * @subpackage Sponsors.
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

// Load places common parts.
require_once( trailingslashit( adace_get_plugin_dir() ) . 'includes/places/common/register.php' );
require_once( trailingslashit( adace_get_plugin_dir() ) . 'includes/places/common/class-adace-places-widget.php' );

// Load places backend parts.
if ( is_admin() ) {
	require_once( trailingslashit( adace_get_plugin_dir() ) . 'includes/places/admin/meta-boxes.php' );
}

// Load places front parts.
if ( ! is_admin() ) {
	require_once( trailingslashit( adace_get_plugin_dir() ) . 'includes/places/front/get-places.php' );
	require_once( trailingslashit( adace_get_plugin_dir() ) . 'includes/places/front/places-shortcode.php' );
}
