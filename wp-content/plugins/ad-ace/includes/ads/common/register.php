<?php
/**
 * Register Ads Post Type.
 *
 * @package AdAce.
 * @subpackage Ads.
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}


add_action( 'init', 'adace_register_post_type_ad' );
/**
 * Register ad post type.
 */
function adace_register_post_type_ad() {
	// Labels for post type.
	$labels = array(
		'name'               => esc_html_x( 'Ads', 'post type general name', 'adace' ),
		'singular_name'      => esc_html_x( 'Ad', 'post type singular name', 'adace' ),
		'menu_name'          => esc_html_x( 'Ads', 'admin menu', 'adace' ),
		'name_admin_bar'     => esc_html_x( 'Ad', 'add new on admin bar', 'adace' ),
		'add_new'            => esc_html_x( 'Add New', 'book', 'adace' ),
		'add_new_item'       => esc_html__( 'Add New Ad', 'adace' ),
		'new_item'           => esc_html__( 'New Ad', 'adace' ),
		'edit_item'          => esc_html__( 'Edit Ad', 'adace' ),
		'view_item'          => esc_html__( 'View Ad', 'adace' ),
		'all_items'          => esc_html__( 'All Ad\'s', 'adace' ),
		'search_items'       => esc_html__( 'Search Ad\'s', 'adace' ),
		'parent_item_colon'  => esc_html__( 'Parent Ad\'s:', 'adace' ),
		'not_found'          => esc_html__( 'No ad\'s found.', 'adace' ),
		'not_found_in_trash' => esc_html__( 'No ad\'s found in Trash.', 'adace' ),
	);
	// Args for post type.
	$args = array(
		'labels'             => $labels,
		'menu_icon'          => 'dashicons-schedule',
		'description'        => esc_html__( 'Description of these amazing ad\'s.', 'adace' ),
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array(
			'slug' => 'adace-ad',
		),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title' ),
	);
	// Register.
	register_post_type( 'adace-ad', apply_filters( 'adace_register_post_type_ad_filter', $args ) );
}
