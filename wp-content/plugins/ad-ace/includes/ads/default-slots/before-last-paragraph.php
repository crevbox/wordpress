<?php
/**
 * Before Last Paragraph
 *
 * @package AdAce
 * @subpackage Default Slots
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Get before_last X Paragraph Slot Id
 *
 * @return string
 */
function adace_get_before_last_paragraph_slot_id() {
	return 'adace-before-last-paragraph';
}

/**
 * Register before_last X Paragraph Slot
 */
adace_register_ad_slot(
	array(
		'id' => adace_get_before_last_paragraph_slot_id(),
		'name' => esc_html__( 'Before the last paragraph', 'adace' ),
		'section' => 'content',
	)
);

add_filter( 'the_content', 'adace_before_last_paragraph_slot_inject' );
/**
 * Before Last Paragraph Slot into content
 *
 * @param string $content Post content.
 * @return string
 */
function adace_before_last_paragraph_slot_inject( $content ) {
	if ( false === adace_is_ad_slot( adace_get_before_last_paragraph_slot_id() ) ) { return $content; }

	$unique = adace_preg_make_unique( '/<p.*p>/U', $content );
	$content = $unique['string'];
	preg_match_all( '/<!--UNIQUEMATCH.*-->/U', $content, $paragraphs );
	if ( count( $paragraphs[0] ) > 1 ) {
		$last_p = array_pop( $paragraphs[0] );
		$new_last_p = adace_get_ad_slot( adace_get_before_last_paragraph_slot_id() ) . $last_p;
		$content = adace_str_replace_first( $last_p, $new_last_p, $content );
	}
	$unique['string'] = $content;
	$content = adace_preg_make_unique_revert( $unique );
	return $content;
}
