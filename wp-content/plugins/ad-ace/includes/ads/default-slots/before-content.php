<?php
/**
 * Before Content
 *
 * @package AdAce
 * @subpackage Default Slots
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Get Before Content Slot Id
 *
 * @return string
 */
function adace_get_before_content_slot_id() {
	return 'adace-before-content';
}

/**
* Register Before Content Slot
*/
adace_register_ad_slot(
	array(
		'id' => adace_get_before_content_slot_id(),
		'name' => esc_html__( 'Before Content', 'adace' ),
		'section' => 'content',
	)
);

add_filter( 'the_content', 'adace_before_content_slot_inject' );
/**
 * Inject Before Content Slot into content
 *
 * @param string $content Post content.
 */
function adace_before_content_slot_inject( $content ) {
	$content_with_inject = adace_get_ad_slot( adace_get_before_content_slot_id() );
	$content_with_inject .= $content;
	return $content_with_inject;
}
