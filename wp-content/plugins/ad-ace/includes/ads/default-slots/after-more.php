<?php
/**
 * After More Tag
 *
 * @package AdAce
 * @subpackage Default Slots
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Get before_last X Paragraph Slot Id
 *
 * @return string
 */
function adace_get_after_more_slot_id() {
	return 'adace-after-more';
}

/**
 * Register before_last X Paragraph Slot
 */
adace_register_ad_slot(
	array(
		'id' => adace_get_after_more_slot_id(),
		'name' => esc_html__( 'After the <!--more--> tag', 'adace' ),
		'section' => 'content',
	)
);

add_filter( 'the_content', 'adace_after_more_slot_inject' );
/**
 * Before Last Paragraph Slot into content
 *
 * @param string $content Post content.
 * @return string
 */
function adace_after_more_slot_inject( $content ) {
	if ( false === adace_is_ad_slot( adace_get_after_more_slot_id() ) ) { return $content; }

	$more_tag = '<span id="more-' . get_the_ID() . '"></span>';
	$ad = adace_get_ad_slot( adace_get_after_more_slot_id() );
	$content = adace_str_replace_first( $more_tag, $ad . $more_tag, $content );

	return $content;
}
