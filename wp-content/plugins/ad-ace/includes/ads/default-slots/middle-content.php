<?php
/**
 * Middle Content
 *
 * @package AdAce
 * @subpackage Default Slots
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Get before_last X Paragraph Slot Id
 *
 * @return string
 */
function adace_get_middle_content_slot_id() {
	return 'adace-middle-content';
}

/**
 * Register before_last X Paragraph Slot
 */
adace_register_ad_slot(
	array(
		'id' => adace_get_middle_content_slot_id(),
		'name' => esc_html__( 'In the middle of the content', 'adace' ),
		'section' => 'content',
	)
);

add_filter( 'the_content', 'adace_middle_content_slot_inject' );
/**
 * Middle Content Slot into content
 *
 * @param string $content Post content.
 * @return string
 */
function adace_middle_content_slot_inject( $content ) {
	if ( false === adace_is_ad_slot( adace_get_middle_content_slot_id() ) ) { return $content; }
	preg_match_all( "/<p.*p>/", $content, $paragraphs );
	$count = count( $paragraphs[0] );
	if ( $count > 1 ) {
		$middle_pos = floor( $count / 2 );
		$middle_p = $paragraphs[0][ $middle_pos ];
		$new_middle_p = $middle_p . adace_get_ad_slot( adace_get_middle_content_slot_id() );
		$content = str_replace( $middle_p, $new_middle_p, $content );
	}

	return $content;
}
