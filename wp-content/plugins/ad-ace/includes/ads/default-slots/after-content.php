<?php
/**
 * After Content
 *
 * @package AdAce
 * @subpackage Default Slots
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Get After Content Slot Id
 *
 * @return string
 */
function adace_get_after_content_slot_id() {
	return 'adace-after-content';
}

/**
* Register After Content Slot
*/
adace_register_ad_slot(
	array(
		'id'   => adace_get_after_content_slot_id(),
		'name' => esc_html__( 'After Content', 'adace' ),
		'section' => 'content',
	)
);

add_filter( 'the_content', 'adace_after_content_slot_inject' );
/**
 * Inject Before Content Slot into content
 *
 * @param string $content Post content.
 */
function adace_after_content_slot_inject( $content ) {
	$content_with_inject = $content;
	$content_with_inject .= adace_get_ad_slot( adace_get_after_content_slot_id() );
	return $content_with_inject;
}
