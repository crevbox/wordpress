<?php
/**
 * Facebook share link
 *
 * @package snax 1.11
 * @subpackage Share
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

global $snax_share_args;
?>
<script type="text/javascript">
	(function () {
		var triggerOnLoad = false;

		window.quizzardShareOnFB = function() {
			jQuery('body').trigger('snaxFbNotLoaded');
			triggerOnLoad = true;
		};

		var _fbAsyncInit = window.fbAsyncInit;

		window.fbAsyncInit = function() {
			FB.init({
				appId      : '<?php echo esc_attr( snax_get_facebook_app_id() ); ?>',
				xfbml      : true,
				version    : 'v2.5'
			});

			window.quizzardShareOnFB = function() {
				var quiz 			= jQuery('.snax_quiz').data('quizzardQuiz');
				var quizTitle 		= '<?php echo esc_html( $snax_share_args['title'] ); ?>';
				var quizDescription	= '<?php echo esc_html( $snax_share_args['description'] ); ?>';

				FB.login(function(response) {
					if (response.status === 'connected') {
						FB.ui({
								method: 'share_open_graph',
								action_type: 'og.shares',
								action_properties: JSON.stringify({
									object : {
										'og:url':           '<?php echo esc_url( $snax_share_args['url'] ); ?>', // Url to share.
										'og:title':         quizTitle,
										'og:description':   quizDescription,
										'og:image':         '<?php echo esc_url( $snax_share_args['thumb'] ); ?>'
									}
								})
							},
							// callback
							function(response) {
								var validShare = response.post_id ? true : false;
//
								quiz.unlock(validShare);
							});
					}
				}, {
					// Beginning October 12, 2016, post_id requires your app to have publish_actions granted,
					// and for the user to share to their timeline or a group.
					scope: 'publish_actions'
				});
			};

			// Fire original callback.
			if (typeof _fbAsyncInit === 'function') {
				_fbAsyncInit();
			}

			// Open share popup as soon as possible, after loading FB SDK.
			if (triggerOnLoad) {
				setTimeout(function() {
					quizzardShareOnFB();
				}, 1000);
			}
		};

		// JS SDK loaded before we hook into it. Trigger callback now.
		if (typeof window.FB !== 'undefined') {
			window.fbAsyncInit();
		}
	})();
</script>

<a class="quizzard-share quizzard-share-facebook" onclick="quizzardShareOnFB(); return false;" href="#" title="<?php esc_attr_e( 'Share on Facebook', 'snax' ); ?>" target="_blank" rel="nofollow">
	<?php esc_html_e( 'Share on Facebook', 'snax' ); ?>
</a>
