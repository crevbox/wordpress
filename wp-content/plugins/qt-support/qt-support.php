<?php 

/**
 * Plugin Name: QantumThemes Support And Manual
 * Description: Get the manual directly in your Wordpress so every knowledge is always with you!
 * Version: 1.0
 * Author: QantumThemes
 */



/**
 * Adds the Manual & Support page to the WordPress admin area
 */

add_action( 'admin_menu', 'qt_manual_admin_page' );
if(!function_exists('qt_manual_admin_page')){
function qt_manual_admin_page() {
    add_theme_page(
    	esc_attr__("QantumThemes Manual & Support"),
    	esc_attr__("Manual & Support"),
    	'edit_theme_options',
    	'manual-and-support',
    	'qt_manual_page'
    );
    
}}





/**
 * Adds the Manual & Support page to the WordPress admin area
 */

add_action( 'admin_enqueue_scripts', 'qt_manual_styles' );
if(!function_exists('qt_manual_styles')){
function qt_manual_styles() {
   	wp_enqueue_style( '_s-main-style', get_template_directory_uri()."/css/qticons.css" );

    
}}



/**
 * Function that creates the manual page
 */

if(!function_exists('qt_manual_page')){
	function qt_manual_page(){
		?>

		<style>
		.glyphs.character-mapping{margin:0 0 20px 0;padding:20px 0 20px 30px;color:rgba(0,0,0,0.5);border:1px solid #d8e0e5;-webkit-border-radius:3px;border-radius:3px;}
		.glyphs.character-mapping li{margin:0 30px 20px 0;display:inline-block;width:90px}
		.glyphs.character-mapping .icon{margin:10px 0 10px 15px;padding:15px;position:relative;width:55px;height:55px;color:#162a36 !important;overflow:hidden;-webkit-border-radius:3px;border-radius:3px;font-size:32px;}
		.glyphs.character-mapping .icon svg{fill:#000}
		.glyphs.character-mapping input{margin:0;padding:5px 0;line-height:12px;font-size:12px;display:block;width:100%;border:1px solid #d8e0e5;-webkit-border-radius:5px;border-radius:5px;text-align:center;outline:0;}
		.glyphs.character-mapping input:focus{border:1px solid #fbde4a;-webkit-box-shadow:inset 0 0 3px #fbde4a;box-shadow:inset 0 0 3px #fbde4a}
		.glyphs.character-mapping input:hover{-webkit-box-shadow:inset 0 0 3px #fbde4a;box-shadow:inset 0 0 3px #fbde4a}
		.glyphs.css-mapping{margin:0 0 60px 0;padding:30px 0 20px 30px;color:rgba(0,0,0,0.5);border:1px solid #d8e0e5;-webkit-border-radius:3px;border-radius:3px;}
		.glyphs.css-mapping li{margin:0 30px 20px 0;padding:0;display:inline-block;overflow:hidden}
		.glyphs.css-mapping .icon{margin:0;margin-right:10px;padding:13px;height:50px;width:50px;color:#162a36 !important;overflow:hidden;float:left;font-size:32px}
		.glyphs.css-mapping input{margin:0;margin-top:5px;padding:8px;line-height:16px;font-size:16px;display:block;width:150px;height:40px;border:1px solid #d8e0e5;-webkit-border-radius:5px;border-radius:5px;background:#fff;outline:0;float:right;}
		.glyphs.css-mapping input:focus{border:1px solid #fbde4a;-webkit-box-shadow:inset 0 0 3px #fbde4a;box-shadow:inset 0 0 3px #fbde4a}
		.glyphs.css-mapping input:hover{-webkit-box-shadow:inset 0 0 3px #fbde4a;box-shadow:inset 0 0 3px #fbde4a}

		</style>
		<h1>QantumThemes Manual and Support</h1>
		<hr>
		<h3>Manual</h3>
		<p>Please check the fresh version of the manual in out HelpDesk to get the latest updates and info about this theme</p>
		<a class="button button-primary" href="http://www.qantumthemes.com/manuals/vice/" target="_blank">Manual</a>
		<hr>
		<h3>Support</h3>
		<p>Please write in our support forum to receive fast support in 24 hours</p>
		<a class="button button-primary" href="http://www.qantumthemes.com/helpdesk/forums/forum/vice-music-dj-and-music-band-wordpress-theme/" target="_blank">Support Forum</a>



		<hr>
		<h3>Icons</h3>
		<p>
		To use the icons in the menu, enable the "class" in the screen options, and add the icon class to the menu item.
		Compatible only with the secondary top menu.
		</p>

		<p>
		To use the icons in the content, use the code below, replacing "qticon-whatever" with your required icon class from the list below:

		</p>
<pre>
&lt;i class="icon qticon-whatever"&gt; &lt;/i&gt;
</pre>

<ul class="glyphs css-mapping">
        <li>
          <div class="icon qticon-amazon"></div>
          <input type="text" readonly="readonly" value="qticon-amazon">
        </li>
        <li>
          <div class="icon qticon-beatport"></div>
          <input type="text" readonly="readonly" value="qticon-beatport">
        </li>
        <li>
          <div class="icon qticon-cd"></div>
          <input type="text" readonly="readonly" value="qticon-cd">
        </li>
        <li>
          <div class="icon qticon-chevron-right"></div>
          <input type="text" readonly="readonly" value="qticon-chevron-right">
        </li>
        <li>
          <div class="icon qticon-eq"></div>
          <input type="text" readonly="readonly" value="qticon-eq">
        </li>
        <li>
          <div class="icon qticon-googleplus"></div>
          <input type="text" readonly="readonly" value="qticon-googleplus">
        </li>
        <li>
          <div class="icon qticon-man"></div>
          <input type="text" readonly="readonly" value="qticon-man">
        </li>
        <li>
          <div class="icon qticon-phone"></div>
          <input type="text" readonly="readonly" value="qticon-phone">
        </li>
        <li>
          <div class="icon qticon-resident-advisor"></div>
          <input type="text" readonly="readonly" value="qticon-resident-advisor">
        </li>
        <li>
          <div class="icon qticon-space"></div>
          <input type="text" readonly="readonly" value="qticon-space">
        </li>
        <li>
          <div class="icon qticon-whatpeopleplay"></div>
          <input type="text" readonly="readonly" value="qticon-whatpeopleplay">
        </li>
        <li>
          <div class="icon qticon-wordpress"></div>
          <input type="text" readonly="readonly" value="qticon-wordpress">
        </li>
        <li>
          <div class="icon qticon-yahoo"></div>
          <input type="text" readonly="readonly" value="qticon-yahoo">
        </li>
        <li>
          <div class="icon qticon-youtube"></div>
          <input type="text" readonly="readonly" value="qticon-youtube">
        </li>
        <li>
          <div class="icon qticon-zoom-out"></div>
          <input type="text" readonly="readonly" value="qticon-zoom-out">
        </li>
        <li>
          <div class="icon qticon-cart"></div>
          <input type="text" readonly="readonly" value="qticon-cart">
        </li>
        <li>
          <div class="icon qticon-cassette"></div>
          <input type="text" readonly="readonly" value="qticon-cassette">
        </li>
        <li>
          <div class="icon qticon-download"></div>
          <input type="text" readonly="readonly" value="qticon-download">
        </li>
        <li>
          <div class="icon qticon-event"></div>
          <input type="text" readonly="readonly" value="qticon-event">
        </li>
        <li>
          <div class="icon qticon-headphones"></div>
          <input type="text" readonly="readonly" value="qticon-headphones">
        </li>
        <li>
          <div class="icon qticon-map"></div>
          <input type="text" readonly="readonly" value="qticon-map">
        </li>
        <li>
          <div class="icon qticon-photobucket"></div>
          <input type="text" readonly="readonly" value="qticon-photobucket">
        </li>
        <li>
          <div class="icon qticon-reverbnation"></div>
          <input type="text" readonly="readonly" value="qticon-reverbnation">
        </li>
        <li>
          <div class="icon qticon-star"></div>
          <input type="text" readonly="readonly" value="qticon-star">
        </li>
        <li>
          <div class="icon qticon-stop"></div>
          <input type="text" readonly="readonly" value="qticon-stop">
        </li>
        <li>
          <div class="icon qticon-th"></div>
          <input type="text" readonly="readonly" value="qticon-th">
        </li>
        <li>
          <div class="icon qticon-th-large"></div>
          <input type="text" readonly="readonly" value="qticon-th-large">
        </li>
        <li>
          <div class="icon qticon-thunder"></div>
          <input type="text" readonly="readonly" value="qticon-thunder">
        </li>
        <li>
          <div class="icon qticon-zoom-in"></div>
          <input type="text" readonly="readonly" value="qticon-zoom-in">
        </li>
        <li>
          <div class="icon qticon-torso"></div>
          <input type="text" readonly="readonly" value="qticon-torso">
        </li>
        <li>
          <div class="icon qticon-triplevision"></div>
          <input type="text" readonly="readonly" value="qticon-triplevision">
        </li>
        <li>
          <div class="icon qticon-tumblr"></div>
          <input type="text" readonly="readonly" value="qticon-tumblr">
        </li>
        <li>
          <div class="icon qticon-twitter"></div>
          <input type="text" readonly="readonly" value="qticon-twitter">
        </li>
        <li>
          <div class="icon qticon-upload"></div>
          <input type="text" readonly="readonly" value="qticon-upload">
        </li>
        <li>
          <div class="icon qticon-vimeo"></div>
          <input type="text" readonly="readonly" value="qticon-vimeo">
        </li>
        <li>
          <div class="icon qticon-volume"></div>
          <input type="text" readonly="readonly" value="qticon-volume">
        </li>
        <li>
          <div class="icon qticon-soundcloud"></div>
          <input type="text" readonly="readonly" value="qticon-soundcloud">
        </li>
        <li>
          <div class="icon qticon-sound"></div>
          <input type="text" readonly="readonly" value="qticon-sound">
        </li>
        <li>
          <div class="icon qticon-skype"></div>
          <input type="text" readonly="readonly" value="qticon-skype">
        </li>
        <li>
          <div class="icon qticon-skip-forward"></div>
          <input type="text" readonly="readonly" value="qticon-skip-forward">
        </li>
        <li>
          <div class="icon qticon-skip-fast-forward"></div>
          <input type="text" readonly="readonly" value="qticon-skip-fast-forward">
        </li>
        <li>
          <div class="icon qticon-skip-fast-backward"></div>
          <input type="text" readonly="readonly" value="qticon-skip-fast-backward">
        </li>
        <li>
          <div class="icon qticon-skip-backward"></div>
          <input type="text" readonly="readonly" value="qticon-skip-backward">
        </li>
        <li>
          <div class="icon qticon-share"></div>
          <input type="text" readonly="readonly" value="qticon-share">
        </li>
        <li>
          <div class="icon qticon-search"></div>
          <input type="text" readonly="readonly" value="qticon-search">
        </li>
        <li>
          <div class="icon qticon-rss"></div>
          <input type="text" readonly="readonly" value="qticon-rss">
        </li>
        <li>
          <div class="icon qticon-rewind"></div>
          <input type="text" readonly="readonly" value="qticon-rewind">
        </li>
        <li>
          <div class="icon qticon-pinterest"></div>
          <input type="text" readonly="readonly" value="qticon-pinterest">
        </li>
        <li>
          <div class="icon qticon-menu"></div>
          <input type="text" readonly="readonly" value="qticon-menu">
        </li>
        <li>
          <div class="icon qticon-heart"></div>
          <input type="text" readonly="readonly" value="qticon-heart">
        </li>
        <li>
          <div class="icon qticon-exclamationmark"></div>
          <input type="text" readonly="readonly" value="qticon-exclamationmark">
        </li>
        <li>
          <div class="icon qticon-close"></div>
          <input type="text" readonly="readonly" value="qticon-close">
        </li>
        <li>
          <div class="icon qticon-chevron-up"></div>
          <input type="text" readonly="readonly" value="qticon-chevron-up">
        </li>
        <li>
          <div class="icon qticon-cd-note"></div>
          <input type="text" readonly="readonly" value="qticon-cd-note">
        </li>
        <li>
          <div class="icon qticon-bebo"></div>
          <input type="text" readonly="readonly" value="qticon-bebo">
        </li>
        <li>
          <div class="icon qticon-arrow-down"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-down">
        </li>
        <li>
          <div class="icon qticon-arrow-expand"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-expand">
        </li>
        <li>
          <div class="icon qticon-behance"></div>
          <input type="text" readonly="readonly" value="qticon-behance">
        </li>
        <li>
          <div class="icon qticon-chat-bubble"></div>
          <input type="text" readonly="readonly" value="qticon-chat-bubble">
        </li>
        <li>
          <div class="icon qticon-arrow-left"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-left">
        </li>
        <li>
          <div class="icon qticon-blogger"></div>
          <input type="text" readonly="readonly" value="qticon-blogger">
        </li>
        <li>
          <div class="icon qticon-chat-bubbles"></div>
          <input type="text" readonly="readonly" value="qticon-chat-bubbles">
        </li>
        <li>
          <div class="icon qticon-close-sign"></div>
          <input type="text" readonly="readonly" value="qticon-close-sign">
        </li>
        <li>
          <div class="icon qticon-exclamationmark-sign"></div>
          <input type="text" readonly="readonly" value="qticon-exclamationmark-sign">
        </li>
        <li>
          <div class="icon qticon-help-buoy"></div>
          <input type="text" readonly="readonly" value="qticon-help-buoy">
        </li>
        <li>
          <div class="icon qticon-minus"></div>
          <input type="text" readonly="readonly" value="qticon-minus">
        </li>
        <li>
          <div class="icon qticon-plane"></div>
          <input type="text" readonly="readonly" value="qticon-plane">
        </li>
        <li>
          <div class="icon qticon-play"></div>
          <input type="text" readonly="readonly" value="qticon-play">
        </li>
        <li>
          <div class="icon qticon-plus"></div>
          <input type="text" readonly="readonly" value="qticon-plus">
        </li>
        <li>
          <div class="icon qticon-plus-sign"></div>
          <input type="text" readonly="readonly" value="qticon-plus-sign">
        </li>
        <li>
          <div class="icon qticon-power"></div>
          <input type="text" readonly="readonly" value="qticon-power">
        </li>
        <li>
          <div class="icon qticon-questionmark"></div>
          <input type="text" readonly="readonly" value="qticon-questionmark">
        </li>
        <li>
          <div class="icon qticon-questionmark-sign"></div>
          <input type="text" readonly="readonly" value="qticon-questionmark-sign">
        </li>
        <li>
          <div class="icon qticon-quote"></div>
          <input type="text" readonly="readonly" value="qticon-quote">
        </li>
        <li>
          <div class="icon qticon-record"></div>
          <input type="text" readonly="readonly" value="qticon-record">
        </li>
        <li>
          <div class="icon qticon-replay"></div>
          <input type="text" readonly="readonly" value="qticon-replay">
        </li>
        <li>
          <div class="icon qticon-pencil"></div>
          <input type="text" readonly="readonly" value="qticon-pencil">
        </li>
        <li>
          <div class="icon qticon-magnet"></div>
          <input type="text" readonly="readonly" value="qticon-magnet">
        </li>
        <li>
          <div class="icon qticon-google"></div>
          <input type="text" readonly="readonly" value="qticon-google">
        </li>
        <li>
          <div class="icon qticon-empty"></div>
          <input type="text" readonly="readonly" value="qticon-empty">
        </li>
        <li>
          <div class="icon qticon-chevron-light-up"></div>
          <input type="text" readonly="readonly" value="qticon-chevron-light-up">
        </li>
        <li>
          <div class="icon qticon-at-sign"></div>
          <input type="text" readonly="readonly" value="qticon-at-sign">
        </li>
        <li>
          <div class="icon qticon-at"></div>
          <input type="text" readonly="readonly" value="qticon-at">
        </li>
        <li>
          <div class="icon qticon-arrow-up"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-up">
        </li>
        <li>
          <div class="icon qticon-arrow-shrink"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-shrink">
        </li>
        <li>
          <div class="icon qticon-arrow-right"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-right">
        </li>
        <li>
          <div class="icon qticon-arrow-move-up"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-move-up">
        </li>
        <li>
          <div class="icon qticon-arrow-move-right"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-move-right">
        </li>
        <li>
          <div class="icon qticon-arrow-move-left"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-move-left">
        </li>
        <li>
          <div class="icon qticon-arrow-move-down"></div>
          <input type="text" readonly="readonly" value="qticon-arrow-move-down">
        </li>
        <li>
          <div class="icon qticon-briefcase"></div>
          <input type="text" readonly="readonly" value="qticon-briefcase">
        </li>
        <li>
          <div class="icon qticon-chat-bubbles-outline"></div>
          <input type="text" readonly="readonly" value="qticon-chat-bubbles-outline">
        </li>
        <li>
          <div class="icon qticon-comment"></div>
          <input type="text" readonly="readonly" value="qticon-comment">
        </li>
        <li>
          <div class="icon qticon-facebook"></div>
          <input type="text" readonly="readonly" value="qticon-facebook">
        </li>
        <li>
          <div class="icon qticon-home"></div>
          <input type="text" readonly="readonly" value="qticon-home">
        </li>
        <li>
          <div class="icon qticon-minus-sign"></div>
          <input type="text" readonly="readonly" value="qticon-minus-sign">
        </li>
        <li>
          <div class="icon qticon-mixcloud"></div>
          <input type="text" readonly="readonly" value="qticon-mixcloud">
        </li>
        <li>
          <div class="icon qticon-music"></div>
          <input type="text" readonly="readonly" value="qticon-music">
        </li>
        <li>
          <div class="icon qticon-mute"></div>
          <input type="text" readonly="readonly" value="qticon-mute">
        </li>
        <li>
          <div class="icon qticon-navigate"></div>
          <input type="text" readonly="readonly" value="qticon-navigate">
        </li>
        <li>
          <div class="icon qticon-news"></div>
          <input type="text" readonly="readonly" value="qticon-news">
        </li>
        <li>
          <div class="icon qticon-pause"></div>
          <input type="text" readonly="readonly" value="qticon-pause">
        </li>
        <li>
          <div class="icon qticon-paypal"></div>
          <input type="text" readonly="readonly" value="qticon-paypal">
        </li>
        <li>
          <div class="icon qticon-loop"></div>
          <input type="text" readonly="readonly" value="qticon-loop">
        </li>
        <li>
          <div class="icon qticon-gear"></div>
          <input type="text" readonly="readonly" value="qticon-gear">
        </li>
        <li>
          <div class="icon qticon-eject"></div>
          <input type="text" readonly="readonly" value="qticon-eject">
        </li>
        <li>
          <div class="icon qticon-chevron-light-right"></div>
          <input type="text" readonly="readonly" value="qticon-chevron-light-right">
        </li>
        <li>
          <div class="icon qticon-caret-up"></div>
          <input type="text" readonly="readonly" value="qticon-caret-up">
        </li>
        <li>
          <div class="icon qticon-caret-right"></div>
          <input type="text" readonly="readonly" value="qticon-caret-right">
        </li>
        <li>
          <div class="icon qticon-caret-left"></div>
          <input type="text" readonly="readonly" value="qticon-caret-left">
        </li>
        <li>
          <div class="icon qticon-card"></div>
          <input type="text" readonly="readonly" value="qticon-card">
        </li>
        <li>
          <div class="icon qticon-caret-down"></div>
          <input type="text" readonly="readonly" value="qticon-caret-down">
        </li>
        <li>
          <div class="icon qticon-camera"></div>
          <input type="text" readonly="readonly" value="qticon-camera">
        </li>
        <li>
          <div class="icon qticon-checkmark"></div>
          <input type="text" readonly="readonly" value="qticon-checkmark">
        </li>
        <li>
          <div class="icon qticon-contrast"></div>
          <input type="text" readonly="readonly" value="qticon-contrast">
        </li>
        <li>
          <div class="icon qticon-fastforward"></div>
          <input type="text" readonly="readonly" value="qticon-fastforward">
        </li>
        <li>
          <div class="icon qticon-itunes"></div>
          <input type="text" readonly="readonly" value="qticon-itunes">
        </li>
        <li>
          <div class="icon qticon-juno"></div>
          <input type="text" readonly="readonly" value="qticon-juno">
        </li>
        <li>
          <div class="icon qticon-lastfm"></div>
          <input type="text" readonly="readonly" value="qticon-lastfm">
        </li>
        <li>
          <div class="icon qticon-linkedin"></div>
          <input type="text" readonly="readonly" value="qticon-linkedin">
        </li>
        <li>
          <div class="icon qticon-list"></div>
          <input type="text" readonly="readonly" value="qticon-list">
        </li>
        <li>
          <div class="icon qticon-location"></div>
          <input type="text" readonly="readonly" value="qticon-location">
        </li>
        <li>
          <div class="icon qticon-fullscreen"></div>
          <input type="text" readonly="readonly" value="qticon-fullscreen">
        </li>
        <li>
          <div class="icon qticon-earth"></div>
          <input type="text" readonly="readonly" value="qticon-earth">
        </li>
        <li>
          <div class="icon qticon-chevron-light-left"></div>
          <input type="text" readonly="readonly" value="qticon-chevron-light-left">
        </li>
        <li>
          <div class="icon qticon-chevron-light-down"></div>
          <input type="text" readonly="readonly" value="qticon-chevron-light-down">
        </li>
        <li>
          <div class="icon qticon-chevron-left"></div>
          <input type="text" readonly="readonly" value="qticon-chevron-left">
        </li>
        <li>
          <div class="icon qticon-chevron-down"></div>
          <input type="text" readonly="readonly" value="qticon-chevron-down">
        </li>
        <li>
          <div class="icon qticon-checkmark-sign"></div>
          <input type="text" readonly="readonly" value="qticon-checkmark-sign">
        </li>
        <li>
          <div class="icon qticon-deviantart"></div>
          <input type="text" readonly="readonly" value="qticon-deviantart">
        </li>
        <li>
          <div class="icon qticon-fire"></div>
          <input type="text" readonly="readonly" value="qticon-fire">
        </li>
        <li>
          <div class="icon qticon-flickr"></div>
          <input type="text" readonly="readonly" value="qticon-flickr">
        </li>
        <li>
          <div class="icon qticon-forrst"></div>
          <input type="text" readonly="readonly" value="qticon-forrst">
        </li>
        <li>
          <div class="icon qticon-forward"></div>
          <input type="text" readonly="readonly" value="qticon-forward">
        </li>
        <li>
          <div class="icon qticon-dribbble"></div>
          <input type="text" readonly="readonly" value="qticon-dribbble">
        </li>
        <li>
          <div class="icon qticon-digg"></div>
          <input type="text" readonly="readonly" value="qticon-digg">
        </li>
      </ul>
      




		<?php

}} //function qt_manual_page()

