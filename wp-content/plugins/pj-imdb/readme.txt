﻿=== PJ IMDB ===
Author: Pouriya Amjadzadeh
Author URI: http://pamjad.me
Contributors: pamjad
Donate link: http://pamjad.me/donate
Tags: imdb,boxoffice,movie
Requires at least: 3.0
Tested up to: 4.7.1
Stable tag: 4.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

fetch the information of the movies from IMDB databse

== Description ==
This plugin will add a metabox in your posts to get information of the movies

Features:

* Nice metabox UI for IMDB.
* Without any advertising.
* Using ajax to get info.

Language Support:

* English
* Persian
* Arabic
* French

== Installation ==
1. Upload "pj-imdb" to the "/wp-content/plugins/" directory.
2. Activate the plugin through the "Plugins" menu in WordPress.

== Frequently Asked Questions ==
= How can show metabox in our template? =
Just go to this [LINK](https://gist.github.com/pamjadz/83db34986a0b0bfee3759847ef1783ee#file-pj-imdb-php) and read functions to use thats.

== Screenshots ==
1. Persian Metabox Perview.
2. English Metabox Perview.

== Changelog ==

= 1.0.1 =
* Fixed IMDB ID field bug.

= 1.1.0 =
* Fixed fetch data.
* Now rates is live
* Changed "IMDB" function to "PJIMDB"
* CSS Now is optimized
* Show rates as star (soon)

= 1.1.1 =
* Fixed minor bugs

= 1.1.2 =
* Seted apikey for free for all users. Thanks to [IMDB CONNECTOR](https://wordpress.org/plugins/imdb-connector/)

== Donations ==
You can Donate plugin author here http://pamjad.me/donate