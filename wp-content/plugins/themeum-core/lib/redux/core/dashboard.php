<?php

    if ( ! defined( 'ABSPATH' ) ) {
        exit;
    }
    
    if (!class_exists('reduxDashboardWidget')) {
        class reduxDashboardWidget {
            public function __construct () {
            }
        }
        
        new reduxDashboardWidget();
    }
