Affiliate Landing Pages
====================

Create dedicated landing pages for your affiliates, which they can promote without using an affiliate link.

= Version 1.0.1, March 2nd, 2017 =
* Fix: Theme customizer does not load when Affiliate Landing Pages is active

= Version 1.0, February 28th, 2017 =
* Initial release
