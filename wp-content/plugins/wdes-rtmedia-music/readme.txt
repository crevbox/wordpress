=== WDES rtMedia Music ===
Contributors: WordPress Developer Extra Support
Donate link: http://4nton.com/donation/
Tags: 4nton, Music, Genre, wdes, rtMedia, Buddypress, Music Genre, Music Image, Music Author, Music Artist, Media, Mp3, Audio, WordPress, WordPress Plugin, Plugin 
Requires at least: 3.0.1
Tested up to: Latest Version
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WDES rtMedia Music for your rtMedia, Buddypress needs.

== Description ==

[WDES rtMedia Music](http://4nton.com/product/wdes-rtmedia-music/) is designed for rtMedia WordPress plugin addons. This plugin is useless without Buddypress and rtMedia installed in your site. [WDES rtMedia Music](http://4nton.com/product/wdes-rtmedia-music/) has a way to categories/search your music by genre, artist, album, and also capable of adding a featured image in every music uploaded. 
Image is this directly save into WordPress uploads and can be found in media dashboard page. Install now to find out more usefull things features.

= Main Features =

* It has admin settings where located in rtMedia > WDES Music dashboard menu. There, you can change the global names, post per page, use shortcode (`[wdes-rtmedia-music]`) in custom pages, and more.
* It has default pages like All Music page with search form and pagination, Upload page, Genre page.
* All front-end templates can override to your theme/child theme folder. Visit [WDES rtMedia Music documentation](http://4nton.com/wdes-rtmedia-music-documentation/).

= Addons =

**WDES rtMedia Music Genre** - [download here](http://4nton.com/product/wdes-rtmedia-music-genre/)

* This addons can categories your music by genre before the music uploaded and in edit media page.
* You can search the music by genre.
* Genre name can be renamed in rtmedia settings in WordPress dashboard.
* Can add/remove list of genre via rtmedia settings in WordPress dashboard.

**WDES rtMedia Music Image** - [download here](http://4nton.com/product/wdes-rtmedia-music-image/)

* This addons can add a featured image in your music media page.
* Image size can be change in rtmedia settings in WordPress dashboard. Both featured image and thumbnail image.
* Add featured image can be done before the files uploaded and in edit music media edit page.
* Images is stored in WordPress media folder (wp-content > uploads).

**WDES rtMedia Music Artist** - [download here](http://4nton.com/product/wdes-rtmedia-music-artist/)

* This addons can set artist before the music uploaded or even in edit music page.
* Can search the music by artist.
* Can view all artist.
* Can view the music by artist.
* Artist name can be renamed in rtmedia settings in WordPress dashboard.
* Artist can be sync/setup by registered user (all users or by roles) or free text in rtmedia settings in WordPress dashboard.

**WDES rtMedia Music Album** - coming soon

**WDES rtMedia Music Author** - coming soon

[WDES rtMedia Music demo](http://4nton.com/members/anthony/all-music/)

= Related plugins =
[WordPress Developer Extra Support Plugins](https://wordpress.org/plugins/tags/wdes)

= Develop by =
[4nton.com - WordPress Developer Extra Support](http://4nton.com/)

Happy coding everyone :D

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the "Dashboard > rtMedia > WDES Music dashboard menu" menu in your dashboard.

== Frequently Asked Questions ==

= Why do we have to use this plugin =

Because it is cool.

== Screenshots ==

1. WDES rtMedia Music edit media page setup.
2. WDES rtMedia Music genre list view.
3. WDES rtMedia Music music upload setup.
3. WDES rtMedia Music music frontend view.

== Changelog ==

= 1.0.0 =
* Lets see what's missing on this plugin for the help of you.

== Upgrade Notice ==

Just upgrade via Wordpress.