<?php


add_action( 'widgets_init', 'podcasts_widget' );
function podcasts_widget() {
	register_widget( 'Podcasts_Widget' );
}

class Podcasts_Widget extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'podcastswidget', 'description' => __('A widget that displays podcasts ', 'podcastswidget') );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'podcastswidget-widget' );
		parent::__construct( 'podcastswidget-widget', __('QT Podcasts Widget', 'podcastswidget'), $widget_ops, $control_ops );
	}
	function widget( $args, $instance ) {
		extract( $args );
		echo $before_widget;
		echo $before_title.$instance['title'].$after_title; 
		global $paged;
		  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array( 
						  'post_type' => 'podcast', 
						  'posts_per_page' => $instance['number'], 
						  'paged' => $paged
						  );
				global $podcastloop;
				wp_reset_query(); 
				$podcastloop= null;
				$podcastloop = new WP_Query();
				$podcastloop->query($args);
		 ?>
         <ul>
         <?php	 
		  if ($podcastloop->have_posts()) : while ($podcastloop->have_posts()) : $podcastloop->the_post(); ?>
            <li>
                <a class="qw-blocklink" href="<?php esc_url(the_permalink()); ?>">
                    <?php
						if($instance['showcover']=='true'){
                       			if(has_post_thumbnail())  { ?>
                              <?php the_post_thumbnail('thumbnail','class=qw-widget-thumbnail');?>
                              <?php
                              }
						}
					?>
                    <?php
					$tit = get_the_title(get_the_ID());
					$tit = substr($tit,0,75);
					if(strlen($tit)>=75){$tit= $tit.' [...]';}
					?>
               		<span class="qw-widg-singleline"><?php echo esc_attr($tit); ?></span><br>
               		<span class="qw-widg-tags"><?php echo get_post_meta($podcastloop ->post->ID,'_podcast_artist',true); ?></span>
                    <div class="canc"></div>
                </a>
            </li>
        <?php endwhile;  endif; 
        if(isset($instance['archivelink']) && isset($instance['archivelink_text'])){
			if($instance['archivelink'] == 'show'){
				if($instance['archivelink_text']==''){$instance['archivelink_text'] = esc_attr__('See all','_s');};
		 	 	echo '<li class="bordertop QTreadmore">
		 	 	<a href="'.get_post_type_archive_link('podcast').'"><i class="icon-chevron-right animated"></i> '.esc_attr($instance['archivelink_text']).'</a>
		 	 	</li>';
		 	} 
		 }?>
        </ul>
        <?php
		wp_reset_query();
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 
		$instance['title'] = strip_tags( $new_instance['title'] );
		$attarray = array(
				'title',
				'showcover',
				'number',
				'archivelink',
				'archivelink_text'
		);
		foreach ($attarray as $a){
			$instance[$a] = esc_attr(strip_tags( $new_instance[$a] ));
		}
		return $instance;
	}
	function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array( 'title' => esc_attr__('Podcasts', '_s'),
							'showcover'=> 'true',
							'number'=> '5',
							'archivelink'=> 'show',
							'archivelink_text'=> esc_attr__('See all','_s')
							);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 	<h2>General options</h2>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo esc_attr__('Title:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo esc_attr__('Quantity:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" style="width:100%;" />
		</p>
      	<p>
			<label for="<?php echo $this->get_field_id( 'showcover' ); ?>"><?php echo esc_attr__('Show cover', '_s'); ?></label><br />			
           True   <input type="radio" name="<?php echo $this->get_field_name( 'showcover' ); ?>" value="true" <?php if($instance['showcover'] == 'true'){ echo ' checked= "checked" '; } ?> />  
           False  <input type="radio" name="<?php echo $this->get_field_name( 'showcover' ); ?>" value="false" <?php if($instance['showcover'] == 'false'){ echo ' checked= "checked" '; } ?> />  
		</p> 
		<p>
			<label for="<?php echo $this->get_field_id( 'archivelink' ); ?>"><?php echo esc_attr__('Show link to archive', '_s'); ?></label><br />			
           Show   <input type="radio" name="<?php echo $this->get_field_name( 'archivelink' ); ?>" value="show" <?php if($instance['archivelink'] == 'show'){ echo ' checked= "checked" '; } ?> />  
           Hide  <input type="radio" name="<?php echo $this->get_field_name( 'archivelink' ); ?>" value="hide" <?php if($instance['archivelink'] == 'hide'){ echo ' checked= "checked" '; } ?> />  
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'archivelink_text' ); ?>"><?php echo esc_attr__('Link text:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'archivelink_text' ); ?>" name="<?php echo $this->get_field_name( 'archivelink_text' ); ?>" value="<?php echo $instance['archivelink_text']; ?>" style="width:100%;" />
		</p> 
	<?php
	}
}
?>