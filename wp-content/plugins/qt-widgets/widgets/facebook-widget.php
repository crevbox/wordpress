<?php

add_action( 'widgets_init', 'facebookfanbox_widget' );
function facebookfanbox_widget() {
	register_widget( 'Facebook_Widget' );
}
class Facebook_Widget extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'fbfanboxwidget', 'description' => esc_attr__('A widget that displays the facebook fanbox ', '_s') );
		$control_ops = array( 'width' => '261px', 'height' => 350, 'id_base' => 'fbfanboxwidget-widget' );
		parent::__construct( 'fbfanboxwidget-widget', esc_attr__('QT Facebook Fanbox Widget', '_s'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$attarray = array(
				'bgcolor',
				'padding',
				'pagename',
				'width',
				'height',
				'colorscheme',
				'showfaces',
				'bordercolor',
				'streaming',
				'header',
				'scrolling'
		);
		foreach ($attarray as $a){

			$instance[$a] = esc_attr(strip_tags( $instance[$a] ));


		}
		echo $before_widget;
		echo $before_title.esc_attr($instance['title']).$after_title; 
		echo '
		<div class="fb_box" style="background:'.esc_attr($instance['bgcolor']).';padding:'.esc_attr($instance['padding']).'; "> 
		<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2F'.$instance['pagename'].'&amp;width='.$instance['width'].'&amp;height='.$instance['height'].'&amp;show_faces='.$instance['showfaces'].'&amp;colorscheme='.$instance['colorscheme'].'&amp;stream='.$instance['streaming'].'&amp;show_border=false&amp;header='.$instance['header'].'&amp;appId=344331825656955" style="height:'.$instance['height'].'px; width:'.str_replace("px","",$instance['height']).'px;"   ></iframe>
		</div>';
		echo $after_widget;
	}
	//Update the widget 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 
		$instance['title'] = strip_tags( $new_instance['title'] );
		$attarray = array(
				'bgcolor',
				'padding',
				'pagename',
				'width',
				'height',
				'colorscheme',
				'showfaces',
				'bordercolor',
				'streaming',
				'header',
				'scrolling'
		);

	

		foreach ($attarray as $a){

			$instance[$a] = esc_attr(strip_tags( $new_instance[$a] ));


		}

		return $instance;

	}



	

	function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array( 'title' => esc_attr__('Facebook FanBox', '_s'),
							'bgcolor' => '020202',
							'padding' => '0px',
							'pagename' => 'QantumThemes',
							'width'		=> '270',
							'height'	=> '310',
							'colorscheme'=> 'dark',
							'showfaces'=> 'true',
							'bordercolor'=> '232323',
							'streaming'=> 'false',
							'header'=> 'false',
							'scrolling'=> 'false'
							);

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 	<h2>General options</h2>
	 	<!-- WIDGET TITLE ========================= -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo esc_attr__('Title:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
		<!-- PAGE NAME ========================= -->
		<p>
			<label for="<?php echo $this->get_field_id( 'pagename' ); ?>"><?php echo esc_attr__('Page name:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'pagename' ); ?>" name="<?php echo $this->get_field_name( 'pagename' ); ?>" value="<?php echo $instance['pagename']; ?>" style="width:100%;" />
		</p>
		<!-- PAGE NAME ========================= -->
		<p>
			<label for="<?php echo $this->get_field_id( 'bgcolor' ); ?>"><?php echo esc_attr__('Background color:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'bgcolor' ); ?>" name="<?php echo $this->get_field_name( 'bgcolor' ); ?>" value="<?php echo $instance['bgcolor']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bordercolor' ); ?>"><?php echo esc_attr__('Border color:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'bordercolor' ); ?>" name="<?php echo $this->get_field_name( 'bordercolor' ); ?>" value="<?php echo $instance['bordercolor']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'padding' ); ?>"><?php echo esc_attr__('Internal padding:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'padding' ); ?>" name="<?php echo $this->get_field_name( 'padding' ); ?>" value="<?php echo $instance['padding']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'width' ); ?>"><?php echo esc_attr__('Box width:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'width' ); ?>" name="<?php echo $this->get_field_name( 'width' ); ?>" value="<?php echo $instance['width']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'height' ); ?>"><?php echo esc_attr__('Box height:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'height' ); ?>" name="<?php echo $this->get_field_name( 'height' ); ?>" value="<?php echo $instance['height']; ?>" style="width:100%;" />
		</p>
			<p>
			<label for="<?php echo $this->get_field_id( 'colorscheme' ); ?>"><?php echo esc_attr__('Color scheme:', '_s'); ?></label><br />
		   Dark   <input type="radio" name="<?php echo $this->get_field_name( 'colorscheme' ); ?>" value="dark" <?php if($instance['colorscheme'] == 'dark'){ echo ' checked= "checked" '; } ?> />  
		   Light  <input type="radio" name="<?php echo $this->get_field_name( 'colorscheme' ); ?>" value="light" <?php if($instance['colorscheme'] == 'light'){ echo ' checked= "checked" '; } ?> />  
		</p> 
		<p>
			<label for="<?php echo $this->get_field_id( 'showfaces' ); ?>"><?php echo esc_attr__('Show faces', '_s'); ?></label><br />			
		   True   <input type="radio" name="<?php echo $this->get_field_name( 'showfaces' ); ?>" value="true" <?php if($instance['showfaces'] == 'true'){ echo ' checked= "checked" '; } ?> />  
		   False  <input type="radio" name="<?php echo $this->get_field_name( 'showfaces' ); ?>" value="false" <?php if($instance['showfaces'] == 'false'){ echo ' checked= "checked" '; } ?> />  
		</p>  
		<p>
			<label for="<?php echo $this->get_field_id( 'streaming' ); ?>"><?php echo esc_attr__('Show faces', '_s'); ?></label><br />			
		   True   <input type="radio" name="<?php echo $this->get_field_name( 'streaming' ); ?>" value="true" <?php if($instance['streaming'] == 'true'){ echo ' checked= "checked" '; } ?> />  
		   False  <input type="radio" name="<?php echo $this->get_field_name( 'streaming' ); ?>" value="false" <?php if($instance['streaming'] == 'false'){ echo ' checked= "checked" '; } ?> />  
		</p>   
		<p>
		<label for="<?php echo $this->get_field_id( 'header' ); ?>"><?php echo esc_attr__('Show streaming', '_s'); ?></label><br />			
		   True   <input type="radio" name="<?php echo $this->get_field_name( 'header' ); ?>" value="true" <?php if($instance['header'] == 'true'){ echo ' checked= "checked" '; } ?> />  
		   False  <input type="radio" name="<?php echo $this->get_field_name( 'header' ); ?>" value="false" <?php if($instance['header'] == 'false'){ echo ' checked= "checked" '; } ?> />  
		</p>   

		<p>
			<label for="<?php echo $this->get_field_id( 'scrolling' ); ?>"><?php echo esc_attr__('Show scrolling bar', '_s'); ?></label><br />			
		   True   <input type="radio" name="<?php echo $this->get_field_name( 'scrolling' ); ?>" value="true" <?php if($instance['scrolling'] == 'true'){ echo ' checked= "checked" '; } ?> />  
		   False  <input type="radio" name="<?php echo $this->get_field_name( 'scrolling' ); ?>" value="false" <?php if($instance['scrolling'] == 'false'){ echo ' checked= "checked" '; } ?> />  
		</p>       

	<?php
	}
}
?>