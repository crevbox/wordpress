<?php

if(!defined('CUSTOM_TYPE_EVENT')){
	define ('CUSTOM_TYPE_EVENT','event');
}

add_action( 'widgets_init', 'events_widget' );


function events_widget() {
	register_widget( 'events_Widget' );
}

class events_Widget extends WP_Widget {
	
	
	/* = Initialize
	=============================================================*/
	
	function __construct() {
		$widget_ops = array( 'classname' => 'eventswidget', 'description' => esc_attr__('A widget that displays events ', '_s') );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'eventswidget-widget' );
		parent::__construct( 'eventswidget-widget', esc_attr__('QT Events Widget', '_s'), $widget_ops, $control_ops );
	}


	/* = Print widget 
	=============================================================*/
	
	function widget( $args, $instance ) {
		extract( $args );
		echo $before_widget;
		echo $before_title.$instance['title'].$after_title; 

		$args = array(
			'post_type' => 'event',
			'posts_per_page' =>$instance['number'],
			'meta_key' => 'eventdate',
			'orderby' => 'meta_value',
			'order' => 'ASC',
			'eventtype' =>$instance ['eventscategory']
		   );

		if(!array_key_exists('hidepastevents', $instance)) {
			$instance['hidepastevents'] = false;
		}

		if($instance['hidepastevents'] == 'true'){
			$args['meta_query'] = array(
           	 	array(
	                'key' => 'eventdate',
	                'value' => date('Y-m-d'),
	                'compare' => '>=',
	                'type' => 'date'
                 )
           );
		}


		$query = new WP_Query();

		$query->query( $args );


		



		/*
		'meta_query' => array(
            array(
                'key' => 'numericdate',
                'value' => date('dmY'),
                'compare' => '>=',
                'type' => 'date'
                 )
           )*/
		
	
		?>
        <ul>
        <?php
		if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
            <li>
                <a class="qw-blocklink" href="<?php esc_url(the_permalink()); ?>">
                	<?php
						if($instance['showcover']=='true'){
                       if(has_post_thumbnail())  { ?>
                              <?php the_post_thumbnail('thumbnail','class=qw-widget-thumbnail');?>
                              <?php
                              }
						}
					?>
					<span class="qw-widg-singleline"><?php echo esc_attr(get_the_title()); ?></span>
               		<br><span class="qw-widg-tags"><?php echo get_post_meta($query->post->ID,'eventdate',true); ?> <?php echo get_post_meta($query->post->ID,'eventlocation',true); ?></span>
                    <div class="canc"></div>
                    </a>
            </li>
        <?php endwhile; endif; ?>
        
        <?php
        
		 if(isset($instance['archivelink']) && isset($instance['archivelink_text'])){
			if($instance['archivelink'] == 'show'){
				if($instance['archivelink_text']==''){$instance['archivelink_text'] = esc_attr__('See all','_s');};
		 	 	echo '<li class="bordertop QTreadmore">
		 	 	<a href="'.get_post_type_archive_link(CUSTOM_TYPE_EVENT).'"><i class="icon-chevron-right animated"></i> '.esc_attr($instance['archivelink_text']).'</a>
		 	 	</li>';
		 	} 
		 }
		
		echo '</ul>';
		echo ((isset($after_widget))?$after_widget:'');
	}

	/* = Update the widget 
	=============================================================*/

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 
		$instance['title'] = strip_tags( $new_instance['title'] );
		$attarray = array(
				'title',
				'showcover',
				'number',
				'hidepastevents',
				'archivelink',
				'eventscategory',
				'archivelink_text'
		);
		foreach ($attarray as $a){
			$instance[$a] = esc_attr(strip_tags( $new_instance[$a] ));
		}
		return $instance;
	}




	/* = Form 
	=============================================================*/


	function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array( 'title' => esc_attr__('Events', '_s'),
							'showcover'=> 'true',
							'hidepastevents' => 'true',
							'number'=> '5',
							'eventscategory' => '',
							'archivelink'=> 'show',
							'archivelink_text'=> esc_attr__('See all','_s')
							);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 	<h2>General options</h2>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'eventscategory' ); ?>"><?php _e('Category:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'eventscategory' ); ?>" name="<?php echo $this->get_field_name( 'eventscategory' ); ?>" value="<?php echo $instance['eventscategory']; ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Quantity:', 'number'); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" style="width:100%;" />
		</p>
      	<p>
			<label for="<?php echo $this->get_field_id( 'showcover' ); ?>"><?php _e('Show cover', '_s'); ?></label><br />			
           True   <input type="radio" name="<?php echo $this->get_field_name( 'showcover' ); ?>" value="true" <?php if($instance['showcover'] == 'true'){ echo ' checked= "checked" '; } ?> />  
           False  <input type="radio" name="<?php echo $this->get_field_name( 'showcover' ); ?>" value="false" <?php if($instance['showcover'] == 'false'){ echo ' checked= "checked" '; } ?> />  
		</p>  

		<p>
			<label for="<?php echo $this->get_field_id( 'hidepastevents' ); ?>"><?php _e('Hide past events', '_s'); ?></label><br />			
           True   <input type="radio" name="<?php echo $this->get_field_name( 'hidepastevents' ); ?>" value="true" <?php if($instance['hidepastevents'] == 'true'){ echo ' checked= "checked" '; } ?> />  
           False  <input type="radio" name="<?php echo $this->get_field_name( 'hidepastevents' ); ?>" value="false" <?php if($instance['hidepastevents'] == 'false'){ echo ' checked= "checked" '; } ?> />  
		</p>  


		 <p>
			<label for="<?php echo $this->get_field_id( 'archivelink' ); ?>"><?php _e('Show link to archive', '_s'); ?></label><br />			
           Show   <input type="radio" name="<?php echo $this->get_field_name( 'archivelink' ); ?>" value="show" <?php if($instance['archivelink'] == 'show'){ echo ' checked= "checked" '; } ?> />  
           Hide  <input type="radio" name="<?php echo $this->get_field_name( 'archivelink' ); ?>" value="hide" <?php if($instance['archivelink'] == 'hide'){ echo ' checked= "checked" '; } ?> />  
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'archivelink_text' ); ?>"><?php _e('Link text:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'archivelink_text' ); ?>" name="<?php echo $this->get_field_name( 'archivelink_text' ); ?>" value="<?php echo $instance['archivelink_text']; ?>" style="width:100%;" />
		</p>
	<?php
	}
}



?>