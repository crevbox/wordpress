<?php

add_action( 'widgets_init', 'artists_widget' );
function artists_widget() {
	register_widget( 'Artists_Widget' );
}

class Artists_Widget extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'artistswidget', 'description' => esc_attr__('A widget that displays artists ', '_s') );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'artistswidget-widget' );
		parent::__construct( 'artistswidget-widget', esc_attr__('QT Artists Widget', '_s'), $widget_ops, $control_ops );
	}
	function widget( $args, $instance ) {
		extract( $args );
		echo $before_widget;
		echo $before_title.$instance['title'].$after_title; 
		$query = new WP_Query();

		//Send our widget options to the query
		
		$queryArray =  array(
			'post_type' => 'artist',
			'posts_per_page' => $instance['number'],
			'ignore_sticky_posts' => 1,
			'order' => 'ASC'
		   );
		
		if($instance['specificid'] != ''){
			$posts = explode(',',$instance['specificid']);
			$finalarr = array();
			foreach($posts as $p){
				if(is_numeric($p)){
					$finalarr[] = $p;
				}
			};
			$queryArray['post__in'] = $finalarr;
		}
		
		$queryArray['orderby'] = 'menu_order';
		if($instance['order'] == 'Random'){
			$queryArray['orderby'] = 'rand';
		}
		
		//echo get_query_var('posts_per_page');
		$query = new WP_Query($queryArray);
		 ?>
         <ul>
         <?php
		if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); 
		global $post;
			?>
            <li>
                <a class="qw-blocklink" href="<?php esc_url(the_permalink()); ?>">
                	<?php
						if($instance['showcover']=='true'){
                       if(has_post_thumbnail())  { ?>
                              <?php the_post_thumbnail('thumbnail','class=qw-widget-thumbnail');?>
                              <?php
                              }
						}
					?>
               		<span class="qw-widg-singleline"><?php echo esc_attr(get_the_title()); ?></span><br>
               		<span class="qw-widg-tags">
               		<?php
               		$terms = get_the_terms($post->ID, 'artistgenre');
               		if(is_array($terms)){
               			
               			$array = array();
	               		foreach($terms as $t){
	               			$array[] = $t->name;
	               			//if (next($terms)==true) echo  " / ";
	               		}
	               		$string = implode(" / ", $array);
	               		echo $string;
               		}

               		?>
               		</span>
                    <div class="canc"></div>
                    </a>
            </li>
        <?php endwhile; endif; 
         if(isset($instance['archivelink']) && isset($instance['archivelink_text'])){
			if($instance['archivelink'] == 'show'){
				if($instance['archivelink_text']==''){$instance['archivelink_text'] = esc_attr__('See all','_s');};
		 	 	echo '<li class="bordertop QTreadmore">
		 	 	<a href="'.esc_url(get_post_type_archive_link('artist')).'"><i class="icon-chevron-right animated"></i> '.esc_attr($instance['archivelink_text']).'</a>
		 	 	</li>';
		 	} 
		 }
		?>
        </ul>
        <?php
        wp_reset_postdata();
		// L'OUTPUT ///////////////////////
		echo $after_widget;
	}

	//Update the widget 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//Strip tags from title and name to remove HTML 

		$attarray = array(
				'title',
				'showcover',
				'number',
				'specificid',
				'order',
				'archivelink',
				'archivelink_text'
		);
		foreach ($attarray as $a){
			$instance[$a] = esc_attr(strip_tags( $new_instance[$a] ));
		}
		return $instance;
	}

	function form( $instance ) {
		//Set up some default widget settings.
		$defaults = array( 'title' => esc_attr__('Artists', '_s'),
							'showcover'=> 'true',
							'number'=> '5',
							'specificid'=> '',
							'order'=> 'Random',
							'archivelink'=> 'show',
							'archivelink_text'=> esc_attr__('See all','_s')
							);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 	<h2>General options</h2>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'specificid' ); ?>"><?php _e('Add only specific artists (by post ID, comma separated):', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'specificid' ); ?>" name="<?php echo $this->get_field_name( 'specificid' ); ?>" value="<?php echo $instance['specificid']; ?>" style="width:100%;" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Quantity:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" style="width:100%;" />
		</p>
      <p>
		<label for="<?php echo $this->get_field_id( 'showcover' ); ?>"><?php _e('Show cover', '_s'); ?></label><br />			
           True   <input type="radio" name="<?php echo $this->get_field_name( 'showcover' ); ?>" value="true" <?php if($instance['showcover'] == 'true'){ echo ' checked= "checked" '; } ?> />
           False  <input type="radio" name="<?php echo $this->get_field_name( 'showcover' ); ?>" value="false" <?php if($instance['showcover'] == 'false'){ echo ' checked= "checked" '; } ?> />  
		</p>  
      <p>
		<label for="<?php echo $this->get_field_id( 'showcover' ); ?>"><?php _e('Order', '_s'); ?></label><br />			
           Random   <input type="radio" name="<?php echo $this->get_field_name( 'order' ); ?>" value="Random" <?php if($instance['order'] == 'Random'){ echo ' checked= "checked" '; } ?> />  
           Page Order  <input type="radio" name="<?php echo $this->get_field_name( 'order' ); ?>" value="Page Order" <?php if($instance['order'] == 'Page Order'){ echo ' checked= "checked" '; } ?> />  
		</p>  
		<p>
			<label for="<?php echo $this->get_field_id( 'archivelink' ); ?>"><?php _e('Show link to archive', '_s'); ?></label><br />			
           Show   <input type="radio" name="<?php echo $this->get_field_name( 'archivelink' ); ?>" value="show" <?php if($instance['archivelink'] == 'show'){ echo ' checked= "checked" '; } ?> />  
           Hide  <input type="radio" name="<?php echo $this->get_field_name( 'archivelink' ); ?>" value="hide" <?php if($instance['archivelink'] == 'hide'){ echo ' checked= "checked" '; } ?> />  
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'archivelink_text' ); ?>"><?php _e('Link text:', '_s'); ?></label>
			<input id="<?php echo $this->get_field_id( 'archivelink_text' ); ?>" name="<?php echo $this->get_field_name( 'archivelink_text' ); ?>" value="<?php echo $instance['archivelink_text']; ?>" style="width:100%;" />
		</p>

	<?php

	}
}
?>