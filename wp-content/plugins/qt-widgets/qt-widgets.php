<?php
/*
Plugin Name: QT Widgets
Plugin URI: http://qantumthemes.com/
Description: Extra widgets for your sidebar: artists, releases, events and more
Author: QantumThemes
Text Domain: _s
Version: 1.0.5
*/

/*
*
*	custom widgets
*
*/
/**/

function my_plugin_load_plugin_textdomain() {
    load_plugin_textdomain( '_s', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'my_plugin_load_plugin_textdomain' );


include 'widgets/facebook-widget.php';
include 'widgets/recent-release-widget.php';
include 'widgets/artists-widget.php';
include 'widgets/podcast-widget.php';
include 'widgets/events_widget.php';
include 'widgets/widget-posts.php';
