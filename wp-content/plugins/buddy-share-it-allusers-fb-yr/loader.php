<?php 
/*
	Plugin Name: Buddy Share It Allusers FB YR
	Plugin URI:	http://www.faxservice.com.ua/wp/ads/buddypress-share-it-allusers-fb-yr-plugin-2/

	Description: Adds a share buttons (Facebook,Twitter,Linked, etc. and also the Buttons you created) to Wordpress or to Buddypress+Wordpress site. You can create your own Button to any own URL. This creative plugin will help you to create different social buttons and other icons and pictures with links. Original code, credit Modemlooper. 

	Author: Yuriy Radko
	Version: 3.2.4

	Author URI: http://www.faxservice.com.ua/wp/yr/
	Text Domain: buddy-share-it-allusers-fb-yr
	Domain Path: /languages
	License: GPL2	Tags:Wordpress,Buddypress,Social,Plugin,share,BP,Facebook,icon,button,Sharer,twitter,linkedin,digg,Buddy,buddypress share,bp share it,buddy share it,fb share,bp social,social icons,YR,blog,links,activity share,activity
*/

// v.3.0 - plugin for: BuddyPress+Wordpress
// v.3.1 - plugin for: BuddyPress+Wordpress OR Wordpress only - 23.12.2016
// v.3.2 - shortcode [yr-bpshare-button] in blog article replaced by intxt button
// v.3.2.1 - imgsrc=http://URL_for_image
// v.3.2.2 - afteractivity  for Activity buttons
// v.3.2.3 - Fixed a bug - for Russian and other languages UTF-8. "download more" (ajax) works correctly in the Activity
// v.3.2.4 - Fixed potential errors (due to new versions of Buddypress) in the plugin block: "Replaces Buddypress URL with activity number on redirect Buddypress URL with member name, -  this enables Facebook Sharer algoritm do right".

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly - WP not loaded

global $yr_bpshare_flag_bp;
if (!$yr_bpshare_flag_bp) {
	$yr_bpshare_flag_bp = 0;    // BP not loaded AND plugin not Initialize
}
	
// start internationalize  buddy-share-it-allusers-fb-yr
add_action( 'plugins_loaded', 'yr_bpshare_load_textdomain' );
function yr_bpshare_load_textdomain() {
    load_plugin_textdomain( 'buddy-share-it-allusers-fb-yr', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
// end internationalize

// Initialize the plugin once 
if ( !function_exists( 'bp_core_install' ) ) {
	require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	if ( is_plugin_active( 'buddypress/bp-loader.php' ) ) {
		require_once ( WP_PLUGIN_DIR . '/buddypress/bp-loader.php' );
	} else {  
		add_action('init', 'yr_wpshare_init');
		return;
	}
} else {
	add_action('init', 'yr_wpshare_init');
	return;
}
	add_action( 'bp_include', 'yr_bpshare_init' );
	
	
function yr_bpshare_init() {
	global $yr_bpshare_flag_bp;
	$yr_bpshare_flag_bp = 1;    // BP loaded and yr-bpshare.php loaded	
	require( dirname( __FILE__ ) . '/yr-bpshare.php' );	
}

function yr_wpshare_init() {	
	global $yr_bpshare_flag_bp;
	$yr_bpshare_flag_bp = 2;	// WP only loaded and yr-bpshare.php loaded
	require( dirname( __FILE__ ) . '/yr-bpshare.php' );	// yr-bpshare.php instead yr-wpshare
}

?>