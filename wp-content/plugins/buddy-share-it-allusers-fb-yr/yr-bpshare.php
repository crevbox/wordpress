<?php
// yr_bpshare 3.2.4
if ( ! defined( 'ABSPATH' ) ) exit;

global $yr_bpshare_options_lines_init_ser;
// declare initial options lines array 
$yr_bpshare_options_lines_init = array(
0 => array ( "name" => "Facebook", "img" => "Facebook16.png", "blog" => 1, "activity" => 1, "forum" => 1, "groups" => 1, "add1" =>0, "links" =>'http://www.facebook.com/sharer.php?t=$title&u=$link' ),
1 => array ( "name" => "Twitter", "img" => "Twitter16.png", "blog" => 1, "activity" => 1, "forum" => 1, "groups" => 1, "add1" =>0, "links" =>'http://twitter.com/share?text=$title&url=$link'),
2 => array ( "name" => "LinkedIn", "img" => "LinkedIn16.png", "blog" => 1, "activity" => 1, "forum" => 1, "groups" => 1, "add1" =>0, "links" =>'http://www.linkedin.com/shareArticle?mini=true&url=$link&title=$title'),
3 => array ( "name" => "Google+", "img" => "Google+16.png", "blog" => 1, "activity" => 1, "forum" => 1, "groups" => 1, "add1" =>0, "links" =>'https://plus.google.com/share?url=$link'),
4 => array ( "name" => "Digg", "img" => "Digg16.png", "blog" => 1, "activity" => 1, "forum" => 1, "groups" => 1, "add1" =>0, "links" =>'http://digg.com/submit?url=$link'),
5 => array ( "name" => "Email", "img" => "Email16.png", "blog" => 1, "activity" => 1, "forum" => 1, "groups" => 1, "add1" =>0, "links" =>'mailto:?subject=$title&body=$link')
);
$yr_bpshare_options_lines_init_ser = serialize($yr_bpshare_options_lines_init);

global $yr_bpshare_options_lines;
$yr_bpshare_options_lines = array(
0 => array(), 1 => array(), 2 => array(), 3 => array(), 4 => array(), 5 => array(),
6 => array()
);


global $yr_bpshare_linemax, $yr_noname1, $button_img_htm3;
global $yr_bpshare_optpath;
$yr_bpshare_linemax = 16;  // max table lines
$yr_noname1 = "no name";    // Share on: (name), if empty
$button_img_htm3 = plugins_url("/img/", __FILE__);
// path to txt file for options (12*7=96)- more simple than Wp options 
$yr_bpshare_optpath = plugin_dir_path(__FILE__)."yr-bpshare-options";

// instruction for admin page:
// 1) You can create your own Button, Share Name, and Sharer URL, directly from admin page.
// Share Name can choose any.
// 2) You can to add (upload) own button image files into plugin dir /img/, 
// and will be better if 1-3 letters of this image name = 1-3 letters of "share on name",
// and images size must be: height = 16 or 32 (weight = any),
// and images name must include digital 16 or 32 
// 3) You can create your own Sharer URL (Php template) with rule: 
// variable $title - plugin replaced by Article title, 
// variable $link - plugin replaced by Article link
// all another text in http: link - may be yours
// 4) Button "Reset to init" - set options lines to plugin initial


add_action('admin_menu', 'yr_bpshare_admin_menu');
add_action( 'network_admin_menu', 'yr_bpshare_admin_menu' );

function yr_bpshare_admin_menu() {
	global $yr_bpshare_options_lines, $yr_bpshare_linemax, $yr_noname1, $button_img_htm3;
	add_submenu_page( 
	'options-general.php', // parent menu options-general.php / bp-general-settings / 
	'bpshare-yr',  // page_title 
	'BP Sharing YR',  // submenu name
	'manage_options',  // Administrator role
	'yr-bpshare',   // options-general.php?page=yr-bpshare
// **!!** admin page = yr-bpshare	
	'yr_bpshare_subpage');   // func displays the submenu page content

	//call register settings function
	add_action( 'admin_init', 'yr_bpshare_reg_settings' );
}

function yr_bpshare_reg_settings() {
	register_setting( 'yr_bpshare',  // option_group
	'yr_bpshare_options' );     // option_name
	}



function yr_bpshare_subpage() {   
	// *********** start admin subpage
		
	if (!current_user_can('manage_options'))  {   // Admin check
		wp_die( __('BuddyShare-YR: You do not have sufficient permissions to access this admin page') );
	}
		
	$pluginpath = plugins_url();		
			
// dcl table
// dcl table htm
// dcl htm name
// v3.2: $name_htm1 = <input type="text"
$name_htm1 = '<tr class="alternate"><td width="68"><p align="center"><textarea name="i';
$name_htm2 = '" rows="3" cols="16">';   /* value */
$name_htm3 = '</textarea></p></td>';	
// dcl htm button
$button_img_htm1 = '<td width="68"><p align="center">';
$button_img_htm2 = '<img src="';
// $button_img_htm3 = global
$button_img_htm4 = '" border="0"> '; // width="16" height="16"
$button_img_htm5 = '</p></td>';
// dcl select image
$select_img_htm1 = '<td width="68"><p align="center"><select name="i';
$select_img_htm1a = '" size="1">';
$select_img_htm2 = '<option selected value="';
$select_img_htm3 = '">';
$select_img_htm4 = '</option>';
$select_img_htm5 = '<option value="';
$select_img_htm6 = '</select></p></td>';
// dcl activity
$activity_htm1 = '<td width="68" valign="middle"><p align="center"><input type="checkbox" name="i';
$activity_htm2 = '" value="1"';
$activity_htm3 = ' checked></p></td>';
$activity_htm4 = '></p></td>';
// dcl blog, forums, groups - the same as activity
// dcl php template
$phptmpl_htm1 = '<td width="68"><p align="center"><textarea name="i';
$phptmpl_htm2 = '" rows="3" cols="20">';
$phptmpl_htm3 = '</textarea></p></td>';

?>

<html>
<div class="wrap">
<h2>BPShare-YR: <?php _e('Settings', 'buddy-share-it-allusers-fb-yr') ?></h2>
<table class="wp-list-table widefat users" border="1" width="100%">
    <tr>
        <td width="68" rowspan="2"><p align="center">Share on: (name)</p></td>
        <td width="68" rowspan="2">
            <p align="center"><?php _e('Button:', 'buddy-share-it-allusers-fb-yr') ?></p>
        </td>
        <td width="68" rowspan="2"><p align="center"><?php _e('Select button image:', 'buddy-share-it-allusers-fb-yr') ?></p></td>
        <td width="290" colspan="4">
            <p align="center"><?php _e('Content to share:', 'buddy-share-it-allusers-fb-yr') ?></p>
        </td>
        <td width="68" rowspan="2"><p align="center"><?php _e('Sharer URL (Php template):', 'buddy-share-it-allusers-fb-yr') ?></p></td>
    </tr>
    <tr>
        <td width="68" height="12">
            <p align="center"><?php _e('Activity', 'buddy-share-it-allusers-fb-yr') ?></p>
        </td>
        <td width="68" height="27">
            <p align="center"><?php _e('Blog', 'buddy-share-it-allusers-fb-yr') ?></p>
        </td>
        <td width="68" height="27">
            <p align="center"><?php _e('Forums', 'buddy-share-it-allusers-fb-yr') ?></p>
        </td>
        <td width="68" height="27">
            <p align="center"><?php _e('Groups', 'buddy-share-it-allusers-fb-yr') ?></p>
        </td>
    </tr>

<form action="<?php echo plugins_url("/yr-bpshare-hnd.php", __FILE__); ?>" method="post">
<?php 
	global $yr_bpshare_linemax, $yr_noname1, $button_img_htm3;
	global $yr_bpshare_options_lines_init_ser;
	global $yr_bpshare_options_lines;
	global $yr_bpshare_optpath;
	
	if ((file_exists($yr_bpshare_optpath) === false) and ( !get_option('yr_bpshare_options') )) {
		// no file and no DB options
		// first plugin start - first put options file - serialize
		$fptr = fopen($yr_bpshare_optpath, 'w');
		fwrite($fptr,$yr_bpshare_options_lines_init_ser);
		fclose($fptr);		
	} else {
		if ((get_option('yr_bpshare_options')) and (file_exists($yr_bpshare_optpath) === false)) {
			// DB options exist and no file - may be deleted plugin
			$yr_bpshare_options_lines_ser = get_option('yr_bpshare_options'); // options DB reserv
			// and first put options file - serialize
			$fptr = fopen($yr_bpshare_optpath, 'w');
			fwrite($fptr,$yr_bpshare_options_lines_ser);
			fclose($fptr);	
		} else {
			// file exist
		}
	}
	// read options from file
	$yr_bpshare_options_lines_ser = file_get_contents($yr_bpshare_optpath);
	$yr_bpshare_options_lines = unserialize ($yr_bpshare_options_lines_ser);
	// and update in DB - reserved options
	delete_option('yr_bpshare_options');
	update_option( 'yr_bpshare_options', $yr_bpshare_options_lines_ser ); // serialize
	
//	$yr_bpshare_redir = $_SERVER[HTTP_REFERER]; // handler redirect	
	$yr_bpshare_redir = admin_url()."options-general.php?page=yr-bpshare";
// **!!** admin page = options-general.php?page=yr-bpshare

	
// **************** table Loop  
$i = 0; // lines. For <input name="iXjY" : X=line (from 0), Y=input col (from 1)

while ($i < $yr_bpshare_linemax) {   // ********* htm table lines loop start

// name
echo $name_htm1. $i. "j1". $name_htm2. $yr_bpshare_options_lines[$i]['name'] .$name_htm3;
// button image
echo $button_img_htm1. $button_img_htm2. $button_img_htm3.$yr_bpshare_options_lines[$i]['img']. $button_img_htm4. $yr_bpshare_options_lines[$i]['img']. $button_img_htm5;
// select button image
echo $select_img_htm1. $i. "j2". $select_img_htm1a. $select_img_htm2. $yr_bpshare_options_lines[$i]['img']. $select_img_htm3. $yr_bpshare_options_lines[$i]['img']. $select_img_htm4;
// read image files name
if ($desdir = opendir( dirname( __FILE__ ) . '/img')) {
    $j=0;
	while (false !== ($bpsh_img_files[$j] = readdir($desdir))) { 
		if (($yr_bpshare_options_lines[$i]['name'] !== false) and ($yr_bpshare_options_lines[$i]['name'] !== "no name")) {
        // echo only if the same first 3 letters in image Name "Fac" = "Fac...png"(v.3.2.1 =2/3let)
		if ((substr($yr_bpshare_options_lines[$i]['name'],1,3) == substr($bpsh_img_files[$j],1,3)) and ($bpsh_img_files[$j] !== $yr_bpshare_options_lines[$i]['img'])) {
			echo $select_img_htm5. $bpsh_img_files[$j] . $select_img_htm3. $bpsh_img_files[$j] . $select_img_htm4;
			}
		}
		else {
			if (substr($bpsh_img_files[$j],0,1) !== ".") {
			echo $select_img_htm5. $bpsh_img_files[$j] . $select_img_htm3. $bpsh_img_files[$j] . $select_img_htm4;
			} else {}
		}
		$j=$j+1;
		}
	closedir($desdir); 
	}
else {}
echo $select_img_htm6;
// activity checked
if ($yr_bpshare_options_lines[$i]['activity'] == 1) {  // checked
	$activity_htmt = $activity_htm3;
	} else {$activity_htmt = $activity_htm4;}   // not checked
echo $activity_htm1. $i. "j3". $activity_htm2. $activity_htmt;
// blog checked
if ($yr_bpshare_options_lines[$i]['blog'] == 1) {  // checked
	$activity_htmt = $activity_htm3;
	} else {$activity_htmt = $activity_htm4;}   // not checked
echo $activity_htm1. $i. "j4". $activity_htm2. $activity_htmt;
// forum checked
if ($yr_bpshare_options_lines[$i]['forum'] == 1) {  // checked
	$activity_htmt = $activity_htm3;
	} else {$activity_htmt = $activity_htm4;}   // not checked
echo $activity_htm1. $i. "j5". $activity_htm2. $activity_htmt;
// groups checked
if ($yr_bpshare_options_lines[$i]['groups'] == 1) {  // checked
	$activity_htmt = $activity_htm3;
	} else {$activity_htmt = $activity_htm4;}   // not checked
echo $activity_htm1. $i. "j6". $activity_htm2. $activity_htmt;
// php template
echo $phptmpl_htm1. $i. "j7". $phptmpl_htm2. $yr_bpshare_options_lines[$i]['links']. $phptmpl_htm3. 
"</tr>";

	$i=$i+1;
}   //  ********  htm table lines loop end

?>
</table>
<input type="hidden" name="linemax" value="<?php echo $yr_bpshare_linemax; ?>">
<input type="hidden" name="noname1" value="<?php echo $yr_noname1; ?>">
<input type="hidden" name="optpath" value="<?php echo $yr_bpshare_optpath; ?>">
<input type="hidden" name="redir" value="<?php echo $yr_bpshare_redir; ?>">
<input type="hidden" name="initlines" value="<?php echo base64_encode($yr_bpshare_options_lines_init_ser); ?>">
  
<p class="submit">
<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'buddy-share-it-allusers-fb-yr') ?>" />
</p>
<p><button name="reset" type="submit" value="Reset">Reset to init</button></p>
</form>
</div>
<div style="font-size:14px;">
<p style="margin-top:10;"><i><?php _e('You can:', 'buddy-share-it-allusers-fb-yr') ?></i></p>
<p><?php _e('1) You can create your own Button, Share Name, and Sharer URL, directly from admin page. Share Name can choose any.', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('2) You can to add (upload) own button image files into plugin dir /img/, &nbsp;and will be better if 1-3 letters of this image name = 1-3 letters of &quot;share on name&quot;, &nbsp;and images size must be: height = 16 or 32 (weight = any), &nbsp;and images name must include digital 16 or 32.', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('3) You can create your own Sharer URL (Php template) with rule: &nbsp;variable $title - plugin replaced by Article title, &nbsp;variable $link - plugin replaced by Article link, all another text in http: link - may be yours.', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('4) Share on:(name) - may be your own text-name, but: if Shareon:(name) is empty or = "no name", then this line is not processed. Shareon:(name) can contain keywords:', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('- If Shareon:(name) includes the keyword "nojsb", then such Button not hidden by one JS Share Button (JS Share Button - the default option).', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('- If Shareon:(name) includes the keyword "intxt", then such Button appears in those blog articles, in the text of which the Shortcode [yr-bpshare-button] is inserted. Only one Shortcode [yr-bpshare-button] can be inserted anywhere in the article. The Shortcode [yr-bpshare-button] will be replaced by "intxt" buttons.', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('- If Shareon:(name) includes the keyword "afteractivity", then such Button will appear after the Activity content. (By default, the button appears before the activity content.)', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('- If Shareon:(name) includes the keyword "imgsrc=" and then a link is written, for example "imgsrc=http://yoursite.com/images/facebround32.png", then the image for such a button will be taken from this link. You can give a link to any image of any size and see how they are displayed on the site. This keyword is convenient for debugging. The keyword "imgsrc=" is more priority than setting the icons from the plugin sub-directory. The image settings from the plugin sub-directory on the admin page remain unchanged.', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('- Examples of the values of the column Share on (name):  Facebook nojsb  / Linked2 intxt / Faceb3 intxt nojsb / Facebook nojsb imgsrc=http://own-home.com.ua/images/facebround32.png', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('5) Button Reset_to_init - set options lines to plugin initial. Be careful. Your settings will be lost.', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><?php _e('6) When upgrading, your settings remain. After upgrade please press Save - rule for versions from 3.0 and higher. However, the sub-directory of the plugin images will be overwritten with images for the new version (all the old plugin icons are saved), i.e your own icon images will not be saved. Your own icons will need to be re-written in the sub-directory of the new version of the plugin.', 'buddy-share-it-allusers-fb-yr') ?></p>
<p><span style="font-size:16pt;"><b>Did you like my plugin?</b></span> <span style="font-size:16pt;">Please </span><b><span style="font-size:16pt;"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=JE4MHRTQN98DU" target="_blank">donate</a></span></b><span style="font-size:16pt;"> any amount for my further development of Wordpress plugins.</span></p>
<p><span style="font-size:16pt;">Thank you.</span></p>
<p><span style="font-size:16pt;">Sincerely yours, <a href="http://www.faxservice.com.ua/wp/yr/" target="_blank">Yuriy Radko</a></span></p>
</div>
</html>

<?php
}  // ********** end subpage

if (file_exists($yr_bpshare_optpath) === false) {
	// options file not exist - how it can be ?
	// read options from DB reserve, if not empty
	if (!get_option('yr_bpshare_options')) {
		$yr_bpshare_options_lines_ser = get_option('yr_bpshare_options');
	} else {
		// if DB empty then options = init
		$yr_bpshare_options_lines_ser = $yr_bpshare_options_lines_init_ser;
	}
} else {
	// options file exist - read options
	$yr_bpshare_options_lines_ser = file_get_contents($yr_bpshare_optpath);
}
$yr_bpshare_options_lines = unserialize ($yr_bpshare_options_lines_ser);

if ($yr_bpshare_flag_bp == 1) {  // processing for Buddypress
	// ** activity hook
	// show not only for members but for all 
	//	add_filter('bp_insert_activity_meta' , 'bp_share_it_content_filter'); // v.321
	add_filter('bp_get_activity_content_body' , 'bp_share_it_content_filter');  // v.322
	function bp_share_it_content_filter($activity_content2) {
		// receive from return - array($buttons_ret,$intxt_buttons_ret)
		$yr_bpsh_activitybuttonsarray = yr_bpshare_activity_meta();
		// buttons before + content + after "afteractivity"
		$yr_bpsh_activity_ret1 = $yr_bpsh_activitybuttonsarray[0] . $activity_content2;
		$yr_bpsh_activity_ret2 = $yr_bpsh_activitybuttonsarray [1];
		return   $yr_bpsh_activity_ret1 . $yr_bpsh_activity_ret2 ;
		// default from v.321 = before
	}

	// ** forum hook - Adds share button to forum topics
	add_action('bp_before_group_forum_topic_posts', 'yr_bpshare_forum_hook', 999);

	// ** group hook
	add_action('bp_group_header_meta', 'yr_bpshare_group_hook', 999);
} else {	// only Wordpress
	
}

// ** blog hook

//add_action('bp_before_blog_single_post', 'yr_bpshare_blog_hook', 999);  // upper left
//add_action('bp_after_blog_single_post', 'yr_bpshare_blog_hook', 999);    // under bottom
// left upper corner each post incl. stream loop on main page
add_filter( 'the_content', 'yr_bpshare_blog_hook');

// function on activity hook
function yr_bpshare_activity_meta() {
	global $yr_bpshare_options_lines;
	global $yr_bpshare_linemax, $button_img_htm3;

	$activitylink = bp_get_activity_thread_permalink();  
// Unspecified case 26 06 17  --- wrong result bp_get_activity_thread_permalink() 
	$pos1 = stripos($activitylink, "Content-Type:");
	if (  $pos1 != false ) {
			$url2 = substr($activitylink,0,$pos1);
			$activitylink = $url2;
		} else {}
// the same case
	$pos1 = stripos($activitylink, "Content-Length:");
	if (  $pos1 != false ) {
			$url2 = substr($activitylink,0,$pos1);
			$activitylink = $url2;
		} else {}
	
//*** Buddypress activity redirect url - special for facebook	
	$position_activity = stripos($activitylink, "/p/");
	if (  ($position_activity != false)  ) {
		// get redirecting buddypress url
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $activitylink);
		curl_setopt($ch, CURLOPT_HEADER, 1);  // incl header
		curl_setopt($ch, CURLOPT_NOBODY, 1);  // not incl txt
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // not echo but return
//		curl_setopt($ch, CURLOPT_HTTPGET, 1);  // in GET (default)
		$headers = "";
		$headers = curl_exec($ch);
		curl_close($ch);
		// url from header
		$pos1 = 0;
		$pos1 = stripos($headers, "Location: ");
		if (  $pos1 != false ) {
			$url2 = substr($headers,$pos1+10);
			$url2_l = strlen($url2);
			$url2 = substr($url2,0,$url2_l-4);  // last 4 bytes
			$activitylink = $url2;
		} else {}
	} else {}
//*** end BP redirect

	$activitytitle = bp_get_activity_feed_item_title() ;  
	$pluginpath = plugins_url();
	
	return yr_bpshare_button_activity_filter("activity",$yr_bpshare_options_lines,$yr_bpshare_linemax,$button_img_htm3,$activitylink,$activitytitle) ;
}

// function on forum hook
function yr_bpshare_forum_hook() {
	global $yr_bpshare_options_lines;
	global $yr_bpshare_linemax, $button_img_htm3;

	$topiclink = bp_get_the_topic_permalink();
	$topictitle = bp_get_the_topic_title();
	$pluginpath = plugins_url();
	
	$activitylink = $topiclink;
	$activitytitle = $topictitle;

	return yr_bpshare_button_activity_filter("forum",$yr_bpshare_options_lines,$yr_bpshare_linemax,$button_img_htm3,$activitylink,$activitytitle) ;
}

// function on group hook
function yr_bpshare_group_hook() {
	global $yr_bpshare_options_lines;
	global $yr_bpshare_linemax, $button_img_htm3;

	$grouplink =  bp_get_group_permalink();
	$groupname = bp_get_group_name();
	$pluginpath = plugins_url();
	
	$activitylink = $grouplink;
	$activitytitle = $groupname;

	return yr_bpshare_button_activity_filter("groups",$yr_bpshare_options_lines,$yr_bpshare_linemax,$button_img_htm3,$activitylink,$activitytitle) ;
}

// function on blog hook
function yr_bpshare_blog_hook($content) {  // $content - if add_filter( 'the_content', 
	global $yr_bpshare_options_lines;
	global $yr_bpshare_linemax, $button_img_htm3;

	$postlink =  get_permalink();
	$posttitle = get_the_title();
	$pluginpath = plugins_url();
	
	$activitylink = $postlink;
	$activitytitle = $posttitle;

	// receive from return - array($buttons_ret,$intxt_buttons_ret)
	$yr_bpshare_blogbuttonsarray = yr_bpshare_button_activity_filter("blog",$yr_bpshare_options_lines,$yr_bpshare_linemax,$button_img_htm3,$activitylink,$activitytitle);
	
	// seek shortcode [yr-bpshare-button] in $content and replace by  $intxt_buttons_ret
	$buttonpos = stripos($content,'[yr-bpshare-button]');
	if ($buttonpos) {
		$content = substr($content,0,$buttonpos) . $yr_bpshare_blogbuttonsarray[1] . substr($content,$buttonpos+19);
		} else { 	}
	
	return $content . $yr_bpshare_blogbuttonsarray[0];
	// for share butt style under blog post
	// if add_filter( 'the_content', 'yr_bpshare_blog_hook');
}


// create BUTTONS function  ==============
function yr_bpshare_button_activity_filter($activity_par,$yr_bpshare_options_lines,$imax,$button_img_url,$activitylink,$activitytitle) {
	global $yr_noname1; 
	$jsbuttonflag = 1;   // "nojsb"  - default = yes JS Butt
	$sharetrans = __('Share more', 'buddy-share-it-allusers-fb-yr');
		
	$shareit = '';   // JS buttons group 
	$shareit_nojs = '';  // not JS buttons group
	$intxt_shareit = '';   // intxt Blog - JS buttons group 
	$intxt_shareit_nojs = '';  // intxt Blog - not JS buttons group
	$afteractiv_shareit = '';   // afteractivity - JS buttons group 
	$afteractiv_shareit_nojs = '';  // afteractivity - not JS buttons group
	
	$i = 0;  
	while ($i < $imax) { 	// START while lines	
		if (($yr_bpshare_options_lines[$i][$activity_par] == 1) and (stripos($yr_bpshare_options_lines[$i]["name"],$yr_noname1) == false)) {
			// links processing 
			// replace $link
			$linkpos = stripos($yr_bpshare_options_lines[$i]["links"],'$link');
			if ($linkpos) {
				$articlelink = substr($yr_bpshare_options_lines[$i]["links"],0,$linkpos) . $activitylink . substr($yr_bpshare_options_lines[$i]["links"],$linkpos+5);
			} else {
				$articlelink = $yr_bpshare_options_lines[$i]["links"];
			}
			// replace $title
			$titlepos = stripos($articlelink,'$title');
			if ($titlepos) {
				$articlelink = substr($articlelink,0,$titlepos) . $activitytitle . substr($articlelink,$titlepos+6);
			} else { 	}
						
			$buttonalt = 'YR_BPshare: ' . $yr_bpshare_options_lines[$i]["name"];
			$buttontitle = $yr_bpshare_options_lines[$i]["name"] . 
			$activitytitle ;  
			// title = 26 - 60 utf name - tuning...
			
			yr_bpshare_clearkeyw($buttonalt,"nojsb");  // clear from keywords
			yr_bpshare_clearkeyw($buttontitle,"nojsb");
			yr_bpshare_clearkeyw($buttonalt,"intxt");
			yr_bpshare_clearkeyw($buttontitle,"intxt");
			yr_bpshare_clearkeyw($buttonalt,"afteractivity");
			yr_bpshare_clearkeyw($buttontitle,"afteractivity");
			
			// button
			$yr_bpshare_button = '';	
			$yr_bpsh_imgsrc = '';   $yr_bpsh_imgsrchttplen = 0;
			yr_bpshare_imgsrc($yr_bpshare_options_lines[$i]["name"],"imgsrc=",$yr_bpsh_imgsrc,$yr_bpsh_imgsrcpos,$yr_bpsh_imgsrchttplen);  // v3.2.1
						
			$yr_bpshare_button = $yr_bpshare_button . '<a class="new-window" target="_blank" href="' . 
			$articlelink .
			'"><img src="' ;
			if ($yr_bpsh_imgsrcpos and $yr_bpsh_imgsrchttplen >0) { // yes  imgsrc= +http not empty
				$yr_bpshare_button = $yr_bpshare_button . 
				$yr_bpsh_imgsrc;
				yr_bpshare_clearkeyw($buttonalt,"imgsrc=");
				yr_bpshare_clearkeyw($buttonalt,$yr_bpsh_imgsrc);
				yr_bpshare_clearkeyw($buttontitle,"imgsrc=");
				yr_bpshare_clearkeyw($buttontitle,$yr_bpsh_imgsrc);
			}
			else {
				$yr_bpshare_button = $yr_bpshare_button . 
				$button_img_url .
				$yr_bpshare_options_lines[$i]["img"] ;
			}
						
			$yr_bpshare_button = $yr_bpshare_button .
			'" alt="' . $buttonalt;
			$yr_bpshare_button = $yr_bpshare_button . '" title="' . $buttontitle;			
						
				// vertical image align - only for 16px pic, eg name = LinkedIn16.png
				if (stripos($yr_bpshare_options_lines[$i]["img"],"16")) { // 16px pic
					$yr_bpshare_button = $yr_bpshare_button . '" class="yr_share_img"  style="margin: 0 0 8 0;"></a>';
				}
				else // 32px image
				{$yr_bpshare_button = $yr_bpshare_button . '" class="yr_share_img" style="margin: 0 0 0 0;"></a>';}
			
			// intxt 
			if (stripos($yr_bpshare_options_lines[$i]["name"],"intxt") ) {  
				// button only for intxt
				if (stripos($yr_bpshare_options_lines[$i]["name"],"nojsb")) {	
					// separate: nojsb
					$intxt_shareit_nojs = $intxt_shareit_nojs . $yr_bpshare_button . ' ';
				} else {	// not nojsb
					$intxt_shareit = $intxt_shareit . $yr_bpshare_button . ' ';
				}	
				
			} else {	// not intxt
				if ($activity_par == "activity") {  
					// and activity hook
					if (stripos($yr_bpshare_options_lines[$i]["name"],"afteractivity")) {  
						// separate before after 
						if (stripos($yr_bpshare_options_lines[$i]["name"],"nojsb")) {	
						// and nojsb
							$afteractiv_shareit_nojs = $afteractiv_shareit_nojs . $yr_bpshare_button . ' ';
						} else {	
						// not nojsb
							$afteractiv_shareit = $afteractiv_shareit . $yr_bpshare_button . ' ';
						}	
				
					} else {	// not afteractivity
						if (stripos($yr_bpshare_options_lines[$i]["name"],"nojsb")) {	
							// and nojsb
							$shareit_nojs = $shareit_nojs . $yr_bpshare_button . ' ';
						} else {	// not nojsb
							$shareit = $shareit . $yr_bpshare_button . ' ';
						}
					}		
				}
				else {
					// not activity hook and not intxt
					if (stripos($yr_bpshare_options_lines[$i]["name"],"nojsb")) {	
						// yes nojsb
						$shareit_nojs = $shareit_nojs . $yr_bpshare_button . ' ';
					} else {	// not nojsb
						$shareit = $shareit . $yr_bpshare_button . ' ';
					}
				}
								
			}	// end not intxt	
			
			
		} else {}
		$i=$i+1;
	}	// END while lines
	
	// classic button - css  -------------
	// JS Share button
	if ($shareit) {
		$buttons = '<div class="bp-share-it-button-group generic-button"><a>'.$sharetrans.'</a></div><div class="share-buttons group"><ul>' . $shareit . '</ul></div>' .
		'<p>&nbsp;</p>';
	}
	// not JS
	if ($shareit_nojs)  {
		$buttons_nojs = '<div id="yr_share_button">'.$shareit_nojs.'</div>';
	}
	// common classic button
	$buttons_ret = '<div class="yr_nojsb">'.$buttons_nojs . '</div>' . $buttons ;  
	
	// intxt button - css	----------------
	// JS intxt Share button
	if ($intxt_shareit) {
		$intxt_buttons = '<div class="bp-share-it-button-group generic-button"><a>'.$sharetrans.'</a></div><div class="share-buttons group"><ul>' . $intxt_shareit . '</ul></div>';
	}
	// not JS intxt
	if ($intxt_shareit_nojs) {
		$intxt_buttons_nojs = '<div id="yr_share_button">'.$intxt_shareit_nojs.'</div>';
	}	 
	// common intxt button
	$intxt_buttons_ret = '<div class="yr_nojsb">'.$intxt_buttons_nojs . '</div>' . $intxt_buttons ;  
	// afteractiv button - css  ------------
	// JS afteractiv Share button
	if ($afteractiv_shareit) {
		$afteractiv_buttons = '<div class="bp-share-it-button-group generic-button"><a>'.$sharetrans.'</a></div><div class="share-buttons group"><ul>' . $afteractiv_shareit . '</ul></div>' . '<p>&nbsp;</p>';
	}
	// not JS afteractiv
	if ($afteractiv_shareit_nojs) {
		$afteractiv_buttons_nojs = '<div id="yr_share_button">'.$afteractiv_shareit_nojs.'</div>'; 
	}	
	// common afteractiv button
	if ($afteractiv_buttons_nojs OR $afteractiv_buttons) {
		$afteractiv_buttons_ret = '<div class="yr_nojsb">'.$afteractiv_buttons_nojs . '</div>' . $afteractiv_buttons ;
	} else {}
		
	if ($activity_par == 'blog' ) {  // blog:  we have 2 group buttons: classic + intxt
		return array($buttons_ret,$intxt_buttons_ret);		
	} else {   
		if ($activity_par == 'activity') {
			// activity: we have 2 group buttons: classic before content + buttons after content
			return array($buttons_ret,$afteractiv_buttons_ret);
		}
		else {} 
	}
	// 1 group buttons - classic
	echo $buttons_ret;	
	
}	// END create BUTTONS function ===============

// function: clear from keywords
function yr_bpshare_clearkeyw(&$yr_clearname,$yr_clearkeyw) {	
	$keywpos = stripos($yr_clearname,$yr_clearkeyw);
	if ($keywpos) {
		$clearkeyw_len = strlen($yr_clearkeyw);
		$clearname1 = substr($yr_clearname,0,$keywpos) . 
		substr($yr_clearname,$keywpos+$clearkeyw_len);
		$yr_clearname = $clearname1;
	} else {}			
}

// function: analyze imgsrc= in name field. imgsrc=http://URL_for_image
function yr_bpshare_imgsrc($yr_bpsh_name,$yr_bpsh_srctxt,&$yr_bpsh_imgsrc,&$yr_bpsh_imgsrcpos, 
	&$yr_bpsh_imgsrchttplen) {
	$yr_bpsh_imgsrcpos = stripos($yr_bpsh_name,$yr_bpsh_srctxt);
	if ($yr_bpsh_imgsrcpos) {
		$yr_bpsh_imgsrchttppos = $yr_bpsh_imgsrcpos + 7;
		$blankpos = stripos(substr($yr_bpsh_name,$yr_bpsh_imgsrchttppos)," ");				
		if ($blankpos > $yr_bpsh_imgsrchttppos) {
			$yr_bpsh_imgsrchttplen = $blankpos;		
			$yr_bpsh_imgsrc = substr($yr_bpsh_name,$yr_bpsh_imgsrchttppos,$yr_bpsh_imgsrchttplen);
		} else {   // blankpos=false -> imgsrc= at line end
			$yr_bpsh_imgsrchttplen = strlen(substr($yr_bpsh_name,$yr_bpsh_imgsrchttppos));			
			if ($yr_bpsh_imgsrchttplen > 0) {
				$yr_bpsh_imgsrc = substr($yr_bpsh_name,$yr_bpsh_imgsrchttppos,$yr_bpsh_imgsrchttplen);
			}
		}
	}
	else {
		$yr_bpsh_imgsrc = '';
		$yr_bpsh_imgsrchttplen =0;
	}
}


/**
 * bp_share_it_scripts()
 * Includes the Javascript and css.
 * (reserved for another plugin options) 
 */

function bp_share_it_scripts() {
	wp_enqueue_script( "buddypress-share-it", path_join(WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/bp-share-it.js"), array( 'jquery' ) );
}
add_action('wp_print_scripts', 'bp_share_it_scripts');

function bp_share_it_button_insert_head() {
	$path_css = path_join(WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/style.css");
	wp_enqueue_style ( "buddy-share-it-style", $path_css);
}
add_action('wp_print_styles', 'bp_share_it_button_insert_head');
//add_action('wp_head', 'bp_share_it_button_insert_head');  

?>