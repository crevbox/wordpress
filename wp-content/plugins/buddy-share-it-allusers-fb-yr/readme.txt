=== Buddy Share It Allusers FB YR ===
Contributors: intels
Donate link:https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=JE4MHRTQN98DU
Tags:Wordpress,Buddypress,Social,Plugin,share,BP,Facebook,icon,button,Sharer,twitter,linkedin,digg,Buddy,buddypress share,bp share it,buddy share it,fb share,bp social,social icons,YR,blog,links,activity share,activity
Requires at least: 3.3.1 
Tested up to: 4.7.5 
Stable tag: 3.2.4
Version: 3.2.4
License: GPLv2 or later

== Description ==
Adds a share buttons (Facebook,Twitter,Linked, etc. and also the Buttons you created) to Wordpress -OR- BuddyPress site. You can create your own Button to any own URL. This creative plugin will help you to create different social buttons and other icons and pictures with links. Original code, credit Modemlooper.

== Installation ==
After updating the plugin version, do the following:

1. Click submenu 'BP Sharing YR' under the 'General options' admin menu
2. Check the options and press "Save Changes" - be sure to click to save and see the site

= Automatic first Installation =

1. From inside your WordPress administration panel, visit 'Plugins -> Add New'
2. Search for 'Buddy Share It Allusers FB YR' (by the keyword YR) and find this plugin in the results
3. Click 'Install'
4. Once installed, activate via the 'Plugins -> Installed' page
5. Click submenu 'BP Sharing YR' under the 'General options' admin menu
6. Check the options you want and press "Save Changes"

= Manual Installation =

1. Upload 'Buddy Share It Allusers FB YR' to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
5. Click submenu 'BP Sharing YR' link under the 'General options' admin menu
6. Check the options you want

== Frequently Asked Questions ==

= Example of settings for share button YR

For example, set up a social network button Vkontakte. 
In column Share on:(name) write "Vkontakte" . Select image vk32.png or vk16.png. Or add the icon file of your button in the plugin directory /img, size 16x16 or 32x32. The file name of the icon should include numbers 16 or 32. It will be better if 1-3 letters of this image name = 1-3 letters of value "Share on name".
In column Sharer URL (Php template): - write http://vk.com/share.php?url=$link&title=$title . Variable $title - plugin replaced by Article title, variable $link - plugin replaced by Article link, all another text in http: link - may be yours. 
Select Blog or/and Activity or/and ... etc. - where the button Vkontakte will appear. If we have only blog on Wordpress - without Buddypress - then you can select only Blog, another selections must be ignored. Press Save Changes. Button set.
If in Share on:(name) write "Vkontakte nojsb" then the button will not be hidden behind the general JS-button.
If Shareon:(name) column includes the keyword "intxt", then such Button appears in those blog articles, in the text of which the Shortcode [yr-bpshare-button] is inserted. Only one Shortcode [yr-bpshare-button] can be inserted anywhere in the article. The Shortcode [yr-bpshare-button] will be replaced with "intxt" buttons.
If Shareon:(name) includes the keyword "imgsrc=" and then a link is written, for example "imgsrc=http://yoursite.com/images/facebround32.png", then the image for such a button will be taken from this link. You can give a link to any image of any size and see how they are displayed on the site. This keyword is convenient for debugging. The keyword "imgsrc=" is more priority than setting the icons from the plugin sub-directory. The image settings from the plugin sub-directory on the admin page remain unchanged.
Now (v.322) you can specify in the settings that the share buttons should be located at the bottom of the Activity content (after content). Now some buttons can be placed upper of the Activity content (by default) and some buttons - after the content by the keyword "afteractivity" in Shareon:(name) .

Examples of the values of the column Share on (name):  Facebook nojsb  / Linked2 intxt / Faceb3 intxt nojsb / Facebook nojsb imgsrc=http://yoursite.com/images/facebround32.png / Twitter   afteractivity /

Examples of the values of the column URL:  http://www.facebook.com/sharer.php?t=$title&u=$link / http://twitter.com/share?text=$title&url=$link / http://www.linkedin.com/shareArticle?mini=true&url=$link&title=$title / https://plus.google.com/share?url=$link / http://digg.com/submit?url=$link / mailto:?subject=$title&body=$link / http://vk.com/share.php?url=$link&title=$title /

= What other features of the plugin?

The plugin works with both Buddypress + Wordpress, and works only with the Wordpress blog - without Buddypress installed. In the Wordpress blog social buttons can be inserted anywhere in the blog text. 
For Buddypress:
Buttons shows for all - not only for registered user. 
Icons on social networks can appear above and/or below the content of activity - that is, there can be two groups of icons - and higher and lower. 
Icons can be "hidden" behind the JS-button SHARE and/or not hidden. 
Icons can be your own pictures. 
Icons URL can be your own - So you can create buttons on new social networks or on those sites that you need. 
You have a great field for creativity with this plugin.
The plugin has the property: Replaces Buddypress URL with activity number on redirect Buddypress URL with member name, -  this enables Facebook Sharer algoritm do right

= Which versions of the WP and BP plugin tested? =

"Buddy Share It Allusers FB YR" plugin tested on versions: Wordpress 3.3.1, Wordpress 3.4.2 and Buddypress 1.6.1, Wordpress 4.7.5 and Buddypress 2.7.3. Between versions: (WP 3.3.1 - WP 4.7.5)+ (BP 1.6.1 - BP 2.7.3) - plugin should work.

== Screenshots ==

1. Admin options 

2. Admin page

3. Activity on WP 3.4.2 and BP 1.6.1

4. Blog on WP 4.7 and BP 2.7.3

5. Activity on WP 4.7 and BP 2.7.3

6. Blog on WP 3.3.1 without Buddypress, 2 Buttons - "nojsb", 4 Buttons hidden by JS Button, RU localization

7. Example of setting social buttons

== Notes ==
License.txt - contains the licensing details for this component.

== Changelog ==

= 3.2.4 =
Fixed potential errors (due to new versions of Buddypress) in the plugin block: "Replaces Buddypress URL with activity number on redirect Buddypress URL with member name, -  this enables Facebook Sharer algoritm do right".

= 3.2.3 =
Fixed a bug - for Russian and other languages UTF-8 excluded the possibility of incorrect characters in the �title� of icons. Thanks to this, "download more" (ajax) works correctly in the Activity.

= 3.2.2 =
Now you can specify in the settings that the share buttons should be located at the bottom of the Activity content (after content). Now some buttons can be placed upper of the Activity content (by default) and some buttons - after the content (keyword "afteractivity" in Shareon:(name) ).

= 3.2.1 =
Now for button icon you can give a link to any image of any size.

= 3.2 =
Now you can insert social buttons and other icons and pictures with links directly into the text of the blog article. The place for the buttons in the blog indicates by the Shortcode [yr-bpshare-button] .

= 3.1 =
Now plugin can operate without Buddypress - working only with WordPress.
Now each button can have a parameter 'nojsb' in Name. That is, at the same time will be JS button and not hidden button (nojsb).

= 3.0 =
The plugin is completely rewritten. Admin page made to create your own "Share" buttons. Tested on WP 4.7 and BP 2.7.3. Russian localization.

= 2.0 =
Buddy Share It Allusers FB YR  - v.2.0  (tested on BP 1.6.1)

== Upgrade Notice ==
= 3.0 =
The plugin is completely rewritten. Now you can from the admin page create your buttons on your "Share" links. The max number of Share Buttons - 12, but may be virtually unlimited. All buttons can be hidden behind one JS button. The plugin gives the administrator a great field of creativity for its buttons.

= 2.0 =
Not required. 2.0 is a first public release - it is a modified �BuddyShare� Plugin.