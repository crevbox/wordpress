msgid ""
msgstr ""
"Project-Id-Version: BuddyPress Friends On-line (FOL) v0.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-03-07 14:03+0200\n"
"PO-Revision-Date: 2012-03-07 14:03+0200\n"
"Last-Translator: slaFFik <slaffik@list.ru>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Russian\n"
"X-Poedit-Country: RUSSIA\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Poedit-Basepath: ./\n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Textdomain-Support: yes"

#: bp-friends-on-line.php:20
#@ fol
msgid "Online"
msgstr "Online"

#: bpfol-custom-widget.php:16
#: bpfol-custom-widget.php:81
#@ fol
msgid "Friends on-line"
msgstr "Freunde Online"

#: bpfol-custom-widget.php:19
#@ fol
msgid "Friends on-line widget"
msgstr "Freunde Online widget"

#: bpfol-custom-widget.php:51
#@ fol
msgid "No friends on-line."
msgstr "Keine Freunde Online."

#: bpfol-custom-widget.php:97
#@ fol
msgid "Title:"
msgstr "Titel:"

#: bpfol-custom-widget.php:99
#@ fol
msgid "View mode"
msgstr "Anzeigemodus"

#: bpfol-custom-widget.php:101
#@ fol
msgid "standart"
msgstr "standard"

#: bpfol-custom-widget.php:102
#@ fol
msgid "only avatar"
msgstr "nur Avatar"

#: bpfol-custom-widget.php:106
#@ fol
msgid "Standart mode settings:"
msgstr "Standard modus Einstellungen"

#: bpfol-custom-widget.php:108
#@ fol
msgid "Display avatar"
msgstr "Zeige Avatar"

#: bpfol-custom-widget.php:110
#@ fol
msgid "Display name"
msgstr "Zeige Name"

#: bpfol-custom-widget.php:112
#@ fol
msgid "Display last active date"
msgstr "Zeige letztes aktives Datum"

#: bpfol-custom-widget.php:115
#@ fol
msgid "Number of friends:"
msgstr "Anzahl von Freunde"

