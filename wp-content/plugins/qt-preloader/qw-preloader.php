<?php
/*
Plugin Name: QT Preloader
Plugin URI: http://qantumthemes.com/
Description: Enable the preloader for the page load
Author: QantumThemes
Version: 1.0.0
*/

if(!function_exists('is_customize_preview')){
	function is_customize_preview(){
		global $wp_customize; 
	  	return is_a( $wp_customize, 'WP_Customize_Manager' ) && $wp_customize->is_preview();  
	}
}
function qt_preloader(){
	if(!is_customize_preview() && !wp_is_mobile()){
		wp_enqueue_script( 'swipeScript',plugins_url( '/qt-preloader.jquery.js' , __FILE__ ), $deps = array("jquery"), $ver = false, $in_footer = true );
		wp_enqueue_style( 'swipeStyle',plugins_url( '/qt-preloader.css' , __FILE__ ),false);
		echo '<style type="text/css" media="screen">	.qw-loading-page.bg {     background-color: #'.get_theme_mod("background_color").';	}</style>';
	}
}



function qt_preloader_init(){
	if(!is_customize_preview()  && !wp_is_mobile()){
	 	echo '<div class="qw-loading-page bg" id="loaderMask"><span>0%</span><div class="qw-preloader" id="qwPagePreloader"><div class="circle"></div><div class="circle1"></div></div></div>';
	}
}
add_action("qt_after_body","qt_preloader_init",0);
add_action("wp_enqueue_scripts",'qt_preloader',-10);


