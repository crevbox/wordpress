<?php

	__('Save Changes', 'cqpim');

	// CQPIM Admin Functions
	
	function cqpim_register_session(){
	
		if( !session_id() )
		
			session_start();
			
	}
	
	add_action('init','cqpim_register_session');
	
	function cqpim_admin_header() {
	
		$screen = get_current_screen();
		
		if(!empty($screen->post_type)) {
		
			$cpt = get_post_type_object( $screen->post_type );
			
		}
		
		if(strpos($screen->base,'cqpim') !== false || strpos($screen->post_type,'cqpim') !== false) {
	
			$user = wp_get_current_user(); 
			
			$roles = $user->roles;
			
			$roles = array_values($roles);
			
			$role = $roles[0];
			
			$role = str_replace('cqpim', 'CQPIM', $role);
			
			$role = str_replace('_',' ', $role);
			
			$role = ucwords($role);
			
			$user_name = $user->display_name;		
			
			// Gravatar
			
			$email = $user->user_email;
			
			$size = 40;
			
			// Work out new messages
			
			$unread = cqpim_new_messages($user->ID);
			
			$unread_stat = isset($unread['read_val']) ? $unread['read_val'] : '';
			
			$unread_qty = isset($unread['new_messages']) ? $unread['new_messages'] : '';

			$avatar = get_option('cqpim_disable_avatars');
			
			$messaging = get_option('cqpim_enable_messaging');
			
			?>
			
			<div style="display:none" id="cqpim_overlay">
			
				<div id="cqpim_spinner">
				
					<img src="<?php echo plugins_url(  ) . '/cqpim/img/loading_spinner.gif'; ?>" />
				
				</div>
			
			</div>
			
			<?php if(!empty($avatar)) { ?>
			
				<style>
				
					#cqpim_admin_head {height:55px !important}
				
				</style>
			
			<?php } ?>
			
			<div id="cqpim_admin_head">
			
				<ul>
							
					<?php if(empty($avatar)) { echo '<li class="cqpim_avatar">' . get_avatar( $user->ID, 50, '', false, array('force_display' => true) ) . '</li>'; } else { echo '<li style="height:50px; width:1px;margin-left:-1px">&nbsp;</li>'; } ?>
					
					<li><span class="cqpim_username rounded_2"><i class="fa fa-user-circle" aria-hidden="true"></i> <?php echo $user_name; ?></span></li>
					
					<li><span class="cqpim_role rounded_2"><i class="fa fa-users" aria-hidden="true"></i> <?php echo $role; ?></span></li>
					
					<?php if(!empty($messaging)) { ?>
					
						<li>
						
							<span class="cqpim_icon">
						
								<a <?php if(!empty($unread_qty)) { echo 'class="cqpim_active"'; } ?> href="<?php echo admin_url() . 'admin.php?page=cqpim-messages'; ?>"><i class="fa fa-envelope-open" aria-hidden="true" title="<?php _e('Messages', 'cqpim'); ?>"></i></a>
								
								<?php if(!empty($unread_qty)) { ?>
								
									<span class="cqpim_counter"><?php echo $unread_qty; ?></span>
								
								<?php } ?>
							
							</span>
						
						</li>
					
					<?php } ?>
					
					<?php //if(current_user_can('cqpim_team_edit_profile')) { ?>
					
					<?php if(0) { ?>
					
						<li>
						
							<span class="cqpim_icon">
						
								<a href="<?php echo admin_url() . 'admin.php?page=cqpim-manage-profile'; ?>"><i class="fa fa-sliders" aria-hidden="true" title="<?php _e('Edit my Profile', 'cqpim'); ?>"></i></a>
							
							</span>
						
						</li>
					
					<?php } ?>
					
					<li>
					
						<span class="cqpim_icon">
					
							<?php
							
								$login_page_id = get_option('cqpim_login_page');
								
								$login_url = get_option('cqpim_logout_url');
								
								if(empty($login_url)) {
								
									$login_url = get_the_permalink($login_page_id);
									
								}
							
							?>
						
							<a href="<?php echo wp_logout_url($login_url); ?>"><i class="fa fa-sign-out" aria-hidden="true" title="<?php _e('Log Out', 'cqpim'); ?>"></i></a>
					
						</span>
					
					</li>
				
				</ul>
			
			</div>
			
			<div id="cqpim_admin_title">
			
				<?php if($screen->base == 'toplevel_page_cqpim-dashboard') {
				
					_e('Dashboard', 'cqpim');
					
					echo '<div class="clear"></div>';
				
				} else {
					
					if($screen->id == 'cqpim_client') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'edit.php?post_type=cqpim_client' . '">' . __('Clients', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_tasks') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-tasks' . '">' . __('My Tasks', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_project') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'edit.php?post_type=cqpim_project' . '">' . __('Projects', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_quote') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'edit.php?post_type=cqpim_quote' . '">' . __('Quotes', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_invoice') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'edit.php?post_type=cqpim_invoice' . '">' . __('Invoices', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_teams') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'edit.php?post_type=cqpim_teams' . '">' . __('Team Members', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_support') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'admin.php?page=support-tickets' . '">' . __('Support Tickets', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_templates') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'edit.php?post_type=cqpim_templates' . '">' . __('Milestone Templates', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_terms') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'edit.php?post_type=cqpim_terms' . '">' . __('Terms Templates', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_supplier') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'edit.php?post_type=cqpim_supplier' . '">' . __('Suppliers', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_expense') {
						
						$id = get_the_ID();
						
						$post = get_post($id);		

						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-expenses' . '">' . __('Expenses', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo $post->post_title;
						
						echo '<div class="clear"></div>';						
						
					} elseif($screen->id == 'cqpim_page_cqpim-messages') {
					
						$conversation = isset($_GET['conversation']) ? $_GET['conversation'] : '';
						
						if(!empty($conversation)) {
							
							$args = array(
							
								'post_type' => 'cqpim_conversations',
								
								'post_status' => 'private',
								
								'posts_per_page' => 1,

								'meta_key' => 'conversation_id',
								
								'meta_value' => $conversation
							
							);
							
							$conversations = get_posts($args);
							
							$conversation = isset($conversations[0]) ? $conversations[0] : array();
							
							echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
							
							echo '<a href="' . admin_url() . 'admin.php?page=cqpim-messages' . '">' . __('My Messages', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
							echo esc_html( $conversation->post_title ) ;
							
							echo '<div class="clear"></div>';							
							
						} else {
							
							echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
						
							echo esc_html( get_admin_page_title() ) ;
							
							echo '<div class="clear"></div>';							
							
						}
					
					} else {
					
						echo '<a href="' . admin_url() . 'admin.php?page=cqpim-dashboard' . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ';
					
						echo esc_html( get_admin_page_title() ) ;
						
						echo '<div class="clear"></div>';
					
					}
					
				} ?>
			
			</div>
			
			<?php //print_r($screen);
			
			//echo '<br /><br />';
			
			//print_r($post); ?>
			
	<?php }
	
	}
	
	add_action('in_admin_header', 'cqpim_admin_header', 10);
	
	function cqpim_admin_head() {
	
		$screen = get_current_screen();
		
		if(strpos($screen->base,'cqpim') !== false || strpos($screen->post_type,'cqpim') !== false) { ?>
		
			<style>			
				#wpcontent {
					margin-left: 140px;
				}
				#wpbody {
					padding-left:20px;
				}
				.wrap h1 {
					display:none;
				}
				.wrap {
					clear:both;
					padding-top:10px;
				}
				@media only screen and (max-width: 960px) {
					#wpbody {
						padding-left:0px;
					}
				}			
			</style>
		
		<?php }
		
	}
	
	add_action('admin_head', 'cqpim_admin_head', 10);
	
	function cqpim_get_user_id_by_display_name( $display_name ) {
		
		global $wpdb;

		if ( ! $user = $wpdb->get_row( $wpdb->prepare(
		
			"SELECT `ID` FROM $wpdb->users WHERE `display_name` = %s", $display_name
			
		) ) )
		
			return false;

		return $user->ID;
	}
	
	function cqpim_restrict_dash() {
		
		$user = wp_get_current_user();
		
		$roles = $user->roles;
		
		if(!is_array($roles)) {
			
			$roles = array($roles);
			
		}
		
		foreach($roles as $role) {
			
			if (strpos($role, 'cqpim_') !== false) {
				
				$restrict = true;
				
			}			
			
		}
		
		if(!empty($restrict) && !empty($GLOBALS['menu'])) {
		
			foreach($GLOBALS['menu'] as $key => $item) {
					
				if($item[0] != 'CQPIM') {
					
					unset($GLOBALS['menu'][$key]);
					
				}
				
			}
		
		}
		
	}
	
	add_filter('months_dropdown_results', 'cqpim_remove_date_filters');
	
	function cqpim_remove_date_filters() {
		
		$screen = get_current_screen();
		
		global $typenow;
		
		if(strpos($typenow, 'cqpim') !== false) {
			
			return array();
			
		}
		
	}
	
	add_action( 'wp_before_admin_bar_render', 'cqpim_add_all_node_ids_to_toolbar' );

	function cqpim_add_all_node_ids_to_toolbar() {
		
		$user = wp_get_current_user();
		
		$roles = $user->roles;
		
		if(!is_array($roles)) {
			
			$roles = array($roles);
			
		}
		
		foreach($roles as $role) {
			
			if (strpos($role, 'cqpim_') !== false) {
				
				$restrict = true;
				
			}			
			
		}
		
		if(!empty($restrict)) {

			global $wp_admin_bar;
			
			$all_toolbar_nodes = $wp_admin_bar->get_nodes();

			if ( $all_toolbar_nodes ) {
				
				foreach ( $all_toolbar_nodes as $node  ) {
							
					if($node->id != 'menu-toggle') {

						$wp_admin_bar->remove_node($node->id);
						
					}
					
				}
				
			}
		
		}
		
	}
	
	add_action('admin_init', 'cqpim_restrict_dash');
	
	// Allow Email Login
	
	add_filter('authenticate', 'cqpim_allow_email_login', 20, 3);

	function cqpim_allow_email_login( $user, $username, $password ) {
	
		if ( is_email( $username ) ) {
		
			$user = get_user_by('email',  $username );
			
			if ( $user ) $username = $user->user_login;
			
		}
		
		return wp_authenticate_username_password( null, $username, $password );
		
	}

	// Custom page template for cqpim client login & dash
	
	function custom_cqpim_template($page_template) {
		 
		$login_page = get_option('cqpim_login_page');
		
		$dash_page = get_option('cqpim_client_page');
		
		$reset_page = get_option('cqpim_reset_page');
		
		$dash_type = get_option('client_dashboard_type');
		
		if($dash_type == 'inc') {

			if (is_page($login_page)) {
			
				$page_template = dirname( __FILE__ ) . '/templates-inc/login.php';
				  
			}
			
			if (is_page($dash_page)) {
			
				$page_template = dirname( __FILE__ ) . '/templates-inc/client-dashboard.php';
				  
			}
			
			if (is_page($reset_page)) {
			
				$page_template = dirname( __FILE__ ) . '/templates-inc/reset.php';
				  
			}
		
		} else {
		
			if (is_page($login_page)) {
			
				$page_template = dirname( __FILE__ ) . '/templates-fe/login.php';
				  
			}
			
			if (is_page($dash_page)) {
			
				$page_template = dirname( __FILE__ ) . '/templates-fe/client-dashboard.php';
				  
			}	

			if (is_page($reset_page)) {
			
				$page_template = dirname( __FILE__ ) . '/templates-fe/reset.php';
				  
			}				
		
		}
		 
		return $page_template;
		 
	}
	
	add_filter( 'page_template', 'custom_cqpim_template', 50 ); 
	
	add_action( "wp_ajax_nopriv_cqpim_ajax_login", 

			"cqpim_ajax_login");

	add_action( "wp_ajax_cqpim_ajax_login", 

		  	"cqpim_ajax_login");	
	
	function cqpim_ajax_login() {
	
		//check_ajax_referer( 'ajax-login-nonce', 'security' );
		
		$username = isset($_POST['username']) ? $_POST['username'] : '';
		
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		
		$dash_page = get_option('cqpim_client_page');
		
		if(empty($username) || empty($password)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Please enter a username and password', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();			
		
		} else {
		
			$creds = array();
			
			$creds['user_login'] = $username;
			
			$creds['user_password'] = $password;
			
			$creds['remember'] = false;
		
			//$login = wp_authenticate($username, $password);
			
			$login = wp_signon( $creds, false );
			
			if(is_wp_error($login)) {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Login Failed. Please try again.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();			
			
			} else {
			
				$roles = $login->roles;
				
				if(in_array('cqpim_client', $roles)) {
				
					$redirect = get_the_permalink($dash_page);
					
				} else {
				
					$redirect = admin_url() . 'admin.php?page=cqpim-dashboard';
					
				}
			
				$return =  array( 

					'error' 	=> false,
					
					'message' => '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Login Successful. Redirecting to your dashboard.', 'cqpim') . '</div>',

					'redirect' 	=> $redirect,

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();			
			
			}
		
		}	
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_ajax_reset", 

			"cqpim_ajax_reset");

	add_action( "wp_ajax_cqpim_ajax_reset", 

		  	"cqpim_ajax_reset");
	
	function cqpim_ajax_reset() {
	
		$username = isset($_POST['username']) ? $_POST['username'] : '';
		
		if(empty($username)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('You must enter an email address.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();			
		
		} else {
		
			$user = get_user_by('login', $username);
			
			if(empty($user)) {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('User not found.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();				
			
			} else {
			
				$string = cqpim_random_string(10);
				
				$hash = md5($string);
				
				update_user_meta( $user->ID, 'reset_hash', $hash );
				
				$reset = get_option('cqpim_reset_page');
				
				$reset = get_the_permalink($reset);
				
				$reset = $reset . '?h=' . $hash;
				
				$to = $user->user_email;
				
				$telephone = get_option('company_telephone');
				
				$sender_name = get_option('company_name');
				
				$sender_email = get_option('company_sales_email');
				
				$subject = get_option('client_password_reset_subject');
				
				$content = get_option('client_password_reset_content');
				
				$content = str_replace('%%CLIENT_NAME%%', $user->display_name, $content);
				
				$content = str_replace('%%PASSWORD_RESET_LINK%%', $reset, $content);
				
				$content = str_replace('%%COMPANY_NAME%%', $sender_name, $content);
				
				$content = str_replace('%%COMPANY_TELEPHONE%%', $telephone, $content);
				
				$content = str_replace('%%COMPANY_SALES_EMAIL%%', $sender_email, $content);
				
				$subject = str_replace('%%COMPANY_NAME%%', $sender_name, $subject);
				
				$attachments = array();
				
				if(cqpim_send_emails( $to, $subject, $content, '', $attachments, 'sales' )) {
			
					$return =  array( 

						'error' 	=> false,

						'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Further instructions have been sent to your registered address.', 'cqpim') . '</div>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);

					exit();	

				} else {
				
					$return =  array( 

						'error' 	=> true,

						'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The email failed to send. Please try again.', 'cqpim') . '</div>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);

					exit();					
				
				}
			
			}
		
		}	
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_ajax_reset_conf", 

			"cqpim_ajax_reset_conf");

	add_action( "wp_ajax_cqpim_ajax_reset_conf", 

		  	"cqpim_ajax_reset_conf");
	
	function cqpim_ajax_reset_conf() {
	
		$hash = isset($_POST['hash']) ? $_POST['hash'] : '';
		
		$pass = isset($_POST['pass']) ? $_POST['pass'] : '';
		
		$pass2 = isset($_POST['pass2']) ? $_POST['pass2'] : '';
		
		if(empty($pass) || empty($pass2)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('You must fill in both fields.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();			
		
		} else {
		
			if($pass != $pass2) {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Passwords do not match.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();			
			
			} else {
			
				if (strlen($pass) < 8 || !preg_match("#[0-9]+#", $pass) || !preg_match("#[a-zA-Z]+#", $pass)) {
				
					$return =  array( 

						'error' 	=> true,

						'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Passwords should be at least 8 characters and should contain at least one letter and one number.', 'cqpim') . '</div>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);

					exit();					
				
				} else {
				
					$args = array(
					
						'meta_key' => 'reset_hash',
						
						'meta_value' => $hash,
						
						'number' => 1,
					
					);
					
					$users = get_users($args);
					
					if(empty($users[0])) {
					
						$return =  array( 

							'error' 	=> true,

							'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Invalid User or reset link', 'cqpim') . '</div>',

						);
						
						header('Content-type: application/json');
						
						echo json_encode($return);

						exit();					
					
					} else {
					
						$user = $users[0];
						
						wp_set_password($pass, $user->ID);
						
						delete_user_meta($user->ID, 'reset_hash');

						$return =  array( 

							'error' 	=> false,

							'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Your password has been reset, you can now log in with your email address and new password.', 'cqpim') . '</div>',

						);
						
						header('Content-type: application/json');
						
						echo json_encode($return);

						exit();	
					
					}
				
				}

			}
		
		}	
	
	}
	
	// Do not allow Clients into WP-ADMIN

	! defined( 'ABSPATH' ) AND exit;

	function cqpim_client_no_admin_access() {
	
		$redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url( '/' );
		
		$user = wp_get_current_user();
		
		if ( in_array('cqpim_client', $user->roles) && strpos($_SERVER['PHP_SELF'], '/admin-ajax.php') === false && strpos($_SERVER['PHP_SELF'], '/async-upload.php') === false || 

			in_array('cqpimuploader', $user->roles) && strpos($_SERVER['PHP_SELF'], '/admin-ajax.php') === false && strpos($_SERVER['PHP_SELF'], '/async-upload.php') === false
			
		)
			
			exit( wp_redirect( $redirect ) );
			
	}
	
	add_action( 'admin_init', 'cqpim_client_no_admin_access', 100 );

	
	// Hide Admi bar on CQPIM Login and Dash
	
	function cqpim_hide_admin_bar(){
	
		$client_login = get_option('cqpim_login_page');
		
		$client_dash = get_option('cqpim_client_page');
		
		$client_reset = get_option('cqpim_reset_page');
		
		$user = wp_get_current_user();
		
		$roles = $user->roles;
		
		if(is_page($client_login) || is_page($client_dash) || is_page($client_reset) || in_array('cqpim_client', $roles) || in_array('cqpimuploader', $roles)) {
		
			return false;
		
		} else {
		
			if($user->ID > 0) {
			
				return true;
				
			}
		
		}
		
	}
	
	add_filter( 'show_admin_bar' , 'cqpim_hide_admin_bar', 20);
	
	function cqpim_hide_admin_bar_uploader(){
		
		$user = wp_get_current_user();
		
		$roles = $user->roles;
		
		if(in_array('cqpimuploader', $user->roles)) {
		
			return false;
		
		}
		
	}
	
	add_filter( 'show_admin_bar' , 'cqpim_hide_admin_bar_uploader', 20);
	
	// Redirect Clients to Client Dashboard on Login
	
	function cqpim_client_login_redirect( $redirect_to, $request, $user ) {
		
		$client_dash = get_option('cqpim_client_page');
		
		if(strpos($request,'?redirect=') !== false) {
		
			$redirect = substr($request, strpos($request, "?redirect"));
		
			$client_dash_link = get_the_permalink($client_dash) . '' . $redirect;		
		
		} else {
		
			$client_dash_link = get_the_permalink($client_dash);
		
		}
		
		$user_dash_link = admin_url() . 'admin.php?page=cqpim-dashboard';
		
		if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		
			//check for admins
			
			if ( in_array('administrator', $user->roles ) ) {
			
				// redirect them to the default place
				
				return $redirect_to;
				
			}
			
			if ( in_array('cqpim_admin', $user->roles ) ) {
			
				return $user_dash_link;
				
			}
			
			foreach($user->roles as $role) {
				
				if (strpos($role, 'cqpim_') !== false) {
					
					return $user_dash_link;
					
				}
				
			}
				
			return $redirect_to;				
			
		} else {
		
			return $redirect_to;
			
		}
		
	}

	add_filter( 'login_redirect', 'cqpim_client_login_redirect', 10, 3 );


	if( function_exists( 'add_image_size' ) ) { 

		// pages which list acts

		add_image_size( 'company-logo', 400, 100, true );
		add_image_size( 'cqpim-login-background', 2000, 1200, true );

	}
	
	// Remove View Link from CPTs
	
	add_filter( 'post_row_actions', 'cqpim_remove_row_actions', 10, 1 );
	

	function cqpim_remove_row_actions( $actions ) {
	
		if( get_post_type() === 'cqpim_teams' ) {
		
			unset( $actions['view'] );
			
			unset($actions['inline hide-if-no-js']);
			
		}
		
		if( get_post_type() === 'cqpim_client' ) {
		
			unset( $actions['view'] );
			
			unset($actions['inline hide-if-no-js']);
			
		}
		
		if( get_post_type() === 'cqpim_quote' ) {
		
			unset( $actions['view'] );
			
			unset($actions['inline hide-if-no-js']);
			
		}
		
		if( get_post_type() === 'cqpim_project' ) {
		
			unset( $actions['view'] );
			
			unset($actions['inline hide-if-no-js']);
			
		}
		
		if( get_post_type() === 'cqpim_project' ) {
		
			unset( $actions['view'] );
			
			unset($actions['inline hide-if-no-js']);
			
		}
		
		if( get_post_type() === 'cqpim_forms' ) {
		
			unset( $actions['view'] );
			
			unset($actions['inline hide-if-no-js']);
			
		}
		
		if( get_post_type() === 'cqpim_supplier' ) {
		
			unset( $actions['view'] );
			
			unset($actions['inline hide-if-no-js']);
			
		}
		
		if( get_post_type() === 'cqpim_expense' ) {
		
			unset( $actions['view'] );
			
			unset($actions['inline hide-if-no-js']);
			
		}
			
		return $actions;
		
	}
	
	//add_filter( 'custom_menu_order', 'cqpim_submenu_order' );

	function cqpim_submenu_order( $menu_ord ) 
	{
		global $submenu;

		// Enable the next line to inspect the $submenu values
		// echo '<pre style="padding-left:400px">'.print_r($submenu['cqpim-dashboard'],true).'</pre>';

		$arr = array();
		
		foreach($submenu['cqpim-dashboard'] as $menu) {
			
			if($menu[0] == 'CQPIM') {
				
				$arr[0] = $menu;
				
			}
			
			if($menu[0] == 'My Calendar') {
				
				$arr[1] = $menu;
				
			}
			
			if($menu[0] == 'My Messages') {
				
				$arr[2] = $menu;
				
			}
			
			if($menu[0] == 'My Tasks') {
				
				$arr[3] = $menu;
				
			}
			
			if($menu[0] == 'All Tasks (Admin)') {
				
				$arr[4] = $menu;
				
			}
			
			if($menu[0] == 'My Expenses') {
				
				$arr[5] = $menu;
				
			}
			
			if($menu[0] == 'All Expenses (Admin)') {
				
				$arr[6] = $menu;
				
			}
			
			if($menu[0] == 'Clients') {
				
				$arr[7] = $menu;
				
			}
			
			if($menu[0] == 'Suppliers') {
				
				$arr[8] = $menu;
				
			}
			
			if($menu[0] == 'Quotes / Estimates') {
				
				$arr[9] = $menu;
				
			}
			
			if($menu[0] == 'Quote Forms') {
				
				$arr[10] = $menu;
				
			}
			
			if($menu[0] == 'Projects') {
				
				$arr[11] = $menu;
				
			}
			
			if($menu[0] == 'Terms Templates') {
				
				$arr[12] = $menu;
				
			}
			
			if($menu[0] == 'Milestone Templates') {
				
				$arr[13] = $menu;
				
			}
			
			if($menu[0] == 'Invoices') {
				
				$arr[14] = $menu;
				
			}
			
			if($menu[0] == 'Support Tickets') {
				
				$arr[15] = $menu;
				
			}
			
			if($menu[0] == 'Team Members') {
				
				$arr[16] = $menu;
				
			}
			
			if($menu[0] == 'Roles & Permissions') {
				
				$arr[17] = $menu;
				
			}
			
			if($menu[0] == 'Custom Fields') {
				
				$arr[18] = $menu;
				
			}
			
			if($menu[0] == 'Settings') {
				
				$arr[19] = $menu;
				
			}
			
			if($menu[0] == 'CQPIM Help') {
				
				$arr[20] = $menu;
				
			}
			
		}		
		
		$submenu['cqpim-dashboard'] = $arr;

		return $menu_ord;
	}
	
	// Edit Team Member email if changed in WP
	
    add_action( 'profile_update', 'cqpim_user_update_profile', 10, 2 );

    function cqpim_user_update_profile( $user_id, $old_user_data ) {
	
		$user = get_userdata( $user_id );
	
		$args = array(
		
			'post_type' => 'cqpim_teams',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private',
			
		);
		
		$team_members = get_posts($args);
		
		foreach($team_members as $member) {
		
			$team_details = get_post_meta($member->ID, 'team_details', true);
			
			if($team_details['user_id'] == $user_id) {
				
				$team_details['team_name'] = $user->display_name;
				
				$team_details['team_email'] = $user->user_email;
				
				$team_details['team_perms'] = $user->roles;
				
				update_post_meta($member->ID, 'team_details', $team_details);
			
			}
		
		}
		
		$args = array(
		
			'post_type' => 'cqpim_client',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private',
			
		);
		
		$clients = get_posts($args);
		
		foreach($clients as $member) {
		
			$team_details = get_post_meta($member->ID, 'client_details', true);
			
			if($team_details['user_id'] == $user_id) {
				
				$team_details['client_contact'] = $user->display_name;
				
				$team_details['client_email'] = $user->user_email;
				
				update_post_meta($member->ID, 'client_details', $team_details);
			
			}
		
		}
        
		
    }	

	// Get client IP
	
	function cqpim_get_client_ip() {
	
		$ipaddress = '';
		
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			
		else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			
		else if(!empty($_SERVER['HTTP_X_FORWARDED']))
		
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			
		else if(!empty($_SERVER['HTTP_FORWARDED_FOR']))
		
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			
		else if(!empty($_SERVER['HTTP_FORWARDED']))
		
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
			
		else if(!empty($_SERVER['REMOTE_ADDR']))
		
			$ipaddress = $_SERVER['REMOTE_ADDR'];
			
		else
		
			$ipaddress = 'UNKNOWN';
			
		return $ipaddress;
		
	}

	// Generate a Random string for Quote / Invoice Passwords

	function cqpim_random_string( $length = 10 ) {

		$key = '';

		$keys = array_merge( range(0,9), range('a', 'z') );

		for( $i=0; $i < $length; $i++ ) {

			$key .= $keys[array_rand($keys)];

		}

		return $key;

	}	

	// Default Titles

	add_filter( 'default_title', 'cqpim_set_default_quote_post_title', 10, 2 );

	function cqpim_set_default_quote_post_title( $title, $post ){

		if( $post->post_type == 'cqpim_quote' ){

			$id = $post->ID;

			$title = "%%CLIENT_COMPANY%% - %%TYPE%%: %%QUOTE_REF%%";

			return $title;

		}
		
		if( $post->post_type == 'cqpim_client' ){

			$id = $post->ID;

			$title = '%%CLIENT_COMPANY%%';

			return $title;

		}
		
		if( $post->post_type == 'cqpim_invoice' ){

			$id = $post->ID;

			$title = cqpim_get_invoice_id();

			return $title;

		}
		
		if( $post->post_type == 'cqpim_teams' ){

			$id = $post->ID;

			$title = "%%NAME%%";

			return $title;

		}
		
		if( $post->post_type == 'cqpim_support' ){

			$id = $post->ID;

			$title = "$id";

			return $title;

		}

	}
	
	// No index for Quotes, Projects, Invoices
	
	function no_index_cqpim_types() {
	
		if(is_singular('cqpim_quote') || is_singular('cqpim_project') || is_singular('cqpim_invoice') || is_singular('cqpim_support')) {
		
			echo '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">';
		
		}
	
	}
	
	add_action('wp_head', 'no_index_cqpim_types');
	
	// Replacement Patterns
	
	// Replaces Tags in content/titles/emails with values
	
	function cqpim_replacement_patterns($content, $post_id, $type) {
	
		// Current User
		
		$current_user_tag = '%%CURRENT_USER%%';
		
		$current_user = wp_get_current_user();
      	
		if ( !($current_user instanceof WP_User) )
          
			return;
		
		$current_user = $current_user->display_name;
		
		$content = str_replace($current_user_tag, $current_user, $content);
	
		// Company Tags
	
		$company_name_tag = '%%COMPANY_NAME%%';
		
		$company_number_tag = '%%COMPANY_NUMBER%%';
		
		$company_address_tag = '%%COMPANY_ADDRESS%%';
		
		$company_postcode_tag = '%%COMPANY_POSTCODE%%';
		
		$company_telephone_tag = '%%COMPANY_TELEPHONE%%';
		
		$company_sales_email_tag = '%%COMPANY_SALES_EMAIL%%';
		
		$company_accounts_email_tag = '%%COMPANY_ACCOUNTS_EMAIL%%';
		
		$account_name_tag = '%%ACCOUNT_NAME%%';
		
		$sort_code_tag = '%%SORT_CODE%%';
		
		$account_number_tag = '%%ACCOUNT_NUMBER%%';
		
		$iban_tag = '%%IBAN%%';
		
		// Company Data
		
		$company_name = get_option('company_name');
		
		$company_number = get_option('company_number');
		
		$company_address = get_option('company_address');
		
		$company_postcode = get_option('company_postcode');
		
		$company_telephone = get_option('company_telephone');
		
		$company_sales_email = get_option('company_sales_email');
		
		$company_accounts_email = get_option('company_accounts_email');

		$account_name = get_option('company_bank_name');
		
		$sort_code = get_option('company_bank_sc');
		
		$account_number = get_option('company_bank_ac');
		
		$iban = get_option('company_bank_iban');	

		// Company Replacements
		
		$content = str_replace($company_name_tag, $company_name, $content);
		
		$content = str_replace($company_number_tag, $company_number, $content);
		
		$content = str_replace($company_address_tag, $company_address, $content);
		
		$content = str_replace($company_postcode_tag, $company_postcode, $content);
		
		$content = str_replace($company_telephone_tag, $company_telephone, $content);
		
		$content = str_replace($company_sales_email_tag, $company_sales_email, $content);
		
		$content = str_replace($company_accounts_email_tag, $company_accounts_email, $content);
		
		$content = str_replace($account_name_tag, $account_name, $content);
		
		$content = str_replace($sort_code_tag, $sort_code, $content);
		
		$content = str_replace($account_number_tag, $account_number, $content);
		
		$content = str_replace($iban_tag, $iban, $content);
		
		// Client Tags
		
		$client_name_tag = '%%CLIENT_NAME%%';
		
		$client_company_tag = '%%CLIENT_COMPANY%%';
		
		$client_address_tag = '%%CLIENT_ADDRESS%%';
		
		$client_postcode_tag = '%%CLIENT_POSTCODE%%';
		
		$client_telephone_tag = '%%CLIENT_TELEPHONE%%';
		
		$client_email_tag = '%%CLIENT_EMAIL%%';
		
		$client_ref_tag = '%%CLIENT_REF%%';	
		
		// Client Data
		
		if($type == 'quote') {
		
			$quote_details = get_post_meta($post_id, 'quote_details', true);
			
			$client_id = $quote_details['client_id'];
		
		} elseif($type == 'project') {
		
			$project_details = get_post_meta($post_id, 'project_details', true);
			
			$client_id = $project_details['client_id'];		
		
		} elseif($type == 'invoice') {
		
			$invoice_details = get_post_meta($post_id, 'invoice_details', true);
			
			$client_id = $invoice_details['client_id'];		
		
		} else {
		
			$client_id = $post_id;
			
		}
		
		$client_details = get_post_meta($client_id, 'client_details', true);
			
		$client_ref = isset($client_details['client_ref']) ? $client_details['client_ref'] : '';
		
		$client_company = isset($client_details['client_company']) ? $client_details['client_company'] : '';
		
		$client_name = isset($client_details['client_contact']) ? $client_details['client_contact'] : '';
		
		$client_address = isset($client_details['client_address']) ? $client_details['client_address'] : '';
		
		$client_postcode = isset($client_details['client_postcode']) ? $client_details['client_postcode'] : '';
		
		$client_telephone = isset($client_details['client_telephone']) ? $client_details['client_telephone'] : '';
		
		$client_email = isset($client_details['client_email']) ? $client_details['client_email'] : '';

		// Client Replacements
		
		$content = str_replace($client_name_tag, $client_name, $content);
		
		$content = str_replace($client_company_tag, $client_company, $content);
		
		$content = str_replace($client_address_tag, $client_address, $content);
		
		$content = str_replace($client_postcode_tag, $client_postcode, $content);
		
		$content = str_replace($client_telephone_tag, $client_telephone, $content);
		
		$content = str_replace($client_email_tag, $client_email, $content);
		
		$content = str_replace($client_ref_tag, $client_ref, $content);
		
		if($type == 'task') {
		
			$task_title_tag = '%%TASK_TITLE%%';
			
			$task_status_tag = '%%TASK_STATUS%%';
			
			$task_priority_tag = '%%TASK_PRIORITY%%';
			
			$task_start_tag = '%%TASK_START%%';
			
			$task_end_tag = '%%TASK_DEADLINE%%';
			
			$task_est_tag = '%%TASK_EST%%';
			
			$task_pc_tag = '%%TASK_PC%%';
			
			$task_project_tag = '%%TASK_PROJECT%%';
			
			$task_milestone_tag = '%%TASK_MILESTONE%%';
			
			$task_owner_tag = '%%TASK_OWNER%%';
			
			$task_url_tag = '%%TASK_URL%%';
			
			$task_object = get_post($post_id);
			
			$task_title = $task_object->post_title;
			
			$task_details = get_post_meta($post_id, 'task_details', true);
			
			$task_status = isset($task_details['status']) ? $task_details['status'] : __('N/A', 'cqpim');
			
			if($task_status == 'pending') { $task_status = __('Pending', 'cqpim'); } 
			
			if($task_status == 'progress') { $task_status = __('In Progress', 'cqpim'); } 
			
			if($task_status == 'complete') { $task_status = __('Complete', 'cqpim'); } 
			
			if($task_status == 'on_hold') { $task_status = __('On Hold', 'cqpim'); } 
			
			$task_status = ucwords($task_status);
			
			$task_priority = isset($task_details['task_priority']) ? $task_details['task_priority'] : __('N/A', 'cqpim');
			
			if($task_priority == 'low') { $task_priority = __('Low', 'cqpim'); } 
			
			if($task_priority == 'normal') { $task_priority = __('Normal', 'cqpim'); } 
			
			if($task_priority == 'high') { $task_priority = __('High', 'cqpim'); } 
			
			if($task_priority == 'immediate') { $task_priority = __('Immediate', 'cqpim'); } 
			
			$task_priority = ucwords($task_priority);
			
			$task_start = isset($task_details['task_start']) ? $task_details['task_start'] : __('N/A', 'cqpim');
			
			if(!empty($task_start)) {
			
				$task_start = date(get_option('cqpim_date_format'), $task_start);
			
			} else {
			
				$task_start = __('N/A', 'cqpim');
			
			}
			
			$deadline = isset($task_details['deadline']) ? $task_details['deadline'] : __('N/A', 'cqpim');
			
			if(!empty($deadline)) {
			
				$deadline = date(get_option('cqpim_date_format'), $deadline);
				
			} else {
			
				$deadline = __('N/A', 'cqpim');
			
			}
			
			$task_est = isset($task_details['task_est_time']) ? $task_details['task_est_time'] : __('N/A', 'cqpim');
			
			$task_pc = isset($task_details['task_pc']) ? $task_details['task_pc'] . '%' : __('N/A', 'cqpim');
			
			$task_owner = get_post_meta($post_id, 'owner', true);
			
			$client_check = preg_replace('/[0-9]+/', '', $task_owner);
			
			if($client_check == 'C') {
			
				$client = true;
				
			}
			
			if($task_owner) {
			
				if($client == true) {
				
					$id = preg_replace("/[^0-9,.]/", "", $task_owner);
					
					$client_object = get_user_by('id', $id);
					
					$task_owner = $client_object->display_name;
				
				} else {
			
					$team_details = get_post_meta($task_owner, 'team_details', true);
					
					$team_name = isset($team_details['team_name']) ? $team_details['team_name']: '';
					
					if(!empty($team_name)) {
					
						$task_owner = $team_name;
					
					}
				
				}
				
			} else {
			
				$task_owner = '';
				
			}
			
			$task_owner = isset($task_owner) ? $task_owner : __('N/A', 'cqpim');
			
			$project_id = get_post_meta($post_id, 'project_id', true);
			
			$project_details = get_post_meta($project_id, 'project_details', true);
			
			$project_object = get_post($project_id);
			
			$project_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
			
			$project_ref = $project_object->post_title;
			
			$project_elements = get_post_meta($project_id, 'project_elements', true);
			
			$milestone_id = get_post_meta($post_id, 'milestone_id', true);
			
			$milestone_name = isset($project_elements[$milestone_id]['title']) ? $project_elements[$milestone_id]['title'] : __('N/A', 'cqpim');
			
			$user = wp_get_current_user();
			
			$roles = $user->roles;
			
			if(in_array('cqpim_client', $roles)) {
			
				$task_url = site_url() . '/wp-admin/post.php?post=' . $task_object->ID . '&action=edit';
				
			} else {
			
				$task_url = get_the_permalink($task_object->ID);
			
			}
			
			$content = str_replace($task_title_tag, $task_title, $content);
			
			$content = str_replace($task_status_tag, $task_status, $content);
			
			$content = str_replace($task_priority_tag, $task_priority, $content);
			
			$content = str_replace($task_start_tag, $task_start, $content);
			
			$content = str_replace($task_end_tag, $deadline, $content);
			
			$content = str_replace($task_est_tag, $task_est, $content);
			
			$content = str_replace($task_pc_tag, $task_pc, $content);
			
			$content = str_replace($task_owner_tag, $task_owner, $content);
			
			$content = str_replace($task_project_tag, $project_ref, $content);
			
			$content = str_replace($task_milestone_tag, $milestone_name, $content);
			
			$content = str_replace($task_url_tag, $task_url, $content);
			
			
		}

		// Support Tickets
		
		if($type == 'ticket') {
		
			$ticket_id_tag = '%%TICKET_ID%%';
			
			$ticket_title_tag = '%%TICKET_TITLE%%';
			
			$ticket_status_tag = '%%TICKET_STATUS%%';
			
			$ticket_priority_tag = '%%TICKET_PRIORITY%%';
			
			$ticket_owner_tag = '%%TICKET_OWNER%%';
			
			$ticket_update_tag = '%%TICKET_UPDATE%%';
			
			$ticket_id = $post_id;
			
			$ticket_title = get_the_title($post_id);
			
			$ticket_title = str_replace('Private:', '', $ticket_title);
			
			$ticket_owner = get_post_meta($post_id, 'ticket_owner', true);
			
			$owner_details = get_post_meta($ticket_owner, 'team_details', true);
			
			$ticket_owner = isset($owner_details['team_name']) ? $owner_details['team_name'] : '';
			
			$ticket_status = get_post_meta($post_id, 'ticket_status', true);
			
			$ticket_status = ucfirst($ticket_status);
			
			$ticket_priority = get_post_meta($post_id, 'ticket_priority', true);
			
			$ticket_priority = ucfirst($ticket_priority);
			
			$content = str_replace($ticket_owner_tag, $ticket_owner, $content);
			
			$content = str_replace($ticket_title_tag, $ticket_title, $content);
			
			$content = str_replace($ticket_id_tag, $ticket_id, $content);
			
			$content = str_replace($ticket_status_tag, $ticket_status, $content);
			
			$content = str_replace($ticket_priority_tag, $ticket_priority, $content);
			
			$ticket_updates = get_post_meta($post_id, 'ticket_updates', true);
			
			if(empty($ticket_updates)) {
				
				$ticket_updates = array();
				
			}
			
			$ticket_updates = array_reverse($ticket_updates);
			
			$ticket_update = $ticket_updates[0]['details'];
			
			$content = str_replace($ticket_update_tag, $ticket_update, $content);
		
		}
		
		if($type == 'team') {
		
			$team_name_tag = '%%TEAM_NAME%%';
			
			$team_email_tag = '%%TEAM_EMAIL%%';
			
			$team_telephone_tag = '%%TEAM_TELEPHONE%%';
			
			$team_job_tag = '%%TEAM_JOB%%';
			
			$team_details = get_post_meta($post_id, 'team_details', true);
			
			$team_name = isset($team_details['team_name']) ? $team_details['team_name'] : '';
			
			$team_email = isset($team_details['team_email']) ? $team_details['team_email'] : '';
			
			$team_telephone = isset($team_details['team_telephone']) ? $team_details['team_telephone'] : '';

			$team_job = isset($team_details['team_job']) ? $team_details['team_job'] : '';
			
			$content = str_replace($team_name_tag, $team_name, $content);
		
			$content = str_replace($team_email_tag, $team_email, $content);

			$content = str_replace($team_telephone_tag, $team_telephone, $content);
		
			$content = str_replace($team_job_tag, $team_job, $content);			
		
		}

		if($type == 'quote') {		
		
			// Quote Tags
			
			$quote_ref_tag = '%%QUOTE_REF%%';
			
			$quote_link_tag = '%%QUOTE_LINK%%';
			
			$quote_type_tag = '%%TYPE%%';
			
			$client_login_tag = '%%CQPIM_LOGIN%%';
			
			// Quote Data
			
			$quote_details = get_post_meta($post_id, 'quote_details', true);
			
			$quote_type = $quote_details['quote_type'];
			
			if($quote_type == 'estimate') {
			
				$quote_type = __('estimate', 'cqpim');
				
			} else {
			
				$quote_type = __('quote', 'cqpim');
			
			}
			
			$quote_link = get_the_permalink($post_id);
			
			$quote_object = get_post($post_id);
			
			$quote_ref = $quote_object->post_title;
			
			$quote_password = md5( $quote_object->post_password );
			
			$quote_link = $quote_link . '?pwd=' . $quote_password;
			
			$quote_link2 = get_the_permalink($post_id);
			
			// Quote Replacements
			
			//$content = str_replace($client_login_tag, $quote_link2, $content);
			
			$content = str_replace($quote_ref_tag, $quote_ref, $content);
			
			$content = str_replace($quote_link_tag, $quote_link, $content);
			
			$content = str_replace($quote_type_tag, $quote_type, $content);
		
		}
		
		if($type == 'project') {		
		
			// Project Tags
			
			$contract_link_tag = '%%CONTRACT_LINK%%';
			
			$summary_link_tag = '%%SUMMARY_LINK%%';
			
			$project_ref_tag = '%%PROJECT_REF%%';
			
			$project_type_tag = '%%TYPE%%';
			
			// Project Data
			
			$project_details = get_post_meta($post_id, 'project_details', true);
			
			$type = isset($project_details['quote_type']) ? $project_details['quote_type'] : '';
			
			$project_link = get_the_permalink($post_id);
			
			$project_object = get_post($post_id);
			
			$project_ref = $project_object->post_title;
			
			$project_password = md5( $project_object->post_password );
			
			$contract_link = $project_link . '?pwd=' . $project_password . '&page=contract';
			
			$summary_link = $project_link . '?pwd=' . $project_password . '&page=summary';
			
			// Project Replacements
			
			$content = str_replace($contract_link_tag, $contract_link, $content);
			
			$content = str_replace($summary_link_tag, $summary_link, $content);
			
			$content = str_replace($project_ref_tag, $project_ref, $content);
			
			$content = str_replace($project_type_tag, $type, $content);
		
		}
		
		if($type == 'invoice') {		
		
			// Invoice Tags
			
			$invoice_id_tag = '%%INVOICE_ID%%';
			
			$invoice_link_tag = '%%INVOICE_LINK%%';
			
			// Invoice Data
			
			$invoice_object = get_post($post_id);
			
			$invoice_id = get_post_meta($post_id, 'invoice_id', true);
			
			$invoice_link = get_the_permalink($post_id);
			
			$invoice_password = md5( $invoice_object->post_password );
			
			$invoice_link = $invoice_link . '?page=print&pwd=' . $invoice_password;
			
			// Invoice Replacements
			
			$content = str_replace($invoice_id_tag, $invoice_id, $content);
			
			$content = str_replace($invoice_link_tag, $invoice_link, $content);
		
		}
		
		// CQPIM Login
		
		$client_login_tag = '%%CQPIM_LOGIN%%';
		
		$client_login = get_option('cqpim_login_page');
		
		$client_login = get_the_permalink($client_login);
		
		$content = str_replace($client_login_tag, $client_login, $content);
		
		// client Passwords
		
		$client_password_tag = '%%CLIENT_PASSWORD_LINK%%';
		
		$reset_page = get_option( 'cqpim_reset_page' );
		
		$client_password = get_the_permalink($reset_page);
		
		$content = str_replace($client_password_tag, $client_password, $content);
		
		return $content;
	
	}
	
	// Send client Welcome Email 
	
	function send_cqpim_welcome_email($client_id, $password) {
	
		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$email_subject = get_option('auto_welcome_subject');
		
		$email_content = get_option('auto_welcome_content');
		
		$email_subject = cqpim_replacement_patterns($email_subject, $client_id, '');
		
		$email_content = cqpim_replacement_patterns($email_content, $client_id, '');
		
		$email_content = str_replace('%%CLIENT_PASSWORD%%', $password, $email_content);
		
		$to = $client_details['client_email'];
		
		$attachments = array();
		
		cqpim_send_emails( $to, $email_subject, $email_content, '', $attachments, 'sales' );
	
	}
	
	// Send Team Welcome Email 
	
	function send_cqpim_team_email($team_id, $password) {
	
		$team_details = get_post_meta($team_id, 'team_details', true);
		
		$email_subject = get_option('team_account_subject');
		
		$email_content = get_option('team_account_email');
		
		$email_subject = cqpim_replacement_patterns($email_subject, $team_id, 'team');
		
		$email_content = cqpim_replacement_patterns($email_content, $team_id, 'team');
		
		$email_content = str_replace('%%TEAM_PASSWORD%%', $password, $email_content);
		
		$to = $team_details['team_email'];
		
		$attachments = array();
		
		cqpim_send_emails( $to, $email_subject, $email_content, '', $attachments, 'sales' );
	
	}
	
	// Create team from ADMIN
	
	add_action( "wp_ajax_nopriv_cqpim_create_team_from_admin", 

			"cqpim_create_team_from_admin");

	add_action( "wp_ajax_cqpim_create_team_from_admin", 

		  	"cqpim_create_team_from_admin");
	
	function cqpim_create_team_from_admin() {
	
		$user_id = $_POST['user_id'];
		
		$user = get_user_by('id', $user_id);
		
		if($user->display_name) {
		
			$new_team_member = array(
			
				'post_type' => 'cqpim_teams',
				
				'post_status' => 'private',
				
				'post_content' => '',
				
				'post_title' => $user->display_name
			
			);
			
			$args = array(
			
				'post_type' => 'cqpim_teams',
				
				'post_status' => 'private',
				
				'posts_per_page' => -1,
				
				'meta_key' => 'admin',
				
				'meta_value' => $user->ID
				
			);
			
			$posts = get_posts($args);
			
			if (empty($posts)){ 
						
				$team_pid = wp_insert_post( $new_team_member );
				
			}
			
			if( ! is_wp_error( $team_pid ) ){	

				update_post_meta($team_pid, 'admin', $user->ID);

				$team_details = array(
				
					'team_name' => $user->display_name,
					
					'team_email' => $user->user_email,
					
					'team_perms' => 'administrator',
					
					'user_id' => $user->ID,
				
				);
				
				update_post_meta($team_pid, 'team_details', $team_details);
				
				$return =  array( 

					'error' 	=> false,

					'message' 	=> ''

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();	

			}
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> ''

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();			
		
		}
	
	}
	
	// Add enctype to forms
	
	add_action('post_edit_form_tag', 'enctype_form_tag');

	function enctype_form_tag(){  

		printf(' enctype="multipart/form-data" encoding="multipart/form-data" ');
		
	}
	
	// custom CPT Template
	
	function cqpim_custom_single_cpt_template($single_template) {
	
		global $post;
		
		$dash_type = get_option('client_dashboard_type');
		
		if($dash_type == 'inc') {

			if ($post->post_type == 'cqpim_support') {
			 
				$single_template = dirname( __FILE__ ) . '/templates-inc/support.php';
				  
			}
			 
			if ($post->post_type == 'cqpim_tasks') {
			 
				$single_template = dirname( __FILE__ ) . '/templates-inc/task.php';
				  
			}
			
			if ($post->post_type == 'cqpim_quote') {
			 
				$single_template = dirname( __FILE__ ) . '/templates-inc/quote.php';
				  
			}
			
			if ($post->post_type == 'cqpim_project') {
			 
				$single_template = dirname( __FILE__ ) . '/templates-inc/project.php';
				  
			}
			
			if ($post->post_type == 'cqpim_invoice') {
				
				$single_template = dirname( __FILE__ ) . '/templates-inc/invoice_redirect.php';
				  
			}
			
			if ($post->post_type == 'attachment') {
			
				$attachment = get_post_meta($post->ID, 'cqpim', true);
				
				//if($attachment == true) {
			 
					$single_template = dirname( __FILE__ ) . '/templates-inc/file.php';
				
				//}
				  
			}
		
		} else {
		
			if ($post->post_type == 'cqpim_support') {
			 
				$single_template = dirname( __FILE__ ) . '/templates-fe/support.php';
				  
			}
			 
			if ($post->post_type == 'cqpim_tasks') {
			 
				$single_template = dirname( __FILE__ ) . '/templates-fe/task.php';
				  
			}
			
			if ($post->post_type == 'cqpim_quote') {
			 
				$single_template = dirname( __FILE__ ) . '/templates-fe/quote.php';
				  
			}
			
			if ($post->post_type == 'cqpim_project') {
			 
				$single_template = dirname( __FILE__ ) . '/templates-fe/project.php';
				  
			}
			
			if ($post->post_type == 'cqpim_invoice') {
			 
				$invoice_template = get_option('cqpim_invoice_template');
				
				if($invoice_template == 1) {
			 
					$single_template = dirname( __FILE__ ) . '/templates-fe/invoice.php';
					
				}
				
				if($invoice_template == 2) {
			 
					$single_template = dirname( __FILE__ ) . '/templates-fe/invoice2.php';
					
				}
				  
			}

			if ($post->post_type == 'attachment') {
			
				$attachment = get_post_meta($post->ID, 'cqpim', true);
				
				if($attachment == true) {
			 
					$single_template = dirname( __FILE__ ) . '/templates-fe/file.php';
				
				}
				  
			}			
		
		}
		 
		return $single_template;
		 
	}
	
	add_filter( 'single_template', 'cqpim_custom_single_cpt_template', 30 );
	
	
	
	add_action( 'admin_footer-edit.php', 'cqpim_invoice_bulk_actions' );	
		
	function cqpim_invoice_bulk_actions() {
	
		$screen = get_current_screen();
		
		if($screen->post_type == 'cqpim_invoice') { ?>
		
			<script type="text/javascript">
			
				jQuery(document).ready(function() {
				
					jQuery('<option>').val('mark_paid').text('<?php _e('Mark as Paid', 'cqpim'); ?>')
					
						.appendTo("select[name='action'], select[name='action2']");
						
					jQuery('<option>').val('send_reminders').text('<?php _e('Send Invoice Reminders', 'cqpim'); ?>')
					
						.appendTo("select[name='action'], select[name='action2']");
						
				});
				
			</script>
		
		<?php }
		
		if($screen->post_type == 'cqpim_client') { ?>
		
			<script type="text/javascript">
			
				jQuery(document).ready(function() {
				
					jQuery('option[value="trash"]').remove();
						
				});
				
			</script>		
		
		<?php } 
		
	}
	
	add_action( 'admin_action_mark_paid', 'cqpim_bulk_mark_paid' );
	
	function cqpim_bulk_mark_paid() {
	
		$posts = isset($_REQUEST['post']) ? $_REQUEST['post'] : array();
	
		foreach($posts as $post) {
		
			$totals = get_post_meta($post, 'invoice_totals', true);
			
			$total = isset($totals['total']) ? $totals['total'] : '';
		
			cqpim_mark_invoice_paid($post, '', $total);
		
		}
	
	}
	
	add_action( 'admin_action_mark_unpaid', 'cqpim_bulk_mark_unpaid' );
	
	function cqpim_bulk_mark_unpaid() {
	
		$posts = isset($_REQUEST['post']) ? $_REQUEST['post'] : array();
	
		foreach($posts as $post) {
		
			$details = get_post_meta($post, 'invoice_details', true);
			
			if(!empty($details['paid'])) {
			
				unset($details['paid']);
			
			}
			
			if(!empty($details['paid_details'])) {
			
				unset($details['paid_details']);
				
			}
			
			update_post_meta($post, 'invoice_details', $details);
		
		}
	
	}
	
	add_action( 'admin_action_send_reminders', 'cqpim_bulk_send_reminders' );
	
	function cqpim_bulk_send_reminders() {
	
		$posts = isset($_REQUEST['post']) ? $_REQUEST['post'] : array();
	
		foreach($posts as $post) {
		
			cqpim_send_invoice_reminders($post);
		
		}
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_filter_calendar", 

			"cqpim_filter_calendar");

	add_action( "wp_ajax_cqpim_filter_calendar", 

		  	"cqpim_filter_calendar");
	
	function cqpim_filter_calendar() {
	
		$filters = isset($_POST['filters']) ? $_POST['filters'] : '';
		
		$_SESSION['cal_filters'] = $filters;	
	
		exit();
	
	}
	
	// Create CQPIM Client action
	
	function cqpim_create_client_from_user($actions, $user_object) {
	
		$user = get_user_by('id', $user_object->ID);
			
		$roles = $user->roles;
		
		if(!in_array('administrator', $roles) && !in_array('cqpim_client', $roles) && !in_array('cqpim_admin', $roles) && !in_array('cqpim_user', $roles)) {
	
			$actions['add_cqpim_client'] = "<a class='create_client' href='" . admin_url( "users.php?action=create_cqpim_client&amp;user=$user_object->ID") . "'>" . __( 'Convert to CQPIM Client', 'cqpim' ) . "</a>";
		
		}
		
		return $actions;
		
	}
	
	add_filter('user_row_actions', 'cqpim_create_client_from_user', 10, 2);
	
	add_action('current_screen', 'cqpim_create_client_from_user_callback');
	
	function cqpim_create_client_from_user_callback() {
	
		$screen = get_current_screen();
		
		$base = isset($screen->base) ? $screen->base : '';
		
		$action = isset($_GET['action']) ? $_GET['action'] : '';
		
		if($base == 'users' && $action == 'create_cqpim_client') {
		
			$user_id = isset($_GET['user']) ? $_GET['user'] : '';
			
			$user = get_user_by('id', $user_id);
			
			$new_client = array(
			
				'post_type' => 'cqpim_client',
				
				'post_status' => 'private',
				
				'post_content' => '',
				
				'post_title' => $user->display_name,
			
			);
			
			$client_pid = wp_insert_post( $new_client, true );	

			if( ! is_wp_error( $client_pid ) ){
			
				$client_updated = array(
					
					'ID' => $client_pid,
						
					'post_name' => $client_pid,
						
				);						
			
				wp_update_post( $client_updated );
			
				$client_details = array(
					
					'client_ref' => $client_pid,

					'client_contact' => $user->display_name,
					
					'client_company' => $user->display_name,
					
					'client_email' => $user->user_email,
				
				);
				
				$client_details['user_id'] = $user_id;
				
				$client_ids = array();
				
				$client_ids[] = $user_id;
				
				update_post_meta($client_pid, 'client_details', $client_details);
				
				update_post_meta($client_pid, 'client_ids', $client_ids);	

				$user_data = array(
				
					'ID' => $user_id,
					
					'role' => 'cqpim_client'
				
				);
				
				wp_update_user($user_data);	

			}
		
		}
	
	}
	
	// Create team from user 
	
	// Create CQPIM Client action
	
	function cqpim_create_team_from_user($actions, $user_object) {
	
		$user = get_user_by('id', $user_object->ID);
			
		$roles = $user->roles;
		
		if(!in_array('administrator', $roles) && !in_array('cqpim_client', $roles) && !in_array('cqpim_admin', $roles) && !in_array('cqpim_user', $roles)) {
	
			$actions['add_cqpim_team'] = "<a class='create_team' href='" . admin_url( "users.php?action=create_cqpim_team&amp;user=$user_object->ID") . "'>" . __( 'Convert to CQPIM Team Member', 'cqpim' ) . "</a>";
			
		}
		
		return $actions;
		
	}
	
	add_filter('user_row_actions', 'cqpim_create_team_from_user', 10, 2);	
	
	add_action('current_screen', 'cqpim_create_team_from_user_callback');
	
	function cqpim_create_team_from_user_callback() {
	
		$screen = get_current_screen();
		
		$base = isset($screen->base) ? $screen->base : '';
		
		$action = isset($_GET['action']) ? $_GET['action'] : '';
		
		if($base == 'users' && $action == 'create_cqpim_team') {
		
			$user_id = isset($_GET['user']) ? $_GET['user'] : '';
			
			$user = get_user_by('id', $user_id);
			
			$new_team = array(
			
				'post_type' => 'cqpim_teams',
				
				'post_status' => 'private',
				
				'post_content' => '',
				
				'post_title' => $user->display_name,
			
			);
			
			$client_pid = wp_insert_post( $new_team, true );	

			if( ! is_wp_error( $client_pid ) ){
			
				$client_updated = array(
					
					'ID' => $client_pid,
						
					'post_name' => $client_pid,
						
				);						
			
				wp_update_post( $client_updated );
			
				$client_details = array(

					'team_name' => $user->display_name,
					
					'team_email' => $user->user_email,
					
					'team_perms' => 'cqpim_user'
				
				);
				
				$client_details['user_id'] = $user_id;
				
				update_post_meta($client_pid, 'team_details', $client_details);

				$user_data = array(
				
					'ID' => $user_id,
					
					'role' => 'cqpim_user'
				
				);
				
				wp_update_user($user_data);	

			}
		
		}
	
	}	
	
	// Delete client
	
	add_filter( 'post_row_actions', 'cqpim_edit_client_trash_action', 10, 1 );

	function cqpim_edit_client_trash_action( $actions ) {

		if( get_post_type() === 'cqpim_client'  && current_user_can('delete_cqpim_clients')) {
		
			global $post;
		
			$actions['trash'] = '<a class="delete_client" data-id="' . $post->ID . '" href="#">' . __('Delete Client' , 'cqpim') . '</a>'; ?>
			
				<div id="delete_client_warning_container_<?php echo $post->ID; ?>" style="display:none">
				
					<div id="delete_client_warning_<?php echo $post->ID; ?>" class="contact_edit">
					
						<div style="padding:12px">

							<h3><?php _e('Delete Client Warning', 'cqpim'); ?></h3>
							
							<p><?php _e('Deleting this client will also delete the associated user account and the user account of all related contacts. If this is not desired then you should first unlink the user account from the client.', 'cqpim'); ?></p>

							<button style="margin-left:0" class="metabox-add-button left uldc" data-id="<?php echo $post->ID; ?>"><?php _e('Unlink User and Delete Client', 'cqpim'); ?></button><button class="metabox-add-button left dcu" data-id="<?php echo $post->ID; ?>"><?php _e('Delete Client and User', 'cqpim'); ?></button>
						
							<div class="clear"></div>
							
							<br />
							
							<div id="client_messages_<?php echo $post->ID; ?>"></div>
							
							<div id="client_spinner_<?php echo $post->ID; ?>" class="ajax_spinner" style="display:none"></div>
						
						</div>
						
					</div>
					
				</div>
			
			<?php
			
		}
		
		return $actions;
		
	}

	add_action( "wp_ajax_cqpim_unlink_delete_client", 

		  	"cqpim_unlink_delete_client");
	
	function cqpim_unlink_delete_client() {
	
		$client_id = isset($_POST['id']) ? $_POST['id'] : '';
		
		if(empty($client_id)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<span class="task_over">' . __('The Client ID is missing. The client could not be deleted', 'cqpim') . '</span>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();
		
		
		} else {
		
			$client_details = get_post_meta($client_id, 'client_details', true);
			
			unset($client_details['user_id']);
			
			update_post_meta($client_id, 'client_details', $client_details);
			
			wp_delete_post($client_id, true);

			$return =  array( 

				'error' 	=> false,

				'message' 	=> '<span class="task_complete">' . __('The user was successfully unlinked and the client was deleted', 'cqpim') . '</span>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();
		
		}

	}

	add_action( "wp_ajax_cqpim_delete_client_user_confirm", 

		  	"cqpim_delete_client_user_confirm");
	
	function cqpim_delete_client_user_confirm() {
	
		$client_id = isset($_POST['id']) ? $_POST['id'] : '';
		
		if(empty($client_id)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<span class="task_over">' . __('The Client ID is missing. The client could not be deleted', 'cqpim') . '</span>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();
		
		
		} else {
		
			$client_details = get_post_meta($client_id, 'client_details', true);
			
			$user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
			
			$client_contacts = get_post_meta($client_id, 'client_contacts', true);
			
			wp_delete_user($user_id);
			
			foreach($client_contacts as $key => $contact) {
			
				wp_delete_user($key);
			
			}
		
			wp_delete_post($client_id, true);

			$return =  array( 

				'error' 	=> false,

				'message' 	=> '<span class="task_complete">' . __('The user and the client was deleted successfully', 'cqpim') . '</span>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);			

			exit();	
			
		}

	}	
	
	function cqpim_get_image_id_by_url ( $url ) {
	
		global $wpdb;
		
		$img_id = 0;
		
			preg_match( '|' . get_site_url() . '|i', $url, $matches );
			
			if ( isset( $matches ) and 0 < count( $matches ) ) {
			
				$url = preg_replace( '/([^?]+).*/', '\1', $url ); 
				
				$guid = preg_replace( '/(.+)-\d+x\d+\.(\w+)/', '\1.\2', $url );
				
				$img_id = $wpdb->get_var( $wpdb->prepare( "SELECT `ID` FROM $wpdb->posts WHERE `guid` = '%s'", $guid ) );
				
			if ( $img_id ) {
			
				$img_id = intval( $img_id );
				
			}
			
		}
		
		return $img_id;
		
	}
	
	function cqpim_upload_dir( $dirs ) {
		$dirs['subdir'] = '/cqpim-uploads';
		$dirs['path'] = $dirs['basedir'] . '/cqpim-uploads';
		$dirs['url'] = $dirs['baseurl'] . '/cqpim-uploads';

		return $dirs;
	}


	function upload_user_file( $file = array(), $project_id ) {

		require_once( ABSPATH . 'wp-admin/includes/admin.php' );
		
		add_filter( 'upload_dir', 'cqpim_upload_dir' );	

		$file_return = wp_handle_upload( $file, array('test_form' => false ) );

		if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
		
			return false;
			
		} else {

			$filename = $file_return['file'];

			$attachment = array(
				'post_mime_type' => $file_return['type'],
				'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_content' => '',
				'post_parent' => $project_id,
				'post_status' => 'inherit',
				'guid' => $file_return['url']
			);

			$attachment_id = wp_insert_attachment( $attachment, $file_return['url'] );

			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
			wp_update_attachment_metadata( $attachment_id, $attachment_data );

			if( 0 < intval( $attachment_id ) ) {
				return $attachment_id;
			}
		}
		
		remove_filter( 'upload_dir', 'cqpim_upload_dir' );	

		return false;
	}
	
	// Delete team user when member is deleted

	add_action( 'before_delete_post', 'cqpim_delete_team_user' );

	function cqpim_delete_team_user( $post_id ){
		
		global $post_type;   
		
		if ( $post_type != 'cqpim_teams' ) return;
		
		require_once(ABSPATH.'wp-admin/includes/user.php' );
		
		$team_details = get_post_meta($post_id, 'team_details', true);
		
		$wp_user_id = $team_details['user_id'];
		
		$user = get_user_by('id', $wp_user_id);
		
		if(!in_array('administrator', $user->roles)) {
		
			wp_delete_user($wp_user_id);
		
		}
		
	}
	
	function cqpim_is_edit_page($new_edit = null){
	
		global $pagenow;
		
		if (!is_admin()) return false;


		if($new_edit == "edit")
		
			return in_array( $pagenow, array( 'post.php',  ) );
			
		elseif($new_edit == "new")
		
			return in_array( $pagenow, array( 'post-new.php' ) );
			
		else
		
			return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
			
	}
	
	// Control post visibility on every save 
	
	add_action( 'edit_post', 'cqpim_assign_post_visibility' );

	function cqpim_assign_post_visibility( $post_id ){	
	
		$post = get_post($post_id);
		
		$password = isset($post->post_password) ? $post->post_password : '';
		
		if(empty($password)) {
		
			$password = cqpim_random_string(10);
			
		}
		
		if($post->post_status != 'trash' && $post->post_status != 'draft') {
		
			if($post->post_type == 'cqpim_invoice' || $post->post_type == 'cqpim_tasks') {
			
				$post_updated = array(
				
					'ID' => $post_id,
					
					'post_status' => 'publish',
					
					'post_password' => $password,
				
				);
			
				if ( ! wp_is_post_revision( $post_id ) ){
					
					remove_action('edit_post', 'cqpim_assign_post_visibility');
					
					wp_update_post( $post_updated );

					add_action('edit_post', 'cqpim_assign_post_visibility');
					
				}			
				
			}
			
			if($post->post_type == 'cqpim_templates' || $post->post_type == 'cqpim_quote' || $post->post_type == 'cqpim_terms' || $post->post_type == 'cqpim_forms' || $post->post_type == 'cqpim_project' || $post->post_type == 'cqpim_client' || $post->post_type == 'cqpim_teams' || $post->post_type == 'cqpim_support' || $post->post_type == 'cqpim_supplier' || $post->post_type == 'cqpim_expense') {
			
				$post_updated = array(
				
					'ID' => $post_id,
					
					'post_status' => 'private',
				
				);
			
				if ( ! wp_is_post_revision( $post_id ) ){
					
					remove_action('edit_post', 'cqpim_assign_post_visibility');
					
					wp_update_post( $post_updated );

					add_action('edit_post', 'cqpim_assign_post_visibility');
					
				}			
			
			}

		}
	
	}
	
	add_action('cqpim_check_email_pipe', 'cqpim_check_email_pipe_minute');
	
	function cqpim_check_email_pipe_minute() {
	
		$mail_server = '{' . get_option('cqpim_mail_server') . '}';
		$mailbox_name = get_option('cqpim_mailbox_name');
		$mailbox_pass = get_option('cqpim_mailbox_pass');
		
		$test = imap_open($mail_server, $mailbox_name, $mailbox_pass);
		
		if(!empty($test)) {

			require_once(realpath(dirname(__FILE__)) . '/assets/php-imap/src/PhpImap/IncomingMail.php');
			require_once(realpath(dirname(__FILE__)) . '/assets/php-imap/src/PhpImap/Mailbox.php');
			
			$dirs = wp_upload_dir();
			
			$mailbox = new PhpImap\Mailbox($mail_server, $mailbox_name, $mailbox_pass, $dirs['basedir'] . '/cqpim-uploads');
			
			$mailsIds = $mailbox->searchMailbox('UNSEEN');
			
			if(!$mailsIds) {
			
				die('Mailbox is empty');
				
			}  
		
		$delete = get_option('cqpim_piping_delete');
		
		foreach($mailsIds as $key => $message) {
	
			$mail = $mailbox->getMail($message);
			
			$attached_media = $mail->getAttachments();
			
			$body = $mail->textPlain;
			$fromName = $mail->fromName;
			$toName = $mail->toString;
			$toEmail = $mail->to;
			$fromEmail = $mail->fromAddress;
			$subject = $mail->subject;	
			$subject_full = $mail->subject;
			
			// Remove the original message

			$body_array = explode("\n",$mail->textPlain);
			$content = "";
			foreach($body_array as $key => $value){
				if($value == "_________________________________________________________________"){
					break;
				} elseif(preg_match("/^From:(.*)/i",$value,$matches)){
					break;
				} elseif(preg_match("/^-*(.*)Original Message(.*)-*/i",$value,$matches)){
					break;
				} elseif(preg_match("/^On(.*)wrote:(.*)/i",$value,$matches)) {
					break;
				} elseif(preg_match("/^On(.*)$fromName(.*)/i",$value,$matches)) {
					break;
				} elseif(preg_match("/^On(.*)$toName(.*)/i",$value,$matches)) {
					break;
				} elseif(preg_match("/^(.*)$toEmail(.*)wrote:(.*)/i",$value,$matches)) {
					break;
				} elseif(preg_match("/^(.*)$fromEmail(.*)wrote:(.*)/i",$value,$matches)) {
					break;
				} elseif(preg_match("/^>(.*)/i",$value,$matches)){
					break;
				} elseif(preg_match("/^---(.*)On(.*)wrote:(.*)/i",$value,$matches)){
					break;
				} else {
					$content .= "$value\n";
				}
						
			}

			// Get the ID of the item	
			
			preg_match("/\[(.*?)\]/", $subject, $subject);	
			
			$subject = $subject[1];
			
			$post_id = preg_replace('/[^0-9]/','', $subject);
			
			// Get the Item
			
			$attachments = array();
			
			if(empty($post_id)) {
			
				// This is where we create a new ticket, but only for registered
				
				$value = get_option('cqpim_create_support_on_email');
				
				if(!empty($value)) {
				
					$item_type = __('support ticket', 'cqpim');
					
					$user = get_user_by('email', $fromEmail);
					
					if(empty($user)) {
					
						$value = get_option('cqpim_send_piping_reject');
						
						if(!empty($value)) {
					
							cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
						
						}
						
						$mailbox->markMailAsRead($message);
						
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
						break;
					
					}
					
					$args = array(
					
						'post_type' => 'cqpim_client',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'client_details', true);
						
						if($team_details['user_id'] == $user->ID) {
						
							$assigned = $member->ID;
							
							$client_type = 'admin';
						
						}
						
						if($team_details['client_email'] == $fromEmail) {
						
							$valid = true;
						
						}
					
					} 
					
					if(empty($assigned)) {
					
						foreach($members as $member) {
						
							$team_ids = get_post_meta($member->ID, 'client_ids', true);
							
							$client_contacts = get_post_meta($member->ID, 'client_contacts', true);
							
							if(!is_array($team_ids)) {
							
								$team_ids = array($team_ids);
							
							}
							
							if(in_array($user->ID, $team_ids)) {
							
								$assigned = $member->ID;
								
								$client_type = 'contact';
							
							}
							
							if(empty($client_contacts)) {
							
								$client_contacts = array();
								
							}
							
							foreach($client_contacts as $contact) {
							
								if($contact['email'] == $fromEmail) {
								
									$valid = true;
								
								}
							
							}
						
						} 			
					
					}

					if(empty($valid)) {
					
						$value = get_option('cqpim_send_piping_reject');
						
						if(!empty($value)) {
					
							cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
						
						}
						
						$mailbox->markMailAsRead($message);
						
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
						break;					
					
					}	

					$title = isset($subject_full) ? $subject_full : __('Email ticket', 'cqpim');
					
					$priority = 'normal';
					
					$details = $content;
					
					$client_object_id = $assigned;
					
					$ticket_status = 'open';
					
					$client_details = get_post_meta($client_object_id, 'client_details', true);
				
					$new_ticket = array(
					
						'post_type' => 'cqpim_support',
						
						'post_status' => 'private',
						
						'post_content' => '',
						
						'post_title' => $title,
						
						'post_author' => $user->ID,				
					
					);
					
					$ticket_pid = wp_insert_post( $new_ticket, true );
					
					if( ! is_wp_error( $ticket_pid ) ){
					
						$ticket_updated = array(
							
							'ID' => $ticket_pid,
								
							'post_name' => $ticket_pid,
								
						);						
					
						wp_update_post( $ticket_updated );	
						
						if(!empty($attached_media)) {
						
							$allowed = get_option('cqpim_allowed_extensions');
							
							$allowed = explode(',', $allowed);
						
							$ticket_changes = array();
							
							$i = 0;
						
							foreach($attached_media as $file) {
							
								$extension = pathinfo($file->name, PATHINFO_EXTENSION);

								if(!in_array(strtolower($extension), $allowed)){
									break;
								}									
								
								$filename = $file->filePath;
								
								$filename = substr($filename, strrpos($filename, '/') + 1);

								$attachment = array(
									'post_mime_type' => $extension,
									'post_title' => $file->name,
									'post_content' => '',
									'post_parent' => $ticket_pid,
									'post_status' => 'publish',
									'guid' => $dirs['baseurl'] . '/cqpim-uploads/' . $filename,
									'post_owner' => $user->ID
								);

								$attachment_id = wp_insert_attachment( $attachment, $dirs['baseurl'] . '/cqpim-uploads/' . $filename );	

								require_once(ABSPATH . 'wp-admin/includes/image.php');
								
								$attachment_data = wp_generate_attachment_metadata( $attachment_id, $file->filePath );
								
								wp_update_attachment_metadata( $attachment_id, $attachment_data );			
																	
								$attachment_obj = wp_get_attachment_metadata( $attachment );
								
								update_post_meta($attachment_id, 'cqpim', true);
								
								$attachment = get_post($attachment_id);
								
								$filename = $attachment->post_title;
								
								$ticket_changes[] = sprintf(__('Uploaded file: %1$s', 'cqpim'), $filename);	
							
								$i++;
								
								$attachments[] = $file->filePath;
							
							}
						
						}

						update_post_meta($ticket_pid, 'ticket_client', $client_object_id);
						
						update_post_meta($ticket_pid, 'ticket_status', $ticket_status);
						
						$ticket_updates = array();
						
						$ticket_updates[] = array(
						
							'details' => $details,
							
							'time' => current_time('timestamp'),
							
							'name' => $user->display_name,
							
							'email' => $user->user_email,
							
							'user' => $client_object_id,
							
							'type' => 'client',
							
							'changes' => $ticket_changes
							
						);
						
						update_post_meta($ticket_pid, 'ticket_updates', $ticket_updates);
						
						update_post_meta($ticket_pid, 'ticket_priority', $priority);
						
						if(!empty($client_details['ticket_assignee'])) {
						
							update_post_meta($ticket_pid, 'ticket_owner', $client_details['ticket_assignee']);
							
						}
						
						$last_updated = current_time('timestamp');
						
						update_post_meta($ticket_pid, 'last_updated', $last_updated);
						
						$to = array();
						
						$to[] = get_option('company_support_email');
						
						$to[] = $sender_email;
						
						if(!empty($client_details['ticket_assignee'])) {
						
							$assignee_details = get_post_meta($client_details['ticket_assignee'], 'team_details', true);
							
							if(!empty($assignee_details['team_email'])) {
							
								$to[] = $assignee_details['team_email'];
							
							}
							
						}			
						
						if($priority == 'high' || $priority == 'immediate') {
						
							add_filter('phpmailer_init','update_priority_mailer');
						
						}

						$email_subject = get_option('client_create_ticket_subject');
						
						$email_content = get_option('client_create_ticket_email');
						
						$email_subject = str_replace('%%PIPING_ID%%', '[' . get_option('cqpim_string_prefix') . ':' . $ticket_pid . ']', $email_subject);
						
						$email_subject = str_replace('%%CLIENT_NAME%%', $user->display_name, $email_subject);
						
						$email_content = str_replace('%%CLIENT_NAME%%', $user->display_name, $email_content);				
						
						$email_subject = cqpim_replacement_patterns($email_subject, $client_object_id, 'client');
						
						$email_subject = cqpim_replacement_patterns($email_subject, $ticket_pid, 'ticket');
						
						$email_content = cqpim_replacement_patterns($email_content, $client_object_id, 'client');
						
						$email_content = cqpim_replacement_patterns($email_content, $ticket_pid, 'ticket');
						
						foreach($to as $recip) {
						
							cqpim_send_emails($recip, $email_subject, $email_content, '', $attachments, 'support');

						}
						
						$mailbox->markMailAsRead($message);
						
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}

						exit;					
					
					}
				
				} else {
				
					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
				
					exit;
					
				}
					
			
			}
			
			$post = get_post($post_id);
			
			if(empty($post)) {
			
				die('No post');
				
			}
			
			$post_type = $post->post_type;
			
			if($post_type == 'cqpim_support') {
			
				$item_type = __('support ticket', 'cqpim');
			
				$user = get_user_by('email', $fromEmail);
				
				if(empty($user)) {
				
					$value = get_option('cqpim_send_piping_reject');
					
					if(!empty($value)) {
				
						cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
					
					}
					
					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
				
					exit;
				
				}
				
				foreach($user->roles as $role) {
				
					if($role == 'cqpim_client') {
					
						$type = 'client';
						
					} else {
					
						$type = 'team';
						
					}
				
				}

				if($type == 'client') {
				
					$args = array(
					
						'post_type' => 'cqpim_client',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'client_details', true);
						
						if($team_details['user_id'] == $user->ID) {
						
							$assigned = $member->ID;
							
							$client_type = 'admin';
						
						}
						
						if($team_details['client_email'] == $fromEmail) {
						
							$valid = true;
						
						}
					
					} 
					
					if(empty($assigned)) {
					
						foreach($members as $member) {
						
							$team_ids = get_post_meta($member->ID, 'client_ids', true);
							
							$client_contacts = get_post_meta($member->ID, 'client_contacts', true);
							
							if(in_array($user->ID, $team_ids)) {
							
								$assigned = $member->ID;
								
								$client_type = 'contact';
							
							}
							
							if(empty($client_contacts)) {
							
								$client_contacts = array();
								
							}
							
							foreach($client_contacts as $contact) {
							
								if($contact['email'] == $fromEmail) {
								
									$valid = true;
								
								}
							
							}
						
						} 			
					
					}
					
					$ticket_client = get_post_meta($post->ID, 'ticket_client', true);
					
					if(empty($valid) || $ticket_client != $assigned) {
					
						$value = get_option('cqpim_send_piping_reject');
						
						if(!empty($value)) {
					
							cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
						
						}
						
						$mailbox->markMailAsRead($message);
						
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
						exit;					
					
					}
					
					if(!empty($attached_media)) {
					
						$allowed = get_option('cqpim_allowed_extensions');
						
						$allowed = explode(',', $allowed);
					
						$ticket_changes = array();
						
						$i = 0;
					
						foreach($attached_media as $file) {
						
							$extension = pathinfo($file->name, PATHINFO_EXTENSION);

							if(!in_array(strtolower($extension), $allowed)){
								break;
							}									
							
							$filename = $file->filePath;
							
							$filename = substr($filename, strrpos($filename, '/') + 1);

							$attachment = array(
								'post_mime_type' => $extension,
								'post_title' => $file->name,
								'post_content' => '',
								'post_parent' => $post->ID,
								'post_status' => 'publish',
								'guid' => $dirs['baseurl'] . '/cqpim-uploads/' . $filename,
								'post_owner' => $user->ID
							);

							$attachment_id = wp_insert_attachment( $attachment, $dirs['baseurl'] . '/cqpim-uploads/' . $filename );	

							require_once(ABSPATH . 'wp-admin/includes/image.php');
							
							$attachment_data = wp_generate_attachment_metadata( $attachment_id, $file->filePath );
							
							wp_update_attachment_metadata( $attachment_id, $attachment_data );			
																
							$attachment_obj = wp_get_attachment_metadata( $attachment );
							
							update_post_meta($attachment_id, 'cqpim', true);
							
							$attachment = get_post($attachment_id);
							
							$filename = $attachment->post_title;
							
							$ticket_changes[] = sprintf(__('Uploaded file: %1$s', 'cqpim'), $filename);	
						
							$i++;
							
							$attachments[] = $file->filePath;
						
						}
					
					}
					
					$status = get_post_meta($post_id, 'ticket_status', true);	
					
					$priority = get_post_meta($post_id, 'ticket_priority', true);
					
					$owner = get_post_meta($post_id, 'ticket_owner', true);
					
					$watchers = get_post_meta($post_id, 'ticket_watchers', true);
					
					$data = array(
					
						'ticket_update_new' => $content,
						
						'ticket_priority_new' => $priority,
						
						'ticket_status_new' => $status,
						
						'ticket_owner' => $owner,
						
						'task_watchers' => $watchers,
						
						'ticket_changes' => $ticket_changes,
					
					);
					
					$files = array();
					
					cqpim_update_support_ticket($post_id, $data, $files, 'client', $user, $attachments);
					
					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
				}				
				
				if($type == 'team') {
				
					$args = array(
					
						'post_type' => 'cqpim_teams',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'team_details', true);
						
						if($team_details['user_id'] == $user->ID) {
						
							$assigned = $member->ID;
							
							$client_type = 'admin';
						
						}
						
						if($team_details['team_email'] == $fromEmail) {
						
							$valid = true;
						
						}
					
					} 
					
					if(empty($valid)) {
					
						$value = get_option('cqpim_send_piping_reject');
						
						if(!empty($value)) {
					
							cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
						
						}
						
						$mailbox->markMailAsRead($message);
						
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
						exit;					
					
					}
					
					if(!empty($attached_media)) {
					
						$allowed = get_option('cqpim_allowed_extensions');
						
						$allowed = explode(',', $allowed);
					
						$ticket_changes = array();
						
						$i = 0;
					
						foreach($attached_media as $file) {
						
							$extension = pathinfo($file->name, PATHINFO_EXTENSION);

							if(!in_array(strtolower($extension), $allowed)){
								break;
							}									
							
							$filename = $file->filePath;
							
							$filename = substr($filename, strrpos($filename, '/') + 1);

							$attachment = array(
								'post_mime_type' => $extension,
								'post_title' => $file->name,
								'post_content' => '',
								'post_parent' => $post->ID,
								'post_status' => 'publish',
								'guid' => $dirs['baseurl'] . '/cqpim-uploads/' . $filename,
								'post_owner' => $user->ID
							);

							$attachment_id = wp_insert_attachment( $attachment, $dirs['baseurl'] . '/cqpim-uploads/' . $filename );	

							require_once(ABSPATH . 'wp-admin/includes/image.php');
							
							$attachment_data = wp_generate_attachment_metadata( $attachment_id, $file->filePath );
							
							wp_update_attachment_metadata( $attachment_id, $attachment_data );			
																
							$attachment_obj = wp_get_attachment_metadata( $attachment );
							
							update_post_meta($attachment_id, 'cqpim', true);
							
							$attachment = get_post($attachment_id);
							
							$filename = $attachment->post_title;
							
							$ticket_changes[] = sprintf(__('Uploaded file: %1$s', 'cqpim'), $filename);	
						
							$i++;
							
							$attachments[] = $file->filePath;
						
						}
					
					}
					
					$status = get_post_meta($post_id, 'ticket_status', true);	
					
					$priority = get_post_meta($post_id, 'ticket_priority', true);
					
					$owner = get_post_meta($post_id, 'ticket_owner', true);
					
					$watchers = get_post_meta($post_id, 'ticket_watchers', true);
					
					$data = array(
					
						'ticket_update_new' => $content,
						
						'ticket_priority_new' => $priority,
						
						'ticket_status_new' => $status,
						
						'ticket_owner' => $owner,
						
						'task_watchers' => $watchers,
						
						'ticket_changes' => $ticket_changes
					
					);
					
					$files = array();
					
					cqpim_update_support_ticket($post_id, $data, $files, 'admin', $user, $attachments);
					
					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
				}				
		
			}

			if($post_type == 'cqpim_tasks') {

				$item_type = __('task', 'cqpim');
			
				$user = get_user_by('email', $fromEmail);
				
				if(empty($user)) {
				
					$value = get_option('cqpim_send_piping_reject');
					
					if(!empty($value)) {
				
						cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
					
					}
					
					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
				
					exit;
				
				}
				
				foreach($user->roles as $role) {
				
					if($role == 'cqpim_client') {
					
						$type = 'client';
						
					} else {
					
						$type = 'team';
						
					}
				
				}

				if($type == 'client') {
				
					$args = array(
					
						'post_type' => 'cqpim_client',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'client_details', true);
						
						if($team_details['user_id'] == $user->ID) {
						
							$assigned = $member->ID;
							
							$client_type = 'admin';
						
						}
						
						if($team_details['client_email'] == $fromEmail) {
						
							$valid = true;
						
						}
					
					} 
					
					if(empty($assigned)) {
					
						foreach($members as $member) {
						
							$team_ids = get_post_meta($member->ID, 'client_ids', true);
							
							$client_contacts = get_post_meta($member->ID, 'client_contacts', true);
							
							if(in_array($user->ID, $team_ids)) {
							
								$assigned = $member->ID;
								
								$client_type = 'contact';
							
							}
							
							if(empty($client_contacts)) {
							
								$client_contacts = array();
								
							}
							
							foreach($client_contacts as $contact) {
							
								if($contact['email'] == $fromEmail) {
								
									$valid = true;
								
								}
							
							}
						
						} 			
					
					}
					
					if(empty($valid)) {
					
						$value = get_option('cqpim_send_piping_reject');
						
						if(!empty($value)) {
					
							cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
						
						}
						
						$mailbox->markMailAsRead($message);
						
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
						exit;					
					
					}
					
					update_post_meta($post_id, 'client_updated', false);
					
					$project_id = get_post_meta($post_id, 'project_id', true);
					
					$task_watchers = get_post_meta($post_id, 'task_watchers', true);
					
					$task_owner = get_post_meta($post_id, 'owner', true);
					
					$project_progress = get_post_meta($project_id, 'project_progress', true);
					
					$task_object = get_post($post_id);
					
					$task_title = $task_object->post_title;
					
					$text = sprintf(__('Task Updated: %1$s'), $task_title );
									
					$project_progress[] = array(
									
						'update' => $text,
										
						'date' => current_time('timestamp'),
								
						'by' => $user->display_name
										
					);
									
					update_post_meta($project_id, 'project_progress', $project_progress );	

					// Add message
					
					$task_messages = get_post_meta($post_id, 'task_messages', true);
					
					$date = current_time('timestamp');
					
					$current_user = wp_get_current_user();
					
					$task_messages[] = array(
						
						'date' => $date,
						
						'message' => $content,
						
						'by' => $user->display_name,
						
						'author' => $user->ID
						
					);		
					
					update_post_meta($post_id, 'task_messages', $task_messages);
					
					if(!empty($attached_media)) {
					
						$allowed = get_option('cqpim_allowed_extensions');
						
						$allowed = explode(',', $allowed);
					
						$ticket_changes = array();
						
						$i = 0;
					
						foreach($attached_media as $file) {
						
							$extension = pathinfo($file->name, PATHINFO_EXTENSION);

							if(!in_array(strtolower($extension), $allowed)){
								break;
							}									
							
							$filename = $file->filePath;
							
							$filename = substr($filename, strrpos($filename, '/') + 1);

							$attachment = array(
								'post_mime_type' => $extension,
								'post_title' => $file->name,
								'post_content' => '',
								'post_parent' => $post_id,
								'post_status' => 'publish',
								'guid' => $dirs['baseurl'] . '/cqpim-uploads/' . $filename,
								'post_owner' => $user->ID
							);

							$attachment_id = wp_insert_attachment( $attachment, $dirs['baseurl'] . '/cqpim-uploads/' . $filename );	

							require_once(ABSPATH . 'wp-admin/includes/image.php');
							
							$attachment_data = wp_generate_attachment_metadata( $attachment_id, $file->filePath );
							
							wp_update_attachment_metadata( $attachment_id, $attachment_data );			
																
							$attachment_obj = wp_get_attachment_metadata( $attachment );
							
							update_post_meta($attachment_id, 'cqpim', true);
							
							$attachment = get_post($attachment_id);
							
							$filename = $attachment->post_title;
							
							$ticket_changes[] = sprintf(__('Uploaded file: %1$s', 'cqpim'), $filename);	
						
							$i++;
							
							$attachments[] = $file->filePath;
						
						}
					
					}
					
					cqpim_send_task_updates($post_id, $project_id, $task_owner, $task_watchers, $content, $user, $attachments);					

					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
				}				
				
				if($type == 'team') {
				
					$args = array(
					
						'post_type' => 'cqpim_teams',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'team_details', true);
						
						if($team_details['user_id'] == $user->ID) {
						
							$assigned = $member->ID;
							
							$client_type = 'admin';
						
						}
						
						if($team_details['team_email'] == $fromEmail) {
						
							$valid = true;
						
						}
					
					} 
					
					if(empty($valid)) {
					
						$value = get_option('cqpim_send_piping_reject');
						
						if(!empty($value)) {
					
							cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
						
						}
						
						$mailbox->markMailAsRead($message);
						
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
						exit;					
					
					}
					
					update_post_meta($post_id, 'team_updated', true);
					
					$project_id = get_post_meta($post_id, 'project_id', true);
					
					$task_watchers = get_post_meta($post_id, 'task_watchers', true);
					
					$task_owner = get_post_meta($post_id, 'owner', true);
					
					$project_progress = get_post_meta($project_id, 'project_progress', true);
					
					$task_object = get_post($post_id);
					
					$task_title = $task_object->post_title;
					
					$text = sprintf(__('Task Updated: %1$s'), $task_title );
									
					$project_progress[] = array(
									
						'update' => $text,
										
						'date' => current_time('timestamp'),
								
						'by' => $user->display_name
										
					);
									
					update_post_meta($project_id, 'project_progress', $project_progress );	
					
					$task_messages = get_post_meta($post_id, 'task_messages', true);
					
					$date = current_time('timestamp');
					
					$current_user = wp_get_current_user();
					
					$task_messages[] = array(
						
						'date' => $date,
						
						'message' => $content,
						
						'by' => $user->display_name,
						
						'author' => $user->ID
						
					);		
					
					update_post_meta($post_id, 'task_messages', $task_messages);
					
					if(!empty($attached_media)) {
					
						$allowed = get_option('cqpim_allowed_extensions');
						
						$allowed = explode(',', $allowed);
					
						$ticket_changes = array();
						
						$i = 0;
					
						foreach($attached_media as $file) {
						
							$extension = pathinfo($file->name, PATHINFO_EXTENSION);

							if(!in_array(strtolower($extension), $allowed)){
								break;
							}									
							
							$filename = $file->filePath;
							
							$filename = substr($filename, strrpos($filename, '/') + 1);

							$attachment = array(
								'post_mime_type' => $extension,
								'post_title' => $file->name,
								'post_content' => '',
								'post_parent' => $post_id,
								'post_status' => 'publish',
								'guid' => $dirs['baseurl'] . '/cqpim-uploads/' . $filename,
								'post_owner' => $user->ID
							);

							$attachment_id = wp_insert_attachment( $attachment, $dirs['baseurl'] . '/cqpim-uploads/' . $filename );	

							require_once(ABSPATH . 'wp-admin/includes/image.php');
							
							$attachment_data = wp_generate_attachment_metadata( $attachment_id, $file->filePath );
							
							wp_update_attachment_metadata( $attachment_id, $attachment_data );			
																
							$attachment_obj = wp_get_attachment_metadata( $attachment );
							
							update_post_meta($attachment_id, 'cqpim', true);
							
							$attachment = get_post($attachment_id);
							
							$filename = $attachment->post_title;
							
							$ticket_changes[] = sprintf(__('Uploaded file: %1$s', 'cqpim'), $filename);	
						
							$i++;
							
							$attachments[] = $file->filePath;
						
						}
					
					}
					
					cqpim_send_task_updates($post_id, $project_id, $task_owner, $task_watchers, $content, $user, $attachments);
					
					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
					
				}	

			}
			
			if($post_type == 'cqpim_conversations') {
				
				$item_type = __('conversation', 'cqpim');
			
				$user = get_user_by('email', $fromEmail);
				
				if(empty($user)) {
				
					$value = get_option('cqpim_send_piping_reject');
					
					if(!empty($value)) {
				
						cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
					
					}
					
					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
				
					exit;
				
				}
				
				foreach($user->roles as $role) {
				
					if($role == 'cqpim_client') {
					
						$type = 'client';
						
					} else {
					
						$type = 'team';
						
					}
				
				}

				$conversation_obj = get_post($post->ID);
				
				$recipients = get_post_meta($conversation_obj->ID, 'recipients', true);
				
				if(!in_array($user->ID, $recipients)) {
					
					$value = get_option('cqpim_send_piping_reject');
					
					if(!empty($value)) {
				
						cqpim_send_unknown_account_email($fromEmail, $fromName, $item_type);
					
					}
					
					$mailbox->markMailAsRead($message);
					
						if(!empty($delete)) {
						
							$mailbox->deleteMail($message);
						
						}
				
					exit;					
					
				}
				
				$conversation_id = get_post_meta($conversation_obj->ID, 'conversation_id', true);
				
				update_post_meta($conversation_obj->ID, 'updated', array('by' => $user->ID, 'at' =>  current_time('timestamp')));
		
				$new_message = array(
				
					'post_type' => 'cqpim_messages',
					
					'post_status' => 'private',
					
					'post_title' => '',
					
					'post_content' => ''
				
				);
				
				$new_message = wp_insert_post($new_message);
				
				if(!is_wp_error($new_message)) {
					
					update_post_meta($new_message, 'conversation_id', $conversation_id);
					
					update_post_meta($new_message, 'sender', $user->ID);
					
					update_post_meta($new_message, 'message', $content);
					
					update_post_meta($new_message, 'piping', true);
					
					update_post_meta($new_message, 'stamp', current_time('timestamp'));
					
					update_post_meta($new_message, 'read', array($user->ID));
					
					$attachments = array();
					
					if(!empty($attached_media)) {
					
						$allowed = get_option('cqpim_allowed_extensions');
						
						$allowed = explode(',', $allowed);
						
						$i = 0;
					
						foreach($attached_media as $file) {
						
							$extension = pathinfo($file->name, PATHINFO_EXTENSION);

							if(!in_array(strtolower($extension), $allowed)){
								break;
							}									
							
							$filename = $file->filePath;
							
							$filename = substr($filename, strrpos($filename, '/') + 1);

							$attachment = array(
								'post_mime_type' => $extension,
								'post_title' => $file->name,
								'post_content' => '',
								'post_parent' => $new_message,
								'post_status' => 'publish',
								'guid' => $dirs['baseurl'] . '/cqpim-uploads/' . $filename,
								'post_owner' => $user->ID
							);

							$attachment_id = wp_insert_attachment( $attachment, $dirs['baseurl'] . '/cqpim-uploads/' . $filename );	

							require_once(ABSPATH . 'wp-admin/includes/image.php');
							
							$attachment_data = wp_generate_attachment_metadata( $attachment_id, $file->filePath );
							
							wp_update_attachment_metadata( $attachment_id, $attachment_data );			
																
							$attachment_obj = wp_get_attachment_metadata( $attachment );
							
							update_post_meta($attachment_id, 'cqpim', true);
							
							$attachment = get_post($attachment_id);
							
							$filename = $attachment->post_title;
						
							$i++;
							
							$attachments[] = $file->filePath;
						
						}
					
					}					
					
				}
				
				foreach($recipients as $recipient) {
					
					$recip = get_user_by('id', $recipient);
					
					$content = get_option('cqpim_new_message_content');
					
					$subject_template = get_option('cqpim_new_message_subject');
					
					$subject_template = str_replace('%%CONVERSATION_ID%%', '[' . $conversation_obj->ID . ']', $subject_template);
					
					$subject_template = str_replace('%%SENDER_NAME%%', $user->display_name, $subject_template);
					
					$content = str_replace('%%RECIPIENT_NAME%%', $recip->display_name, $content);

					$content = str_replace('%%SENDER_NAME%%', $user->display_name, $content);
					
					$content = str_replace('%%CONVERSATION_SUBJECT%%', $subject, $content);
					
					$content = str_replace('%%MESSAGE%%', $update, $content);

					$content = cqpim_replacement_patterns($content, $new_message, '');
					
					if($recip->ID != $user->ID) {
					
						cqpim_send_emails( $recip->user_email, $subject_template, $content, '', $attachments, 'sales' );
					
					}
					
				}				
				
				$mailbox->markMailAsRead($message);
				
					if(!empty($delete)) {
					
						$mailbox->deleteMail($message);
					
					}				
				
			}
		
		}
		
		} else {
		
			die();
			
		}
		
		exit;
	
	}
	
	function cqpim_send_unknown_account_email($fromEmail, $fromName, $type) {
	
		if(empty($type)) {
		
			$type = __('item', 'cqpim');
			
		}
	
		$email_subject = get_option('cqpim_bounce_subject');
		
		$email_content = get_option('cqpim_bounce_content');
		
		$email_content = str_replace('%%SENDER_NAME%%', $fromName, $email_content);
		
		$email_content = str_replace('%%TYPE%%', $type, $email_content);
		
		$email_content = cqpim_replacement_patterns($email_content, 0, '');
		
		$attachments = array();
		
		$to = $fromEmail;				

		cqpim_send_emails($to, $email_subject, $email_content, '', $attachments, 'sales');
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_test_piping", 

			"cqpim_test_piping");

	add_action( "wp_ajax_cqpim_test_piping", 

		  	"cqpim_test_piping");

	function cqpim_test_piping() {

		$mail_server2 = isset($_POST['cqpim_mail_server']) ? $_POST['cqpim_mail_server']: '';
		$mail_server = '{' . $mail_server2 . '}';
		$mailbox_name = isset($_POST['cqpim_mailbox_name']) ? $_POST['cqpim_mailbox_name']: '';
		$mailbox_pass = isset($_POST['cqpim_mailbox_pass']) ? $_POST['cqpim_mailbox_pass']: '';
		
		if(empty($mail_server2) || empty($mailbox_name) || empty($mailbox_pass)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> __('You must complete all fields', 'cqpim'),

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();			
		
		}
		
		$mbox = imap_open($mail_server, $mailbox_name, $mailbox_pass);
		
		if(!empty($mbox)) {
		
			$return =  array( 

				'error' 	=> false,

				'message' 	=> __('Settings are correct.', 'cqpim'),

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();				
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> __('Failed to connect to mailbox', 'cqpim'),

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();		
		
		}
	
	}
	
	// Online Users
	
	function  cqpim_user_online_update(){

		if ( is_user_logged_in()) {

			$logged_in_users = get_transient('online_status');

			$user = wp_get_current_user();

			$no_need_to_update = isset($logged_in_users[$user->ID])

				&& $logged_in_users[$user->ID] >  (time() - (1 * 60));

			if(!$no_need_to_update){
			
			  $logged_in_users[$user->ID] = time();
			  
			  set_transient('online_status', $logged_in_users, (1*60));
			  
			}
		}
	}
	add_action( 'admin_init', 'cqpim_user_online_update' );

	function cqpim_display_logged_in_users(){

		$logged_in_users = get_transient('online_status');

		if ( !empty( $logged_in_users ) ) {
		
			echo '<h3>Clients</h3>';
			
			$i = 0;
		
			foreach ( $logged_in_users as $key => $value) {
			
					$user = get_user_by( 'id', $key );
					
					if(in_array('cqpim_client', $user->roles)) {
					
					// Assignment
					
					$args = array(
					
						'post_type' => 'cqpim_client',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'client_details', true);
						
						if($team_details['user_id'] == $user->ID) {
						
							$assigned = $member->ID;
							
							$client_type = 'admin';
						
						}
					
					} 
					
					if(empty($assigned)) {
					
						foreach($members as $member) {
						
							$team_ids = get_post_meta($member->ID, 'client_ids', true);
							
							if(!is_array($team_ids)) {
							
								$team_ids = array($team_ids);
							
							}
							
							if(in_array($user->ID, $team_ids)) {
							
								$assigned = $member->ID;
								
								$client_type = 'contact';
							
							}
						
						} 			
					
					}
					
					echo '<div class="online-user"><a href="' . get_edit_post_link($assigned) . '" title="' . $user->display_name . '">';
					
							echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 							
					
					echo '</a></div>';
					
					$i++;
					
					}
					
			}
			
			echo '<div class="clear"></div>';
			
			if($i == 0) {
			
				echo '<p>There are no clients online</p>';
				
			}
			
			echo '<h3>Team Members</h3>';
			
			$i = 0;
			
			foreach ( $logged_in_users as $key => $value) {
			
					$user = get_user_by( 'id', $key );
					
					if(!in_array('cqpim_client', $user->roles)) {
					
					// Assignment
					
					$args = array(
					
						'post_type' => 'cqpim_teams',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'team_details', true);
						
						if($team_details['user_id'] == $user->ID) {
						
							$assigned = $member->ID;
							
							$client_type = 'admin';
						
						}
					
					} 
					
					if(empty($assigned)) {
						
						$assigned = '';
						
					}
					
					echo '<div class="online-user"><a href="' . get_edit_post_link($assigned) . '" title="' . $user->display_name . '">';
					
							echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 							
					
					echo '</a></div>';
					
					$i++;
					
					}
					
			}
			
			if($i == 0) {
			
				echo '<p>There are no team members online</p>';
				
			}
			
			echo '<div class="clear"></div>';
			
		} else {
		
			echo '<p><strong>' . __('There are currently no users online', 'cqpim') . '</strong></p>';
			
		}

	}
	
	function cqpim_clear_transient_on_logout() {

		$user_id = get_current_user_id();

		$users_transient_id = get_transient('online_status');

		if(is_array($users_transient_id)){
		
			foreach($users_transient_id as $id => $value ){
			
				if ( $id == $user_id ) {
				
					unset($users_transient_id[$user_id]);
					
					set_transient('online_status', $users_transient_id, (2*60));
					
					break;
					
				}
				
			}
			
		} else {
		
			delete_transient('online_status');
			
		}
	}
	add_action('clear_auth_cookie', 'cqpim_clear_transient_on_logout');
	
	$args = array(
		'name'          => __( 'CQPIM Client Dashboard Sidebar', 'cqpim' ),
		'id'            => 'cqpim_client_sidebar',
		'description'   => '',
		'class'         => 'cqpim-sidebar',
		'before_widget' => '<li id="%1$s" class="widget">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>' 
	);
	
	register_sidebar( $args );
	
	function cqpim_calculate_currency($post_id, $amount = '') {
	
		$currency = get_option('currency_symbol');
		
		$currency_position = get_option('currency_symbol_position');
		
		$currency_space = get_option('currency_symbol_space'); 
		
		$post_currency = get_post_meta($post_id, 'currency_symbol', true);
			
		$post_currency_space = get_post_meta($post_id, 'currency_space', true);		

		$post_currency_position = get_post_meta($post_id, 'currency_position', true);
		
		if($amount == "0") {
		
			return $post_currency . "0";
		
		}
		
		if(empty($amount)) {
			
			return $post_currency;
		
		} else {	
		
			$amount = number_format((float)$amount, 2, '.', '');
				
			if(!empty($post_currency_space)) {
			
				$space = ' ';
			
			} else {
			
				$space = '';
			
			}
			
			if($post_currency_position == 'l') {
			
				return $post_currency . $space . $amount;
			
			} else {
			
				return $amount . $space . $post_currency;
			
			}
		
		}
	
	}
	
	function cqpim_send_emails($to, $subject, $message, $headers = null, $attachments, $type) {
		
		$headers = array();
		
		$sender_name = get_option('company_name');
		
		if(!empty($type) && $type == 'support') {
		
			$sender_email = get_option('company_support_email');
		
		}
		
		if(!empty($type) && $type == 'accounts') {
		
			$sender_email = get_option('company_accounts_email');
		
		}
		
		if(!empty($type) && $type == 'sales') {
		
			$sender_email = get_option('company_sales_email');
		
		}
		
		$sender_name = $sender_name;
		
		$headers[] .= 'MIME-Version: 1.0';
		
		$headers[] .= 'Content-Type: text/html; charset=UTF-8';
	
		$headers[] = 'From: ' . $sender_name . ' <' . $sender_email . '>';
	
		$headers[] = 'X-Mailer: PHP/' . phpversion();

		$headers[] = 'X-Originating-IP: ' . $_SERVER['SERVER_ADDR'];
		
		$value = get_option('cqpim_piping_address');
		
		$cc = get_option('cqpim_cc_address');
		
		if(!empty($cc)) {
			
			$headers[] = 'BCC: ' . $cc;			
			
		}
		
		if(!empty($value) && $type == 'sales' || !empty($value) && $type == 'support') {
		
			$headers[] = 'Reply-to: ' . $value;
		
		}

		$message = nl2br($message);
		
		$html = get_option('cqpim_html_email');
		
		if(!empty($html)) {
			
			$logo = get_option('company_logo');
			
			$logo = isset($logo['company_logo']) ? $logo['company_logo'] : '';
			
			$styles = get_option('cqpim_html_email_styles');
			
			$html_message = '<html><head><style>' . $styles . '</style></head>';
			
			$html_message .= '<body>' . $html . '</body>';
			
			$html_message .= '</html>';
			
			$html_message = str_replace('%%EMAIL_CONTENT%%', $message, $html_message);
			
			$html_message = str_replace('%%LOGO%%', '<img src="' . $logo . '" />', $html_message);
			
			$message = $html_message;
			
		}
		
		return wp_mail($to, $subject, $message, $headers, $attachments);
		
	}
	
	add_action( "wp_ajax_cqpim_remove_logo", 

		"cqpim_remove_logo");	
	
	function cqpim_remove_logo() {
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		update_option($type, '');
		
		exit();
		
	}
	
	add_action( 'admin_init', 

		'cqpim_import_default_settings');	
	
	function cqpim_import_default_settings() {
		
		include('import/settings.php');
		
		$installed = get_option('cqpim_settings_imported');
		
		$company = get_option('company_name');
		
		$welcome = get_option('auto_welcome_subject');
		
		if(empty($installed) && empty($company) && empty($welcome)) {
		
			$settings = cqpim_settings_values();
			
			foreach($settings as $element) {
				
				foreach($element as $key => $setting) {
					
					update_option($key, $setting);
					
				}
				
			}
			
		}
		
	}

	
	
	
