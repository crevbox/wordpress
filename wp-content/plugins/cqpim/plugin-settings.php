<?php
/**
 * 	TC+ Dashboard
 *
 */
	
	// Add Link to external help page

	add_action( 'admin_menu' , 'register_cqpim_help_page', 31 );

	function register_cqpim_help_page() {

		$mypage = add_submenu_page(	
					'cqpim-dashboard',
					__('CQPIM Help', 'cqpim'), 			
					__('CQPIM Help', 'cqpim'),  			
					'edit_cqpim_help', 				
					'cqpim-help', 		
					'cqpim_help'
		);

	}

	function cqpim_help() {

	}
	
	 // Redirect help link to documentation

	function cqpim_help_preprocess_pages($value){

		global $pagenow;
		
		$page = (isset($_REQUEST['page']) ? $_REQUEST['page'] : false);
		
		if($pagenow=='admin.php' && $page=='cqpim-help'){
		
			wp_redirect('http://www.cqpim.uk/docs/');
			
			exit;
			
		}
		
	}

	add_action('admin_init', 'cqpim_help_preprocess_pages');
	
	// Add CQPIM settings page

	add_action( 'admin_menu' , 'register_cqpim_settings_page', 30 ); 

	function register_cqpim_settings_page() {

		$mypage = add_submenu_page(	
					'cqpim-dashboard',
					__('Settings', 'cqpim'),  			
					__('Settings', 'cqpim'),  			
					'edit_cqpim_settings', 				
					'cqpim-settings', 		
					'cqpim_settings'
		);

		add_action( 'load-' . $mypage, 'cqpim_enqueue_plugin_option_scripts' );
		
		// register settings

		add_action( 'admin_init', 'register_cqpim_settings' );

	}
	
	// Validate uploaded logo

	function cqpim_validate_image($plugin_options) { 

		//$keys = array_keys($_FILES); 
		
		$i = 0; 
		
		foreach ( $_FILES as $key => $image ) {    
		
			if ($key == 'company_logo' && $image['size']) { 
				
				if ( preg_match('/(jpg|jpeg|png|gif)$/', $image['type']) ) { 
				
					$override = array('test_form' => false);      
					
					$file = wp_handle_upload( $image, $override );
					
					$plugin_options[$key] = $file['url'];  
					
				} else {             
				
					$options = get_option('company_logo');       
					
					$plugin_options[$key] = $options[$logo];          
					
					wp_die('No image was uploaded.');     
				
				}  
				
			} else {     
			
				$options = get_option('company_logo');     
				
				if(!empty($options[$key])) {
				
					$plugin_options[$key] = $options[$key];

				}   
				
			}   
			
			$i++; 
			
		} 
		
		return $plugin_options;
		
	}
	
	function cqpim_validate_logo($plugin_options) { 

		//$keys = array_keys($_FILES); 
		
		$i = 0; 
		
		foreach ( $_FILES as $key => $image ) {    
		
			if ($key == 'cqpim_dash_logo' && $image['size']) { 
				
				if ( preg_match('/(jpg|jpeg|png|gif)$/', $image['type']) ) { 
				
					$override = array('test_form' => false);      
					
					$file = wp_handle_upload( $image, $override );
					
					$plugin_options[$key] = $file['url'];  
					
				} else {             
				
					$options = get_option('cqpim_dash_logo');       
					
					$plugin_options[$key] = $options[$logo];          
					
					wp_die('No image was uploaded.');     
				
				}  
				
			} else {     
			
				$options = get_option('cqpim_dash_logo');     
				
				if(!empty($options[$key])) {
				
					$plugin_options[$key] = $options[$key];

				}  
				
			}   
			
			$i++; 
			
		} 
		
		return $plugin_options;
		
	}
	
	function cqpim_validate_bg($plugin_options) { 

		//$keys = array_keys($_FILES); 
		
		$i = 0; 
		
		foreach ( $_FILES as $key => $image ) {    
		
			if ($key == 'cqpim_dash_bg' && $image['size']) { 
				
				if ( preg_match('/(jpg|jpeg|png|gif)$/', $image['type']) ) { 
				
					$override = array('test_form' => false);      
					
					$file = wp_handle_upload( $image, $override );
					
					$plugin_options[$key] = $file['url'];  
					
				} else {             
				
					$options = get_option('cqpim_dash_bg');       
					
					$plugin_options[$key] = $options[$logo];          
					
					wp_die('No image was uploaded.');     
				
				}  
				
			} else {     
			
				$options = get_option('cqpim_dash_bg');     
				
				if(!empty($options[$key])) {
				
					$plugin_options[$key] = $options[$key];

				}   
				
			}   
			
			$i++; 
			
		} 
		
		return $plugin_options;
		
	}
	
	function cqpim_validate_invlogo($plugin_options) { 

		//$keys = array_keys($_FILES); 
		
		$i = 0; 
		
		foreach ( $_FILES as $key => $image ) {    
		
			if ($key == 'cqpim_invoice_logo' && $image['size']) { 
				
				if ( preg_match('/(jpg|jpeg|png|gif)$/', $image['type']) ) { 
				
					$override = array('test_form' => false);      
					
					$file = wp_handle_upload( $image, $override );
					
					$plugin_options[$key] = $file['url'];
					
				} else {             
				
					$options = get_option('cqpim_invoice_logo');       
					
					$plugin_options[$key] = $options[$logo];          
					
					wp_die('No image was uploaded.');     
				
				}  
				
			} else {     
			
				$options = get_option('cqpim_invoice_logo');  

				if(!empty($options[$key])) {
				
					$plugin_options[$key] = $options[$key];

				}
				
			}   
			
			$i++; 
			
		} 
		
		return $plugin_options;
		
	}

	function register_cqpim_settings() {

		// Plugin Settings
		
		register_setting( 'cqpim_settings', 'plugin_name', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'cqpim_date_format', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_allowed_extensions', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_timezone', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_disable_avatars', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_invoice_slug', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_quote_slug', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_project_slug', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_support_slug', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_task_slug', '' );
		
		register_setting( 'cqpim_settings', 'enable_quotes', '' );
		
		register_setting( 'cqpim_settings', 'cqpim_enable_messaging', '' );
		
		register_setting( 'cqpim_settings', 'enable_quote_terms', '' );
		
		register_setting( 'cqpim_settings', 'enable_project_creation', '' );
		
		register_setting( 'cqpim_settings', 'enable_project_contracts', '' );
		
		register_setting( 'cqpim_settings', 'disable_invoices', '' );
		
		register_setting( 'cqpim_settings', 'invoice_workflow', '' );
		
		register_setting( 'cqpim_settings', 'auto_send_invoices', '' );
		
		// Company Settings
		
		register_setting( 'cqpim_settings', 'team_type' );
		
		register_setting( 'cqpim_settings', 'company_name', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_address', '' );
		
		register_setting( 'cqpim_settings', 'company_postcode', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_telephone', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_sales_email', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_accounts_email', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_support_email', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'cqpim_cc_address', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_logo', 'cqpim_validate_image' );
		
		register_setting( 'cqpim_settings', 'company_bank_name', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'currency_symbol', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'currency_symbol_position');
		
		register_setting( 'cqpim_settings', 'currency_symbol_space');
		
		register_setting( 'cqpim_settings', 'allow_client_currency_override', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'allow_project_currency_override', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'allow_quote_currency_override', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'allow_invoice_currency_override', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'allow_supplier_currency_override', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'currency_code' );
		
		register_setting( 'cqpim_settings', 'company_bank_ac', 'sanitize_text_field' );

		register_setting( 'cqpim_settings', 'company_bank_sc', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_bank_iban', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_invoice_terms', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'sales_tax_rate', 'sanitize_text_field' );

		register_setting( 'cqpim_settings', 'sales_tax_name', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'sales_tax_reg', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'secondary_sales_tax_rate', 'sanitize_text_field' );

		register_setting( 'cqpim_settings', 'secondary_sales_tax_name', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'secondary_sales_tax_reg', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'company_number', 'sanitize_text_field' );
		
		// Client Settings
		
		register_setting( 'cqpim_settings', 'client_dashboard_type' );
		
		register_setting( 'cqpim_settings', 'auto_welcome' );
		
		register_setting( 'cqpim_settings', 'auto_welcome_subject' );
		
		register_setting( 'cqpim_settings', 'auto_welcome_content' );
		
		register_setting( 'cqpim_settings', 'client_password_reset_subject' );
		
		register_setting( 'cqpim_settings', 'client_password_reset_content' );
		
		register_setting( 'cqpim_settings', 'password_reset_subject' );
		
		register_setting( 'cqpim_settings', 'password_reset_content' );
		
		register_setting( 'cqpim_settings', 'added_contact_subject' );
		
		register_setting( 'cqpim_settings', 'added_contact_content' );
		
		register_setting( 'cqpim_settings', 'allow_client_settings' );
		
		register_setting( 'cqpim_settings', 'allow_client_users' );
		
		register_setting( 'cqpim_settings', 'cqpim_dash_logo', 'cqpim_validate_logo' );
		
		register_setting( 'cqpim_settings', 'cqpim_dash_bg', 'cqpim_validate_bg' );
		
		// Quote Settings
		
		register_setting( 'cqpim_settings', 'enable_frontend_anon_quotes' );
		
		register_setting( 'cqpim_settings', 'enable_client_quotes' );
		
		register_setting( 'cqpim_settings', 'quote_header' );
		
		register_setting( 'cqpim_settings', 'quote_footer' );
		
		register_setting( 'cqpim_settings', 'quote_acceptance_text' );
		
		register_setting( 'cqpim_settings', 'quote_email_subject', 'sanitize_text_field' );
		
		register_setting( 'cqpim_settings', 'quote_default_email' );
		
		// Project Settings
		
		register_setting( 'cqpim_settings', 'default_contract_text' );

		register_setting( 'cqpim_settings', 'contract_acceptance_text' );
		
		register_setting( 'cqpim_settings', 'client_contract_subject' );
		
		register_setting( 'cqpim_settings', 'client_contract_email' );
		
		register_setting( 'cqpim_settings', 'client_update_subject' );
		
		register_setting( 'cqpim_settings', 'client_update_email' );
		
		
		register_setting( 'cqpim_settings', 'client_message_subject' );
		
		register_setting( 'cqpim_settings', 'client_message_email' );
		
		register_setting( 'cqpim_settings', 'company_message_subject' );
		
		register_setting( 'cqpim_settings', 'company_message_email' );
		
		register_setting( 'cqpim_settings', 'auto_contract' );
		
		register_setting( 'cqpim_settings', 'auto_invoice' );
		
		register_setting( 'cqpim_settings', 'auto_update' );
		
		register_setting( 'cqpim_settings', 'auto_completion' );
		
		// Invoice Settings
		
		register_setting( 'cqpim_settings', 'cqpim_invoice_template' );
		
		register_setting( 'cqpim_settings', 'cqpim_invoice_logo', 'cqpim_validate_invlogo' );
		
		register_setting( 'cqpim_settings', 'cqpim_clean_main_colour');
		
		register_setting( 'cqpim_settings', 'client_invoice_email_attach' );
		
		register_setting( 'cqpim_settings', 'client_invoice_after_send_remind_days' );
		
		register_setting( 'cqpim_settings', 'client_invoice_before_terms_remind_days' );
		
		register_setting( 'cqpim_settings', 'client_invoice_after_terms_remind_days' );
		
		register_setting( 'cqpim_settings', 'client_invoice_high_priority' );
		
		register_setting( 'cqpim_settings', 'client_invoice_paypal_address' );
		
		register_setting( 'cqpim_settings', 'client_invoice_stripe_key' );
		
		register_setting( 'cqpim_settings', 'client_invoice_stripe_secret' );
		
		register_setting( 'cqpim_settings', 'client_invoice_subject' );
		
		register_setting( 'cqpim_settings', 'client_invoice_email' );
		
		register_setting( 'cqpim_settings', 'client_deposit_invoice_subject' );
		
		register_setting( 'cqpim_settings', 'client_deposit_invoice_email' );
		
		register_setting( 'cqpim_settings', 'client_invoice_reminder_subject' );
		
		register_setting( 'cqpim_settings', 'client_invoice_reminder_email' );
		
		register_setting( 'cqpim_settings', 'client_invoice_overdue_subject' );
		
		register_setting( 'cqpim_settings', 'client_invoice_overdue_email' );
		
		register_setting( 'cqpim_settings', 'client_invoice_footer' );
		
		register_setting( 'cqpim_settings', 'client_deposit_invoice_email' );
		
		register_setting( 'cqpim_settings', 'client_invoice_allow_partial');
		
		register_setting( 'cqpim_settings', 'client_invoice_twocheck_sid');
		
		register_setting( 'cqpim_settings', 'client_invoice_receipt_subject' );
		
		register_setting( 'cqpim_settings', 'client_invoice_receipt_email' );
		
		// Teams

		register_setting( 'cqpim_settings', 'team_account_subject' );
		
		register_setting( 'cqpim_settings', 'team_account_email' );	
		
		register_setting( 'cqpim_settings', 'team_reset_subject' );
		
		register_setting( 'cqpim_settings', 'team_reset_email' );

		register_setting( 'cqpim_settings', 'team_project_subject' );
		
		register_setting( 'cqpim_settings', 'team_project_email' );	

		register_setting( 'cqpim_settings', 'team_assignment_subject' );
		
		register_setting( 'cqpim_settings', 'team_assignment_email' );	

		// Support
		
		register_setting( 'cqpim_settings', 'client_create_ticket_subject' );
		
		register_setting( 'cqpim_settings', 'client_create_ticket_email' );
		
		register_setting( 'cqpim_settings', 'client_update_ticket_subject' );
		
		register_setting( 'cqpim_settings', 'client_update_ticket_email' );
		
		register_setting( 'cqpim_settings', 'company_update_ticket_subject' );
		
		register_setting( 'cqpim_settings', 'company_update_ticket_email' );
		
		// Quote Forms
		
		register_setting( 'cqpim_settings', 'cqpim_frontend_form' );
		
		register_setting( 'cqpim_settings', 'cqpim_backend_form' );
		
		register_setting( 'cqpim_settings', 'form_reg_auto_welcome' );
		
		register_setting( 'cqpim_settings', 'form_auto_welcome' );
		
		register_setting( 'cqpim_settings', 'new_quote_subject' );
		
		register_setting( 'cqpim_settings', 'new_quote_email' );
		
		register_setting( 'cqpim_settings', 'cqpim_dash_css' );
		
		register_setting( 'cqpim_settings', 'cqpim_logout_url' );
		
		// Suppliers and Expenses
		
		register_setting( 'cqpim_settings', 'cqpim_activate_expense_auth' );
		
		register_setting( 'cqpim_settings', 'cqpim_expense_auth_limit' );
		
		register_setting( 'cqpim_settings', 'cqpim_expense_auth_members' );
		
		register_setting( 'cqpim_settings', 'cqpim_auth_email_subject' );
		
		register_setting( 'cqpim_settings', 'cqpim_auth_email_content' );
		
		register_setting( 'cqpim_settings', 'cqpim_authorised_email_subject' );
		
		register_setting( 'cqpim_settings', 'cqpim_authorised_email_content' );
		
		// Piping Settings 
		
		register_setting( 'cqpim_settings', 'cqpim_mail_server' );
		
		register_setting( 'cqpim_settings', 'cqpim_piping_address' );
		
		register_setting( 'cqpim_settings', 'cqpim_mailbox_name' );
		
		register_setting( 'cqpim_settings', 'cqpim_mailbox_pass' );
		
		register_setting( 'cqpim_settings', 'cqpim_string_prefix' );
		
		register_setting( 'cqpim_settings', 'cqpim_create_support_on_email' );
		
		register_setting( 'cqpim_settings', 'cqpim_send_piping_reject' );
		
		register_setting( 'cqpim_settings', 'cqpim_piping_delete' );
		
		register_setting( 'cqpim_settings', 'cqpim_bounce_subject' );
		
		register_setting( 'cqpim_settings', 'cqpim_bounce_content' );
		
		// Messaging  Settings
		
		register_setting( 'cqpim_settings', 'cqpim_new_message_subject');
		
		register_setting( 'cqpim_settings', 'cqpim_new_message_content');
		
		register_setting( 'cqpim_settings', 'cqpim_messages_allow_client');
		
		// HTML Email
		
		register_setting( 'cqpim_settings', 'cqpim_html_email_styles');
		
		register_setting( 'cqpim_settings', 'cqpim_html_email');
		
	}
	
	// Allow CQPIM admins access to these settings

	function cqpim_settings_page_capability( $capability ) {

		return 'edit_cqpim_settings';
		
	}

	add_filter( 'option_page_capability_cqpim_settings', 'cqpim_settings_page_capability' );


	function cqpim_settings() { ?>

		<div class="wrap" id="cqpim-settings"><div id="icon-tools" class="icon32"></div>

			<h1><?php $text = __('CQPIM Settings', 'cqpim'); _e('CQPIM Settings', 'cqpim'); ?></h1>
			
		<?php 
		
		$user = wp_get_current_user();
		
		if(in_array('administrator', $user->roles)) { 
		
			$args = array(
			
				'post_type' => 'cqpim_teams',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$members = get_posts($args);
			
			foreach($members as $member) {
			
				$team_details = get_post_meta($member->ID, 'team_details', true);
				
				if($team_details['user_id'] == $user->ID) {
				
					$assigned = $member->ID;
				
				}
			
			}
			
			if(empty($assigned)) { ?>
			
				<div class="cqpim-dash-item-full grid-item">
					
					<div class="cqpim_block cqpim-alert cqpim-alert-warning">
					
						<div style="padding:20px">	
						
							<h3><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php _e('You need to link your account to a CQPIM Team Member', 'cqpim'); ?></h3>

							<p><?php $text = __('It would appear that the Wordpress Administrator account that you are logged in with is not related to a CQPIM Team Member. In order for the plugin to work correctly, you need to add a Team Member that is linked to your WP User Account.', 'cqpim'); _e('It would appear that the Wordpress Administrator account that you are logged in with is not related to a CQPIM Team Member. In order for the plugin to work correctly, you need to add a Team Member that is linked to your WP User Account.', 'cqpim'); ?></p>
							
							<p><?php $text = __('We can do this for you though, just click Create Linked Team Member. You will then be able to add other team members or just work with this account.', 'cqpim'); _e('We can do this for you though, just click Create Linked Team Member. You will then be able to add other team members or just work with this account.', 'cqpim') ?></p>
							
							<button style="margin-left:0" id="create_linked_team" class="metabox-add-button left" data-uid="<?php echo $user->ID; ?>"><?php $text = __('Create Linked Team Member'); _e('Create Linked Team Member'); ?></button>

							<div class="clear"></div>
						
						</div>
					
					</div>
					
				</div>
			
			<?php }
		
		}		
		
		?>
		
		
		<form method="post" action="options.php" enctype="multipart/form-data">

				<div id="main-container" class="cqpim_block">
				
					<div class="cqpim_block_title">
					
						<div class="caption">
							
							<i class="fa fa-cog font-green-sharp" aria-hidden="true"></i>
								
							<span class="caption-subject font-green-sharp sbold"><?php _e('Plugin Settings', 'cqpim'); ?> </span>
						
						</div>
						
					</div>

					<?php 

					$option_group = 'cqpim_settings';

					settings_fields( $option_group ); 
					
					$settings = get_option('wordpress_settings_48njgknjg6745hjdjhjshbhgjf'); ?>

					<div id="tabs">

						<ul>
						
							<li><a href="#tabs-11"><?php $text = __('Plugin Settings', 'cqpim'); _e('Plugin Settings', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-1"><?php $text = __('Your Company', 'cqpim'); _e('Your Company', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-2"><?php $text = __('Clients', 'cqpim'); _e('Clients', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-9"><?php $text = __('Client Dashboard', 'cqpim'); _e('Client Dashboard', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-3"><?php $text = __('Quotes / Estimates', 'cqpim'); _e('Quotes / Estimates', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-4"><?php $text = __('Projects', 'cqpim'); _e('Projects', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-5"><?php $text = __('Invoices', 'cqpim'); _e('Invoices', 'cqpim'); ?></a></li>							
							
							<li><a href="#tabs-6"><?php $text = __('Team Members', 'cqpim'); _e('Team Members', 'cqpim'); ?></a></li>

							<li><a href="#tabs-10"><?php $text = __('Tasks', 'cqpim'); _e('Tasks', 'cqpim'); ?></a></li>								
							
							<li><a href="#tabs-7"><?php $text = __('Support Tickets', 'cqpim'); _e('Support Tickets', 'cqpim'); ?></a></li>

							<li><a href="#tabs-8"><?php $text = __('Forms', 'cqpim'); _e('Forms', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-16"><?php $text = __('Suppliers / Expenses', 'cqpim'); _e('Suppliers / Expenses', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-12"><?php $text = __('Email Piping', 'cqpim'); _e('Email Piping', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-14"><?php $text = __('Messaging System', 'cqpim'); _e('Messaging System', 'cqpim'); ?></a></li>
							
							<li><a href="#tabs-15"><?php $text = __('HTML Email Template', 'cqpim'); _e('HTML Email Template', 'cqpim'); ?></a></li>
							
							<?php 
							
							$user = wp_get_current_user();
							
							if(in_array('administrator', $user->roles)) { ?>
							
								<li><a href="#tabs-13"><?php _e('Plugin Reset', 'cqpim'); ?></a></li>
							
							<?php } ?>
							
						</ul>
						
						<div id="tabs-11">
								
							<h3><?php $text = __('Date', 'cqpim'); _e('Date', 'cqpim'); ?></h3>
								
							<table>
								
								<tr>
								
									<td class="title"><?php $text = __('Date Format:', 'cqpim'); _e('Date Format:', 'cqpim'); ?></td>
									
									<td>
									
										<?php $value = get_option('cqpim_date_format'); ?>
									
										<select name="cqpim_date_format">
										
											<option value=""><?php _e('Choose a date format', 'cqpim'); ?></option>
											
											<option value="Y-m-d" <?php selected( $value, "Y-m-d" ); ?>>Y-m-d (<?php echo date('Y-m-d'); ?>)</option>
											
											<option value="m/d/Y" <?php selected( $value, "m/d/Y" ); ?>>m/d/Y (<?php echo date('m/d/Y'); ?>)</option>
											
											<option value="d/m/Y" <?php selected( $value, "d/m/Y" ); ?>>d/m/Y (<?php echo date('d/m/Y'); ?>)</option>
										
										</select>
									
									</td>
									
								</tr>

							</table>
							
								<h3><?php $text = __('File Uploads', 'cqpim'); _e('File Uploads', 'cqpim'); ?></h3>
								
								<p><?php _e('Enter a comma separated list of extensions that you want clients to be able to upload to tasks and tickets.', 'cqpim'); ?></p>
								
								<table>
									
									<tr>
									
										<td class="title"><?php $text = __('Allowed Extensions:', 'cqpim'); _e('Allowed Extensions:', 'cqpim'); ?></td>
										
										<td>
										
											<?php $value = get_option('cqpim_allowed_extensions'); ?>
										
											<input type="text" name="cqpim_allowed_extensions" value="<?php echo $value; ?>" />
										
										</td>
										
									</tr>
									
								</table>
								
							<h3><?php _e('Avatars', 'cqpim'); ?></h3>
							
								<p><?php _e('CQPIM uses the WordPress avatar to show thumbnails of users. By default this uses Gravatar.org, but you can also use plugins to upload custom avatars.', 'cqpim'); ?></p>
								
								<table>
									
									<tr>
									
										<td class="title"><?php _e('Disable Avatars', 'cqpim'); ?></td>
										
										<td>
										
											<?php $disable = get_option('cqpim_disable_avatars'); ?>
										
											<input type="checkbox" name="cqpim_disable_avatars" value="1" <?php if(!empty($disable)) { ?> checked="checked"<?php } ?>/>
										
										</td>
										
									</tr>
									
								</table>

							
							<h3><?php $text = __('URL Rewrites', 'cqpim'); _e('URL Rewrites', 'cqpim'); ?></h3>
							
							<p><?php _e('By default, invoices, quotes and projects will have CQPIM based URL\'s. This is done for compatibility so that you don\'t end up with duplicate slugs in Wordpress. You can change these here. Make sure that the URL slug that you choose does not exist on your site already, and also make sure that you flush your permalinks when these are updated, otherwise you may experience 404 errors.', 'cqpim'); ?></p>
						
							<table>
								
								<tr>
								
									<td class="title"><?php $text = __('Invoices:', 'cqpim'); _e('Invoices:', 'cqpim'); ?></td>
									
									<td>
									
										<?php $value = get_option('cqpim_invoice_slug'); ?>
									
										<input type="text" name="cqpim_invoice_slug" value="<?php echo $value; ?>" />
									
									</td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Quotes:', 'cqpim'); _e('Quotes:', 'cqpim'); ?></td>
									
									<td>
									
										<?php $value = get_option('cqpim_quote_slug'); ?>
									
										<input type="text" name="cqpim_quote_slug" value="<?php echo $value; ?>" />
									
									</td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Projects:', 'cqpim'); _e('Projects:', 'cqpim'); ?></td>
									
									<td>
									
										<?php $value = get_option('cqpim_project_slug'); ?>
									
										<input type="text" name="cqpim_project_slug" value="<?php echo $value; ?>" />
									
									</td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Support Tickets:', 'cqpim'); _e('Support Tickets:', 'cqpim'); ?></td>
									
									<td>
									
										<?php $value = get_option('cqpim_support_slug'); ?>
									
										<input type="text" name="cqpim_support_slug" value="<?php echo $value; ?>" />
									
									</td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Tasks:', 'cqpim'); _e('Tasks:', 'cqpim'); ?></td>
									
									<td>
									
										<?php $value = get_option('cqpim_task_slug'); ?>
									
										<input type="text" name="cqpim_task_slug" value="<?php echo $value; ?>" />
									
									</td>
									
								</tr>

							</table>							
							
							<div class="clear"></div>
							
							<h3><?php $text = __('Workflow', 'cqpim'); _e('Workflow', 'cqpim'); ?></h3>
							
							<h4><?php _e('Project Workflow', 'cqpim'); ?></h4>
							
							<?php $checked = get_option('enable_quotes'); ?>
							
							<input type="checkbox" name="enable_quotes" value="1" <?php checked($checked, 1, true); ?> /> <?php _e('Enable the quotes system.', 'cqpim'); ?><br /><br />
							
							<?php $checked = get_option('enable_quote_terms'); ?>
							
							<input type="checkbox" name="enable_quote_terms" value="1" <?php checked($checked, 1, true); ?> /> <?php _e('Add Terms & Conditions section to quotes.', 'cqpim'); ?><br /><br />
							
							<?php $checked = get_option('enable_project_creation'); ?>
							
							<input type="checkbox" name="enable_project_creation" value="1" <?php checked($checked, 1, true); ?> /> <?php _e('Create a project automatically when a quote is accepted.', 'cqpim'); ?><br /><br />
							
							<?php $checked = get_option('enable_project_contracts'); ?>
							
							<input type="checkbox" name="enable_project_contracts" value="1" <?php checked($checked, 1, true); ?> /> <?php _e('Enable the contracts feature in projects.', 'cqpim'); ?><br /><br />
							
							<?php $checked = get_option('auto_contract'); ?>
							
							<input type="checkbox" name="auto_contract" id="auto_contract" value="1" <?php checked($checked, 1, true); ?> /> <?php $text = __('Send the project contract automatically when a quote is accepted and the project is created.', 'cqpim'); _e('Send the project contract automatically when a quote is accepted and the project is created.', 'cqpim'); ?>
							
							<br /><br />
							
							<h4><?php _e('Invoice Workflow', 'cqpim'); ?></h4>

							<?php $checked = get_option('disable_invoices'); ?>

                            <input type="checkbox" name="disable_invoices" id="disable_invoices" value="1" <?php checked($checked, 1, true); ?> /> <?php $text = __('Disable the Invoice Section.', 'cqpim'); _e('Disable the Invoice section.', 'cqpim'); ?><br /><br />
							
							<?php $checked = get_option('invoice_workflow'); ?>
							
							<table>
								
								<tr><td><input type="radio" name="invoice_workflow" value="0" <?php checked($checked, 0, true); ?> /></td><td><?php _e('Create deposit invoice (if deposit amount selected) automatically when contract is signed (contract mode only) or when project is created from quote (no contract mode). Create a completion invoice (project total minus deposit) when project is marked as signed off.', 'cqpim'); ?></td></tr>
								
								<tr><td colspan="2"></td></tr>
								
								<tr><td><input type="radio" name="invoice_workflow" value="1" <?php checked($checked, 1, true); ?> /></td><td><?php _e('Create deposit invoice (if deposit amount selected) automatically when contract is signed (contract mode only) or when project is created from quote (no contract mode). Create a new invoice when milestones are marked as complete for the total milestone fee minus the deposit percentage.', 'cqpim'); ?></td></tr>
							
							</table>
							
							<br />
							
							<?php $checked = get_option('auto_send_invoices'); ?>
							
							<input type="checkbox" name="auto_send_invoices" value="1" <?php checked($checked, 1, true); ?> /> <?php _e('Send invoices to the client automatically when they are created.', 'cqpim'); ?>
							
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>						
						
						</div>
						
						<div id="tabs-1">
						
							<table>
								
								<tr>
								
									<td colspan="2"><h3><?php $text = __('Company Details', 'cqpim'); _e('Company Details', 'cqpim'); ?></h3></td>
									
								</tr>
						
								<tr>
								
									<td class="title"><?php $text = __('Company Name:', 'cqpim'); _e('Company Name:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_name" name="company_name" value="<?php echo get_option('company_name'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Company Address:', 'cqpim'); _e('Company Address:', 'cqpim'); ?></td>
									
									<td><textarea id="company_address" name="company_address"><?php echo get_option('company_address'); ?></textarea></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Company Postcode:', 'cqpim'); _e('Company Postcode:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_postcode" name="company_postcode" value="<?php echo get_option('company_postcode'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Company Telephone:', 'cqpim'); _e('Company Telephone:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_telephone" name="company_telephone" value="<?php echo get_option('company_telephone'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Sales Email:', 'cqpim'); _e('Sales Email:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_sales_email" name="company_sales_email" value="<?php echo get_option('company_sales_email'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Accounts Email:', 'cqpim'); _e('Accounts Email:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_accounts_email" name="company_accounts_email" value="<?php echo get_option('company_accounts_email'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Support Email (For Support Tickets):', 'cqpim'); _e('Support Email (For Support Tickets):', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_support_email" name="company_support_email" value="<?php echo get_option('company_support_email'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Outgoing Email BCC Address (copies all outgoing emails):', 'cqpim'); _e('Outgoing Email BCC Address (copies all outgoing emails):', 'cqpim'); ?></td>
									
									<td><input type="text" id="cqpim_cc_address" name="cqpim_cc_address" value="<?php echo get_option('cqpim_cc_address'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Company Number:', 'cqpim'); _e('Company Number:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_number" name="company_number" value="<?php echo get_option('company_number'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td colspan="2"><h3><?php $text = __('Company Logo:', 'cqpim'); _e('Company Logo:', 'cqpim'); ?></h3></td>
									
								</tr>
								
								<?php 
								
								$logo = get_option('company_logo'); 
								
								if($logo) { ?>
								
								<tr>
								
								
									<td colspan="2"><img style="max-width:400px; margin:20px 0; background:#ececec" src="<?php echo $logo['company_logo']; ?>" />
									
									<br />
									
									<button class="remove_logo cqpim_button mt-20 op bg-red font-white rounded_2" data-type="company_logo"><?php _e('Remove', 'cqpim'); ?></button>
									
									<br /><br />
									
									</td>
									
								</tr>
								
								<?php } ?>
								
								<tr>
								
									<td colspan="2"><input type="file" name="company_logo" /></td>
									
								</tr>
								
								<tr>
								
									<td colspan="2"><h3><?php $text = __('Financial Details', 'cqpim'); _e('Financial Details', 'cqpim'); ?></h3></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Currency Symbol:', 'cqpim'); _e('Currency Symbol:', 'cqpim'); ?></td>
									
									<td><input type="text" id="currency_symbol" name="currency_symbol" value="<?php echo get_option('currency_symbol'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Currency Symbol Position:', 'cqpim'); _e('Currency Symbol Position:', 'cqpim'); ?></td>
									
									<td>
									
										<?php
										
											$value = get_option('currency_symbol_position');
										
										?>
										
										<select name="currency_symbol_position">
										
											<option value="l" <?php if($value == 'l') { echo 'selected="selected"'; } ?>><?php _e('Before Amount', 'cqpim'); ?></option>
											
											<option value="r" <?php if($value == 'r') { echo 'selected="selected"'; } ?>><?php _e('After Amount', 'cqpim'); ?></option>
										
										</select>
									
									</td>
									
								</tr>								
								
								<tr style="margin:5px 0">
								
									<?php
									
										$value = get_option('currency_symbol_space');
									
									?>
								
									<td class="title"><?php _e('Add Space Between Amount and Currency Symbol', 'cqpim'); ?></td>
									
									<td><input type="checkbox" id="currency_symbol_space" name="currency_symbol_space" value="1" <?php if($value == '1') { echo 'checked'; } ?> /></td>
									
								</tr>

								<tr style="margin:5px 0">
								
									<?php
									
										$value = get_option('allow_client_currency_override');
									
									?>
								
									<td class="title"><?php _e('Allow Currency to be Set Per Client:', 'cqpim'); ?></td>
									
									<td><input type="checkbox" id="allow_client_currency_override" name="allow_client_currency_override" value="1" <?php if($value == '1') { echo 'checked'; } ?> /></td>
									
								</tr>
								
								<tr style="margin:5px 0">
								
									<?php
									
										$value = get_option('allow_quote_currency_override');
									
									?>
								
									<td class="title"><?php _e('Allow Currency to be Set Per Quote:', 'cqpim'); ?></td>
									
									<td><input type="checkbox" id="allow_quote_currency_override" name="allow_quote_currency_override" value="1" <?php if($value == '1') { echo 'checked'; } ?> /></td>
									
								</tr>
								
								<tr style="margin:5px 0">
								
									<?php
									
										$value = get_option('allow_project_currency_override');
									
									?>
								
									<td class="title"><?php _e('Allow Currency to be Set Per Project:', 'cqpim'); ?></td>
									
									<td><input type="checkbox" id="allow_project_currency_override" name="allow_project_currency_override" value="1" <?php if($value == '1') { echo 'checked'; } ?> /></td>
									
								</tr>
								
								<tr style="margin:5px 0">
								
									<?php
									
										$value = get_option('allow_invoice_currency_override');
									
									?>
								
									<td class="title"><?php _e('Allow Currency to be Set Per Invoice:', 'cqpim'); ?></td>
									
									<td><input type="checkbox" id="allow_invoice_currency_override" name="allow_invoice_currency_override" value="1" <?php if($value == '1') { echo 'checked'; } ?> /></td>
									
								</tr>
								
								<?php if(is_plugin_active('cqpim-expenses/cqpim-expenses.php')) { ?>
								
									<tr style="margin:5px 0">
									
										<?php
										
											$value = get_option('allow_supplier_currency_override');
										
										?>
									
										<td class="title"><?php _e('Allow Currency to be Set Per Supplier:', 'cqpim'); ?></td>
										
										<td><input type="checkbox" id="allow_supplier_currency_override" name="allow_supplier_currency_override" value="1" <?php if($value == '1') { echo 'checked'; } ?> /></td>
										
									</tr>
								
								<?php } ?>
								
								<tr>
								
									<td class="title"><?php $text = __('Currency Code (Used for Payment Gateways):', 'cqpim'); _e('Currency Code (Used for Payment Gateways):', 'cqpim'); ?></td>
									
									<td>
									
										<?php $code = get_option('currency_code'); ?>
									
										<select name="currency_code" id="currency_code">
									
											<option value="Select a Currency"><?php $text = __('Select a Currency', 'cqpim'); _e('Select a Currency', 'cqpim'); ?></option>
											
											<option value="AUD" <?php if($code == 'AUD') { echo 'selected="selected"'; } ?>><?php _e('Australian Dollar (AUD)', 'cqpim'); ?></option>
											
											<option value="BRL" <?php if($code == 'BRL') { echo 'selected="selected"'; } ?>><?php _e('Brazilian Real (BRL)', 'cqpim'); ?></option>
											
											<option value="CAD" <?php if($code == 'CAD') { echo 'selected="selected"'; } ?>><?php _e('Canadian Dollar (CAD)', 'cqpim'); ?></option>
											
											<option value="CZK" <?php if($code == 'CZK') { echo 'selected="selected"'; } ?>><?php _e('Czech Koruna (CZK)', 'cqpim'); ?></option>
											
											<option value="DKK" <?php if($code == 'DKK') { echo 'selected="selected"'; } ?>><?php _e('Danish Krone (DKK)', 'cqpim'); ?></option>
											
											<option value="EUR" <?php if($code == 'EUR') { echo 'selected="selected"'; } ?>><?php _e('Euro (EUR)', 'cqpim'); ?></option>
											
											<option value="HKD" <?php if($code == 'HKD') { echo 'selected="selected"'; } ?>><?php _e('Hong Kong Dollar (HKD)', 'cqpim'); ?></option>
											
											<option value="ILS" <?php if($code == 'ILS') { echo 'selected="selected"'; } ?>><?php _e('Israeli New Sheqel (ILS)', 'cqpim'); ?></option>
											
											<option value="MXN" <?php if($code == 'MXN') { echo 'selected="selected"'; } ?>><?php _e('Mexican Peso (MXN)', 'cqpim'); ?></option>
											
											<option value="NOK" <?php if($code == 'NOK') { echo 'selected="selected"'; } ?>><?php _e('Norwegian Krone (NOK)', 'cqpim'); ?></option>
											
											<option value="NZD" <?php if($code == 'NZD') { echo 'selected="selected"'; } ?>><?php _e('New Zealand Dollar (NZD)', 'cqpim'); ?></option>

											<option value="PHP" <?php if($code == 'PHP') { echo 'selected="selected"'; } ?>><?php _e('Philippine Peso (PHP)', 'cqpim'); ?></option>

											<option value="PLN" <?php if($code == 'PLN') { echo 'selected="selected"'; } ?>><?php _e('Polish Zloty (PLN)', 'cqpim'); ?></option>

											<option value="GBP" <?php if($code == 'GBP') { echo 'selected="selected"'; } ?>><?php _e('Pound Sterling (GBP)', 'cqpim'); ?></option>

											<option value="RUB" <?php if($code == 'RUB') { echo 'selected="selected"'; } ?>><?php _e('Russian Ruble (RUB)', 'cqpim'); ?></option>

											<option value="SGD" <?php if($code == 'SGD') { echo 'selected="selected"'; } ?>><?php _e('Singapore Dollar (SGD)', 'cqpim'); ?></option>

											<option value="SEK" <?php if($code == 'SEK') { echo 'selected="selected"'; } ?>><?php _e('Swedish Krona (SEK)', 'cqpim'); ?></option>

											<option value="CHF" <?php if($code == 'CHF') { echo 'selected="selected"'; } ?>><?php _e('Swiss Franc (CHF)', 'cqpim'); ?></option>

											<option value="THB" <?php if($code == 'THB') { echo 'selected="selected"'; } ?>><?php _e('Thai Baht (THB)', 'cqpim'); ?></option>

											<option value="USD" <?php if($code == 'USD') { echo 'selected="selected"'; } ?>><?php _e('U.S. Dollar (USD)', 'cqpim'); ?></option>										
										
                                        </select>									
									
									</td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Account Name:', 'cqpim'); _e('Account Name:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_bank_name" name="company_bank_name" value="<?php echo get_option('company_bank_name'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Account Number:', 'cqpim'); _e('Account Number:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_bank_ac" name="company_bank_ac" value="<?php echo get_option('company_bank_ac'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Sort Code:', 'cqpim'); _e('Sort Code:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_bank_sc" name="company_bank_sc" value="<?php echo get_option('company_bank_sc'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('IBAN:', 'cqpim'); _e('IBAN:', 'cqpim'); ?></td>
									
									<td><input type="text" id="company_bank_iban" name="company_bank_iban" value="<?php echo get_option('company_bank_iban'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Invoice Terms:', 'cqpim'); _e('Invoice Terms:', 'cqpim'); ?></td>
									
									<td>
									
									<?php $terms = get_option('company_invoice_terms'); ?>
									
									<select id="company_invoice_terms" name="company_invoice_terms">
									
										<option value="1" <?php if($terms == 1) { echo 'selected'; } ?>><?php $text = __('Due on Receipt', 'cqpim'); _e('Due on Receipt', 'cqpim'); ?></option>
									
										<option value="7" <?php if($terms == 7) { echo 'selected'; } ?>>7 <?php $text = __('days', 'cqpim'); _e('days', 'cqpim'); ?></option>
										
										<option value="14" <?php if($terms == 14) { echo 'selected'; } ?>>14 <?php $text = __('days', 'cqpim'); _e('days', 'cqpim'); ?></option>
										
										<option value="28" <?php if($terms == 28) { echo 'selected'; } ?>>28 <?php $text = __('days', 'cqpim'); _e('days', 'cqpim'); ?></option>
										
										<option value="30" <?php if($terms == 30) { echo 'selected'; } ?>>30 <?php $text = __('days', 'cqpim'); _e('days', 'cqpim'); ?></option>
										
										<option value="60" <?php if($terms == 60) { echo 'selected'; } ?>>60 <?php $text = __('days', 'cqpim'); _e('days', 'cqpim'); ?></option>
										
										<option value="90" <?php if($terms == 90) { echo 'selected'; } ?>>90 <?php $text = __('days', 'cqpim'); _e('days', 'cqpim'); ?></option>
									
									</select>
									
									</td>
									
								</tr>
								
								<tr>
								
									<td colspan="2"><h3><?php $text = __('Sales Tax', 'cqpim'); _e('Sales Tax', 'cqpim'); ?></h3></td>
									
								</tr>
								
								<tr>
								
									<td colspan="2"><?php $text = __('These settings apply to sales tax, such as VAT. If you do not charge sales tax, then leave these fields blank.', 'cqpim'); _e('These settings apply to sales tax, such as VAT. If you do not charge sales tax, then leave these fields blank.', 'cqpim'); ?></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Tax Percentage (eg. 20):', 'cqpim'); _e('Tax Percentage (eg. 20):', 'cqpim'); ?></td>
									
									<td><input type="text" name="sales_tax_rate" id="sales_tax_rate" value="<?php echo get_option('sales_tax_rate'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Tax Name (eg. VAT):', 'cqpim'); _e('Tax Name (eg. VAT):', 'cqpim'); ?></td>
									
									<td><input type="text" name="sales_tax_name" id="sales_tax_name" value="<?php echo get_option('sales_tax_name'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Tax Reg Number:', 'cqpim'); _e('Tax Reg Number:', 'cqpim'); ?></td>
									
									<td><input type="text" name="sales_tax_reg" id="sales_tax_reg" value="<?php echo get_option('sales_tax_reg'); ?>" /></td>
									
								</tr>
								
								<?php $v3 = get_option('cqpim_3'); ?>
								
								<?php if($v3 == true) { ?>
								
								<tr>
								
									<td colspan="2"><h3><?php $text = __('Secondary Sales Tax', 'cqpim'); _e('Secondary Sales Tax', 'cqpim'); ?></h3></td>
									
								</tr>
								
								<tr>
								
									<td colspan="2"><?php $text = __('These settings apply to a secondary sales tax. If you do not charge a secondary sales tax, then leave these fields blank.', 'cqpim'); _e('These settings apply to a secondary sales tax. If you do not charge a secondary sales tax, then leave these fields blank.', 'cqpim'); ?></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Secondary  Tax Percentage (eg. 20):', 'cqpim'); _e('Secondary  Tax Percentage (eg. 20):', 'cqpim'); ?></td>
									
									<td><input type="text" name="secondary_sales_tax_rate" id="sales_tax_rate" value="<?php echo get_option('secondary_sales_tax_rate'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Secondary  Tax Name (eg. VAT):', 'cqpim'); _e('Secondary  Tax Name (eg. VAT):', 'cqpim'); ?></td>
									
									<td><input type="text" name="secondary_sales_tax_name" id="sales_tax_name" value="<?php echo get_option('secondary_sales_tax_name'); ?>" /></td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Secondary  Tax Reg Number:', 'cqpim'); _e('Secondary  Tax Reg Number:', 'cqpim'); ?></td>
									
									<td><input type="text" name="secondary_sales_tax_reg" id="sales_tax_reg" value="<?php echo get_option('secondary_sales_tax_reg'); ?>" /></td>
									
								</tr>								
								
								<?php } ?>
							
							</table>
												
							<div class="clear"></div>

							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>					
						
						</div>
						
						<div id="tabs-9">
						
							<h3><?php _e('Client Dashboard', 'cqpim'); ?></h3>
							
							<p><?php _e('CQPIM has a full width theme included that is used on the Client Dashboard. If you prefer, you can have the Client Dashboard load inside your active WordPress theme instead.', 'cqpim'); ?></p>
						
							<?php $type = get_option('client_dashboard_type'); 
							
								$theme = wp_get_theme();
							
							?>
							
							<select id="client_dashboard_type" name="client_dashboard_type">
								
									<option value="inc" <?php if($type == 'inc') { echo 'selected'; } ?>><?php _e('CQPIM Client Dashboard Theme', 'cqpim'); ?></option>

									<option value="active" <?php if($type != 'inc') { echo 'selected'; } ?>><?php _e('Current Active WP Theme', 'cqpim'); ?> (<?php echo $theme->name; ?>)</option>									
							
							</select>

							<div class="clear"></div>
							
							<br />
							
							<p><strong><?php _e('Client Dashboard Logout URL', 'cqpim'); ?></strong></p>
							
							<p><?php _e('This must be on the same domain as CQPIM, otherwise this setting will not work', 'cqpim'); ?></p>
							
							<?php $logout = get_option('cqpim_logout_url'); ?>
							
							<input type="text" name="cqpim_logout_url" id="cqpim_logout_url" value="<?php echo $logout; ?>" />
							
						
							<div class="clear"></div>
							
							<br /><br />
							
							<?php $client_settings = get_option('allow_client_settings'); ?>
							
							<input type="checkbox" name="allow_client_settings" id="allow_client_settings" value="1" <?php checked($client_settings, 1, true); ?>/> <?php $text = __('Allow Clients to update their details from their dashboard.', 'cqpim'); _e('Allow Clients to update their details from their dashboard', 'cqpim'); ?>
							
							<br /><br />
							
							<div class="clear"></div>
							
							<?php $client_settings = get_option('allow_client_users'); ?>
							
							<input type="checkbox" name="allow_client_users" id="allow_client_users" value="1" <?php checked($client_settings, 1, true); ?>/> <?php $text = __('Allow Clients to grant access to their dashboard & create users for other team members/colleagues.', 'cqpim'); _e('Allow Clients to grant access to their dashboard & create users for other team members/colleagues.', 'cqpim'); ?>
							
							<br /><br />
							
							<div class="clear"></div>
							
							<h3><?php $text = __('Dashboard Password Reset', 'cqpim'); _e('Dashboard Password Reset', 'cqpim'); ?></h3>
							
							<p><?php $text = __('This email is sent when a client requests a password reset.', 'cqpim'); _e('This email is sent when a client requests a password reset.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Password Reset Email Subject', 'cqpim'); _e('Password Reset Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_password_reset_subject" name="client_password_reset_subject" value="<?php echo get_option('client_password_reset_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('Password Reset Email Content', 'cqpim'); _e('Password Reset Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="client_password_reset_content" name="client_password_reset_content"><?php echo get_option('client_password_reset_content'); ?></textarea>
							
							<h3><?php _e('Dashboard Logo (CQPIM Dash Only)', 'cqpim'); ?></h3>
								
								<?php 
								
								$logo = get_option('cqpim_dash_logo'); 
								
								if($logo) { ?>
								
								
									<img style="max-width:400px; margin:20px 0; background:#ececec" src="<?php echo $logo['cqpim_dash_logo']; ?>" />
									
									<br />
									
									<button class="remove_logo cqpim_button mt-20 op bg-red font-white rounded_2" data-type="cqpim_dash_logo"><?php _e('Remove', 'cqpim'); ?></button>
									
									<br /><br />
									
								<?php } ?>
									
								<input type="file" name="cqpim_dash_logo" />
									
							<br /><br />
							
							<div class="clear"></div>	

							<h3><?php _e('Dashboard Background (CQPIM Dash Only)', 'cqpim'); ?></h3>
								
								<?php 
								
								$logo = get_option('cqpim_dash_bg'); 
								
								if($logo) { ?>
								
								
									<img style="max-width:600px; margin:20px 0; background:#ececec" src="<?php echo $logo['cqpim_dash_bg']; ?>" />
									
									<br />
									
									<button class="remove_logo cqpim_button mt-20 op bg-red font-white rounded_2" data-type="cqpim_dash_bg"><?php _e('Remove', 'cqpim'); ?></button>
									
									<br /><br />
									
								<?php } ?>
									
								<input type="file" name="cqpim_dash_bg" />
									
							<br /><br />
							
							<div class="clear"></div>	
							
							<h3><?php _e('Built-In Client Dashboard Custom CSS', 'cqpim'); ?></h3>
							
							<textarea style="width:100%; height:500px" name="cqpim_dash_css" id="cqpim_dash_css"><?php echo get_option('cqpim_dash_css'); ?></textarea>
							
							<br /><br />

							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>						
						
						</div>

						<div id="tabs-2">						
							
							<h3><?php $text = __('Client Settings', 'cqpim'); _e('Client Settings', 'cqpim'); ?></h3>
							
							<p><?php $text = __('When a Client is created, a user account is also created to allow the new client to log in to their dashboard. On this page you can choose whether or not to send an automated welcome email when the client\'s account has been added.', 'cqpim'); _e('When a Client is created, a user account is also created to allow the new client to log in to their dashboard. On this page you can choose whether or not to send an automated welcome email when the client\'s account has been added.', 'cqpim'); ?></p>

							<p><strong><?php $text = __('Welcome Email', 'cqpim'); _e('Welcome Email', 'cqpim'); ?></strong></p>
							
							<?php $auto_welcome = get_option('auto_welcome'); ?>
							
							<input type="checkbox" name="auto_welcome" id="auto_welcome" value="1" <?php checked($auto_welcome, 1, true); ?>/> <?php $text = __('Send the client a welcome email with login details to their dashboard (Recommended).', 'cqpim'); _e('Send the client a welcome email with login details to their dashboard (Recommended).', 'cqpim'); ?>
							
							<br /><br />
							
							<p><strong><?php $text = __('Welcome Email Subject', 'cqpim'); _e('Welcome Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="auto_welcome_subject" name="auto_welcome_subject" value="<?php echo get_option('auto_welcome_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('Welcome Email Content', 'cqpim'); _e('Welcome Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="auto_welcome_content" name="auto_welcome_content"><?php echo get_option('auto_welcome_content'); ?></textarea>
							
							<h3><?php $text = __('Password Reset', 'cqpim'); _e('Password Reset', 'cqpim'); ?></h3>
							
							<p><strong><?php $text = __('Password Reset Email', 'cqpim'); _e('Password Reset Email', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('This email is optionally sent to the client when an admin resets their password.', 'cqpim'); _e('This email is optionally sent to the client when an admin resets their password.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Password Reset Email Subject', 'cqpim'); _e('Password Reset Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="password_reset_subject" name="password_reset_subject" value="<?php echo get_option('password_reset_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('Password Reset Email Content', 'cqpim'); _e('Password Reset Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="password_reset_content" name="password_reset_content"><?php echo get_option('password_reset_content'); ?></textarea>

							<h3><?php $text = __('New Contact Settings', 'cqpim'); _e('New Contact Settings', 'cqpim'); ?></h3>
							
							<p><strong><?php $text = __('New Contact Email Subject', 'cqpim'); _e('New Contact Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="added_contact_subject" name="added_contact_subject" value="<?php echo get_option('added_contact_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('New Contact Email Content', 'cqpim'); _e('New Contact Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="added_contact_content" name="added_contact_content"><?php echo get_option('added_contact_content'); ?></textarea>

							
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>
						
						</div>
						
						<div id="tabs-3">
				
							<h3><?php $text = __('Header & Footer', 'cqpim'); _e('Header & Footer', 'cqpim'); ?></h3>
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Default Quote Header Text', 'cqpim'); _e('Default Quote Header Text', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('The contents of this field will appear at the top of quotes/estimates. It can also be overridden on an individual basis when creating quotes/estimates.', 'cqpim'); _e('The contents of this field will appear at the top of quotes/estimates. It can also be overridden on an individual basis when creating quotes/estimates.', 'cqpim'); ?></p>
						
							<?php
							
							$content = get_option( 'quote_header' );
							
							?>	

							<textarea style="width:100%; height:200px" id="quote_header" name="quote_header"><?php echo $content; ?></textarea>
						
							<div class="clear"></div>
							
							</div>
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Default Quote Footer Text', 'cqpim'); _e('Default Quote Footer Text', 'cqpim'); ?></strong></p>

							<p><?php $text = __('The contents of this field will appear at the bottom of quotes/estimates, just before the quote/estimate acceptance text. It can also be overridden on an individual basis when creating quotes/estimates.', 'cqpim'); _e('The contents of this field will appear at the bottom of quotes/estimates, just before the quote/estimate acceptance text. It can also be overridden on an individual basis when creating quotes/estimates.', 'cqpim'); ?></p>
							
							<?php
							
							$content = get_option( 'quote_footer' );
							
							?>	
							
							<textarea style="width:100%; height:200px" id="quote_footer" name="quote_footer"><?php echo $content; ?></textarea>
												
							</div>

							<div class="clear"></div>
							
							<h3><?php $text = __('Default Quote Acceptance Text', 'cqpim'); _e('Default Quote Acceptance Text', 'cqpim'); ?></h3>
							
							<p><strong><?php $text = __('Quote Acceptance Text', 'cqpim'); _e('Quote Acceptance Text', 'cqpim'); ?></strong></p>

							<p><?php $text = __('The contents of this field will appear alongside the form that clients will use to accept quotes/estimates. It should include instructions on how to proceed.', 'cqpim'); _e('The contents of this field will appear alongside the form that clients will use to accept quotes/estimates. It should include instructions on how to proceed.', 'cqpim'); ?></p>
							
							<div class="settings-box">
							
							<?php
							
							$content   = get_option( 'quote_acceptance_text' );
							
							?>	

							<textarea style="width:100%; height:200px" id="quote_acceptance_text" name="quote_acceptance_text"><?php echo $content; ?></textarea>
							

							</div>
						
							<div class="clear"></div>
							
							<h3><?php $text = __('Quote Emails', 'cqpim'); _e('Quote Emails', 'cqpim'); ?></h3>
							
							<div class="settings-box">
							
							<p><?php $text = __('When a quote is sent to a client by email, these fields will be used for the subject and content of the email.', 'cqpim'); _e('When a quote is sent to a client by email, these fields will be used for the subject and content of the email.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Quote Default Email Subject', 'cqpim'); _e('Quote Default Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" name="quote_email_subject" id="quote_email_subject" value="<?php echo get_option('quote_email_subject'); ?>"/>
							
							<p><strong><?php $text = __('Quote Default Email Content', 'cqpim'); _e('Quote Default Email Content', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'quote_default_email' );
							
							?>
							
							<textarea style="width:100%; height:200px" id="quote_default_email" name="quote_default_email"><?php echo $content; ?></textarea>
								
							</div>

							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>
						
						</div>
						
						<div id="tabs-4">
																		
							<h3><?php $text = __('Terms & Conditions', 'cqpim'); _e('Terms & Conditions', 'cqpim'); ?></h3>
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Default Contract Terms & Conditions', 'cqpim'); _e('Default Contract Terms & Conditions', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('These Terms & Conditions will appear on the contract that is sent to your clients. You can add templates by clicking Terms Templates in the CQPIM menu.', 'cqpim'); _e('These Terms & Conditions will appear on the contract that is sent to your clients. You can add templates by clicking Terms Templates in the CQPIM menu.', 'cqpim'); ?></p>
							
							<?php
							
							$content = get_option( 'default_contract_text' ); 
							
							?>

							<select name="default_contract_text">
							
								<?php 
								
								$args = array(
								
									'post_type' => 'cqpim_terms',
									
									'posts_per_page' => -1,
									
									'post_status' => 'private'
									
								);
								
								$terms = get_posts($args);
								
								foreach($terms as $term) {
								
									if($term->ID == $content) {
									
										$selected = 'selected="selected"';
										
									} else {
									
										$selected = '';
										
									}
									
									echo '<option value="' . $term->ID . '" ' . $selected . '>' . $term->post_title . '</option>';
								
								}
								
								?>
								
							
							</select>
							
							</div>
							
							<h3><?php $text = __('Contract Acceptance Text', 'cqpim'); _e('Contract Acceptance Text', 'cqpim'); ?></h3>
							
							<p><?php $text = __('This will appear on the contract alongside the form that clients will use to e-sign. It should include instructions on how to proceed.', 'cqpim'); _e('This will appear on the contract alongside the form that clients will use to e-sign. It should include instructions on how to proceed.', 'cqpim'); ?></p>
							
							<div class="settings-box">
							
							<?php
							
							$content   = get_option( 'contract_acceptance_text' );
							
							?>

							<textarea style="width:100%; height:200px" id="contract_acceptance_text" name="contract_acceptance_text"><?php echo $content; ?></textarea>
							
							</div>
						
							<div class="clear"></div>
							
							<h3><?php $text = __('Contract & Update Emails', 'cqpim'); _e('Contract & Update Emails', 'cqpim'); ?></h3>
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Client Contract Email', 'cqpim'); _e('Client Contract Email', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('This email is sent out when a project has been created and should contain information on what a client needs to do in order to sign their contract.', 'cqpim'); _e('This email is sent out when a project has been created and should contain information on what a client needs to do in order to sign their contract.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Client Contract Email Subject', 'cqpim'); _e('Client Contract Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_contract_subject" name="client_contract_subject" value="<?php echo get_option('client_contract_subject'); ?>" />
							
							<p><strong><?php $text = __('Client Contract Email Content', 'cqpim'); _e('Client Contract Email Content', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'client_contract_email' );
							
							?>

							<textarea style="width:100%; height:200px" id="client_contract_email" name="client_contract_email"><?php echo $content; ?></textarea>
							
							</div>
							
							<div class="clear"></div>
							
							<br />
							
							<hr />
							
							<h3><?php $text = __('New Message Emails', 'cqpim'); _e('New Message Emails', 'cqpim'); ?></h3>
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Client Message Email', 'cqpim'); _e('Client Message Email', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('This email is sent to your client when you send a new project message.', 'cqpim'); _e('This email is sent to your client when you send a new project message.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Client Message Email Subject', 'cqpim'); _e('Client Message Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_message_subject" name="client_message_subject" value="<?php echo get_option('client_message_subject'); ?>" />
							
							<p><strong><?php $text = __('Client Message Email Content', 'cqpim'); _e('Client Message Email Content', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'client_message_email' );
							
							?>

							<textarea style="width:100%; height:200px" id="client_message_email" name="client_message_email"><?php echo $content; ?></textarea>
							
							</div>
							
							<div class="clear"></div>
							
							<br /><hr />
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Company Message Email', 'cqpim'); _e('Company Message Email', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('This email is sent to you when a client has sent a new project message.', 'cqpim'); _e('This email is sent to you when a client has sent a new project message.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Company Message Email Subject', 'cqpim'); _e('Company Message Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="company_message_subject" name="company_message_subject" value="<?php echo get_option('company_message_subject'); ?>" />
							
							<p><strong><?php $text = __('Company Message Email Content', 'cqpim'); _e('Company Message Email Content', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'company_message_email' );
							
							?>

							<textarea style="width:100%; height:200px" id="company_message_email" name="company_message_email"><?php echo $content; ?></textarea>
							
							</div>
							
							<div class="clear"></div>

							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>					
						
						</div>
						
						<div id="tabs-5">
						
							<h3><?php $text = __('Invoice Template', 'cqpim'); _e('Invoice Template', 'cqpim'); ?></h3>
							
							<?php 
							
							$checked = get_option('cqpim_invoice_template'); 
							
							if(empty($checked)) {
								
								update_option('cqpim_invoice_template', 1);
								
							}
							
							?>
							
							<table>
								
								<tr><td><input type="radio" name="cqpim_invoice_template" value="1" <?php checked($checked, 1, true); ?> /></td><td><?php _e('Default', 'cqpim'); ?></td></tr>
								
								<tr><td colspan="2"></td></tr>
								
								<tr><td><input type="radio" name="cqpim_invoice_template" value="2" <?php checked($checked, 2, true); ?> /></td><td><?php _e('Clean', 'cqpim'); ?> | <?php _e('Main Colour (HEX, eg. #333333)', 'cqpim'); ?>: <input type="text" style="width:100px" name="cqpim_clean_main_colour" value="<?php echo get_option('cqpim_clean_main_colour'); ?>" /></td></tr>
							
							</table>
							
							<h3><?php $text = __('Invoice Logo', 'cqpim'); _e('Invoice Logo', 'cqpim'); ?></h3>
							
							<p><?php _e('By default, the invoice will use the global company logo. You can override the invoice logo here if you wish', 'cqpim'); ?></p>
								
							<?php 
							
							$logo = get_option('cqpim_invoice_logo'); 
							
							if($logo) { ?>
								
								<img style="max-width:400px; margin:20px 0; background:#ececec" src="<?php echo $logo['cqpim_invoice_logo']; ?>" />
								
								<br />
								
								<button class="remove_logo cqpim_button mt-20 op bg-red font-white rounded_2" data-type="cqpim_invoice_logo"><?php _e('Remove', 'cqpim'); ?></button>
								
								<br /><br />
							
							<?php } ?>	
							
							<p><input type="file" name="cqpim_invoice_logo" /></p>
		
							<h3><?php $text = __('PDF Invoice Email Attachments', 'cqpim'); _e('PDF Invoice Email Attachments', 'cqpim'); ?></h3>
							
							<?php $client_invoice_email_attach = get_option('client_invoice_email_attach'); ?>
							
							<p><strong><?php $text = __('IMPORTANT:', 'cqpim'); _e('IMPORTANT:', 'cqpim'); ?></strong> <?php $text = __('PDF Invoice attachments require the PHP cURL Extension. If you experience blank PDFs then check that you have this installed and your host is not blocking the requests.', 'cqpim'); _e('PDF Invoice attachments require the PHP cURL Extension. If you experience blank PDFs then check that you have this installed and your host is not blocking the requests.', 'cqpim'); ?></p>
							
							<input type="checkbox" name="client_invoice_email_attach" id="client_invoice_email_attach" value="1" <?php checked($client_invoice_email_attach, 1, true); ?>/> <?php $text = __('Attach a PDF Invoice to Client Emails	', 'cqpim'); _e('Attach a PDF Invoice to Client Emails	', 'cqpim'); ?>					
							
							<h3><?php $text = __('Partial Invoice Payments', 'cqpim'); _e('Partial Invoice Payments', 'cqpim'); ?></h3>
							
							<?php $client_invoice_allow_partial = get_option('client_invoice_allow_partial'); ?>
							
							<input type="checkbox" name="client_invoice_allow_partial" id="client_invoice_allow_partial" value="1" <?php checked($client_invoice_allow_partial, 1, true); ?>/> <?php _e('Allow partial invoice payments (This is a global setting and can be overridden on a per invoice basis. Deposit invoices do not allow partial payments.)', 'cqpim'); ?>
							
							<h3><?php $text = __('Invoice Reminder Settings', 'cqpim'); _e('Invoice Reminder Settings', 'cqpim'); ?></h3>
							
							<p><strong><?php _e('IMPORTANT:', 'cqpim'); ?> </strong> <?php $text = __('The following settings require the use of WP Cron, which some hosts block access to. Please check that you have access to wp cron, otherwise these settings may not work properly.', 'cqpim'); _e('The following settings require the use of WP Cron, which some hosts block access to. Please check that you have access to wp cron, otherwise these settings may not work properly.', 'cqpim'); ?></p>

							<table>
							
								<tr>
								
									<td class="title"><?php $text = __('Reminder email after invoice sent:', 'cqpim'); _e('Reminder email after invoice sent:', 'cqpim'); ?></td>
									
									<td>
									
									<?php $terms = get_option('client_invoice_after_send_remind_days');
									
									$days = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20); ?>
									
									<select id="client_invoice_after_send_remind_days" name="client_invoice_after_send_remind_days">
									
										<option value="" <?php if(!$terms) { echo 'selected'; } ?>><?php _e('Choose...', 'cqpim') ?></option>
										
										<?php foreach($days as $day) { ?>
										
											<option value="<?php echo $day; ?>" <?php if($terms == $day) { echo 'selected'; } ?>><?php echo $day; ?> day<?php if($day != 1) { echo 's'; } ?></option>
																			
										<?php } ?>
									
									</select>
									
									</td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Reminder email before due date:', 'cqpim'); _e('Reminder email before due date:', 'cqpim'); ?></td>
									
									<td>
									
									<?php $terms = get_option('client_invoice_before_terms_remind_days');
									
									$days = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20); ?>
									
									<select id="client_invoice_before_terms_remind_days" name="client_invoice_before_terms_remind_days">
									
										<option value="" <?php if(!$terms) { echo 'selected'; } ?>><?php _e('Choose...', 'cqpim') ?></option>
										
										<?php foreach($days as $day) { ?>
										
											<option value="<?php echo $day; ?>" <?php if($terms == $day) { echo 'selected'; } ?>><?php echo $day; ?> day<?php if($day != 1) { echo 's'; } ?></option>
																			
										<?php } ?>
									
									</select>
									
									</td>
									
								</tr>
								
								<tr>
								
									<td class="title"><?php $text = __('Overdue email after due date:', 'cqpim'); _e('Overdue email after due date:', 'cqpim'); ?></td>
									
									<td>
									
									<?php $terms = get_option('client_invoice_after_terms_remind_days');
									
									$days = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20); ?>
									
									<select id="client_invoice_after_terms_remind_days" name="client_invoice_after_terms_remind_days">
									
										<option value="" <?php if(!$terms) { echo 'selected'; } ?>><?php _e('Choose...', 'cqpim') ?></option>
										
										<?php foreach($days as $day) { ?>
										
											<option value="<?php echo $day; ?>" <?php if($terms == $day) { echo 'selected'; } ?>><?php echo $day; ?> day<?php if($day != 1) { echo 's'; } ?></option>
																			
										<?php } ?>
									
									</select>
									
									</td>
									
								</tr>
							
							</table>
							
							<br />
							
							<?php $client_invoice_high_priority = get_option('client_invoice_high_priority'); ?>
							
							<input type="checkbox" name="client_invoice_high_priority" id="client_invoice_high_priority" value="1" <?php checked($client_invoice_high_priority, 1, true); ?>/> Mark invoice reminder/overdue emails as high priority?
							
							<h3><?php $text = __('Payment Gateways', 'cqpim'); _e('Payment Gateways', 'cqpim'); ?></h3>
							
							<p><strong><?php $text = __('Paypal', 'cqpim'); _e('Paypal', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('To allow clients to pay invoices via Paypal, enter your Paypal email address below.', 'cqpim'); _e('To allow clients to pay invoices via Paypal, enter your Paypal email address below.', 'cqpim'); ?></p>

							<input type="text" name="client_invoice_paypal_address" id="client_invoice_paypal_address" value="<?php echo get_option('client_invoice_paypal_address'); ?>" />
								
							<p><strong><?php $text = __('Stripe', 'cqpim'); _e('Stripe', 'cqpim'); ?></strong></p>

							<p><?php $text = __('To allow clients to pay invoices via Stripe, enter your Stripe Publishable Key below.', 'cqpim'); _e('To allow clients to pay invoices via Stripe, enter your Stripe Publishable Key below.', 'cqpim'); ?></p>

							<input type="text" name="client_invoice_stripe_key" id="client_invoice_stripe_key" value="<?php echo get_option('client_invoice_stripe_key'); ?>" />
							
							<p><?php _e('Stripe Secret Key', 'cqpim'); ?></p>
							
							<input type="text" name="client_invoice_stripe_secret" id="client_invoice_stripe_secret" value="<?php echo get_option('client_invoice_stripe_secret'); ?>" />
							
							<?php
							
								if(function_exists('cqpim_twocheck_return_settings')) {
									
									echo cqpim_twocheck_return_settings();
									
								}
							
							?>
																		
							<h3><?php $text = __('Invoice Footer', 'cqpim'); _e('Invoice Footer', 'cqpim'); ?></h3>
							
							<p><?php $text = __('This text will appear at the bottom of invoices, so should contain instructions for payment etc.', 'cqpim'); _e('This text will appear at the bottom of invoices, so should contain instructions for payment etc.', 'cqpim'); ?> <a href="#invoice_footer_example" class="settings-example cqpim_button cqpim_small_button font-dark-blue border-dark-blue op colorbox"><?php _e('View Example Content', 'cqpim'); ?></a></p>
							
							<div class="settings-box">
							
							
							<?php
							
							$content   = get_option( 'client_invoice_footer' );
							
							?>

							<textarea style="width:100%; height:200px" id="client_invoice_footer" name="client_invoice_footer"><?php echo $content; ?></textarea>
							
							</div>
							
							<h3><?php _e('Emails', 'cqpim'); ?></h3>
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Client Invoice Email Subject', 'cqpim'); _e('Client Invoice Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_invoice_subject" name="client_invoice_subject" value="<?php echo get_option('client_invoice_subject'); ?>" />
							
							<p><strong><?php $text = __('Client Invoice Email Content', 'cqpim'); _e('Client Invoice Email Content', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'client_invoice_email' ); 
							
							?>

							<textarea style="width:100%; height:200px" id="client_invoice_email" name="client_invoice_email"><?php echo $content; ?></textarea>
							
							</div>
							
							<br />
							
							<hr />
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Client Deposit Invoice Email Subject', 'cqpim'); _e('Client Deposit Invoice Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_deposit_invoice_subject" name="client_deposit_invoice_subject" value="<?php echo get_option('client_deposit_invoice_subject'); ?>" />

							
							<p><strong><?php $text = __('Client Deposit Invoice Email', 'cqpim'); _e('Client Deposit Invoice Email', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'client_deposit_invoice_email' );
							
							?>
							
							<textarea style="width:100%; height:200px" id="client_deposit_invoice_email" name="client_deposit_invoice_email"><?php echo $content; ?></textarea>
							
							</div>
							
							<br />
							
							<hr />
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Client Invoice Reminder Email Subject', 'cqpim'); _e('Client Invoice Reminder Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_invoice_reminder_subject" name="client_invoice_reminder_subject" value="<?php echo get_option('client_invoice_reminder_subject'); ?>" />
							
							<p><strong><?php $text = __('Client Invoice Reminder Email', 'cqpim'); _e('Client Invoice Reminder Email', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'client_invoice_reminder_email' );
							
							?>

							<textarea style="width:100%; height:200px" id="client_invoice_reminder_email" name="client_invoice_reminder_email"><?php echo $content; ?></textarea>
							
							</div>
							
							<br />
							
							<hr />
							
							<div class="settings-box">
							
							<p><strong><?php $text = __('Client Invoice Overdue Email Subject', 'cqpim'); _e('Client Invoice Overdue Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_invoice_overdue_subject" name="client_invoice_overdue_subject" value="<?php echo get_option('client_invoice_overdue_subject'); ?>" />
							
							<p><strong><?php $text = __('Client Invoice Overdue Email', 'cqpim'); _e('Client Invoice Overdue Email', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'client_invoice_overdue_email' ); 
							
							?>

							<textarea style="width:100%; height:200px" id="client_invoice_overdue_email" name="client_invoice_overdue_email"><?php echo $content; ?></textarea>
							
							</div>
							
							<br />
							
							<hr />
							
							<div class="settings-box">
							
							<p><strong><?php _e('Client Payment Notication Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_invoice_receipt_subject" name="client_invoice_receipt_subject" value="<?php echo get_option('client_invoice_receipt_subject'); ?>" />
							
							<p><strong><?php _e('Client Payment Notication Email', 'cqpim'); ?></strong></p>
							
							<?php
							
							$content   = get_option( 'client_invoice_receipt_email' ); 
							
							?>

							<textarea style="width:100%; height:200px" id="client_invoice_receipt_email" name="client_invoice_receipt_email"><?php echo $content; ?></textarea>
							
							</div>

							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>

						</div>
						
						<div id="tabs-6">
						
							<h3><?php $text = __('New Account', 'cqpim'); _e('New Account', 'cqpim'); ?></h3>
							
							<p><strong><?php _e('New Account Email', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('This email is sent out when an admin creates a new team member.', 'cqpim'); _e('This email is sent out when an admin creates a new team member.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('New Account Subject', 'cqpim'); _e('New Account Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="team_account_subject" name="team_account_subject" value="<?php echo get_option('team_account_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('New Account Email Content', 'cqpim'); _e('New Account Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="team_account_email" name="team_account_email"><?php echo get_option('team_account_email'); ?></textarea>

						
							<h3><?php $text = __('Password Reset', 'cqpim'); _e('Password Reset', 'cqpim'); ?></h3>
							
							<p><strong><?php $text = __('Password Reset Email', 'cqpim'); _e('Password Reset Email', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('This email is optionally sent to the team member when an admin resets their password.', 'cqpim'); _e('This email is optionally sent to the team member when an admin resets their password.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Password Reset Subject', 'cqpim'); _e('Password Reset Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="team_reset_subject" name="team_reset_subject" value="<?php echo get_option('team_reset_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('Password Reset Email Content', 'cqpim'); _e('Password Reset Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="team_reset_email" name="team_reset_email"><?php echo get_option('team_reset_email'); ?></textarea>

							<h3><?php $text = __('New Project', 'cqpim'); _e('New Project', 'cqpim'); ?></h3>
							
							<p><strong><?php $text = __('New Project Email', 'cqpim'); _e('New Project Email', 'cqpim'); ?></strong></p>
							
							<p><?php $text = __('This email is sent to a team member when an admin adds them to a project.', 'cqpim'); _e('This email is sent to a team member when an admin adds them to a project.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('New Project Email Subject', 'cqpim'); _e('New Project Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="team_project_subject" name="team_project_subject" value="<?php echo get_option('team_project_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('New Project Email Content', 'cqpim'); _e('New Project Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="team_project_email" name="team_project_email"><?php echo get_option('team_project_email'); ?></textarea>
							
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>

						</div>
						
						<div id="tabs-10">
						
							<h3><?php $text = __('Task Update Email', 'cqpim'); _e('Task Update Email', 'cqpim'); ?></h3>
							
							<p><?php $text = __('This email is sent to the client and all watchers when a task is updated.', 'cqpim'); _e('This email is sent to the client and all watchers when a task is updated.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Task Update Email Subject', 'cqpim'); _e('Task Update Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="team_assignment_subject" name="team_assignment_subject" value="<?php echo get_option('team_assignment_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('Task Update Email Content', 'cqpim'); _e('Task Update Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="team_assignment_email" name="team_assignment_email"><?php echo get_option('team_assignment_email'); ?></textarea>
						
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>						
						
						</div>
						
						<div id="tabs-7">
						
							<h3><?php $text = __('New Ticket Email', 'cqpim'); _e('New Ticket Email', 'cqpim'); ?></h3>
							
							<p><?php $text = __('This email is sent to your Support email address when a client raises a new ticket.', 'cqpim'); _e('This email is sent to your Support email address when a client raises a new ticket.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('New Ticket Email Subject', 'cqpim'); _e('New Ticket Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_create_ticket_subject" name="client_create_ticket_subject" value="<?php echo get_option('client_create_ticket_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('New Ticket Email Content', 'cqpim'); _e('New Ticket Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="client_create_ticket_email" name="client_create_ticket_email"><?php echo get_option('client_create_ticket_email'); ?></textarea>

							<h3><?php $text = __('Updated Ticket Email', 'cqpim'); _e('Updated Ticket Email', 'cqpim'); ?></h3>
							
							<p><?php $text = __('This email is sent to the owner, client and watchers when a ticket is updated', 'cqpim'); _e('This email is sent to the owner, client and watchers when a ticket is updated', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Updated Ticket Email Subject', 'cqpim'); _e('Updated Ticket Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="client_update_ticket_subject" name="client_update_ticket_subject" value="<?php echo get_option('client_update_ticket_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('Updated Ticket Email Content', 'cqpim'); _e('Client Updated Ticket Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="client_update_ticket_email" name="client_update_ticket_email"><?php echo get_option('client_update_ticket_email'); ?></textarea>
						
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>						
						
						</div>
						
						<div id="tabs-8">
						
							<h3><?php $text = __('Client Registration Form (No quote created)', 'cqpim'); _e('Client Registration Form (No quote Required)', 'cqpim'); ?></h3>
							
							<p><?php _e('If you would like to add a form for clients to register without creating a quote, you can use the [cqpim_registration_form] shortcode anywhere on your site. ', 'cqpim'); ?></p>
						
							<?php $auto_welcome = get_option('form_reg_auto_welcome'); ?>
							
							<input type="checkbox" name="form_reg_auto_welcome" id="form_reg_auto_welcome" value="1" <?php checked($auto_welcome, 1, true); ?>/> <?php $text = __('Send the client a welcome email with login details to their dashboard (Recommended).', 'cqpim'); _e('Send the client a welcome email with login details to their dashboard (Recommended).', 'cqpim'); ?>


							<h3><?php $text = __('Frontend Quote Form', 'cqpim'); _e('Frontend Quote Form', 'cqpim'); ?></h3>

							<p><?php _e('This form can be displayed anywhere in your theme with the [cqpim_frontend_form] shortcode. Completion of the form will send an email to the sales email address, create a new client and a new quote and will copy any additional form fields into the Project Brief within the quote.', 'cqpim'); ?></p>
							
							<?php 
							
							$value = get_option('cqpim_frontend_form'); 
							
							$args = array(
							
								'post_type' => 'cqpim_forms',
								
								'posts_per_page' => -1,
								
								'meta_key' => 'form_type',
								
								'meta_value' => 'anonymous_frontend',
								
								'post_status' => 'private'
							
							);
							
							$forms = get_posts($args);
							
							?>
							
							<select name="cqpim_frontend_form" id="cqpim_frontend_form">
							
								<option value=""><?php _e('Choose a form', 'cqpim') ?></option>
								
								<?php foreach($forms as $form) {
								
									if($form->ID == $value) {
									
										$selected = 'selected="selected"';
									
									} else {
									
										$selected = '';
										
									}
								
									echo '<option value="' . $form->ID . '" ' . $selected . '>' . $form->post_title . '</option>';
								
								} ?>
							
							</select>
							
							<br /> <br />
							
							<?php $auto_welcome = get_option('form_auto_welcome'); ?>
							
							<input type="checkbox" name="form_auto_welcome" id="form_auto_welcome" value="1" <?php checked($auto_welcome, 1, true); ?>/> <?php $text = __('Send the client a welcome email with login details to their dashboard (Recommended).', 'cqpim'); _e('Send the client a welcome email with login details to their dashboard (Recommended).', 'cqpim'); ?>
							
							<h3><?php $text = __('Dashboard Quote Form', 'cqpim'); _e('Dashboard Quote Form', 'cqpim'); ?></h3>
							
							<p><?php _e('This form will be displayed in the client Dashboard. Completion of the form will send an email to the sales email address, create a new quote and will copy the form fields into the Project Brief within the quote. Leave this field blank to disable the client Dashboard form', 'cqpim'); ?></p>
							
							<?php 
							
							$value = get_option('cqpim_backend_form');
							
							$args = array(
							
								'post_type' => 'cqpim_forms',
								
								'posts_per_page' => -1,
								
								'meta_key' => 'form_type',
								
								'meta_value' => 'client_dashboard',
								
								'post_status' => 'private'
							
							);
							
							$forms = get_posts($args);
							
							?>
							
							<select name="cqpim_backend_form" id="cqpim_backend_form">
							
								<option value=""><?php _e('Choose a form', 'cqpim') ?></option>
								
								<?php foreach($forms as $form) {
								
									if($form->ID == $value) {
									
										$selected = 'selected="selected"';
									
									} else {
									
										$selected = '';
										
									}
								
									echo '<option value="' . $form->ID . '" ' . $selected . '>' . $form->post_title . '</option>';
								
								} ?>
							
							</select>
							
							<h3><?php $text = __('Form Emails', 'cqpim'); _e('Form Emails', 'cqpim'); ?></h3>
							
							<p><?php $text = __('This email will be sent to your sales email address when a quote has been requested.', 'cqpim'); _e('This email will be sent to your sales email address when a quote has been requested.', 'cqpim'); ?></p>
							


							<p><strong><?php $text = __('New Quote Email Subject', 'cqpim'); _e('New Quote Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="new_quote_subject" name="new_quote_subject" value="<?php echo get_option('new_quote_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('New Quote Email Content', 'cqpim'); _e('New Quote Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="new_quote_email" name="new_quote_email"><?php echo get_option('new_quote_email'); ?></textarea>														
							
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>						
						
						</div>
						
						<div id="tabs-16">
						
							<?php if(is_plugin_active('cqpim-expenses/cqpim-expenses.php')) { ?>
							
								<h3><?php _e('Suppliers / Expenses', 'cqpim'); ?></h3>
								
								<p><strong><?php _e('Expenses Authorisation', 'cqpim'); ?></strong></p>
								
								<?php $auth = get_option('cqpim_activate_expense_auth'); ?>

								<input type="checkbox" name="cqpim_activate_expense_auth" value="1" <?php checked($auth, 1); ?>/> <?php _e('Expenses should be authorised by an Admin', 'cqpim'); ?>

								<p><strong><?php _e('Expenses Authorisation Limit', 'cqpim'); ?></strong></p>
								
								<p><?php _e('If you\'d like to skip authorisation for smaller value expenses, enter the limit here. Any expenses with a value less than entered here will not require authorisation. Leave this blank if you would prefer not to set a limit.', 'cqpim'); ?></p>
		
								<input type="number" name="cqpim_expense_auth_limit" value="<?php echo get_option('cqpim_expense_auth_limit'); ?>" />
		
								<p><strong><?php _e('Permissions', 'cqpim'); ?></strong></p>
								
								<p><?php _e('To control who can authorise expenses, and who can skip authorisation, please visit the plugin Roles & Permissions page.', 'cqpim'); ?></p>
		
								<h3><?php _e('Authorisation Emails', 'cqpim'); ?></h3>
								
								<p><strong><?php $text = __('Authorisation Email Subject', 'cqpim'); _e('Authorisation Email Subject', 'cqpim'); ?></strong></p>
								
								<input type="text" id="cqpim_auth_email_subject" name="cqpim_auth_email_subject" value="<?php echo get_option('cqpim_auth_email_subject'); ?>" />

								<br /><br />
								
								<p><strong><?php $text = __('Authorisation Email Content', 'cqpim'); _e('Authorisation Email Content', 'cqpim'); ?></strong></p>
								
								<textarea style="width:100%; height:200px" id="cqpim_auth_email_content" name="cqpim_auth_email_content"><?php echo get_option('cqpim_auth_email_content'); ?></textarea>														

								<p><strong><?php $text = __('Authorised Email Subject', 'cqpim'); _e('Authorised Email Subject', 'cqpim'); ?></strong></p>
								
								<input type="text" id="cqpim_authorised_email_subject" name="cqpim_authorised_email_subject" value="<?php echo get_option('cqpim_authorised_email_subject'); ?>" />

								<br /><br />
								
								<p><strong><?php $text = __('Authorised Email Content', 'cqpim'); _e('Authorised Email Content', 'cqpim'); ?></strong></p>
								
								<textarea style="width:100%; height:200px" id="cqpim_authorised_email_content" name="cqpim_authorised_email_content"><?php echo get_option('cqpim_authorised_email_content'); ?></textarea>
		
								<p class="submit">

									<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

								</p>						
							
							<?php } else { ?>
							
								<h3><?php _e('Suppliers / Expenses Add-On Not Found', 'cqpim'); ?></h3>
								
								<?php $link = '<a href="http://www.cqpim.uk/cqpim-suppliers-expenses-add-on/" target="_blank">http://www.cqpim.uk/cqpim-suppliers-expenses-add-on/</a>'; ?>
								
								<p><?php printf(__('To use the Suppliers / Expenses part of the plugin, you need to purchase the CQPIM Suppliers / Expenses Add-On. Please visit %1$s for more information.', 'cqpim'), $link); ?></p>
							
							<?php } ?>						
						
						</div>
						
						<div id="tabs-12">
						
							<h3><?php $text = __('Email Piping', 'cqpim'); _e('Email Piping', 'cqpim'); ?></h3>
							
							<p><?php _e('Email Piping works by scanning your mailbox and parsing new emails into the relevant Task/Support Ticket/Project.', 'cqpim'); ?></p>
							
							<p><?php _e('We highly recommend creating a new mailbox for this process, as any incoming email (that doesn\'t relate to an existing item) with an address that is in the system as a client will register a new Support Ticket (If configured).', 'cqpim'); ?></p>
							
							<p><?php _e('It is also critical to place the %%PIPING_ID%% tag in the subject line of all emails related to Support Tickets, Tasks and Project Messages.', 'cqpim'); ?></p>
							
							<p><?php _e('You should also check that the emails contain the latest update message. You can check for the correct tag by clicking the "View Sample Content" button next to each email.', 'cqpim'); ?></p>
							
							<h3><?php _e('Mail Settings', 'cqpim'); ?></h3>
							
							<?php $value = get_option('cqpim_mail_server'); ?>
							
							<p><?php _e('Mail Server Address (including port and path if necessary. eg. for Gmail, it would be "imap.gmail.com:993/imap/ssl"', 'cqpim'); ?></p>
							
							<input type="text" name="cqpim_mail_server" id="cqpim_mail_server" value="<?php echo $value; ?>" />
							
							<br /><br />
							
							<?php $value = get_option('cqpim_piping_address'); ?>
							
							<p><?php _e('Email Address (If Piping is active, this address will be the reply address of all ticket/task emails. Ensure it matches the mailbox below)', 'cqpim'); ?></p>
							
							<input type="text" name="cqpim_piping_address" id="cqpim_piping_address" value="<?php echo $value; ?>" />
							
							<br /><br />
							
							<p><?php _e('Email Username (often the same as the email address)', 'cqpim'); ?></p>
							
							<?php $value = get_option('cqpim_mailbox_name'); ?>
							
							<input type="text" name="cqpim_mailbox_name" id="cqpim_mailbox_name" value="<?php echo $value; ?>" />
							
							<br /><br />
							
							<p><?php _e('Email Password', 'cqpim'); ?></p>
							
							<?php $value = get_option('cqpim_mailbox_pass'); ?>
							
							<input type="password" name="cqpim_mailbox_pass" id="cqpim_mailbox_pass" value="<?php echo $value; ?>" /> <button id="test_piping" /><?php _e('Test Settings', 'cqpim'); ?></button> <div id="test_apinner" class="ajax_spinner" style="display:none"></div>
							
							<br /><br />
							
							<p><?php _e('ID Prefix', 'cqpim'); ?></p>
							
							<p><?php _e('The ID Prefix is used in the Piping ID tag and helps the system to identify where updates should go. For example, if you enter "ID" in this field, the result of the %%PIPING_ID%% tag would be "[ID:1234]".','cqpim'); ?></p>
							
							<?php $value = get_option('cqpim_string_prefix'); ?>
							
							<input type="text" name="cqpim_string_prefix" value="<?php echo $value; ?>" />
							
							<br /><br />
							
							<h3><?php _e('Other Settings', 'cqpim'); ?></h3>
							
							<?php $value = get_option('cqpim_create_support_on_email'); ?>
							
							<input type="checkbox" name="cqpim_create_support_on_email" <?php if($value == 1) { echo 'checked="checked"'; } ?> value="1" /> <?php _e('Create a new support ticket if an email arrives from an address registered to a client in the system', 'cqpim'); ?><br /><br />
							
							<?php $value = get_option('cqpim_send_piping_reject'); ?>
							
							<input type="checkbox" name="cqpim_send_piping_reject" <?php if($value == 1) { echo 'checked="checked"'; } ?> value="1" /> <?php _e('Send a reject email (below) if an email is received that doesn\'t match a client in the system.', 'cqpim'); ?>
							
							<br /><br />
							
							<?php $value = get_option('cqpim_piping_delete'); ?>
							
							<input type="checkbox" name="cqpim_piping_delete" <?php if($value == 1) { echo 'checked="checked"'; } ?> value="1" /> <?php _e('Delete the email from the Piping inbox once it has been processed.', 'cqpim'); ?>
							
							<br /><br />
						
							<h3><?php $text = __('Reject Email', 'cqpim'); _e('Reject Email', 'cqpim'); ?></h3>
							
							<p><?php $text = __('This email will be sent to the sender if the from email address is not registered in CQPIM.', 'cqpim'); _e('This email will be sent to the sender if the from email address is not registered in CQPIM.', 'cqpim'); ?></p>
							
							<p><strong><?php $text = __('Reject Email Subject', 'cqpim'); _e('Reject Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="cqpim_bounce_subject" name="cqpim_bounce_subject" value="<?php echo get_option('cqpim_bounce_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('Reject Email Content', 'cqpim'); _e('Reject Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="cqpim_bounce_content" name="cqpim_bounce_content"><?php echo get_option('cqpim_bounce_content'); ?></textarea>		
						
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>							
						
						</div>
						
						<?php 
						
						$user = wp_get_current_user();
						
						if(in_array('administrator', $user->roles)) { ?>
							
						<div id="tabs-13">
						
							<h3><?php _e('Plugin Reset', 'cqpim'); ?></h3>
							
							<p><?php _e('If you would like to reset CQPIM, click the reset button. This will deactivate the plugin and remove ALL data, including settings, roles, permissions and all posts (projects, clients etc). This cannot be undone.', 'cqpim'); ?></p>
							
							<p><strong><?php _e('IMPORTANT: Any users who have a CQPIM role will need to have their role reassigned and will not be able to access the site until this is done.', 'cqpim'); ?></strong></p>
											
							<br /><br />
							
							<button id="reset-cqpim"><?php _e('Reset CQPIM and remove ALL data', 'cqpim'); ?></button>
							
							<br /><br />
							
							<div id="reset_cqpim_container" style="display:none">
							
								<div id="reset_cqpim">
								
									<div style="padding:12px">
									
										<h3><?php _e('Are you sure?', 'cqpim'); ?></h3>
										
										<p><?php _e('Are you sure you want to deactivate CQPIM and remove ALL associated data and settings?', 'cqpim'); ?></p>
					
										<a class="cancel-colorbox"><?php _e('Cancel', 'cqpim'); ?></a>
										
										<button class="reset-cqpim-conf metabox-add-button"><?php _e('Confirm', 'cqpim'); ?></button>
										
									</div>
									
								</div>
								
							</div>
						
						</div>
						
						<?php } ?>
						
						<div id="tabs-14">
						
							<h3><?php _e('Enable Messaging System', 'cqpim'); ?></h3>
							
							<?php $checked = get_option('cqpim_enable_messaging'); ?>
							
							<input type="checkbox" name="cqpim_enable_messaging" value="1" <?php checked($checked, 1, true); ?> /> <?php _e('Enable Messaging System for Team Members', 'cqpim'); ?>
						
							<br /><br />
							
							<?php $checked = get_option('cqpim_messages_allow_client'); ?>
							
							<input type="checkbox" name="cqpim_messages_allow_client" value="1" <?php checked($checked, 1, true); ?> /> <?php _e('Enable Messaging System for Clients', 'cqpim'); ?>
							
							<h3><?php _e('New Message Notication', 'cqpim'); ?></h3>
							
							<p><strong><?php $text = __('New Message Email Subject', 'cqpim'); _e('New Message Email Subject', 'cqpim'); ?></strong></p>
							
							<input type="text" id="cqpim_new_message_subject" name="cqpim_new_message_subject" value="<?php echo get_option('cqpim_new_message_subject'); ?>" />

							<br /><br />
							
							<p><strong><?php $text = __('New Message Email Content', 'cqpim'); _e('New Message Email Content', 'cqpim'); ?></strong></p>
							
							<textarea style="width:100%; height:200px" id="cqpim_new_message_content" name="cqpim_new_message_content"><?php echo get_option('cqpim_new_message_content'); ?></textarea>
							
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>						
						
						</div>
						
						<div id="tabs-15">
						
							<h3><?php _e('HTML Email Template', 'cqpim'); ?></h3>
							
							<p><?php _e('If you would like to customise the outgoing emails from CQPIM with an HTML email template, you can build one here.', 'cqpim'); ?></p>
							
							<p><?php _e('You can use the %%EMAIL_CONTENT%% tag to render the content of the email in your template', 'cqpim'); ?></p>
							
							<p><?php _e('You can use the %%LOGO%% tag to render the Company Logo', 'cqpim'); ?></p>
							
							<h3><?php _e('HTML Email Styles', 'cqpim'); ?></h3>
							
							<textarea style="width:100%; height:300px" name="cqpim_html_email_styles"><?php echo get_option('cqpim_html_email_styles'); ?></textarea>
							
							<h3><?php _e('HTML Email Markup', 'cqpim'); ?></h3>
						
							<textarea style="width:100%; height:500px" name="cqpim_html_email"><?php echo get_option('cqpim_html_email'); ?></textarea>
						
							<p class="submit">

								<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cqpim'); ?>" />

							</p>						
						
						</div>	

						<div class="clear"></div>
						
					</div>
					
					<div class="clear"></div>

				</div><!-- eof #main-container -->

				<div class="clear"></div>

			</form>


			<div class="clear"></div>

		</div><!-- eof.wrap -->

<?php } 