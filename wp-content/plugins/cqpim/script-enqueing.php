<?php

	/**

	* Register all custom non admin scrips in one function and then call them

	* conditionally as and when required.

	*/

	// REGISTER ALL SCRIPTS TO BE LOADED ON BOTH FRONT AND BACKEND

	add_action( 'wp_loaded', 'cqpim_register_front_and_back_scripts' );

	function cqpim_register_front_and_back_scripts(){

		wp_register_script( 

			'jquery-min',

			'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js',

			array( 'jquery' ),

			FALSE,

			TRUE

		);

		wp_register_script( 

			'jquery-ui',

			'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',

			array( 'jquery' ),

			FALSE,

			TRUE

		);

		// JQUERY UI CSS

		wp_register_style( 

			'jquery-ui-styles', 

			'https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css',

			array(), 

			'1.10.3', 

			'all' 

		);	
		
		wp_register_style( 

			'fontawesome', 

			plugin_dir_url(__FILE__)  . 'assets/font-awesome/css/font-awesome.css',

			array(), 

			'1.10.3', 

			'all' 

		);
		
		wp_register_script( 

			'autocomplete',

			plugin_dir_url(__FILE__)  . 'assets/autocomplete/src/jquery.tokeninput.js',

			array( 'jquery' ),

			FALSE,

			TRUE

		);
		
		wp_register_style( 

			'autocomplete-styles', 

			plugin_dir_url(__FILE__)  . 'assets/autocomplete/styles/token-input.css',

			array(), 

			NULL, 

			'all'

		);
		
		wp_register_script( 

			'colorbox',

			plugin_dir_url(__FILE__)  . 'assets/colorbox/jquery.colorbox-min.js', 

			array( 'jquery' ), 

			NULL, 

			TRUE 

		);	

		wp_register_style( 

			'colorbox-styles', 

			plugin_dir_url(__FILE__)  . 'assets/colorbox/colorbox.css',

			array(), 

			NULL, 

			'all'

		);
		
		// DataTables
		
		wp_register_script( 

			'datatables',

			plugin_dir_url(__FILE__)  . 'assets/jquery-datatables/js/jquery.dataTables.min.js', 

			array( 'jquery' ), 

			NULL, 

			TRUE 

		);	

		wp_register_style( 

			'datatables-styles', 

			plugin_dir_url(__FILE__)  . 'assets/jquery-datatables/css/jquery.dataTables.min.css',

			array(), 

			NULL, 

			'all'

		);
		
		wp_register_style( 

			'cqpim-upload-styles', 

			plugin_dir_url(__FILE__)  . 'assets/jquery-uploader/assets/css/style.css',

			array(), 

			NULL, 

			'all'

		);
		
		wp_register_script( 

			'cqpim-upload-knob',

			plugin_dir_url(__FILE__)  . 'assets/jquery-uploader/assets/js/jquery.knob.js', 

			array( 'jquery' ), 

			NULL, 

			TRUE 

		);
		
		wp_register_script( 

			'cqpim-upload-transport',

			plugin_dir_url(__FILE__)  . 'assets/jquery-uploader/assets/js/jquery.iframe-transport.js', 

			array( 'jquery' ), 

			NULL, 

			TRUE 

		);	
		
		wp_register_script( 

			'cqpim-upload-widget',

			plugin_dir_url(__FILE__)  . 'assets/jquery-uploader/assets/js/jquery.ui.widget.js', 

			array( 'jquery' ), 

			NULL, 

			TRUE 

		);
		
		wp_register_script( 

			'cqpim-upload-fileupload',

			plugin_dir_url(__FILE__)  . 'assets/jquery-uploader/assets/js/jquery.fileupload.js', 

			array( 'jquery' ), 

			NULL, 

			TRUE 

		);
		
		wp_register_script( 

			'cqpim-upload-main',

			plugin_dir_url(__FILE__)  . 'assets/jquery-uploader/assets/js/script.js', 

			array( 'jquery' ), 

			NULL, 

			TRUE 

		);
		
		wp_register_script( 

			'cqpim-client-messaging',

			plugin_dir_url(__FILE__)  . 'js/messaging-client-custom.js', 

			array( 'jquery' ), 

			NULL, 

			TRUE 

		);

	}	

	// REGISTER ALL NON-ADMIN (FRONTEND) SCRIPTS

	add_action( 'wp_enqueue_scripts', 'cqpim_register_all_non_admin_scripts' );

	function cqpim_register_all_non_admin_scripts() {
	
		wp_register_style( 

			'masonry', 

			plugin_dir_url(__FILE__)  . 'assets/masonry/masonry.js',

			array('jquery'), 

			NULL,

			TRUE

		);

		wp_register_style( 

			'cqpim-frontend-styles', 

			plugin_dir_url(__FILE__)  . 'css/frontend-styles.css',

			array(), 

			'1.10.3', 

			'all' 

		);
		
		wp_register_style( 

			'cqpim-client-styles', 

			plugin_dir_url(__FILE__)  . 'css/cqpim-client-styles-new.css',

			array(), 

			'1.10.3', 

			'all' 

		);
		
		wp_register_style( 

			'print-styles', 

			plugin_dir_url(__FILE__)  . 'css/cqpim-print.css',

			array(), 

			'1.10.3', 

			'print' 

		);
		
		wp_register_style( 

			'raleway-font', 

			'https://fonts.googleapis.com/css?family=Open+Sans',

			array(), 

			'1.10.3', 
			
			'all'

		);
		
		wp_register_script( 

			'quote-frontend-ajax',

			plugin_dir_url(__FILE__)  . 'js/quote-frontend-ajax.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'project-frontend-ajax',

			plugin_dir_url(__FILE__)  . 'js/project-frontend-ajax.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'client-dashboard-custom',

			plugin_dir_url(__FILE__)  . 'js/client-dashboard-custom.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'support-tickets-frontend',

			plugin_dir_url(__FILE__)  . 'js/support-tickets-frontend.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'task-frontend-ajax',

			plugin_dir_url(__FILE__)  . 'js/task-frontend-ajax.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);

	}

	// REGISTER ALL ADMIN SCRIPTS

	add_action( 'admin_enqueue_scripts', 'cqpim_register_all_admin_scripts' );

	function cqpim_register_all_admin_scripts(){
		
		wp_register_script( 

			'plugin_dash_messaging_page',

			plugin_dir_url(__FILE__)  . 'js/messaging-admin-custom.js',

			array( 'jquery', 'jquery-ui' ),

			NULL,

			TRUE

		);
	
		wp_register_script( 

			'tasks_admin_custom',

			plugin_dir_url(__FILE__)  . 'js/tasks-admin-custom.js',

			array( 'jquery', 'jquery-ui' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'tasks_admin_ajax',

			plugin_dir_url(__FILE__)  . 'js/tasks-admin-ajax.js',

			array( 'jquery', 'jquery-ui' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'templates_admin_ajax',

			plugin_dir_url(__FILE__)  . 'js/templates-admin-ajax.js',

			array( 'jquery', 'jquery-ui' ),

			NULL,

			TRUE

		);

		wp_register_script( 

			'plugin_options_pages_custom',

			plugin_dir_url(__FILE__)  . 'js/plugin-options-custom-js.js',

			array( 'jquery', 'jquery-ui' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'throughout_admin',

			plugin_dir_url(__FILE__)  . 'js/throughout-admin.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'plugin_dash_page_custom',

			plugin_dir_url(__FILE__)  . 'js/plugin-dash-custom-js.js',

			array( 'jquery', 'jquery-ui' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'permissions-custom',

			plugin_dir_url(__FILE__)  . 'js/permissions-custom.js',

			array( 'jquery', 'jquery-ui' ),

			NULL,

			TRUE

		);
		
		wp_register_style( 

			'cqpim-admin-styles', 

			plugin_dir_url(__FILE__)  . 'css/cqpim-admin-styles-new.css',

			array(), 

			'1.10.3', 

			'all' 

		);
		
		wp_register_script( 

			'quote-cpt-ajax',

			plugin_dir_url(__FILE__)  . 'js/quote-admin-ajax.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'quote-cpt-custom',

			plugin_dir_url(__FILE__)  . 'js/quote-admin-custom.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'forms-cpt-ajax',

			plugin_dir_url(__FILE__)  . 'js/forms-admin-ajax.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'client-cpt-custom',

			plugin_dir_url(__FILE__)  . 'js/client-admin-custom.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'project-cpt-ajax',

			plugin_dir_url(__FILE__)  . 'js/project-admin-ajax.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'project-cpt-custom',

			plugin_dir_url(__FILE__)  . 'js/project-admin-custom.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'invoice-cpt-custom',

			plugin_dir_url(__FILE__)  . 'js/invoice-admin-custom.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'invoice-cpt-ajax',

			plugin_dir_url(__FILE__)  . 'js/invoice-admin-ajax.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'jquery-repeater',

			plugin_dir_url(__FILE__)  . 'assets/jquery-repeater/jquery.repeater.min.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'formbuilder',

			plugin_dir_url(__FILE__)  . 'assets/formbuilder/assets/js/form-builder.min.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'formbuilder-render',

			plugin_dir_url(__FILE__)  . 'assets/formbuilder/assets/js/form-render.min.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_style( 

			'formbuilder-styles', 

			plugin_dir_url(__FILE__)  . 'assets/formbuilder/assets/css/form-builder.min.css',

			array(), 

			'1.10.3', 

			'all' 

		);
		
		wp_register_script( 

			'jquery-repeater-perms',

			plugin_dir_url(__FILE__)  . 'assets/jquery-repeater/jquery.repeater.perms.min.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'jquery-timer',

			plugin_dir_url(__FILE__)  . 'assets/jquery-timer/jquery.timer.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'support-tickets-custom',

			plugin_dir_url(__FILE__)  . 'js/support-tickets-custom.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'cqpim-custom-fields-ajax',

			plugin_dir_url(__FILE__)  . 'js/custom-fields-admin-ajax.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'fullcal-moment',

			plugin_dir_url(__FILE__)  . 'assets/fullcalendar/lib/moment.min.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_script( 

			'fullcal',

			plugin_dir_url(__FILE__)  . 'assets/fullcalendar/fullcalendar.min.js',

			array( 'jquery' ),

			NULL,

			TRUE

		);
		
		wp_register_style( 

			'fullcal-styles', 

			plugin_dir_url(__FILE__)  . 'assets/fullcalendar/fullcalendar.min.css',

			array(), 

			'1.10.3', 

			'all' 

		);
		
		wp_register_script(
				'fullcal-locale',
				plugin_dir_url(__FILE__)  . 'assets/fullcalendar/lang/' . substr(get_locale(), 0, 2) . '.js',
				array( 'jquery', 'fullcal' ),
				NULL,
				TRUE
		);
		
		wp_register_script( 

			'charts',

			'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js',

			array( 'jquery' ),

			NULL,

			TRUE
			
		);


	}

	// ENQUEUE NON-ADMIN SCRIPTS CONDITIONALLY

	add_action( 'wp_enqueue_scripts', 'cqpim_enqueue_scripts_where_required', 49 );

	function cqpim_enqueue_scripts_where_required() {
	
	
		global $wp_locale;
		
		$args = array(
			'closeText'         => __( 'Done', 'cqpim' ),
			'currentText'       => __( 'Today', 'cqpim' ),
			'monthNames'        => array_values( $wp_locale->month ),
			'monthNamesShort'   => array_values( $wp_locale->month_abbrev ),
			'monthStatus'       => __( 'Show a different month', 'cqpim' ),
			'dayNames'          => array_values( $wp_locale->weekday ),
			'dayNamesShort'     => array_values( $wp_locale->weekday_abbrev ),
			'dayNamesMin'       => array_values( $wp_locale->weekday_initial ),
			// set the date format to match the WP general date settings
			'dateFormat'        => date_format_php_to_js( get_option( 'cqpim_date_format' ) ),
			// get the start of week from WP general setting
			'firstDay'          => get_option( 'start_of_week' )
		);
		
		$datatables = array(
		
			'sEmptyTable' => __('No data available in table', 'cqpim'),
			'sInfo' => __('Showing _START_ to _END_ of _TOTAL_', 'cqpim'),
			'sInfoEmpty' => __('Showing 0 to 0 of 0', 'cqpim'),
			'sInfoFiltered' => __('(filtered from _MAX_ total entries)', 'cqpim'),
			'sInfoPostFix' => '',
			'sInfoThousands' => ',',
			'sLengthMenu' => __('Show _MENU_', 'cqpim'),
			'sLoadingRecords' => __('Loading...', 'cqpim'),
			'sProcessing' => __('Processing...', 'cqpim'),
			'sSearch' => __('Search:', 'cqpim'),
			'sZeroRecords' => __('No matching records found', 'cqpim'),

				'sFirst' => __('First', 'cqpim'),
				'sLast' => __('Last', 'cqpim'),
				'sNext' => __('Next', 'cqpim'),
				'sPrevious' => __('Previous', 'cqpim'),

				'sSortAscending' =>  __(': activate to sort column ascending', 'cqpim'),
				'sSortDescending' => __(': activate to sort column descending', 'cqpim'),
	
		);
		
		$theme = get_option('client_dashboard_type');
	
		if(is_singular('attachment')) {
		
			global $post;
			
			$attachment = get_post_meta($post->ID, 'cqpim', true);
			
			if($attachment == true) {
				
				if($theme == 'inc') {
			
					global $wp_styles;
						
					foreach( $wp_styles->queue as $handle ) {
					
						wp_dequeue_style($handle);
						
					}
					
					global $wp_scripts;
						
					foreach( $wp_scripts->queue as $handle ) {
					
						wp_dequeue_script($handle);
						
					}
				
				}
				
				wp_enqueue_script( 'jquery-ui' );
			
				wp_enqueue_style( 'raleway-font' );
				
				wp_enqueue_script( 'task-frontend-ajax' );
			
				wp_localize_script( 'task-frontend-ajax', 'task_frontend', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
				
				wp_enqueue_style( 'fontawesome' );
				
				if($theme == 'inc') {
					
					wp_enqueue_style( 'cqpim-client-styles' );
					
				} else {
					
					wp_enqueue_style( 'cqpim-frontend-styles' );
					
				}
			
			}
		
		}

		if(is_singular('cqpim_quote')) {
			
			wp_enqueue_script( 'jquery-ui' );
			
			if($theme == 'inc') {
		
				global $wp_styles;
					
				foreach( $wp_styles->queue as $handle ) {
				
					wp_dequeue_style($handle);
					
				}
				
				global $wp_scripts;
					
				foreach( $wp_scripts->queue as $handle ) {
				
					wp_dequeue_script($handle);
					
				}
				
				wp_enqueue_style( 'raleway-font' );
			
			}
		
			if($theme == 'inc') {
				
				wp_enqueue_style( 'cqpim-client-styles' );
				
			} else {
				
				wp_enqueue_style( 'cqpim-frontend-styles' );
				
			}
			
			wp_enqueue_script( 'quote-frontend-ajax' );
			
			wp_localize_script( 'quote-frontend-ajax', 'quote_frontend', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			
			wp_enqueue_style( 'fontawesome' );
		
		}
		
		if(is_singular('cqpim_tasks')) {
			
			wp_enqueue_script( 'jquery-ui' );
			
			if($theme == 'inc') {
		
				global $wp_styles;
					
				foreach( $wp_styles->queue as $handle ) {
				
					wp_dequeue_style($handle);
					
				}
				
				global $wp_scripts;
					
				foreach( $wp_scripts->queue as $handle ) {
				
					wp_dequeue_script($handle);
					
				}
				
				wp_enqueue_style( 'raleway-font' );
			
			}
			
			wp_enqueue_script('masonry');
		
			if($theme == 'inc') {
				
				wp_enqueue_style( 'cqpim-client-styles' );
				
			} else {
				
				wp_enqueue_style( 'cqpim-frontend-styles' );
				
			}
			
			wp_enqueue_script( 'task-frontend-ajax' );
			
			wp_localize_script( 'task-frontend-ajax', 'task_frontend', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			
			wp_enqueue_style( 'cqpim-upload-styles'  );

			wp_enqueue_script( 'cqpim-upload-knob');
			
			wp_enqueue_script( 'cqpim-upload-widget');
			
			wp_enqueue_script( 'cqpim-upload-transport');
			
			wp_enqueue_script( 'cqpim-upload-fileupload');
			
			wp_enqueue_script( 'cqpim-upload-main');
			
			wp_enqueue_style( 'fontawesome' );
		
		}
		
		if(is_singular('cqpim_project')) {
			
			if($theme == 'inc') {
		
				global $wp_styles;
					
				foreach( $wp_styles->queue as $handle ) {
				
					wp_dequeue_style($handle);
					
				}
				
				wp_enqueue_style( 'raleway-font' );
				
				global $wp_scripts;
					
				foreach( $wp_scripts->queue as $handle ) {
				
					wp_dequeue_script($handle);
					
				}
			
			}
			
			wp_enqueue_script( 'jquery-ui' );
			
			wp_enqueue_script( 'gantt' );
			
			wp_enqueue_style( 'gantt-styles' );
		
			wp_enqueue_script( 'datatables' );
			
			wp_enqueue_style( 'datatables-styles' );
			
			wp_enqueue_script( 'colorbox' );
			
			wp_enqueue_style( 'colorbox-styles' );
		
			if($theme == 'inc') {
				
				wp_enqueue_style( 'cqpim-client-styles' );
				
			} else {
				
				wp_enqueue_style( 'cqpim-frontend-styles' );
				
			}
			
			wp_enqueue_script( 'project-frontend-ajax' );
			
			wp_localize_script( 'project-frontend-ajax', 'project_frontend', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
				
			wp_enqueue_script('masonry');
			
			wp_enqueue_style( 'fontawesome' );
			
		}
		
		if(is_singular('cqpim_invoice') || is_singular('cqpim_support')) {
			
			wp_enqueue_script( 'jquery-ui' );
			
			if($theme == 'inc') {
		
				global $wp_styles;
					
				foreach( $wp_styles->queue as $handle ) {
				
					wp_dequeue_style($handle);
					
				}
				
				global $wp_scripts;
					
				foreach( $wp_scripts->queue as $handle ) {
				
					wp_dequeue_script($handle);
					
				}
				
				wp_enqueue_style( 'raleway-font' );
			
			}
			
			wp_enqueue_script( 'jquery-ui' );
		
			if($theme == 'inc') {
				
				wp_enqueue_style( 'cqpim-client-styles' );
				
			} else {
				
				wp_enqueue_style( 'cqpim-frontend-styles' );
				
			}
			
			wp_enqueue_script( 'support-tickets-custom' );
			
			wp_enqueue_style( 'fontawesome' );
		
		}
		
		if(is_singular('cqpim_invoice')) {
		
			global $wp_scripts;
				
			foreach( $wp_scripts->queue as $handle ) {
			
				wp_dequeue_script($handle);
				
			}
		
			wp_enqueue_script( 'datatables' );
			
			wp_enqueue_style( 'datatables-styles' );
		
			wp_enqueue_style( 'print-styles' );
			
			wp_enqueue_script('client-dashboard-custom');
			
			wp_enqueue_script('masonry');
			
			wp_enqueue_script( 'colorbox' );
		
			wp_enqueue_style( 'colorbox-styles' );
			
			wp_localize_script( 'client-dashboard-custom', 'objectL10n', $args );	
			
			wp_localize_script( 'client-dashboard-custom', 'dti18n', $datatables );
			
			wp_enqueue_style( 'fontawesome' );
		
		}
		
		if(is_singular('cqpim_support')) {
			
			wp_enqueue_script( 'jquery-ui' );
			
			wp_enqueue_style( 'jquery-ui-styles' );
		
			wp_enqueue_script( 'support-tickets-frontend' );
			
			wp_localize_script( 'support-tickets-frontend', 'support_frontend', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			
			wp_localize_script( 'support-tickets-frontend', 'objectL10n', $args );
		
			wp_enqueue_script('masonry');
			
			wp_enqueue_style( 'cqpim-upload-styles'  );

			wp_enqueue_script( 'cqpim-upload-knob');
			
			wp_enqueue_script( 'cqpim-upload-widget');
			
			wp_enqueue_script( 'cqpim-upload-transport');
			
			wp_enqueue_script( 'cqpim-upload-fileupload');
			
			wp_enqueue_script( 'cqpim-upload-main');
			
			wp_enqueue_style( 'fontawesome' );
			
		}
		
		$login_page = get_option('cqpim_login_page');
		
		$dash_page = get_option('cqpim_client_page');
		
		$reset_page = get_option('cqpim_reset_page');
		
		if(is_page($login_page) || is_page($dash_page) || is_page($reset_page)) {
			
			if($theme == 'inc') {
		
				global $wp_styles;
					
				foreach( $wp_styles->queue as $handle ) {
				
					wp_dequeue_style($handle);
					
				}
				
				global $wp_scripts;
					
				foreach( $wp_scripts->queue as $handle ) {
				
					wp_dequeue_script($handle);
					
				}
				
				wp_enqueue_style( 'raleway-font' );
			
			}
			
			wp_enqueue_script( 'jquery-ui' );
			
			wp_enqueue_style( 'jquery-ui-styles' );
			
			if($theme == 'inc') {
				
				wp_enqueue_style( 'cqpim-client-styles' );
				
			} else {
				
				wp_enqueue_style( 'cqpim-frontend-styles' );
				
			}
			
			wp_enqueue_script('masonry');
		
			wp_enqueue_script('colorbox');
			
			wp_enqueue_style('colorbox-styles');
		
			wp_enqueue_script('client-dashboard-custom');
			
			wp_enqueue_script( 'datatables' );
			
			wp_enqueue_style( 'datatables-styles' );
			
			wp_localize_script( 'client-dashboard-custom', 'dti18n', $datatables );
			
			wp_enqueue_style( 'cqpim-upload-styles'  );

			wp_enqueue_script( 'cqpim-upload-knob');
			
			wp_enqueue_script( 'cqpim-upload-widget');
			
			wp_enqueue_script( 'cqpim-upload-transport');
			
			wp_enqueue_script( 'cqpim-upload-fileupload');

			wp_enqueue_script( 'cqpim-upload-main');

			wp_localize_script( 'client-dashboard-custom', 'client_frontend', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			
			wp_localize_script( 'client-dashboard-custom', 'objectL10n', $args );	

			wp_enqueue_style( 'fontawesome' );
			
			wp_enqueue_script( 'autocomplete' );
			
			wp_enqueue_style( 'autocomplete-styles' );
			
			wp_enqueue_script( 'cqpim-client-messaging' );
				
		}

	}
	

	// ENQUEUE ADMIN SCRIPTS

	add_action( 'admin_enqueue_scripts', 'cqpim_enqueue_admin_js' );

	function cqpim_enqueue_admin_js() {
	
		global $wp_locale;
		
		$args = array(
			'closeText'         => __( 'Done', 'cqpim' ),
			'currentText'       => __( 'Today', 'cqpim' ),
			'monthNames'        => array_values( $wp_locale->month ),
			'monthNamesShort'   => array_values( $wp_locale->month_abbrev ),
			'monthStatus'       => __( 'Show a different month', 'cqpim' ),
			'dayNames'          => array_values( $wp_locale->weekday ),
			'dayNamesShort'     => array_values( $wp_locale->weekday_abbrev ),
			'dayNamesMin'       => array_values( $wp_locale->weekday_initial ),
			'dateFormat'        => date_format_php_to_js( get_option( 'cqpim_date_format' ) ),
			'firstDay'          => get_option( 'start_of_week' )
		);	
		
		$datatables = array(
		
			'sEmptyTable' => __('No data available in table', 'cqpim'),
			'sInfo' => __('Showing _START_ to _END_ of _TOTAL_', 'cqpim'),
			'sInfoEmpty' => __('Showing 0 to 0 of 0', 'cqpim'),
			'sInfoFiltered' => __('(filtered from _MAX_ total entries)', 'cqpim'),
			'sInfoPostFix' => '',
			'sInfoThousands' => ',',
			'sLengthMenu' => __('Show _MENU_', 'cqpim'),
			'sLoadingRecords' => __('Loading...', 'cqpim'),
			'sProcessing' => __('Processing...', 'cqpim'),
			'sSearch' => __('Search:', 'cqpim'),
			'sZeroRecords' => __('No matching records found', 'cqpim'),

				'sFirst' => __('First', 'cqpim'),
				'sLast' => __('Last', 'cqpim'),
				'sNext' => __('Next', 'cqpim'),
				'sPrevious' => __('Previous', 'cqpim'),

				'sSortAscending' =>  __(': activate to sort column ascending', 'cqpim'),
				'sSortDescending' => __(': activate to sort column descending', 'cqpim'),
	
		);

		global $post_type;

		$screen = get_current_screen();

		// ALL ADMIN PAGES
		
		wp_enqueue_script( 'jquery-ui' );
		
		// SPECIFIC POST TYPES

		switch ( $post_type ) {
		
			case 'cqpim_tasks':
			
				wp_enqueue_style( 'jquery-ui-styles' );
				
				wp_enqueue_script( 'throughout_admin' );
				
				wp_enqueue_script( 'tasks_admin_custom' );
				
				wp_enqueue_script( 'tasks_admin_ajax' );
				
				wp_localize_script( 'tasks_admin_ajax', 'dti18n', $datatables );
				
				wp_enqueue_script( 'jquery-timer' );
				
				wp_enqueue_script( 'datatables' );
			
				wp_enqueue_style( 'datatables-styles' );
				
				wp_localize_script( 'tasks_admin_custom', 'objectL10n', $args );
				
				wp_localize_script( 'tasks_admin_custom', 'dti18n', $datatables );
				
				wp_enqueue_script( 'cqpim-upload-knob');
				wp_enqueue_script( 'cqpim-upload-widget');
				wp_enqueue_script( 'cqpim-upload-transport');
				wp_enqueue_script( 'cqpim-upload-fileupload');
				wp_enqueue_script( 'cqpim-upload-main');
				
				wp_enqueue_style( 'cqpim-upload-styles'  );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;
		
			case 'cqpim_forms':
			
				wp_enqueue_script( 'throughout_admin' );
				
				wp_enqueue_style( 'jquery-ui-styles' );
				
				wp_dequeue_script('upme_chosen_js');
				
				wp_enqueue_script( 'formbuilder' );
				
				wp_enqueue_script( 'formbuilder-render' );
				
				wp_enqueue_style( 'formbuilder-styles' );
				
				wp_enqueue_script( 'colorbox' );
			
				wp_enqueue_style( 'colorbox-styles' );
				
				wp_enqueue_script( 'forms-cpt-ajax' );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;
		
			case 'cqpim_client':
			
				wp_enqueue_script( 'throughout_admin' );
			
				wp_enqueue_style( 'jquery-ui-styles' );
				
				wp_enqueue_script( 'colorbox' );
			
				wp_enqueue_style( 'colorbox-styles' );
				
				wp_enqueue_script( 'client-cpt-custom' );
				
				wp_enqueue_script( 'password-strength-meter' );
				
				wp_enqueue_script( 'jquery-repeater' );
				
				wp_enqueue_script( 'datatables' );
			
				wp_enqueue_style( 'datatables-styles' );
				
				wp_localize_script( 'client-cpt-custom', 'objectL10n', $args );
				
				wp_localize_script( 'client-cpt-custom', 'dti18n', $datatables );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;
				
			case 'cqpim_teams':
			
				wp_enqueue_script( 'throughout_admin' );
			
				wp_enqueue_style( 'jquery-ui-styles' );
				
				wp_enqueue_script( 'colorbox' );
				
				wp_enqueue_script( 'charts' );
			
				wp_enqueue_style( 'colorbox-styles' );
				
				wp_enqueue_script( 'client-cpt-custom' );
				
				wp_localize_script( 'client-cpt-custom', 'dti18n', $datatables );
				
				wp_enqueue_script( 'datatables' );
			
				wp_enqueue_style( 'datatables-styles' );
				
				wp_localize_script( 'client-cpt-custom', 'objectL10n', $args );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;
				
			case 'cqpim_terms':
			
				wp_enqueue_style( 'cqpim-admin-styles' );
				
				wp_enqueue_style( 'fontawesome' );
			
				break;

			case 'cqpim_quote':
			
				wp_enqueue_script( 'throughout_admin' );
			
				wp_enqueue_style( 'jquery-ui-styles' );
				
				wp_enqueue_script( 'colorbox' );
			
				wp_enqueue_style( 'colorbox-styles' );

				wp_enqueue_script( 'quote-cpt-ajax' );
				
				wp_enqueue_script( 'quote-cpt-custom' );
				
				wp_localize_script( 'quote-cpt-custom', 'dti18n', $datatables );
				
				wp_enqueue_script( 'datatables' );
			
				wp_enqueue_style( 'datatables-styles' );
				
				wp_localize_script( 'quote-cpt-custom', 'objectL10n', $args );
				
				$vars = array(
				
					'assign_error' => __('You are not assigned to this project', 'cqpim'),
					'project_dates' => __('Please choose the Type, Client, Ref and Dates for this Quote / Estimate', 'cqpim'),
				
				);
				
				wp_localize_script( 'quote-cpt-custom', 'vars', $vars );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;	
				
			case 'cqpim_templates':
			
				wp_enqueue_script( 'throughout_admin' );
			
				wp_enqueue_style( 'jquery-ui-styles' );
				
				wp_enqueue_script( 'colorbox' );
			
				wp_enqueue_style( 'colorbox-styles' );

				wp_enqueue_script( 'templates_admin_ajax' );
				
				wp_localize_script( 'templates_admin_ajax', 'objectL10n', $args );
				
				wp_localize_script( 'templates_admin_ajax', 'dti18n', $datatables );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;	

			case 'cqpim_project':
			
				wp_enqueue_script( 'throughout_admin' );
			
				$vars = array(
				
					'assign_error' => __('You are not assigned to this project', 'cqpim'),
					'project_dates' => __('Please add a Ref and Dates for this Project', 'cqpim'),
					'ms_complete' => __('You cannot mark a milestone as complete until you have completed the finished cost field.', 'cqpim')
				
				);
				
				wp_enqueue_script( 'colorbox' );
				
				wp_enqueue_style( 'jquery-ui-styles' );
			
				wp_enqueue_style( 'colorbox-styles' );

				wp_enqueue_script( 'project-cpt-ajax' );
				
				wp_enqueue_script( 'project-cpt-custom' );
				
				wp_enqueue_media();
				
				wp_enqueue_script( 'datatables' );
			
				wp_enqueue_style( 'datatables-styles' );
				
				wp_localize_script( 'project-cpt-custom', 'objectL10n', $args );
				
				wp_localize_script( 'project-cpt-custom', 'dti18n', $datatables );
				
				wp_localize_script( 'project-cpt-custom', 'vars', $vars );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;	

			case 'cqpim_invoice':
			
				wp_enqueue_script( 'throughout_admin' );
			
				wp_enqueue_style( 'jquery-ui-styles' );
			
				wp_enqueue_script( 'colorbox' );
			
				wp_enqueue_style( 'colorbox-styles' );

				wp_enqueue_script( 'invoice-cpt-custom' );
				
				wp_enqueue_script( 'invoice-cpt-ajax' );
				
				wp_enqueue_script( 'jquery-repeater' );
				
				wp_localize_script( 'invoice-cpt-custom', 'objectL10n', $args );
				
				wp_localize_script( 'invoice-cpt-custom', 'dti18n', $datatables );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;

			case 'cqpim_support':
			
				wp_enqueue_script( 'throughout_admin' );
			
				wp_enqueue_style( 'jquery-ui-styles' );
			
				wp_enqueue_script( 'colorbox' );
			
				wp_enqueue_style( 'colorbox-styles' );

				wp_enqueue_script( 'support-tickets-custom' );
				
				wp_localize_script( 'support-tickets-custom', 'objectL10n', $args );
				
				wp_localize_script( 'support-tickets-custom', 'dti18n', $datatables );
								
				wp_enqueue_style( 'cqpim-upload-styles'  );
				
				wp_enqueue_style( 'fontawesome' );
				
				wp_enqueue_style( 'cqpim-admin-styles' );

				break;					

			default:

				break;

		}

	}
	
	function date_format_php_to_js( $sFormat ) {
		switch( $sFormat ) {
			//Predefined WP date formats
			case 'F j, Y':
				return( 'MM dd, yy' );
				break;
			case 'Y/m/d':
				return( 'yy/mm/dd' );
				break;
			case 'm/d/Y':
				return( 'mm/dd/yy' );
				break;
			case 'd/m/Y':
				return( 'dd/mm/yy' );
				break;
			case 'Y-m-d':
				return( 'yy-mm-dd' );
				break;
		}
	 
	}

	function cqpim_enqueue_plugin_option_scripts() {

		add_action( 'admin_enqueue_scripts', 'cqpim_enqueue_plugin_option_scripts_now' );		

	}

	function cqpim_enqueue_plugin_option_scripts_now() {

			$scripts = array(
			
				'throughout_admin',
			
				'colorbox',

				'plugin_options_pages_custom'

			);

			cqpim_custom_enqueue_scripts( $scripts );
			
			$styles = array(
			
				'colorbox-styles',
				
				'jquery-ui-styles',
				
				'fontawesome',
				
				'cqpim-admin-styles',

			);

			cqpim_custom_enqueue_styles( $styles );

	}
	
	function cqpim_enqueue_cqpim_custom_fields_scripts() {

		add_action( 'admin_enqueue_scripts', 'cqpim_enqueue_cqpim_custom_fields_scripts_now' );		

	}
	
	function cqpim_enqueue_cqpim_custom_fields_scripts_now() {

			$scripts = array(
			
				'throughout_admin',
			
				'colorbox',
				
				'formbuilder',
				
				'formbuilder-render',
				
				'cqpim-custom-fields-ajax'

			);

			cqpim_custom_enqueue_scripts( $scripts );
			
			$styles = array(
			
				'colorbox-styles',
				
				'jquery-ui-styles',
				
				'formbuilder-styles',
				
				'fontawesome',
				
				'cqpim-admin-styles',

			);

			cqpim_custom_enqueue_styles( $styles );

	}
	
	function enqueue_cqpim_dash_option_scripts() {

		add_action( 'admin_enqueue_scripts', 'enqueue_cqpim_dash_option_scripts_now' );		

	}

	function enqueue_cqpim_dash_option_scripts_now() {

			$scripts = array(
			
				'throughout_admin',
			
				'colorbox',

				'plugin_options_pages_custom',
				
				'plugin_dash_page_custom',
				
				'jquery-timer',
				
				'datatables',
				
				'masonry',
				
				'fullcal-moment',
				
				'fullcal',
				
				'fullcal-locale',
				
				'charts'

			);

			cqpim_custom_enqueue_scripts( $scripts );
			
			$styles = array(
			
				'colorbox-styles',
				
				'datatables-styles',
				
				'fullcal-styles',
				
				'jquery-ui-styles',
				
				'fontawesome',
				
				'cqpim-admin-styles',

			);

			cqpim_custom_enqueue_styles( $styles );

	}
	
	function enqueue_cqpim_dash_messaging_scripts() {

		add_action( 'admin_enqueue_scripts', 'enqueue_cqpim_dash_messaging_scripts_now' );		

	}

	function enqueue_cqpim_dash_messaging_scripts_now() {

		$scripts = array(
		
			'throughout_admin',
		
			'colorbox',
			
			'plugin_dash_messaging_page',
			
			'datatables',
			
			'autocomplete',

		);

		cqpim_custom_enqueue_scripts( $scripts );
		
		$styles = array(
		
			'colorbox-styles',
			
			'datatables-styles',
			
			'jquery-ui-styles',
			
			'autocomplete-styles',
			
			'fontawesome',
			
			'cqpim-admin-styles',

		);

		cqpim_custom_enqueue_styles( $styles );

	}
	
	function cqpim_enqueue_plugin_support_scripts() {

		add_action( 'admin_enqueue_scripts', 'cqpim_enqueue_plugin_support_scripts_now' );		

	}

	function cqpim_enqueue_plugin_support_scripts_now() {

		$scripts = array(
		
			'throughout_admin',

			'support-tickets-custom',
			
			'datatables'

		);

		cqpim_custom_enqueue_scripts( $scripts );
		
		$styles = array(
			
			'datatables-styles',
			
			'jquery-ui-styles',
			
			'fontawesome',
			
			'cqpim-admin-styles',

		);

		cqpim_custom_enqueue_styles( $styles );

	}
	
	function cqpim_enqueue_plugin_permissions_scripts() {

		add_action( 'admin_enqueue_scripts', 'cqpim_enqueue_plugin_permissions_scripts_now' );		

	}

	function cqpim_enqueue_plugin_permissions_scripts_now() {

		$scripts = array(
		
			'throughout_admin',

			'jquery-repeater-perms',
			
			'permissions-custom'

		);

		cqpim_custom_enqueue_scripts( $scripts );
		
		$styles = array(
			
			'fontawesome',
			
			'cqpim-admin-styles',

		);

		cqpim_custom_enqueue_styles( $styles );

	}

	function cqpim_custom_enqueue_scripts( $scripts ){

		if( ! is_array( $scripts ) ){ return; }		
		
		$datatables = array(
		
			'sEmptyTable' => __('No data available in table', 'cqpim'),
			'sInfo' => __('Showing _START_ to _END_ of _TOTAL_', 'cqpim'),
			'sInfoEmpty' => __('Showing 0 to 0 of 0', 'cqpim'),
			'sInfoFiltered' => __('(filtered from _MAX_ total entries)', 'cqpim'),
			'sInfoPostFix' => '',
			'sInfoThousands' => ',',
			'sLengthMenu' => __('Show _MENU_', 'cqpim'),
			'sLoadingRecords' => __('Loading...', 'cqpim'),
			'sProcessing' => __('Processing...', 'cqpim'),
			'sSearch' => __('Search:', 'cqpim'),
			'sZeroRecords' => __('No matching records found', 'cqpim'),

				'sFirst' => __('First', 'cqpim'),
				'sLast' => __('Last', 'cqpim'),
				'sNext' => __('Next', 'cqpim'),
				'sPrevious' => __('Previous', 'cqpim'),

				'sSortAscending' =>  __(': activate to sort column ascending', 'cqpim'),
				'sSortDescending' => __(': activate to sort column descending', 'cqpim'),
	
		);
		
		$cf_alerts = array(
		
			'done' => __('Custom fields updated successfully', 'cqpim'),
			
			'fail' => __('There was a problem updating the fields. Perhaps you didn\'t add any?', 'cqpim')
		
		);
		
		global $wp_locale;
		
		$args = array(
			'closeText'         => __( 'Done', 'cqpim' ),
			'currentText'       => __( 'Today', 'cqpim' ),
			'monthNames'        => array_values( $wp_locale->month ),
			'monthNamesShort'   => array_values( $wp_locale->month_abbrev ),
			'monthStatus'       => __( 'Show a different month', 'cqpim' ),
			'dayNames'          => array_values( $wp_locale->weekday ),
			'dayNamesShort'     => array_values( $wp_locale->weekday_abbrev ),
			'dayNamesMin'       => array_values( $wp_locale->weekday_initial ),
			// set the date format to match the WP general date settings
			'dateFormat'        => date_format_php_to_js( get_option( 'cqpim_date_format' ) ),
			// get the start of week from WP general setting
			'firstDay'          => get_option( 'start_of_week' )
		);	

		foreach ( $scripts as $script ) {

				wp_enqueue_script( $script );
				
				wp_localize_script( $script, 'objectL10n', $args );
				
				wp_localize_script( $script, 'dti18n', $datatables );
				
				wp_localize_script( $script, 'cf_alerts', $cf_alerts );

		}

	}
	
	function cqpim_custom_enqueue_styles( $styles ){

		if( ! is_array( $styles ) ){ return; }

		foreach ( $styles as $style ) {

				wp_enqueue_style( $style );

		}

	}