

	<!DOCTYPE html>
	<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if (gte IE 9)|!(IE)]><!--><html class="ie ie9" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title><?php wp_title(); ?></title>   
		<?php wp_head(); ?>
		<?php echo '<style>' . get_option('cqpim_dash_css') . '</style>'; ?>
	</head>
	
	<?php $bg = get_option('cqpim_dash_bg'); ?>
	
	<?php if(empty($bg)) { ?>

		<body style="background:#f6f6f6; height:100vh">
		
	<?php } else { ?>
	
		<body style="background:url(<?php echo $bg['cqpim_dash_bg']; ?>) center top no-repeat; background-size:cover; height:100vh">
	
	<?php } ?>
	
	<div id="overlay" style="display:none">
	
		<div id="spinner">
		
			<img src="<?php echo plugins_url(  ) . '/cqpim/img/loading_spinner.gif'; ?>" />
		
		</div>
	
	</div>

	<div id="content" role="main">	 
		
		<div class="cqpim-login">
		
			<?php
		
			$logo = get_option('cqpim_dash_logo'); 
			
			if($logo) { ?>
			
				<div style="text-align:center">

					<img style="max-width:100%; margin:20px 0 0" src="<?php echo $logo['cqpim_dash_logo']; ?>" />
				
				</div>

			<?php } ?>
		
			<div class="cqpim-project-box">
				
				<div class="content">
				
					<h3><?php echo $post->post_title; ?></h3>
					
					<form id="cqpim-login">
				
						<?php wp_nonce_field('ajax-register-nonce', 'signonsecurity'); ?> 
					
						<input type="text" id="username" placeholder="<?php _e('Email Address', 'cqpim'); ?>" />
						
						<input type="password" id="password" placeholder="<?php _e('Password', 'cqpim'); ?>" />
						
						<input type="submit" class="cqpim_button font-white bg-dark-blue right op" value="<?php _e('Log In', 'cqpim'); ?>" />
					
						<div class="clear"></div>
					
					</form>
					
					<div class="clear"></div>
					
					<?php $reset = get_option('cqpim_reset_page'); 
					
					if(!empty($reset)) { ?>
					
						<a href="<?php echo get_the_permalink($reset); ?>" id="forgot" class="mt-20 cqpim_button_link cqpim_button font-white bg-grey-cascade"><?php _e('Forgotten Password?', 'cqpim'); ?></a>
					
					<?php } ?>
					
					<?php include_once(ABSPATH.'wp-admin/includes/plugin.php');
					
					if(is_plugin_active('cqpim-envato/cqpim-envato.php')) {
					
						$register_page = get_option('cqpim_envato_register_page'); ?>
						
						<a class="mt-20 cqpim_button_link cqpim_button bg-blue font-white" href="<?php echo get_the_permalink($register_page); ?>"><?php _e('Envato Buyer?', 'cqpim'); ?> <?php _e('Register Here', 'cqpim'); ?></a>
						
					<?php } ?>
					
					<div id="login_messages" style="display:none"></div>
				
				</div>
			
			</div>
		
		</div>
		
	</div>

	<?php wp_footer(); ?>
	</body>
	</html>
	


