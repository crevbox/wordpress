	<!DOCTYPE html>
	<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if (gte IE 9)|!(IE)]><!--><html  class="ie ie9" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title><?php wp_title(); ?></title>   
		<?php wp_head(); ?>
		<?php echo '<style>' . get_option('cqpim_dash_css') . '</style>'; ?>
	</head>
	
	<?php

	$avatar = get_option('cqpim_disable_avatars');

	$user = wp_get_current_user();
	
	$messaging = get_option('cqpim_messages_allow_client');
	
	$unread = cqpim_new_messages($user->ID);
	
	$unread_stat = isset($unread['read_val']) ? $unread['read_val'] : '';
	
	$unread_qty = isset($unread['new_messages']) ? $unread['new_messages'] : '';
				
	?>

	<body style="min-height:700px;">	
	
	<div id="overlay" style="display:none">
	
		<div id="spinner">
		
			<img src="<?php echo plugins_url(  ) . '/cqpim/img/loading_spinner.gif'; ?>" />
		
		</div>
	
	</div>
	
	<div id="cqpim_dashboard_container">

	<div class="cqpim-client-dash" role="main">	
	
		<div id="cqpim_admin_head">
		
			<ul>
			
				<li class="nodesktop cd-menu">
				
					<a class="menu-open" title="<?php _e('Menu', 'cqpim'); ?>" href="#"><img src="<?php echo plugin_dir_url( __FILE__ ) . 'img/menu.png'; ?>" alt="Menu" /></a>
					
					<a style="display:none" class="menu-close" title="<?php _e('Close Menu', 'cqpim'); ?>" href="#"><img src="<?php echo plugin_dir_url( __FILE__ ) . 'img/close.png'; ?>" alt="Menu" /></a>
					
				</li>
			
				<?php
			
				$logo = get_option('cqpim_dash_logo'); 
				
				if($logo) { ?>
				
					<li style="width:190px; padding-right:35px; text-align:center; overflow:hidden; max-height:54px">
					
						<img style="max-width:100%" src="<?php echo $logo['cqpim_dash_logo']; ?>" />			
					
					</li>

				<?php } ?>
						
				<?php if(empty($avatar)) { echo '<li class="cqpim_avatar desktop_items">' . get_avatar( $user->ID, 50, '', false, array('force_display' => true) ) . '</li>'; } else { echo '<li style="height:50px; width:1px;margin-left:-1px">&nbsp;</li>'; } ?>
				
				<li class="desktop_items"><span class="cqpim_username rounded_2"><i class="fa fa-user-circle" aria-hidden="true"></i> <?php echo $user->display_name; ?></span></li>
				
				<li class="desktop_items"><span class="cqpim_role rounded_2"><i class="fa fa-users" aria-hidden="true"></i> <?php echo isset($client_details['client_company']) ? $client_details['client_company'] : ''; ?> <?php if($client_type == 'admin') { echo '&nbsp;' . __('(Main Contact)', 'cqpim'); } ?></span></li>
				
			</ul>
			
			<ul id="cd-head-actions">				
				
				<?php if(!empty($messaging)) { ?>
				
					<li class="desktop_items">
					
						<span class="cqpim_icon">
					
							<a <?php if(!empty($unread_qty)) { echo 'class="cqpim_active"'; } ?> href="<?php echo get_the_permalink($client_dash) . '?page=messages'; ?>"><i class="fa fa-envelope-open cqpim_tooltip" aria-hidden="true" title="<?php _e('Messages', 'cqpim'); ?>"></i></a>
							
							<?php if(!empty($unread_qty)) { ?>
							
								<span class="cqpim_counter"><?php echo $unread_qty; ?></span>
							
							<?php } ?>
						
						</span>
					
					</li>
				
				<?php } ?>
				
				<?php
				
				$client_settings = get_option('allow_client_settings');
				
				if($client_settings == 1) { ?>
				
					<li class="desktop_items">
					
						<span class="cqpim_icon">
					
							<a href="<?php echo get_the_permalink($client_dash) . '?page=settings'; ?>"><i class="fa fa-sliders cqpim_tooltip" aria-hidden="true" title="<?php _e('Edit my Profile', 'cqpim'); ?>"></i></a>
						
						</span>
					
					</li>
				
				<?php }
				
				$client_settings = get_option('allow_client_users');
				
				if($client_settings == 1) { ?>
				
					<li class="contacts desktop_items">
					
						<span class="cqpim_icon">
					
							<a href="<?php echo get_the_permalink($client_dash) . '?page=contacts'; ?>"><i class="fa fa-users cqpim_tooltip" aria-hidden="true" title="<?php _e('Contacts', 'cqpim'); ?>"></i></a>
						
						</span>
					
					</li>
				
				<?php } ?>
				
				<li class="desktop_items">
				
					<span class="cqpim_icon">
				
						<?php
						
							$login_page_id = get_option('cqpim_login_page');
							
							$login_url = get_option('cqpim_logout_url');
							
							if(empty($login_url)) {
							
								$login_url = get_the_permalink($login_page_id);
								
							}
						
						?>
					
						<a href="<?php echo wp_logout_url($login_url); ?>"><i class="fa fa-sign-out cqpim_tooltip" aria-hidden="true" title="<?php _e('Log Out', 'cqpim'); ?>"></i></a>
				
					</span>
				
				</li>
			
			</ul>

			<div class="clear"></div>

		</div>		