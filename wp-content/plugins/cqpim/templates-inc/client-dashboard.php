<?php 

$user = wp_get_current_user();

if (!in_array('cqpim_client', $user->roles)) {

	$login_page = get_option('cqpim_login_page');
	
	$url = get_the_permalink($login_page);
	
	wp_redirect($url, 302);
	
	exit();

} else {
	
	include_once( realpath(dirname(__FILE__)) . '/upload.php' ); 

	$user = wp_get_current_user(); 	
	
	$login_page_id = get_option('cqpim_login_page');
	
	$client_dash = get_option('cqpim_client_page');
	
	$login_url = get_the_permalink($login_page_id);
	
	$user_id = $user->ID;
	
	$dash_type = get_option('client_dashboard_type');
	
	$theme = wp_get_theme();
	
	$quote_form = get_option('cqpim_backend_form');
	
	// Assignment
	
	$args = array(
	
		'post_type' => 'cqpim_client',
		
		'posts_per_page' => -1,
		
		'post_status' => 'private'
	
	);
	
	$members = get_posts($args);
	
	foreach($members as $member) {
	
		$team_details = get_post_meta($member->ID, 'client_details', true);
		
		if($team_details['user_id'] == $user->ID) {
		
			$assigned = $member->ID;
			
			$client_type = 'admin';
		
		}
	
	} 
	
	if(empty($assigned)) {
	
		foreach($members as $member) {
		
			$team_ids = get_post_meta($member->ID, 'client_ids', true);
			
			if(!is_array($team_ids)) {
			
				$team_ids = array($team_ids);
			
			}
			
			if(in_array($user->ID, $team_ids)) {
			
				$assigned = $member->ID;
				
				$client_type = 'contact';
			
			}
		
		} 			
	
	}
	
	$client_details = get_post_meta($assigned, 'client_details', true);
	
	$client_ids = get_post_meta($assigned, 'client_ids', true);
	
	$client_ids_untouched = $client_ids;
	
	if(empty($client_ids_untouched)) {
		
		$client_ids_untouched = array();
		
	}
	
	$login_url = get_option('cqpim_logout_url');
	
	if(empty($login_url)) {
	
		$login_url = get_the_permalink($login_page_id);
		
	}
	
}

// Is this an invoice payment redirect?

		$payment_status = isset($_POST['payment_status']) ? $_POST['payment_status'] : '';	
		
		if($payment_status == 'completed') {
			
			$last = $_SESSION['last_invoice'];
		
			cqpim_mark_invoice_paid($last, 'PayPal', $_SESSION['payment_amount_' . $last]);
			
			$payment = true;
		
		}
		
		$twocheck = isset($_GET['credit_card_processed']) ? $_GET['credit_card_processed'] : '';
		
		if($twocheck == 'Y') {
			
			$last = $_SESSION['last_invoice'];
		
			cqpim_mark_invoice_paid($last, '2Checkout', $_SESSION['payment_amount_' . $last]);
			
			$payment = true;
		
		}
		
		$stripe_token = isset($_POST['stripeToken']) ? $_POST['stripeToken'] : '';
		
		if(!empty($stripe_token)) {
			
			$last = $_SESSION['last_invoice'];
		
			require_once(plugin_dir_path( __FILE__ ) . 'stripe/init.php');
		
			\Stripe\Stripe::setApiKey(get_option('client_invoice_stripe_secret'));

			$token = $_POST['stripeToken'];

			try {
			
				$charge = \Stripe\Charge::create(array(
					"amount" => $_SESSION['payment_amount_' . $last] * 100,
					"currency" => get_option('currency_code'),
					"source" => $token,
					"description" => __('Invoice', 'cqpim') . $post->post_title)
				);
			} catch(\Stripe\Error\Card $e) {
			
			  
			}	
		
			cqpim_mark_invoice_paid($last, 'Stripe', $_SESSION['payment_amount_' . $last]);
			
			$payment = true;
		
		}

?>

	<?php include('header.php'); ?>
	
	<div id="cqpim-dash-sidebar-back"></div>
		
	<div class="cqpim-dash-content">
	
		<div id="cqpim-dash-sidebar">
		
			<?php include('sidebar.php'); ?>
		
		</div>
		
		<div id="cqpim-dash-content">
		
			<div id="cqpim_admin_title">
			
				<?php

					if( isset( $_GET['page'] ) && $_GET['page'] == 'quotes'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Quotes / Estimates', 'cqpim');

					} elseif( isset( $_GET['page'] ) && $_GET['page'] == 'projects'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Projects', 'cqpim');

					} elseif( isset( $_GET['page'] ) && $_GET['page'] == 'support'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Support Tickets', 'cqpim');

					} elseif( isset( $_GET['page'] ) && $_GET['page'] == 'invoices'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Invoices', 'cqpim');

					} elseif( isset( $_GET['page'] ) && $_GET['page'] == 'quote_form'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Request a Quote', 'cqpim');

					} elseif( isset( $_GET['page'] ) && $_GET['page'] == 'settings'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Settings', 'cqpim');

					} elseif( isset( $_GET['page'] ) && $_GET['page'] == 'messages'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Messages', 'cqpim');

					} elseif( isset( $_GET['page'] ) && $_GET['page'] == 'contacts'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Contacts', 'cqpim');

					}  elseif( isset( $_GET['page'] ) && $_GET['page'] == 'add-support-ticket'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('Add Support Ticket', 'cqpim');

					}  elseif( isset( $_GET['page'] ) && $_GET['page'] == 'add-envato-purchase'  ) {

						echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . __('My Envato Items', 'cqpim');

					} elseif(empty($_GET['page'])) {
					
						_e('Dashboard', 'cqpim');
					
					}
					
				?>										
			
			</div>
			
			<div id="cqpim-cdash-inside">
	
				<?php if(!empty($payment) && $payment == true) { ?>
				
					<div class="cqpim-alert cqpim-alert-success alert-display">
					  <strong><?php _e('Payment Successful.', 'cqpim'); ?></strong> <?php _e('Your payment has been accepted, thank you.', 'cqpim'); ?>
					</div>			
				
				<?php } ?>
			
				<?php

				if(isset( $_GET['page'] ) && $_GET['page'] == 'quotes' && get_option('enable_quotes') == 1 ) {
				
					include('client-dashboard/quotes-page.php');

				} if(isset( $_GET['page'] ) && $_GET['page'] == 'projects'  ) {

					include('client-dashboard/projects-page.php');

				} if(isset( $_GET['page'] ) && $_GET['page'] == 'support'  ) {

					include('client-dashboard/ticket-page.php');

				} if(isset( $_GET['page'] ) && $_GET['page'] == 'invoices'  ) {
				
					include('client-dashboard/invoice-page.php');
			
				} if(isset( $_GET['page'] ) && $_GET['page'] == 'quote_form' && !empty($quote_form)  ) {
			
					include('client-dashboard/quote-form.php');
			
				} if(isset( $_GET['page'] ) && $_GET['page'] == 'add-envato-purchase'  ) { 
				
					include('client-dashboard/add-envato-purchase.php');
			
				} if(isset( $_GET['page'] ) && $_GET['page'] == 'add-support-ticket'  ) { 
				
					include('client-dashboard/add-ticket.php');
			
				} if(isset( $_GET['page'] ) && $_GET['page'] == 'settings'  ) {
			
					include('client-dashboard/client-settings.php');
			
				} if(isset( $_GET['page'] ) && $_GET['page'] == 'contacts'  ) {
			
					include('client-dashboard/client-contacts.php');

				} if(isset( $_GET['page'] ) && $_GET['page'] == 'messages'  ) {
			
					include('client-dashboard/messages.php');
						
				} if(empty($_GET['page'])) {
			
					include('client-dashboard/dashboard.php');
				
				} ?>

			</div>			

		</div>
		
		<div class="clear"></div>
	
	</div>
		
	<?php include('footer.php'); ?>
	
	


