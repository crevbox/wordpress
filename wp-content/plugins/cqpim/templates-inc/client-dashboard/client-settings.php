	<br />

	<div class="cqpim_block">
	
	<div class="cqpim_block_title">
	
		<div class="caption">
			
			<i class="fa fa-cog font-green-sharp" aria-hidden="true"></i>
				
			<span class="caption-subject font-green-sharp sbold"> <?php _e('Profile', 'cqpim'); ?></span>
		
		</div>
		
	</div>

	<?php 

	$user = wp_get_current_user();

	$client_logs = get_post_meta($assigned, 'client_logs', true);

	if(empty($client_logs)) {

		$client_logs = array();
		
	}

	$now = time();

	$client_logs[$now] = array(

		'user' => $user->ID,
		
		'page' => __('Client Dashboard Settings Page', 'cqpim')

	);
	
	update_post_meta($assigned, 'client_logs', $client_logs);

	$client_settings = get_option('allow_client_settings');
	
	if($client_settings == 1) { ?>

			<div id="cqpim_backend_quote">
			
				<?php
				
					if($client_type == 'admin') {
											
						$client_details = get_post_meta($assigned, 'client_details', true);
						
					} else {
					
						$client_details = get_post_meta($assigned, 'client_details', true);
					
						$client_contacts = get_post_meta($assigned, 'client_contacts', true);
						
						if(empty($client_contacts)) {
						
							$client_contacts = array();
							
						}
						
						foreach($client_contacts as $key => $contact) {
						
							if($key == $user_id) {
							
								$client_details['client_telephone'] = $contact['telephone'];
								
								$client_details['client_contact'] = $contact['name'];
								
								$client_details['client_email'] = $contact['email'];
							
							}
						
						}
					
					}
				
				?>
		
				<form id="client_settings">
				
					<h4><?php _e('My Details', 'cqpim'); ?></h4>
					
					<label for="client_email"><?php _e('Email Address', 'cqpim'); ?></label>
					
					<input style="width:98%; padding:1%" type="text" id="client_email" name="client_email" value="<?php echo $user->user_email; ?>" required />
					
					<label for="client_phone"><?php _e('Telephone', 'cqpim'); ?></label>
					
					<input style="width:98%; padding:1%" type="text" id="client_phone" name="client_phone" value="<?php echo isset($client_details['client_telephone']) ? $client_details['client_telephone'] : ''; ?>" required />
					
					<label for="client_email"><?php _e('Display Name', 'cqpim'); ?></label>
					
					<input style="width:98%; padding:1%" type="text" id="client_name" name="client_name" value="<?php echo $user->display_name; ?>" required />
					
					<h4 style="margin-top:20px"><?php _e('Company Details', 'cqpim'); ?></h4>
					
					<label for="company_name"><?php _e('Company Name', 'cqpim'); ?></label>
					
					<input style="width:98%; padding:1%" type="text" id="company_name" name="company_name" value="<?php echo isset($client_details['client_company']) ? $client_details['client_company'] : ''; ?>" required />
					
					<label for="company_address"><?php _e('Company Address', 'cqpim'); ?></label>
					
					<textarea style="width:98%; padding:1%; height:100px" id="company_address" name="company_address" required ><?php echo isset($client_details['client_address']) ? $client_details['client_address'] : ''; ?></textarea>

					<label for="company_postcode"><?php _e('Company Postcode', 'cqpim'); ?></label>
					
					<input style="width:98%; padding:1%" type="text" id="company_postcode" name="company_postcode" value="<?php echo isset($client_details['client_postcode']) ? $client_details['client_postcode'] : ''; ?>" required />							
					
					<h4 style="margin-top:20px"><?php _e('Change Password', 'cqpim'); ?></h4>
					
					<label for="client_pass"><?php _e('New Password', 'cqpim'); ?></label>
					
					<input style="width:98%; padding:1%" type="password" id="client_pass" name="client_pass" value="" />
					
					<label for="client_pass_rep"><?php _e('Repeat New Password', 'cqpim'); ?></label>
					
					<input style="width:98%; padding:1%" type="password" id="client_pass_rep" name="client_pass_rep" value=""  />
					
					<h4 style="margin-top:20px"><?php _e('Email Notification Preferences', 'cqpim'); ?></h4>
					
					<p><strong><?php _e('Tasks', 'cqpim'); ?></strong></p>
					
					<?php 
					
					if($client_type == 'admin') { 
					
						$notifications = get_post_meta($assigned, 'client_notifications', true);
					
					} else {
						
						$client_contacts = get_post_meta($assigned, 'client_contacts', true);
						
						$notifications = $client_contacts[$user->ID]['notifications'];
						
					}
					
					$no_tasks = isset($notifications['no_tasks']) ? $notifications['no_tasks']: 0;
					
					$no_tasks_comment = isset($notifications['no_tasks_comment']) ? $notifications['no_tasks_comment']: 0;
					
					$no_tickets = isset($notifications['no_tickets']) ? $notifications['no_tickets']: 0;
					
					$no_tickets_comment = isset($notifications['no_tickets_comment']) ? $notifications['no_tickets_comment']: 0;
					
					?>
					
					<input type="checkbox" name="no_tasks" id="no_tasks" value="1" <?php if($no_tasks == 1) { echo 'checked="checked"'; } ?> /> <?php _e('Disable all task notification emails.', 'cqpim'); ?>
					
					<br />
					
					<input type="checkbox" name="no_tasks_comment" id="no_tasks_comment" value="1" <?php if($no_tasks_comment == 1) { echo 'checked="checked"'; } ?> <?php if($no_tasks == 1) { echo 'disabled'; } ?> /> <?php _e('Only notify me if a task has a new comment added.', 'cqpim'); ?>
					
					<br />
					
					<p><strong><?php _e('Support Tickets', 'cqpim'); ?></strong></p>
					
					<input type="checkbox" name="no_tickets" id="no_tickets" value="1" <?php if($no_tickets == 1) { echo 'checked="checked"'; } ?>  /> <?php _e('Disable all ticket notification emails.', 'cqpim'); ?>
					
					<br />
					
					<input type="checkbox" name="no_tickets_comment" id="no_tickets_comment" value="1" <?php if($no_tickets_comment == 1) { echo 'checked="checked"'; } ?> <?php if($no_tickets == 1) { echo 'disabled'; } ?> /> <?php _e('Only notify me if a ticket has a new comment added.', 'cqpim'); ?>
					
					<br />
					
					<input style="width:100%" type="hidden" id="client_type" name="client_type" value="<?php echo $client_type; ?>" />
					
					<input style="width:100%" type="hidden" id="client_object" name="client_object" value="<?php echo $assigned; ?>" />
					
					<input style="width:100%" type="hidden" id="client_user_id" name="client_user_id" value="<?php echo $user->ID; ?>" />
					
					<br />
					
					<input type="submit" id="client_settings_submit" class="cqpim_button font-white bg-blue rounded_2 op" value="<?php _e('Update Settings', 'cqpim'); ?>" />
				
					<div class="clear"></div>
					
					<br />
					
					<div id="settings_messages"></div>
					
				</form>
			
			</div>
		
		<?php } ?>	
		
	</div>