jQuery(document).ready(function() {
	
	jQuery('.cqpim-file-upload').on('change', function(e) {
		
		e.preventDefault();
		
		var element = jQuery(this);
		
		var id = jQuery(this).attr('id');

		var formData = new FormData();

		formData.append('action', 'upload-attachment');
		
		formData.append('async-upload', jQuery(this)[0].files[0]);
		
		formData.append('name', jQuery(this)[0].files[0].name);
		
		formData.append('_wpnonce', upload_config.nonce);

		jQuery.ajax({
			
			url: upload_config.upload_url,
			
			data: formData,
			
			processData: false,
			
			contentType: false,
			
			dataType: 'json',
			
			type: 'POST',
			
			beforeSend: function() {
				
				jQuery(element).hide();
				
				jQuery('#upload_messages_' + id).show().html('<span class="cqpim-alert cqpim-alert-info cqpim-uploading cqpim-messages-upload">' + upload_config.strings.uploading + '</span>'); 
				
			},
			
			success: function(resp) {
				
				console.log(resp);
				
				if ( resp.success ) {
					
					jQuery('#upload_messages_' + id).show().html('<span class="cqpim-alert cqpim-alert-success cqpim-messages-upload">' + upload_config.strings.success + ' - ' + resp.data.filename + ' <i style="cursor:pointer" class="fa fa-trash btn-change-image" aria-hidden="true" data-id="' + id + '"></i></span>');
					
					jQuery('#upload_' + id).val(resp.data.id);

				} else {
					
					jQuery('#upload_messages_' + id).show().html('<span class="cqpim-alert cqpim-alert-danger cqpim-messages-upload cqpim-upload-error">' + upload_config.strings.error + '</span>');
					
					jQuery(element).show();
					
					jQuery('#upload_' + id).val('');
					
				}
				
				
				
			}
			
		});
		
	});	
	
	jQuery('.btn-change-image').live( 'click', function(e) {
		
		e.preventDefault();
		
		var id = jQuery(this).data('id');
		
		var element = jQuery('#' + id);
		
		jQuery('#upload_messages_' + id).empty().hide();
		
		element.val('').show();
		
		jQuery('#upload_' + id).val('');
		
	});
	
	jQuery('.cqpim-file-upload').on('click', function() {
		
		var id = jQuery(this).data('id');
		
		jQuery(this).val('');
		
		jQuery('#upload_' + id).val('');
		
	});

});