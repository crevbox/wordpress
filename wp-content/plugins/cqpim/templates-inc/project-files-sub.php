<?php

	$user = wp_get_current_user();

	$client_logs = get_post_meta($assigned, 'client_logs', true);

	if(empty($client_logs)) {

		$client_logs = array();
		
	}
	
	$title = get_the_title();
	
	$title = str_replace('Private:', '', $title);

	$now = time();

	$client_logs[$now] = array(

		'user' => $user->ID,
		
		'page' => sprintf(__('Project %1$s - %2$s (Files Page)', 'cqpim'), get_the_ID(), $title)

	);
	
	update_post_meta($assigned, 'client_logs', $client_logs);

?>

	<div class="masonry-grid">
	
		<div class="grid-sizer"></div>
		
		<div class="cqpim-dash-item-full grid-item tasks-box">
		
			<div class="cqpim_block">
			
				<div class="cqpim_block_title">
				
					<div class="caption">
							
						<span class="caption-subject font-green-sharp sbold"><?php _e('Project Files', 'cqpim'); ?></span>
					
					</div>	

				</div>
				
				<?php
				
					$all_attached_files = get_attached_media( '', $post->ID );
					
					$args = array(
					
						'post_type' => 'cqpim_tasks',
						
						'posts_per_page' => -1,
						
						'meta_key' => 'project_id',
						
						'meta_value' => $post->ID
					
					);
					
					$tasks = get_posts($args);
					
					foreach($tasks as $task) {
					
						$args = array(
						
							'post_parent' => $task->ID,
							
							'post_type' => 'attachment',
							
							'numberposts' => -1
						
						);
						
						$children = get_children($args);
						
						foreach($children as $child) {
						
							$all_attached_files[] = $child;
							
						}
					
					}
					
					if(!$all_attached_files) {
					
						echo '<p style="padding:20px">' . __('There are no files uploaded to this project.', 'cqpim') . '</p>';
					
					} else {
					
						echo '<table class="cqpim_table files"><thead><tr>';
						
						echo '<th>' . __('File Name', 'cqpim') . '</th><th>' . __('Related Task', 'cqpim') . '</th><th>' . __('File Type', 'cqpim') . '</th><th>' . __('Uploaded', 'cqpim') . '</th><th>' . __('Uploaded By', 'cqpim') . '</th><th>' . __('Actions', 'cqpim') . '</th>';
						
						echo '</tr></thead><tbody>';
					
						foreach($all_attached_files as $file) {
						
							$file_object = get_post($file->ID);
							
							$url = get_the_permalink($file->ID);
							
							$parent = $file->post_parent;
							
							$parent_title = get_the_title($parent);
							
							$parent_title = str_replace('Protected: ', '', $parent_title);
							
							$parent_url = get_the_permalink($parent);
						
							$user = get_user_by( 'id', $file->post_author );
							
							echo '<tr>';
							
							echo '<td><span class="nodesktop"><strong>' . __('Title', 'cqpim') . '</strong>: </span> <a class="cqpim-link" href="' . $file->guid . '" download="' . $file->post_title . '">' . $file->post_title . '</a></td>';
							
							echo '<td><span class="nodesktop"><strong>' . __('Task', 'cqpim') . '</strong>: </span> <a class="cqpim-link" href="' . $parent_url . '">' . $parent_title . '</a></td>';
							
							echo '<td><span class="nodesktop"><strong>' . __('Type', 'cqpim') . '</strong>: </span> ' . $file->post_mime_type . '</td>';
							
							echo '<td><span class="nodesktop"><strong>' . __('Added', 'cqpim') . '</strong>: </span> ' . $file->post_date . '</td>';
							
							echo '<td><span class="nodesktop"><strong>' . __('Added By', 'cqpim') . '</strong>: </span> ' . $user->display_name . '</td>';
							
							echo '<td><a href="' . $file->guid . '" download="' . $file->post_title . '" class="cqpim_button cqpim_small_button border-green font-green op" value="' . $file->ID . '"><i class="fa fa-download" aria-hidden="true"></i></a></td>';
							
							echo '</tr>';
						
						}
						
						echo '</tbody></table>';
					
					}
					
				?>
				
			</div>
			
		</div>
		
	</div>
