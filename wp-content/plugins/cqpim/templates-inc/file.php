<?php 

if (!current_user_can( 'cqpim_client' )) {

	$login_page = get_option('cqpim_login_page');
	
	$server = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : '';
	
	$protocol = $server == 'on' ? 'https' : 'http';
	
	$request_url = $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	
	$site_url = site_url();
	
	$redirect = str_replace($site_url, '', $request_url);
	
	$url = get_the_permalink($login_page) . '?redirect=' . $redirect;
	
	wp_redirect($url, 302);

} else {

	$user = wp_get_current_user(); 
	
	$login_page_id = get_option('cqpim_login_page');
	
	$client_dash = get_option('cqpim_client_page');
	
	$login_url = get_the_permalink($login_page_id);
	
	$user_id = $user->ID;
	
	$dash_type = get_option('client_dashboard_type');
	
	$quote_form = get_option('cqpim_backend_form');
	
	// Assignment
	
	$args = array(
	
		'post_type' => 'cqpim_client',
		
		'posts_per_page' => -1,
		
		'post_status' => 'private'
	
	);
	
	$members = get_posts($args);
	
	foreach($members as $member) {
	
		$team_details = get_post_meta($member->ID, 'client_details', true);
		
		if($team_details['user_id'] == $user->ID) {
		
			$assigned = $member->ID;
			
			$client_type = 'admin';
		
		}
	
	} 
	
	if(empty($assigned)) {
	
		foreach($members as $member) {
		
			$team_ids = get_post_meta($member->ID, 'client_ids', true);
			
			if(!is_array($team_ids)) {
			
				$team_ids = array($team_ids);
			
			}
			
			if(in_array($user->ID, $team_ids)) {
			
				$assigned = $member->ID;
				
				$client_type = 'contact';
			
			}
		
		} 			
	
	}
	
	$client_details = get_post_meta($assigned, 'client_details', true);
	
	$client_ids = get_post_meta($assigned, 'client_ids', true);
	
	$parent_task = $post->post_parent;
	
	$parent_object = get_post($parent_task);
	
	$ppid = get_post_meta($parent_task, 'project_id', true);
	
	$project_details = get_post_meta($ppid, 'project_details', true);
	
	$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
	
	if($parent_object->post_type == 'cqpim_support') {
	
		// Assignment

		$args = array(

			'post_type' => 'cqpim_client',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'

		);

		$members = get_posts($args);

		foreach($members as $member) {

			$team_details = get_post_meta($member->ID, 'client_details', true);
			
			if($team_details['user_id'] == $parent_object->post_author) {
			
				$client_id = $member->ID;
				
				$client_type = 'admin';
			
			}

		} 

		if(empty($client_id)) {

			foreach($members as $member) {
			
				$team_ids = get_post_meta($member->ID, 'client_ids', true);
				
				if(in_array($parent_object->post_author, $team_ids)) {
				
					$client_id = $member->ID;
					
					$client_type = 'contact';
				
				}
			
			} 			

		}		
	
	}
	
}

?>

	<?php include('header.php'); ?>
	
	<div id="cqpim-dash-sidebar-back"></div>
		
	<div class="cqpim-dash-content">
	
		<div id="cqpim-dash-sidebar">
		
			<?php include('sidebar.php'); ?>
		
		</div>
		
		<div id="cqpim-dash-content">
		
				<div id="cqpim_admin_title">
				
				<?php if($assigned == $client_id) {
				
					$title = get_the_title(); $title = str_replace('Protected:', '', $title); echo $title; 
					
				} else {
				
					_e('ACCESS DENIED', 'cqpim');
					
				}
				
				?>	

			</div>
			
			<div id="cqpim-cdash-inside">
		
				<?php

					if($assigned == $client_id) { 
					
						if($post->post_mime_type == 'application/pdf') { ?>
					
						<iframe src="http://docs.google.com/gview?url=<?php echo wp_get_attachment_url($post->ID) ?>&embedded=true" class="cqpim_attachment_pdf" frameborder="0"></iframe> 
						
						<?php } elseif($post->post_mime_type == 'image/jpeg' || $post->post_mime_type == 'image/png' || $post->post_mime_type == 'image/gif') { ?>
						
							<div style="text-align:center">
						
								<img style="max-width:100%" src="<?php echo $post->guid; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
							
							</div>
							
						<?php }	else {

							the_content();
							
						}
						
					} else {
					
						echo '<h1 style="margin-top:0">' . __('ACCESS DENIED', 'cqpim') . '</h1>';
						
					}
				
				 ?>			

			</div><!-- #content -->
			
		</div>
		
	</div>
	
<?php include('footer.php'); ?>



