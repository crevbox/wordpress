<?php 

if (!current_user_can( 'cqpim_client' )) {

	$login_page = get_option('cqpim_login_page');
	
	$url = get_the_permalink($login_page);
	
	wp_redirect($url, 302);

} else {

	$user = wp_get_current_user(); 
	
	$login_page_id = get_option('cqpim_login_page');
	
	$client_dash = get_option('cqpim_client_page');
	
	$login_url = get_the_permalink($login_page_id);
	
	$user_id = $user->ID;
	
	// Gravatar
	
	$email = $user->user_email;
	
	$size = 60;
	
	$grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "&s=" . $size;
	
	$dash_type = get_option('client_dashboard_type');
	
	$quote_form = get_option('cqpim_backend_form');
	
	// Assignment
	
	$args = array(
	
		'post_type' => 'cqpim_client',
		
		'posts_per_page' => -1,
		
		'post_status' => 'private'
	
	);
	
	$members = get_posts($args);
	
	foreach($members as $member) {
	
		$team_details = get_post_meta($member->ID, 'client_details', true);
		
		if($team_details['user_id'] == $user->ID) {
		
			$assigned = $member->ID;
			
			$client_type = 'admin';
		
		}
	
	} 
	
	if(empty($assigned)) {
	
		foreach($members as $member) {
		
			$team_ids = get_post_meta($member->ID, 'client_ids', true);
			
			if(!is_array($team_ids)) {
			
				$team_ids = array($team_ids);
			
			}
			
			if(in_array($user->ID, $team_ids)) {
			
				$assigned = $member->ID;
				
				$client_type = 'contact';
			
			}
		
		} 			
	
	}
	
	$client_details = get_post_meta($assigned, 'client_details', true);

	$client_ids = get_post_meta($assigned, 'client_ids', true);
	
	$login_url = get_option('cqpim_logout_url');
	
	if(empty($login_url)) {
	
		$login_url = get_the_permalink($login_page_id);
		
	}
	
}

?>

	<?php include('header.php'); ?>
	
	<div id="cqpim-dash-sidebar-back"></div>
		
	<div class="cqpim-dash-content">
	
		<div id="cqpim-dash-sidebar">
		
			<?php include('sidebar.php'); ?>
		
		</div>
		
		<div id="cqpim-dash-content">
		
			<div id="cqpim_admin_title">
		
				<?php $title = get_the_title(); $title = str_replace('Private:', '', $title); echo '<a href="' . get_the_permalink($client_dash) . '">' . __('Dashboard', 'cqpim') . '</a> <i class="fa fa-circle"></i> <a href="' . get_the_permalink($client_dash) . '?page=projects">' . __('Projects', 'cqpim') . '</a> <i class="fa fa-circle"></i> ' . $title; ?>							
		
			</div>
			
			<div id="cqpim-cdash-inside">

				<?php
			
				$dash_page = get_option('cqpim_client_page');
			
				$dash_url = get_the_permalink($dash_page);
			
				$user = wp_get_current_user(); 
						
				$user_id = $user->ID;		
				
				$logo = get_option('company_logo');
						
				$logo_url = isset($logo['company_logo']) ? $logo['company_logo'] : '';
				
				$title = get_the_title();
						
				$title = str_replace('Private: ', '', $title);
				
				$company_name = get_option('company_name');
				
				$company_address = get_option('company_address');
				
				$company_postcode = get_option('company_postcode');
				
				$contract_text = get_option('default_contract_text');
				
				$currency = get_option('currency_symbol');
						
				$vat = get_option('sales_tax_rate');
						
				$invoice_terms = get_option('company_invoice_terms');
				
				$tax_name = get_option('sales_tax_name');
						
				if($vat) {
						
					$vat_string = '+' . $tax_name;
							
				} else {
						
					$vat_string = '';
							
				}
			
				$project_details = get_post_meta($post->ID, 'project_details', true);
				
				$project_elements = get_post_meta($post->ID, 'project_elements', true);
				
				$project_progress = get_post_meta($post->ID, 'project_progress', true);
				
				$type = isset($project_details['quote_type']) ? $project_details['quote_type'] : '';
						
				$upper_type = ucfirst($type);
				
				$quote_id = isset($project_details['quote_id']) ? $project_details['quote_id'] : '';
				
				$quote_details = get_post_meta($quote_id, 'quote_details', true);
				
				$project_summary = isset($quote_details['quote_summary']) ? $quote_details['quote_summary'] : '';
				
				$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
				
				$client_details = get_post_meta($client_id, 'client_details', true);
				
				$project_client_ids = get_post_meta($client_id, 'client_ids', true);
				
				if(empty($project_client_ids)) {
					
					$project_client_ids = array();
					
				}
				
				$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
				
				$client_address = isset($client_details['client_address']) ? $client_details['client_address'] : '';
				
				$client_postcode = isset($client_details['client_postcode']) ? $client_details['client_postcode'] : '';
				
				$client_company_name = isset($client_details['client_company']) ? $client_details['client_company'] : '';
				
				$start_date = isset($project_details['start_date']) ? $project_details['start_date'] : '';

				$finish_date = isset($project_details['finish_date']) ? $project_details['finish_date'] : '';
				
				$deposit = isset($project_details['deposit_amount']) ? $project_details['deposit_amount'] : '';

				$confirmed = isset($project_details['confirmed']) ? $project_details['confirmed'] : '';
				
				$pm_name = isset($project_details['pm_name']) ? $project_details['pm_name'] : '';
				
				$sent = isset($project_details['sent']) ? $project_details['sent'] : '';

				$deposit_invoice_id = isset($project_details['deposit_invoice_id']) ? $project_details['deposit_invoice_id'] : '';

				$signoff = isset($project_details['signoff']) ? $project_details['signoff'] : '';
				
				$completion_invoice_id = isset($project_details['completion_invoice_id']) ? $project_details['completion_invoice_id'] : '';
				
				$deposit_invoice_details = get_post_meta($deposit_invoice_id, 'invoice_details', true);
				
				$completion_invoice_details = get_post_meta($completion_invoice_id, 'invoice_details', true);
				
				$deposit_sent = isset($deposit_invoice_details['sent']) ? $deposit_invoice_details['sent'] : '';

				$deposit_paid = isset($deposit_invoice_details['paid']) ? $deposit_invoice_details['paid'] : '';
				
				$completion_sent = isset($completion_invoice_details['sent']) ? $completion_invoice_details['sent'] : '';

				$completion_paid = isset($completion_invoice_details['paid']) ? $completion_invoice_details['paid'] : '';
				
				$closed = isset($project_details['closed']) ? $project_details['closed'] : '';
				
				if($signoff) {
				
					$status = __('Signed Off / Completed', 'cqpim');
					
				} else {
				
					if($confirmed) {
					
						$status = __('Contract Signed', 'cqpim');
					
					} else {
					
						if($sent) {
						
							$status = __('Contract Sent', 'cqpim');
						
						} else {
						
							$status = __('Contract Not Sent', 'cqpim');
						
						}
					
					}
				
				}
				
				if(!is_numeric($finish_date)) {
				
					$str_finish_date = str_replace('/','-', $finish_date);
					
					$unix_finish_date = strtotime($str_finish_date);
				
				} else {
				
					$unix_finish_date = $finish_date;
				
				}
				
				$current_date = time();
				
				$days_to_due = round(abs($current_date - $unix_finish_date) / 86400);				
				
				$task_count = 0;
				
				$task_total_count = 0;
				
				$task_complete_count = 0;
				
				if(empty($project_elements)) {
				
					$project_elements = array();
					
				}
				
				foreach ($project_elements as $element) {
				
					$args = array(
					
						'post_type' => 'cqpim_tasks',
						
						'posts_per_page' => -1,
						
						'meta_key' => 'milestone_id',
						
						'meta_value' => $element['id'],
						
						'orderby' => 'date',
						
						'order' => 'ASC'
						
					);
					
					$tasks = get_posts($args);	
					
					foreach($tasks as $task) {
					
						$task_total_count++;
					
						$task_details = get_post_meta($task->ID, 'task_details', true);
						
						if($task_details['status'] != 'complete') {
						
							$task_count++;
						
						}
						
						if($task_details['status'] == 'complete') {
						
							$task_complete_count++;
						
						}
					
					}
				
				}
				
				if($task_total_count != 0) {
				
					$pc_per_task = 100 / $task_total_count;
				
					$pc_complete = $pc_per_task * $task_complete_count;
				
				} else {
				
					$pc_complete = 0;
				
				}
				
				?>
			
				<?php if(current_user_can( 'cqpim_view_project_client_page' ) OR $client_user_id == $user_id OR in_array($user->ID, $project_client_ids)) { ?>
				
				<?php if($closed) { ?>
				
					<div class="cqpim-alert cqpim-alert-info alert-display">
										
						<?php _e('THIS PROJECT HAS BEEN CLOSED', 'cqpim'); ?>
								
					</div>	
				
				<?php } ?>
					
				<?php 
				
					// Summary Page
					
					if( isset( $_GET['sub'] ) && $_GET['sub'] == 'info'  ) {

						include( 'project-info-sub.php' );

					} 
				
					// Summary Page
					
					if( isset( $_GET['sub'] ) && $_GET['sub'] == 'updates'  ) {

						include( 'project-updates-sub.php' );

					} 
					
					// Messages Page
					
					if( isset( $_GET['sub'] ) && $_GET['sub'] == 'messages'  ) {

						include( 'project-messages-sub.php' );

					} 
					
					// Files Page
					
					if( isset( $_GET['sub'] ) && $_GET['sub'] == 'files'  ) {

						include( 'project-files-sub.php' );

					} 
					
					// Milestones & Tasks Page
					
					if( isset( $_GET['sub'] ) && $_GET['sub'] == 'milestones'  ) {

						include( 'project-milestones-sub.php' );

					}
					
					// Costs & Invoices Page
					
					if( isset( $_GET['sub'] ) && $_GET['sub'] == 'invoices'  ) {

						include( 'project-invoices-sub.php' );

					}
				
				
				?>

				<?php } else { ?>
				
					<h1><?php _e('Access Denied', 'cqpim'); ?></h1>
				
				<?php } ?>	
		
			</div>
			
		</div>
		
	</div>	

<?php include('footer.php'); ?>
	
<?php exit; ?>
	


