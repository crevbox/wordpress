<?php
/**
 * 	TC+ Custom Fields
 *
 */
	
	// Add CQPIM Custom Fields page

	add_action( 'admin_menu' , 'register_cqpim_custom_fields_page', 29 ); 

	function register_cqpim_custom_fields_page() {

		$mypage = add_submenu_page(	
					'cqpim-dashboard',
					__('Custom Fields', 'cqpim'),		
					__('Custom Fields', 'cqpim'), 			
					'edit_cqpim_settings', 			
					'cqpim-custom-fields', 		
					'cqpim_custom_fields'
		);

		add_action( 'load-' . $mypage, 'cqpim_enqueue_cqpim_custom_fields_scripts' );
		


	}


	function cqpim_custom_fields() {
	
		$post_type = isset($_SESSION['cqpim_cf_post_type']) ? $_SESSION['cqpim_cf_post_type'] : 'support'; ?>
		
		<br />
		
		<div class="cqpim-dash-item-full tasks-box" style="padding-right:10px">
					
			<div class="cqpim-dash-item-inside">
		
				<p>
				
					<?php _e('Currently managing:', 'cqpim'); ?> 
			
					<select id="post_type">
					
						<option value="support" <?php if($post_type == 'support') { echo 'selected="selected"'; } ?>><?php _e('Support Tickets', 'cqpim'); ?></option>
					
					</select>
					
					<span class="ajax_spinner" style="display:none"></span>
					
				</p>
				
				<?php
				
				$data = get_option('cqpim_custom_fields_' . $post_type);	
				
				if(!empty($data)) {	
				
					$builder = $data;		
					
				} else {
				
					$builder = '';
					
				}
				
				?>
				
				<script>
				
					jQuery(document).ready(function() {
					
						// form Builder
						
						var options = {
							
							editOnAdd: false,
							
							fieldRemoveWarn: true,
						
							disableFields: ['autocomplete', 'button', 'file', 'hidden', 'checkbox', 'paragraph'],
							
							formData : "<?php echo $builder; ?>",
							
							dataType: 'json',
							
						};
						
						jQuery('#form_builder_container').formBuilder(options);
						
						var $fbEditor = jQuery(document.getElementById('form_builder_container'));
						  
						var formBuilder2 = $fbEditor.data('formBuilder');
						  
						jQuery(".form-builder-save").click(function(e) {
						  
							e.preventDefault();
							
							jQuery('#builder_data').val(formBuilder2.formData.replace(/("[^"]*")|\s/g, "$1"));
							
							cqpim_save_custom_fields();
							
						});
						
					});
				
				</script>
				
				<div id="form_builder_container">
				

				
				</div>
				
				<input type="hidden" id="post_type" value="<?php echo $post_type; ?>" />
				
				<textarea style="display:none" name="builder_data" id="builder_data"><?php if(!empty($builder)) { echo $builder; } else { echo ''; } ?></textarea>
				
			</div>
			
		</div>
			
	<?php }