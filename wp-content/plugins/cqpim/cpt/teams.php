<?php
/**
 *  Clients CPT
 */ 
if ( ! function_exists('cqpim_teams_cpt') ) {
	// Register Custom Post Type
	function cqpim_teams_cpt() {
	
		// Create new Team perms
 
		if(current_user_can('cqpim_create_new_team') && current_user_can('publish_cqpim_teams')) {
		 
			$team_caps = array();
		 
		} else {
		
			$team_caps = array('create_posts' => false);
			
		}
		
		$labels = array(
			'name'                => _x( 'Team Members', 'Post Type General Name', 'cqpim' ),
			'singular_name'       => _x( 'Team Member', 'Post Type Singular Name', 'cqpim' ),
			'menu_name'           => __( 'Team Members', 'cqpim' ),
			'parent_item_colon'   => __( 'Parent Team Member:', 'cqpim' ),
			'all_items'           => __( 'Team Members', 'cqpim' ),
			'view_item'           => __( 'View Team Member', 'cqpim' ),
			'add_new_item'        => __( 'Add New Team Member', 'cqpim' ),
			'add_new'             => __( 'New Team Member', 'cqpim' ),
			'edit_item'           => __( 'Edit Team Member', 'cqpim' ),
			'update_item'         => __( 'Update Team Member', 'cqpim' ),
			'search_items'        => __( 'Search Team Members', 'cqpim' ),
			'not_found'           => __( 'No Team Members found', 'cqpim' ),
			'not_found_in_trash'  => __( 'No Team Members found in trash', 'cqpim' ),
		);
		$args = array(
			'label'               => __( 'team', 'cqpim' ),
			'description'         => __( 'Team Members', 'cqpim' ),
			'labels'              => $labels,
			'supports'            => array( 'title' ),
			'hierarchical'        => false,
			'capabilities'        => $team_caps,
			'public'              => false,
			'show_ui'             => true,
			'show_in_nav_menus'   => false,			'show_in_menu'		  => 'cqpim-dashboard',	
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => plugins_url() . "/img/contact.png",
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => array('cqpim_team', 'cqpim_teams'),
			'map_meta_cap'        => true,
			'query_var'			  => true,
		);

		register_post_type( 'cqpim_teams', $args );

	}
	add_action( 'init', 'cqpim_teams_cpt', 15 );
}


/**

 *	Edit 'teams' cpt columns

 */
 

if ( ! function_exists( 'teams_cpt_custom_columns' )){

	function teams_cpt_custom_columns( $columns ) {

		$new_columns = array(

			'title' 			=> __('Title', 'cqpim'),
			
			'user_account'      => __('Associated User', 'cqpim'),

			'contact_details'	=> __('Contact Details', 'cqpim')

		);
		
		unset($columns['date']);

	    return array_merge( $columns, $new_columns );

	}

	add_filter('manage_cqpim_teams_posts_columns' , 'teams_cpt_custom_columns', 10, 1 );

}


/**

 *	Content for 'client' cpt columns defined above

 */



add_action( 'manage_cqpim_teams_posts_custom_column', 'content_teams_cpt_columns', 10, 2 );

function content_teams_cpt_columns( $column, $post_id ) {

	global $post;

	$team_details = get_post_meta( $post_id, 'team_details', true );

	switch( $column ) {
		
		case 'user_account' :

			$user_id = isset($team_details['user_id']) ? $team_details['user_id'] : '';
			
			$user_object = get_user_by('id', $user_id);
			
			$user_edit_link = get_edit_user_link( $user_id );
			
			echo '<strong>' . _e('User ID:', 'cqpim') . ' </strong>' . $user_object->ID . '<br />';
			
			echo '<strong>' . _e('Login:', 'cqpim') . ' </strong>' . $user_object->user_email . '</strong><br />';			

		break;

		case 'contact_details' :

			$company_contact = isset($team_details['team_name']) ? $team_details['team_name'] : '';
			
			$company_email = isset($team_details['team_email']) ? $team_details['team_email'] : '';
			
			$company_telephone = isset($team_details['team_telephone']) ? $team_details['team_telephone'] : '';
			
			echo '<strong>' . _e('Contact Name:', 'cqpim') . ' </strong>' . $company_contact . '<br />';
			
			echo '<strong>' . _e('Email:', 'cqpim') . ' </strong>' . $company_email . '<br />';
			
			echo '<strong>' . _e('Telephone:', 'cqpim') . ' </strong>' . $company_telephone . '<br />';

		break;

		default :

			break;

	}

} 

// Set classes if user cannot edit



function cqpim_post_classes($classes) {

	if(is_admin()) {

		global $post;
		
		if($post->post_type == 'cqpim_project') {
		
			$user = wp_get_current_user();
			
			$args = array(
			
				'post_type' => 'cqpim_teams',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$members = get_posts($args);
			
			foreach($members as $member) {
			
				$team_details = get_post_meta($member->ID, 'team_details', true);
				
				if($team_details['user_id'] == $user->ID) {
				
					$assigned = $member->ID;
				
				}
			
			}
			
			if(!current_user_can('cqpim_view_all_projects')) {
			
				$project_contributors = get_post_meta($post->ID, 'project_contributors', true);
				
				if(empty($project_contributors)) {
				
					$project_contributors = array();
				
				}
				
				foreach($project_contributors as $contributor) {
					
					if($assigned == $contributor['team_id']) {
						
						$access = true;
						
					}
					
				}
				
				if(empty($access)) {
				
					$classes[] = 'no_access';
				
				} else {
					
					$classes[] = 'can_access';
					
				}
			
			} else {
			
				$classes[] = 'can_access';
				
			}
		
		}
	
	}
	
	return $classes;
	
}

if(is_admin()) {

	add_filter('post_class', 'cqpim_post_classes'); 

}

