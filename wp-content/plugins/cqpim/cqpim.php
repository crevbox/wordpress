<?php
/**
* Plugin Name: CQPIM Project Management
* Description: CQPIM is a solution for freelancers and small agencies who want to manage their clients, quotes, projects and invoices more efficiently. Work individually or as part of a team, and start streamlining your processes!
* Version: 3.2.1
* Author: TimelessInteractive+
* Author URI: http://www.cqpim.uk
* Text Domain: cqpim
* Domain Path: /languages
*/

	define('CQPIM_VERSION', '3.2.1');

	update_option('cqpim_3', true);
	
	add_action( 'admin_menu' , 'register_cqpim_dashboard', 9 ); 

	function register_cqpim_dashboard() {

		$mypage = add_menu_page(__('My Dashboard', 'cqpim'), 'CQPIM', 'cqpim_view_dashboard', 'cqpim-dashboard', 'cqpim_dashboard', '', 28);

		add_action( 'load-' . $mypage, 'enqueue_cqpim_dash_option_scripts' );

	}

	// Theme Compatibilities

	function cqpim_theme_compat() {

		$theme = wp_get_theme();
		
		$dash_page = get_option('cqpim_client_page');
		
		$login_page = get_option('cqpim_login_page');
		
		if(is_page($login_page) || is_page($dash_page) || is_singular('cqpim_project') || is_singular('cqpim_quote') || is_singular('cqpim_support') || is_singular('cqpim_invoice')) {
			
			wp_dequeue_script('x-cranium-migration-head');
			
			wp_dequeue_script('x-site-head');
		
		}
	
	}
	
	add_action( 'wp_print_scripts', 'cqpim_theme_compat', 100 );

	$dashboard = get_option('client_dashboard_type');
	
	if(empty($dashboard)) {
	
		update_option('client_dashboard_type', 'inc');
	
	}

	// Languages
	
	function cqpim_plugin_setup() {
	
		load_plugin_textdomain('cqpim', false, dirname(plugin_basename(__FILE__)) . '/languages/admin/');
		
		load_plugin_textdomain('cqpim', false, dirname(plugin_basename(__FILE__)) . '/languages/frontend/');
		
	} 
	
	add_action('plugins_loaded', 'cqpim_plugin_setup');

	// Include Required Files
 
	// Main functions
 
	require_once( 'cqpim_functions.php' );
	
	require_once( 'cqpim_admin.php' );
	
	// Admin Dashboard & Settings
	
	require_once( 'plugin-settings.php' );
	
	require_once( 'dashboard.php' );
	
	require_once( 'calendar.php' );
	
	//require_once( 'team-profile.php' );
	
	require_once( 'custom-fields.php' );
	
	$value = get_option('cqpim_enable_messaging');
	
	if($value == 1) {
	
		require_once( 'messages.php' );
	
	}
	
	require_once( 'cqpim_messaging_functions.php' );
	
	require_once( 'tasks.php' );
	
	require_once( 'tasks_admin.php' );
	
	require_once( 'support-tickets.php' );
	
	require_once( 'capabilities.php' );
	
	
	
	// CPT's
	
	require_once( 'cpt/teams.php' );
	
	require_once( 'cpt/clients.php' );
	
	if(get_option('enable_quotes') == 1) {
	
		require_once( 'cpt/quotes.php' );
	
		require_once( 'cpt/forms.php' );
	
	}
	
	require_once( 'cpt/projects.php' );
	
	require_once( 'cpt/terms.php' );
	
	if(get_option('disable_invoices') != 1) {
	
		require_once( 'cpt/invoices.php' );
	
	}
	
	require_once( 'cpt/tasks.php' );
	
	require_once( 'cpt/support.php' );
	
	require_once( 'cpt/templates.php' );
	
	require_once( 'cpt/messages.php' );
	
	
	// Metaboxes
	
	require_once( 'metaboxes/index.php' );
	
	// Shortcodes
	
	require_once( 'shortcodes/shortcodes.php' );
	
	// Script Enqueing
	
	require_once( 'script-enqueing.php' );
	
	// Add custom event scheduler
	
	function cqpim_schedule_minute( $schedules ) {
	 
		$schedules['every_minute'] = array(
		
				'interval'  => 60,
				
				'display'   => __( 'Every Minute', 'cqpim' )
				
		);
		 
		return $schedules;
		
	}
	
	add_filter( 'cron_schedules', 'cqpim_schedule_minute' );
	
	add_action('admin_init', 'cqpim_anon_upload');
	
	function cqpim_anon_upload() {
		
		if(!username_exists( 'cqpimuploader' )) {
			
			$userdata = array(
			
				'user_login'  =>  'cqpimuploader',

				'user_pass'   =>  cqpim_random_string(),
				
				'role' => 'cqpimuploader',
				
			);

			$user_id = wp_insert_user( $userdata );	

			$user = get_user_by('id', $user_id);
			
		}
		
	}
	
	add_action('wp', 'cqpim_signon_uploader');
	
	function cqpim_signon_uploader() {	
	
		$form_page = get_option('cqpim_form_page');
		
		if(empty($form_page)) {
		
			update_option('cqpim_form_page', 1000000, true);
			
		}
		
		global $post;
		
		if(!empty($post->ID) && $post->ID == $form_page) {
		
			$pass = cqpim_random_string();

			if(!is_user_logged_in()) {
				
				$username = 'cqpimuploader';
				
				$user = get_user_by('login', $username );
				
				wp_set_password( $pass, $user->ID );
				
				$creds = array();
				
				$creds['user_login'] = $username;
				
				$creds['user_password'] = $pass;
				
				$creds['remember'] = false;
				
				$signon = wp_signon($creds, false);
				
				wp_redirect(get_the_permalink($post->ID));

			}	
		
		} else {
			
			$user = wp_get_current_user();
			
			if(!empty($user->roles) && in_array('cqpimuploader', $user->roles)) {
				
				wp_logout();
				
				wp_redirect(get_the_permalink($post->ID));
				
			}
			
		}
	
	}
	
	
	// Plugin Activation	
	
	register_activation_hook( __FILE__, 'cqpim_activation' );
	
	function cqpim_activation() {
   
		$upload = wp_upload_dir();
		$upload_dir = $upload['basedir'];
		$upload_dir = $upload_dir . '/cqpim-uploads';
		if (! is_dir($upload_dir)) {
		   mkdir( $upload_dir, 0775 );
		}
	
		// Set up hourly checking of recurring invoices
		
		wp_schedule_event(time(), 'hourly', 'cqpim_check_recurring_invoices');
		
		// set up minute checking of email piping
		
		wp_schedule_event(time(), 'every_minute', 'cqpim_check_email_pipe');
	
		// Add custom client role 
		
		$role = get_role('cqpim_client');
		
		if(empty($role)) {
		 
			$result = add_role( 'cqpim_client', __( 'CQPIM Client', 'cqpim' ),
			 
				array(
			 
					'read' => true,
					'read_cqpim_quotes' => true,
					'read_cqpim_projects' => true,
					'read_cqpim_supports' => true,
					'read_private_cqpim_quotes' => true,
					'read_private_cqpim_projects' => true,
					'read_private_cqpim_supports' => true,
					'upload_files' => true,
			 
				)
			 
			);
			
		}
		
		$role = get_role('cqpim_admin');
		
		if(empty($role)) {		
			
			$result = add_role( 'cqpim_admin', __( 'CQPIM Admin', 'cqpim' ),
			 
				array(
			 
					'read' => true,
					
					'upload_files' => true,
			 
				)
			 
			);
			
			$cqpim_roles = array('cqpim_admin', 'user');
			
			update_option('cqpim_roles', $cqpim_roles);
		
		}
		
		$role = get_role('cqpimuploader');
		
		if(empty($role)) {		
			
			$result = add_role( 'cqpimuploader', __( 'CQPIM Uploader', 'cqpim' ),
			 
				array(
			 
					'read' => true,
					
					'upload_files' => true,
			 
				)
			 
			);
		
		}
	
		// Create the CQPIM Client Login Page

		$new_page_title = 'CQPIM Login';
		
		$new_page_content = '';
		
		$new_page_template = '';

		$page_check = get_page_by_title($new_page_title);
		
		$new_page = array(
		
				'post_type' => 'page',
				
				'post_title' => $new_page_title,
				
				'post_content' => $new_page_content,
				
				'post_excerpt' => '',
				
				'post_status' => 'publish',
				
				'post_author' => 1,
				
				'post_name' => 'cqpim-login',
				
				'comment_status' => 'closed'
				
		);
		
		if(!isset($page_check->ID)){
		
				$new_page_id = wp_insert_post($new_page);
				
				update_option( 'cqpim_login_page', $new_page_id, true );
				
				if(!empty($new_page_template)){
				
						update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
						
				}
				
		}

	// Create the CQPIM Client Dashboard Page

		$new_page_title = 'CQPIM Client Dashboard';
		
		$new_page_content = '';
		
		$new_page_template = '';

		$page_check = get_page_by_title($new_page_title);
		
		$new_page = array(
		
				'post_type' => 'page',
				
				'post_title' => $new_page_title,
				
				'post_content' => $new_page_content,
				
				'post_status' => 'publish',
				
				'post_author' => 1,
				
				'post_name' => 'cqpim-client',
				
				'post_excerpt' => '',
				
				'comment_status' => 'closed'
				
		);
		
		if(!isset($page_check->ID)){
		
				$new_page_id = wp_insert_post($new_page);
				
				update_option( 'cqpim_client_page', $new_page_id, true );
				
				if(!empty($new_page_template)){
				
						update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
						
				}
				
		}
		
			$new_page_title = 'CQPIM Password Reset';
			
			$new_page_content = '';
			
			$new_page_template = '';

			$page_check = get_page_by_title($new_page_title);
			
			$new_page = array(
			
					'post_type' => 'page',
					
					'post_title' => $new_page_title,
					
					'post_content' => $new_page_content,
					
					'post_excerpt' => '',
					
					'post_status' => 'publish',
					
					'post_author' => 1,
					
					'post_name' => 'cqpim-reset',
					
					'comment_status' => 'closed'
					
			);
			
			if(!isset($page_check->ID)){
			
					$new_page_id = wp_insert_post($new_page);
					
					update_option( 'cqpim_reset_page', $new_page_id, true );
					
					if(!empty($new_page_template)){
					
							update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
							
					}
					
			}
		
		update_option('client_dashboard_type', 'inc');
		
		//cqpim_set_options();
	
	}
	
	// Plugin deactivation
	
	register_deactivation_hook( __FILE__, 'cqpim_deactivation' );
	
	function cqpim_deactivation() {
		
		$client_login = get_option('cqpim_login_page');
		
		$client_dash = get_option('cqpim_client_page');
		
		$client_reset = get_option('cqpim_reset_page');
		
		wp_delete_post($client_login, true);
		
		wp_delete_post($client_reset, true);
		
		wp_delete_post($client_dash, true);
		
		wp_clear_scheduled_hook('cqpim_check_recurring_invoices');
		
		wp_clear_scheduled_hook('cqpim_check_email_pipe');
		
		// Create custom roles add in plugin

		$roles = get_option('cqpim_roles'); 

		if(!is_array($roles)) {

			$roles = array(get_option('cqpim_roles'));

		}

		foreach($roles as $role) {

			$cqpim_role = 'cqpim_' . $role;

			remove_role($cqpim_role);

		}
	
	}
	
	// Manage Roles
	
	add_action('admin_init', 'cqpim_manage_roles');
	
	function cqpim_manage_roles() {
	
		$roles = get_option('cqpim_roles'); 
		
		if(!is_array($roles)) {
		
			$roles = array(get_option('cqpim_roles'));
		
		}
		
		if($roles) {
		
			foreach($roles as $role) {
			
				if($role != 'cqpim_admin') {
			
					$cqpim_role = 'cqpim_' . $role;
				
					$role_exists = get_role($cqpim_role);
					
					if(!$role_exists) {
					
						$caps = array(
						
							'read' => true,
							
							'upload_files' => true,
						
						);
						
						$role_name = str_replace('_', ' ', $cqpim_role);
						
						$role_name = str_replace('cqpim', 'CQPIM', $role_name);
						
						$role_name = ucwords($role_name);
						
						add_role($cqpim_role, $role_name, $caps);
					
					}
				
				}
			
			}
		
		}

		if ( !function_exists('get_editable_roles') ) {
		
		require_once( ABSPATH . '/wp-admin/includes/user.php' );
		
		}
		
		$system_roles = get_editable_roles();

		$plugin_roles = get_option('cqpim_roles');

		if(!is_array($plugin_roles)) {

			$plugin_roles = array(get_option('cqpim_roles'));

		}

		$plugin_roles_ready = array();
		
		foreach($plugin_roles as $plugin_role) {
		
			if($plugin_role == 'cqpim_admin') {
			
				$plugin_roles_ready[] = $plugin_role;
				
			} else {
		
				$plugin_roles_ready[] = 'cqpim_' . $plugin_role;
			
			}
		
		}	

		foreach($system_roles as $key => $system_role) {
		
			if(strpos($key,'cqpim_') !== false && !in_array($key, $plugin_roles_ready)) {
			
				if($key != 'cqpim_client') {
			
					remove_role($key);
				
				}
			
			}
		
		}	
	
	}
	
	// Admin Capabilities (CQPIM Admin and Administrator get everything!)
	
    add_action('admin_init','cqpim_add_role_caps', 100);
	
    function cqpim_add_role_caps() {
		
		$client_role = get_role('cqpim_client');
		
		if(!empty($client_role)) {
			
			$client_role->add_cap('upload_files');
			
		}
		

		$roles = array('cqpim_admin', 'administrator');
		
		foreach($roles as $the_role) { 

		    $role = get_role($the_role);
			
			if(!empty($role)) {
			
				$role->add_cap('cqpim_view_dashboard');
				$role->add_cap('cqpim_view_tickets');
				$role->add_cap('edit_cqpim_permissions');
				$role->add_cap('cqpim_create_new_client');
				$role->add_cap('cqpim_create_new_form');
				$role->add_cap('cqpim_reset_client_passwords');
				$role->add_cap('cqpim_create_new_team');
				$role->add_cap('cqpim_reset_team_passwords');
				$role->add_cap('edit_cqpim_help');
				$role->add_cap('edit_cqpim_settings');
				$role->add_cap('cqpim_grant_admin_role');
				$role->add_cap('cqpim_create_new_quote');
				$role->add_cap('cqpim_send_quotes');
				$role->add_cap('cqpim_view_project_financials');
				$role->add_cap('cqpim_view_all_projects');
				$role->add_cap('cqpim_publish_all_projects');
				$role->add_cap('cqpim_send_project_messages');
				$role->add_cap('cqpim_edit_project_milestones');
				$role->add_cap('cqpim_edit_project_members');
				$role->add_cap('cqpim_edit_project_dates');
				$role->add_cap('cqpim_dash_view_all_tasks');
				$role->add_cap('cqpim_create_new_invoice');			
				$role->add_cap('cqpim_send_invoices');
				$role->add_cap('cqpim_mark_invoice_paid');			
				$role->add_cap('cqpim_view_project_contract');
				$role->add_cap('cqpim_view_project_client_page');
				$role->add_cap('cqpim_mark_project_signedoff');
				$role->add_cap('cqpim_mark_project_closed');
				$role->add_cap('cqpim_create_new_project');
				$role->add_cap('cqpim_create_new_terms');
				$role->add_cap('cqpim_create_new_templates');
				$role->add_cap('cqpim_apply_project_templates');
				$role->add_cap('cqpim_create_new_supports');
				$role->add_cap('cqpim_dash_view_whos_online');
				$role->add_cap('cqpim_view_project_client_info');
				$role->add_cap('cqpim_assign_adhoc_tasks');
				$role->add_cap('cqpim_delete_assigned_tasks');
				$role->add_cap('access_cqpim_messaging');
				$role->add_cap('cqpim_message_all_clients');
				$role->add_cap('cqpim_message_clients_from_projects');
				$role->add_cap('cqpim_team_edit_profile');
				
				$role->add_cap('view_posts');
				$role->add_cap('edit_posts');
				$role->add_cap('edit_private_posts');			
				$role->add_cap('cqpim_do_all');						
				$role->add_cap('upload_files');	
				
				// CPT Permissions
				
				$role->add_cap( 'read_cqpim_client');
				$role->add_cap( 'read_private_cqpim_clients' );
				$role->add_cap( 'edit_cqpim_clients' );
				$role->add_cap( 'edit_others_cqpim_clients' );
				$role->add_cap( 'edit_published_cqpim_clients' );
				$role->add_cap( 'edit_private_cqpim_clients' );
				$role->add_cap( 'publish_cqpim_clients' );
				$role->add_cap( 'delete_cqpim_clients' );
				$role->add_cap( 'delete_others_cqpim_clients' );
				$role->add_cap( 'delete_private_cqpim_clients' );
				$role->add_cap( 'delete_published_cqpim_clients' );
				
				$role->add_cap( 'read_cqpim_task');
				$role->add_cap( 'read_private_cqpim_tasks' );
				$role->add_cap( 'edit_cqpim_tasks' );
				$role->add_cap( 'edit_others_cqpim_tasks' );
				$role->add_cap( 'edit_published_cqpim_tasks' );
				$role->add_cap( 'edit_private_cqpim_tasks' );
				$role->add_cap( 'publish_cqpim_tasks' );
				
				$role->add_cap( 'read_cqpim_template');
				$role->add_cap( 'read_private_cqpim_templates' );
				$role->add_cap( 'edit_cqpim_templates' );
				$role->add_cap( 'edit_others_cqpim_templates' );
				$role->add_cap( 'edit_published_cqpim_templates' );
				$role->add_cap( 'edit_private_cqpim_templates' );
				$role->add_cap( 'publish_cqpim_templates' );
				$role->add_cap( 'delete_cqpim_templates' );
				$role->add_cap( 'delete_others_cqpim_templates' );
				$role->add_cap( 'delete_private_cqpim_templates' );
				$role->add_cap( 'delete_published_cqpim_templates' );
				
				$role->add_cap( 'read_cqpim_term');
				$role->add_cap( 'read_private_cqpim_terms' );
				$role->add_cap( 'edit_cqpim_terms' );
				$role->add_cap( 'edit_others_cqpim_terms' );
				$role->add_cap( 'edit_published_cqpim_terms' );
				$role->add_cap( 'edit_private_cqpim_terms' );
				$role->add_cap( 'publish_cqpim_terms' );
				$role->add_cap( 'delete_cqpim_terms' );
				$role->add_cap( 'delete_others_cqpim_terms' );
				$role->add_cap( 'delete_private_cqpim_terms' );
				$role->add_cap( 'delete_published_cqpim_terms' );
				
				$role->add_cap( 'read_cqpim_form');
				$role->add_cap( 'read_private_cqpim_forms' );
				$role->add_cap( 'edit_cqpim_forms' );
				$role->add_cap( 'edit_others_cqpim_forms' );
				$role->add_cap( 'edit_published_cqpim_forms' );
				$role->add_cap( 'edit_private_cqpim_forms' );
				$role->add_cap( 'publish_cqpim_forms' );
				$role->add_cap( 'delete_cqpim_forms' );
				$role->add_cap( 'delete_others_cqpim_forms' );
				$role->add_cap( 'delete_private_cqpim_forms' );
				$role->add_cap( 'delete_published_cqpim_forms' );
				
				$role->add_cap( 'read_cqpim_team');
				$role->add_cap( 'read_private_cqpim_teams' );
				$role->add_cap( 'edit_cqpim_teams' );
				$role->add_cap( 'edit_others_cqpim_teams' );
				$role->add_cap( 'edit_published_cqpim_teams' );
				$role->add_cap( 'edit_private_cqpim_teams' );
				$role->add_cap( 'publish_cqpim_teams' );
				$role->add_cap( 'delete_cqpim_teams' );
				$role->add_cap( 'delete_others_cqpim_teams' );
				$role->add_cap( 'delete_private_cqpim_teams' );
				$role->add_cap( 'delete_published_cqpim_teams' );
				
				$role->add_cap( 'read_cqpim_support');
				$role->add_cap( 'read_private_cqpim_supports' );
				$role->add_cap( 'edit_cqpim_supports' );
				$role->add_cap( 'edit_others_cqpim_supports' );
				$role->add_cap( 'edit_published_cqpim_supports' );
				$role->add_cap( 'edit_private_cqpim_supports' );
				$role->add_cap( 'publish_cqpim_supports' );
				$role->add_cap( 'delete_cqpim_supports' );
				$role->add_cap( 'delete_others_cqpim_supports' );
				$role->add_cap( 'delete_private_cqpim_supports' );
				$role->add_cap( 'delete_published_cqpim_supports' );
				
				$role->add_cap( 'read_cqpim_project');
				$role->add_cap( 'read_private_cqpim_projects' );
				$role->add_cap( 'edit_cqpim_projects' );
				$role->add_cap( 'edit_others_cqpim_projects' );
				$role->add_cap( 'edit_published_cqpim_projects' );
				$role->add_cap( 'edit_private_cqpim_projects' );
				$role->add_cap( 'publish_cqpim_projects' );
				$role->add_cap( 'delete_cqpim_projects' );
				$role->add_cap( 'delete_others_cqpim_projects' );
				$role->add_cap( 'delete_private_cqpim_projects' );
				$role->add_cap( 'delete_published_cqpim_projects' );
				
				$role->add_cap( 'read_cqpim_quote');
				$role->add_cap( 'read_private_cqpim_quotes' );
				$role->add_cap( 'edit_cqpim_quotes' );
				$role->add_cap( 'edit_others_cqpim_quotes' );
				$role->add_cap( 'edit_published_cqpim_quotes' );
				$role->add_cap( 'edit_private_cqpim_quotes' );
				$role->add_cap( 'publish_cqpim_quotes' );
				$role->add_cap( 'delete_cqpim_quotes' );
				$role->add_cap( 'delete_others_cqpim_quotes' );
				$role->add_cap( 'delete_private_cqpim_quotes' );
				$role->add_cap( 'delete_published_cqpim_quotes' );
				
				$role->add_cap( 'read_cqpim_invoice');
				$role->add_cap( 'read_private_cqpim_invoices' );
				$role->add_cap( 'edit_cqpim_invoices' );
				$role->add_cap( 'edit_others_cqpim_invoices' );
				$role->add_cap( 'edit_published_cqpim_invoices' );
				$role->add_cap( 'edit_private_cqpim_invoices' );
				$role->add_cap( 'publish_cqpim_invoices' );
				$role->add_cap( 'delete_cqpim_invoices' );
				$role->add_cap( 'delete_others_cqpim_invoices' );
				$role->add_cap( 'delete_private_cqpim_invoices' );
				$role->add_cap( 'delete_published_cqpim_invoices' );
				
				if(is_plugin_active('cqpim-expenses/cqpim-expenses.php')) {
					
					// Custom
					
					$role->add_cap('cqpim_view_expenses');
					$role->add_cap('cqpim_bypass_expense_auth');
					$role->add_cap('cqpim_auth_expense');
					$role->add_cap('cqpim_create_new_expense');
					$role->add_cap('cqpim_create_new_supplier');
					$role->add_cap('cqpim_view_expenses_admin');
				
					$role->add_cap( 'read_cqpim_supplier');
					$role->add_cap( 'read_private_cqpim_suppliers' );
					$role->add_cap( 'edit_cqpim_suppliers' );
					$role->add_cap( 'edit_others_cqpim_suppliers' );
					$role->add_cap( 'edit_published_cqpim_suppliers' );
					$role->add_cap( 'edit_private_cqpim_suppliers' );
					$role->add_cap( 'publish_cqpim_suppliers' );
					$role->add_cap( 'delete_cqpim_suppliers' );
					$role->add_cap( 'delete_others_cqpim_suppliers' );
					$role->add_cap( 'delete_private_cqpim_suppliers' );
					$role->add_cap( 'delete_published_cqpim_suppliers' );
					
					$role->add_cap( 'read_cqpim_expense');
					$role->add_cap( 'read_private_cqpim_expenses' );
					$role->add_cap( 'edit_cqpim_expenses' );
					$role->add_cap( 'edit_others_cqpim_expenses' );
					$role->add_cap( 'edit_published_cqpim_expenses' );
					$role->add_cap( 'edit_private_cqpim_expenses' );
					$role->add_cap( 'publish_cqpim_expenses' );
					$role->add_cap( 'delete_cqpim_expenses' );
					$role->add_cap( 'delete_others_cqpim_expenses' );
					$role->add_cap( 'delete_private_cqpim_expenses' );
					$role->add_cap( 'delete_published_cqpim_expenses' );
				
				}
			
			}
			
		}
		
	}
	
	// Custom Role Caps
	
	add_action('admin_init','cqpim_custom_role_caps');
	
	function cqpim_custom_role_caps() {
	
		$cqpim_roles = get_option('cqpim_roles');
		
		$cqpim_perms = get_option('cqpim_permissions');
		
		if(!is_array($cqpim_roles)) {
		
			$cqpim_roles = array(get_option('cqpim_roles'));
		
		}	

		$roles_to_assign = array();
		
		foreach($cqpim_roles as $cqpim_role) {
		
			if($cqpim_role != 'cqpim_admin') {
			
				$roles_to_assign[] = 'cqpim_' . $cqpim_role;
			
			}
		
		}
		
		foreach($roles_to_assign as $the_role) {
		
			$role = get_role($the_role);
			
			$role->add_cap('view_posts');
			$role->add_cap('edit_posts');
			$role->add_cap('edit_private_posts');
			$role->add_cap('cqpim_view_dashboard');				
			$role->add_cap('access_cqpim_messaging');
			
			// Grant Read/Update on all Projects that are assigned
			
	        $role->add_cap( 'read_cqpim_project');
	        $role->add_cap( 'read_private_cqpim_projects' );
	        $role->add_cap( 'edit_cqpim_projects' );
	        $role->add_cap( 'edit_others_cqpim_projects' );
	        $role->add_cap( 'edit_published_cqpim_projects' );
			$role->add_cap( 'edit_private_cqpim_projects' );
	        $role->add_cap( 'publish_cqpim_projects' );
			
	        $role->add_cap( 'read_cqpim_task');
	        $role->add_cap( 'read_private_cqpim_tasks' );
	        $role->add_cap( 'edit_cqpim_tasks' );
	        $role->add_cap( 'edit_others_cqpim_tasks' );
	        $role->add_cap( 'edit_published_cqpim_tasks' );
			$role->add_cap( 'edit_private_cqpim_tasks' );
	        $role->add_cap( 'publish_cqpim_tasks' );

			$role_to_check = str_replace('cqpim_', '', $the_role);
			
			if($cqpim_perms) {
			
			foreach($cqpim_perms as $key => $perm) {
			
				if(in_array($role_to_check, $perm)) {
				
					$role->add_cap($key);
					
					if($key == 'cqpim_view_tickets') {
					
						$role->add_cap( 'read_cqpim_support');
						$role->add_cap( 'read_private_cqpim_supports' );
						$role->add_cap( 'edit_cqpim_supports' );
						$role->add_cap( 'edit_others_cqpim_supports' );
						$role->add_cap( 'edit_published_cqpim_supports' );
						$role->add_cap( 'edit_private_cqpim_supports' );						
					
					}
					
					if($key == 'delete_cqpim_supports') {
					
						$role->add_cap( 'delete_others_cqpim_supports' );
						$role->add_cap( 'delete_private_cqpim_supports' );
						$role->add_cap( 'delete_published_cqpim_supports' );						
					
					}
					
					if($key == 'read_cqpim_client') {
					
						$role->add_cap( 'read_private_cqpim_clients' );
						$role->add_cap( 'edit_cqpim_clients' );
						$role->add_cap( 'edit_others_cqpim_clients' );
						$role->add_cap( 'edit_published_cqpim_clients' );
						$role->add_cap( 'edit_private_cqpim_clients' );					
					
					}
					
					if($key == 'delete_cqpim_clients') {
					
						$role->add_cap( 'delete_others_cqpim_clients' );
						$role->add_cap( 'delete_private_cqpim_clients' );
						$role->add_cap( 'delete_published_cqpim_clients' );						
					
					}
					
					if($key == 'read_cqpim_templates') {
					
						$role->add_cap( 'read_private_cqpim_templates' );
						$role->add_cap( 'edit_cqpim_templates' );
						$role->add_cap( 'edit_others_cqpim_templates' );
						$role->add_cap( 'edit_published_cqpim_templates' );
						$role->add_cap( 'edit_private_cqpim_templates' );					
					
					}
					
					if($key == 'delete_cqpim_templates') {
					
						$role->add_cap( 'delete_others_cqpim_templates' );
						$role->add_cap( 'delete_private_cqpim_templates' );
						$role->add_cap( 'delete_published_cqpim_templates' );						
					
					}
					
					if($key == 'read_cqpim_terms') {
					
						$role->add_cap( 'read_private_cqpim_terms' );
						$role->add_cap( 'edit_cqpim_terms' );
						$role->add_cap( 'edit_others_cqpim_terms' );
						$role->add_cap( 'edit_published_cqpim_terms' );
						$role->add_cap( 'edit_private_cqpim_terms' );					
					
					}
					
					if($key == 'delete_cqpim_terms') {
					
						$role->add_cap( 'delete_others_cqpim_terms' );
						$role->add_cap( 'delete_private_cqpim_terms' );
						$role->add_cap( 'delete_published_cqpim_terms' );						
					
					}
					
					if($key == 'read_cqpim_team') {
					
						$role->add_cap( 'read_private_cqpim_teams' );
						$role->add_cap( 'edit_cqpim_teams' );
						$role->add_cap( 'edit_others_cqpim_teams' );
						$role->add_cap( 'edit_published_cqpim_teams' );
						$role->add_cap( 'edit_private_cqpim_teams' );					
					
					}
					
					if($key == 'delete_cqpim_teams') {
					
						$role->add_cap( 'delete_others_cqpim_teams' );
						$role->add_cap( 'delete_private_cqpim_teams' );
						$role->add_cap( 'delete_published_cqpim_teams' );						
					
					}
					
					if($key == 'read_cqpim_quote') {
					
						$role->add_cap( 'read_private_cqpim_quotes' );
						$role->add_cap( 'edit_cqpim_quotes' );
						$role->add_cap( 'edit_others_cqpim_quotes' );
						$role->add_cap( 'edit_published_cqpim_quotes' );
						$role->add_cap( 'edit_private_cqpim_quotes' );					
					
					}
					
					if($key == 'delete_cqpim_quotes') {
					
						$role->add_cap( 'delete_others_cqpim_quotes' );
						$role->add_cap( 'delete_private_cqpim_quotes' );
						$role->add_cap( 'delete_published_cqpim_quotes' );						
					
					}
					
					if($key == 'read_cqpim_form') {
					
						$role->add_cap( 'read_private_cqpim_forms' );
						$role->add_cap( 'edit_cqpim_forms' );
						$role->add_cap( 'edit_others_cqpim_forms' );
						$role->add_cap( 'edit_published_cqpim_forms' );
						$role->add_cap( 'edit_private_cqpim_forms' );					
					
					}
					
					if($key == 'delete_cqpim_forms') {
					
						$role->add_cap( 'delete_others_cqpim_forms' );
						$role->add_cap( 'delete_private_cqpim_forms' );
						$role->add_cap( 'delete_published_cqpim_forms' );						
					
					}
					
					if($key == 'delete_cqpim_projects') {
					
						$role->add_cap( 'delete_others_cqpim_projects' );
						$role->add_cap( 'delete_private_cqpim_projects' );
						$role->add_cap( 'delete_published_cqpim_projects' );						
					
					}
					
					if($key == 'read_cqpim_invoice') {
					
						$role->add_cap( 'read_private_cqpim_invoices' );
						$role->add_cap( 'edit_cqpim_invoices' );
						$role->add_cap( 'edit_others_cqpim_invoices' );
						$role->add_cap( 'edit_published_cqpim_invoices' );
						$role->add_cap( 'edit_private_cqpim_invoices' );					
					
					}
					
					if($key == 'delete_cqpim_invoices') {
					
						$role->add_cap( 'delete_others_cqpim_invoices' );
						$role->add_cap( 'delete_private_cqpim_invoices' );
						$role->add_cap( 'delete_published_cqpim_invoices' );						
					
					}
					
					if($key == 'read_cqpim_supplier') {
					
						$role->add_cap( 'read_private_cqpim_suppliers' );
						$role->add_cap( 'edit_cqpim_suppliers' );
						$role->add_cap( 'edit_others_cqpim_suppliers' );
						$role->add_cap( 'edit_published_cqpim_suppliers' );
						$role->add_cap( 'edit_private_cqpim_suppliers' );					
					
					}
					
					if($key == 'delete_cqpim_suppliers') {
					
						$role->add_cap( 'delete_others_cqpim_suppliers' );
						$role->add_cap( 'delete_private_cqpim_suppliers' );
						$role->add_cap( 'delete_published_cqpim_suppliers' );						
					
					}
					
					if($key == 'read_cqpim_expense') {
					
						$role->add_cap( 'cqpim_view_expenses' );
						$role->add_cap( 'read_private_cqpim_expenses' );
						$role->add_cap( 'edit_cqpim_expenses' );
						$role->add_cap( 'edit_others_cqpim_expenses' );
						$role->add_cap( 'edit_published_cqpim_expenses' );
						$role->add_cap( 'edit_private_cqpim_expenses' );					
					
					}
					
					if($key == 'delete_cqpim_expenses') {
					
						$role->add_cap( 'delete_others_cqpim_expenses' );
						$role->add_cap( 'delete_private_cqpim_expenses' );
						$role->add_cap( 'delete_published_cqpim_expenses' );						
					
					}
				
				} else {
				
					$role->remove_cap($key);
					
					if($key == 'cqpim_view_tickets') {
					
						$role->remove_cap( 'read_cqpim_support');
						$role->remove_cap( 'read_private_cqpim_supports' );
						$role->remove_cap( 'edit_cqpim_supports' );
						$role->remove_cap( 'edit_others_cqpim_supports' );
						$role->remove_cap( 'edit_published_cqpim_supports' );
						$role->remove_cap( 'edit_private_cqpim_supports' );						
					
					}
					
					if($key == 'delete_cqpim_supports') {
					
						$role->remove_cap( 'delete_others_cqpim_supports' );
						$role->remove_cap( 'delete_private_cqpim_supports' );
						$role->remove_cap( 'delete_published_cqpim_supports' );						
					
					}
					
					if($key == 'read_cqpim_client') {
					
						$role->remove_cap( 'read_private_cqpim_clients' );
						$role->remove_cap( 'edit_cqpim_clients' );
						$role->remove_cap( 'edit_others_cqpim_clients' );
						$role->remove_cap( 'edit_published_cqpim_clients' );
						$role->remove_cap( 'edit_private_cqpim_clients' );					
					
					}
					
					if($key == 'delete_cqpim_clients') {
					
						$role->remove_cap( 'delete_others_cqpim_clients' );
						$role->remove_cap( 'delete_private_cqpim_clients' );
						$role->remove_cap( 'delete_published_cqpim_clients' );						
					
					}
					
					if($key == 'read_cqpim_templates') {
					
						$role->remove_cap( 'read_private_cqpim_templates' );
						$role->remove_cap( 'edit_cqpim_templates' );
						$role->remove_cap( 'edit_others_cqpim_templates' );
						$role->remove_cap( 'edit_published_cqpim_templates' );
						$role->remove_cap( 'edit_private_cqpim_templates' );					
					
					}
					
					if($key == 'delete_cqpim_templates') {
					
						$role->remove_cap( 'delete_others_cqpim_templates' );
						$role->remove_cap( 'delete_private_cqpim_templates' );
						$role->remove_cap( 'delete_published_cqpim_templates' );						
					
					}
					
					if($key == 'read_cqpim_terms') {
					
						$role->remove_cap( 'read_private_cqpim_terms' );
						$role->remove_cap( 'edit_cqpim_terms' );
						$role->remove_cap( 'edit_others_cqpim_terms' );
						$role->remove_cap( 'edit_published_cqpim_terms' );
						$role->remove_cap( 'edit_private_cqpim_terms' );					
					
					}
					
					if($key == 'delete_cqpim_terms') {
					
						$role->remove_cap( 'delete_others_cqpim_terms' );
						$role->remove_cap( 'delete_private_cqpim_terms' );
						$role->remove_cap( 'delete_published_cqpim_terms' );						
					
					}
					
					if($key == 'read_cqpim_team') {
					
						$role->remove_cap( 'read_private_cqpim_teams' );
						$role->remove_cap( 'edit_cqpim_teams' );
						$role->remove_cap( 'edit_others_cqpim_teams' );
						$role->remove_cap( 'edit_published_cqpim_teams' );
						$role->remove_cap( 'edit_private_cqpim_teams' );					
					
					}
					
					if($key == 'delete_cqpim_teams') {
					
						$role->remove_cap( 'delete_others_cqpim_teams' );
						$role->remove_cap( 'delete_private_cqpim_teams' );
						$role->remove_cap( 'delete_published_cqpim_teams' );						
					
					}
					
					if($key == 'read_cqpim_quote') {
					
						$role->remove_cap( 'read_private_cqpim_quotes' );
						$role->remove_cap( 'edit_cqpim_quotes' );
						$role->remove_cap( 'edit_others_cqpim_quotes' );
						$role->remove_cap( 'edit_published_cqpim_quotes' );
						$role->remove_cap( 'edit_private_cqpim_quotes' );					
					
					}
					
					if($key == 'delete_cqpim_quotes') {
					
						$role->remove_cap( 'delete_others_cqpim_quotes' );
						$role->remove_cap( 'delete_private_cqpim_quotes' );
						$role->remove_cap( 'delete_published_cqpim_quotes' );						
					
					}
					
					if($key == 'read_cqpim_form') {
					
						$role->remove_cap( 'read_private_cqpim_forms' );
						$role->remove_cap( 'edit_cqpim_forms' );
						$role->remove_cap( 'edit_others_cqpim_forms' );
						$role->remove_cap( 'edit_published_cqpim_forms' );
						$role->remove_cap( 'edit_private_cqpim_forms' );					
					
					}
					
					if($key == 'delete_cqpim_forms') {
					
						$role->remove_cap( 'delete_others_cqpim_forms' );
						$role->remove_cap( 'delete_private_cqpim_forms' );
						$role->remove_cap( 'delete_published_cqpim_forms' );						
					
					}
					
					if($key == 'delete_cqpim_projects') {
					
						$role->remove_cap( 'delete_others_cqpim_projects' );
						$role->remove_cap( 'delete_private_cqpim_projects' );
						$role->remove_cap( 'delete_published_cqpim_projects' );						
					
					}
					
					if($key == 'read_cqpim_invoice') {
					
						$role->remove_cap( 'read_private_cqpim_invoices' );
						$role->remove_cap( 'edit_cqpim_invoices' );
						$role->remove_cap( 'edit_others_cqpim_invoices' );
						$role->remove_cap( 'edit_published_cqpim_invoices' );
						$role->remove_cap( 'edit_private_cqpim_invoices' );					
					
					}
					
					if($key == 'delete_cqpim_invoices') {
					
						$role->remove_cap( 'delete_others_cqpim_invoices' );
						$role->remove_cap( 'delete_private_cqpim_invoices' );
						$role->remove_cap( 'delete_published_cqpim_invoices' );						
					
					}
					
					if($key == 'read_cqpim_supplier') {
					
						$role->remove_cap( 'read_private_cqpim_suppliers' );
						$role->remove_cap( 'edit_cqpim_suppliers' );
						$role->remove_cap( 'edit_others_cqpim_suppliers' );
						$role->remove_cap( 'edit_published_cqpim_suppliers' );
						$role->remove_cap( 'edit_private_cqpim_suppliers' );					
					
					}
					
					if($key == 'delete_cqpim_suppliers') {
					
						$role->remove_cap( 'delete_others_cqpim_suppliers' );
						$role->remove_cap( 'delete_private_cqpim_suppliers' );
						$role->remove_cap( 'delete_published_cqpim_suppliers' );						
					
					}
					
					if($key == 'read_cqpim_expense') {
						
						$role->remove_cap( 'cqpim_view_expenses' );
						$role->remove_cap( 'read_private_cqpim_expenses' );
						$role->remove_cap( 'edit_cqpim_expenses' );
						$role->remove_cap( 'edit_others_cqpim_expenses' );
						$role->remove_cap( 'edit_published_cqpim_expenses' );
						$role->remove_cap( 'edit_private_cqpim_expenses' );					
					
					}
					
					if($key == 'delete_cqpim_expenses') {
					
						$role->remove_cap( 'delete_others_cqpim_expenses' );
						$role->remove_cap( 'delete_private_cqpim_expenses' );
						$role->remove_cap( 'delete_published_cqpim_expenses' );						
					
					}
				
				}
			
			}
			
			}
		
		}
	
	}
	
	add_action( "wp_ajax_cqpim_remove_all_data", 

		  	"cqpim_remove_all_data");
	
	function cqpim_remove_all_data() {
		
		// Deactivate Plugin & Remove Pages
		
		deactivate_plugins( plugin_basename( __FILE__ ) );
		
		// Remove Projects
		
		$args = array(
		
			'post_type' => 'cqpim_project',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Quotes
		
		$args = array(
		
			'post_type' => 'cqpim_quote',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Clients
		
		$args = array(
		
			'post_type' => 'cqpim_client',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Teams
		
		$args = array(
		
			'post_type' => 'cqpim_teams',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Tickets
		
		$args = array(
		
			'post_type' => 'cqpim_support',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Tasks
		
		$args = array(
		
			'post_type' => 'cqpim_tasks',
			
			'posts_per_page' => -1,
			
			'post_status' => 'publish'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Forms
		
		$args = array(
		
			'post_type' => 'cqpim_forms',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove MS Templates
		
		$args = array(
		
			'post_type' => 'cqpim_templates',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Terms
		
		$args = array(
		
			'post_type' => 'cqpim_terms',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Conversations
		
		$args = array(
		
			'post_type' => 'cqpim_conversations',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Messages
		
		$args = array(
		
			'post_type' => 'cqpim_messages',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Invoices
		
		$args = array(
		
			'post_type' => 'cqpim_invoice',
			
			'posts_per_page' => -1,
			
			'post_status' => 'publish'
			
		);
		
		$posts = get_posts($args);
		
		foreach($posts as $post) {
			
			wp_delete_post($post->ID, true);
			
		}
		
		// Remove Roles
		
		$roles = get_option('cqpim_roles'); 

		if(!is_array($roles)) {

			$roles = array(get_option('cqpim_roles'));

		}

		foreach($roles as $role) {

			$cqpim_role = 'cqpim_' . $role;

			remove_role($cqpim_role);

		}
		
		remove_role('cqpim_admin');
		
		remove_role('cqpim_client');
		
		delete_option('cqpim_roles');
		
		delete_option('cqpim_permissions');
		
		delete_option('vthreeone_completed', '0');
		
		delete_option('vthree_completed', '0');
		
		delete_option('twopoint9_completed', '0');
		
		delete_option('twopoint8_completed', '0');
		
		delete_option('twopoint7five_completed', '0');
		
		delete_option('twopoint7_completed', '0');
		
		delete_option('twopoint6_completed', '0');
		
		// Remove Settings
		
		delete_option('plugin_name');
		
		delete_option('cqpim_date_format');
		
		delete_option('cqpim_allowed_extensions');
		
		delete_option('cqpim_timezone');
		
		delete_option('cqpim_disable_avatars');
		
		delete_option('cqpim_invoice_slug');
		
		delete_option('cqpim_quote_slug');
		
		delete_option('cqpim_project_slug');
		
		delete_option('cqpim_support_slug');
		
		delete_option('cqpim_task_slug');
		
		delete_option('enable_quotes');
		
		delete_option('enable_quote_terms');
		
		delete_option('enable_project_creation');
		
		delete_option('enable_project_contracts');
		
		delete_option('disable_invoices');
		
		delete_option('invoice_workflow');
		
		delete_option('auto_send_invoices');
		
		// Company Settings
		
		delete_option('team_type' );
		
		delete_option('company_name');
		
		delete_option('company_address');
		
		delete_option('company_postcode');
		
		delete_option('company_telephone');
		
		delete_option('company_sales_email');
		
		delete_option('company_accounts_email');
		
		delete_option('company_support_email');
		
		delete_option('company_logo' );
		
		delete_option('company_bank_name');
		
		delete_option('currency_symbol');
		
		delete_option('currency_symbol_position');
		
		delete_option('currency_symbol_space');
		
		delete_option('allow_client_currency_override');
		
		delete_option('allow_project_currency_override');
		
		delete_option('allow_quote_currency_override');
		
		delete_option('allow_invoice_currency_override');
		
		delete_option('currency_code' );
		
		delete_option('company_bank_ac');

		delete_option('company_bank_sc');
		
		delete_option('company_bank_iban');
		
		delete_option('company_invoice_terms');
		
		delete_option('sales_tax_rate');

		delete_option('sales_tax_name');
		
		delete_option('sales_tax_reg');
		
		delete_option('secondary_sales_tax_rate');

		delete_option('secondary_sales_tax_name');
		
		delete_option('secondary_sales_tax_reg');
		
		delete_option('company_number');
		
		// Client Settings
		
		delete_option('client_dashboard_type' );
		
		delete_option('auto_welcome' );
		
		delete_option('auto_welcome_subject' );
		
		delete_option('auto_welcome_content' );
		
		delete_option('client_password_reset_subject' );
		
		delete_option('client_password_reset_content' );
		
		delete_option('password_reset_subject' );
		
		delete_option('password_reset_content' );
		
		delete_option('added_contact_subject' );
		
		delete_option('added_contact_content' );
		
		delete_option('allow_client_settings' );
		
		delete_option('allow_client_users' );
		
		delete_option('cqpim_dash_logo');
		
		delete_option('cqpim_dash_bg');
		
		// Quote Settings
		
		delete_option('enable_frontend_anon_quotes' );
		
		delete_option('enable_client_quotes' );
		
		delete_option('quote_header' );
		
		delete_option('quote_footer' );
		
		delete_option('quote_acceptance_text' );
		
		delete_option('quote_email_subject');
		
		delete_option('quote_default_email' );
		
		// Project Settings
		
		delete_option('default_contract_text' );

		delete_option('contract_acceptance_text' );
		
		delete_option('client_contract_subject' );
		
		delete_option('client_contract_email' );
		
		delete_option('client_update_subject' );
		
		delete_option('client_update_email' );	
		
		delete_option('client_message_subject' );
		
		delete_option('client_message_email' );
		
		delete_option('company_message_subject' );
		
		delete_option('company_message_email' );
		
		delete_option('auto_contract' );
		
		delete_option('auto_invoice' );
		
		delete_option('auto_update' );
		
		delete_option('auto_completion' );
		
		// Invoice Settings
		
		delete_option('client_invoice_email_attach' );
		
		delete_option('client_invoice_after_send_remind_days' );
		
		delete_option('client_invoice_before_terms_remind_days' );
		
		delete_option('client_invoice_after_terms_remind_days' );
		
		delete_option('client_invoice_high_priority' );
		
		delete_option('client_invoice_paypal_address' );
		
		delete_option('client_invoice_stripe_key' );
		
		delete_option('client_invoice_stripe_secret' );
		
		delete_option('client_invoice_subject' );
		
		delete_option('client_invoice_email' );
		
		delete_option('client_deposit_invoice_subject' );
		
		delete_option('client_deposit_invoice_email' );
		
		delete_option('client_invoice_reminder_subject' );
		
		delete_option('client_invoice_reminder_email' );
		
		delete_option('client_invoice_overdue_subject' );
		
		delete_option('client_invoice_overdue_email' );
		
		delete_option('client_invoice_footer' );
		
		delete_option('client_deposit_invoice_email' );
		
		delete_option('client_invoice_allow_partial');
		
		delete_option('client_invoice_twocheck_sid');
		
		delete_option('client_invoice_receipt_subject' );
		
		delete_option('client_invoice_receipt_email' );
		
		// Teams

		delete_option('team_account_subject' );
		
		delete_option('team_account_email' );	
		
		delete_option('team_reset_subject' );
		
		delete_option('team_reset_email' );

		delete_option('team_project_subject' );
		
		delete_option('team_project_email' );	

		delete_option('team_assignment_subject' );
		
		delete_option('team_assignment_email' );	

		// Support
		
		delete_option('client_create_ticket_subject' );
		
		delete_option('client_create_ticket_email' );
		
		delete_option('client_update_ticket_subject' );
		
		delete_option('client_update_ticket_email' );
		
		delete_option('company_update_ticket_subject' );
		
		delete_option('company_update_ticket_email' );
		
		// Quote Forms
		
		delete_option('cqpim_frontend_form' );
		
		delete_option('cqpim_backend_form' );
		
		delete_option('form_reg_auto_welcome' );
		
		delete_option('form_auto_welcome' );
		
		delete_option('new_quote_subject' );
		
		delete_option('new_quote_email' );
		
		delete_option('cqpim_dash_css' );
		
		delete_option('cqpim_logout_url' );
		
		// Piping Settings 
		
		delete_option('cqpim_mail_server' );
		
		delete_option('cqpim_piping_address' );
		
		delete_option('cqpim_mailbox_name' );
		
		delete_option('cqpim_mailbox_pass' );
		
		delete_option('cqpim_string_prefix' );
		
		delete_option('cqpim_create_support_on_email' );
		
		delete_option('cqpim_send_piping_reject' );
		
		delete_option('cqpim_piping_delete' );
		
		delete_option('cqpim_bounce_subject' );
		
		delete_option('cqpim_bounce_content' );
		
		delete_option('cqpim_new_message_subject');
		
		delete_option('cqpim_new_message_content');
		
		delete_option('cqpim_messages_allow_client');
		
		delete_option('cqpim_html_email_styles');
		
		delete_option('cqpim_html_email');
		
		delete_option('cqpim_settings_imported');
		
		delete_option('cqpim_cc_address');
		
		$return =  array( 

			'error' 	=> false,

			'message' 	=> __('The plugin has been reset and deactivated, redirecting now', 'cqpim'),
			
			'redirect' => admin_url() . '/plugins.php',

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);

		exit();	
		
	}
	
	
	// 2.6 Compat
	
	add_action('admin_init', 'cqpim_twopointsix_compat');
	
	function cqpim_twopointsix_compat() {
	
		$complete = get_option('twopoint6_completed');
	
		if($complete != '1') {
		
			$args = array(
			
				'post_type' => 'cqpim_client',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
				
			);
			
			$clients = get_posts($args);
			
			foreach($clients as $client) {
			
				$client_details = get_post_meta($client->ID, 'client_details', true);
				
				$user_id = $client_details['user_id'];
				
				$client_ids = get_post_meta($client->ID, 'client_ids', true);
				
				if(empty($client_ids)) {
				
					$client_ids = array();
					
				}
				
				if(!in_array($user_id, $client_ids)) {
				
					$client_ids[] = $user_id;
				
				}
				
				update_post_meta($client->ID, 'client_ids', $client_ids);
			
			}
			
			// Move terms from settings to post
			
			$terms = get_option('default_contract_text');
			
			$user = wp_get_current_user();
			
			$new_terms = array(
			
				'post_type' => 'cqpim_terms',
				
				'post_status' => 'private',
				
				'post_content' => '',
				
				'post_author' => $user->ID,
				
				'post_title' => __('Default', 'cqpim'),
			
			);
			
			$terms_pid = wp_insert_post( $new_terms, false );

			update_post_meta($terms_pid, 'terms', $terms);
			
			update_option('default_contract_text', $terms_pid);
			
			update_option('twopoint6_completed', '1');
		
		}
	
	}
	
	// 2.7 Compat
	
	add_action('admin_init', 'cqpim_twopointseven_compat');
	
	function cqpim_twopointseven_compat() {
	
		$complete = get_option('twopoint7_completed');
		
		if($complete != '1') {
		
			update_option('cqpim_quote_slug', 'cqpim_quote');
			
			update_option('cqpim_invoice_slug', 'cqpim_invoice');
			
			update_option('cqpim_project_slug', 'cqpim_project');
			
			update_option('cqpim_support_slug', 'cqpim_support');
			
			update_option('cqpim_task_slug', 'cqpim_tasks');
		
			delete_option( 'client_update_email' );
			
			delete_option( 'client_update_subject' );
			
			delete_option( 'company_update_ticket_email' );
			
			delete_option( 'company_update_ticket_subject' );
		
			update_option('team_assignment_subject', 'Project %%TASK_PROJECT%%: Task Updated - %%TASK_TITLE%%.');
			
			update_option('cqpim_date_format', 'd/m/Y');
			
			$content = 'Dear %%NAME%%

%%CURRENT_USER%% has just updated a task: (%%TASK_TITLE%%)
				
Task Status: %%TASK_STATUS%%
Task Priority: %%TASK_PRIORITY%%
Start Date: %%TASK_START%%
Deadline: %%TASK_DEADLINE%%
Estimated Time (Hours): %%TASK_EST%%
Percentage Complete: %%TASK_PC%%

Project: %%TASK_PROJECT%%
Milestone: %%TASK_MILESTONE%%

Assignee: %%TASK_OWNER%%

%%TASK_UPDATE%%

Click here to see the updates -

%%TASK_URL%% 

Best Regards

%%CURRENT_USER%%

%%COMPANY_NAME%%
Tel: %%COMPANY_TELEPHONE%%
Email: %%COMPANY_SALES_EMAIL%%	
			
			';
			
			update_option('team_assignment_email', $content);
			
$content = 'Dear %%NAME%%

%%CURRENT_USER%% has just updated Support Ticket %%TICKET_ID%%

Title: %%TICKET_TITLE%%
ID: %%TICKET_ID%%
Priority: %%TICKET_PRIORITY%%
Status: %%TICKET_STATUS%%
Assignee: %%TICKET_OWNER%%

%%TICKET_UPDATE%%

Please log in to your dashboard to read and reply to their update.

Best Regards

%%COMPANY_NAME%% Support
Tel: %%COMPANY_TELEPHONE%%
Email: %%COMPANY_SALES_EMAIL%%
';			
		update_option('client_update_ticket_email', $content);
		
		update_option('client_update_ticket_subject', '%%CURRENT_USER%% has just updated Support Ticket %%TICKET_ID%%');
			
			// Replace Dates
			
			$args = array(
			
				'post_type' => 'cqpim_quote',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$quotes = get_posts($args);
			
			foreach($quotes as $quote) {
			
				$quote_details = get_post_meta($quote->ID, 'quote_details', true);
				
				if(!empty($quote_details['start_date']) && !is_numeric($quote_details['start_date'])) {
				
					$quote_details['start_date'] = cqpim_convert_date($quote_details['start_date']);
				
				}
				
				if(!empty($quote_details['finish_date']) && !is_numeric($quote_details['finish_date'])) {
				
					$quote_details['finish_date'] = cqpim_convert_date($quote_details['finish_date']);
				
				}
				
				if(!empty($quote_details['sent_details']['date']) && !is_numeric($quote_details['sent_details']['date'])) {
				
					$quote_details['sent_details']['date'] = cqpim_convert_date($quote_details['sent_details']['date'], 'd/m/Y H:i:s');
				
				}
				
				if(!empty($quote_details['confirmed_details']['date']) && !is_numeric($quote_details['confirmed_details']['date'])) {
				
					$quote_details['confirmed_details']['date'] = cqpim_convert_date($quote_details['confirmed_details']['date'], 'd/m/Y H:i:s');
				
				}
				
				update_post_meta($quote->ID, 'quote_details', $quote_details);
				
				$quote_elements = get_post_meta($quote->ID, 'quote_elements', true);
				
				if(!empty($quote_elements)) {
				
					foreach($quote_elements as $key => $element) {
					
						if(!empty($element['deadline']) && !is_numeric($element['deadline'])) {
						
							$quote_elements[$key]['deadline'] = cqpim_convert_date($element['deadline']);
						
						}

						if(!empty($element['start']) && !is_numeric($element['start'])) {
						
							$quote_elements[$key]['start'] = cqpim_convert_date($element['start']);
						
						}							
					
					}
				
				}
				
				update_post_meta($quote->ID, 'quote_elements', $quote_elements);
			
			}
			

			
			// Replace Dates
			
			$args = array(
			
				'post_type' => 'cqpim_project',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$quotes = get_posts($args);
			
			foreach($quotes as $quote) {
			
				$quote_details = get_post_meta($quote->ID, 'project_details', true);
				
				if(!empty($quote_details['start_date']) && !is_numeric($quote_details['start_date'])) {
				
					$quote_details['start_date'] = cqpim_convert_date($quote_details['start_date']);
				
				}
				
				if(!empty($quote_details['finish_date']) && !is_numeric($quote_details['finish_date'])) {
				
					$quote_details['finish_date'] = cqpim_convert_date($quote_details['finish_date']);
				
				}
				
				if(!empty($quote_details['sent_details']['date']) && !is_numeric($quote_details['sent_details']['date'])) {
				
					$quote_details['sent_details']['date'] = cqpim_convert_date($quote_details['sent_details']['date'], 'd/m/Y H:i:s');
				
				}
				
				if(!empty($quote_details['confirmed_details']['date']) && !is_numeric($quote_details['confirmed_details']['date'])) {
				
					$quote_details['confirmed_details']['date'] = cqpim_convert_date($quote_details['confirmed_details']['date'], 'd/m/Y H:i:s');
				
				}
				
				if(!empty($quote_details['signoff_details']['at']) && !is_numeric($quote_details['signoff_details']['at'])) {
				
					$quote_details['signoff_details']['at'] = cqpim_convert_date($quote_details['signoff_details']['at'], 'd/m/Y H:i:s');
				
				}
				
				if(!empty($quote_details['closed_details']['at']) && !is_numeric($quote_details['closed_details']['at'])) {
				
					$quote_details['closed_details']['at'] = cqpim_convert_date($quote_details['closed_details']['at'], 'd/m/Y H:i:s');
				
				}
				
				update_post_meta($quote->ID, 'project_details', $quote_details);
				
				$quote_elements = get_post_meta($quote->ID, 'project_elements', true);
				
				if(!empty($quote_elements)) {
				
					foreach($quote_elements as $key => $element) {
					
						if(!empty($element['deadline']) && !is_numeric($element['deadline'])) {
						
							$quote_elements[$key]['deadline'] = cqpim_convert_date($element['deadline']);
						
						}

						if(!empty($element['start']) && !is_numeric($element['start'])) {
						
							$quote_elements[$key]['start'] = cqpim_convert_date($element['start']);
						
						}							
					
					}
				
				}
				
				update_post_meta($quote->ID, 'project_elements', $quote_elements);
				
				$quote_elements = get_post_meta($quote->ID, 'project_progress', true);
				
				if(!empty($quote_elements)) {
				
					foreach($quote_elements as $key => $element) {
					
						if(!empty($element['date']) && !is_numeric($element['date'])) {
						
							$quote_elements[$key]['date'] = cqpim_convert_date($element['date'], 'd/m/Y H:i:s');
						
						}							
					
					}
				
				}
				
				update_post_meta($quote->ID, 'project_progress', $quote_elements);
			
			}
			
			$args = array(
			
				'post_type' => 'cqpim_invoice',
				
				'posts_per_page' => -1,
				
				'post_status' => 'publish'
				
			);
			
			$invoices = get_posts($args);
			
			foreach($invoices as $invoice) {
			
				$invoice_details = get_post_meta($invoice->ID, 'invoice_details', true);
				
				if(!empty($invoice_details['invoice_date']) && !is_numeric($invoice_details['invoice_date'])) {
				
					$invoice_details['invoice_date'] = preg_replace('/\s+/', '', $invoice_details['invoice_date']);
				
					$invoice_details['invoice_date'] = cqpim_convert_date($invoice_details['invoice_date']);
				
				}
				
				if(!empty($invoice_details['due']) && !is_numeric($invoice_details['due'])) {
				
					$invoice_details['due'] = cqpim_convert_date($invoice_details['due']);
				
				}
				
				if(!empty($invoice_details['sent_details']['date']) && !is_numeric($invoice_details['sent_details']['date'])) {
				
					$invoice_details['sent_details']['date'] = cqpim_convert_date($invoice_details['sent_details']['date'], 'd/m/Y H:i:s');
				
				}
				
				if(!empty($invoice_details['paid_details']['date']) && !is_numeric($invoice_details['paid_details']['date'])) {
				
					$invoice_details['paid_details']['date'] = cqpim_convert_date($invoice_details['paid_details']['date'], 'd/m/Y H:i:s');
				
				}
				
				update_post_meta($invoice->ID, 'invoice_details', $invoice_details);
			
			}
			
			$args = array(
			
				'post_type' => 'cqpim_support',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
				
			);
			
			$invoices = get_posts($args);
			
			foreach($invoices as $invoice) {	

				$ticket_updated = get_post_meta($invoice->ID, 'last_updated', true);
				
				if(!empty($ticket_updated) && !is_numeric($ticket_updated)) {
				
					$ticket_updated = cqpim_convert_date($ticket_updated, 'd/m/Y H:i');
				
				}	
				
				update_post_meta($invoice->ID, 'last_updated', $ticket_updated);

				$ticket_updates = get_post_meta($invoice->ID, 'ticket_updates', true);
				
				if(!empty($ticket_updates)) {
				
					foreach($ticket_updates as $key => $update) {
					
						if(!empty($update['time']) && !is_numeric($update['time'])) {
						
							$ticket_updates[$key]['time'] = cqpim_convert_date($update['time'], 'd/m/Y H:i');
						
						}						
					
					}
				
				}
				
				update_post_meta($invoice->ID, 'ticket_updates', $ticket_updates);

			}
			
			update_option('twopoint7_completed', '1');
			
		}
	
	}
	
	add_action('admin_init', 'cqpim_twopointsevenfive_compat');
	
	function cqpim_twopointsevenfive_compat() {
	
		$complete = get_option('twopoint7five_completed');
		
		if($complete != '1') {
		
			update_option('enable_quotes', 1);
			
			update_option('enable_project_creation', 1);
			
			update_option('enable_project_contracts', 1);
			
			update_option('auto_contract', 1);
			
			update_option('invoice_workflow', 0);
			
			update_option('auto_send_invoices', 1);		
		
			update_option('twopoint7five_completed', '1');
		
		}
		
	}
	
	add_action('admin_init', 'cqpim_twopointeight_compat');
	
	function cqpim_twopointeight_compat() {
	
		$complete = get_option('twopoint8_completed');
		
		if($complete != '1') {
		
			$system_tax_rate = get_option('sales_tax_rate');
			
			$args = array(
			
				'post_type' => array('cqpim_quote', 'cqpim_project', 'cqpim_invoice'),
				
				'posts_per_page' => -1,
				
				'post_status' => array('publish', 'draft', 'private')
			
			);
			
			$posts = get_posts($args);
			
			foreach($posts as $post) {

				if(empty($system_tax_rate)) {
				
					update_post_meta($post->ID, 'tax_applicable', 0);
					update_post_meta($post->ID, 'tax_set', 1);
					update_post_meta($post->ID, 'tax_rate', 0);
				
				} else { 
				
					update_post_meta($post->ID, 'tax_applicable', 1);
					update_post_meta($post->ID, 'tax_set', 0);
					update_post_meta($post->ID, 'tax_rate', $system_tax_rate);
				
				}
			
			}
		
			update_option('twopoint8_completed', '1');
		
		}
		
	}
	
	add_action('admin_init', 'cqpim_twopointnine_compat');
	
	function cqpim_twopointnine_compat() {
	
		$complete = get_option('twopoint9_completed');
		
		if($complete != '1') {
		
			update_option('cqpim_allowed_extensions', 'png,jpg,gif,zip,doc,docx,xls,xlsx,pdf');
		
			update_option('twopoint9_completed', '1');
		
		}
		
	}
	
	//update_option('three_completed', '0');
	
	add_action('admin_init', 'cqpim_vthree_compat');
	
	function cqpim_vthree_compat() {
	
		$complete = get_option('vthree_completed');
		
		if($complete != '1') {
		
			$args = array(
			
				'post_type' => 'cqpim_forms',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$forms = get_posts($args);
			
			foreach($forms as $form) {
			
				$form_data = get_post_meta($form->ID, 'builder_data', true);

				if(is_array($form_data)) {

					$form_data = '';

				}				
				
				$form_data = json_decode($form_data);
				
				$fields = isset($form_data->fields) ? $form_data->fields : array();
				
				$new_form_data = array();
				
				if(!empty($fields)) {
				
					foreach($fields as $key => $field) {
					
						$id = strtolower($field->label);
						
						$field_options = $field->field_options;
						
						$id = str_replace(' ', '_', $id);
						
						$id = str_replace('-', '_', $id);
						
						$id = preg_replace('/[^\w-]/', '', $id);
					
						if($field->field_type == 'text') {
						
							$new_form_data[$key] = array(
							
								'type' => $field->field_type,
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'text',
								
								'className' => 'form-control',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->maxlength)) {
							
								$new_form_data[$key]['maxlength'] = $field_options->maxlength;
							
							}
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
						
						} elseif($field->field_type == 'website') {
						
							$new_form_data[$key] = array(
							
								'type' => 'text',
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'text',
								
								'className' => 'form-control',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
						
						} elseif($field->field_type == 'number') {
						
							$new_form_data[$key] = array(
							
								'type' => $field->field_type,
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'number',
								
								'className' => 'form-control',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
							
							if(!empty($field_options->min)) {
							
								$new_form_data[$key]['min'] = $field_options->min;
							
							}
							
							if(!empty($field_options->max)) {
							
								$new_form_data[$key]['max'] = $field_options->max;
							
							}
						
						} elseif($field->field_type == 'paragraph') {
						
							$new_form_data[$key] = array(
							
								'type' => 'textarea',
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'textarea',
								
								'className' => 'form-control',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
							
							if(!empty($field_options->maxlength)) {
							
								$new_form_data[$key]['maxlength'] = $field_options->maxlength;
							
							}
						
						} elseif($field->field_type == 'date') {
						
							$new_form_data[$key] = array(
							
								'type' => $field->field_type,
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'text',
								
								'className' => 'calendar',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
						
						} elseif($field->field_type == 'email') {
						
							$new_form_data[$key] = array(
							
								'type' => 'text',
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'email',
								
								'className' => 'form-control',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
						
						} elseif($field->field_type == 'checkboxes') {
							
							$options = $field_options->options;
							
							$new_form_data[$key] = array(
							
								'type' => 'checkbox-group',
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'checkbox-group',
								
								'className' => 'checkbox-group',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
							
							if(!empty($options)) {
							
								$new_form_data[$key]['values'] = array();
							
							}
							
							foreach($options as $okey => $option) {
							
								$opvalue = str_replace(' ', '_', $option->label);
								
								$opvalue = str_replace('-', '_', $opvalue);
								
								$opvalue = preg_replace('/[^\w-]/', '', $opvalue);
							
								$new_form_data[$key]['values'][$okey] = array(
								
									'label' => $option->label,
									
									'value' => $opvalue,
									
									'selected' => $option->checked,
								
								);
								
							}
						
						} elseif($field->field_type == 'radio') {
							
							$options = $field_options->options;
							
							$new_form_data[$key] = array(
							
								'type' => 'radio-group',
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'radio-group',
								
								'className' => 'radio-group',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
							
							if(!empty($options)) {
							
								$new_form_data[$key]['values'] = array();
							
							}
							
							foreach($options as $okey => $option) {
							
								$opvalue = str_replace(' ', '_', $option->label);
								
								$opvalue = str_replace('-', '_', $opvalue);
								
								$opvalue = preg_replace('/[^\w-]/', '', $opvalue);
							
								$new_form_data[$key]['values'][$okey] = array(
								
									'label' => $option->label,
									
									'value' => $opvalue,
									
									'selected' => $option->checked,
								
								);
								
							}
						
						} elseif($field->field_type == 'dropdown') {
							
							$options = $field_options->options;
							
							$new_form_data[$key] = array(
							
								'type' => 'select',
								
								'required' => isset($field->required) ? $field->required : false,
								
								'label' => $field->label,
								
								'subtype' => 'select',
								
								'className' => 'form-control',
								
								'name' => $id,
							
							);
							
							if(!empty($field_options->description)) {
							
								$new_form_data[$key]['description'] = $field_options->description;
							
							}
							
							if(!empty($options)) {
							
								$new_form_data[$key]['values'] = array();
							
							}
							
							foreach($options as $okey => $option) {
							
								$opvalue = str_replace(' ', '_', $option->label);
								
								$opvalue = str_replace('-', '_', $opvalue);
								
								$opvalue = preg_replace('/[^\w-]/', '', $opvalue);
							
								$new_form_data[$key]['values'][$okey] = array(
								
									'label' => $option->label,
									
									'value' => $opvalue,
									
									'selected' => $option->checked,
								
								);
								
							}
						
						}
						
						if(!empty($field_options->include_other_option) && $field_options->include_other_option == 1) {
						
							$new_form_data[$key]['other'] = true;
						
						}
					
					}
					
					$new_form_data = json_encode($new_form_data);
					
					update_post_meta($form->ID, 'builder_data', $new_form_data);
					
				} else {
				
					continue;
					
				}				
			
			}
			
			update_option('currency_symbol_position', 'l');
			
			update_option('currency_symbol_space', '0');
			
			$currency = get_option('currency_symbol');
			
			$currency_code = get_option('currency_code');
			
			$currency_position = get_option('currency_symbol_position');
			
			$currency_space = get_option('currency_symbol_space'); 
			
			$args = array(
			
				'post_type' => 'cqpim_project',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);	

			$projects = get_posts($args);
			
			foreach($projects as $project) {
				
				$project_contributors = get_post_meta($project->ID, 'project_contributors', true);
				
				$new_pc = array();
				
				foreach($project_contributors as $key => $contributor) {
					
					if(empty($contributor['team_id'])) {
						
						$run = true;
						
					}
					
				}
				
				if(!empty($run)) {
				
					foreach($project_contributors as $contributor) {
						
						$new_pc[] = array(
						
							'team_id' => $contributor,
							
							'pm' => false
							
						);
						
					}
				
				}
				
				update_post_meta($project->ID, 'project_contributors', $new_pc);
			
				update_post_meta($project->ID, 'currency_symbol', $currency);
				
				update_post_meta($project->ID, 'currency_code', $currency_code);
				
				update_post_meta($project->ID, 'currency_position', $currency_position);
				
				update_post_meta($project->ID, 'currency_space', $currency_space);			
			
			}
			
			$args = array(
			
				'post_type' => 'cqpim_quote',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);	

			$quotes = get_posts($args);
			
			foreach($quotes as $quote) {
			
				update_post_meta($quote->ID, 'currency_symbol', $currency);
				
				update_post_meta($quote->ID, 'currency_code', $currency_code);
				
				update_post_meta($quote->ID, 'currency_position', $currency_position);
				
				update_post_meta($quote->ID, 'currency_space', $currency_space);			
			
			}
			
			$args = array(
			
				'post_type' => 'cqpim_invoice',
				
				'posts_per_page' => -1,
				
				'post_status' => 'publish'
			
			);	

			$invoices = get_posts($args);
			
			foreach($invoices as $invoice) {
			
				update_post_meta($invoice->ID, 'currency_symbol', $currency);
				
				update_post_meta($invoice->ID, 'currency_code', $currency_code);
				
				update_post_meta($invoice->ID, 'currency_position', $currency_position);
				
				update_post_meta($invoice->ID, 'currency_space', $currency_space);			
			
			}
		
			update_option('vthree_completed', '1');
		
		}
	
	}
	
	add_action('admin_init', 'cqpim_vthreeone_compat');
	
	function cqpim_vthreeone_compat() {
	
		$complete = get_option('vthreeone_completed');
		
		if($complete != '1') {
			
			// Create the messages table
			
			update_option('cqpim_enable_messaging', true);
			
			update_option('cqpim_messages_allow_client', true);
			
$content = 'Dear %%RECIPIENT_NAME%%

%%SENDER_NAME%% has just sent a new message in conversation %%CONVERSATION_SUBJECT%%

%%MESSAGE%%

Please log in to your dashboard to view and action the message.

Best Regards

%%COMPANY_NAME%%
Tel: %%COMPANY_TELEPHONE%%
Email: %%COMPANY_SALES_EMAIL%%
';			
			update_option('cqpim_new_message_content', $content);
			
			update_option('cqpim_new_message_subject', '%%CONVERSATION_ID%% %%SENDER_NAME%% has sent you a new message');
			
			update_option('vthreeone_completed', '1');
			
		}
		
	}

	
	
	
	
	
	