jQuery(document).ready(function() {
	
	jQuery('.dataTable').dataTable({
	
		"ordering" : false,
		
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.dataTable-DBInvoice').dataTable({
	
		"order": [[ 0, 'desc' ]],
		
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.dt-tk').dataTable({
	
		"ordering": false,
		
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.dt-st').dataTable({
	
		"ordering": false,
		
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.masonry-grid').masonry({
  
		columnWidth: '.grid-sizer',
		itemSelector: '.grid-item',
		percentPosition: true
	  
	});

	jQuery('.add_timer').on('click', function(e) {
	
		e.preventDefault();
		
		var task_id = jQuery(this).val();
		
		var task_title = jQuery(this).data('title');

		jQuery.colorbox({
			
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#add-time-div',							

			'opacity': '0.5',	
			
			'overlayClose'  : false, 
				
			'escKey' : false,
			
			'onCleanup' : function() {
			
				jQuery('.timer').timer('remove');
			
			}
			
		});	
		
		jQuery.colorbox.resize();	

		jQuery('#task_time_task').val(task_id);
	
	});

	jQuery('#create_linked_team').on('click', function(e) {
	
		e.preventDefault();
		
		var user_id = jQuery(this).data('uid');
		
		var spinner = jQuery('#cqpim_overlay');
	
		var data = {
		
			'action' : 'cqpim_create_team_from_admin',
			
			'user_id' : user_id,
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#create_linked_team').prop('disabled', true);

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#create_linked_team').prop('disabled', false);
				
				alert('Something went wrong, pleae check your WordPress account to make sure the Display Name field has been completed. Then try again.');
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#create_linked_team').prop('disabled', false);
				
				location.reload();
			
			}

		});	
	
	});
    
	jQuery('.colorbox').on('click', function(e) {
	
		e.preventDefault();
	
		var anc = jQuery(this).attr('href');
		
		var id = anc.replace('#', '');
		
		var thediv = jQuery('#' + id);
		
		jQuery(thediv).parent('div').attr('id', id + '_container');
		
		jQuery.colorbox({
			
			'inline': true,
				
			'href': '#' + id,
			
			'opacity': '0.5',
			
		});	
	
	});

	jQuery('body').on('focus',".datepicker", function(){
	
		jQuery(this).datepicker({ dateFormat: 'dd/mm/yy' });
	
	});	
	
	// Delete Time Entry
	
	jQuery('.time_remove').live('click', function(e) {
	
		e.preventDefault();
		
		var task_id = jQuery(this).data('task');
		
		var key = jQuery(this).data('key');
		
		var element = jQuery(this);
		
		var domain = document.domain;
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_remove_time_entry',
			
			'task_id' : task_id,
			
			'key' : key
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

				jQuery('a.time_remove').prop('disabled', true);
				
				jQuery.colorbox.resize();

			},
			
		}).done(function(response){
					
			if(response.error == true) {
			
			spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('a.time_remove').prop('disabled', false);

				
			} else {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('a.time_remove').prop('disabled', false);
				
				jQuery(element).parents('li').fadeOut('slow');
				
				jQuery(element).parents('li').remove();
				
				jQuery.colorbox.resize();
				
			
			}				

		});
	
	});

	var hasTimer = false;
	// Init timer start
	jQuery('.start-timer-btn').on('click', function() {
		hasTimer = true;
		jQuery('.timer').timer({
			editable: false,
			format: '%H:%M:%S' 
		});
		jQuery(this).addClass('hidden');
		jQuery('.pause-timer-btn, .remove-timer-btn').removeClass('hidden');
	});

	// Init timer resume
	jQuery('.resume-timer-btn').on('click', function() {
		jQuery('.timer').timer('resume');
		jQuery(this).addClass('hidden');
		jQuery('.pause-timer-btn, .remove-timer-btn').removeClass('hidden');
	});


	// Init timer pause
	jQuery('.pause-timer-btn').on('click', function() {
		jQuery('.timer').timer('pause');
		jQuery(this).addClass('hidden');
		jQuery('.resume-timer-btn').removeClass('hidden');
	});

	// Remove timer
	jQuery('.remove-timer-btn').on('click', function() {
		hasTimer = false;
		jQuery('.timer').timer('remove');
		jQuery(this).addClass('hidden');
		jQuery('.start-timer-btn').removeClass('hidden');
		jQuery('.pause-timer-btn, .resume-timer-btn').addClass('hidden');
	});

	// Additional focus event for this demo
	jQuery('.timer').on('focus', function() {
		if(hasTimer) {
			jQuery('.pause-timer-btn').addClass('hidden');
			jQuery('.resume-timer-btn').removeClass('hidden');
		}
	});

	// Additional blur event for this demo
	jQuery('.timer').on('blur', function() {
		if(hasTimer) {
			jQuery('.pause-timer-btn').removeClass('hidden');
			jQuery('.resume-timer-btn').addClass('hidden');
		}
	});
	
	// AJAX Add Time from Timer
	
	jQuery('#add_time_ajax').live('click', function(e) {
	
		e.preventDefault();
		
		var task_id = jQuery('#task_time_task').val();
		
		var time = jQuery('#task_time_value').val();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_add_timer_time',
			
			'task_id' : task_id,
			
			'time' : time
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

				jQuery('#add_time_ajax').prop('disabled', true);
				
				jQuery.colorbox.resize();

			},
			
		}).done(function(response){
					
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#add_time_ajax').prop('disabled', false);
				
				jQuery('#time_messages').html('<div class="cqpim-alert cqpim-alert-danger">' + response.message + '</div>');
				
				jQuery.colorbox.resize();

				
			} else {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#add_time_ajax').prop('disabled', false);
				
				jQuery('#time_messages').html('<div class="cqpim-alert cqpim-alert-success">' + response.message + '</div>');
				
				jQuery.colorbox.resize();
				
				location.reload();
				
			
			}				

		});
	
	});
	
	jQuery('#task_status_filter').live('change', function(e) {
	
		e.preventDefault();
		
		var filter = jQuery(this).val();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_filter_tasks',
			
			'filter' : filter,
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

			},
			
		}).done(function(response){
					
			if(response.error == true) {
			
				spinner.hide();
				
			} else {
			
				spinner.hide();
				
				location.reload();
				
			
			}				

		});
	
	});	
	
	jQuery('.calendar_filter').live('change', function(e) {
	
		e.preventDefault();
		
		var filters = jQuery('.calendar_filter:checkbox:checked').map(function() {
			return this.value;
		}).get();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_filter_calendar',
			
			'filters' : filters,
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

			},
			
		}).done(function(response){
						
			location.reload();	

		});
	
	});	
	
	// Change graph year
	
	jQuery('#income_control_date').live('change', function(e) {
	
		e.preventDefault();
		
		var date = jQuery('#income_control_date').val();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_edit_income_graph',
			
			'date' : date,
			
			'type' : 'date',
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

			},
			
		}).done(function(response){
			
			spinner.hide();
						
			location.reload();	

		});
	
	});	
	
	// Change graph type
	
	jQuery('#income_control_type').live('change', function(e) {
	
		e.preventDefault();
		
		var date = jQuery('#income_control_type').val();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_edit_income_graph',
			
			'date' : date,
			
			'type' : 'type',
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

			},
			
		}).done(function(response){
			
			spinner.hide();
						
			location.reload();	

		});
	
	});		
	
});



