jQuery(document).ready(function() {
	
	setTimeout(
	
	  function() {
		
		jQuery('.cqpim-alert').fadeOut("slow");
		
	  }, 2000);  
	
	jQuery('.milestones').dataTable({
		
		"order": [[ 4, 'desc' ]],
	
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}	
	
	});
	
	jQuery('#send-message').on('click', function(e) {
		
		e.preventDefault();
		
		jQuery('#cqpim-new-message').toggle();		
				
		jQuery('#cqpim-reply-message').hide();
		
	});
	
	jQuery('#cqpim-convo-reply').on('click', function(e) {
		
		e.preventDefault();
		
		jQuery('#cqpim-reply-message').toggle();
		
		jQuery('#cqpim-new-message').hide();
		
	});
	
	jQuery('#cancel').on('click', function(e) {
		
		e.preventDefault();
		
		jQuery('#cqpim-new-message').toggle();
		
	});
	
	jQuery('#cancel-reply').on('click', function(e) {
		
		e.preventDefault();
		
		jQuery('#cqpim-reply-message').toggle();		
		
	});
	

	
	jQuery('#cqpim-edit-subject').on('click', function(e) {
		
		e.preventDefault();
		
		jQuery('#cqpim-title-editable').toggle();
		
		jQuery('#cqpim-title-editable-field').toggle();
		
		jQuery('#cqpim-cancel-edit-subject').toggle();
		
		jQuery('#cqpim-save-subject').toggle();
		
	});
	
	jQuery('#cqpim-cancel-edit-subject').on('click', function(e) {
		
		e.preventDefault();

		jQuery('#cqpim-title-editable').toggle();
		
		jQuery('#cqpim-title-editable-field').toggle();
		
		jQuery('#cqpim-cancel-edit-subject').toggle();
		
		jQuery('#cqpim-save-subject').toggle();		
		
	});

	jQuery('#send').on('click', function(e) {
		
		e.preventDefault();
		
		var element = jQuery(this);
		
		var recipients = jQuery('#to').val();
		
		var subject = jQuery('#subject').val();
		
		var message = jQuery('#message').val();
		
		var attachments = jQuery('#upload_attachment_ids').val();
		
		var responsebox = jQuery('#message-ajax-response');
		
		var data = {
			
			'action' : 'cqpim_create_conversation',
			
			'recipients' : recipients,
			
			'subject' : subject,
			
			'message' : message,
			
			'attachments' : attachments
			
		}
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				jQuery('#cqpim_overlay').show();
				
				jQuery(element).prop('disabled', true);
				
				jQuery(responsebox).html('');

			},

		}).always(function(response){
		
			console.log(response);
		
		}).done(function(response){
		
			if(response.error == true) {
				
				jQuery('#cqpim_overlay').hide();
				
				jQuery(element).prop('disabled', false);
				
				alert(response.message);
				
			} else {

				window.location.replace(response.redirect);
			
			}

		});
		
	});
	
	jQuery('#send-reply').on('click', function(e) {
		
		e.preventDefault();
		
		var element = jQuery(this);
		
		var conversation = jQuery(element).data('conversation');
		
		var message = jQuery('#rmessage').val();
		
		var attachments = jQuery('#rupload_attachment_ids').val();
		
		var responsebox = jQuery('#rmessage-ajax-response');
		
		var data = {
			
			'action' : 'cqpim_create_conversation_reply',
			
			'conversation' : conversation,
			
			'message' : message,
			
			'attachments' : attachments
			
		}
		
		console.log(data);
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				jQuery('#cqpim_overlay').show();
				
				jQuery(element).prop('disabled', true);
				
				jQuery(responsebox).html('');

			},

		}).always(function(response){
		
			console.log(response);
		
		}).done(function(response){
		
			if(response.error == true) {
				
				jQuery('#cqpim_overlay').hide();
				
				jQuery(element).prop('disabled', false);
				
				alert(response.message);
				
			} else {

				window.location.reload();
			
			}

		});
		
	});
	
	jQuery('#cqpim-convo-delete').on('click', function(e) {
		
		e.preventDefault();
	
		jQuery('#delete-confirm').dialog({
			
			resizable: false,
			
			height: "auto",
			
			width: 400,
			
			modal: true,
			
			buttons: [{
					
				text: messaging.dialogs.deleteconv,
					
				click: function() {
					
					jQuery( this ).dialog( "close" );
					
					var element = jQuery('#cqpim-convo-delete');
						
					var conversation = jQuery('#jq-conv-id').val();
					
					var data = {
						
						'action' : 'cqpim_delete_conversation',
						
						'conversation' : conversation,
						
					}
					
					jQuery.ajax({

						url: ajaxurl,

						data: data,

						type: 'POST',

						dataType: 'json',

						beforeSend: function(){

							// show spinner

							jQuery('#cqpim_overlay').show();
							
							jQuery(element).prop('disabled', true);

						},

					}).always(function(response){
					
						console.log(response);
					
					}).done(function(response){
					
						if(response.error == true) {
						
							jQuery('#cqpim_overlay').hide();
							
							jQuery(element).prop('disabled', false);
							
							alert(response.message);
							
						} else {

							window.location.replace(response.redirect);
						
						}

					});
						
				}			
					
			},{
					
				text: messaging.dialogs.cancel,
					
				click: function() {
						
					jQuery( this ).dialog( "close" );
						
				}			
					
			}]
			
		});
	
	});
	
	jQuery('#cqpim-save-subject').on('click', function(e) {
		
		e.preventDefault();
		
		var element = jQuery(this);
		
		var conversation = jQuery('#jq-conv-id').val();
		
		var title = jQuery('#cqpim-title-editable-field').val();
		
		var data = {
			
			'action' : 'cqpim_edit_conversation_title',
			
			'conversation' : conversation,
			
			'title' : title
			
		}
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				jQuery('#cqpim_overlay').show();
				
				jQuery(element).prop('disabled', true);

			},

		}).always(function(response){
		
			console.log(response);
		
		}).done(function(response){
		
			if(response.error == true) {
			
				jQuery('#cqpim_overlay').hide();
				
				jQuery(element).prop('disabled', false);
				
				alert(response.message);
				
			} else {

				window.location.reload();
			
			}

		});
		
	});
	
	jQuery('#cqpim-convo-leave').on('click', function(e) {
		
		e.preventDefault();
		
		jQuery('#leave-confirm').dialog({
			
			resizable: false,
			
			height: "auto",
			
			width: 400,
			
			modal: true,
			
			buttons: [{
					
				text: messaging.dialogs.leaveconv,
					
				click: function() {
					
					jQuery( this ).dialog( "close" );
					
					var element = jQuery('#cqpim-convo-leave');
						
					var conversation = jQuery('#jq-conv-id').val();
					
					var user = jQuery('#jq-user-id').val();
					
					var data = {
						
						'action' : 'cqpim_remove_conversation_user',
						
						'conversation' : conversation,
						
						'user' : user,
						
						'type' : 'leave'
						
					}
					
					jQuery.ajax({

						url: ajaxurl,

						data: data,

						type: 'POST',

						dataType: 'json',

						beforeSend: function(){

							// show spinner

							jQuery('#cqpim_overlay').show();
							
							jQuery(element).prop('disabled', true);

						},

					}).always(function(response){
					
						console.log(response);
					
					}).done(function(response){
					
						if(response.error == true) {
						
							jQuery('#cqpim_overlay').hide();
							
							jQuery(element).prop('disabled', false);
							
							alert(response.message);
							
						} else {

							window.location.replace(response.redirect);
						
						}

					});
						
				}			
					
			},{
					
				text: messaging.dialogs.cancel,
					
				click: function() {
						
					jQuery( this ).dialog( "close" );
						
				}			
					
			}]
			
		});
		
	});
	
	jQuery('#cqpim-convo-remove').on('click', function(e) {
		
		e.preventDefault();
		
		jQuery('#remove-confirm').dialog({
			
			resizable: false,
			
			height: "auto",
			
			width: 400,
			
			modal: true,
			
			buttons: [{
					
				text: messaging.dialogs.removeconv,
					
				click: function() {
					
					jQuery( this ).dialog( "close" );
					
					var element = jQuery('#cqpim-convo-remove');
						
					var conversation = jQuery('#jq-conv-id').val();
					
					var user = jQuery('#cqpim-remove-user').val();
					
					var data = {
						
						'action' : 'cqpim_remove_conversation_user',
						
						'conversation' : conversation,
						
						'user' : user,
						
						'type' : 'remove'
						
					}
					
					jQuery.ajax({

						url: ajaxurl,

						data: data,

						type: 'POST',

						dataType: 'json',

						beforeSend: function(){

							// show spinner

							jQuery('#cqpim_overlay').show();
							
							jQuery(element).prop('disabled', true);

						},

					}).always(function(response){
					
						console.log(response);
					
					}).done(function(response){
					
						if(response.error == true) {
						
							jQuery('#cqpim_overlay').hide();
							
							jQuery(element).prop('disabled', false);
							
							alert(response.message);
							
						} else {

							window.location.replace(response.redirect);
						
						}

					});
						
				}			
					
			},{
					
				text: messaging.dialogs.cancel,
					
				click: function() {
						
					jQuery( this ).dialog( "close" );
						
				}			
					
			}]
			
		});
		
	});
	
	jQuery('#cqpim-convo-add').on('click', function(e) {
		
		e.preventDefault();
		
		jQuery('#add-confirm').dialog({
			
			resizable: false,
			
			height: "auto",
			
			width: 500,
			
			modal: true,
			
			buttons: [{
					
				text: messaging.dialogs.addconv,
					
				click: function() {
					
					jQuery( this ).dialog( "close" );
					
					var element = jQuery('#cqpim-convo-add');
						
					var conversation = jQuery('#jq-conv-id').val();
					
					var recipients = jQuery('#ato').val();
					
					var data = {
						
						'action' : 'cqpim_add_conversation_user',
						
						'conversation' : conversation,
						
						'recipients' : recipients,
						
					}
					
					jQuery.ajax({

						url: ajaxurl,

						data: data,

						type: 'POST',

						dataType: 'json',

						beforeSend: function(){

							// show spinner

							jQuery('#cqpim_overlay').show();
							
							jQuery(element).prop('disabled', true);

						},

					}).always(function(response){
					
						console.log(response);
					
					}).done(function(response){
					
						if(response.error == true) {
						
							jQuery('#cqpim_overlay').hide();
							
							jQuery(element).prop('disabled', false);
							
							alert(response.message);
							
						} else {

							window.location.replace(response.redirect);
						
						}

					});
						
				}			
					
			},{
					
				text: messaging.dialogs.cancel,
					
				click: function() {
						
					jQuery( this ).dialog( "close" );
						
				}			
					
			}]
			
		});
		
	});

});