jQuery(document).ready(function() {
	
	if ( jQuery.isFunction(jQuery.fn.tooltip) ) {
	
	jQuery('.cqpim_tooltip').tooltip();
	
	}
	
	jQuery('.menu-open').on('click', function(e) {
		
		e.preventDefault;
		
		jQuery(this).hide();
		
		jQuery('#cqpim-dash-sidebar').show();
		
		jQuery('.menu-close').show();
		
	});
	
	jQuery('.menu-close').on('click', function(e) {
		
		e.preventDefault;
		
		jQuery(this).hide();
		
		jQuery('#cqpim-dash-sidebar').hide();
		
		jQuery('.menu-open').show();
		
	});
	
	/*jQuery( window ).resize(function() {
		
		var width = jQuery( window ).width();
		
		//alert(width);
		
		if(width > 900) {
			
			jQuery('#cqpim-dash-sidebar').show();
			
		} else {
			
			//jQuery('#cqpim-dash-sidebar').hide();
			
			jQuery('.menu-open').show();
			
			jQuery('.menu-close').hide();
			
		}
		
	});*/

	jQuery('.masonry-grid').masonry({
	  
		columnWidth: '.grid-sizer',
		itemSelector: '.grid-item',
		percentPosition: true
	  
	});
	
	jQuery('#payment-amount').show();
	
	jQuery('#stripe-pay').show();

	jQuery('#pp-pay').show();
	
	jQuery('#save_amount').on('click', function(e) {
	
		e.preventDefault;
		
		var amount = jQuery('#amount_to_pay').val();
		
		jQuery('#overlay').show();
		
		window.location.href = window.location.pathname+"?"+jQuery.param({'atp':amount})
	
	});
	
	jQuery('#cqpim_pay_now').click(function(e) {
	
		e.preventDefault();
		
		jQuery.colorbox({
	
			'inline': true,
		
			'href': '#cqpim_payment_methods',	

			'opacity': '0.5',
	
		});	
		
		jQuery.colorbox.resize();
	
	});
	

	jQuery('.dataTable').dataTable({
	
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.dataTable-CST').dataTable({
	
		"order": [[ 4, 'desc' ]],
		
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.dataTable-CI').dataTable({
	
		"order": [[ 0, 'desc' ]],
		
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.dataTable-CQ').dataTable({
	
		"order": [[ 1, 'asc' ]],
		
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.dataTable-CP').dataTable({
	
		"order": [[ 4, 'asc' ]],
		
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('#switch_to_resolved').click(function(e) {
	
		e.preventDefault();
		
		var spinner = jQuery('#overlay');
			
		var data = {
		
			'action' : 'cqpim_switch_resolved_tickets'
		
		};
		
		jQuery.ajax({

			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#switch_to_resolved').prop('disabled', true);

			},

		}).done(function(response){

			spinner.hide();

			jQuery('#switch_to_resolved').prop('disabled', false);
				
			location.reload();

		});	
	
	});
	
	jQuery('#support-submit').click(function(e) {
	
		e.preventDefault();
		
		var form_data = {};
		
		form_data['ticket_title'] = jQuery('#ticket_title').val();
		
		form_data['ticket_priority_new'] = jQuery('#ticket_priority_new').val();
		
		form_data['ticket_update_new'] = jQuery('#ticket_update_new').val();

		form_data['ticket_item'] = jQuery('#ticket_item').val();	
		
		form_data['reject_reason'] = jQuery('#reject_reason').val();
		
		form_data['custom'] = {};
		
		i = 0;

		jQuery('.cqpim-custom').each(function() {
			
			if(jQuery(this).attr('type') == 'radio') {
				
				if (jQuery(this).is(':checked')) {
					
					form_data['custom'][jQuery(this).attr('name')] = jQuery(this).val();	
					
				}
				
			} else if (jQuery(this).attr('type') == 'checkbox') {
				
				if (!form_data['custom'][jQuery(this).attr('name')]) {
				
					form_data['custom'][jQuery(this).attr('name')] = {};
				
				}
				
				if (jQuery(this).is(':checked')) {
				
					form_data['custom'][jQuery(this).attr('name')][i] = jQuery(this).val();
				
				}
				
			} else {
				
				form_data['custom'][jQuery(this).attr('name')] = jQuery(this).val();
				
			}
			
			i = i + 1;
			
		});
		
		var data = {
		
			'action' : 'cqpim_client_raise_support_ticket',
			
			'data' : form_data,	
		
		}
		
		jQuery.ajax({
		
			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				jQuery('#overlay').show();

				// disable form elements while awaiting data

				jQuery('#client_add_support').prop('disabled', true);

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){

			if(response.error == true) {
			
				jQuery('#overlay').hide();

				jQuery('#client_add_support').prop('disabled', false);
			
				alert(response.message);
				
				//jQuery.colorbox.resize();
				
			} else {

				//jQuery('#overlay').hide();

				jQuery('#client_add_support').prop('disabled', false);
				
				//jQuery(messages).html(response.message);

				window.location.replace(response.message);				
			
			}

		});
		
	});
	
	jQuery('#client_settings').submit(function(e) {
	
		e.preventDefault();
		
		var spinner = jQuery('#overlay');
		
		var messages = jQuery('#settings_messages');
		
		var user_id = jQuery('#client_user_id').val();
		
		var client_object = jQuery('#client_object').val();
		
		var client_type = jQuery('#client_type').val();
		
		var client_email = jQuery('#client_email').val();
		
		var client_phone = jQuery('#client_phone').val();
		
		var client_name = jQuery('#client_name').val();
		
		var company_name = jQuery('#company_name').val();
		
		var company_address = jQuery('#company_address').val();
		
		var company_postcode = jQuery('#company_postcode').val();
		
		var client_pass = jQuery('#client_pass').val();
		
		var client_pass_rep = jQuery('#client_pass_rep').val();
		
		var no_tasks = 0;
		
		var no_tasks_comment = 0;
		
		var no_tickets = 0;
		
		var no_tickets_comment = 0;
		
		if(jQuery('#no_tasks').is(":checked")) {
			
			no_tasks = 1;
			
		}
		
		if(jQuery('#no_tasks_comment').is(":checked")) {
			
			no_tasks_comment = 1;
			
		}
		
		if(jQuery('#no_tickets').is(":checked")) {
			
			no_tickets = 1;
			
		}
		
		if(jQuery('#no_tickets_comment').is(":checked")) {
			
			no_tickets_comment = 1;
			
		}
		
		var data = {
		
			'action' : 'cqpim_client_update_details',
			
			'user_id' : user_id,
			
			'client_object' : client_object,
			
			'client_type' : client_type,
			
			'client_email' : client_email,
			
			'client_phone' : client_phone,
			
			'client_name' : client_name,
			
			'company_name' : company_name,
			
			'company_address' : company_address,
			
			'company_postcode' : company_postcode,
			
			'client_pass' : client_pass,
			
			'client_pass_rep' : client_pass_rep,

			'no_tasks' : no_tasks,
			
			'no_tasks_comment' : no_tasks_comment,
			
			'no_tickets' : no_tickets,
			
			'no_tickets_comment' : no_tickets_comment
		
		}
		
		jQuery.ajax({

			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#client_settings_submit').prop('disabled', true);

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){

			if(response.error == true) {
			
				spinner.hide();

				jQuery('#client_settings_submit').prop('disabled', false);
			
				jQuery(messages).html(response.message);
				
				jQuery.colorbox.resize();
				
			} else {

				spinner.hide();

				jQuery('#client_settings_submit').prop('disabled', false);
				
				jQuery(messages).html(response.message);

				//location.reload();				
			
			}

		});
	
	});
	
	// Contacts
	
	jQuery('button.cancel-colorbox').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox.close();
	
	});
	
	jQuery('#add_client_team').click(function(e) {
	
		e.preventDefault();
		
		jQuery.colorbox({
	
			'inline': true,
			
			'fixed': true,
		
			'href': '#add_client_team_ajax',	

			'opacity': '0.5',
	
		});	
	
	});
	
	jQuery('.edit-milestone').click(function(e) {
	
		e.preventDefault();
		
		var key = jQuery(this).val();
		
		jQuery.colorbox({
	
			'inline': true,
			
			'fixed': true,
		
			'href': '#contact_edit_' + key,	

			'opacity': '0.5',
	
		});	
	
	});
	
	jQuery('#add_client_team_submit').click(function(e) {
	
		e.preventDefault();
		
		var contact_name = jQuery('#contact_name').val();
	
		var contact_telephone = jQuery('#contact_telephone').val();
		
		var contact_email = jQuery('#contact_email').val();
		
		var entity_id = jQuery('#post_ID').val();
		
		var spinner = jQuery('#overlay');
		
		if(jQuery('#send_contact_details').is(':checked')) {
		
			send = 1;
		
		} else {
		
			send = 0
		
		}
				
		var data = {
		
			'action' : 'cqpim_client_add_contact',
		
			'contact_name' : contact_name,
			
			'contact_telephone' : contact_telephone,
			
			'contact_email' : contact_email,
			
			'entity_id' : entity_id,
			
			'send' : send
		
		};
		
		jQuery.ajax({

			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#add_client_team_submit').prop('disabled', true);
				
				jQuery.colorbox.resize();

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#add_client_team_submit').prop('disabled', false);
			
				jQuery('#client_team_messages').html(response.message);
				
				jQuery.colorbox.resize();
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#add_client_team_submit').prop('disabled', false);
				
				jQuery('#client_team_messages').html(response.message);
				
				jQuery.colorbox.resize();
				
				location.reload();
			
			}

		});
	
	});	
	
	// Remove Team Member

	jQuery('.delete_team').click(function(e) {
	
		e.preventDefault();
		
		var key = jQuery(this).val();

		var project_id = jQuery('#post_ID').val();
		
		var domain = document.domain;
		
		var spinner = jQuery('#overlay');
		
		var data = {
		
			'action' : 'cqpim_remove_client_contact',
		
			'key' : key,
			
			'project_id' : project_id,
		
		};
		
		jQuery.ajax({

			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('.delete_team').prop('disabled', true);

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				jQuery('.delete_team').prop('disabled', false);
				
			} else {

				spinner.hide();

				jQuery('.delete_team').prop('disabled', false);
				
				location.reload();
			
			}

		});
	
	});
	
	jQuery('.contact_edit_submit').click(function(e) {
	
		e.preventDefault();
		
		var key = jQuery(this).val();

		var project_id = jQuery('#post_ID').val();
		
		var name = jQuery('#contact_name_' + key).val();
		
		var phone = jQuery('#contact_telephone_' + key).val();
		
		var email = jQuery('#contact_email_' + key).val();
		
		var password = jQuery('#new_password_' + key).val();
		
		var password2 = jQuery('#confirm_password_' + key).val();
		
		var spinner = jQuery('#overlay');
		
		var messages = jQuery('#client_team_messages_' + key);
		
		if(jQuery('#send_new_password_' + key).is(':checked')) {
		
			send = 1;
		
		} else {
		
			send = 0
		
		}
		
		var data = {
		
			'action' : 'cqpim_edit_client_contact',
		
			'key' : key,
			
			'project_id' : project_id,
			
			'name' : name,
			
			'email' : email,
			
			'phone' : phone,
			
			'password' : password,
			
			'password2' : password2,
			
			'send' : send
		
		};
		
		jQuery.ajax({

			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('.delete_team').prop('disabled', true);
				
				jQuery.colorbox.resize();

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				jQuery('.contact_edit_submit').prop('disabled', false);
				
				jQuery(messages).html(response.message);
				
				jQuery.colorbox.resize();
				
			} else {

				spinner.hide();

				jQuery('.contact_edit_submit').prop('disabled', false);
				
				jQuery(messages).html(response.message);
				
				jQuery.colorbox.resize();
				
				location.reload();
			
			}

		});
	
	});
	
	jQuery('#username').change(function() {
	
		jQuery('#login_messages').fadeOut(500);
	
	});
	
	jQuery('#password').change(function() {
	
		jQuery('#login_messages').fadeOut(500);
	
	});
	
	jQuery('#cqpim-login').submit(function(e) {
	
		e.preventDefault();
		
		var nonce = jQuery('#signonsecurity').val();
		
		var username = jQuery('#username').val();
		
		var password = jQuery('#password').val();
		
		var data = {
		
			'action' : 'cqpim_ajax_login',
			
			'nonce' : nonce,	
			
			'username' : username,
			
			'password' : password
		
		}
		
		jQuery.ajax({
		
			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				jQuery('#overlay').show();

				// disable form elements while awaiting data

				jQuery('#log-in').prop('disabled', true);

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){

			if(response.error == true) {
			
				jQuery('#overlay').hide();

				jQuery('#log-in').prop('disabled', false);
				
				jQuery('#login_messages').removeClass('success');
			
				jQuery('#login_messages').addClass('error');
				
				jQuery('#login_messages').html(response.message);
				
				jQuery('#login_messages').fadeIn(500);
				
				//jQuery.colorbox.resize();
				
			} else {

				jQuery('#overlay').hide();

				jQuery('#log-in').prop('disabled', false);
				
				jQuery('#login_messages').removeClass('error');
				
				jQuery('#login_messages').addClass('success');
				
				jQuery('#login_messages').html(response.message);
				
				jQuery('#login_messages').fadeIn(500);
				
				//jQuery(messages).html(response.message);

				window.location.replace(response.redirect);				
			
			}

		});
		
	});
	
	jQuery('#cqpim-reset-pass').submit(function(e) {
	
		e.preventDefault();
		
		var nonce = jQuery('#signonsecurity').val();
		
		var username = jQuery('#username').val();
		
		var data = {
		
			'action' : 'cqpim_ajax_reset',
			
			'nonce' : nonce,	
			
			'username' : username,
		
		}
		
		jQuery.ajax({
		
			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				jQuery('#overlay').show();

				// disable form elements while awaiting data

				jQuery('#log-in').prop('disabled', true);

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){

			if(response.error == true) {
			
				jQuery('#overlay').hide();

				jQuery('#log-in').prop('disabled', false);
				
				jQuery('#login_messages').removeClass('success');
			
				jQuery('#login_messages').addClass('error');
				
				jQuery('#login_messages').html(response.message);
				
				jQuery('#login_messages').fadeIn(500);
				
				//jQuery.colorbox.resize();
				
			} else {

				jQuery('#overlay').hide();

				jQuery('#log-in').prop('disabled', false);
				
				jQuery('#login_messages').removeClass('error');
				
				jQuery('#login_messages').addClass('success');
				
				jQuery('#login_messages').html(response.message);
				
				jQuery('#login_messages').fadeIn(500);
				
				jQuery('#username').val('');
				
				//jQuery(messages).html(response.message);

				//window.location.replace(response.redirect);				
			
			}

		});
		
	});
	
	jQuery('#reset_pass_conf').submit(function(e) {
	
		e.preventDefault();
		
		var hash = jQuery('#hash').val();
		
		var pass = jQuery('#password').val();
		
		var pass2 = jQuery('#password2').val();
		
		var data = {
		
			'action' : 'cqpim_ajax_reset_conf',
			
			'hash' : hash,	
			
			'pass' : pass,
			
			'pass2' : pass2
		
		}
		
		jQuery.ajax({
		
			url: client_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				jQuery('#overlay').show();

				// disable form elements while awaiting data

				jQuery('#log-in').prop('disabled', true);

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){

			if(response.error == true) {
			
				jQuery('#overlay').hide();

				jQuery('#log-in').prop('disabled', false);
				
				jQuery('#login_messages').removeClass('success');
			
				jQuery('#login_messages').addClass('error');
				
				jQuery('#login_messages').html(response.message);
				
				jQuery('#login_messages').fadeIn(500);
				
				//jQuery.colorbox.resize();
				
			} else {

				jQuery('#overlay').hide();

				jQuery('#log-in').prop('disabled', false);
				
				jQuery('#login_messages').removeClass('error');
				
				jQuery('#login_messages').addClass('success');
				
				jQuery('#login_messages').html(response.message);
				
				jQuery('#login_messages').fadeIn(500);
				
				jQuery('#password').val('');
				
				jQuery('#password2').val('');
				
				//jQuery(messages).html(response.message);

				//window.location.replace(response.redirect);				
			
			}

		});
		
	});

});