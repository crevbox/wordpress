jQuery(document).ready(function() {
	
	jQuery('.menu-open').on('click', function(e) {
		
		e.preventDefault;
		
		jQuery(this).hide();
		
		jQuery('#cqpim-dash-sidebar').show();
		
		jQuery('.menu-close').show();
		
	});
	
	jQuery('.menu-close').on('click', function(e) {
		
		e.preventDefault;
		
		jQuery(this).hide();
		
		jQuery('#cqpim-dash-sidebar').hide();
		
		jQuery('.menu-open').show();
		
	});

	jQuery('.refresh').on('click', function(e) {
	
		e.preventDefault();
	
		location.reload();
	
	});
	
	jQuery("#uploaded_files").load(location.href + " #uploaded_files");
	
	jQuery('.masonry-grid').masonry({
	  
		columnWidth: '.grid-sizer',
		itemSelector: '.grid-item',
		percentPosition: true
	  
	});
	
	jQuery('#update_task').click(function(e) {
	
		e.preventDefault();
		
		var file_task_id = jQuery('#file_task_id').val();	
		
		var add_task_message = jQuery('#add_task_message').val();	
		
		var owner = jQuery('#task_owner').val();	
		
		var data = {
		
			'action' : 'cqpim_client_update_task',
			
			'file_task_id' : file_task_id,
			
			'add_task_message' : add_task_message,
			
			'task_owner' : owner
		
		}
		
		jQuery.ajax({
		
			url: task_frontend.ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				jQuery('#overlay').show();

				// disable form elements while awaiting data

				jQuery('#update_task').prop('disabled', true);

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){

			if(response.error == true) {
			
				jQuery('#overlay').hide();

				jQuery('#update_task').prop('disabled', false);
			
				alert(response.message);
				
				//jQuery.colorbox.resize();
				
			} else {

				//jQuery('#overlay').hide();

				jQuery('#update_task').prop('disabled', false);
				
				//jQuery(messages).html(response.message);

				location.reload();				
			
			}

		});
		
	});

});