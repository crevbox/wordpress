jQuery(document).ready(function() {

	// add Task update miltseont dropdown via ajax
	
	jQuery('#task_project_id').change(function(e) {
	
		e.preventDefault();

		update_milestone_dropdown();
	
	});
	
	// Delete Time Entry
	
	jQuery('.time_remove').live('click', function(e) {
	
		e.preventDefault();
		
		var task_id = jQuery(this).data('task');
		
		var key = jQuery(this).data('key');
		
		var element = jQuery(this);
		
		var domain = document.domain;
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_remove_time_entry',
			
			'task_id' : task_id,
			
			'key' : key
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

				jQuery('a.time_remove').prop('disabled', true);

			},
			
		}).done(function(response){
					
			if(response.error == true) {
			
			spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('a.time_remove').prop('disabled', false);

				
			} else {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('a.time_remove').prop('disabled', false);
				
				jQuery(element).parents('li').fadeOut('slow');
				
				jQuery(element).parents('li').remove();
				
				location.reload();
				
			
			}				

		});
	
	});
	
	// AJAX Add Time from Timer
	
	jQuery('#add_time_ajax').live('click', function(e) {
	
		e.preventDefault();
		
		var task_id = jQuery('#task_time_task').val();
		
		var time = jQuery('#task_time_value').val();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_add_timer_time',
			
			'task_id' : task_id,
			
			'time' : time
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

				jQuery('#add_time_ajax').prop('disabled', true);

			},
			
		}).done(function(response){
					
			if(response.error == true) {
			
			spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#add_time_ajax').prop('disabled', false);
				
				jQuery('#time_messages').html(response.message);

				
			} else {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#add_time_ajax').prop('disabled', false);
				
				jQuery('#time_messages').html(response.message);
				
				location.reload();
				
			
			}				

		});
	
	});
	
	// AJAX Add Time from Timer
	
	jQuery('#add_mtime_ajax').live('click', function(e) {
	
		e.preventDefault();
		
		var task_id = jQuery('#task_time_task').val();
		
		var time = jQuery('#task_add_time').val();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_add_manual_task_time',
			
			'task_id' : task_id,
			
			'time' : time
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

				jQuery('#add_mtime_ajax').prop('disabled', true);

			},
			
		}).always(function(response){
		
			console.log(response);
		
		}).done(function(response){
					
			if(response.error == true) {
			
			spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#add_mtime_ajax').prop('disabled', false);
				
				alert(response.errors);

				
			} else {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#add_mtime_ajax').prop('disabled', false);
				
				location.reload();
				
			
			}				

		});
	
	});
	
	jQuery('#delete_task').live('click', function(e) {
	
		e.preventDefault();
		
		var task_id = jQuery(this).data('id');
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_delete_task_page',
			
			'task_id' : task_id,
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

				jQuery('#delete_task').prop('disabled', true);

			},
			
		}).done(function(response){
					
			if(response.error == true) {
			
			spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#delete_task').prop('disabled', false);

				
			} else {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#delete_task').prop('disabled', false);
				
				window.location.href = response.redirect;				
			
			}				

		});
	
	});
	
	// Delete Message
	
	jQuery('button.delete_message').click(function(e) {
	
		e.preventDefault();
		
		var project_id = jQuery('#post_ID').val();
		
		var key = jQuery(this).data('id');
		
		var domain = document.domain;
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_delete_task_message',
			
			'project_id' : project_id,
			
			'key' : key
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data
				
				spinner.show();

				jQuery('#button.delete_message').prop('disabled', true);

			},
			
		}).done(function(){
					
				location.reload();

		});
	
	});

});

function update_milestone_dropdown() {

		var project_id = jQuery('#task_project_id').val();
		
		var domain = document.domain;
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_populate_project_milestone',
			
			'ID' : project_id
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#task_project_id').prop('disabled', true);

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#task_project_id').prop('disabled', false);
			
				jQuery('#task_milestone_id').html(response.options);
				
				jQuery('#task_owner').html(response.team_options);
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#task_project_id').prop('disabled', false);
				
				jQuery('#task_milestone_id').html(response.options);
				
				jQuery('#task_owner').html(response.team_options);
				
			
			}

		});
		
}
		
