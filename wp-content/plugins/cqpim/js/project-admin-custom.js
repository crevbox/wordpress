jQuery(document).ready(function() {

	var scrollTop = localStorage.getItem('scrollTop');
	
    if (scrollTop !== null) {
	
        jQuery(window).scrollTop(Number(scrollTop));
		
        localStorage.removeItem('scrollTop');
		
    }

	jQuery('#publish').click(function(event) {

		localStorage.setItem('scrollTop', jQuery(window).scrollTop());
		
		return true;

	});
	
	jQuery('#dd-container').sortable({
	
		handle: '.dd-reorder',
		
		cancel: '',
		
		update: function( event, ui ) {
		
			var i = 0;
			
			jQuery('.dd-milestone').each(function(i) {
			
				jQuery(this).children('input.element_weight').val(i);
				
				i = i + 1;
			
			});
		
		}
	
	});
	
	jQuery('.dd-tasks').sortable({
	
		handle: '.dd-reorder',
		
		cancel: '',
		
		update: function( event, ui ) {
		
			var tasks = jQuery(this).find('.dd-task');
		
			var i = 0;
			
			weights = {};
			
			jQuery(tasks).each(function(i) {
					
				var task_id = jQuery(this).children('input.task_id').val();
			
				jQuery(this).children('input.task_weight').val(i);
				
				weights[task_id] = {
				
					'task_id' : task_id,
					
					'weight' : i,
				
				};
				
				i = i + 1;
			
			});
			
			//console.log(weights);
			
			var data = {

				'action' : 'cqpim_update_task_weight',
				
				'weights' : weights,

			};		
								
			jQuery.ajax({
			
				url: ajaxurl,

				data: data,

				type: 'POST',

				dataType: 'json'

			}).always(function(response){
			
				console.log(response);
			
			});
			
		}
		
	});
	
	jQuery('.dd-subtasks').sortable({
	
		handle: '.dd-reorder',
		
		cancel: '',
		
		update: function( event, ui ) {
		
			var tasks = jQuery(this).find('.dd-subtask');
		
			var i = 0;
			
			weights = {};
			
			jQuery(tasks).each(function(i) {
					
				var task_id = jQuery(this).children('input.task_id').val();
			
				jQuery(this).children('input.task_weight').val(i);
				
				weights[task_id] = {
				
					'task_id' : task_id,
					
					'weight' : i,
				
				};
				
				i = i + 1;
			
			});
			
			//console.log(weights);
			
			var data = {

				'action' : 'cqpim_update_task_weight',
				
				'weights' : weights,

			};		
								
			jQuery.ajax({
			
				url: ajaxurl,

				data: data,

				type: 'POST',

				dataType: 'json'

			}).always(function(response){
			
				console.log(response);
			
			});
			
		}
		
	});
	
	jQuery('.dataTable').dataTable({
	
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('.dataTable-PE').dataTable({
		"order": [[ 2, 'desc' ]],
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}	
	
	});
	
	jQuery('.author-other.no_access td.column-title span.post-state').remove();
	
	jQuery('.author-other.no_access td.column-title a').remove();
	
	jQuery('.author-other.no_access td.column-title a').remove();
	
	jQuery('.author-other.no_access td.column-title .row-actions').remove();
	
	jQuery('.author-other.no_access td.column-title strong').html(vars.assign_error);

	jQuery('.datepicker').datepicker({ 

		showButtonPanel: true,
		closeText: objectL10n.closeText,
		currentText: objectL10n.currentText,
		monthNames: objectL10n.monthNames,
		monthNamesShort: objectL10n.monthNamesShort,
		dayNames: objectL10n.dayNames,
		dayNamesShort: objectL10n.dayNamesShort,
		dayNamesMin: objectL10n.dayNamesMin,
		dateFormat: objectL10n.dateFormat,
		firstDay: objectL10n.firstDay,	
	
	});
	
	jQuery('button.cancel-colorbox').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox.close();
	
	});
	
	// Project Basics if new
	
	var type = jQuery('#project_ref_for_basics').val();
	
	if(!jQuery('.subsubsub').length && jQuery('#project_details').length) {
	
		if(!type) {

			jQuery.colorbox({
				
				'maxWidth':'95%',
				
				'inline': true,
					
				'href': '#quote_basics',							
						
				'overlayClose'  : false, 
				
				'escKey' : false,

				'opacity': '0.5',	

				'onLoad': function() {
				
					jQuery('#cboxClose').remove();
				
				},
				
			});	
			
			jQuery.colorbox.resize();
					
		}
	
	}
	
	jQuery('button.save-basics').click(function(e){
	
		e.preventDefault();
		
		var ref = jQuery('#quote_ref').val();
		
		var start = jQuery('#start_date').val();
		
		var finish = jQuery('#finish_date').val();
		
		if(ref && start && finish) {
		
			jQuery.colorbox.close();
			
			setTimeout(function(){
		
				jQuery('#publish').trigger('click');
			
			}, 500);
		
		} else {
		
			jQuery('#basics-error').html('<div class="cqpim-alert cqpim-alert-danger alert-display">' + vars.project_dates + '</div><div class="clear"></div>');
			
			jQuery.colorbox.resize();
		
		}
	
	});
	
	jQuery('#edit-quote-details').on('click', function(e){
	
		e.preventDefault();

			jQuery.colorbox({
				
				'width' : '500px',
				
				'maxWidth':'95%',
				
				'inline': true,
					
				'href': '#quote_basics',							

				'opacity': '0.5',	
				
			});	
			
			jQuery.colorbox.resize();	
	
	});
	
	jQuery('#unsigned_off').on('click', function(e){
	
		e.preventDefault();

			jQuery.colorbox({
				
				'width' : '500px',
				
				'maxWidth':'95%',
				
				'inline': true,
					
				'href': '#quote_unsign',							

				'opacity': '0.5',	
				
			});	
			
			jQuery.colorbox.resize();	
	
	});
	
	jQuery('a#add-milestone').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox({			
				
			'width' : '500px',
			
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#add-milestone-div',
			
			'opacity': '0.5',
			
		});	
		
		jQuery.colorbox.resize();
	
	});
	
	jQuery('a#apply-template').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox({			
				
			'width' : '500px',
			
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#apply-template-div',
			
			'opacity': '0.5',
			
		});	
		
		jQuery.colorbox.resize();
	
	});
	
	jQuery('a#clear-all').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox({
				
			'width' : '500px',
						
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#clear-all-div',
			
			'opacity': '0.5',
			
		});	
		
		jQuery.colorbox.resize();
	
	});
	
	jQuery('button.edit-milestone').on('click', function(e){
	
		e.preventDefault();
		
		var key = jQuery(this).val();
		
		jQuery.colorbox({
				
			'width' : '500px',
						
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#edit-milestone-' + key,
			
			'opacity': '0.5',
			
		});	
		
		jQuery.colorbox.resize();
	
	});
	
	jQuery('button#add_team_member').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox({
				
			'width' : '500px',
						
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#add_team_member_div',
			
			'opacity': '0.5',
			
		});
	
	});
	
	jQuery('button#add_message_trigger').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox({
				
			'width' : '500px',
						
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#add_message',
			
			'opacity': '0.5',
			
		});
	
	});
	
	jQuery('.add_task').on('click', function(e){
	
		e.preventDefault();
		
		var id = jQuery(this).data('ms');
		
		jQuery.colorbox({
				
			'width' : '500px',
						
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#add-task-div-' + id,
			
			'opacity': '0.5',
			
		});		
	
	});
	
	jQuery('.add_subtask').on('click', function(e){
	
		e.preventDefault();
		
		var id = jQuery(this).val();
		
		jQuery.colorbox({
			
			'width' : '500px',
			
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#add-subtask-div-' + id,
			
			'opacity': '0.5',
			
		});		
	
	});
	
	jQuery('button.edit-task').on('click', function(e){
	
		e.preventDefault();
		
		var key = jQuery(this).val();
		
		jQuery.colorbox({
				
			'width' : '500px',
						
			'maxWidth':'95%',
			
			'inline': true,
				
			'href': '#edit-task-div-' + key,
			
			'opacity': '0.5',
			
		});	
	
	});
	
	jQuery('button.save').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox.close();
		
		setTimeout(function(){
	
			jQuery('#publish').trigger('click');
		
		}, 500);
	
	});
	
	jQuery('button.save-milestone').on('click', function(e){
	
		e.preventDefault();
		
		var key = jQuery(this).val();
		
		var finished = jQuery('.finished-' + key).val();
		
		var status = jQuery('.status-' + key).val();
		
		if(status != 'complete') {
		
			jQuery.colorbox.close();
			
			setTimeout(function(){
		
				jQuery('#publish').trigger('click');
			
			}, 500);
		
		} else { 
		
			if(finished) {

				jQuery.colorbox.close();
				
				setTimeout(function(){
			
					jQuery('#publish').trigger('click');
				
				}, 500);			
			
			} else {
			
				jQuery('#update-ms-message-' + key).html('<p style="color:#f00">' + vars.ms_complete + '</p>');
				
				jQuery.colorbox.resize();
				
			
			}
		
		}
	
	});
	
	// Delete Quote Stage
	
	// Confirm
	
	jQuery(document).on('click', 'button.delete_stage_conf', function(e){
	
		e.preventDefault();
		
		var id = jQuery(this).val();
		
			jQuery.colorbox({
				
				
			'width' : '500px',
			
				
				'maxWidth':'95%',
			
				'inline': true,
				
				'fixed': true,
			
				'href': '#delete-milestone-div-' + id,	

				'opacity': '0.5',
					
			});			
	
	});
	
	// Cancel Delete
	
	jQuery(document).on('click', 'button.cancel_delete_stage', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox.close();
	
	});
	
	//Delete
	
	jQuery(document).on('click', 'button.delete_stage', function(e){
	
		e.preventDefault();
		
		var element = jQuery(this);
		
		var idToRemove = jQuery(element).val();

		var hiddenfield = '<input type="hidden" name="delete_stage[]" value="' + idToRemove + '" data-id="' + idToRemove + '" >';		

		// set a hidden field that will remove the data from the database

		jQuery(hiddenfield).appendTo('#add-milestone');

		// remove the div containing all the data

		jQuery(this).parents('div.quote_element_add').css('display', 'none');
					
		jQuery('#publish').trigger('click');

	});
	
    // Instantiates the variable that holds the media library frame.
    var meta_image_frame;
 
    // Runs when the image button is clicked.
    jQuery('#meta-image-button').click(function(e){
 
        e.preventDefault();
 
        // If the frame already exists, re-open it.
        if ( meta_image_frame ) {
            wp.media.editor.open();
            return;
        }
 
        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: meta_image.title,
            button: { text:  meta_image.button },
            library: { type: 'image' }
        });
 
        // Runs when an image is selected.
        meta_image_frame.on('select', function(){
 
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON();
 
            // Sends the attachment URL to our custom image input field.
            jQuery('#meta-image').val(media_attachment.url);
        });
 
        // Opens the media library frame.
        wp.media.editor.open();
    });
	
	window.send_to_editor = function(html) {

    var image_url = [];
	
	jQuery(html).each(function() {
	
		image_url.push(jQuery(this).attr('href'));
	
	});
	
	var image_field = jQuery('#meta_image').val();
	
	if(! image_field){
	
		jQuery('#meta_image').val(image_url);

	} else {
	
		jQuery('#meta_image').val(jQuery('#meta_image').val() + image_url)
	
	};
	
	}
	
	// Delete a File
	
	// DELETE SINGLE IMAGE OR FILE IN ACT

	jQuery(document).on('click', 'button.delete_file', function(e){		

		e.preventDefault();

		var attID = jQuery(this).data('id');
		
		jQuery('#cqpim_overlay').show();

		var hiddenField = '<input type="hidden" name="delete_file[]" value="' + attID + '" />';

		// add hidden field to ensure removal from db once post is saved

		jQuery(this).parents('div.inside').prepend(hiddenField);
		
		jQuery('#publish').trigger('click');

	});


});
