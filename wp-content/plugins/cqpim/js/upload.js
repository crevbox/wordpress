jQuery(document).ready(function() {
	
	jQuery('.cqpim-file-upload').on('change', function(e) {
		
		e.preventDefault();
		
		var element = jQuery(this);
		
		var id = jQuery(this).attr('id');

		var formData = new FormData();

		formData.append('action', 'upload-attachment');
		
		formData.append('async-upload', jQuery(this)[0].files[0]);
		
		formData.append('name', jQuery(this)[0].files[0].name);
		
		formData.append('_wpnonce', upload_config.nonce);

		jQuery.ajax({
			
			url: upload_config.upload_url,
			
			data: formData,
			
			processData: false,
			
			contentType: false,
			
			dataType: 'json',
			
			type: 'POST',
			
			beforeSend: function() {
				
				//jQuery(element).hide();
				
				jQuery('#upload_attachments').show().append('<span class="cqpim-alert cqpim-alert-info cqpim-uploading cqpim-messages-upload">' + upload_config.strings.uploading + '</span>'); 
				
			},
			
			success: function(resp) {
				
				console.log(resp);
				
				if ( resp.success ) {
					
					jQuery('.cqpim-uploading').remove();
					
					jQuery('.cqpim-upload-error').remove();
					
					jQuery('#upload_attachments').show().append('<span id="' + resp.data.id + '" class="cqpim-alert cqpim-alert-success cqpim-messages-upload">' + upload_config.strings.success + ' - ' + resp.data.filename + ' <i style="cursor:pointer" class="fa fa-trash btn-change-image" aria-hidden="true" data-id="' + resp.data.id + '"></i></span>');
					
					ids = jQuery('#upload_attachment_ids').val();
					
					if(!ids) {
						
						jQuery('#upload_attachment_ids').val(resp.data.id);
						
					} else {
					
						jQuery('#upload_attachment_ids').val(ids + ',' + resp.data.id);
						
					}

				} else {
					
					jQuery('.cqpim-uploading').remove();
					
					jQuery('.cqpim-upload-error').remove();
					
					jQuery('#upload_attachments').show().append('<span class="cqpim-alert cqpim-alert-danger cqpim-messages-upload cqpim-upload-error">' + upload_config.strings.error + '</span>');
					
					jQuery(element).show();
					
				}
				
				
				
			}
			
		});
		
	});	
	
	jQuery('.rcqpim-file-upload').on('change', function(e) {
		
		e.preventDefault();
		
		var element = jQuery(this);
		
		var id = jQuery(this).attr('id');

		var formData = new FormData();

		formData.append('action', 'upload-attachment');
		
		formData.append('async-upload', jQuery(this)[0].files[0]);
		
		formData.append('name', jQuery(this)[0].files[0].name);
		
		formData.append('_wpnonce', upload_config.nonce);

		jQuery.ajax({
			
			url: upload_config.upload_url,
			
			data: formData,
			
			processData: false,
			
			contentType: false,
			
			dataType: 'json',
			
			type: 'POST',
			
			beforeSend: function() {
				
				//jQuery(element).hide();
				
				jQuery('#rupload_attachments').show().append('<span class="cqpim-alert cqpim-alert-info cqpim-uploading cqpim-messages-upload">' + upload_config.strings.uploading + '</span>'); 
				
			},
			
			success: function(resp) {
				
				console.log(resp);
				
				if ( resp.success ) {
					
					jQuery('.cqpim-uploading').remove();
					
					jQuery('.cqpim-upload-error').remove();
					
					jQuery('#rupload_attachments').show().append('<span id="' + resp.data.id + '" class="cqpim-alert cqpim-alert-success cqpim-messages-upload">' + upload_config.strings.success + ' - ' + resp.data.filename + ' <i style="cursor:pointer" class="fa fa-trash btn-change-image" aria-hidden="true" data-id="' + resp.data.id + '"></i></span>');
					
					ids = jQuery('#rupload_attachment_ids').val();
					
					if(!ids) {
						
						jQuery('#rupload_attachment_ids').val(resp.data.id);
						
					} else {
					
						jQuery('#rupload_attachment_ids').val(ids + ',' + resp.data.id);
						
					}

				} else {
					
					jQuery('.cqpim-uploading').remove();
					
					jQuery('.cqpim-upload-error').remove();
					
					jQuery('#rupload_attachments').show().append('<span class="cqpim-alert cqpim-alert-danger cqpim-messages-upload cqpim-upload-error">' + upload_config.strings.error + '</span>');
					
					jQuery(element).show();
					
				}
				
				
				
			}
			
		});
		
	});	
	
	jQuery('.btn-change-image').live( 'click', function(e) {
		
		e.preventDefault();
		
		var id = jQuery(this).data('id');
		
		var element = jQuery('#' + id).remove();
		
		var list = jQuery('#upload_attachment_ids').val();
		
		var rlist = jQuery('#rupload_attachment_ids').val();
		
		var new_list = cqpim_removeValue(list,id,',');
		
		var rnew_list = cqpim_removeValue(rlist,id,',');
		
		jQuery('#upload_attachment_ids').val(new_list);
		
		jQuery('#rupload_attachment_ids').val(rnew_list);
		
	});
	
	jQuery('.cqpim-file-upload').on('click', function() {
		
		var id = jQuery(this).data('id');
		
		jQuery(this).val('');
		
		//jQuery('#upload_attachment_ids').val('');
		
	});
	
	jQuery('.rcqpim-file-upload').on('click', function() {
		
		var id = jQuery(this).data('id');
		
		jQuery(this).val('');
		
		//jQuery('#upload_attachment_ids').val('');
		
	});

});

var cqpim_removeValue = function(list, value, separator) {
	
  separator = separator || ",";
  
  var values = list.split(separator);
  
  for(var i = 0 ; i < values.length ; i++) {
	  
    if(values[i] == value) {
		
      values.splice(i, 1);
	  
      return values.join(separator);
	  
    }
	
  }
  
  return list;
  
}