jQuery(document).ready(function() {
	
	jQuery('.save').live('click', function(e){
	
		e.preventDefault();
		
			jQuery('#cqpim_overlay').show();
	
			jQuery('#publish').trigger('click');
	
	});

	jQuery('.repeater').repeater();
	
	jQuery('.datepicker').datepicker({ 

		showButtonPanel: true,
		closeText: objectL10n.closeText,
		currentText: objectL10n.currentText,
		monthNames: objectL10n.monthNames,
		monthNamesShort: objectL10n.monthNamesShort,
		dayNames: objectL10n.dayNames,
		dayNamesShort: objectL10n.dayNamesShort,
		dayNamesMin: objectL10n.dayNamesMin,
		dateFormat: objectL10n.dateFormat,
		firstDay: objectL10n.firstDay,

	});
	
	jQuery('.invoice_price').live('change', function() {
	
		var row = jQuery(this).data('row');
	
		calculate_totals(this, row);
	
	});
	
	jQuery('.line_tax').live('change', function() {
	
		var row = jQuery(this).data('row');
		
		var element = jQuery('#invoice_price_' + row);
	
		calculate_totals(element, row);
	
	});
	
	jQuery('.line_stax').live('change', function() {
	
		var row = jQuery(this).data('row');
		
		var element = jQuery('#invoice_price_' + row);
	
		calculate_totals(element, row);
	
	});
	
	jQuery('.invoice_qty').live('change', function() {
	
		var row = jQuery(this).data('row');
	
		var element = jQuery('#invoice_price_' + row);
	
		calculate_totals(element, row);
	
	});
	
	jQuery('.line_delete').live('click', function() {
	
		calculate_totals(this);
	
	});

});

function calculate_totals(element, row) {

		var qty = +jQuery('#invoice_qty_' + row).val();
		
		var price = +jQuery(element).val();
		
		var ltot = price * qty;
		
		ltot = ltot.toFixed(2);
		
		jQuery('#invoice_line_total_' + row).val(ltot);
	
		var sub = 0;
		
		jQuery('.invoice_line_total').each(function() {
		
			sub += Number(jQuery(this).val());
		
		});
		
		sub = sub.toFixed(2);
		
		jQuery('#invoice_subtotal').val(sub);
		
		// here
		
		var tax = 0;
		
		var stax = 0;
		
		var tax_rate = +jQuery('#tax_rate').val();
		
		var stax_rate = +jQuery('#stax_rate').val();
		
		jQuery('.invoice_line_total').each(function() {
		
			amount = Number(jQuery(this).val());
			
			row = jQuery(this).data('row');
			
			if(jQuery('#line_tax_' + row).is(':checked')) {
			
				
			
			} else {
			
				tax_figure = amount / 100 * tax_rate;
			
				tax = tax + tax_figure;
			
			}	

			if(jQuery('#line_stax_' + row).is(':checked')) {
			

			
			} else {
			
				stax_figure = amount / 100 * stax_rate;
			
				stax = stax + stax_figure;
			
			}			
		
		});
		
		//var tax = sub / 100 * tax_rate;
		
		//var stax = sub / 100 * stax_rate;
		
		// to here
		
		tax = tax.toFixed(2);
		
		stax = stax.toFixed(2);
		
		jQuery('#invoice_vat').val(tax);
		
		jQuery('#invoice_svat').val(stax);
		
		if(jQuery('#invoice_svat').length) {
		
			var total = +sub + +tax + +stax;
		
		} else {
		
			var total = +sub + +tax;
		
		}
		
		total = total.toFixed(2);
		
		jQuery('#invoice_total').val(total);

}