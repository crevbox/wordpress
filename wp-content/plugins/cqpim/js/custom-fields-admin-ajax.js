jQuery(document).ready(function() {



});

function cqpim_save_custom_fields() {
	
	var type = jQuery('#post_type').val();
	
	var builder = jQuery('#builder_data').val();
	
	var spinner = jQuery('.ajax_spinner');
	
	var data = {
		
		'action' : 'cqpim_save_custom_fields',
		
		'builder' : builder,
		
		'type' : type,
		
	}
	
	jQuery.ajax({

		url: ajaxurl,

		data: data,

		type: 'POST',

		dataType: 'json',

		beforeSend: function(){

			// show spinner

			spinner.show();

		},

	}).done(function(response){
	
		if(response.error == true) {
		
			spinner.hide();
			
			alert(cf_alerts.fail);
			
		} else {

			spinner.hide();
			
			alert(cf_alerts.done);

		}

	});
	
}