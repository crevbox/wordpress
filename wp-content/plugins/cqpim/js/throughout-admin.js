jQuery(document).ready(function() {
	
	jQuery('.cqpim_tooltip').tooltip();

	if(jQuery('body').hasClass('post-type-cqpim_tasks')) {
	
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-has-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-has-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-menu-open');
		
		jQuery('#toplevel_page_cqpim-dashboard').removeClass('wp-not-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard a.menu-top-last').addClass('wp-has-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard a.menu-top-last').removeClass('wp-not-current-submenu');
	
	};
	
	if(jQuery('body').hasClass('post-type-cqpim_support')) {
	
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-has-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-has-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-menu-open');
		
		jQuery('#toplevel_page_cqpim-dashboard').removeClass('wp-not-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard a.menu-top-last').addClass('wp-has-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard a.menu-top-last').removeClass('wp-not-current-submenu');
	
	};
	
	if(jQuery('body').hasClass('post-type-cqpim_expense')) {
	
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-has-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-has-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard').addClass('wp-menu-open');
		
		jQuery('#toplevel_page_cqpim-dashboard').removeClass('wp-not-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard a.menu-top-last').addClass('wp-has-current-submenu');
		
		jQuery('#toplevel_page_cqpim-dashboard a.menu-top-last').removeClass('wp-not-current-submenu');
	
	};
	
	jQuery('#test_piping').click(function(e) {
	
		e.preventDefault();
		
		var spinner = jQuery('#cqpim_overlay');	

		var cqpim_mail_server = jQuery('#cqpim_mail_server').val();
		
		var cqpim_piping_address = jQuery('#cqpim_piping_address').val();
		
		var cqpim_mailbox_name = jQuery('#cqpim_mailbox_name').val();
		
		var cqpim_mailbox_pass = jQuery('#cqpim_mailbox_pass').val();
		
		var data = {
		
			'action' : 'cqpim_test_piping',
			
			'cqpim_mail_server' : cqpim_mail_server,
			
			'cqpim_mailbox_name' : cqpim_mailbox_name,
			
			'cqpim_mailbox_pass' : cqpim_mailbox_pass,
		
		}
		
		jQuery.ajax({
		
			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){
			
				jQuery('#test_piping').prop('disabled', false);

				spinner.show();

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){

			if(response.error == true) {
			
				spinner.hide();

				jQuery('#test_piping').prop('disabled', false);
			
				alert(response.message);
				
			} else {

				spinner.hide();

				jQuery('#test_piping').prop('disabled', false);
				
				alert(response.message);
			
			}

		});
		
	});

});