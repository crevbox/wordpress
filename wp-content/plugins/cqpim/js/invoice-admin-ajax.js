jQuery(document).ready(function() {

	jQuery('#invoice_client').live('change', function(e) {
	
		e.preventDefault();
		
		var client_id = jQuery(this).val();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_update_client_contacts',
			
			'client_id' : client_id
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();
				
			} else {

				spinner.hide();

				jQuery('#client_contact').prop('disabled', false);
				
				jQuery('#client_contact').html(response.contacts);
			
			}

		});
	
	});

	var project = jQuery('#invoice_project').val();
	
	if(!project) {

		update_projects_dropdown();
	
	}

	jQuery('#invoice_client').change(function(e) {
	
		e.preventDefault();

		update_projects_dropdown()
	
	});
	
	// Send Invoice to client

	jQuery('#send_invoice').click(function(e) {
	
		e.preventDefault();
		
		jQuery('#messages').html('');
		
		var invoice_id = jQuery(this).data('id');
		
		var type = jQuery(this).data('type');
		
		var spinner = jQuery('#cqpim_overlay');
		
		var messages = jQuery('#messages');
		
		var domain = document.domain;
		
		var data = {
		
			'action' : 'cqpim_process_invoice_emails',
			
			'invoice_id' : invoice_id,
			
			'type' : type
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#send_invoice').prop('disabled', true);

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#send_invoice').prop('disabled', false);
			
				jQuery('#messages').html(response.errors);
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#send_invoice').prop('disabled', false);
				
				jQuery('#messages').html(response.message);
				
				jQuery('#publish').trigger('click');
			
			}

		});
	
	});
	
	jQuery('#mark_paid_trigger').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox({
			
			'inline': true,
				
			'href': '#invoice_payment',
			
			'opacity': '0.5',
			
		});	
		
		jQuery.colorbox.resize();
	
	});	
	
	// Invoice Paid

	jQuery('#mark_paid').click(function(e) {
	
		e.preventDefault();
		
		jQuery('#messages').html('');
		
		var invoice_id = jQuery(this).data('id');
		
		var amount = jQuery('#payment_amount').val();
		
		var notes = jQuery('#payment_notes').val();
		
		var spinner = jQuery('#cqpim_overlay');
		
		var messages = jQuery('#messages');
		
		var domain = document.domain;
		
		var data = {
		
			'action' : 'cqpim_mark_invoice_paid',
			
			'invoice_id' : invoice_id,
			
			'amount' : amount,
			
			'notes' : notes
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#mark_paid').prop('disabled', true);

			},

		}).always(function(response) {
		
			console.log(response);
		
		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#mark_paid').prop('disabled', false);
			
				jQuery('#messages').html(response.errors);
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#mark_paid').prop('disabled', false);
				
				jQuery('#messages').html(response.message);
				
				jQuery('#publish').trigger('click');
			
			}

		});
	
	});
	
	// Send Reminde / Overdue Emails

	jQuery('.send_reminder').click(function(e) {
	
		e.preventDefault();
		
		jQuery('#messages').html('');
		
		var invoice_id = jQuery(this).data('id');
		
		var type = jQuery(this).data('type');
		
		var spinner = jQuery('#cqpim_overlay');
		
		var messages = jQuery('#messages');
		
		var domain = document.domain;
		
		var data = {
		
			'action' : 'cqpim_send_invoice_reminders',
			
			'invoice_id' : invoice_id,
			
			'type' : type
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#send_reminder').prop('disabled', true);

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#send_reminder').prop('disabled', false);
			
				jQuery('#messages').html(response.errors);
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#send_reminder').prop('disabled', false);
				
				jQuery('#messages').html(response.message);
				
				location.reload();
			
			}

		});
	
	});
	
		// Delete a payment_amount

	jQuery('button.delete_stage_conf').on('click', function(e){
	
		e.preventDefault();
		
		var payment_id = jQuery(this).val();
		
		var invoice_id = jQuery('#post_ID').val();
				
		var domain = document.domain;
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_delete_payment',
			
			'payment_id' : payment_id,
			
			'invoice_id' : invoice_id
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('button.delete_stage_conf').prop('disabled', true);

			},

		}).always(function(response) {

			console.log(response);
		
		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('button.delete_stage_conf').prop('disabled', false);
			
				alert(response.errors);
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('button.delete_stage_conf').prop('disabled', false);
				
				location.reload();
			
			}

		});		
	
	
	});
	
	jQuery('.edit-milestone').on('click', function(e){
	
		e.preventDefault();
		
		var key = jQuery(this).val();
		
		jQuery.colorbox({
			
			'inline': true,
				
			'href': '#invoice_payment_' + key,
			
			'opacity': '0.5',
			
		});	
		
		jQuery.colorbox.resize();
	
	});
	
	jQuery('button#edit_paid').on('click', function(e){
	
		e.preventDefault();
		
		var payment_id = jQuery(this).attr('data-key');
		
		var invoice_id = jQuery('#post_ID').val();
		
		var amount = jQuery('#payment_amount_' + payment_id).val();
	
		var notes = jQuery('#payment_notes_' + payment_id).val();
				
		var domain = document.domain;
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_edit_payment',
			
			'payment_id' : payment_id,
			
			'invoice_id' : invoice_id,
			
			'amount' : amount,
			
			'notes' : notes
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('button#edit_paid').prop('disabled', true);

			},

		}).always(function(response) {

			console.log(response);
		
		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('button#edit_paid').prop('disabled', false);
			
				alert(response.errors);
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('button#edit_paid').prop('disabled', false);
				
				location.reload();
			
			}

		});		
	
	
	});

});

function update_projects_dropdown() {

		var client_id = jQuery('#invoice_client').val();
		
		var domain = document.domain;
		
		var spinner = jQuery('#cqpim_overlay');
		
		var data = {
		
			'action' : 'cqpim_populate_invoice_projects',
			
			'ID' : client_id
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#invoice_client').prop('disabled', true);

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#invoice_client').prop('disabled', false);
			
				jQuery('#invoice_project').html(response.options);
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#invoice_client').prop('disabled', false);
				
				jQuery('#invoice_project').html(response.options);
				
			
			}

		});
		
};