jQuery(document).ready(function() {
	
	jQuery('#tabs').tabs();
	
	jQuery('.colorbox').on('click', function(e) {
	
		e.preventDefault();
	
		var anc = jQuery(this).attr('href');
		
		var id = anc.replace('#', '');
		
		var thediv = jQuery('#' + id);
		
		jQuery(thediv).parent('div').attr('id', id + '_container');
		
		jQuery.colorbox({
			
			'inline': true,
				
			'href': '#' + id,
			
			'opacity': '0.5',
			
		});	
	
	});
	
	jQuery('#create_linked_team').on('click', function(e) {
	
		e.preventDefault();
		
		var user_id = jQuery(this).data('uid');
		
		var spinner = jQuery('#cqpim_overlay');
	
		var data = {
		
			'action' : 'cqpim_create_team_from_admin',
			
			'user_id' : user_id,
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// show spinner

				spinner.show();

				// disable form elements while awaiting data

				jQuery('#create_linked_team').prop('disabled', true);

			},

		}).done(function(response){
		
			if(response.error == true) {
			
				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#create_linked_team').prop('disabled', false);
				
				alert('Something went wrong, pleae check your WordPress account to make sure the Display Name field has been completed. Then try again.');
				
			} else {

				spinner.hide();

				// re-enable form elements so that new enquiry can be posted

				jQuery('#create_linked_team').prop('disabled', false);
				
				location.reload();
			
			}

		});	
	
	});
	
	jQuery('.cancel-colorbox').on('click', function(e){
	
		e.preventDefault();
		
		jQuery.colorbox.close();
	
	})
	
	jQuery('#reset-cqpim').on('click', function(e){
	
		e.preventDefault();

			jQuery.colorbox({
				
				'maxWidth':'95%',
				
				'inline': true,
					
				'href': '#reset_cqpim',							

				'opacity': '0.5',	
				
			});	
			
			jQuery.colorbox.resize();	
	
	});
	
	jQuery('.reset-cqpim-conf').on('click', function(e) {
	
		e.preventDefault();
		
		jQuery.colorbox.close();
	
		var data = {
		
			'action' : 'cqpim_remove_all_data',
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				// disable form elements while awaiting data

				jQuery('.reset-cqpim-conf').prop('disabled', true);

			},

		}).done(function(response){
			
			location.replace(response.redirect);
			
		});	
	
	});
	
	jQuery('.remove_logo').on('click', function(e) {
	
		e.preventDefault();
		
		var type = jQuery(this).data('type');		
	
		var data = {
		
			'action' : 'cqpim_remove_logo',
			
			'type' : type,
		
		};
		
		jQuery.ajax({

			url: ajaxurl,

			data: data,

			type: 'POST',

			dataType: 'json',

			beforeSend: function(){

				jQuery('#cqpim_overlay').show();

			},

		}).done(function(response){
			
			location.reload()
			
		});	
	
	});

});

