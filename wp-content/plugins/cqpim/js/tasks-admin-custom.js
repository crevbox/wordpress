jQuery(document).ready(function() {

	jQuery('body').on('focus',".datepicker", function(){
	
		jQuery(this).datepicker({ 

		showButtonPanel: true,
		closeText: objectL10n.closeText,
		currentText: objectL10n.currentText,
		monthNames: objectL10n.monthNames,
		monthNamesShort: objectL10n.monthNamesShort,
		dayNames: objectL10n.dayNames,
		dayNamesShort: objectL10n.dayNamesShort,
		dayNamesMin: objectL10n.dayNamesMin,
		dateFormat: objectL10n.dateFormat,
		firstDay: objectL10n.firstDay,

		});
	
	});
	
	jQuery('.dataTable').dataTable({
	
		"language": {
			"processing":     dti18n.sProcessing,
			"search":         dti18n.sSearch,
			"lengthMenu":     dti18n.sLengthMenu,
			"info":           dti18n.sInfo,
			"infoEmpty":      dti18n.sInfoEmpty,
			"infoFiltered":   dti18n.sInfoFiltered,
			"infoPostFix":    dti18n.sInfoPostFix,
			"loadingRecords": dti18n.sLoadingRecords,
			"zeroRecords":    dti18n.sZeroRecords,
			"emptyTable":     dti18n.sEmptyTable,
			"paginate": {
				"first":      dti18n.sFirst,
				"previous":   dti18n.sPrevious,
				"next":       dti18n.sNext,
				"last":       dti18n.sLast
			},
			"aria": {
				"sortAscending":  dti18n.sSortAscending,
				"sortDescending": dti18n.sSortDescending
			}
		}
	
	});
	
	jQuery('#task_pc').on('change', function() {
	
		var pc = jQuery(this).val();
		
		if(pc == 0) {
		
			jQuery('#task_status').val('pending');
		
		} else if(pc == 100) {
		
			jQuery('#task_status').val('complete');
		
		} else {
		
			jQuery('#task_status').val('progress');
		
		}
	
	});
	
	jQuery('button.s_button').on('click', function(e){
	
		e.preventDefault();
		
		jQuery('#cqpim_overlay').show();
		
		setTimeout(function(){
	
			jQuery('#publish').trigger('click');
		
		}, 500);
	
	});
	
	jQuery(document).on('click', 'button.delete_file', function(e){		

		e.preventDefault();

		var attID = jQuery(this).data('id');
		
		jQuery('#cqpim_overlay').show();

		var hiddenField = '<input type="hidden" name="delete_file[]" value="' + attID + '" />';

		// add hidden field to ensure removal from db once post is saved

		jQuery(this).parents('div.inside').prepend(hiddenField);
		
		jQuery('#publish').trigger('click');

	});
	
	var hasTimer = false;
	// Init timer start
	jQuery('.start-timer-btn').on('click', function(e) {
		e.preventDefault();
		hasTimer = true;
		jQuery('.timer').timer({
			editable: false,
			format: '%H:%M:%S' 
		});
		jQuery(this).addClass('hidden');
		jQuery('.pause-timer-btn, .remove-timer-btn').removeClass('hidden');
	});

	// Init timer resume
	jQuery('.resume-timer-btn').on('click', function(e) {
		e.preventDefault();
		jQuery('.timer').timer('resume');
		jQuery(this).addClass('hidden');
		jQuery('.pause-timer-btn, .remove-timer-btn').removeClass('hidden');
	});


	// Init timer pause
	jQuery('.pause-timer-btn').on('click', function(e) {
		e.preventDefault();
		jQuery('.timer').timer('pause');
		jQuery(this).addClass('hidden');
		jQuery('.resume-timer-btn').removeClass('hidden');
	});

	// Remove timer
	jQuery('.remove-timer-btn').on('click', function(e) {
		e.preventDefault();
		hasTimer = false;
		jQuery('.timer').timer('remove');
		jQuery(this).addClass('hidden');
		jQuery('.start-timer-btn').removeClass('hidden');
		jQuery('.pause-timer-btn, .resume-timer-btn').addClass('hidden');
	});

	// Additional focus event for this demo
	jQuery('.timer').on('focus', function(e) {
		e.preventDefault();
		if(hasTimer) {
			jQuery('.pause-timer-btn').addClass('hidden');
			jQuery('.resume-timer-btn').removeClass('hidden');
		}
	});

	// Additional blur event for this demo
	jQuery('.timer').on('blur', function(e) {
		e.preventDefault();
		if(hasTimer) {
			jQuery('.pause-timer-btn').removeClass('hidden');
			jQuery('.resume-timer-btn').addClass('hidden');
		}
	});

});