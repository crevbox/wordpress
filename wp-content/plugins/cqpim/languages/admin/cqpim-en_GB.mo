��          <      \       p      q   �   �   �   x  �       �  �     �                      Create Linked Team Member It would appear that the Wordpress Administrator account that you are logged in with is not related to a CQPIM Team Member. In order for the plugin to work correctly, you need to add a Team Member that is linked to your WP User Account. We can do this for you though, just click Create Linked Team Member. You will then be able to add other team members or just work with this account. Project-Id-Version: 
POT-Creation-Date: 2017-09-08 10:05+0100
PO-Revision-Date: 2017-09-08 10:05+0100
Last-Translator: 
Language-Team: TimelessInteractive+ <hello@timeless-interactive.com>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _e;__;_x;_n;_n:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: assets
 Create Linked Team Member It would appear that the Wordpress Administrator account that you are logged in with is not related to a CQPIM Team Member. In order for the plugin to work correctly, you need to add a Team Member that is linked to your WP User Account. We can do this for you though, just click Create Linked Team Member. You will then be able to add other team members or just work with this account. 