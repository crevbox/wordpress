<?php

	add_action( 'admin_menu' , 'register_cqpim_profile_page', 9 ); 

	function register_cqpim_profile_page() {

		$mypage = add_submenu_page(	
					'cqpim-dashboard',
					__('My Profile', 'cqpim'),		
					__('My Profile', 'cqpim'), 			
					'cqpim_team_edit_profile', 			
					'cqpim-manage-profile', 		
					'cqpim_team_profile'
		);

		add_action( 'load-' . $mypage, 'enqueue_cqpim_dash_option_scripts' );
		


	}


	function cqpim_team_profile() { 
	
		$user = wp_get_current_user(); 
		
		$roles = $user->roles;
		
		$args = array(
		
			'post_type' => 'cqpim_teams',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
		
		);
		
		$members = get_posts($args);
		
		foreach($members as $member) {
		
			$team_details = get_post_meta($member->ID, 'team_details', true);
			
			if($team_details['user_id'] == $user->ID) {
			
				$assigned = $member->ID;
			
			}
		
		}
		
		if(empty($assigned)) {
		
			$assigned = '';
			
		}
		
		$team_details = get_post_meta($assigned, 'team_details', true);
		
		print_r($team_details);
		
		?>
	
	<br />
	
	<div class="cqpim-dash-item-full">
	
		<div class="cqpim_block">
		
			<div class="cqpim_block_title">
			
				<div class="caption">
					
					<i class="fa fa-pencil-square-o font-green-sharp" aria-hidden="true"></i>
						
					<span class="caption-subject font-green-sharp sbold"><?php _e('My Profile', 'cqpim'); ?> </span>
				
				</div>
				
			</div>
				
				test
			
		</div>
			
	</div>
			
	<?php }