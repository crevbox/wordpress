<?php 

	function cqpim_convert_date($date, $format = 0) {
	
		if(empty($format)) {
		
			$format = get_option('cqpim_date_format');
			
		}
		
		if(!empty($date)) {
	
			$date = DateTime::createFromFormat($format, $date);
		
			$date = $date->getTimestamp();
		
		}	
		
		return $date;
	
	}

	/********* 1. Client FUNCTIONS *************/
	
	// Client Update Details
	
	add_action( "wp_ajax_nopriv_cqpim_client_update_details", 

			"cqpim_client_update_details");

	add_action( "wp_ajax_cqpim_client_update_details", 

		  	"cqpim_client_update_details");
	
	function cqpim_client_update_details() {
	
		$data = isset($_POST) ? $_POST : '';
		
		$user_id = isset($data['user_id']) ? $data['user_id']: '';
		
		$client_object = isset($data['client_object']) ? $data['client_object']: '';
		
		$client_type = isset($data['client_type']) ? $data['client_type']: '';
		
		$client_email = isset($data['client_email']) ? $data['client_email']: '';
		
		$client_phone = isset($data['client_phone']) ? $data['client_phone']: '';
		
		$client_name = isset($data['client_name']) ? $data['client_name']: '';
		
		$company_name = isset($data['company_name']) ? $data['company_name']: '';
		
		$company_address = isset($data['company_address']) ? $data['company_address']: '';
		
		$company_postcode = isset($data['company_postcode']) ? $data['company_postcode']: '';
		
		$client_pass = isset($data['client_pass']) ? $data['client_pass']: '';
		
		$client_pass_rep = isset($data['client_pass_rep']) ? $data['client_pass_rep']: '';
		
		// Email Preferences
		
		$no_tasks = isset($data['no_tasks']) ? $data['no_tasks']: 0;
		
		$no_tasks_comment = isset($data['no_tasks_comment']) ? $data['no_tasks_comment']: 0;
		
		$no_tickets = isset($data['no_tickets']) ? $data['no_tickets']: 0;
		
		$no_tickets_comment = isset($data['no_tickets_comment']) ? $data['no_tickets_comment']: 0;
		
		if(empty($user_id) || empty($client_object) || empty($client_type)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There is some missing data. The update has failed.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();		
		
		} else {
		
			$email = email_exists($client_email);
			
			$username = username_exists($client_email);
		
			if( !empty($email) && $email != $user_id  || !empty($username) && $username != $user_id ) {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The email address entered is already in the system, please try another.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();			
			
			} else {
			
				if(!empty($client_pass) || !empty($client_pass_rep)) {
				
					if($client_pass != $client_pass_rep) {
					
						$return =  array( 

							'error' 	=> true,

							'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The Passwords do not match. Please try again.', 'cqpim') . '</div>',

						);
						
						header('Content-type: application/json');
						
						echo json_encode($return);

						exit();						
					
					} else {
					
						$user = wp_get_current_user();
						
						if($user->ID != $user_id) {
						
							$return =  array( 

								'error' 	=> true,

								'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Cheatin\' uh? Better luck next time.', 'cqpim') . '</div>',

							);
							
							header('Content-type: application/json');
							
							echo json_encode($return);

							exit();							
						
						} else {
						
							wp_set_password( $client_pass, $user_id );
							
							wp_set_auth_cookie( $user_id, '', '' );
						
						}
					
					}
				
				}
			
				$user_data = array(
				
					'ID' => $user_id,
					
					'display_name' => $client_name,
					
					'first_name' => $client_name,
					
					'user_email' => $client_email,
				
				);
				
				wp_update_user($user_data);	
				
				$client_notifications = array(
				
					'no_tasks' => $no_tasks,
					
					'no_tasks_comment' => $no_tasks_comment,
					
					'no_tickets' => $no_tickets,
					
					'no_tickets_comment' => $no_tickets_comment,
				
				);	
				
				if($client_type == 'admin') {

					$client_details = get_post_meta($client_object, 'client_details', true);
					
					$client_details['client_contact'] = $client_name;
					
					$client_details['client_telephone'] = $client_phone;
					
					$client_details['client_email'] = $client_email;

					$client_details['client_company'] = $company_name;
					
					$client_details['client_address'] = $company_address;
					
					$client_details['client_postcode'] = $company_postcode;

					update_post_meta($client_object, 'client_details', $client_details);
					
					$client_updated = array(
					
						'ID' => $client_object,
						
						'post_title' => $company_name,
					
					);
					
					wp_update_post($client_updated);				
					
					update_post_meta($client_object, 'client_notifications', $client_notifications);									
					
				} else {
				
					$user = wp_get_current_user();
				
					$client_contacts = get_post_meta($client_object, 'client_contacts', true);
					
					$client_contacts[$user->ID]['telephone'] = $client_phone;
					
					$client_contacts[$user->ID]['name'] = $client_name;
					
					$client_contacts[$user->ID]['email'] = $client_email;
					
					$client_contacts[$user->ID]['notifications'] = $client_notifications;
					
					update_post_meta($client_object, 'client_contacts', $client_contacts);
				
				}
		
				$return =  array( 

					'error' 	=> false,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Details successfully updated.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();	

			}
		
		}			
	
	}
	
	// Client add contact
	
	add_action( "wp_ajax_nopriv_cqpim_client_add_contact", 

			"cqpim_client_add_contact");

	add_action( "wp_ajax_cqpim_client_add_contact", 

		  	"cqpim_client_add_contact");

	function cqpim_client_add_contact() {
	
		$data = isset($_POST) ? $_POST : '';
		
		$client_id = isset($data['entity_id']) ? $data['entity_id'] : '';
		
		$contact_name = isset($data['contact_name']) ? $data['contact_name'] : '';
		
		$contact_telephone = isset($data['contact_telephone']) ? $data['contact_telephone'] : '';
		
		$contact_email = isset($data['contact_email']) ? $data['contact_email'] : '';
		
		$send = isset($data['send']) ? $data['send'] : '';
		
		if(empty($client_id) || empty($contact_name) || empty($contact_telephone) || empty($contact_email)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Please complete all fields.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		} else {
		
			$email = email_exists($contact_email);
			
			$username = username_exists($contact_email);
		
			if(!empty($email)  || !empty($username)) {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The email address entered is already in the system, please try another.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();				
			
			} else {
			
				$password = cqpim_random_string(10);
				
				$user_id = wp_create_user( $contact_email, $password, $contact_email );
				
				$user = new WP_User( $user_id );
				
				$user->set_role( 'cqpim_client' );
				
				$user_data = array(
				
					'ID' => $user_id,
					
					'display_name' => $contact_name,
					
					'first_name' => $contact_name,
				
				);
				
				wp_update_user($user_data);
				
				$contacts = get_post_meta($client_id, 'client_contacts', true);
				
				$contacts[$user_id] = array(
				
					'user_id' => $user_id,
					
					'name' => $contact_name,
					
					'email' => $contact_email,
					
					'telephone' => $contact_telephone
				
				);
				
				update_post_meta($client_id, 'client_contacts', $contacts);
				
				$ids = get_post_meta($client_id, 'client_ids', true);
				
				if(empty($ids)) {
				
					$ids = array();
					
				}
				
				$ids[] = $user_id;
				
				update_post_meta($client_id, 'client_ids', $ids);
				
				if($send == 1) {							
		
					$email_subject = get_option('added_contact_subject');
					
					$email_content = get_option('added_contact_content');
					
					$email_subject = cqpim_replacement_patterns($email_subject, $client_id, '');
					
					$email_content = cqpim_replacement_patterns($email_content, $client_id, '');
					
					$email_content = str_replace('%%CONTACT_NAME%%', $contact_name, $email_content);
					
					$email_content = str_replace('%%CONTACT_EMAIL%%', $contact_email, $email_content);
					
					$email_content = str_replace('%%CONTACT_PASSWORD%%', $password, $email_content);
					
					$to = $contact_email;
					
					$attachments = array();
					
					if(cqpim_send_emails( $to, $email_subject, $email_content, '', $attachments, 'sales' )) {

						$return =  array( 

							'error' 	=> false,

							'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Contact has been created. An email has been sent to the contact.', 'cqpim') . '</div>',

						);
						
						header('Content-type: application/json');
						
						echo json_encode($return);	

						exit();	

					} else {
					
						$return =  array( 

							'error' 	=> true,

							'message' 	=> '<div class="cqpim-alert cqpim-alert-warning alert-display">' . __('The contact was added but the email failed to send. Check you have completed the email subject and content fields in the plugin settings.', 'cqpim') . '</div>',

						);
						
						header('Content-type: application/json');
						
						echo json_encode($return);

						exit();						
					
					}
				
				} else {
		
					$return =  array( 

						'error' 	=> false,

						'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Contact has been created. No email has been sent.', 'cqpim') . '</div>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	

					exit();	
					
				}

			}
		
		}

	}
	
	// Remove Client Contact
	
	add_action( "wp_ajax_nopriv_cqpim_remove_client_contact", 

			"cqpim_remove_client_contact");

	add_action( "wp_ajax_cqpim_remove_client_contact", 

		  	"cqpim_remove_client_contact");
	
	function cqpim_remove_client_contact() {
	
		$key = isset($_POST['key']) ? $_POST['key'] : '';
		
		$client_id = isset($_POST['project_id']) ? $_POST['project_id'] : '';
		
		$client_contacts = get_post_meta($client_id, 'client_contacts', true);
		
		$user_id = $client_contacts[$key]['user_id'];
		
		wp_delete_user($user_id);
		
		unset($client_contacts[$key]);
		
		$client_contacts = array_filter($client_contacts);
		
		update_post_meta($client_id, 'client_contacts', $client_contacts);
		
		$client_ids = get_post_meta($client_id, 'client_ids', true);
		
		if(!is_array($client_ids)) {
			
			$client_ids = array($client_ids);
			
		}
		
		foreach($client_ids as $key => $client_id_ind) {
			
			if($client_id_ind == $user_id) {
				
				unset($client_ids[$key]);
				
			}
			
		}
		
		update_post_meta($client_id, 'client_ids', $client_ids);
				
		$return =  array( 

			'error' 	=> false,

			'message' 	=> '',

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit();	
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_edit_client_contact", 

			"cqpim_edit_client_contact");

	add_action( "wp_ajax_cqpim_edit_client_contact", 

		  	"cqpim_edit_client_contact");
	
	function cqpim_edit_client_contact() {
	
		$data = isset($_POST) ? $_POST : '';
		
		$client_id = isset($data['project_id']) ? $data['project_id'] : '';
		
		$key = isset($data['key']) ? $data['key'] : '';
		
		$admin = isset($_POST['admin']) ? $_POST['admin'] : '';
		
		$contact_name = isset($data['name']) ? $data['name'] : '';
		
		$contact_telephone = isset($data['phone']) ? $data['phone'] : '';
		
		$contact_email = isset($data['email']) ? $data['email'] : '';
		
		$password = isset($data['password']) ? $data['password'] : '';
		
		$password2 = isset($data['password2']) ? $data['password2'] : '';
		
		$send = isset($data['send']) ? $data['send'] : '';
		
		$no_tasks = isset($data['no_tasks']) ? $data['no_tasks']: 0;
		
		$no_tasks_comment = isset($data['no_tasks_comment']) ? $data['no_tasks_comment']: 0;
		
		$no_tickets = isset($data['no_tickets']) ? $data['no_tickets']: 0;
		
		$no_tickets_comment = isset($data['no_tickets_comment']) ? $data['no_tickets_comment']: 0;

		if(empty($contact_name) || empty($contact_telephone) || empty($contact_email)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Missing Data. Please ensure you complete all fields.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			exit();			
		
		} else {
		
			$contacts = get_post_meta($client_id, 'client_contacts', true);
			
			$user_id = $contacts[$key]['user_id'];
			
			$email = email_exists($contact_email);
			
			$username = username_exists($contact_email);
				
			if( !empty($email) && $email != $user_id  || !empty($username) && $username != $user_id ) {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The email address entered is already in the system, please try another.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

				exit();				
			
			} else {
			
				if(!empty($password) || !empty($password2)) {
				
					if($password != $password2) {
					
						$return =  array( 

							'error' 	=> true,

							'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The passwords do not match.', 'cqpim') . '</div>',

						);
						
						header('Content-type: application/json');
						
						echo json_encode($return);

						exit();				
					
					} else {
					
						wp_set_password($password2, $user_id);
						
						if(empty($admin)) {
						
							wp_set_auth_cookie( $user_id, '', '' );
						
						}
						
						if($send == 1) {
						
							$email_subject = get_option('password_reset_subject');
							
							$email_content = get_option('password_reset_content');
							
							$email_content = str_replace('%%CLIENT_NAME%%', $contact_name, $email_content);
							
							$email_content = str_replace('%%CLIENT_EMAIL%%', $contact_email, $email_content);
							
							$email_subject = cqpim_replacement_patterns($email_subject, $entity_id, '');
				
							$email_content = cqpim_replacement_patterns($email_content, $entity_id, '');

							$to = $contact_email;
							
							$email_content = str_replace('%%NEW_PASSWORD%%', $password2, $email_content);
							
							$attachments = array();
							
							cqpim_send_emails($to, $email_subject, $email_content, '', $attachments, 'sales');						
						
						}
					
					}
				
				}
			
				$contacts[$key] = array(
				
					'user_id' => $user_id,
					
					'name' => $contact_name,
					
					'email' => $contact_email,
					
					'telephone' => $contact_telephone			
				
				);
				
				$contacts[$key]['notifications'] = array(
				
					'no_tasks' => $no_tasks,
					
					'no_tasks_comment' => $no_tasks_comment,
					
					'no_tickets' => $no_tickets,
					
					'no_tickets_comment' => $no_tickets_comment,
				
				);	
				
				update_post_meta($client_id, 'client_contacts', $contacts);
				
				$user_details = array(
				
					'ID' => $user_id,
					
					'display_name' => $contact_name,
					
					'first_name' => $contact_name,
					
					'user_email' => $contact_email,				
				
				);
				
				wp_update_user($user_details);
		
				$return =  array( 

					'error' 	=> false,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Update Successful.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();	
			
			}

		}
	
	}

	
	/********* 2. QUOTE FUNCTIONS *************/
	
	// Update Client Contact DD
	
	add_action( "wp_ajax_nopriv_cqpim_update_client_contacts", 

			"cqpim_update_client_contacts");

	add_action( "wp_ajax_cqpim_update_client_contacts", 

		  	"cqpim_update_client_contacts");

	function cqpim_update_client_contacts() {
	
		$data = isset($_POST) ? $_POST : array();
		
		$client_id = isset($data['client_id']) ? $data['client_id'] : '';
		
		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$client_contact = '';
		
		$main_user_id = $client_details['user_id'];
		
		$client_contact .= '<option value="' . $main_user_id . '">' . $client_details['client_contact'] . ' - ' . sprintf(__('Main Contact', 'cqpim')) . '</option>';
		
		$client_contacts = get_post_meta($client_id, 'client_contacts', true);
		
		if(empty($client_contacts)) {
		
			$client_contacts = array();
			
		}
		
		foreach($client_contacts as $contact) {
		
			$client_contact .= '<option value="' . $contact['user_id'] . '">' . $contact['name'] . '</option>';
		
		}

		$return =  array( 

			'error' 	=> false,

			'contacts' 	=> $client_contact,

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);
		
		exit();

	}	
	
	// Add Step to Quote / Project

	add_action( "wp_ajax_nopriv_cqpim_add_step_to_quote", 

			"cqpim_add_step_to_quote");

	add_action( "wp_ajax_cqpim_add_step_to_quote", 

		  	"cqpim_add_step_to_quote");

	function cqpim_add_step_to_quote() {
		
		$quote_id = isset($_POST['ID']) ? $_POST['ID'] : '';
		
		$title = isset($_POST['title']) ? $_POST['title'] : '';
		
		$start = isset($_POST['start']) ? $_POST['start'] : '';
		
		if(!empty($start)) {
		
			$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
		
		}
		
		$deadline = isset($_POST['deadline']) ? $_POST['deadline'] : '';
		
		if(!empty($deadline)) {
		
			$deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $deadline)->getTimestamp();
		
		}
		
		$milestone_id = isset($_POST['milestone_id']) ? $_POST['milestone_id'] : '';
		
		$cost = isset($_POST['cost']) ? $_POST['cost'] : '';
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		if($title && $deadline) {
		
			if($type == 'project') {
			
				$quote_elements = get_post_meta($quote_id, 'project_elements', true);
			
			} else {
		
				$quote_elements = get_post_meta($quote_id, 'quote_elements', true);
			
			}
			
			$i = 0;
			
			if(empty($quote_elements)) {
			
				$quote_elements = array();
			
			}
			
			foreach($quote_elements as $element) {
			
				$i++;
			
			}
			
			$element_to_add = array(
			
				'title' => $title,
				
				'id' => $milestone_id,
				
				'deadline' => $deadline,
				
				'start' => $start,
				
				'cost' => $cost,
				
				'weight' => $i++,
				
			);
			
			$quote_elements[$milestone_id] = $element_to_add;
			
			if($type == 'project') {
			
				update_post_meta($quote_id, 'project_elements', $quote_elements);
			
			} else {
			
				update_post_meta($quote_id, 'quote_elements', $quote_elements);
				
			}
			
			if($type == 'project') {
			
				$current_user = wp_get_current_user();
				
				$current_user = $current_user->display_name;
			
				$project_progress = get_post_meta($quote_id, 'project_progress', true);
						
				$project_progress[] = array(
						
					'update' => __('Milestone Created', 'cqpim') . ': ' . $title,
							
					'date' => current_time('timestamp'),
					
					'by' => $current_user
							
				);
						
				update_post_meta($quote_id, 'project_progress', $project_progress );
			
			}
			
			return true;
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> __('You must fill in the title and deadline as a minimum.', 'cqpim')

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);
			
		}
		
		exit();

	}
	
	// Process Quote Emails

	add_action( "wp_ajax_nopriv_cqpim_process_quote_emails", 

			"cqpim_process_quote_emails");

	add_action( "wp_ajax_cqpim_process_quote_emails", 

		  	"cqpim_process_quote_emails");

	function cqpim_process_quote_emails() {
	
		$quote_id = $_POST['quote_id'];
		
		$quote_object = get_post($quote_id);
	
		$quote_details = get_post_meta($quote_id, 'quote_details', true);
		
		$client_id = $quote_details['client_id'];
		
		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$client_contacts = get_post_meta($client_id, 'client_contacts', true);
		
		$client_contact = isset($quote_details['client_contact']) ? $quote_details['client_contact'] : $client_details['user_id'];
		
		$client_main_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
		
		if(empty($client_contacts)) {
		
			$client_contacts = array();
		
		}
		
		if(!empty($client_contact)) {
		
			if($client_contact == $client_main_id) {
			
				$to = $client_details['client_email'];
			
			} else {
			
				$to = $client_contacts[$client_contact]['email'];
			
			}
		
		} else {
		
			$to = $client_details['client_email'];
		
		}
		
		$email_content = get_option('quote_default_email');
		
		if($client_contact == $client_main_id) {
		
			$email_content = str_replace('%%CLIENT_NAME%%', $client_details['client_contact'], $email_content);
			
			$email_content = str_replace('%%CLIENT_EMAIL%%', $client_details['client_email'], $email_content);
		
		} else {
		
			$email_content = str_replace('%%CLIENT_NAME%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['name'] : '', $email_content);
			
			$email_content = str_replace('%%CLIENT_EMAIL%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['email'] : '', $email_content);
		
		}
		
		$message = cqpim_replacement_patterns($email_content, $quote_id, 'quote');
		
		$subject = get_option('quote_email_subject');
		
		if($client_contact == $client_main_id) {
		
			$subject = str_replace('%%CLIENT_NAME%%', $client_details['client_contact'], $subject);
		
		} else {
		
			$subject = str_replace('%%CLIENT_NAME%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['name'] : '', $subject);
		
		}
		
		$subject = cqpim_replacement_patterns($subject, $quote_id, 'quote');
		
		$attachments = array();
		
		if( $to && $subject && $message ){
		
			if( cqpim_send_emails( $to, $subject, $message, '', $attachments, 'sales' ) ) :
			
				$current_user = wp_get_current_user();
				
				$current_user = $current_user->display_name;
		
	            $quote_details = get_post_meta($quote_id, 'quote_details', true);

	        	$quote_details['sent_details'] = array(

						'date' 	=> current_time('timestamp'),

						'by'	=> $current_user,
						
						'to'    => $to,

				);
				
				unset($quote_details['confirmed']);
				
				unset($quote_details['confirmed_details']);
				
				$quote_details['sent'] = true;

	        	update_post_meta($quote_id, 'quote_details', $quote_details );	
		
				$return =  array( 

					'error' 	=> false,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Email sent successfully...', 'cqpim') . '</div>'

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);		

			else :

				$return =  array( 

					'error' 	=> true,

					'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem with WP Mail, ehck that your instalaltion is able to send emails and try again.', 'cqpim') . '</div>'

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);

			endif;	
		
		} else {

			$return =  array( 

				'error' 	=> true,

				'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem sending the email, check that you have completed ALL email subject and content fields in the settings.', 'cqpim') . '</div>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

		}
		
		exit();
	
	}
	
	/* PROCESS CLIENT QUOTE ACCEPTANCE */

	add_action("wp_ajax_nopriv_cqpim_client_accept_quote", "cqpim_client_accept_quote");

	add_action("wp_ajax_cqpim_client_accept_quote", "cqpim_client_accept_quote");

	function cqpim_client_accept_quote() {

		$quote_id = isset( $_POST['quote_id'] ) ? (int) $_POST['quote_id'] : 0;
		
		$signed_name = isset( $_POST['name'] ) ? $_POST['name'] : '';
		
		$pm_name = isset( $_POST['pm_name'] ) ? $_POST['pm_name'] : '';

		$quote_details = get_post_meta( $quote_id, 'quote_details', true );
		
		$quote_ref = $quote_details['quote_ref'];
		
		$quote_type = $quote_details['quote_type'];
		
		$ip = cqpim_get_client_ip();
		
		if($signed_name) {

			$quote_details['confirmed_details'] = array(

				'date' 	=> current_time('timestamp'),

				'by'	=> $signed_name,
								
				'ip'	=> $ip,

			);
			
			$quote_details['confirmed'] = true;

			update_post_meta( $quote_id, 'quote_details', $quote_details );
			
			// Send email to confirm acceptance
			
			$sender_email = get_option('company_sales_email');
			
			$to = $sender_email;
			
			$attachments = array();
			
			$admin_quote = admin_url() . 'post.php?post=' . $quote_id . '&action=edit';
			
			$subject = sprintf(__('%1$s has just accepted Quote: %2$s', 'cqpim'), $signed_name, $quote_ref);
			
			$content = sprintf(__('%1$s has just accepted Quote: %2$s. You can view the details by clicking here - %3$s', 'cqpim'), $signed_name, $quote_ref, $admin_quote);
			
			cqpim_send_emails( $to, $subject, $content, '', $attachments, 'sales' );
			
			$return =  array( 

				'error' 	=> false,

				'message' 	=> __('All good!', 'cqpim')

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			// Call function to create project
			
			if(get_option('enable_project_creation') == 1) {
			
				cqpim_create_project_from_quote($quote_id, $pm_name);
			
			}
			
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem, please try again.', 'cqpim') . '</div>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);		
		
		}
					
		die();
	}
	
	/* CREATE PROJECT ON QUOTE ACCEPTANCE */
	
	function cqpim_create_project_from_quote($quote_id, $pm_name) {
	
		$quote_details = get_post_meta($quote_id, 'quote_details', true);
		
		$quote_milestones = get_post_meta($quote_id, 'quote_elements', true);
		
		$tax_app = get_post_meta($quote_id, 'tax_applicable', true);	

		$tax_rate = get_post_meta($quote_id, 'tax_rate', true);
		
		$stax_app = get_post_meta($quote_id, 'stax_applicable', true);	

		$stax_rate = get_post_meta($quote_id, 'stax_rate', true);
		
		$quote_ref = isset($quote_details['quote_ref']) ? $quote_details['quote_ref'] : '';
		
		$quote_type = isset($quote_details['quote_type']) ? $quote_details['quote_type'] : '';
		
		$deposit = isset($quote_details['deposit_amount']) ? $quote_details['deposit_amount'] : '';
		
		$client_id = isset($quote_details['client_id']) ? $quote_details['client_id'] : '';
		
		$client_contact = isset($quote_details['client_contact']) ? $quote_details['client_contact'] : '';
		
		$contract = isset($quote_details['default_contract_text']) ? $quote_details['default_contract_text'] : '';
		
		$start_date = isset($quote_details['start_date']) ? $quote_details['start_date'] : '';
		
		$finish_date = isset($quote_details['finish_date']) ? $quote_details['finish_date'] : '';
		
		$project_summary = isset($quote_details['quote_summary']) ? $quote_details['quote_summary'] : '';
		
		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$client_company = isset($client_details['client_company']) ? $client_details['client_company'] : '';
	
		$project_title = sprintf(__('%1$s - Project: %2$s', 'cqpim'), $client_company, $quote_ref);
		
		$new_project = array(
		
			'post_type' => 'cqpim_project',
			
			'post_status' => 'private',
			
			'post_content' => '',
			
			'post_title' => $project_title
		
		);
		
		$project_pid = wp_insert_post( $new_project, true );
		
		if( ! is_wp_error( $project_pid ) ){
		
			$project_updated = array(
				
				'ID' => $project_pid,
					
				'post_name' => $project_pid,
					
			);						
		
			wp_update_post( $project_updated );
		
			$project_details = array(
			
				'client_id' => $client_id,
				
				'quote_ref' => $quote_ref,
				
				'start_date' => $start_date,
				
				'finish_date' => $finish_date,
				
				'pm_name' => $pm_name,
				
				'deposit_amount' => $deposit,
				
				'default_contract_text' => $contract,
				
				'quote_id' => $quote_id,
				
				'quote_type' => $quote_type,
				
				'client_contact' => $client_contact,
				
				'project_summary' => $project_summary,
			
			);
			
			$project_progress = array();
			
			$project_progress[] = array(
			
				'update' => __('Project created', 'cqpim'),
				
				'date' => current_time('timestamp'),
				
				'by' => __('System', 'cqpim')
				
			);
			
			update_post_meta($project_pid, 'project_details', $project_details);
			
			update_post_meta($project_pid, 'tax_applicable', $tax_app);
			
			update_post_meta($project_pid, 'tax_set', 1);

			update_post_meta($project_pid, 'tax_rate', $tax_rate);	
			
			update_post_meta($project_pid, 'stax_applicable', $stax_app);
			
			update_post_meta($project_pid, 'stax_set', 1);

			update_post_meta($project_pid, 'stax_rate', $stax_rate);
			
			update_post_meta($project_pid, 'project_elements', $quote_milestones);
			
			update_post_meta($project_pid, 'project_progress', $project_progress);
			
			$currency = get_option('currency_symbol');
			
			$currency_code = get_option('currency_code');
			
			$currency_position = get_option('currency_symbol_position');
			
			$currency_space = get_option('currency_symbol_space'); 
			
			$client_currency = get_post_meta($project_details['client_id'], 'currency_symbol', true);
				
			$client_currency_code = get_post_meta($project_details['client_id'], 'currency_code', true);
				
			$client_currency_space = get_post_meta($project_details['client_id'], 'currency_space', true);		

			$client_currency_position = get_post_meta($project_details['client_id'], 'currency_position', true);
			
			$quote_currency = get_post_meta($project_details['quote_id'], 'currency_symbol', true);
				
			$quote_currency_code = get_post_meta($project_details['quote_id'], 'currency_code', true);
				
			$quote_currency_space = get_post_meta($project_details['quote_id'], 'currency_space', true);	

			$quote_currency_position = get_post_meta($project_details['quote_id'], 'currency_position', true);
			
			if(!empty($quote_currency)) {
			
				update_post_meta($project_pid, 'currency_symbol', $quote_currency);
			
			} else {
			
				if(!empty($client_currency)) {
				
					update_post_meta($project_pid, 'currency_symbol', $client_currency);
				
				} else {
				
					update_post_meta($project_pid, 'currency_symbol', $currency);
				
				}
			
			}
			
			if(!empty($quote_currency_code)) {
			
				update_post_meta($project_pid, 'currency_code', $quote_currency_code);
			
			} else {
			
				if(!empty($client_currency_code)) {
				
					update_post_meta($project_pid, 'currency_code', $client_currency_code);
				
				} else {
				
					update_post_meta($project_pid, 'currency_code', $currency_code);
				
				}
			
			}
			
			if(!empty($quote_currency_space)) {
			
				update_post_meta($project_pid, 'currency_space', $quote_currency_space);
			
			} else {
			
				if(!empty($client_currency_space)) {
				
					update_post_meta($project_pid, 'currency_space', $client_currency_space);
				
				} else {
				
					update_post_meta($project_pid, 'currency_space', $currency_space);
				
				}
			
			}
			
			if(!empty($quote_currency_position)) {
			
				update_post_meta($project_pid, 'currency_position', $quote_currency_position);
			
			} else {
			
				if(!empty($client_currency_position)) {
				
					update_post_meta($project_pid, 'currency_position', $client_currency_position);
				
				} else {
				
					update_post_meta($project_pid, 'currency_position', $currency_position);
				
				}
			
			}		

			$client_contracts = get_post_meta($client_id, 'client_contract', true);
			
			$auto_contract = get_option('auto_contract');
			
			$checked = get_option('enable_project_contracts'); 	
			
			if($auto_contract && $checked == 1 && empty($client_contracts)) {
			
				cqpim_process_contract_emails($project_pid);
			
			}		
			
			if(empty($checked) || !empty($client_contracts)) {
			
				// Get all quote tasks and give them a project ID
				
				$project_details = get_post_meta($project_pid, 'project_details', true);
				
				$project_details['sent'] = true;
				
				update_post_meta($project_pid, 'project_details', $project_details);
				
				$project_elements = get_post_meta($project_pid, 'project_elements', true);
				
				if(empty($project_elements)) {
				
					$project_elements = array();
				
				}
				
				foreach($project_elements as $element) {
				
					$args = array(
					
						'post_type' => 'cqpim_tasks',
						
						'posts_per_page' => -1,
						
						'meta_key' => 'milestone_id',
						
						'meta_value' => $element['id'],
						
						'orderby' => 'date',
						
						'order' => 'ASC',
						
					);
					
					$tasks = get_posts($args);
									
					foreach($tasks as $task) {

						update_post_meta($task->ID, 'project_id', $project_pid);
						
						update_post_meta($task->ID, 'active', true);

					}					
				
				}
				
				if(!empty($deposit) && $deposit != 'none') {
				
					cqpim_create_deposit_invoice($project_pid);
				
				}
			
			} else {
			
				// Get all quote tasks and give them a project ID
				
				$project_elements = get_post_meta($project_pid, 'project_elements', true);
				
				if(empty($project_elements)) {
				
					$project_elements = array();
				
				}
				
				foreach($project_elements as $element) {
				
					$args = array(
					
						'post_type' => 'cqpim_tasks',
						
						'posts_per_page' => -1,
						
						'meta_key' => 'milestone_id',
						
						'meta_value' => $element['id'],
						
						'orderby' => 'date',
						
						'order' => 'ASC',
						
					);
					
					$tasks = get_posts($args);
									
					foreach($tasks as $task) {

						update_post_meta($task->ID, 'project_id', $project_pid);
						
						//update_post_meta($task->ID, 'active', true);

					}					
				
				}			
			
			}
			
		
		} else {
		
			exit();
			
		}
	
	}
	
	/********* 3. PROJECT FUNCTIONS *************/
	
	// Process Contract Emails

	add_action( "wp_ajax_nopriv_cqpim_process_contract_emails", 

			"cqpim_process_contract_emails");

	add_action( "wp_ajax_cqpim_process_contract_emails", 

		  	"cqpim_process_contract_emails");

	function cqpim_process_contract_emails($project_id) {
	
		if(!empty($_POST['project_id'])) {
	
			$project_id = $_POST['project_id'];
			
			$ajax_post = true;
			
		} 
	
		$project_details = get_post_meta($project_id, 'project_details', true);
		
		$quote_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
		
		$client_id = $project_details['client_id'];
		
		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$client_contacts = get_post_meta($client_id, 'client_contacts', true);
		
		$client_contact = isset($project_details['client_contact']) ? $project_details['client_contact'] : '';
		
		$client_main_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
		
		if(empty($client_contacts)) {
		
			$client_contacts = array();
		
		}
		
		if(!empty($client_contact)) {
		
			if($client_contact == $client_main_id) {
			
				$to = $client_details['client_email'];
			
			} else {
			
				$to = $client_contacts[$client_contact]['email'];
			
			}
		
		} else {
		
			$to = $client_details['client_email'];
		
		}
		
		$email_content = get_option('client_contract_email');
		
		if($client_contact == $client_main_id) {
		
			$email_content = str_replace('%%CLIENT_NAME%%', $client_details['client_contact'], $email_content);
			
			$email_content = str_replace('%%CLIENT_EMAIL%%', $client_details['client_email'], $email_content);
		
		} else {
		
			$email_content = str_replace('%%CLIENT_NAME%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['name'] : '', $email_content);
			
			$email_content = str_replace('%%CLIENT_EMAIL%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['email'] : '', $email_content);
		
		}
		
		if(empty($ajax_post)) {
		
			$current_user_tag = '%%CURRENT_USER%%';
			
			$email_content = str_replace($current_user_tag, '', $email_content);
		
		}
		
		$message = cqpim_replacement_patterns($email_content, $project_id, 'project');
		
		$attachments = array();
		
		$subject = get_option('client_contract_subject');
		
		$subject = cqpim_replacement_patterns($subject, $project_id, 'project');
		
		if( $to && $subject && $message ){
		
			if( cqpim_send_emails( $to, $subject, $message, '', $attachments, 'sales' ) ) :
			
				if(!empty($ajax_post)) {
			
					$current_user = wp_get_current_user();
					
					$current_user = $current_user->display_name;
				
				} else {
				
					$current_user = 'System';
					
				}
		
	            $project_details = get_post_meta($project_id, 'project_details', true);

	        	$project_details['sent_details'] = array(

						'date' 	=> current_time('timestamp'),

						'by'	=> $current_user,
						
						'to'    => $to,

				);
				
				unset($project_details['confirmed']);
				
				unset($project_details['confirmed_details']);
				
				$project_details['sent'] = true;
				
				update_post_meta($project_id, 'project_details', $project_details );
				
				if(!empty($ajax_post)) {
					
					$project_progress = get_post_meta($project_id, 'project_progress', true);
					
					$project_progress[] = array(
					
						'update' => __('Contract sent', 'cqpim'),
						
						'date' => current_time('timestamp'),
						
						'by' => $current_user,
						
					);
					
					update_post_meta($project_id, 'project_progress', $project_progress );
					
				} else {
				
					$project_progress = get_post_meta($project_id, 'project_progress', true);
					
					$project_progress[] = array(
					
						'update' => __('Contract sent', 'cqpim'),
						
						'date' => current_time('timestamp'),
						
						'by' => __('System', 'cqpim'),
						
					);
					
					update_post_meta($project_id, 'project_progress', $project_progress );				
				
				}

				if(!empty($ajax_post)) {
		
					$return =  array( 

						'error' 	=> false,

						'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Email sent successfully...', 'cqpim') . '</div>'

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	

				};			

			else :
			
				if(!empty($ajax_post)) {

					$return =  array( 

						'error' 	=> true,

						'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem sending the email. Check that you have completed the email subject and content templates in the settings.', 'cqpim') , '</div>'

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);
				
				};

			endif;	
		
		} else {
			
			if($ajax_post) {

				$return =  array( 

					'error' 	=> true,

					'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem sending the email, check that you have completed ALL email subject and content fields in the settings.', 'cqpim') . '</div>'

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);
			
			};

		}
		
		if(!empty($ajax_post)) {
		
			exit();
			
		};
	
	}
	
	/* PROCESS CLIENT CONTRACT ACCEPTANCE */

	add_action("wp_ajax_nopriv_cqpim_client_accept_contract", "cqpim_client_accept_contract");

	add_action("wp_ajax_cqpim_client_accept_contract", "cqpim_client_accept_contract");

	function cqpim_client_accept_contract() {

		$project_id = isset( $_POST['project_id'] ) ? (int) $_POST['project_id'] : 0;
		
		$signed_name = isset( $_POST['name'] ) ? $_POST['name'] : '';
		
		$pm_name = isset( $_POST['pm_name'] ) ? $_POST['pm_name'] : '';

		$project_details = get_post_meta( $project_id, 'project_details', true );
		
		$deposit = isset($project_details['deposit_amount']) ? $project_details['deposit_amount'] : '';
		
		$deposit_invoice_id = isset($project_details['deposit_invoice_id']) ? $project_details['deposit_invoice_id'] : '';
		
		$contract = isset($project_details['default_contract_text']) ? $project_details['default_contract_text'] : '';
		
		$terms = get_post_meta($project_id, 'terms', true);
		
		if(empty($terms)) {
		
			if(empty($contract)) {
			
				$text = get_post_meta($contract_text, 'terms', true);
				
				$text = cqpim_replacement_patterns($text, $project_id, 'project');
				
				update_post_meta($project_id, 'terms', $text);
				
			} else {
			
				$text = get_post_meta($contract, 'terms', true);
				
				$text = cqpim_replacement_patterns($text, $project_id, 'project');
				
				update_post_meta($project_id, 'terms', $text);			
			
			}		
		
		}
		
		$quote_ref = $project_details['quote_ref'];
		
		$ip = cqpim_get_client_ip();
		
		if(!empty($signed_name)) {

			$project_details['confirmed_details'] = array(

				'date' 	=> current_time('timestamp'),

				'by'	=> $signed_name,
								
				'ip'	=> $ip,

			);
			
			$project_details['confirmed'] = true;

			update_post_meta( $project_id, 'project_details', $project_details );
			
			// Send email to confirm acceptance
			
			$sender_email = get_option('company_sales_email');
			
			$pm_email = array();
			
			$pm_email[] = $sender_email;
			
			$project_contributors = get_post_meta($project_id, 'project_contributors', true);
			
			if(empty($project_contributors)) {
				
				$project_contributors = array();
				
			}
			
			foreach($project_contributors as $contrib) {
				
				if($contrib['pm'] == true) {
					
					$team_details = get_post_meta($contrib['team_id'], 'team_details', true);
					
					$pm_email[] = isset($team_details['team_email']) ? $team_details['team_email'] : '';
					
				}
				
			}
			
			$attachments = array();
			
			$admin_quote = admin_url() . 'post.php?post=' . $project_id . '&action=edit';
			
			$subject = sprintf(__('%1$s has just accepted Contract %2$s', 'cqpim'), $signed_name, $quote_ref);
			
			$content = sprintf(__('%1$s has just accepted Contract %2$s. You can view the details by clicking here - %3$s', 'cqpim'), $signed_name, $quote_ref, $admin_quote);
			
			foreach($pm_email as $address) {
			
				cqpim_send_emails( $address, $subject, $content, '', $attachments, 'sales' );
			
			}
			
			$project_progress = get_post_meta($project_id, 'project_progress', true);
			
			$text = sprintf(__('Contract for Project %1$s has been confirmed!', 'cqpim'), $quote_ref);
					
			$project_progress[] = array(
					
				'update' => $text,
						
				'date' => current_time('timestamp'),
						
				'by' => $signed_name,
						
			);
					
			update_post_meta($project_id, 'project_progress', $project_progress );
			
			if(!empty($deposit) && $deposit != 'none' && empty($deposit_invoice_id)) {
			
				cqpim_create_deposit_invoice($project_id, $pm_name);
			
			}
			
			// Get all quote tasks and give them a project ID
			
			$project_elements = get_post_meta($project_id, 'project_elements', true);
			
			if(empty($project_elements)) {
			
				$project_elements = array();
			
			}
			
			foreach($project_elements as $element) {
			
				$args = array(
				
					'post_type' => 'cqpim_tasks',
					
					'posts_per_page' => -1,
					
					'meta_key' => 'milestone_id',
					
					'meta_value' => $element['id'],
					
					'orderby' => 'date',
					
					'order' => 'ASC',
					
				);
				
				$tasks = get_posts($args);
								
				foreach($tasks as $task) {
					
					update_post_meta($task->ID, 'active', true);

				}					
			
			}
			
			$return =  array( 

				'error' 	=> false,

				'message' 	=> __('All good!', 'cqpim')

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

			// Call function to create Deposit Invoice if set
			
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem, please try again.', 'cqpim') . '</div>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);		
		
		}
					
		die();
	}
	
	// MArk Project Complete

	add_action( "wp_ajax_nopriv_cqpim_mark_project_complete", 

			"cqpim_mark_project_complete");

	add_action( "wp_ajax_cqpim_mark_project_complete", 

		  	"cqpim_mark_project_complete");

	function cqpim_mark_project_complete() {
	
		$project_id = $_POST['project_id'];
		
		$project_details = get_post_meta($project_id, 'project_details', true);
		
		$quote_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
		
		$project_details['signoff'] = true;
		
		$current_user = wp_get_current_user();
					
		$current_user = $current_user->display_name;
		
		$project_details['signoff_details'] = array(
		
			'by' => $current_user,
			
			'at' => current_time('timestamp'),
		
		);
		
		update_post_meta($project_id, 'project_details', $project_details);
		
		$project_progress = get_post_meta($project_id, 'project_progress', true);
		
		$text = sprintf(__('Project %1$s has been marked as Complete!', 'cqpim'), $quote_ref);
		
		$project_progress[] = array(
					
			'update' => $text,
						
			'date' => current_time('timestamp'),
						
			'by' => $current_user,
						
		);
					
		update_post_meta($project_id, 'project_progress', $project_progress );
		
			$return =  array( 

				'error' 	=> false,

				'messages' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Project successfully updated...', 'cqpim') . '</div>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			// Create the Completion INVOICE
			
			$checked = get_option('invoice_workflow');
			
			if($checked == 0) {
			
				cqpim_create_completion_invoice($project_id);
			
			}
		
		exit();
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_mark_project_incomplete", 

			"cqpim_mark_project_incomplete");

	add_action( "wp_ajax_cqpim_mark_project_incomplete", 

		  	"cqpim_mark_project_incomplete");

	function cqpim_mark_project_incomplete() {
	
		$project_id = $_POST['project_id'];
		
		$project_details = get_post_meta($project_id, 'project_details', true);
		
		$completion_invoice_id = isset($project_details['completion_invoice_id']) ? $project_details['completion_invoice_id'] : '';
		
		$project_details['signoff'] = false;
		
		unset($project_details['signoff_details']);
		
		unset($project_details['completion_invoice_id']);
		
		update_post_meta($project_id, 'project_details', $project_details);
		
		wp_delete_post($completion_invoice_id);
		
		$return =  array( 

			'error' 	=> false,

			'messages' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Project successfully marked as in progress.', 'cqpim') , '</div>'

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	
		
		exit();
	
	}
	
	// MArk Project Closed

	add_action( "wp_ajax_nopriv_cqpim_mark_project_closed", 

			"cqpim_mark_project_closed");

	add_action( "wp_ajax_cqpim_mark_project_closed", 

		  	"cqpim_mark_project_closed");

	function cqpim_mark_project_closed() {
	
		$project_id = $_POST['project_id'];
		
		$project_details = get_post_meta($project_id, 'project_details', true);
		
		$quote_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
		
		$project_details['closed'] = true;
		
		$project_details['signoff'] = true;
		
		$current_user = wp_get_current_user();
					
		$current_user = $current_user->display_name;
		
		$project_details['closed_details'] = array(
		
			'by' => $current_user,
			
			'at' => current_time('timestamp'),
		
		);
		
		update_post_meta($project_id, 'project_details', $project_details);
		
		$project_progress = get_post_meta($project_id, 'project_progress', true);
		
		$text = sprintf(__('Project %1$s has been marked as Closed', 'cqpim'), $quote_ref);
		
		$project_progress[] = array(
					
			'update' => $text,
						
			'date' => current_time('timestamp'),
						
			'by' => $current_user,
						
		);
					
		update_post_meta($project_id, 'project_progress', $project_progress );
		
			$return =  array( 

				'error' 	=> false,

				'messages' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Project successfully updated...', 'cqpim') . '</div>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	
		
		exit();
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_mark_project_open", 

			"cqpim_mark_project_open");

	add_action( "wp_ajax_cqpim_mark_project_open", 

		  	"cqpim_mark_project_open");

	function cqpim_mark_project_open() {
	
		$project_id = $_POST['project_id'];
		
		$project_details = get_post_meta($project_id, 'project_details', true);
		
		$quote_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
		
		$project_details['closed'] = false;
		
		$current_user = wp_get_current_user();
					
		$current_user = $current_user->display_name;
		
		unset($project_details['closed_details']);
		
		update_post_meta($project_id, 'project_details', $project_details);
		
		$project_progress = get_post_meta($project_id, 'project_progress', true);
		
		$text = sprintf(__('Project %1$s has been re-opened', 'cqpim'), $quote_ref);
		
		$project_progress[] = array(
					
			'update' => $text,
						
			'date' => current_time('timestamp'),
						
			'by' => $current_user,
						
		);
					
		update_post_meta($project_id, 'project_progress', $project_progress );
		
			$return =  array( 

				'error' 	=> false,

				'messages' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Project successfully re-opened...', 'cqpim') . '</div>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	
		
		exit();
	
	}
	
	// Mark Milestone/Task as Complete Button

	add_action( "wp_ajax_cqpim_mark_item_complete", 

		  	"cqpim_mark_item_complete");

	function cqpim_mark_item_complete() {
	
		$data = isset($_POST) ? $_POST : array();
		
		if(!empty($data['type'])) {
		
			if($data['type'] == 'task') {
			
				$task_details = get_post_meta($data['item_id'], 'task_details', true);
				
				$task_details['status'] = 'complete';
				
				$task_details['task_pc'] = 100;
				
				update_post_meta($data['item_id'], 'task_details', $task_details);
				
				$task_owner = get_post_meta($data['item_id'], 'owner', true);
				
				$task_watchers = get_post_meta($data['item_id'], 'task_watchers', true);
				
				$message = '';
				
				if(isset($data['ppid'])) {
				
					$current_user = wp_get_current_user();;
						
					$project_progress = get_post_meta($data['ppid'], 'project_progress', true);
					
					$task_object = get_post($data['item_id']);
					
					$task_title = $task_object->post_title;
					
					$text = sprintf(__('Task Completed: %1$s', 'cqpim'), $task_title );
									
					$project_progress[] = array(
									
						'update' => $text,
										
						'date' => current_time('timestamp'),
								
						'by' => $current_user->display_name
										
					);
									
					update_post_meta($data['ppid'], 'project_progress', $project_progress );
				
				}				
				
				cqpim_send_task_updates($data['item_id'], $data['ppid'], $task_owner, $task_watchers, $message);
			
			}
			
			if($data['type'] == 'ms') {
			
			
			
			}
		
			$return =  array( 

				'error' 	=> false,

				'errors' 	=> ''

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> __('The item type is missing, please try again', 'cqpim')

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);		
		
		}

		exit;			
	
	}
	
	add_action( "wp_ajax_cqpim_toggle_complete", 

		  	"cqpim_toggle_complete");

	function cqpim_toggle_complete() {
	
		$data = isset($_POST) ? $_POST : array();
		
		if(!empty($data['ppid'])) {
		
			$quote_details = get_post_meta($data['ppid'], 'project_details', true);
			
			if(empty($quote_details['hide_complete'])) {
			
				$quote_details['hide_complete'] = false;
				
			}
			
			if($quote_details['hide_complete'] == false) {
			
				$quote_details['hide_complete'] = true;
				
			} else {
			
				$quote_details['hide_complete'] = false;
				
			}
			
			update_post_meta($data['ppid'], 'project_details', $quote_details);
			
			$return =  array( 

				'error' 	=> false,

				'errors' 	=> ''

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> __('The project ID is missing, please try again', 'cqpim')

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);		
		
		}
		
		exit;
	
	}
	
	
	
	/******** INVOICE FUNCTIONS *************/
	
	add_action( "wp_ajax_cqpim_edit_income_graph", 

		  	"cqpim_edit_income_graph");
	
	function cqpim_edit_income_graph() {
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		if($type == 'type') {
			
			$_SESSION['invoice_payments'] = isset($_POST['date']) ? $_POST['date'] : 'invoice';
			
		} else {
			
			$_SESSION['invoice_year'] = isset($_POST['date']) ? $_POST['date'] : date('Y');
			
		}
		
		$return =  array( 

			'error' 	=> false,

			'errors' 	=> ''

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);		
		
		exit;
		
	}
	
	// Get most recent invoice ID
	
	function cqpim_get_invoice_id() {
	
		$args = array(
		
			'post_type' => 'cqpim_invoice',
			
			'posts_per_page' => 1,
			
			'orderby' => 'date',
			
			'order' => 'desc',
			
			'post_status' => 'publish'
			
		);
		
		$invoices = get_posts($args);
		
		if(!empty($invoices)) {
		
			foreach($invoices as $invoice) {
			
				$invoice_id = get_post_meta($invoice->ID, 'invoice_id', true);
			
			}
			
			$invoice_id++;
		
		} else {
		
			$invoice_id = 1;
			
		}
		
		return $invoice_id;
	
	}
	
	// Check recurring invoices
	
	add_action('cqpim_check_recurring_invoices', 'cqpim_check_recurring_invoices_hourly');
	
	function cqpim_check_recurring_invoices_hourly() {
	
		$args = array(
		
			'post_type' => 'cqpim_client',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
			
		);
		
		$now = time();
		
		$clients = get_posts($args);
		
		foreach($clients as $client) {
		
			$recurring_invoices = get_post_meta($client->ID, 'recurring_invoices', true);
			
			$client_details = get_post_meta($client->ID, 'client_details', true);
			
			$invoice_terms = isset($client_details['invoice_terms']) ? $client_details['invoice_terms']: '';
		
			$system_invoice_terms = get_option('company_invoice_terms');
			
			if(!empty($invoice_terms)) {
			
				$terms = $invoice_terms;
				
			} else {
			
				$terms = $system_invoice_terms;
				
			}
			
			if(empty($recurring_invoices)) {
			
				$recurring_invoices = array();
				
			}
			
			foreach($recurring_invoices as $key => $invoice) {
			
				if(empty($invoice['end'])) {
				
					$end = strtotime('+365 days', $now);
					
				} else {
				
					$end = $invoice['end'];
					
					if(!is_numeric($end)) {
					
						$end = str_replace('/','-',$end);
					
						$end = strtotime($end);
					
					} else {
					
							$end = $end;
					
					}
					
				}
			
				if($invoice['next_run'] < $now && $invoice['status'] == 1 && $end > strtotime('-2 days', $now)) {
				
					$invoice_id = cqpim_get_invoice_id();
				
					$new_invoice = array(
					
						'post_type' => 'cqpim_invoice',
						
						'post_status' => 'publish',
						
						'post_content' => '',
						
						'post_title' =>  $invoice_id,
						
						'post_password' => cqpim_random_string(10),
					
					);
					
					$invoice_pid = wp_insert_post( $new_invoice, true );
					
					if( ! is_wp_error( $invoice_pid ) ){
						
						$date = time();
			
						$terms_over = strtotime('+' . $terms . ' days', $date);
						
						if($terms == 1) {
						
							$invoice_details = array(
							
								'client_id' => $client->ID,
								
								'due' => __('Due on Receipt', 'cqpim'),
								
								'on_receipt' => true,
								
								'terms_over' => $terms_over,
								
								'invoice_date' => current_time('timestamp'),
								
								'allow_partial' => $invoice['partial'],
							
							);														
							
						} else {
					
							$invoice_details = array(
							
								'client_id' => $client->ID,
								
								'due' => $terms_over,
								
								'terms_over' => $terms_over,
								
								'invoice_date' => current_time('timestamp'),
								
								'allow_partial' => $invoice['partial'],
							
							);
						
						}
						
						update_post_meta($invoice_pid, 'invoice_id', $invoice_id);
						
						if(isset($invoice['contact'])) {
						
							update_post_meta($invoice_pid, 'client_contact', $invoice['contact']);
						
						}
						
						update_post_meta($invoice_pid, 'invoice_client', $client->ID);
						
						$currency = get_option('currency_symbol');
						
						$currency_code = get_option('currency_code');
						
						$currency_position = get_option('currency_symbol_position');
						
						$currency_space = get_option('currency_symbol_space'); 
						
						$client_currency = get_post_meta($client->ID, 'currency_symbol', true);
							
						$client_currency_code = get_post_meta($client->ID, 'currency_code', true);
							
						$client_currency_space = get_post_meta($client->ID, 'currency_space', true);		

						$client_currency_position = get_post_meta($client->ID, 'currency_position', true);
						
						if(!empty($client_currency)) {
						
							update_post_meta($invoice_pid, 'currency_symbol', $client_currency);
						
						} else {
						
							update_post_meta($invoice_pid, 'currency_symbol', $currency);
						
						}
						
						if(!empty($client_currency_code)) {
						
							update_post_meta($invoice_pid, 'currency_code', $client_currency_code);
						
						} else {
						
							update_post_meta($invoice_pid, 'currency_code', $currency_code);
						
						}
					
						if(!empty($client_currency_space)) {
						
							update_post_meta($invoice_pid, 'currency_space', $client_currency_space);
						
						} else {
						
							update_post_meta($invoice_pid, 'currency_space', $currency_space);
						
						}
					
						if(!empty($client_currency_position)) {
						
							update_post_meta($invoice_pid, 'currency_position', $client_currency_position);
						
						} else {
						
							update_post_meta($invoice_pid, 'currency_position', $currency_position);
						
						}
						
						update_post_meta($invoice_pid, 'invoice_details', $invoice_details);
						
						$line_items = array();
						
						$items = $invoice['items'];
						
						$subtotal = 0;
						
						foreach($items as $item) {
						
							$line_items[] = array(
							
								'qty' => $item['qty'],
								
								'desc' => $item['desc'],
								
								'price' => $item['price'],
								
								'sub' => $item['price'] * $item['qty'],
							
							);
							
							$subtotal = $item['price'] * $item['qty'] + $subtotal;
						
						}
						
						update_post_meta($invoice_pid, 'line_items', $line_items);
						
						$tax_app = get_post_meta($invoice_pid, 'tax_set', true);
						
						$system_tax = get_option('sales_tax_rate');
						
						$system_stax = get_option('secondary_sales_tax_rate');

						if(empty($tax_app)) {

							$client_details = get_post_meta($client->ID, 'client_details', true);
							
							$client_tax = isset($client_details['tax_disabled']) ? $client_details['tax_disabled'] : '';
							
							$client_stax = isset($client_details['stax_disabled']) ? $client_details['stax_disabled'] : '';
							
							if(!empty($system_tax) && empty($client_tax)) {
							
								update_post_meta($invoice_pid, 'tax_applicable', 1);
								
								update_post_meta($invoice_pid, 'tax_set', 1);	

								update_post_meta($invoice_pid, 'tax_rate', $system_tax);	
								
								if(!empty($system_stax) && empty($client_stax)) {
								
									update_post_meta($invoice_pid, 'stax_applicable', 1);
									
									update_post_meta($invoice_pid, 'stax_set', 1);	

									update_post_meta($invoice_pid, 'stax_rate', $system_stax);			
								
								} else {
								
									update_post_meta($invoice_pid, 'stax_applicable', 0);
									
									update_post_meta($invoice_pid, 'stax_set', 1);

									update_post_meta($invoice_pid, 'stax_rate', 0);				
								
								}
							
							} else {
							
								update_post_meta($invoice_pid, 'tax_applicable', 0);
								
								update_post_meta($invoice_pid, 'tax_set', 1);

								update_post_meta($invoice_pid, 'tax_rate', 0);			
							
							}

						}
						
						if(!empty($system_tax) && empty($client_tax)) {

							$tax = $subtotal / 100 * $system_tax;
							
							if(!empty($system_stax) && empty($client_stax)) {
							
								$stax = $subtotal / 100 * $system_stax;
								
								$total = $subtotal + $tax + $stax;
							
							} else {
							
								$stax = 0;
							
								$total = $subtotal + $tax;
								
							}
							
						} else {

							$tax = 0;
							
							$total = $subtotal;
							
						}

						$invoice_totals = array(

							'sub' => number_format((float)$subtotal, 2, '.', ''),
							
							'tax' => number_format((float)$tax, 2, '.', ''),
							
							'stax' => number_format((float)$stax, 2, '.', ''),
							
							'total' => number_format((float)$total, 2, '.', '')

						);
						
						update_post_meta($invoice_pid, 'invoice_totals', $invoice_totals);
						
						$auto_invoice = $invoice['auto'];
						
						if($auto_invoice == 1) {
						
							$deposit = false;
							
							$pm_name = false;
						
							cqpim_process_invoice_emails($invoice_pid, $pm_name, $deposit);
						
						}						
					
					}
				
					$frequency = $invoice['frequency'];
					
					$days = 86400;
					
					$weeks = 604800;
					
					$biweeks = 1209600;
					
					$months = 2592000;
					
					$bimonths = 5184000;
					
					$threemonths = 7884000;
					
					$sixmonths = 15768000;
					
					$years = 31536000;
					
					$biyears = 31536000;
					
					if($frequency == 'daily') {
					
						$next_run = $invoice['next_run'] + $days;
						
					} else if($frequency == 'weekly') {
					
						$next_run = $invoice['next_run'] + $weeks;
						
					} else if($frequency == 'biweekly') {
					
						$next_run = $invoice['next_run'] + $biweeks;
						
					} else if($frequency == 'monthly') {
					
						$next_run = $invoice['next_run'] + $months;
						
					} else if($frequency == 'bimonthly') {
					
						$next_run = $invoice['next_run'] + $bimonths;
						
					} else if($frequency == 'threemonthly') {
					
						$next_run = $invoice['next_run'] + $threemonths;
						
					} else if($frequency == 'sixmonthly') {
					
						$next_run = $invoice['next_run'] + $sixmonths;
						
					} else if($frequency == 'yearly') {
					
						$next_run = $invoice['next_run'] + $years;
						
					} else if($frequency == 'biyearly') {
					
						$next_run = $invoice['next_run'] + $biyears;
						
					}

					$recurring_invoices[$key]['next_run'] = $next_run;
										
					$recurring_invoices[$key]['last_run'] = time();					
				
				}
				
				if($end < $now) {
				
					$recurring_invoices[$key]['next_run'] = '<span class="task_over">' . __('Finished', 'cqpim') . '</div>';
					
					$recurring_invoices[$key]['status'] = 0;
				
				}
			
			}
			
			update_post_meta($client->ID, 'recurring_invoices', $recurring_invoices);
		
		}
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_add_new_recurring_invoice", 

			"cqpim_add_new_recurring_invoice");

	add_action( "wp_ajax_cqpim_add_new_recurring_invoice", 

		  	"cqpim_add_new_recurring_invoice");
	
	function cqpim_add_new_recurring_invoice() {
	
		$client_id = isset($_POST['client_id']) ? $_POST['client_id'] : '';
		
		$title = isset($_POST['title']) ? $_POST['title'] : '';
		
		$start = isset($_POST['start']) ? $_POST['start'] : '';
		
		$start = cqpim_convert_date($start);
		
		$end = isset($_POST['end']) ? $_POST['end'] : '';
		
		$end = cqpim_convert_date($end);
		
		$frequency = isset($_POST['frequency']) ? $_POST['frequency'] : '';
		
		$status = isset($_POST['status']) ? $_POST['status'] : '';
		
		$contact = isset($_POST['contact']) ? $_POST['contact'] : '';
		
		$auto = isset($_POST['auto']) ? $_POST['auto'] : '';
		
		$partial = isset($_POST['partial']) ? $_POST['partial'] : '';
		
		$items = isset($_POST['items']) ? $_POST['items'] : '';
		
		if(!empty($client_id)) {
		
			if(!empty($start)) {
			
				if(!is_numeric($start)) {
			
					$start_str = str_replace('/','-',$start);
				
					$next = strtotime($start_str);
				
				} else {
				
					$next = $start;
				
				}
			
			} else {
			
				$start = current_time('timestamp');
			
				$next = time();
				
			}
		
			$recurring_invoices = get_post_meta($client_id, 'recurring_invoices', true);
			
			if(empty($recurring_invoices)) {
			
				$recurring_invoices = array();
				
			}		
			
			$recurring_invoices[] = array(
			
				'title' => $title,
				
				'start' => $start,
				
				'end' => $end,
				
				'frequency' => $frequency,
				
				'status' => $status,
				
				'contact' => $contact,
				
				'auto' => $auto,
				
				'partial' => $partial,
				
				'items' => $items,
				
				'next_run' => $next,
			
			);
			
			update_post_meta($client_id, 'recurring_invoices', $recurring_invoices);
		
			$return =  array( 

				'error' 	=> false,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('The Recurring Invoice has been added.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The Client ID is missing and the Recurring Invoice could not be added. Please try again.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		}
	
	}
	
	// Edit Rec INVOICE
	
	add_action( "wp_ajax_nopriv_cqpim_edit_recurring_invoice", 

			"cqpim_edit_recurring_invoice");

	add_action( "wp_ajax_cqpim_edit_recurring_invoice", 

		  	"cqpim_edit_recurring_invoice");
	
	function cqpim_edit_recurring_invoice() {
	
		$key = isset($_POST['key']) ? $_POST['key'] : '';
	
		$client_id = isset($_POST['client_id']) ? $_POST['client_id'] : '';
		
		$title = isset($_POST['title']) ? $_POST['title'] : '';
		
		$start = isset($_POST['start']) ? $_POST['start'] : '';
		
		$start = cqpim_convert_date($start);
		
		$end = isset($_POST['end']) ? $_POST['end'] : '';
		
		$end = cqpim_convert_date($end);
		
		$frequency = isset($_POST['frequency']) ? $_POST['frequency'] : '';
		
		$status = isset($_POST['status']) ? $_POST['status'] : '';
		
		$contact = isset($_POST['contact']) ? $_POST['contact'] : '';
		
		$auto = isset($_POST['auto']) ? $_POST['auto'] : '';
		
		$partial = isset($_POST['partial']) ? $_POST['partial'] : '';
		
		$items = isset($_POST['items']) ? $_POST['items'] : '';
		
		if($client_id) {
		
			if(!empty($start)) {
			
				if(!is_numeric($start)) {
			
					$start_str = str_replace('/','-',$start);
				
					$next = strtotime($start_str);
				
				} else {
				
					$next = $start;
				
				}
			
			} else {
			
				$start = current_time('timestamp');
			
				$next = current_time('timestamp');
				
			}
		
			$recurring_invoices = get_post_meta($client_id, 'recurring_invoices', true);		
			
			$recurring_invoices[$key] = array(
			
				'title' => $title,
				
				'start' => $start,
				
				'end' => $end,
				
				'frequency' => $frequency,
				
				'status' => $status,
				
				'contact' => $contact,
				
				'auto' => $auto,
				
				'partial' => $partial,
				
				'items' => $items,
				
				'next_run' => $next,
			
			);
			
			update_post_meta($client_id, 'recurring_invoices', $recurring_invoices);
		
			$return =  array( 

				'error' 	=> false,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('The Recurring Invoice has been Updated.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The Client ID or Invoice Key is missing and the Recurring Invoice could not be edited. Please try again.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		}
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_delete_recurring_invoice", 

			"cqpim_delete_recurring_invoice");

	add_action( "wp_ajax_cqpim_delete_recurring_invoice", 

		  	"cqpim_delete_recurring_invoice");
	
	function cqpim_delete_recurring_invoice() {
	
		$client_id = $_POST['client_id'];
		
		$key = $_POST['key'];
		
		$recurring_invoices = get_post_meta($client_id, 'recurring_invoices', true);
		
		unset($recurring_invoices[$key]);
		
		$recurring_invoices = array_filter($recurring_invoices);
		
		update_post_meta($client_id, 'recurring_invoices', $recurring_invoices);
		
		$return =  array( 

			'error' 	=> false,

			'message' 	=> '',

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit();	
	
	}
	
	// CREATE DEPOSIT INVOICE ON CONTRACT ACCEPTANCE
	
	function cqpim_create_deposit_invoice($project_id, $pm_name = NULL) {
	
		if(get_option('disable_invoices') != 1) {
	
		$project_details = get_post_meta($project_id, 'project_details', true);
		
		$tax_app = get_post_meta($project_id, 'tax_applicable', true);	

		$tax_rate = get_post_meta($project_id, 'tax_rate', true);
		
		$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
		
		$project_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
		
		$client_contact = isset($project_details['client_contact']) ? $project_details['client_contact'] : '';
		
		$start = isset($project_details['start_date']) ? $project_details['start_date'] : '';
		
		$deposit = isset($project_details['deposit_amount']) ? $project_details['deposit_amount'] : '';
	
		$invoice_id = cqpim_get_invoice_id();
	
		$new_invoice = array(
		
			'post_type' => 'cqpim_invoice',
			
			'post_status' => 'publish',
			
			'post_content' => '',
			
			'post_title' =>  $invoice_id,
			
			'post_password' => cqpim_random_string(10),
		
		);
		
		$invoice_pid = wp_insert_post( $new_invoice, true );
		
		if( ! is_wp_error( $invoice_pid ) ){
		
			// now we have an id, let's update the invoice title	
			
			if(!is_numeric($start)) {
			
				$start_unix = str_replace('/', '-', $start);

				$start_unix = strtotime($start_unix);
			
			} else {
			
				$start_unix = $start;
			
			}
		
			$invoice_details = array(
			
				'client_id' => $client_id,
				
				'deposit' => true,
				
				'due' => $start,
				
				'project_id' => $project_id,
				
				'terms_over' => $start_unix,
				
				'invoice_date' => current_time('timestamp'),
				
				'allow_partial' => false,
			
			);
			
			update_post_meta($invoice_pid, 'invoice_project', $project_id);
			
			update_post_meta($invoice_pid, 'invoice_client', $client_id);
			
			update_post_meta($invoice_pid, 'client_contact', $client_contact);
			
			update_post_meta($invoice_pid, 'invoice_id', $invoice_id);
			
			update_post_meta($invoice_pid, 'invoice_details', $invoice_details);			
			
			$currency = get_option('currency_symbol');
			
			$currency_code = get_option('currency_code');
			
			$currency_position = get_option('currency_symbol_position');
			
			$currency_space = get_option('currency_symbol_space'); 
			
			$client_currency = get_post_meta($client_id, 'currency_symbol', true);
				
			$client_currency_code = get_post_meta($client_id, 'currency_code', true);
				
			$client_currency_space = get_post_meta($client_id, 'currency_space', true);		

			$client_currency_position = get_post_meta($client_id, 'currency_position', true);
			
			$quote_currency = get_post_meta($project_id, 'currency_symbol', true);
				
			$quote_currency_code = get_post_meta($project_id, 'currency_code', true);
				
			$quote_currency_space = get_post_meta($project_id, 'currency_space', true);	

			$quote_currency_position = get_post_meta($project_id, 'currency_position', true);
			
			if(!empty($quote_currency)) {
			
				update_post_meta($invoice_pid, 'currency_symbol', $quote_currency);
			
			} else {
			
				if(!empty($client_currency)) {
				
					update_post_meta($invoice_pid, 'currency_symbol', $client_currency);
				
				} else {
				
					update_post_meta($invoice_pid, 'currency_symbol', $currency);
				
				}
			
			}
			
			if(!empty($quote_currency_code)) {
			
				update_post_meta($invoice_pid, 'currency_code', $quote_currency_code);
			
			} else {
			
				if(!empty($client_currency_code)) {
				
					update_post_meta($invoice_pid, 'currency_code', $client_currency_code);
				
				} else {
				
					update_post_meta($invoice_pid, 'currency_code', $currency_code);
				
				}
			
			}
			
			if(!empty($quote_currency_space)) {
			
				update_post_meta($invoice_pid, 'currency_space', $quote_currency_space);
			
			} else {
			
				if(!empty($client_currency_space)) {
				
					update_post_meta($invoice_pid, 'currency_space', $client_currency_space);
				
				} else {
				
					update_post_meta($invoice_pid, 'currency_space', $currency_space);
				
				}
			
			}
			
			if(!empty($quote_currency_position)) {
			
				update_post_meta($invoice_pid, 'currency_position', $quote_currency_position);
			
			} else {
			
				if(!empty($client_currency_position)) {
				
					update_post_meta($invoice_pid, 'currency_position', $client_currency_position);
				
				} else {
				
					update_post_meta($invoice_pid, 'currency_position', $currency_position);
				
				}
			
			}			
			
			$project_details = get_post_meta($project_id, 'project_details', true);
			
			$project_details['deposit_invoice_id'] = $invoice_pid;
			
			update_post_meta($project_id, 'project_details', $project_details);
			
			$project_elements = get_post_meta($project_id, 'project_elements', true);
			
			$project_total = 0;
			
			foreach($project_elements as $element) {
			
				$cost = preg_replace("/[^\\d.]+/","", $element['cost']);
			
				$element_total = $cost;
			
				$project_total = $project_total + $element_total;
			
			}
			
			$deposit_to_pay = $project_total / 100 * $deposit;
			
			$deposit_to_pay = number_format((float)$deposit_to_pay, 2, '.', '');
			
			$description = sprintf(__('Deposit Payment. Project Ref %1$s', 'cqpim'), $project_ref);
			
			$line_items = array();
			
			$line_items[] = array(
			
				'qty' => 1,
				
				'desc' => $description,
				
				'price' => $deposit_to_pay,
				
				'sub' => $deposit_to_pay				
			
			);
			
			update_post_meta($invoice_pid, 'line_items', $line_items);
			
			$tax_app = get_post_meta($invoice_pid, 'tax_set', true);
			
			$system_tax = get_option('sales_tax_rate');
			
			$system_stax = get_option('secondary_sales_tax_rate');

			if(empty($tax_app)) {

				$client_details = get_post_meta($client_id, 'client_details', true);
				
				$client_tax = isset($client_details['tax_disabled']) ? $client_details['tax_disabled'] : '';
				
				$client_stax = isset($client_details['stax_disabled']) ? $client_details['stax_disabled'] : '';
				
				if(!empty($system_tax) && empty($client_tax)) {
				
					update_post_meta($invoice_pid, 'tax_applicable', 1);
					
					update_post_meta($invoice_pid, 'tax_set', 1);	

					update_post_meta($invoice_pid, 'tax_rate', $system_tax);	
					
					if(!empty($system_stax) && empty($client_stax)) {
					
						update_post_meta($invoice_pid, 'stax_applicable', 1);
						
						update_post_meta($invoice_pid, 'stax_set', 1);	

						update_post_meta($invoice_pid, 'stax_rate', $system_stax);			
					
					} else {
					
						update_post_meta($invoice_pid, 'stax_applicable', 0);
						
						update_post_meta($invoice_pid, 'stax_set', 1);

						update_post_meta($invoice_pid, 'stax_rate', 0);				
					
					}
				
				} else {
				
					update_post_meta($invoice_pid, 'tax_applicable', 0);
					
					update_post_meta($invoice_pid, 'tax_set', 1);

					update_post_meta($invoice_pid, 'tax_rate', 0);			
				
				}

			}	

			$subtotal = $deposit_to_pay;
			
			if(!empty($system_tax) && empty($client_tax)) {

				$tax = $subtotal / 100 * $system_tax;
				
				if(!empty($system_stax) && empty($client_stax)) {
				
					$stax = $subtotal / 100 * $system_stax;
					
					$total = $subtotal + $tax + $stax;
				
				} else {
				
					$stax = 0;
				
					$total = $subtotal + $tax;
					
				}
				
			} else {

				$tax = 0;
				
				$total = $subtotal;
				
			}

			$invoice_totals = array(

				'sub' => number_format((float)$subtotal, 2, '.', ''),
				
				'tax' => number_format((float)$tax, 2, '.', ''),
				
				'stax' => number_format((float)$stax, 2, '.', ''),
				
				'total' => number_format((float)$total, 2, '.', '')

			);
			
			update_post_meta($invoice_pid, 'invoice_totals', $invoice_totals);			
			
			$auto_invoice = get_option('auto_send_invoices');
			
			$project_details = get_post_meta($project_id, 'project_details', true);
			
			$pm_name = isset($project_details['pm_name']) ? $project_details['pm_name'] : '';
			
			$project_progress = get_post_meta($project_id, 'project_progress', true);
			
			$text = sprintf(__('Deposit Invoice created for Project %1$s', 'cqpim'), $project_ref);
				
			$project_progress[] = array(
				
				'update' => $text,
					
				'date' => current_time('timestamp'),
				
				'by' => 'System'
					
			);
				
			update_post_meta($project_id, 'project_progress', $project_progress );
			
			if($auto_invoice) {
			
				$deposit = true;
			
				cqpim_process_invoice_emails($invoice_pid, $pm_name, $deposit);
			
			}
			
		
		} else {
		
			exit();
			
		}	
		
		} else {
		
			return;
			
		}
	
	}
	
	// CREATE COMPLETION INVOICE
	
	function cqpim_create_completion_invoice($project_id) {
	
		if(get_option('disable_invoices') != 1) {
	
		$project_details = get_post_meta($project_id, 'project_details', true);
		
		$tax_app = get_post_meta($project_id, 'tax_applicable', true);	

		$tax_rate = get_post_meta($project_id, 'tax_rate', true);
		
		$deposit_invoice = isset($project_details['deposit_invoice_id']) ? $project_details['deposit_invoice_id'] : '';
		
		$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
		
		$client_contact = isset($project_details['client_contact']) ? $project_details['client_contact'] : '';
		
		$project_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
		
		$type = isset($project_details['quote_type']) ? $project_details['quote_type'] : '';
		
		// Prepare the line items for the completion invoice
		
		$project_elements = get_post_meta($project_id, 'project_elements', true);
			
		$project_total = 0;
			
		foreach($project_elements as $element) {
			
			$cost = preg_replace("/[^\\d.]+/","", $element['acost']);
			
			$element_total = $cost;
			
			$project_total = $project_total + $element_total;
			
		}		
			
		$line_items = array();
			
		foreach($project_elements as $element) {
		
			$cost = preg_replace("/[^\\d.]+/","", $element['acost']);
			
			$line_items[] = array(
					
				'qty' => 1,
						
				'desc' => $element['title'],
						
				'price' => number_format((float)$cost, 2, '.', ''),
						
				'sub' => number_format((float)$cost, 2, '.', '')				
					
			);
			
		}
		
		if(!empty($deposit_invoice)) {
		
			$deposit_invoice = get_post($deposit_invoice);
					
			$invoice_details = get_post_meta($deposit_invoice->ID, 'invoice_details', true);
			
			$invoice_id = get_post_meta($deposit_invoice->ID, 'invoice_id', true);	
				
			if(!empty($invoice_details['paid'])) {
		
				$line_items_dep = get_post_meta($deposit_invoice->ID, 'line_items', true);
				
				foreach($line_items_dep as $item) {
				
					$line_items[] = array(
								
						'qty' => $item['qty'],
									
						'desc' => sprintf(__('LESS ALREADY RECEIVED - Invoice: %1$s', 'cqpim'),  $invoice_id),
									
						'price' => '-' . $item['price'],
									
						'sub' => '-' . $item['sub']				
								
					);
				
				}
			
			}
		
		}
	
		$invoice_id = cqpim_get_invoice_id();
	
		$new_invoice = array(
		
			'post_type' => 'cqpim_invoice',
			
			'post_status' => 'publish',
			
			'post_content' => '',
			
			'post_title' =>  $invoice_id,
			
			'post_password' => cqpim_random_string(10),
		
		);
		
		$invoice_pid = wp_insert_post( $new_invoice, true );
		
		if( ! is_wp_error( $invoice_pid ) ){
			
			$client_details = get_post_meta($client_id, 'client_details', true);
			
			$invoice_terms = isset($client_details['invoice_terms']) ? $client_details['invoice_terms']: '';
		
			$system_invoice_terms = get_option('company_invoice_terms');
			
			if(!empty($invoice_terms)) {
			
				$terms = $invoice_terms;
				
			} else {
			
				$terms = $system_invoice_terms;
				
			}
			
			$date = time();
			
			$terms_over = strtotime('+' . $terms . ' days', $date);
			
			$allow_partial = get_option('client_invoice_allow_partial');
			
			$allow_partial = isset($allow_partial) ? $allow_partial : 0;
			
			if($terms != 1) {
		
				$invoice_details = array(
				
					'client_id' => $client_id,
					
					'project_id' => $project_id,
					
					'completion' => true,
					
					'terms_over' => $terms_over,
					
					'invoice_date' => current_time('timestamp'),
					
					'allow_partial' => $allow_partial,
				
				);
			
			} else {
			
				$invoice_details = array(
				
					'client_id' => $client_id,
					
					'project_id' => $project_id,
					
					'on_receipt' => true,
					
					'completion' => true,
					
					'terms_over' => $terms_over,
					
					'invoice_date' => current_time('timestamp'),
					
					'allow_partial' => $allow_partial,
				
				);
			
			
			}
			
			update_post_meta($invoice_pid, 'invoice_project', $project_id);
			
			update_post_meta($invoice_pid, 'invoice_id', $invoice_id);
			
			update_post_meta($invoice_pid, 'client_contact', $client_contact);
			
			update_post_meta($invoice_pid, 'invoice_details', $invoice_details);
			
			update_post_meta($invoice_pid, 'invoice_client', $client_id);
			
			$currency = get_option('currency_symbol');
			
			$currency_code = get_option('currency_code');
			
			$currency_position = get_option('currency_symbol_position');
			
			$currency_space = get_option('currency_symbol_space'); 
			
			$client_currency = get_post_meta($client_id, 'currency_symbol', true);
				
			$client_currency_code = get_post_meta($client_id, 'currency_code', true);
				
			$client_currency_space = get_post_meta($client_id, 'currency_space', true);		

			$client_currency_position = get_post_meta($client_id, 'currency_position', true);
			
			$quote_currency = get_post_meta($project_id, 'currency_symbol', true);
				
			$quote_currency_code = get_post_meta($project_id, 'currency_code', true);
				
			$quote_currency_space = get_post_meta($project_id, 'currency_space', true);	

			$quote_currency_position = get_post_meta($project_id, 'currency_position', true);
			
			if(!empty($quote_currency)) {
			
				update_post_meta($invoice_pid, 'currency_symbol', $quote_currency);
			
			} else {
			
				if(!empty($client_currency)) {
				
					update_post_meta($invoice_pid, 'currency_symbol', $client_currency);
				
				} else {
				
					update_post_meta($invoice_pid, 'currency_symbol', $currency);
				
				}
			
			}
			
			if(!empty($quote_currency_code)) {
			
				update_post_meta($invoice_pid, 'currency_code', $quote_currency_code);
			
			} else {
			
				if(!empty($client_currency_code)) {
				
					update_post_meta($invoice_pid, 'currency_code', $client_currency_code);
				
				} else {
				
					update_post_meta($invoice_pid, 'currency_code', $currency_code);
				
				}
			
			}
			
			if(!empty($quote_currency_space)) {
			
				update_post_meta($invoice_pid, 'currency_space', $quote_currency_space);
			
			} else {
			
				if(!empty($client_currency_space)) {
				
					update_post_meta($invoice_pid, 'currency_space', $client_currency_space);
				
				} else {
				
					update_post_meta($invoice_pid, 'currency_space', $currency_space);
				
				}
			
			}
			
			if(!empty($quote_currency_position)) {
			
				update_post_meta($invoice_pid, 'currency_position', $quote_currency_position);
			
			} else {
			
				if(!empty($client_currency_position)) {
				
					update_post_meta($invoice_pid, 'currency_position', $client_currency_position);
				
				} else {
				
					update_post_meta($invoice_pid, 'currency_position', $currency_position);
				
				}
			
			}
			
			// Tell the project that this invoice is the completion invoice for it
			
			$project_details = get_post_meta($project_id, 'project_details', true);
			
			$project_details['completion_invoice_id'] = $invoice_pid;
			
			update_post_meta($project_id, 'project_details', $project_details);
			
			// Update the line items with the array that we created earlier.
			
			update_post_meta($invoice_pid, 'line_items', $line_items);
			
			// Add invoice totals
			
			$subtotal = 0;
			
			foreach($line_items as $item) {
			
				$subtotal = $subtotal + $item['sub'];
			
			}
			
			$tax_app = get_post_meta($invoice_pid, 'tax_set', true);
			
			$system_tax = get_option('sales_tax_rate');
			
			$system_stax = get_option('secondary_sales_tax_rate');

			if(empty($tax_app)) {

				$client_details = get_post_meta($client_id, 'client_details', true);
				
				$client_tax = isset($client_details['tax_disabled']) ? $client_details['tax_disabled'] : '';
				
				$client_stax = isset($client_details['stax_disabled']) ? $client_details['stax_disabled'] : '';
				
				if(!empty($system_tax) && empty($client_tax)) {
				
					update_post_meta($invoice_pid, 'tax_applicable', 1);
					
					update_post_meta($invoice_pid, 'tax_set', 1);	

					update_post_meta($invoice_pid, 'tax_rate', $system_tax);	
					
					if(!empty($system_stax) && empty($client_stax)) {
					
						update_post_meta($invoice_pid, 'stax_applicable', 1);
						
						update_post_meta($invoice_pid, 'stax_set', 1);	

						update_post_meta($invoice_pid, 'stax_rate', $system_stax);			
					
					} else {
					
						update_post_meta($invoice_pid, 'stax_applicable', 0);
						
						update_post_meta($invoice_pid, 'stax_set', 1);

						update_post_meta($invoice_pid, 'stax_rate', 0);				
					
					}
				
				} else {
				
					update_post_meta($invoice_pid, 'tax_applicable', 0);
					
					update_post_meta($invoice_pid, 'tax_set', 1);

					update_post_meta($invoice_pid, 'tax_rate', 0);			
				
				}

			}
			
			if(!empty($system_tax) && empty($client_tax)) {

				$tax = $subtotal / 100 * $system_tax;
				
				if(!empty($system_stax) && empty($client_stax)) {
				
					$stax = $subtotal / 100 * $system_stax;
					
					$total = $subtotal + $tax + $stax;
				
				} else {
				
					$stax = 0;
				
					$total = $subtotal + $tax;
					
				}
				
			} else {

				$tax = 0;
				
				$total = $subtotal;
				
			}

			$invoice_totals = array(

				'sub' => number_format((float)$subtotal, 2, '.', ''),
				
				'tax' => number_format((float)$tax, 2, '.', ''),
				
				'stax' => number_format((float)$stax, 2, '.', ''),
				
				'total' => number_format((float)$total, 2, '.', '')

			);
			
			update_post_meta($invoice_pid, 'invoice_totals', $invoice_totals);			
			
			$auto_invoice = get_option('auto_send_invoices');
			
			$project_details = get_post_meta($project_id, 'project_details', true);
			
			$project_progress = get_post_meta($project_id, 'project_progress', true);
			
			$text = sprintf(__('Completion Invoice created for Project %1$s', 'cqpim'), $project_details['quote_ref']);
				
			$project_progress[] = array(
				
				'update' => $text,
					
				'date' => current_time('timestamp'),
				
				'by' => 'System'
					
			);
				
			update_post_meta($project_id, 'project_progress', $project_progress );
			
			if($auto_invoice == 1) {
			
				$completion = true;
			
				cqpim_process_invoice_emails($invoice_pid);
			
			}
			
		
		} else {
		
			exit();
			
		}	
		
		} else {
		
			return;
	
		}
	
	}

	
	function cqpim_create_ms_completion_invoice($project_id, $milestone) {	
	
		if(get_option('disable_invoices') != 1) {
		
			$project_details = get_post_meta($project_id, 'project_details', true);
			
			$tax_app = get_post_meta($project_id, 'tax_applicable', true);	

			$tax_rate = get_post_meta($project_id, 'tax_rate', true);
			
			$project_elements = get_post_meta($project_id, 'project_elements', true);		
			
			$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
			
			$project_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
			
			$client_contact = isset($project_details['client_contact']) ? $project_details['client_contact'] : '';
			
			$deposit = isset($project_details['deposit_amount']) ? $project_details['deposit_amount'] : '';
			
			$invoice_id = cqpim_get_invoice_id();
		
			$new_invoice = array(
			
				'post_type' => 'cqpim_invoice',
				
				'post_status' => 'publish',
				
				'post_content' => '',
				
				'post_title' =>  $invoice_id,
				
				'post_password' => cqpim_random_string(10),
			
			);
			
			remove_action('save_post', 'save_project_elements_metabox_data');
			
			$invoice_pid = wp_insert_post( $new_invoice, true );
			
			add_action('save_post', 'save_project_elements_metabox_data');	

			if( ! is_wp_error( $invoice_pid ) ){
			
				$client_details = get_post_meta($client_id, 'client_details', true);
				
				$invoice_terms = isset($client_details['invoice_terms']) ? $client_details['invoice_terms']: '';
			
				$system_invoice_terms = get_option('company_invoice_terms');
				
				if(!empty($invoice_terms)) {
				
					$terms = $invoice_terms;
					
				} else {
				
					$terms = $system_invoice_terms;
					
				}
				
				$date = time();
				
				$terms_over = strtotime('+' . $terms . ' days', $date);
				
				$allow_partial = get_option('client_invoice_allow_partial');
				
				$allow_partial = isset($allow_partial) ? $allow_partial : 0;
				
				if($terms != 1) {
			
					$invoice_details = array(
					
						'client_id' => $client_id,
						
						'project_id' => $project_id,
						
						'completion' => true,
						
						'terms_over' => $terms_over,
						
						'invoice_date' => current_time('timestamp'),
						
						'allow_partial' => $allow_partial,
					
					);
				
				} else {
				
					$invoice_details = array(
					
						'client_id' => $client_id,
						
						'project_id' => $project_id,
						
						'on_receipt' => true,
						
						'completion' => true,
						
						'terms_over' => $terms_over,
						
						'invoice_date' => current_time('timestamp'),
						
						'allow_partial' => $allow_partial,
					
					);
				
				
				}
				
				update_post_meta($invoice_pid, 'invoice_project', $project_id);
				
				update_post_meta($invoice_pid, 'invoice_id', $invoice_id);
				
				update_post_meta($invoice_pid, 'client_contact', $client_contact);
				
				update_post_meta($invoice_pid, 'invoice_details', $invoice_details);
				
				update_post_meta($invoice_pid, 'invoice_client', $client_id);	
				
				$currency = get_option('currency_symbol');
				
				$currency_code = get_option('currency_code');
				
				$currency_position = get_option('currency_symbol_position');
				
				$currency_space = get_option('currency_symbol_space'); 
				
				$client_currency = get_post_meta($client_id, 'currency_symbol', true);
					
				$client_currency_code = get_post_meta($client_id, 'currency_code', true);
					
				$client_currency_space = get_post_meta($client_id, 'currency_space', true);		

				$client_currency_position = get_post_meta($client_id, 'currency_position', true);
				
				$quote_currency = get_post_meta($project_id, 'currency_symbol', true);
					
				$quote_currency_code = get_post_meta($project_id, 'currency_code', true);
					
				$quote_currency_space = get_post_meta($project_id, 'currency_space', true);	

				$quote_currency_position = get_post_meta($project_id, 'currency_position', true);
				
				if(!empty($quote_currency)) {
				
					update_post_meta($invoice_pid, 'currency_symbol', $quote_currency);
				
				} else {
				
					if(!empty($client_currency)) {
					
						update_post_meta($invoice_pid, 'currency_symbol', $client_currency);
					
					} else {
					
						update_post_meta($invoice_pid, 'currency_symbol', $currency);
					
					}
				
				}
				
				if(!empty($quote_currency_code)) {
				
					update_post_meta($invoice_pid, 'currency_code', $quote_currency_code);
				
				} else {
				
					if(!empty($client_currency_code)) {
					
						update_post_meta($invoice_pid, 'currency_code', $client_currency_code);
					
					} else {
					
						update_post_meta($invoice_pid, 'currency_code', $currency_code);
					
					}
				
				}
				
				if(!empty($quote_currency_space)) {
				
					update_post_meta($invoice_pid, 'currency_space', $quote_currency_space);
				
				} else {
				
					if(!empty($client_currency_space)) {
					
						update_post_meta($invoice_pid, 'currency_space', $client_currency_space);
					
					} else {
					
						update_post_meta($invoice_pid, 'currency_space', $currency_space);
					
					}
				
				}
				
				if(!empty($quote_currency_position)) {
				
					update_post_meta($invoice_pid, 'currency_position', $quote_currency_position);
				
				} else {
				
					if(!empty($client_currency_position)) {
					
						update_post_meta($invoice_pid, 'currency_position', $client_currency_position);
					
					} else {
					
						update_post_meta($invoice_pid, 'currency_position', $currency_position);
					
					}
				
				}
				
				$total = 0;	
					
				$cost = preg_replace("/[^\\d.]+/","", $milestone['acost']);
					
				$element_total = $cost;
					
				$project_total = $project_total + $element_total;					
					
				$line_items = array();		
					
				$line_items[] = array(
						
					'qty' => 1,
							
					'desc' => __('Milestone', 'cqpim') . ': ' . $milestone['title'],
							
					'price' => number_format((float)$cost, 2, '.', ''),
							
					'sub' => number_format((float)$cost, 2, '.', '')				
						
				);	
				
				if($deposit != 'none') {
				
					$deductions = $cost / 100 * $deposit;

					$line_items[] = array(
							
						'qty' => 1,
								
						'desc' => sprintf(__('LESS DEPOSIT PERCENTAGE - %1$s ', 'cqpim'),  $deposit),
								
						'price' => '-' . $deductions,
								
						'sub' => '-' . $deductions,				
							
					);	

				}

				// Update the line items with the array that we created earlier.
				
				update_post_meta($invoice_pid, 'line_items', $line_items);
				
				// Add invoice totals
				
				$subtotal = 0;
				
				foreach($line_items as $item) {
				
					$subtotal = $subtotal + $item['sub'];
				
				}
				
				$tax_app = get_post_meta($invoice_pid, 'tax_set', true);
				
				$system_tax = get_option('sales_tax_rate');
				
				$system_stax = get_option('secondary_sales_tax_rate');

				if(empty($tax_app)) {

					$client_details = get_post_meta($client_id, 'client_details', true);
					
					$client_tax = isset($client_details['tax_disabled']) ? $client_details['tax_disabled'] : '';
					
					$client_stax = isset($client_details['stax_disabled']) ? $client_details['stax_disabled'] : '';
					
					if(!empty($system_tax) && empty($client_tax)) {
					
						update_post_meta($invoice_pid, 'tax_applicable', 1);
						
						update_post_meta($invoice_pid, 'tax_set', 1);	

						update_post_meta($invoice_pid, 'tax_rate', $system_tax);	
						
						if(!empty($system_stax) && empty($client_stax)) {
						
							update_post_meta($invoice_pid, 'stax_applicable', 1);
							
							update_post_meta($invoice_pid, 'stax_set', 1);	

							update_post_meta($invoice_pid, 'stax_rate', $system_stax);			
						
						} else {
						
							update_post_meta($invoice_pid, 'stax_applicable', 0);
							
							update_post_meta($invoice_pid, 'stax_set', 1);

							update_post_meta($invoice_pid, 'stax_rate', 0);				
						
						}
					
					} else {
					
						update_post_meta($invoice_pid, 'tax_applicable', 0);
						
						update_post_meta($invoice_pid, 'tax_set', 1);

						update_post_meta($invoice_pid, 'tax_rate', 0);			
					
					}

				}
				
				if(!empty($system_tax) && empty($client_tax)) {

					$tax = $subtotal / 100 * $system_tax;
					
					if(!empty($system_stax) && empty($client_stax)) {
					
						$stax = $subtotal / 100 * $system_stax;
						
						$total = $subtotal + $tax + $stax;
					
					} else {
					
						$stax = 0;
					
						$total = $subtotal + $tax;
						
					}
					
				} else {

					$tax = 0;
					
					$total = $subtotal;
					
				}

				$invoice_totals = array(

					'sub' => number_format((float)$subtotal, 2, '.', ''),
					
					'tax' => number_format((float)$tax, 2, '.', ''),
					
					'stax' => number_format((float)$stax, 2, '.', ''),
					
					'total' => number_format((float)$total, 2, '.', '')

				);
				
				update_post_meta($invoice_pid, 'invoice_totals', $invoice_totals);	
				
				$auto_invoice = get_option('auto_send_invoices');
				
				$project_details = get_post_meta($project_id, 'project_details', true);
				
				if($auto_invoice == 1) {
				
					cqpim_process_invoice_emails($invoice_pid);
				
				}			
			
			}
		
		} else {
		
			return;
			
		}
	
	}
	
	// CREATE TICKET INVOICE
	
	add_action( "wp_ajax_nopriv_cqpim_send_ticket_invoice", 

			"cqpim_send_ticket_invoice");

	add_action( "wp_ajax_cqpim_send_ticket_invoice", 

		  	"cqpim_send_ticket_invoice");
	
	function cqpim_send_ticket_invoice($ticket_id) {
	
		if(get_option('disable_invoices') != 1) {
		
			$ticket_id = isset($_POST['pid']) ? $_POST['pid'] : '';
			
			if(empty($ticket_id)) {
			
				$return =  array( 

					'error' 	=> true,

					'errors' 	=> __('Ticket Id not present, please try again', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);
				
				exit;
			
			}
			
			$ticket_object = get_post($ticket_id);
			
			$client_id = get_post_meta($ticket_id, 'ticket_client', true);			
			
			// Prepare the line items for the completion invoice
			
			$project_elements = get_post_meta($ticket_id, 'quote_elements', true);
			
			// Check MS status
			
			foreach($project_elements as $element) {
			
				$cost = isset($element['acost']) ? $element['acost'] : '';
				
				$status = isset($element['status']) ? $element['status'] : '';
			
				$cost = preg_replace("/[^\\d.]+/","", $element['acost']);
				
				if(empty($cost) || $status != 'complete') {
				
					$fail = true;
					
				}
			
			}
			
			if(!empty($fail)) {
			
				$return =  array( 

					'error' 	=> true,

					'errors' 	=> __('At least one milestone does not have a finished cost or is not marked as complete!', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);
				
				exit;			
			
			}			
				
			$project_total = 0;
				
			foreach($project_elements as $element) {
				
				$cost = preg_replace("/[^\\d.]+/","", $element['acost']);
				
				$element_total = $cost;
				
				$project_total = $project_total + $element_total;
				
			}		
				
			$line_items = array();
				
			foreach($project_elements as $element) {
			
				$cost = preg_replace("/[^\\d.]+/","", $element['acost']);
				
				$line_items[] = array(
						
					'qty' => 1,
							
					'desc' => __('Ticket: ', 'cqpim') . $ticket_object->ID . ' - ' . $element['title'],
							
					'price' => number_format((float)$cost, 2, '.', ''),
							
					'sub' => number_format((float)$cost, 2, '.', '')				
						
				);
				
			}
		
			$invoice_id = cqpim_get_invoice_id();
		
			$new_invoice = array(
			
				'post_type' => 'cqpim_invoice',
				
				'post_status' => 'publish',
				
				'post_content' => '',
				
				'post_title' =>  $invoice_id,
				
				'post_password' => cqpim_random_string(10),
			
			);
			
			$invoice_pid = wp_insert_post( $new_invoice, true );
			
			if( ! is_wp_error( $invoice_pid ) ){
				
				$client_details = get_post_meta($client_id, 'client_details', true);
				
				$invoice_terms = isset($client_details['invoice_terms']) ? $client_details['invoice_terms']: '';
			
				$system_invoice_terms = get_option('company_invoice_terms');
				
				if(!empty($invoice_terms)) {
				
					$terms = $invoice_terms;
					
				} else {
				
					$terms = $system_invoice_terms;
					
				}
				
				$date = time();
				
				$terms_over = strtotime('+' . $terms . ' days', $date);
				
				$allow_partial = get_option('client_invoice_allow_partial');
				
				$allow_partial = isset($allow_partial) ? $allow_partial : 0;
				
				if($terms != 1) {
			
					$invoice_details = array(
					
						'client_id' => $client_id,
						
						'project_id' => $ticket_id,
						
						'completion' => true,
						
						'terms_over' => $terms_over,
						
						'invoice_date' => current_time('timestamp'),
						
						'ticket' => true,
						
						'allow_partial' => $allow_partial,
					
					);
				
				} else {
				
					$invoice_details = array(
					
						'client_id' => $client_id,
						
						'project_id' => $ticket_id,
						
						'on_receipt' => true,
						
						'completion' => true,
						
						'terms_over' => $terms_over,
						
						'invoice_date' => current_time('timestamp'),
						
						'ticket' => true,
						
						'allow_partial' => $allow_partial,
					
					);
				
				
				}
				
				update_post_meta($invoice_pid, 'invoice_project', $ticket_id);
				
				update_post_meta($invoice_pid, 'invoice_id', $invoice_id);
				
				update_post_meta($invoice_pid, 'invoice_details', $invoice_details);
				
				update_post_meta($invoice_pid, 'invoice_client', $client_id);
				
				update_post_meta($invoice_pid, 'client_contact', $ticket_object->post_author);
				
				$currency = get_option('currency_symbol');
				
				$currency_code = get_option('currency_code');
				
				$currency_position = get_option('currency_symbol_position');
				
				$currency_space = get_option('currency_symbol_space'); 
				
				$client_currency = get_post_meta($client_id, 'currency_symbol', true);
					
				$client_currency_code = get_post_meta($client_id, 'currency_code', true);
					
				$client_currency_space = get_post_meta($client_id, 'currency_space', true);		

				$client_currency_position = get_post_meta($client_id, 'currency_position', true);
				
				$quote_currency = get_post_meta($ticket_id, 'currency_symbol', true);
					
				$quote_currency_code = get_post_meta($ticket_id, 'currency_code', true);
					
				$quote_currency_space = get_post_meta($ticket_id, 'currency_space', true);	

				$quote_currency_position = get_post_meta($ticket_id, 'currency_position', true);
				
				if(!empty($quote_currency)) {
				
					update_post_meta($invoice_pid, 'currency_symbol', $quote_currency);
				
				} else {
				
					if(!empty($client_currency)) {
					
						update_post_meta($invoice_pid, 'currency_symbol', $client_currency);
					
					} else {
					
						update_post_meta($invoice_pid, 'currency_symbol', $currency);
					
					}
				
				}
				
				if(!empty($quote_currency_code)) {
				
					update_post_meta($invoice_pid, 'currency_code', $quote_currency_code);
				
				} else {
				
					if(!empty($client_currency_code)) {
					
						update_post_meta($invoice_pid, 'currency_code', $client_currency_code);
					
					} else {
					
						update_post_meta($invoice_pid, 'currency_code', $currency_code);
					
					}
				
				}
				
				if(!empty($quote_currency_space)) {
				
					update_post_meta($invoice_pid, 'currency_space', $quote_currency_space);
				
				} else {
				
					if(!empty($client_currency_space)) {
					
						update_post_meta($invoice_pid, 'currency_space', $client_currency_space);
					
					} else {
					
						update_post_meta($invoice_pid, 'currency_space', $currency_space);
					
					}
				
				}
				
				if(!empty($quote_currency_position)) {
				
					update_post_meta($invoice_pid, 'currency_position', $quote_currency_position);
				
				} else {
				
					if(!empty($client_currency_position)) {
					
						update_post_meta($invoice_pid, 'currency_position', $client_currency_position);
					
					} else {
					
						update_post_meta($invoice_pid, 'currency_position', $currency_position);
					
					}
				
				}			

				
				// Update the line items with the array that we created earlier.
				
				update_post_meta($invoice_pid, 'line_items', $line_items);
				
				// Add invoice totals
				
				$subtotal = 0;
				
				foreach($line_items as $item) {
				
					$subtotal = $subtotal + $item['sub'];
				
				}
				
				$tax_app = get_post_meta($invoice_pid, 'tax_set', true);
				
				$system_tax = get_option('sales_tax_rate');
				
				$system_stax = get_option('secondary_sales_tax_rate');

				if(empty($tax_app)) {

					$client_details = get_post_meta($client_id, 'client_details', true);
					
					$client_tax = isset($client_details['tax_disabled']) ? $client_details['tax_disabled'] : '';
					
					$client_stax = isset($client_details['stax_disabled']) ? $client_details['stax_disabled'] : '';
					
					if(!empty($system_tax) && empty($client_tax)) {
					
						update_post_meta($invoice_pid, 'tax_applicable', 1);
						
						update_post_meta($invoice_pid, 'tax_set', 1);	

						update_post_meta($invoice_pid, 'tax_rate', $system_tax);	
						
						if(!empty($system_stax) && empty($client_stax)) {
						
							update_post_meta($invoice_pid, 'stax_applicable', 1);
							
							update_post_meta($invoice_pid, 'stax_set', 1);	

							update_post_meta($invoice_pid, 'stax_rate', $system_stax);			
						
						} else {
						
							update_post_meta($invoice_pid, 'stax_applicable', 0);
							
							update_post_meta($invoice_pid, 'stax_set', 1);

							update_post_meta($invoice_pid, 'stax_rate', 0);				
						
						}
					
					} else {
					
						update_post_meta($invoice_pid, 'tax_applicable', 0);
						
						update_post_meta($invoice_pid, 'tax_set', 1);

						update_post_meta($invoice_pid, 'tax_rate', 0);			
					
					}

				}
				
				if(!empty($system_tax) && empty($client_tax)) {

					$tax = $subtotal / 100 * $system_tax;
					
					if(!empty($system_stax) && empty($client_stax)) {
					
						$stax = $subtotal / 100 * $system_stax;
						
						$total = $subtotal + $tax + $stax;
					
					} else {
					
						$stax = 0;
					
						$total = $subtotal + $tax;
						
					}
					
				} else {

					$tax = 0;
					
					$total = $subtotal;
					
				}

				$invoice_totals = array(

					'sub' => number_format((float)$subtotal, 2, '.', ''),
					
					'tax' => number_format((float)$tax, 2, '.', ''),
					
					'stax' => number_format((float)$stax, 2, '.', ''),
					
					'total' => number_format((float)$total, 2, '.', '')

				);
				
				update_post_meta($invoice_pid, 'invoice_totals', $invoice_totals);			
				
				$auto_invoice = get_option('auto_send_invoices');
				
				if($auto_invoice == 1) {
				
					$completion = true;
				
					cqpim_process_invoice_emails($invoice_pid);
				
				}	

				$ticket_details = get_post_meta($ticket_id, 'project_details', true);
				
				$ticket_details['invoice_sent'] = true;
				
				update_post_meta($ticket_id, 'project_details', $ticket_details);				
				
				$return =  array( 

					'error' 	=> false,

					'errors' 	=> '',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);
				
				exit;				
			
			} else {
			
				$return =  array( 

					'error' 	=> true,

					'errors' 	=> __('Whoops, something went wrong!', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);
				
				exit;
				
			}	
		
		} else {
		
			return;
	
		}
	
	}
	
	// Populate Project Box

	add_action( "wp_ajax_nopriv_cqpim_populate_invoice_projects", 

			"cqpim_populate_invoice_projects");

	add_action( "wp_ajax_cqpim_populate_invoice_projects", 

		  	"cqpim_populate_invoice_projects");

	function cqpim_populate_invoice_projects() {
		
		$client_id = isset($_POST['ID']) ? $_POST['ID']: '';
		
		$args = array(
			
			'post_type' => 'cqpim_project',
				
			'posts_per_page' => -1,
				
			'post_status' => 'private',
				
		);
		
		$projects = get_posts($args);
		
		$projects_to_display = '';
		
		
		foreach($projects as $project) {
		
			$project_details = get_post_meta($project->ID, 'project_details', true);
			
			$project_client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
			
			if($project_client_id == $client_id) {
			
				$projects_to_display .= '<option value="' . $project->ID . '">' . $project_details['quote_ref'] . ' - ' . $project->post_title . '</option>';
			
			}
		
		}
		
		if(empty($projects_to_display)) {

			$return =  array( 

				'error' 	=> true,

				'options' 	=> '<option value="">' . __('No projects available', 'cqpim') . '</option>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

		} else {
		
			$return =  array( 

				'error' 	=> false,

				'options' 	=> '<option value="">' . __('Choose a Project', 'cqpim') . '</option>' . $projects_to_display

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);		
		
		}
		
		exit();
			
	
	}
	
	// Process Invoice Emails

	add_action( "wp_ajax_nopriv_cqpim_process_invoice_emails", 

			"cqpim_process_invoice_emails");

	add_action( "wp_ajax_cqpim_process_invoice_emails", 

		  	"cqpim_process_invoice_emails");

	function cqpim_process_invoice_emails($invoice_id, $pm_name = 0, $deposit = 0) {
	
		if(!empty($_POST['invoice_id'])) {
	
			$invoice_id = $_POST['invoice_id'];
			
			$type = $_POST['type'];
			
			$ajax_post = true;
			
		} else {
		
			$type = 'send';
			
		}
			
		$invoice_details = get_post_meta($invoice_id, 'invoice_details', true);
		
		$invoice_project_id = get_post_meta($invoice_id, 'invoice_project', true);
		
		$project_details = get_post_meta($invoice_project_id, 'project_details', true);
		
		$client_contact = get_post_meta($invoice_id, 'client_contact', true);
		
		$client_id = $invoice_details['client_id'];
		
		$client_contacts = get_post_meta($client_id, 'client_contacts', true);
		
		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$terms = isset($client_details['invoice_terms']) ? $client_details['invoice_terms'] : '';
		
		if(empty($terms)) {
		
			$terms = get_option('company_invoice_terms');
		
		}
		
		$client_main_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
		
		if(empty($client_contacts)) {
		
			$client_contacts = array();
		
		}
		
		if(!empty($client_contact)) {
		
			if($client_contact == $client_main_id) {
			
				$to = $client_details['client_email'];
			
			} else {
			
				$to = $client_contacts[$client_contact]['email'];
			
			}
		
		} else {
		
			$to = $client_details['client_email'];
		
		}
		
		if($deposit == true) {
		
			$email_content = get_option('client_deposit_invoice_email');
		
		} else {
		
			$email_content = get_option('client_invoice_email');
			
		}
		
		if($deposit) {
		
			$subject = get_option('client_deposit_invoice_subject');		
		
		} else {
		
			$subject = get_option('client_invoice_subject');
			
		}
		
		if($client_contact == $client_main_id) {
		
			$email_content = str_replace('%%CLIENT_NAME%%', $client_details['client_contact'], $email_content);
			
			$email_content = str_replace('%%CLIENT_EMAIL%%', $client_details['client_email'], $email_content);
		
		} else {
		
			$email_content = str_replace('%%CLIENT_NAME%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['name'] : '', $email_content);
			
			$email_content = str_replace('%%CLIENT_EMAIL%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['email'] : '', $email_content);
		
		}
		
		$subject = cqpim_replacement_patterns($subject, $invoice_id, 'invoice');
		
		$message = cqpim_replacement_patterns($email_content, $invoice_id, 'invoice');
		
		$attachments = array();
		
		$pdf_attach = get_option('client_invoice_email_attach');
		
		if(!empty($pdf_attach)) {
		
			$pdf = cqpim_generate_pdf_invoice($invoice_id);
			
			$attachments[] = $pdf;
		
		}
		
		if( $to && $subject && $message ){
		
			if( cqpim_send_emails( $to, $subject, $message, '', $attachments, 'accounts' ) ) :
			
				if(!empty($pdf)) {
			
					unlink($pdf);
					
				}
			
				if(!empty($ajax_post)) {
			
					$current_user = wp_get_current_user();
					
					$current_user = $current_user->display_name;
				
				} else {
				
					$current_user = __('System', 'cqpim');
					
				}
		
	            $invoice_details = get_post_meta($invoice_id, 'invoice_details', true);

	        	$invoice_details['sent_details'] = array(

						'date' 	=> current_time('timestamp'),

						'by'	=> $current_user,
						
						'to'    => $to,

				);
				
				$invoice_details['sent'] = true;

	        	update_post_meta($invoice_id, 'invoice_details', $invoice_details );
				
				// Schedule reminders
				
				// check that this is not a 'resend' to avoid duplicate reminders
				
				if($type == 'send' && $terms != 1) {
				
					$after_send_reminder = get_option('client_invoice_after_send_remind_days');
					
					$before_due_reminder = get_option('client_invoice_before_terms_remind_days');
					
					$overdue_reminder = get_option('client_invoice_after_terms_remind_days');
					
					// Schedule the after send reminder if set 
					
					if(!empty($after_send_reminder)) {
					
						$invoice_date_string = isset($invoice_details['invoice_date']) ? $invoice_details['invoice_date'] : '';
						
						$send_date = strtotime('+' . $after_send_reminder . ' days', $invoice_date_string);
						
						$args = array(
						
							'invoice_id' => $invoice_id,
							
							'type' => 'reminder',
							
							'which' => 'after_send'
						
						);
						
						wp_schedule_single_event( $send_date, 'cqpim_invoice_reminders', $args );
					
					}
					
					// Schedule the before due reminder if set 
					
					if(!empty($before_due_reminder)) {
					
						$due = isset($invoice_details['terms_over']) ? $invoice_details['terms_over'] : '';

						$send_date = strtotime('-' . $before_due_reminder . ' days', $due);
						
						$now = time();
						
						if($send_date > $now) {

							$args = array(
							
								'invoice_id' => $invoice_id,
								
								'type' => 'reminder',
								
								'which' => 'before_due'
							
							);
							
							wp_schedule_single_event( $send_date, 'cqpim_invoice_reminders', $args );	

						}						
					
					}
					
					// Schedule the overdue reminder if set 
					
					if(!empty($overdue_reminder)) {
					
						$due = isset($invoice_details['terms_over']) ? $invoice_details['terms_over'] : '';

						$send_date = strtotime('+' . $overdue_reminder . ' days', $due);

						$args = array(
						
							'invoice_id' => $invoice_id,
							
							'type' => 'overdue'
						
						);
						
						wp_schedule_single_event( $send_date, 'cqpim_invoice_reminders', $args );					
					
					}
				
				}
				
				

				if(!empty($ajax_post)) {
		
					$return =  array( 

						'error' 	=> false,

						'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Email sent successfully...', 'cqpim') . '</div>'

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	
					
					exit();

				};			

			else :
			
				if(!empty($ajax_post)) {

					$return =  array( 

						'error' 	=> true,

						'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem sending the email, please try again.', 'cqpim') . '</div>'

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);
					
					exit();
				
				};

			endif;	
		
		} else {
			
			if(!empty($ajax_post)) {

				$return =  array( 

					'error' 	=> true,

					'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem sending the email, please try again.', 'cqpim') . '</div>'

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);
				
				exit();
			
			};

		}
		
		if(!empty($ajax_post)) {
		
			exit();
			
		};
	
	}
	
	function cqpim_generate_pdf_invoice($invoice_id) {
	
		$invoice = get_post($invoice_id);
		
		$url = get_the_permalink($invoice_id) . '?pwd=' . md5($invoice->post_password) . '&page=print';

		$url = urldecode($url);

		// For $_POST i.e. forms with fields

		if (count($_POST)>0) {

			$ch = curl_init($url);
			
			curl_setopt($ch, CURLOPT_HEADER, 0);
			
			  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1 );  
			  
			foreach($_POST AS $name=>$post) {	
			
				$formvars = array($name=>$post." \n");	
				
			}	
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $formvars);
			
			$html = curl_exec($ch);	
			
			curl_close($ch);
			
		}
		else if (ini_get('allow_url_fopen')) {

			$html = file_get_contents($url);	
			
		}
		else {

			$ch = curl_init($url);	
			
			curl_setopt($ch, CURLOPT_HEADER, 0);	
			
			curl_setopt ( $ch , CURLOPT_RETURNTRANSFER , 1 );	
			
			$html = curl_exec($ch);	
			
			curl_close($ch);
			
		}

		require_once('assets/mpdf/mpdf.php');
		
		$invoice_template = get_option('cqpim_invoice_template');
		
		if($invoice_template == 1) {
	 
			$mpdf= new mPDF('c'); 
			
		}
		
		if($invoice_template == 2) {
	 
			$mpdf= new mPDF('c', 'A4', 0, '', 0, 0, 0, 0, 0, 0); 
			
		}

		$mpdf->allow_output_buffering = true;

		$mpdf->useSubstitutions=false; 

		$mpdf->SetDisplayMode('fullpage');

		$mpdf->CSSselectMedia='print'; // assuming you used this in the document header

		$mpdf->setBasePath($url);

		$mpdf->WriteHTML($html);

		$filename = realpath(dirname(__FILE__)) . "/temp/invoice_$invoice_id.pdf";

		$mpdf->Output($filename,'F');

		return $filename;
	
	}
	
	// Clear Reminders on Invoice Trash
	
	add_action( 'wp_trash_post', 'cqpim_clear_invoice_reminders_on_trash' );

	function cqpim_clear_invoice_reminders_on_trash( $post_id ){

		global $post;
		
		$id = $post->ID;
		
		if($post->post_type == 'cqpim_invoice') {
			
			$args = array(
				
				'invoice_id' => "$id",
				
				'type' => 'reminder',
				
				'which' => 'before_due'
			
			);
			
			wp_clear_scheduled_hook( 'cqpim_invoice_reminders', $args );
			
			$args = array(
				
				'invoice_id' => "$id",
				
				'type' => 'reminder',
				
				'which' => 'after_send'
			
			);
			
			wp_clear_scheduled_hook( 'cqpim_invoice_reminders', $args );
			
			$args = array(
				
				'invoice_id' => "$id",
				
				'type' => 'overdue'
			
			);
			
			wp_clear_scheduled_hook( 'cqpim_invoice_reminders', $args );
			
		}
	
	}
	
	// MArk Invoice as Paid

	add_action( "wp_ajax_nopriv_cqpim_mark_invoice_paid", 

			"cqpim_mark_invoice_paid");

	add_action( "wp_ajax_cqpim_mark_invoice_paid", 

		  	"cqpim_mark_invoice_paid");

	function cqpim_mark_invoice_paid($invoice_id, $current_user = 0, $amount = 0) {
	
		if(!empty($_POST['invoice_id'])) {
	
			$invoice_id = $_POST['invoice_id'];
			
			$amount = $_POST['amount'];
			
			$notes = $_POST['notes'];
			
			$ajax_post = true;
			
		} 
		
		$amount = preg_replace('/[^0-9\.]/', '', $amount);
		
		if(empty($current_user)) {
		
			$user = wp_get_current_user();				
						
			$current_user = $user->display_name;

		}
		
		$invoice_totals = get_post_meta($invoice_id, 'invoice_totals', true);
		
		$vat_rate = get_option('sales_tax_rate');
		
		$total = isset($invoice_totals['total']) ? $invoice_totals['total'] : 0;
		
		$invoice_payments = get_post_meta($invoice_id, 'invoice_payments', true);
		
		$invoice_details = get_post_meta($invoice_id, 'invoice_details', true);
		
		$invoice_project = isset($invoice_details['project_id']) ? $invoice_details['project_id'] : '';
		
		if(empty($invoice_details['paid'])) {
		
			if(empty($invoice_payments)) {
			
				$invoice_payments = array();
				
			}
			
			if(empty($notes)) {
			
				$notes = '';
			
			}
			
			$invoice_payments[] = array(
			
				'date' => current_time('timestamp'),
				
				'amount' => $amount,
				
				'notes' => $notes,
				
				'by' => $current_user,
			
			);
			
			if(!empty($invoice_project)) {
			
				$invoice_object = get_post($invoice_id);
			
				$project_progress = get_post_meta($invoice_project, 'project_progress', true);
				
				$text = sprintf(__('Invoice payment received: %1$s', 'cqpim'), $invoice_object->post_title);
				
				$project_progress[] = array(
				
					'update' => $text,
					
					'date' => current_time('timestamp'),
					
					'by' => $current_user,
					
				);
				
				update_post_meta($invoice_project, 'project_progress', $project_progress );
				
			}
			
			foreach($invoice_payments as $key => $payment) {
			
				$amount = isset($payment['amount']) ? $payment['amount'] : 0;
				
				$amount = preg_replace("/[^0-9\.]/", '', $amount);
				
				$total = $total - $amount;
				
				$invoice_payments[$key]['amount'] = $amount;
			
			}
			
			update_post_meta($invoice_id, 'invoice_payments', $invoice_payments);
			
			if($total <= 0 && empty($ajax_post)) {
			
				$invoice_details = get_post_meta($invoice_id, 'invoice_details', true);

				$invoice_details['paid_details'] = array(

						'date' 	=> current_time('timestamp'),

						'by'	=> $current_user,

				);
				
				$invoice_details['paid'] = true;

				update_post_meta($invoice_id, 'invoice_details', $invoice_details );
				
				$invoice_project = get_post_meta($invoice_id, 'invoice_project', true);
				
				if($invoice_project) {
				
					$invoice_object = get_post($invoice_id);
				
					$project_progress = get_post_meta($invoice_project, 'project_progress', true);
					
					$text = sprintf(__('Invoice marked as paid: %1$s', 'cqpim'), $invoice_object->post_title);
					
					$project_progress[] = array(
					
						'update' => $text,
						
						'date' => current_time('timestamp'),
						
						'by' => $current_user,
						
					);
					
					update_post_meta($invoice_project, 'project_progress', $project_progress );
					
				}
			
			}
			
			// Send a notification
			
			$email_subject = get_option('client_invoice_receipt_subject');
			
			$email_content = get_option('client_invoice_receipt_email');
			
			$attachments = array();
			
			// set the client email address			

			$invoice_details = get_post_meta($invoice_id, 'invoice_details', true);
			
			$paid = isset($invoice_details['paid']) ? $invoice_details['paid'] : '';
			
			$client_id = $invoice_details['client_id'];
			
			$client_details = get_post_meta($client_id, 'client_details', true);
			
			$client_main_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
			
			$client_contacts = get_post_meta($client_id, 'client_contacts', true);
			
			$client_contact = get_post_meta($invoice_id, 'client_contact', true);
			
			$to = array();
			
			if(empty($client_contacts)) {
			
				$client_contacts = array();
			
			}
			
			if(!empty($client_contact)) {
			
				if($client_contact == $client_main_id) {
				
					$to[] = $client_details['client_email'];
				
				} else {
				
					$to[] = $client_contacts[$client_contact]['email'];
				
				}
			
			} else {
			
				$to[] = $client_details['client_email'];
			
			}
			
			$to[] = get_option('company_sales_email');
			
			$to[] = get_option('company_accounts_email');
			
			$to = array_unique($to);
			
			if($client_contact == $client_main_id) {
			
				$email_content = str_replace('%%CLIENT_NAME%%', $client_details['client_contact'], $email_content);
				
				$email_content = str_replace('%%CLIENT_EMAIL%%', $client_details['client_email'], $email_content);
			
			} else {
			
				$email_content = str_replace('%%CLIENT_NAME%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['name'] : '', $email_content);
				
				$email_content = str_replace('%%CLIENT_EMAIL%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['email'] : '', $email_content);
			
			}		
			
			// Run the replacements patterns
			
			$email_content = cqpim_replacement_patterns($email_content, $invoice_id, 'invoice');
			
			$email_subject = cqpim_replacement_patterns($email_subject, $invoice_id, 'invoice');	

			$amount_tag = '%%AMOUNT%%';
			
			$email_content = str_replace($amount_tag, cqpim_calculate_currency($invoice_id, $amount), $email_content);
			
			foreach($to as $email) {
				
				cqpim_send_emails($email, $email_subject, $email_content, '', $attachments, 'accounts' );
				
			}
			

			if(!empty($ajax_post)) {

				$return =  array( 

					'error' 	=> false,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Sucessfully updated invoice status.', 'cqpim') . '</div>'

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

			};				
			
			if(!empty($ajax_post)) {
			
				exit();
				
			};
		
		}
	
	}
	
	// Process Invoice Reminders

	add_action( "wp_ajax_nopriv_cqpim_send_invoice_reminders", 

			"cqpim_send_invoice_reminders");

	add_action( "wp_ajax_cqpim_send_invoice_reminders", 

		  	"cqpim_send_invoice_reminders");
			
	add_action( "cqpim_invoice_reminders", 

		  	"cqpim_send_invoice_reminders");

	function cqpim_send_invoice_reminders($invoice_id, $type = NULL) {
	
		if(isset($_POST['invoice_id'])) {
		
			$invoice_id = $_POST['invoice_id'];
			
			$type = $_POST['type'];
			
			$ajax_call = true;
	
		}
		
		$invoice_object = get_post($invoice_id);
		
		if(empty($invoice_object) || !empty($invoice_object) && $invoice_object->post_status == 'trash') {
			
			die;
			
		}
		
		// Get email body & subjects
		
		if($type == 'overdue') {
		
			$email_content = get_option('client_invoice_overdue_email');
			
			$email_subject = get_option('client_invoice_overdue_subject');
		
		} else {
		
			$email_content = get_option('client_invoice_reminder_email');
			
			$email_subject = get_option('client_invoice_reminder_subject');	
		
		}
		
		$priority = get_option('client_invoice_high_priority');
		
		// set priority based on options
		
		if($priority == 1) {
		
			add_filter('phpmailer_init','update_priority_mailer');
		
		}
		
		$attachments = array();
		
		// set the client email address
		
		$invoice_details = get_post_meta($invoice_id, 'invoice_details', true);
		
		$paid = isset($invoice_details['paid']) ? $invoice_details['paid'] : '';
		
		$client_id = $invoice_details['client_id'];
		
		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$client_main_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
		
		$client_contacts = get_post_meta($client_id, 'client_contacts', true);
		
		$client_contact = get_post_meta($invoice_id, 'client_contact', true);
		
		if(empty($client_contacts)) {
		
			$client_contacts = array();
		
		}
		
		if(!empty($client_contact)) {
		
			if($client_contact == $client_main_id) {
			
				$to = $client_details['client_email'];
			
			} else {
			
				$to = $client_contacts[$client_contact]['email'];
			
			}
		
		} else {
		
			$to = $client_details['client_email'];
		
		}
		
		if($client_contact == $client_main_id) {
		
			$email_content = str_replace('%%CLIENT_NAME%%', $client_details['client_contact'], $email_content);
			
			$email_content = str_replace('%%CLIENT_EMAIL%%', $client_details['client_email'], $email_content);
		
		} else {
		
			$email_content = str_replace('%%CLIENT_NAME%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['name'] : '', $email_content);
			
			$email_content = str_replace('%%CLIENT_EMAIL%%', isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['email'] : '', $email_content);
		
		}		
		
		// Run the replacements patterns
		
		$email_content = cqpim_replacement_patterns($email_content, $invoice_id, 'invoice');
		
		$email_subject = cqpim_replacement_patterns($email_subject, $invoice_id, 'invoice');
		
		if(empty($paid)) {
		
			if($to && $email_content && $email_subject) {
			
				if( cqpim_send_emails( $to, $email_subject, $email_content, '', $attachments, 'accounts' ) ) {			
				
					if(!empty($ajax_call)) {
					
						$return =  array( 

							'error' 	=> false,

							'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('The Email was sent successfully.', 'cqpim') . '</div>'

						);
							
						header('Content-type: application/json');
							
						echo json_encode($return);				
					
					}			
				
				} else {
				
					if(!empty($ajax_call)) {
					
						$return =  array( 

							'error' 	=> true,

							'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem sending the email. Please try again.', 'cqpim') . '</div>'

						);
							
						header('Content-type: application/json');
							
						echo json_encode($return);					
					
					}			
				
				}
			
			} else {
			
				if(!empty($ajax_call)) {
				
					$return =  array( 

						'error' 	=> true,

						'errors' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('There was a problem sending the email. Some data is missing. Please check and try again.', 'cqpim') . '</div>'

					);
						
					header('Content-type: application/json');
						
					echo json_encode($return);			
				
				}
			
			}
		
		} else {
		
			if(!empty($ajax_call)) {
				
				$return =  array( 

					'error' 	=> true,

					'errors' 	=> __('ALREADY PAID!', 'cqpim')

				);
					
				header('Content-type: application/json');
						
				echo json_encode($return);			
				
			}		
		
		}
		
		if(!empty($ajax_call)) {
			
			exit();
			
		}
	
	}
	
	function update_priority_mailer($mailer){
	
		$mailer->Priority = 1;
		
		return $mailer;
		
	}

	add_action( "wp_ajax_cqpim_delete_payment", 

		  	"cqpim_delete_payment");

	function cqpim_delete_payment() {
	
		$invoice_id = isset($_POST['invoice_id']) ? $_POST['invoice_id'] : '';
		
		$payment_id = isset($_POST['payment_id']) ? $_POST['payment_id'] : '';
		
		$payments = get_post_meta($invoice_id, 'invoice_payments', true);
		
		unset($payments[$payment_id]);
		
		update_post_meta($invoice_id, 'invoice_payments', $payments);
		
		$return =  array( 

			'error' 	=> false

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit;
	
	}

	add_action( "wp_ajax_cqpim_edit_payment", 

		  	"cqpim_edit_payment");

	function cqpim_edit_payment() {
	
		$invoice_id = isset($_POST['invoice_id']) ? $_POST['invoice_id'] : '';
		
		$payment_id = isset($_POST['payment_id']) ? $_POST['payment_id'] : '';
		
		$amount = isset($_POST['amount']) ? $_POST['amount'] : '';
		
		$amount = preg_replace("/[^0-9\.]/","", $amount);
		
		$notes = isset($_POST['notes']) ? $_POST['notes'] : '';
		
		$payments = get_post_meta($invoice_id, 'invoice_payments', true);
		
		$payments[$payment_id]['amount'] = $amount;
		
		$payments[$payment_id]['notes'] = $notes;
		
		update_post_meta($invoice_id, 'invoice_payments', $payments);
		
		$return =  array( 

			'error' 	=> false,
			
			'errors' => 'none'

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit;
	
	}
	
	add_action( 'current_screen', 'cqpim_change_invoice_status' );
	
	function cqpim_change_invoice_status() {
	
		$screen = get_current_screen();
	
		$action = isset($_GET['action']) ? $_GET['action'] : '';
	
		if($screen->post_type == 'cqpim_invoice' && $action == 'edit') {
		
			$post_id = $_GET['post'];
			
			$post = get_post($post_id);
			
			$line_items = get_post_meta($post->ID, 'line_items', true);
	
			$tax_app = get_post_meta($post->ID, 'tax_applicable', true);
			
			$invoice_totals = get_post_meta($post->ID, 'invoice_totals', true);
			
			if($tax_app == 1) {
			
				$total = isset($invoice_totals['total']) ? $invoice_totals['total'] : 0;		
			
			} else {
			
				$total = isset($invoice_totals['sub']) ? $invoice_totals['sub'] : 0;		
			
			}
			
			$invoice_payments = get_post_meta($post->ID, 'invoice_payments', true);
			
			$invoice_details = get_post_meta($post->ID, 'invoice_details', true);
			
			if(empty($invoice_payments)) {
			
				$invoice_payments = array();
			
			}
			
			foreach($invoice_payments as $key => $payment) {
			
				$amount = isset($payment['amount']) ? $payment['amount'] : 0;
				
				$amount = preg_replace('/[^a-zA-Z0-9\.]/', '', $amount);
				
				$total = $total - $amount;
			
			}
			
			if($total <= 0 && empty($invoice_details['paid_details']['date']) && !empty($line_items)) {
			
				$user = wp_get_current_user();				
								
				$current_user = $user->display_name;		
			
				$invoice_details = get_post_meta($post->ID, 'invoice_details', true);

				$invoice_details['paid_details'] = array(

						'date' 	=> current_time('timestamp'),

						'by'	=> $current_user,

				);
				
				$invoice_details['paid'] = true;

				update_post_meta($post->ID, 'invoice_details', $invoice_details );
				
				$invoice_project = get_post_meta($post->ID, 'invoice_project', true);
				
				if($invoice_project) {
				
					$invoice_object = get_post($post->ID);
				
					$project_progress = get_post_meta($invoice_project, 'project_progress', true);
					
					$text = sprintf(__('Invoice marked as paid: %1$s', 'cqpim'), $invoice_object->post_title);
					
					$project_progress[] = array(
					
						'update' => $text,
						
						'date' => current_time('timestamp'),
						
						'by' => $current_user,
						
					);
					
					update_post_meta($invoice_project, 'project_progress', $project_progress );
					
				}
			
			}
			
			if($total > 0) {
			
				$invoice_details = get_post_meta($post->ID, 'invoice_details', true);

				unset($invoice_details['paid_details']);
				
				unset($invoice_details['paid']);

				update_post_meta($post->ID, 'invoice_details', $invoice_details );			
			
			}
		
		}
	
	}
	
	// Add message to project

	add_action( "wp_ajax_nopriv_cqpim_add_message_to_project", 

			"cqpim_add_message_to_project");

	add_action( "wp_ajax_cqpim_add_message_to_project", 

		  	"cqpim_add_message_to_project");	
	
	function cqpim_add_message_to_project() {
	
		$project_id = isset($_POST['project_id']) ? $_POST['project_id'] : '';
		
		$visibility = isset($_POST['visibility']) ? $_POST['visibility'] : '';
		
		$message = isset($_POST['message']) ? $_POST['message'] : '';
		
		$message = make_clickable($message);
		
		$who = isset($_POST['who']) ? $_POST['who'] : '';
		
		$date = current_time('timestamp');
		
		$current_user = wp_get_current_user();
		
		if($project_id && $visibility && $message && $who) {
		
			$project_messages = get_post_meta($project_id, 'project_messages', true);
			
			if(!$project_messages) {
			
				$project_messages = array();
				
			}
			
			$project_messages[] = array(
			
				'visibility' => $visibility,
				
				'date' => $date,
				
				'message' => $message,
				
				'by' => $current_user->display_name,
				
				'author' => $current_user->ID
				
			);
			
			update_post_meta($project_id, 'project_messages', $project_messages);
			
			$project_progress = get_post_meta($project_id, 'project_progress', true);
			
			$text = sprintf(__('%1$s just sent a message', 'cqpim'), $current_user->display_name );
					
			$project_progress[] = array(
					
				'update' => $text,
						
				'date' => current_time('timestamp'),
				
				'by' => $current_user->display_name
						
			);
					
			update_post_meta($project_id, 'project_progress', $project_progress );
			
			if($visibility == 'all') {
			
				if($who == 'client') {
				
					$addresses_to_send = array();
					
					$subject = get_option('company_message_subject');
					
					$content = get_option('company_message_email');
					
					$content = str_replace('%%MESSAGE%%', $message, $content);
					
					$project_contributors = get_post_meta($project_id, 'project_contributors', true);
					
					foreach($project_contributors as $contributor) {
					
						$team_details = get_post_meta($contributor['team_id'], 'team_details', true);
						
						$team_email = isset($team_details['team_email']) ? $team_details['team_email'] : '';

						$addresses_to_send[] = $team_email;
					
					}
					
					$addresses_to_send[] = get_option('company_sales_email');
					
					cqpim_project_message_mailer($addresses_to_send, $subject, $content, $project_id);
					
				
				} elseif($who == 'admin') {
				
					$send_to_team = isset($_POST['send_to_team']) ? $_POST['send_to_team'] : '';
		
					$send_to_client = isset($_POST['send_to_client']) ? $_POST['send_to_client'] : '';
				
					$addresses_to_send = array();
					
					$project_details = get_post_meta($project_id, 'project_details', true);
					
					$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
					
					$client_details = get_post_meta($client_id, 'client_details', true);
					
					$client_contacts = get_post_meta($client_id, 'client_contacts', true);
					
					$client_contact = isset($project_details['client_contact']) ? $project_details['client_contact'] : '';
					
					
					if(!empty($client_contact)) {
					
						if($client_details['user_id'] == $client_contact) {
							
							$client_email = isset($client_details['client_email']) ? $client_details['client_email'] : '';
						
						} else {
							
							$client_email = isset($client_contacts[$client_contact]['email']) ? $client_contacts[$client_contact]['email'] : '';		
						
						}
					
					} else {
						
						$client_email = isset($client_details['client_email']) ? $client_details['client_email'] : '';		
					
					}					
			
					$addresses_to_send[] = get_option('company_sales_email');
					
					if($send_to_client == 1) {
					
						$addresses_to_send[] = $client_email;
						
						$subject = get_option('client_message_subject');
					
						$content = get_option('client_message_email');
						
						$content = str_replace('%%MESSAGE%%', $message, $content);
						
						cqpim_project_message_mailer($addresses_to_send, $subject, $content, $project_id);
					
					}
					
					$addresses_to_send = array();
					
					if($send_to_team == 1) {
					
						$project_contributors = get_post_meta($project_id, 'project_contributors', true);
						
						foreach($project_contributors as $contributor) {
						
							$team_details = get_post_meta($contributor['team_id'], 'team_details', true);
							
							$team_email = isset($team_details['team_email']) ? $team_details['team_email'] : '';

							$addresses_to_send[] = $team_email;
						
						}
						
						$subject = get_option('company_message_subject');
						
						$content = get_option('company_message_email');
						
						$content = str_replace('%%MESSAGE%%', $message, $content);
						
						cqpim_project_message_mailer($addresses_to_send, $subject, $content, $project_id);
					
					}
				
				}
			
			}

			$return =  array( 

				'error' 	=> false,

				'errors' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Message added successfully', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);				
		
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('You have not entered a message!', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);			
		
		}
		
		exit();
	
	}
	
	function cqpim_project_message_mailer($addresses, $subject, $content, $project_id) {
	
		$subject = cqpim_replacement_patterns($subject, $project_id, 'project');
		
		$content = cqpim_replacement_patterns($content, $project_id, 'project');
		
		$attachments = array();
		
		foreach($addresses as $to) {
		
			cqpim_send_emails($to, $subject, $content, '', $attachments, 'sales');
		
		}
	}
	
	// Delete project message

	add_action( "wp_ajax_nopriv_cqpim_delete_project_message", 

			"cqpim_delete_project_message");

	add_action( "wp_ajax_cqpim_delete_project_message", 

		  	"cqpim_delete_project_message");	
	
	function cqpim_delete_project_message() {
	
		$project_id = isset($_POST['project_id']) ? $_POST['project_id'] : '';
		
		$key = isset($_POST['key']) ? $_POST['key'] : '';
		
		$project_messages = get_post_meta($project_id, 'project_messages', true);
		
		$project_messages = array_reverse($project_messages);
		
		unset($project_messages[$key]);
		
		$project_messages = array_filter($project_messages);
		
		$project_messages = array_reverse($project_messages);
		
		update_post_meta($project_id, 'project_messages', $project_messages);
				
		exit();
	
	}
	
	// Client / Team Password Reset
	
	add_action( "wp_ajax_nopriv_cqpim_reset_password", 

			"cqpim_reset_password");

	add_action( "wp_ajax_cqpim_reset_password", 

		  	"cqpim_reset_password");
	
	function cqpim_reset_password() {
	
		$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
		
		$entity_id = isset($_POST['entity_id']) ? $_POST['entity_id'] : '';
		
		$new_password = isset($_POST['new_password']) ? $_POST['new_password'] : '';
		
		$confirm_password = isset($_POST['confirm_password']) ? $_POST['confirm_password'] : '';
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		$send = isset($_POST['send']) ? $_POST['send'] : '';
		
		wp_set_password($confirm_password, $user_id);
		
		if($send == 1) {
		
			if($type == 'client') {
			
				$email_subject = get_option('password_reset_subject');
				
				$email_content = get_option('password_reset_content');
				
				$client_details = get_post_meta($entity_id, 'client_details', true);
				
				$to = $client_details['client_email'];
				
				$email_subject = cqpim_replacement_patterns($email_subject, $entity_id, 'client');
				
				$email_content = cqpim_replacement_patterns($email_content, $entity_id, 'client');
			
			} elseif($type == 'team') {
			
				$email_subject = get_option('team_reset_subject');
				
				$email_content = get_option('team_reset_email');

				$team_details = get_post_meta($entity_id, 'team_details', true);				

				$to = $team_details['team_email'];	

				$email_subject = cqpim_replacement_patterns($email_subject, $entity_id, 'team');
				
				$email_content = cqpim_replacement_patterns($email_content, $entity_id, 'team');				
			
			}
			
			$email_content = str_replace('%%NEW_PASSWORD%%', $new_password, $email_content);
			
			$attachments = array();
			
			if(cqpim_send_emails($to, $email_subject, $email_content, '', $attachments, 'sales')) {

				$return =  array( 

					'error' 	=> false,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Password was successfully reset. An email has been sent.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	
				
				exit();

			} else {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-warning alert-display">' . __('Password was successfully reset. There was an error sending the email. Check you\'ve completed the email subject and content fields in the settings.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();
			
			}
		
		}
		
		$return =  array( 

			'error' 	=> false,

			'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Password was successfully reset.', 'cqpim') . '</div>',

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	
		
		exit();
	
	}
	
	/******** TEAMS *************/
	
	// Add Team to Project
	
	add_action( "wp_ajax_nopriv_cqpim_add_team_to_project", 

			"cqpim_add_team_to_project");

	add_action( "wp_ajax_cqpim_add_team_to_project", 

		  	"cqpim_add_team_to_project");
	
	function cqpim_add_team_to_project() {
	
		$project_id = isset($_POST['project_id']) ? $_POST['project_id'] : '';
		
		$team_id = isset($_POST['team_id']) ? $_POST['team_id'] : '';
		
		$pm = isset($_POST['pm']) ? $_POST['pm'] : '';
		
		$project_contributors = get_post_meta($project_id, 'project_contributors', true);
		
		if(!empty($team_id)) {
		
			if(!$project_contributors) {
			
				$project_contributors = array();
				
			}			
			
			if(!empty($pm) && $pm == 1) {

				$project_contributors[] = array(
				
					'team_id' => $team_id,
					
					'pm' => true
					
				);
			
			} else {			
			
				$project_contributors[] = array(
				
					'team_id' => $team_id,
					
				);
			
			}
			
			update_post_meta($project_id, 'project_contributors', $project_contributors);
			
			$team_details = get_post_meta($team_id, 'team_details', true);
					
			$current_user = wp_get_current_user();
					
			$current_user = $current_user->display_name;
				
			$project_progress = get_post_meta($project_id, 'project_progress', true);
			
			$text = sprintf(__('Team Member Added: %1$s', 'cqpim'), $team_details['team_name']);
							
			$project_progress[] = array(
							
				'update' => $text,
								
				'date' => current_time('timestamp'),
						
				'by' => $current_user
								
			);
							
			update_post_meta($project_id, 'project_progress', $project_progress );
			
			$team_details = get_post_meta($team_id, 'team_details', true);
			
			$to = $team_details['team_email'];
			
			$email_subject = get_option('team_project_subject');
			
			$email_content = get_option('team_project_email');
			
			$email_subject = cqpim_replacement_patterns($email_subject, $team_id, 'team');
			
			$email_subject = cqpim_replacement_patterns($email_subject, $project_id, 'project');
			
			$email_content = cqpim_replacement_patterns($email_content, $team_id, 'team');
			
			$email_content = cqpim_replacement_patterns($email_content, $project_id, 'project');
			
			$attachments = array();
			
			if(cqpim_send_emails($to, $email_subject, $email_content, '', $attachments, 'sales')) {
			
				$return =  array( 

					'error' 	=> false,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('The team member was successfully added. An email has been sent.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();				
			
			} else {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-warning alert-display">' . __('The team member was successfully added. The email failed to send. Check you have completed the email subject and content fields in the plugin settings.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();				
			
			}
			

		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was an issue. The Team Member ID is missing. Please try again.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();		
		
		}
		
		exit();
	
	}
	
	// Remove Team Member
	
	add_action( "wp_ajax_nopriv_cqpim_remove_team_member", 

			"cqpim_remove_team_member");

	add_action( "wp_ajax_cqpim_remove_team_member", 

		  	"cqpim_remove_team_member");
	
	function cqpim_remove_team_member() {
	
		$key = isset($_POST['key']) ? $_POST['key'] : '';
		
		$project_id = isset($_POST['project_id']) ? $_POST['project_id'] : '';
		
		$team = isset($_POST['team']) ? $_POST['team'] : '';
		
		$project_contributors = get_post_meta($project_id, 'project_contributors', true);
		
		unset($project_contributors[$key]);
		
		$project_contributors = array_filter($project_contributors);
		
		update_post_meta($project_id, 'project_contributors', $project_contributors);
		
		// Remove ownership of tasks
		
		$args = array(
		
			'post_type' => 'cqpim_tasks',
			
			'posts_per_page' => -1,
			
			'meta_key' => 'owner',
			
			'meta_value' => $team
		
		);
		
		$tasks = get_posts($args);
		
		foreach($tasks as $task) {
		
			delete_post_meta($task->ID, 'owner');
		
		}
		
		$return =  array( 

			'error' 	=> false,

			'message' 	=> '',

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit();	
	
	}
	
	// Remove time entry
	
	add_action( "wp_ajax_nopriv_cqpim_remove_time_entry", 

			"cqpim_remove_time_entry");

	add_action( "wp_ajax_cqpim_remove_time_entry", 

		  	"cqpim_remove_time_entry");
	
	function cqpim_remove_time_entry() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		$key = isset($_POST['key']) ? $_POST['key'] : '';		
		
		if(!$task_id) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> __('There is some missing data. Delete unsuccessful.', 'cqpim'),

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		} else {
		
			$task_time = get_post_meta($task_id, 'task_time_spent', true);
			
			unset($task_time[$key]);
			
			$task_time = array_filter($task_time);
			
			update_post_meta($task_id, 'task_time_spent', $task_time);
			
			$return =  array( 

				'error' 	=> false,

				'message' 	=> '',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();	
		
		}
		
		exit();
	
	}
	
	
	/********** SUPPORT TICKETS ************/
	
	// Delete support message

	add_action( "wp_ajax_nopriv_cqpim_delete_support_message", 

			"cqpim_delete_support_message");

	add_action( "wp_ajax_cqpim_delete_support_message", 

		  	"cqpim_delete_support_message");	
	
	function cqpim_delete_support_message() {
	
		$project_id = isset($_POST['project_id']) ? $_POST['project_id'] : '';
		
		$key = isset($_POST['key']) ? $_POST['key'] : '';
		
		$project_messages = get_post_meta($project_id, 'ticket_updates', true);
		
		$project_messages = array_reverse($project_messages);
		
		unset($project_messages[$key]);
		
		$project_messages = array_filter($project_messages);
		
		$project_messages = array_reverse($project_messages);
		
		update_post_meta($project_id, 'ticket_updates', $project_messages);
				
		exit();
	
	}
	

	
	// Client create support ticket
	
	add_action( "wp_ajax_nopriv_cqpim_client_raise_support_ticket", 

			"cqpim_client_raise_support_ticket");

	add_action( "wp_ajax_cqpim_client_raise_support_ticket", 

		  	"cqpim_client_raise_support_ticket");
	
	function cqpim_client_raise_support_ticket($data, $files = array(), $attachments = array()) {
	
		$data = isset($_POST['data']) ? $_POST['data'] : '';
		
		$custom_fields = get_option('cqpim_custom_fields_support');	
		
		$custom_fields = str_replace('\"', '"', $custom_fields);
		
		$custom_fields = json_decode($custom_fields);
	
		$custom = isset($data['custom']) ? $data['custom'] : array();
		
		foreach($custom_fields as $custom_field) {
			
			if(empty($custom[$custom_field->name]) && !empty($custom_field->required)) {
				
				$return =  array( 

					'error' 	=> true,

					'title' 	=> __('Required Fields Missing', 'cqpim'),
					
					'message' 	=> __('Please complete all required fields.', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();						
				
			}
			
		}
	
		$title = isset($data['ticket_title']) ? $data['ticket_title'] : '';
		
		$priority = isset($data['ticket_priority_new']) ? $data['ticket_priority_new'] : 'normal';
		
		$details = isset($data['ticket_update_new']) ? $data['ticket_update_new'] : '';
		
		$details = make_clickable($details);
		
		include_once(ABSPATH.'wp-admin/includes/plugin.php');
	
		if(is_plugin_active('cqpim-envato/cqpim-envato.php')) {	
		
			$item = isset($data['ticket_item']) ? $data['ticket_item'] : '';
			
			$reject = isset($data['reject_reason']) ? $data['reject_reason'] : '';
			
			if($reject == 'nobuy' || $reject == 'inobuy') {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> __('You have not purchased the item that you have selected, please try again.', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();				
			
			}
			
			$exp_allow = get_option('cqpim_allow_unsupported');
			
			if(empty($exp_allow) && $reject == 'exp') {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> __('You cannot raise a ticket because your support entitlement for the selected item has expired.', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();				
			
			}
		
		} else {
		
			$item = 1;
			
			$reject = 0;
			
		}
		
		if(empty($title) || empty($details) || empty($item)) {
		
			if(is_plugin_active('cqpim-envato/cqpim-envato.php')) {	
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> __('You must enter a title, message and choose an item from the list.', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();	
			
			} else {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> __('You must enter a title and message.', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();	
				
			}		
		
		} else {
		
			$user = wp_get_current_user();
			
			$args = array(
			
				'post_type' => 'cqpim_client',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private',
				
			);
			
			$clients = get_posts($args);
			
			foreach($clients as $client) {
			
				$client_details = get_post_meta($client->ID, 'client_details', true);
				
				$client_user_id = $client_details['user_id'];
				
				if($user->ID === $client_user_id) {
				
					$client_object_id = $client->ID;
				
				}
			
			}
			
			if(empty($client_object_id)) {
			
				foreach($clients as $client) {
				
					$client_ids = get_post_meta($client->ID, 'client_ids', true);
					
					if(in_array($user->ID, $client_ids)) {
					
						$client_object_id = $client->ID;
						
						$client_type = 'contact';
					
					}
				
				} 			
			
			}
		
			$client_details = get_post_meta($client_object_id, 'client_details', true);
		
			$new_ticket = array(
			
				'post_type' => 'cqpim_support',
				
				'post_status' => 'private',
				
				'post_content' => '',
				
				'post_title' => $title,
				
				'post_author' => $user->ID,				
			
			);
			
			$ticket_pid = wp_insert_post( $new_ticket, true );
			
			if( ! is_wp_error( $ticket_pid ) ){
			
				$ticket_updated = array(
					
					'ID' => $ticket_pid,
						
					'post_name' => $ticket_pid,
						
				);						
			
				wp_update_post( $ticket_updated );
		
				$custom = isset($data['custom']) ? $data['custom'] : array();
				
				update_post_meta($ticket_pid, 'custom_fields', $custom);
				
				update_post_meta($ticket_pid, 'ticket_client', $client_object_id);
				
				$currency = get_option('currency_symbol');
				
				$currency_code = get_option('currency_code');
				
				$currency_position = get_option('currency_symbol_position');
				
				$currency_space = get_option('currency_symbol_space'); 
				
				$client_currency = get_post_meta($client_object_id, 'currency_symbol', true);
					
				$client_currency_code = get_post_meta($client_object_id, 'currency_code', true);
					
				$client_currency_space = get_post_meta($client_object_id, 'currency_space', true);		

				$client_currency_position = get_post_meta($client_object_id, 'currency_position', true);
				
				if(!empty($client_currency)) {
				
					update_post_meta($ticket_pid, 'currency_symbol', $client_currency);
				
				} else {
				
					update_post_meta($ticket_pid, 'currency_symbol', $currency);
				
				}
				
				if(!empty($client_currency_code)) {
				
					update_post_meta($ticket_pid, 'currency_code', $client_currency_code);
				
				} else {
				
					update_post_meta($ticket_pid, 'currency_code', $currency_code);
				
				}
			
				if(!empty($client_currency_space)) {
				
					update_post_meta($ticket_pid, 'currency_space', $client_currency_space);
				
				} else {
				
					update_post_meta($ticket_pid, 'currency_space', $currency_space);
				
				}
			
				if(!empty($client_currency_position)) {
				
					update_post_meta($ticket_pid, 'currency_position', $client_currency_position);
				
				} else {
				
					update_post_meta($ticket_pid, 'currency_position', $currency_position);
				
				}
			
				$ticket_status = 'open';
				
				if(is_plugin_active('cqpim-envato/cqpim-envato.php')) {	
				
					update_post_meta($ticket_pid, 'envato_item', $item);
				
				}
				
				update_post_meta($ticket_pid, 'ticket_status', $ticket_status);
				
				$attachments = isset($_SESSION['upload_ids']) ? $_SESSION['upload_ids'] : array();
				
				foreach($attachments as $attachment) {
				
					$attachment_updated = array(
					
						'ID' => $attachment,
						
						'post_parent' => $ticket_pid
						
					);
					
					wp_update_post($attachment_updated);
				
				}
				
				$ticket_changes = isset($_SESSION['ticket_changes']) ? $_SESSION['ticket_changes'] : array();
				
				$ticket_updates = array();
				
				$ticket_updates[] = array(
				
					'details' => $details,
					
					'time' => current_time('timestamp'),
					
					'name' => $user->display_name,
					
					'email' => $user->user_email,
					
					'user' => $client_object_id,
					
					'type' => 'client',
					
					'changes' => $ticket_changes
					
				);
				
				update_post_meta($ticket_pid, 'ticket_updates', $ticket_updates);
				
				update_post_meta($ticket_pid, 'ticket_priority', $priority);
				
				update_post_meta($ticket_pid, 'ticket_client', $client_object_id);
				
				if(!empty($client_details['ticket_assignee'])) {
				
					update_post_meta($ticket_pid, 'ticket_owner', $client_details['ticket_assignee']);
					
				}
				
				$last_updated = current_time('timestamp');
				
				update_post_meta($ticket_pid, 'last_updated', $last_updated);
				
				$sender_name = get_option('company_name');
				
				$sender_name = $sender_name;
			
				$sender_email = get_option('company_support_email');
				
				$to = array();
				
				$to[] = $sender_email;
				
				if(!empty($client_details['ticket_assignee'])) {
				
					$assignee_details = get_post_meta($client_details['ticket_assignee'], 'team_details', true);
					
					if(!empty($assignee_details['team_email'])) {
					
						$to[] = $assignee_details['team_email'];
					
					}
					
				}
				
				if($priority == 'high' || $priority == 'immediate') {
				
					add_filter('phpmailer_init','update_priority_mailer');
				
				}

				$email_subject = get_option('client_create_ticket_subject');
				
				$email_content = get_option('client_create_ticket_email');
				
				$email_subject = str_replace('%%PIPING_ID%%', '[' . get_option('cqpim_string_prefix') . ':' . $ticket_pid . ']', $email_subject);
				
				$email_subject = str_replace('%%CLIENT_NAME%%', $user->display_name, $email_subject);
				
				$email_content = str_replace('%%CLIENT_NAME%%', $user->display_name, $email_content);				
				
				$email_subject = cqpim_replacement_patterns($email_subject, $client_object_id, 'client');
				
				$email_subject = cqpim_replacement_patterns($email_subject, $ticket_pid, 'ticket');
				
				$email_content = cqpim_replacement_patterns($email_content, $client_object_id, 'client');
				
				$email_content = cqpim_replacement_patterns($email_content, $ticket_pid, 'ticket');
				
				foreach($to as $recip) {
				
					cqpim_send_emails($recip, $email_subject, $email_content, '', $attachments, 'support');

				}
				
				$dashboard = get_option('cqpim_client_page');
				
				$permalink = get_the_permalink($dashboard);
				
				$support_slug = get_option('cqpim_support_slug');
				
				$return =  array( 

					'error' 	=> false,

					'message' 	=> site_url() . '/' . $support_slug . '/' . $ticket_pid,

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();	
			
			} else {
		
				$return =  array( 

					'error' 	=> true,

					'message' 	=> __('The ticket could not be created', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();	
				
			}
		
		}
	
	}
	
	// Switch Resolved/Open Tickets
	
	add_action( "wp_ajax_nopriv_cqpim_switch_resolved_tickets", 

			"cqpim_switch_resolved_tickets");

	add_action( "wp_ajax_cqpim_switch_resolved_tickets", 

		  	"cqpim_switch_resolved_tickets");
	
	function cqpim_switch_resolved_tickets() {
	
		$status = isset($_SESSION['ticket_status']) ? $_SESSION['ticket_status'] : '';
		
		if($status == 'resolved') {
		
			$_SESSION['ticket_status'] = array('open', 'hold');
			
		} else {
		
			$_SESSION['ticket_status'] = 'resolved';
			
		}
		
		exit();
	
	}
	
	// Update support ticket
	
	add_action( "wp_ajax_nopriv_cqpim_update_support_ticket", 

			"cqpim_update_support_ticket");

	add_action( "wp_ajax_cqpim_update_support_ticket", 

		  	"cqpim_update_support_ticket");
	
	function cqpim_update_support_ticket($ticket_id, $data = array(), $files = array(), $type = NULL, $user = NULL, $attachments = array()) {
	
		if(empty($ticket_id)) {
		
			$data = isset($_POST['data']) ? $_POST['data'] : '';
			
			$ticket_id = isset($_POST['post_id']) ? $_POST['post_id'] : '';
			
			$type = isset($_POST['type']) ? $_POST['type'] : '';
			
			$ajax_call = true;
			
			$custom_fields = get_option('cqpim_custom_fields_support');	
			
			$custom_fields = str_replace('\"', '"', $custom_fields);
			
			$custom_fields = json_decode($custom_fields);
		
			$custom = isset($data['custom']) ? $data['custom'] : array();
			
			foreach($custom_fields as $custom_field) {
				
				if(empty($custom[$custom_field->name]) && !empty($custom_field->required)) {
					
					$return =  array( 

						'error' 	=> true,

						'title' 	=> __('Required Fields Missing', 'cqpim'),
						
						'message' 	=> __('Please complete all required fields.', 'cqpim'),

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	

					exit();						
					
				}
				
			}
			
		}
	
		$ticket_changes = isset($_SESSION['ticket_changes']) ? $_SESSION['ticket_changes'] : array();
		
		if(empty($ticket_changes)) {
		
			$ticket_changes = isset($data['ticket_changes']) ? $data['ticket_changes'] : array();
		
		}
		
		$status = isset($data['ticket_status_new']) ? $data['ticket_status_new'] : '';
		
		$priority = isset($data['ticket_priority_new']) ? $data['ticket_priority_new'] : '';
		
		$update = isset($data['ticket_update_new']) ? $data['ticket_update_new'] : '';
		
		$update = make_clickable($update);
		
		$owner = isset($data['ticket_owner']) ? $data['ticket_owner'] : '';
		
		$watchers = isset($data['task_watchers']) ? $data['task_watchers'] : '';
		
		$files = isset($files) ? $files : array();
		
		$client_id = get_post_meta($ticket_id, 'ticket_client', true);
	
		if($type == 'admin') {
		
			update_post_meta($ticket_id, 'unread', 1);
			
			if(empty($user)) {
	
				$user = wp_get_current_user();
			
			}
			
			$args = array(
			
				'post_type' => 'cqpim_teams',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$members = get_posts($args);
			
			foreach($members as $member) {
			
				$team_details = get_post_meta($member->ID, 'team_details', true);
				
				if($team_details['user_id'] == $user->ID) {
				
					$assigned = $member->ID;
				
				}
			
			}
			
			update_post_meta($ticket_id, 'ticket_watchers', $watchers);
			
			$ticket_owner = get_post_meta($ticket_id, 'ticket_owner', true);				
			
			if($ticket_owner != $owner) {
			
				$owner_details = get_post_meta($owner, 'team_details', true);
				
				$owner_cap = $owner_details['team_name'];
			
				$ticket_changes[] = sprintf(__('Assignee changed to %1$s', 'cqpim'), $owner_cap);					
				
				update_post_meta($ticket_id, 'ticket_owner', $owner);
			
			}
		
		} 
		
		if($type == 'client') {
		
			if(empty($user)) {
	
				$user = wp_get_current_user();
			
			}
			
			$args = array(
			
				'post_type' => 'cqpim_client',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$members = get_posts($args);
			
			foreach($members as $member) {
			
				$team_details = get_post_meta($member->ID, 'client_details', true);
				
				if($team_details['user_id'] == $user->ID) {
				
					$assigned = $member->ID;
				
				}
			
			}

			if(empty($assigned)) {
			
				foreach($members as $member) {
				
					$team_ids = get_post_meta($member->ID, 'client_ids', true);
					
					if(!is_array($team_ids)) {
					
						$team_ids = array($team_ids);
					
					}
					
					if(in_array($user->ID, $team_ids)) {
					
						$assigned = $member->ID;
						
						$client_type = 'contact';
					
					}
				
				} 			
			
			}			
		
		}
		
		if(!empty($ajax_call)) {
		
			$custom = isset($data['custom']) ? $data['custom'] : array();
			
			update_post_meta($ticket_id, 'custom_fields', $custom);
		
		}
		
		$ticket_status = get_post_meta($ticket_id, 'ticket_status', true);
		
		if($ticket_status != $status) {
		
			$status_cap = __($status, 'cqpim');
		
			$status_cap = ucfirst($status);
		
			$ticket_changes[] = sprintf(__('Ticket Status changed to %1$s', 'cqpim'), $status_cap);			
			
			update_post_meta($ticket_id, 'ticket_status', $status);
		
		}
		
		$ticket_priority = get_post_meta($ticket_id, 'ticket_priority', true);
		
		if($ticket_priority != $priority) {
		
			$priority_cap = __($priority, 'cqpim');
		
			$priority_cap = ucfirst($priority);
			
			$ticket_changes[] = sprintf(__('Ticket Priority changed to %1$s', 'cqpim'), $priority_cap);
			
			update_post_meta($ticket_id, 'ticket_priority', $priority);
		
		}

		$last_updated = current_time('timestamp');
		
		update_post_meta($ticket_id, 'last_updated', $last_updated);
		
		if(!empty($ticket_changes) || !empty($update)) {
		
			$ticket_updates = get_post_meta($ticket_id, 'ticket_updates', true);
			
			$ticket_updates[] = array(
			
				'details' => $update,
				
				'time' => current_time('timestamp'),
				
				'user' => $assigned,
				
				'name' => $user->display_name,
				
				'email' => $user->user_email,
				
				'type' => $type,
				
				'changes' => $ticket_changes
				
			);			
			
			update_post_meta($ticket_id, 'ticket_updates', $ticket_updates);
		
		}
		
		if(!empty($_SESSION['upload_ids'])) {
		
			$attachments = isset($_SESSION['upload_ids']) ? $_SESSION['upload_ids'] : array();
			
			foreach($attachments as $attachment) {
			
				$attachment_updated = array(
				
					'ID' => $attachment,
					
					'post_parent' => $ticket_id
					
				);
				
				wp_update_post($attachment_updated);
			
			}
		
		}
		
		if($priority == 'high' || $priority == 'immediate') {
		
			add_filter('phpmailer_init','update_priority_mailer');
		
		}
		
		$email_subject = get_option('client_update_ticket_subject');
			
		$email_content = get_option('client_update_ticket_email');
			
		$email_subject = cqpim_replacement_patterns($email_subject, $ticket_id, 'ticket');
			
		$email_content = cqpim_replacement_patterns($email_content, $ticket_id, 'ticket');
		
		$addresses_to_send = array();
		
		// Get client email
		
		$ticket = get_post($ticket_id);
		
		$client = $ticket->post_author;
		
		$client = get_user_by('id', $client);		
		
		if(empty($client_email)) {
			
			// NEED TO GET PREFERENCES AND DECIDE WHETHER TO INCLUDE
			
			$args = array(
			
				'post_type' => 'cqpim_client',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$members = get_posts($args);
			
			foreach($members as $member) {
			
				$team_details = get_post_meta($member->ID, 'client_details', true);
				
				if($team_details['user_id'] == $client->ID) {
				
					$notifications = get_post_meta($member->ID, 'client_notifications', true);
				
				}
			
			}

			if(empty($assigned)) {
			
				foreach($members as $member) {
				
					$client_contacts = get_post_meta($member->ID, 'client_contacts', true);
					
					if(empty($client_contacts)) {
						
						$client_contacts = array();
						
					}
					
					foreach($client_contacts as $contact) {
						
						if($contact['user_id'] == $client->ID) {
							
							$notifications = isset($contact['notifications']) ? $contact['notifications'] : array();
							
						}
						
					}
				
				} 			
			
			}
		
			$no_tickets = isset($notifications['no_tickets']) ? $notifications['no_tickets']: 0;
			
			$no_tickets_comment = isset($notifications['no_tickets_comment']) ? $notifications['no_tickets_comment']: 0;			
			
			$client_email = $client->user_email;
			
			if(!empty($no_tickets)) {
				
				$client_email = '';
				
			}
			
			if(!empty($no_tickets_comment) && empty($update)) {
				
				$client_email = '';
				
			}
		
		}
		
		if(!empty($client_email)) {
		
			$addresses_to_send[] = array(
			
				'email' => $client->user_email,
				
				'name' => $client->display_name
				
			);
		
		}
		
		// Get owner email
		
		$ticket_owner = get_post_meta($ticket_id, 'ticket_owner', true);
		
		$owner_details = get_post_meta($ticket_owner, 'team_details', true);
		
		$owner_email = isset($owner_details['team_email']) ? $owner_details['team_email'] : '';
		
		$addresses_to_send[] = array(
		
			'email' => $owner_email,
			
			'name' => $owner_details['team_name']
			
		);
		
		// Get Watcher emails_to_send
		
		$watchers_send = get_post_meta($ticket_id, 'ticket_watchers', true);
		
		if(empty($watchers_send)) {
		
			$watchers_send = array();
			
		}
		
		foreach($watchers_send as $watcher) {
		
			$watcher_details = get_post_meta($watcher, 'team_details', true);
			
			$watcher_email = isset($watcher_details['team_email']) ? $watcher_details['team_email'] : '';
			
			$addresses_to_send[] = array(
			
				'email' => $watcher_email,
				
				'name' => $watcher_details['team_name']
				
			);
		
		}
		
		foreach($addresses_to_send as $key => $address) {
		
			if($address['email'] == $user->user_email) {
			
				unset($addresses_to_send[$key]);
			
			}
		
		}
			
		$current_name = $user->display_name;			
		
		if(!empty($update) || $ticket_owner != $owner) {
		
			$i = 0;
			
			foreach($addresses_to_send as $address) {
			
				$name = $address['name'];
				
				${"email_subject_" . $i} = str_replace('%%UPDATER_NAME%%', $current_name, $email_subject);
				
				${"email_subject_" . $i} = str_replace('%%NAME%%', $name, ${"email_subject_" . $i});
				
				${"email_subject_" . $i} = str_replace('%%PIPING_ID%%', '[' . get_option('cqpim_string_prefix') . ':' . $ticket_id . ']', ${"email_subject_" . $i});
			
				${"email_content_" . $i} = str_replace('%%NAME%%', $name, $email_content);
				
				${"email_content_" . $i} = str_replace('%%UPDATER_NAME%%', $current_name, ${"email_content_" . $i});
			
				cqpim_send_emails($address['email'], ${"email_subject_" . $i}, ${"email_content_" . $i}, '', $attachments, 'support');
				
				$i++;
			
			}
		
		}
		
		if(!empty($ajax_call)) {
		
			$return =  array( 

				'error' 	=> false,

				'title' 	=> __('Ticket Updated', 'cqpim'),
				
				'message' 	=> __('The ticket was successfully updated.', 'cqpim'),

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();				
		
		} else {
		
			return;
			
		}
	
	}
	
	
	
	/*********** FORMS ****************/
	
	// Frontend Register Submission
	
	add_action( "wp_ajax_nopriv_cqpim_frontend_register_submission", 

			"cqpim_frontend_register_submission");

	add_action( "wp_ajax_cqpim_frontend_register_submission", 

		  	"cqpim_frontend_register_submission");
	
	function cqpim_frontend_register_submission() {
	
		$data = isset($_POST) ? $_POST : '';
		
		if(empty($data)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#d9534f">' . __('There is missing data, please try again filling in every field.', 'cqpim') . '</span>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		} else {
		
			unset($data['action']);
		
			$name = isset($data['name']) ? $data['name'] : '';
			
			unset($data['name']);
			
			$company = isset($data['company']) ? $data['company'] : '';
			
			unset($data['company']);
			
			$address = isset($data['address']) ? $data['address'] : '';
			
			unset($data['address']);
			
			$postcode = isset($data['postcode']) ? $data['postcode'] : '';
			
			unset($data['postcode']);
			
			$telephone = isset($data['telephone']) ? $data['telephone'] : '';
			
			unset($data['telephone']);
			
			$email = isset($data['email']) ? $data['email'] : '';
			
			unset($data['email']);
			
			if ( username_exists( $email ) || email_exists( $email ) ) {
				
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#d9534f">' . __('The email address entered is already in our system, please try again with a different email address or contact us.', 'cqpim') . '</span>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();				
			
			} else {
			
				$new_client = array(
				
					'post_type' => 'cqpim_client',
					
					'post_status' => 'private',
					
					'post_content' => '',
					
					'post_title' => $company,
				
				);
				
				$client_pid = wp_insert_post( $new_client, true );
				
				if( ! is_wp_error( $client_pid ) ){
				
					$client_updated = array(
						
						'ID' => $client_pid,
							
						'post_name' => $client_pid,
							
					);						
				
					wp_update_post( $client_updated );
				
					$client_details = array(
						
						'client_ref' => $client_pid,
						
						'client_company' => $company,

						'client_contact' => $name,
						
						'client_address' => $address,
						
						'client_postcode' => $postcode,
						
						'client_telephone' => $telephone,
						
						'client_email' => $email,
					
					);
					
					$passw = cqpim_random_string(10);
					
					$login = $email;
					
					$user_id = wp_create_user( $login, $passw, $email );
					
					$user = new WP_User( $user_id );
					
					$user->set_role( 'cqpim_client' );
					
					$client_details['user_id'] = $user_id;
					
					$client_ids = array();
					
					$client_ids[] = $user_id;
					
					update_post_meta($client_pid, 'client_details', $client_details);
					
					update_post_meta($client_pid, 'client_ids', $client_ids);
					
					$user_data = array(
					
						'ID' => $user_id,
						
						'display_name' => $name,
						
						'first_name' => $name,
					
					);
					
					wp_update_user($user_data);	

					$form_auto_welcome = get_option('form_reg_auto_welcome');
					
					if($form_auto_welcome == 1) {
					
						send_cqpim_welcome_email($client_pid, $passw);
					
					}	

					$return =  array( 

						'error' 	=> false,

						'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#8ec165">' . __('Account created, please check your email for your password.', 'cqpim') . '</span>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	

					exit();						
				
				} else {
				
					$return =  array( 

						'error' 	=> true,

						'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#d9534f">' . __('Unable to create client entry, please try again or contact us.', 'cqpim') . '</span>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	

					exit();	
					
				}				

			}
		
		}
		
		exit();	
	
	}
	
	// Frontend Quote Submission
	
	add_action( "wp_ajax_nopriv_cqpim_frontend_quote_submission", 

			"cqpim_frontend_quote_submission");

	add_action( "wp_ajax_cqpim_frontend_quote_submission", 

		  	"cqpim_frontend_quote_submission");
	
	function cqpim_frontend_quote_submission() {
	
		$data = isset($_POST) ? $_POST : '';
		
		if(empty($data)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#d9534f">' . __('There is missing data, please try again filling in every field.', 'cqpim') . '</span>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		} else {
		
			unset($data['action']);
		
			$name = isset($data['name']) ? $data['name'] : '';
			
			unset($data['name']);
			
			$company = isset($data['company']) ? $data['company'] : '';
			
			unset($data['company']);
			
			$address = isset($data['address']) ? $data['address'] : '';
			
			unset($data['address']);
			
			$postcode = isset($data['postcode']) ? $data['postcode'] : '';
			
			unset($data['postcode']);
			
			$telephone = isset($data['telephone']) ? $data['telephone'] : '';
			
			unset($data['telephone']);
			
			$email = isset($data['email']) ? $data['email'] : '';
			
			unset($data['email']);
			
			if ( username_exists( $email ) || email_exists( $email ) ) {
				
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#d9534f">' . __('The email address entered is already in our system, please try again with a different email address or contact us.', 'cqpim') . '</span>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();				
			
			} else {
			
				$new_client = array(
				
					'post_type' => 'cqpim_client',
					
					'post_status' => 'private',
					
					'post_content' => '',
					
					'post_title' => $company,
				
				);
				
				$client_pid = wp_insert_post( $new_client, true );
				
				if( ! is_wp_error( $client_pid ) ){
				
					$client_updated = array(
						
						'ID' => $client_pid,
							
						'post_name' => $client_pid,
							
					);						
				
					wp_update_post( $client_updated );
				
					$client_details = array(
						
						'client_ref' => $client_pid,
						
						'client_company' => $company,

						'client_contact' => $name,
						
						'client_address' => $address,
						
						'client_postcode' => $postcode,
						
						'client_telephone' => $telephone,
						
						'client_email' => $email,
					
					);
					
					$passw = cqpim_random_string(10);
					
					$login = $email;
					
					$user_id = wp_create_user( $login, $passw, $email );
					
					$user = new WP_User( $user_id );
					
					$user->set_role( 'cqpim_client' );
					
					$client_details['user_id'] = $user_id;
					
					$client_ids = array();
					
					$client_ids[] = $user_id;
					
					update_post_meta($client_pid, 'client_details', $client_details);
					
					update_post_meta($client_pid, 'client_ids', $client_ids);
					
					$user_data = array(
					
						'ID' => $user_id,
						
						'display_name' => $name,
						
						'first_name' => $name,
					
					);
					
					wp_update_user($user_data);	

					$form_auto_welcome = get_option('form_auto_welcome');
					
					if($form_auto_welcome == 1) {
					
						send_cqpim_welcome_email($client_pid, $passw);
					
					}	
					
					$new_quote = array(
					
						'post_type' => 'cqpim_quote',
						
						'post_status' => 'private',
						
						'post_content' => '',
						
						'post_title' => '',
					
					);
					
					$quote_pid = wp_insert_post( $new_quote, true );
					
					if( ! is_wp_error( $quote_pid ) ){					
					
						$title = $company . ' - ' . __('Quote', 'cqpim') . ': ' . $quote_pid;
						
						$quote_updated = array(
							
							'ID' => $quote_pid,
								
							'post_title' => $title,
								
							'post_name' => $quote_pid,
								
						);						
					
						wp_update_post( $quote_updated );
						
						$uploaded_files = array();
						
						$summary = '';
						
						foreach($data as $key => $field) {
						
							if(is_array($field)) {
							
								$field = implode(', ', $field);
							
							}
						
							$title = str_replace('_', ' ', $key);
							
							$title = ucwords($title);
							
							if(strpos($title, 'Cqpimuploader') !== false) {
								
								$file_object = get_post($field);
								
								$title = str_replace('Cqpimuploader ', '', $title);
								
								$summary .= '<p><strong>' . $title . ': </strong> ' . $file_object->post_title . '</p>';
								
								$attachment_updated = array(
								
									'ID' => $field,
									
									'post_parent' => $quote_pid,
									
								);
								
								wp_update_post($attachment_updated);
								
								update_post_meta($field, 'cqpim', true);
								
							} else {
							
								$summary .= '<p><strong>' . $title . ': </strong> ' . $field . '</p>';
							
							}
						
						}
						
						$header = get_option( 'quote_header' );
						
						$header = str_replace('%%CLIENT_NAME%%', $name, $header);
						
						$footer = get_option( 'quote_footer' );
						
						$footer = str_replace('%%CURRENT_USER%%', '', $footer);
						
						$currency = get_option('currency_symbol');
						
						$currency_code = get_option('currency_code');
						
						$currency_position = get_option('currency_symbol_position');
						
						$currency_space = get_option('currency_symbol_space'); 
						
						update_post_meta($quote_pid, 'currency_symbol', $currency);
						
						update_post_meta($quote_pid, 'currency_code', $currency_code);
						
						update_post_meta($quote_pid, 'currency_position', $currency_position);
						
						update_post_meta($quote_pid, 'currency_space', $currency_space);
						
						$quote_details = array(
						
							'quote_type' => 'quote',
							
							'quote_ref' => $quote_pid,
							
							'client_id' => $client_pid,
							
							'quote_summary' => $summary,
							
							'quote_header' => $header,
							
							'quote_footer' => $footer,
							
							'client_contact' => $user_id
						
						);
						
						update_post_meta($quote_pid, 'quote_details', $quote_details);

						$to = get_option('company_sales_email');
						
						$attachments = array();
						
						$subject = get_option('new_quote_subject');
						
						$content = get_option('new_quote_email');
						
						$name_tag = '%%NAME%%';
						
						$link_tag = '%%QUOTE_URL%%';
						
						$company_tag = '%%COMPANY_NAME%%';
						
						$quote_link = admin_url() . 'post.php?post=' . $quote_pid . '&action=edit';
						
						$subject = str_replace($name_tag, $name, $subject);
								
						$content = str_replace($name_tag, $name, $content);
						
						$content = str_replace($link_tag, $quote_link, $content);
						
						$content = str_replace($company_tag, $sender_name, $content);
						
						cqpim_send_emails( $to, $subject, $content, '', $attachments, 'sales' );
				
						$return =  array( 

							'error' 	=> false,

							'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#8ec165">' . __('Quote request submitted, we\'ll get back to you soon!', 'cqpim') . '</span>',

						);
						
						header('Content-type: application/json');
						
						echo json_encode($return);	

						exit();						
					
					} else {
					
						$return =  array( 

							'error' 	=> true,

							'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#d9534f">' . __('Unable to create quote, please try again or contact us.', 'cqpim') . '</span>',

						);
						
						header('Content-type: application/json');
						
						echo json_encode($return);	

						exit();						
					
					}

					
				
				} else {
				
					$return =  array( 

						'error' 	=> true,

						'message' 	=> '<span style="display:block; width:96%; padding:2%; color:#fff; background:#d9534f">' . __('Unable to create client entry, please try again or contact us.', 'cqpim') . '</span>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	

					exit();	
					
				}				

			}
		
		}
		
		exit();	
	
	}
	
	// Frontend Quote Submission
	
	add_action( "wp_ajax_nopriv_cqpim_backend_quote_submission", 

			"cqpim_backend_quote_submission");

	add_action( "wp_ajax_cqpim_backend_quote_submission", 

		  	"cqpim_backend_quote_submission");
	
	function cqpim_backend_quote_submission() {
	
		$data = isset($_POST) ? $_POST : '';
		
		if(empty($data)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There is missing data, please try again filling in every field.', 'cqpim') . '</div>',

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		} else {
		
			unset($data['action']);
		
			$client = isset($data['client']) ? $data['client'] : '';
			
			unset($data['client']);
			
			$client_details = get_post_meta($client, 'client_details', true);
			
			$name = isset($client_details['client_contact']) ? $client_details['client_contact'] : '';
			
			$company = isset($client_details['client_company']) ? $client_details['client_company'] : '';
			
			if ( empty($client) ) {
				
				$return =  array( 

					'error' 	=> true,

					'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Missing Client ID, please try again.', 'cqpim') . '</div>',

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();				
			
			} else {	
					
				$new_quote = array(
				
					'post_type' => 'cqpim_quote',
					
					'post_status' => 'private',
					
					'post_content' => '',
					
					'post_title' => '',
				
				);
				
				$quote_pid = wp_insert_post( $new_quote, true );
				
				if( ! is_wp_error( $quote_pid ) ){					
				
					$title = $company . ' - ' . __('Quote', 'cqpim') . ': ' . $quote_pid;
					
					$quote_updated = array(
						
						'ID' => $quote_pid,
							
						'post_title' => $title,
							
						'post_name' => $quote_pid,
							
					);						
				
					wp_update_post( $quote_updated );
					
					$summary = '';
					
					foreach($data as $key => $field) {
					
						if(is_array($field)) {
						
							$field = implode(', ', $field);
						
						}
					
						$title = str_replace('_', ' ', $key);
						
						$title = ucwords($title);
						
						if(strpos($title, 'Cqpimuploader') !== false) {
							
							$file_object = get_post($field);
							
							$title = str_replace('Cqpimuploader ', '', $title);
							
							$summary .= '<p><strong>' . $title . ': </strong> ' . $file_object->post_title . '</p>';
							
							$attachment_updated = array(
							
								'ID' => $field,
								
								'post_parent' => $quote_pid,
								
							);
							
							wp_update_post($attachment_updated);
							
							update_post_meta($field, 'cqpim', true);
							
						} else {
						
							$summary .= '<p><strong>' . $title . ': </strong> ' . $field . '</p>';
						
						}
					
					}
					
					$user = wp_get_current_user();
					
					$header = get_option( 'quote_header' );
					
					$header = str_replace('%%CLIENT_NAME%%', $user->display_name, $header);
					
					$footer = get_option( 'quote_footer' );
					
					$footer = str_replace('%%CURRENT_USER%%', '', $footer);
					
					$currency = get_option('currency_symbol');
					
					$currency_code = get_option('currency_code');
					
					$currency_position = get_option('currency_symbol_position');
					
					$currency_space = get_option('currency_symbol_space'); 
					
					update_post_meta($quote_pid, 'currency_symbol', $currency);
					
					update_post_meta($quote_pid, 'currency_code', $currency_code);
					
					update_post_meta($quote_pid, 'currency_position', $currency_position);
					
					update_post_meta($quote_pid, 'currency_space', $currency_space);
					
					$quote_details = array(
					
						'quote_type' => 'quote',
						
						'quote_ref' => $quote_pid,
						
						'client_id' => $client,
						
						'quote_summary' => $summary,
						
						'quote_header' => $header,
						
						'quote_footer' => $footer,
						
						'client_contact' => $user->ID
					
					);
					
					update_post_meta($quote_pid, 'quote_details', $quote_details);

					$to = get_option('company_sales_email');
					
					$attachments = array();
					
					$subject = get_option('new_quote_subject');
					
					$content = get_option('new_quote_email');
					
					$name_tag = '%%NAME%%';
					
					$link_tag = '%%QUOTE_URL%%';
					
					$company_tag = '%%COMPANY_NAME%%';
					
					$quote_link = admin_url() . 'post.php?post=' . $quote_pid . '&action=edit';
					
					$subject = str_replace($name_tag, $user->display_name, $subject);
							
					$content = str_replace($name_tag, $user->display_name, $content);
					
					$content = str_replace($link_tag, $quote_link, $content);
					
					$content = str_replace($company_tag, $sender_name, $content);
					
					cqpim_send_emails( $to, $subject, $content, '', $attachments, 'sales' );
			
					$return =  array( 

						'error' 	=> false,

						'message' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('Quote request submitted, we\'ll get back to you soon!', 'cqpim') . '</div>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	

					exit();						
				
				} else {
				
					$return =  array( 

						'error' 	=> true,

						'message' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Unable to create quote, please try again or contact us.', 'cqpim') . '</div>',

					);
					
					header('Content-type: application/json');
					
					echo json_encode($return);	

					exit();						
				
				}				

			}
		
		}
		
		exit();	
	
	}
	
	
	
	/********** TASKS ***************/
	
	add_action( "wp_ajax_nopriv_cqpim_create_task", 

			"cqpim_create_task");

	add_action( "wp_ajax_cqpim_create_task", 

		  	"cqpim_create_task");
	
	function cqpim_create_task() {
	
		if(isset($_POST['task_title'])) {
			
			$task_weight = isset($_POST['task_weight']) ? $_POST['task_weight'] : '';
		
			$task_title = isset($_POST['task_title']) ? $_POST['task_title'] : '';
			
			$task_milestone_id = isset($_POST['task_milestone_id']) ? $_POST['task_milestone_id'] : '';
			
			$task_project_id = isset($_POST['task_project_id']) ? $_POST['task_project_id'] : '';
			
			$task_deadline = isset($_POST['task_finish']) ? $_POST['task_finish'] : '';
			
			$task_time = isset($_POST['task_time']) ? $_POST['task_time'] : '';
			
			if(!empty($task_deadline)) {
			
				$task_deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $task_deadline)->getTimestamp();
			
			}
			
			$start = isset($_POST['start']) ? $_POST['start'] : '';
			
			if(!empty($start)) {
			
				$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
			
			}
			
			$description = isset($_POST['description']) ? $_POST['description'] : '';
			
			$owner = isset($_POST['owner']) ? $_POST['owner'] : '';
			
			$type = isset($_POST['type']) ? $_POST['type'] : '';
			
			$ppid = isset($_POST['ppid']) ? $_POST['ppid'] : '';
			
			$new_task = array(
			
				'post_type' => 'cqpim_tasks',
				
				'post_status' => 'publish',
				
				'post_content' => '',
				
				'post_title' => $task_title,
				
				'post_password' => cqpim_random_string(10),
			
			);
			
			$task_pid = wp_insert_post( $new_task, true );
			
			if( ! is_wp_error( $task_pid ) ){
			
				$task_updated = array(
					
					'ID' => $task_pid,
						
					'post_name' => $task_pid,
						
				);						
			
				wp_update_post( $task_updated );
			
				// Add Project Relationship
				
				update_post_meta($task_pid, 'project_id', $task_project_id);
				
				if(!empty($task_project_id)) {
				
					update_post_meta($task_pid, 'active', true);
					
					update_post_meta($task_pid, 'published', true);
				
				}
				
				// Add milestone relationship
				
				update_post_meta($task_pid, 'milestone_id', $task_milestone_id);
				
				// Add Owner
				
				if(!empty($owner)) {
				
					update_post_meta($task_pid, 'owner', $owner);
				
				} else {
				
					$owner = wp_get_current_user();
					
					$args = array(
					
						'post_type' => 'cqpim_teams',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'team_details', true);
						
						if($team_details['user_id'] == $owner->ID) {
						
							$assigned = $member->ID;
						
						}
					
					}
					
					update_post_meta($task_pid, 'owner', $assigned);
					
				}
				
				// Add details
				
				$task_details = array(
				
					'weight' => $task_weight,
				
					'deadline' => $task_deadline,
					
					'status' => 'pending',
					
					'task_start' => $start,
					
					'task_description' => $description,
					
					'task_pc' => 0,
					
					'task_priority' => 'normal',
					
					'task_est_time' => $task_time,
				
				);
				
				update_post_meta($task_pid, 'task_details', $task_details);
				

				
				if($type == 'project') {
							
					$current_user = wp_get_current_user();
							
					$current_user = $current_user->display_name;
						
					$project_progress = get_post_meta($ppid, 'project_progress', true);
					
					$text = sprintf(__('Task Created: %1$s', 'cqpim'), $task_title);
									
					$project_progress[] = array(
									
						'update' => $text,
										
						'date' => current_time('timestamp'),
								
						'by' => $current_user
										
					);
									
					update_post_meta($ppid, 'project_progress', $project_progress );
						
				}
				
				$return =  array( 

					'error' 	=> false,

					'errors' 	=> __('The Task was successfully created.', 'cqpim')

				);
					
				header('Content-type: application/json');
						
				echo json_encode($return);
			
			} else {
			
				$return =  array( 

					'error' 	=> true,

					'errors' 	=> __('The Task could not be created at this time, please try again.', 'cqpim')

				);
					
				header('Content-type: application/json');
						
				echo json_encode($return);				
			
			}
		
		}
		
		exit();
		
	}
	
	add_action( "wp_ajax_nopriv_cqpim_create_subtask", 

			"cqpim_create_subtask");

	add_action( "wp_ajax_cqpim_create_subtask", 

		  	"cqpim_create_subtask");
	
	function cqpim_create_subtask() {
	
		if(isset($_POST['task_title'])) {
			
			$task_weight = isset($_POST['task_weight']) ? $_POST['task_weight'] : '';
		
			$task_title = isset($_POST['task_title']) ? $_POST['task_title'] : '';
			
			$task_milestone_id = isset($_POST['task_milestone_id']) ? $_POST['task_milestone_id'] : '';
			
			$task_parent_id = isset($_POST['parent']) ? $_POST['parent'] : '';
			
			$task_project_id = isset($_POST['task_project_id']) ? $_POST['task_project_id'] : '';
			
			$task_deadline = isset($_POST['task_finish']) ? $_POST['task_finish'] : '';
			
			$task_time = isset($_POST['task_time']) ? $_POST['task_time'] : '';
			
			if(!empty($task_deadline)) {
			
				$task_deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $task_deadline)->getTimestamp();
			
			}
			
			$start = isset($_POST['start']) ? $_POST['start'] : '';
			
			if(!empty($start)) {
			
				$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
			
			}
			
			$description = isset($_POST['description']) ? $_POST['description'] : '';
			
			$owner = isset($_POST['owner']) ? $_POST['owner'] : '';
			
			$type = isset($_POST['type']) ? $_POST['type'] : '';
			
			$ppid = isset($_POST['ppid']) ? $_POST['ppid'] : '';
			
			$new_task = array(
			
				'post_type' => 'cqpim_tasks',
				
				'post_status' => 'publish',
				
				'post_content' => '',
				
				'post_title' => $task_title,
				
				'post_parent' => $task_parent_id,
				
				'post_password' => cqpim_random_string(10),
			
			);
			
			$task_pid = wp_insert_post( $new_task, true );
			
			if( ! is_wp_error( $task_pid ) ){
			
				$task_updated = array(
					
					'ID' => $task_pid,
						
					'post_name' => $task_pid,
						
				);						
			
				wp_update_post( $task_updated );
			
				// Add Project Relationship
				
				update_post_meta($task_pid, 'project_id', $task_project_id);
				
				if(!empty($task_project_id)) {
				
					update_post_meta($task_pid, 'active', true);
					
					update_post_meta($task_pid, 'published', true);
				
				}
				
				// Add milestone relationship
				
				update_post_meta($task_pid, 'milestone_id', $task_milestone_id);
				
				// Add Owner
				
				if(!empty($owner)) {
				
					update_post_meta($task_pid, 'owner', $owner);
				
				} else {
				
					$owner = wp_get_current_user();
					
					$args = array(
					
						'post_type' => 'cqpim_teams',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'team_details', true);
						
						if($team_details['user_id'] == $owner->ID) {
						
							$assigned = $member->ID;
						
						}
					
					}
					
					update_post_meta($task_pid, 'owner', $assigned);
					
				}
				
				// Add details
				
				$task_details = array(
				
					'weight' => $task_weight,
				
					'deadline' => $task_deadline,
					
					'status' => 'pending',
					
					'task_start' => $start,
					
					'task_description' => $description,
					
					'task_pc' => 0,
					
					'task_priority' => 'normal',
					
					'task_est_time' => $task_time,
				
				);
				
				update_post_meta($task_pid, 'task_details', $task_details);
				

				
				if($type == 'project') {
							
					$current_user = wp_get_current_user();
							
					$current_user = $current_user->display_name;
						
					$project_progress = get_post_meta($ppid, 'project_progress', true);
					
					$text = sprintf(__('Task Created: %1$s', 'cqpim'), $task_title);
									
					$project_progress[] = array(
									
						'update' => $text,
										
						'date' => current_time('timestamp'),
								
						'by' => $current_user
										
					);
									
					update_post_meta($ppid, 'project_progress', $project_progress );
						
				}
				
				$return =  array( 

					'error' 	=> false,

					'errors' 	=> __('The Task was successfully created.', 'cqpim')

				);
					
				header('Content-type: application/json');
						
				echo json_encode($return);
			
			} else {
			
				$return =  array( 

					'error' 	=> true,

					'errors' 	=> __('The Task could not be created at this time, please try again.', 'cqpim')

				);
					
				header('Content-type: application/json');
						
				echo json_encode($return);				
			
			}
		
		}
		
		exit();
		
	}
	
	// Update Task AJAX Callback
	
	add_action( "wp_ajax_nopriv_cqpim_update_task", 

			"cqpim_update_task");

	add_action( "wp_ajax_cqpim_update_task", 

		  	"cqpim_update_task");
	
	function cqpim_update_task() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		$ppid = isset($_POST['ppid']) ? $_POST['ppid'] : '';
		
		$status = isset($_POST['status']) ? $_POST['status'] : '';
		
		$time = isset($_POST['time']) ? $_POST['time'] : '';
		
		$owner = isset($_POST['owner']) ? $_POST['owner'] : '';
		
		$changes = '';
		
		if(!$task_id) {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> 'Task ID cannot be found.'

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
		
		} else {
			
			$old_owner = get_post_meta($task_id, 'owner', true);
			
			$old_team_details = get_post_meta($old_owner, 'team_details', true);
			
			$new_team_details = get_post_meta($owner, 'team_details', true);			
			
			update_post_meta($task_id, 'owner', $owner);
			
			if($owner != $old_owner) {
				
				if(empty($old_owner)) {
				
					$changes .= sprintf(__('Assignee changed to %1$s', 'cqpim'), $new_team_details['team_name']) . "\r\n";
				
				} elseif(empty($owner)){
				
					$changes .= sprintf(__('Assignee changed %1$s to unassigned', 'cqpim'), $old_team_details['team_name']) . "\r\n";
				
				} else {
			
					$changes .= sprintf(__('Assignee changed from %1$s to %2$s', 'cqpim'), $old_team_details['team_name'], $new_team_details['team_name']) . "\r\n";
					
				}					
			
			}
		
			$title = isset($_POST['title']) ? $_POST['title'] : '';
			
			$deadline = isset($_POST['deadline']) ? $_POST['deadline'] : '';
			
			if(!empty($deadline)) {
			
				$deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $deadline)->getTimestamp();
			
			}
			
			$start = isset($_POST['start']) ? $_POST['start'] : '';
			
			if(!empty($start)) {
			
				$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
			
			}
				
			$description = isset($_POST['description']) ? $_POST['description'] : '';
			
			$watchers = isset($_POST['watchers']) ? $_POST['watchers'] : '';
			
			$updated_task = array(
			
				'ID'           => $task_id,
				
				'post_title'   => $title,
				
			);

			$updated = wp_update_post( $updated_task );
			
			if($updated != false) {
			
				$task_details = get_post_meta($task_id, 'task_details', true);
				
				$task_details['deadline'] = $deadline;
				
				$task_details['status'] = $status;
				
				$task_details['task_start'] = $start;
				
				$task_details['task_description'] = $description;
				
				update_post_meta($task_id, 'task_details', $task_details);
				
				if($time) {
				
					$time_spent = get_post_meta($task_id, 'task_time_spent', true);
					
					$user = wp_get_current_user();
					
					$args = array(
					
						'post_type' => 'cqpim_teams',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private'
					
					);
					
					$members = get_posts($args);
					
					foreach($members as $member) {
					
						$team_details = get_post_meta($member->ID, 'team_details', true);
						
						if($team_details['user_id'] == $user->ID) {
						
							$assigned = $member->ID;
						
						}
					
					}
					
					if(!$time_spent) {
					
						$time_spent = array();
						
					}
					
					$time_spent[] = array(
					
						'team' => $user->display_name,
						
						'team_id' => $assigned,
						
						'time' => $time,
					
					);
					
					update_post_meta($task_id, 'task_time_spent', $time_spent);
				
				}
				
				if($type == 'project') {
				
					if($status == 'complete') {
					
						$current_user = wp_get_current_user();
						
						$current_user = $current_user->display_name;
					
						$project_progress = get_post_meta($ppid, 'project_progress', true);
						
						$text = sprintf(__('Task Completed: %1$s', 'cqpim'), $title );
								
						$project_progress[] = array(
								
							'update' => $text,
									
							'date' => current_time('timestamp'),
							
							'by' => $current_user
									
						);
								
						update_post_meta($ppid, 'project_progress', $project_progress );									
					
					} else {
				
						$current_user = wp_get_current_user();
						
						$current_user = $current_user->display_name;
					
						$project_progress = get_post_meta($ppid, 'project_progress', true);
						
						$text = sprintf(__('Task Updated: %1$s', 'cqpim'), $title );
								
						$project_progress[] = array(
								
							'update' => $text,
									
							'date' => current_time('timestamp'),
							
							'by' => $current_user
									
						);
								
						update_post_meta($ppid, 'project_progress', $project_progress );
					
					}
				
				}
				
				$return =  array( 

					'error' 	=> false,

					'messages' 	=> __('The task was successfully updated', 'cqpim')

				);
						
				header('Content-type: application/json');
							
				echo json_encode($return);
			
			} else {
			
				$return =  array( 

					'error' 	=> true,

					'errors' 	=> __('Task could not be updated at this time, please try again.', 'cqpim')

				);
						
				header('Content-type: application/json');
							
				echo json_encode($return);
			
			}
		
		}
		
		exit();
	
	}
		
	
	// Delete Task AJAX Callback
	
	add_action( "wp_ajax_nopriv_cqpim_delete_task", 

			"cqpim_delete_task");

	add_action( "wp_ajax_cqpim_delete_task", 

		  	"cqpim_delete_task");
	
	function cqpim_delete_task() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		$ppid = isset($_POST['ppid']) ? $_POST['ppid'] : '';
		
		if($type == 'project') {
				
			$current_user = wp_get_current_user();
					
			$current_user = $current_user->display_name;
				
			$project_progress = get_post_meta($ppid, 'project_progress', true);
			
			$task_object = get_post($task_id);
			
			$task_title = $task_object->post_title;
			
			$text = sprintf(__('Task Deleted: %1$s', 'cqpim'), $task_title );
							
			$project_progress[] = array(
							
				'update' => $text,
								
				'date' => current_time('timestamp'),
						
				'by' => $current_user
								
			);
							
			update_post_meta($ppid, 'project_progress', $project_progress );
				
		}
		
		$args = array(
		
			'post_type' => 'cqpim_tasks',
			
			'posts_per_page' => -1,			
			
			'post_parent' => $task_id,
			
			'orderby' => 'date',
			
			'order' => 'ASC'
			
		);
		
		$subtasks = get_posts($args);
		
		foreach($subtasks as $subtask) {
			
			$sdeleted = wp_delete_post($subtask->ID);
			
		}
		
		$deleted = wp_delete_post($task_id);
		
		if($deleted == false) {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> __('The Task could not be deleted at this time, please try again.', 'cqpim')

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);		
		
		} else {
		
			$return =  array( 

				'error' 	=> false,

				'messages' 	=> __('The task was successfully deleted.', 'cqpim')

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
		
		}
		
		exit();
	
	}
	
	// Task Timer add time
	
	add_action( "wp_ajax_nopriv_cqpim_add_timer_time", 

			"cqpim_add_timer_time");

	add_action( "wp_ajax_cqpim_add_timer_time", 

		  	"cqpim_add_timer_time");
	
	function cqpim_add_timer_time() {
	
		$time = isset($_POST['time']) ? $_POST['time'] : '';
		
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		
		
		if(empty($task_id)) {
		
			$return =  array( 

				'error' 	=> true,

				'message' 	=> __('The Task ID is missing, make sure you have selected a from the list.', 'cqpim'),

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();			
		
		} else {
		
			if(empty($time)) {
			
				$return =  array( 

					'error' 	=> true,

					'message' 	=> __('There is no time to add.', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();				
			
			} else {
			
				$time = explode(':', $time);
				
				$hours = $time[0];
				
				$hours = str_replace('00', '0', $hours);
				
				$minutes = str_replace('0', '', $time[1]);
				
				$min_dec = $minutes / 60;
				
				$time = $min_dec + $hours;
				
				$time_spent = get_post_meta($task_id, 'task_time_spent', true);
				
				$user = wp_get_current_user();
				
				$args = array(
				
					'post_type' => 'cqpim_teams',
					
					'posts_per_page' => -1,
					
					'post_status' => 'private'
				
				);
				
				$members = get_posts($args);
				
				foreach($members as $member) {
				
					$team_details = get_post_meta($member->ID, 'team_details', true);
					
					if($team_details['user_id'] == $user->ID) {
					
						$assigned = $member->ID;
					
					}
				
				}
				
				if(!$time_spent) {
				
					$time_spent = array();
					
				}
				
				$time_spent[] = array(
				
					'team' => $user->display_name,
					
					'team_id' => $assigned,
					
					'time' => bcdiv($time, 1, 2),
					
					'stamp' => time(),
				
				);
				
				update_post_meta($task_id, 'task_time_spent', $time_spent);
		
				$return =  array( 

					'error' 	=> false,

					'message' 	=> __('Time added successfully.', 'cqpim'),

				);
				
				header('Content-type: application/json');
				
				echo json_encode($return);	

				exit();	

			}
		
		}
	
	}
	
	
	// Populate Milestones Box

	add_action( "wp_ajax_nopriv_cqpim_populate_project_milestone", 

			"cqpim_populate_project_milestone");

	add_action( "wp_ajax_cqpim_populate_project_milestone", 

		  	"cqpim_populate_project_milestone");

	function cqpim_populate_project_milestone() {
	
		$user = wp_get_current_user();
		
		$args = array(
		
			'post_type' => 'cqpim_teams',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
		
		);
		
		$members = get_posts($args);
		
		foreach($members as $member) {
		
			$team_details = get_post_meta($member->ID, 'team_details', true);
			
			if($team_details['user_id'] == $user->ID) {
			
				$assigned = $member->ID;
			
			}
		
		}
		
		$project_id = isset($_POST['ID']) ? $_POST['ID']: '';
		
		$milestones = get_post_meta($project_id, 'project_elements', true);
		
		$milestones_to_display = '';
		
		if(empty($milestones)) {
		
			$milestones = array();
			
		}
		
		foreach($milestones as $milestone) {
		
			$milestones_to_display .= '<option value="' . $milestone['id'] . '">' . $milestone['title'] . '</option>';
		
		}
		
		$project_contributors = get_post_meta($project_id, 'project_contributors', true);
		
		$project_contributors_to_display = '';
		
		if(empty($project_contributors)) {
		
			$project_contributors = array();
			
		}
		
		foreach($project_contributors as $contributor) {
		
			$team_details = get_post_meta($contributor['team_id'], 'team_details', true);
			
			$team_job = isset($team_details['team_job']) ? $team_details['team_job'] : '';
			
			$team_name = isset($team_details['team_name']) ? $team_details['team_name'] : '';
			
			$project_contributors_to_display .= '<option value="' . $contributor . '">' . $team_name . ' - ' . $team_job . '</option>';
		
		}
	
		if(!$milestones_to_display && !$project_contributors_to_display) {

			$return =  array( 

				'error' 	=> true,

				'options' 	=> '<option value="">' . __('No milestones available', 'cqpim') . '</option>',
				
				'team_options' 	=> '<option value="' . $assigned . '">' . __('Me', 'cqpim') . '</option>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

		} else if(!$milestones_to_display && $project_contributors_to_display) {

			$return =  array( 

				'error' 	=> true,

				'options' 	=> '<option value="">' . __('No milestones available', 'cqpim') . '</option>',
				
				'team_options' 	=> '<option value="">' . __('Choose a team member', 'cqpim') . '</option>' . $project_contributors_to_display

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

		} else if(!$project_contributors_to_display && $milestones_to_display) {

			$return =  array( 

				'error' 	=> true,

				'options' 	=> $milestones_to_display,
				
				'team_options' 	=> '<option value="">' . __('No team members available', 'cqpim') . '</option>'

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

		} else if($project_contributors_to_display && $milestones_to_display) {

			$return =  array( 

				'error' 	=> true,

				'options' 	=> $milestones_to_display,
				
				'team_options' 	=> '<option value="' . $assigned . '">' . __('Choose a team member', 'cqpim') . '</option>' . $project_contributors_to_display

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);

		}
		
		exit();
			
	
	}
	

	add_action( "wp_ajax_nopriv_cqpim_delete_task_page", 

			"cqpim_delete_task_page");

	add_action( "wp_ajax_cqpim_delete_task_page", 

		  	"cqpim_delete_task_page");

	function cqpim_delete_task_page() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		$ppid = get_post_meta($task_id, 'project_id', true);
		
		if(!empty($ppid)) {
		
			$current_user = wp_get_current_user();;
				
			$project_progress = get_post_meta($ppid, 'project_progress', true);
			
			$task_object = get_post($task_id);
			
			$task_title = $task_object->post_title;
			
			$text = sprintf(__('Task Deleted: %1$s', 'cqpim'), $task_title );
							
			$project_progress[] = array(
							
				'update' => $text,
								
				'date' => current_time('timestamp'),
						
				'by' => $current_user->display_name
								
			);
							
			update_post_meta($ppid, 'project_progress', $project_progress );		
		
		}
		
		wp_delete_post($task_id, true);
		
		$return =  array( 

			'error' 	=> false,
			
			'redirect' 	=> admin_url() . 'admin.php?page=cqpim-tasks'

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit();
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_client_update_task", 

			"cqpim_client_update_task");

	add_action( "wp_ajax_cqpim_client_update_task", 

		  	"cqpim_client_update_task");
	
	function cqpim_client_update_task() {
	
		$changes = '';
	
		$task_id = isset ( $_POST['file_task_id'] ) ? sanitize_text_field( $_POST['file_task_id'] ) : '';
		
		$message = isset($_POST['add_task_message']) ? sanitize_text_field($_POST['add_task_message']) : '';
		
		$message = make_clickable($message);
		
		$owner = isset($_POST['task_owner']) ? sanitize_text_field($_POST['task_owner']) : '';
		
		$task_owner = get_post_meta($task_id, 'owner', true);
		
		update_post_meta($task_id, 'owner', $owner);
		
		$project_id = get_post_meta($task_id, 'project_id', true);
		
		$task_owner = get_post_meta($task_id, 'owner', true);
		
		$task_watchers = get_post_meta($task_id, 'task_watchers', true);
		
		$task_link = get_the_permalink($task_id);
		
		$task_object = get_post($task_id);
		
		$task_link = '<a class="cqpim-link" href="' . $task_link . '">' . $task_object->post_title . '</a>';
		
		$attachments = isset($_SESSION['upload_ids']) ? $_SESSION['upload_ids'] : array();
		
		foreach($attachments as $attachment) {
		
			$attachment_updated = array(
			
				'ID' => $attachment,
				
				'post_parent' => $task_id
				
			);
			
			wp_update_post($attachment_updated);
		
		}
		
		if(!empty($message)) {
		
			$task_messages = get_post_meta($task_id, 'task_messages', true);
			
			$date = current_time('timestamp');
			
			$current_user = wp_get_current_user();
			
			if(empty($message)) {
			
				$message = '';
			
			}
			
			$task_messages[] = array(
				
				'date' => $date,
				
				'message' => $message,
				
				'by' => $current_user->display_name,
				
				'author' => $current_user->ID,
				
			);		
			
			update_post_meta($task_id, 'task_messages', $task_messages);
			
			$changes .= "\r\n" . __('New Message Added', 'cqpim') . ": \r\n\r\n" . $message . "\r\n";
			
			$project_progress = get_post_meta($project_id, 'project_progress', true);
					
			$project_progress[] = array(
					
				'update' => sprintf(__('Message sent in task %1$s', 'cqpim'), $task_link),
						
				'date' => current_time('timestamp'),
				
				'by' => $current_user->display_name
						
			);
			
			foreach($attachments as $attachment) {
			
				$post = get_post($attachment);
				
				$project_progress[] = array(
						
					'update' => sprintf(__('File "%1$s" uploaded to task %2$s', 'cqpim'), $post->post_title, $task_link),
							
					'date' => current_time('timestamp'),
					
					'by' => $current_user->display_name
							
				);		

				$changes .= "\r\n" . __('New File Added', 'cqpim') . ": \r\n\r\n" . $post->post_title . "\r\n";
			
			}
					
			update_post_meta($project_id, 'project_progress', $project_progress );
		
		}
		
		update_post_meta($task_id, 'client_updated', true);
		
		update_post_meta($task_id, 'team_updated', false);
		
		cqpim_send_task_updates($task_id, $project_id, $task_owner, $task_watchers, $changes);
		
		$return =  array( 

			'error' 	=> false

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit();		
	
	}
	
	
	function cqpim_send_task_updates($post_id, $project_id, $task_owner, $task_watchers, $message, $user = null, $attachments = array()) {
		
		$emails_to_send = array();
		
		$attachments = array();
		
		$project_details = get_post_meta($project_id, 'project_details', true);
		
		// Email the Assignee (If not the person updating)	

		$client_check = preg_replace('/[0-9]+/', '', $task_owner);
		
		if($client_check == 'C') {
		
			$client = true;
			
		}
		
		if($task_owner) {
		
			if($client == true) {
			
				$id = preg_replace("/[^0-9,.]/", "", $task_owner);
				
				$client = get_user_by('id', $id);
				
				$client_email = $client->user_email;
			
			} else {
		
				$emails_to_send[] = $task_owner;
			
			}
			
		} else {
		
			$task_owner = '';
			
		}			
		
		// If Not assigned to client, Email the client main contact
		
		if(empty($client_email)) {
			
			// NEED TO GET PREFERENCES AND DECIDE WHETHER TO INCLUDE
			
			$client_contact = isset($project_details['client_contact']) ? $project_details['client_contact'] : '';
			
			$client = get_user_by('id', $client_contact);
			
			$args = array(
			
				'post_type' => 'cqpim_client',
				
				'posts_per_page' => -1,
				
				'post_status' => 'private'
			
			);
			
			$members = get_posts($args);
			
			foreach($members as $member) {
			
				$team_details = get_post_meta($member->ID, 'client_details', true);
				
				if($team_details['user_id'] == $client->ID) {
				
					$notifications = get_post_meta($member->ID, 'client_notifications', true);
				
				}
			
			}

			if(empty($assigned)) {
			
				foreach($members as $member) {
				
					$client_contacts = get_post_meta($member->ID, 'client_contacts', true);
					
					if(empty($client_contacts)) {
						
						$client_contacts = array();
						
					}
					
					foreach($client_contacts as $contact) {
						
						if($contact['user_id'] == $client->ID) {
							
							$notifications = isset($contact['notifications']) ? $contact['notifications'] : array();
							
						}
						
					}
				
				} 			
			
			}
		
			$no_tasks = isset($notifications['no_tasks']) ? $notifications['no_tasks']: 0;
			
			$no_tasks_comment = isset($notifications['no_tasks_comment']) ? $notifications['no_tasks_comment']: 0;			
			
			$client_email = $client->user_email;
			
			if(!empty($no_tasks)) {
				
				$client_email = '';
				
			}
			
			if(!empty($no_tasks_comment) && empty($message)) {
				
				$client_email = '';
				
			}
		
		}
		
		if(empty($user)) {
		
			$user = wp_get_current_user();
		
		}
		
		if(!empty($client_email)) {
		
			$subject = get_option('team_assignment_subject');
			
			$content = get_option('team_assignment_email');
			
			$url = get_the_permalink($post_id);
			
			$subject = str_replace('%%PIPING_ID%%', '[' . get_option('cqpim_string_prefix') . ':' . $post_id . ']', $subject);
			
			$content = str_replace('%%TASK_UPDATE%%', $message, $content);
			
			$content = str_replace('%%CURRENT_USER%%', $user->display_name, $content);
			
			$content = str_replace('%%NAME%%', $client->display_name, $content);
			
			$content = str_replace('%%TASK_URL%%', $url, $content);
			
			$subject = cqpim_replacement_patterns($subject, $post_id, 'task');
			
			$content = cqpim_replacement_patterns($content, $post_id, 'task');
			
			if($user->user_email != $client_email) {
		
				cqpim_send_emails($client_email, $subject, $content, '', $attachments, 'sales');
			
			}
		
		}
		
		// Email the PM
		
		$project_contributors = get_post_meta($project_id, 'project_contributors', true);
		
		if(empty($project_contributors)) {
			
			$project_contributors = array();
			
		}
		
		foreach($project_contributors as $contrib) {
		
			if($contrib['pm'] == 1) {
			
				$emails_to_send[] = $contrib['team_id'];
			
			}
		
		}	
		
		// Email the watchers
		
		if(empty($task_watchers)) {
		
			$task_watchers = array();
			
		} else {
		
			$task_watchers = $task_watchers;
			
		}
		
		foreach($task_watchers as $watcher) {
		
			$emails_to_send[] = $watcher;
		
		}
		
		$emails_to_send = array_unique($emails_to_send);
					
		foreach($emails_to_send as $email) {
		
			$team_details = get_post_meta($email, 'team_details', true);
			
			$team_email = isset($team_details['team_email']) ? $team_details['team_email'] : '';
			
			$subject = get_option('team_assignment_subject');
			
			$subject = str_replace('%%PIPING_ID%%', '[' . get_option('cqpim_string_prefix') . ':' . $post_id . ']', $subject);
			
			$content = get_option('team_assignment_email');
			
			$url = admin_url() . 'post.php?post=' . $post_id . '&action=edit';
			
			$content = str_replace('%%TASK_URL%%', $url, $content);
			
			$content = str_replace('%%NAME%%', $team_details['team_name'], $content);
			
			$content = str_replace('%%CURRENT_USER%%', $user->display_name, $content);
			
			$content = str_replace('%%TASK_UPDATE%%', $message, $content);
			
			$subject = cqpim_replacement_patterns($subject, $post_id, 'task');
			
			$content = cqpim_replacement_patterns($content, $post_id, 'task');
			
			if($user->user_email != $team_email) {
		
				cqpim_send_emails($team_email, $subject, $content, '', $attachments, 'sales');
			
			}
			
		}
	
	}
	
	// Delete task message

	add_action( "wp_ajax_nopriv_cqpim_delete_task_message", 

			"cqpim_delete_task_message");

	add_action( "wp_ajax_cqpim_delete_task_message", 

		  	"cqpim_delete_task_message");	
	
	function cqpim_delete_task_message() {
	
		$project_id = isset($_POST['project_id']) ? $_POST['project_id'] : '';
		
		$key = isset($_POST['key']) ? $_POST['key'] : '';
		
		$project_messages = get_post_meta($project_id, 'task_messages', true);
		
		$project_messages = array_reverse($project_messages);
		
		unset($project_messages[$key]);
		
		$project_messages = array_filter($project_messages);
		
		$project_messages = array_reverse($project_messages);
		
		update_post_meta($project_id, 'task_messages', $project_messages);
				
		exit();
	
	}
	
	add_action( "wp_ajax_cqpim_add_manual_task_time", 

		  	"cqpim_add_manual_task_time");
	
	function cqpim_add_manual_task_time() {
	
		$time = isset($_POST['time']) ? $_POST['time'] : '';
		
		$post_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		if(empty($time)) {
		
			$return =  array( 

				'error' 	=> true,
				
				'errors' => __('You must enter how many hours have been completed', 'cqpim')

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);	

			exit();		
		
		}
	
		$user = wp_get_current_user();
		
		$args = array(
		
			'post_type' => 'cqpim_teams',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
		
		);
		
		$members = get_posts($args);
		
		foreach($members as $member) {
		
			$team_details = get_post_meta($member->ID, 'team_details', true);
			
			if($team_details['user_id'] == $user->ID) {
			
				$assigned = $member->ID;
			
			}
		
		}
		
		$team_details = get_post_meta($assigned, 'team_details', true);
		
		$time_spent = get_post_meta($post_id, 'task_time_spent', true);
		
		if(empty($time_spent)) {
		
			$time_spent = array();
			
		}
		
		$time_spent[] = array(
		
			'team' => $team_details['team_name'],
			
			'team_id' => $assigned,
			
			'time' => $time,
			
			'stamp' => time(),
		
		);
		
		update_post_meta($post_id, 'task_time_spent', $time_spent);
		
		$return =  array( 

			'error' 	=> false

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit();
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_filter_tasks", 

			"cqpim_filter_tasks");

	add_action( "wp_ajax_cqpim_filter_tasks", 

		  	"cqpim_filter_tasks");	
	
	function cqpim_filter_tasks() {
	
		$filter = isset($_POST['filter']) ? $_POST['filter'] : '';
		
		if(empty($filter)) {
		
			$_SESSION['task_status'] = array('pending', 'progress');
			
		} elseif($filter == 'all') {
		
			$_SESSION['task_status'] = array('pending', 'progress', 'on_hold', 'complete');
		
		}else {
		
			$filter_arr = array();
			
			$filter_arr[] = $filter;
			
			$_SESSION['task_status'] = $filter_arr;
		
		}
				
		$return =  array( 

			'error' 	=> false

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit();
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_delete_file", 

			"cqpim_delete_file");

	add_action( "wp_ajax_cqpim_delete_file", 

		  	"cqpim_delete_file");		
	
	function cqpim_delete_file() {
	
  		$att_to_delete = $_POST['ID'];

  		wp_delete_attachment( $att_to_delete, TRUE );
		
		exit();
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_delete_support_page", 

			"cqpim_delete_support_page");

	add_action( "wp_ajax_cqpim_delete_support_page", 

		  	"cqpim_delete_support_page");

	function cqpim_delete_support_page() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		wp_delete_post($task_id, true);
		
		$return =  array( 

			'error' 	=> false,
			
			'redirect' 	=> admin_url() . 'admin.php?page=support-tickets'

		);
		
		header('Content-type: application/json');
		
		echo json_encode($return);	

		exit();
	
	}
	
	/************** TEMPLATE FUNCTIONS **************/
	
	// Add Step to Template

	add_action( "wp_ajax_nopriv_cqpim_add_step_to_template", 

			"cqpim_add_step_to_template");

	add_action( "wp_ajax_cqpim_add_step_to_template", 

		  	"cqpim_add_step_to_template");

	function cqpim_add_step_to_template() {
		
		$quote_id = isset($_POST['ID']) ? $_POST['ID'] : '';
		
		$title = isset($_POST['title']) ? $_POST['title'] : '';
		
		$start = isset($_POST['start']) ? $_POST['start'] : '';
		
		if(!empty($start)) {
		
			$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
		
		}
		
		$deadline = isset($_POST['deadline']) ? $_POST['deadline'] : '';
		
		if(!empty($deadline)) {
		
			$deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $deadline)->getTimestamp();
		
		}
		
		$milestone_id = isset($_POST['milestone_id']) ? $_POST['milestone_id'] : '';
		
		$cost = isset($_POST['cost']) ? $_POST['cost'] : '';
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		$order = isset($_POST['order']) ? $_POST['order'] : '';
		
		if($title) {
			
			$quote_elements = get_post_meta($quote_id, 'project_template', true);
			
			if(empty($quote_elements)) {
			
				$quote_elements = array();
				
			}
			
			$i = 0;
			
			if(!empty($quote_elements)) {
			
				foreach($quote_elements as $element) {
				
					$i++;
				
				}
			
			}
			
			$element_to_add = array(
			
				'title' => $title,
				
				'id' => $quote_id . '-' . $milestone_id,
				
				'deadline' => $deadline,
				
				'start' => $start,
				
				'cost' => $cost,
				
				'weight' => $order,
				
				'tasks' => array(),
				
			);
			
			$quote_elements['ms_key'] = $milestone_id + 1;
			
			$quote_elements['milestones'][$quote_id . '-' . $milestone_id] = $element_to_add;
			
			update_post_meta($quote_id, 'project_template', $quote_elements);
			
			$return =  array( 

				'error' 	=> false,

				'errors' 	=> __('Milestone Added.', 'cqpim')

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);
		
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> __('You must fill in the title as a minimum.', 'cqpim')

			);
			
			header('Content-type: application/json');
			
			echo json_encode($return);
			
		}
		
		exit();

	}
	
	add_action( "wp_ajax_nopriv_cqpim_create_task_template", 

			"cqpim_create_task_template");

	add_action( "wp_ajax_cqpim_create_task_template", 

		  	"cqpim_create_task_template");
	
	function cqpim_create_task_template() {
	
		if(isset($_POST['task_title'])) {
		
			$task_title = isset($_POST['task_title']) ? $_POST['task_title'] : '';
			
			$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
			
			$task_milestone_id = isset($_POST['task_milestone_id']) ? $_POST['task_milestone_id'] : '';
			
			$task_deadline = isset($_POST['task_finish']) ? $_POST['task_finish'] : '';
			
			if(!empty($task_deadline)) {
			
				$task_deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $task_deadline)->getTimestamp();
			
			}
			
			$start = isset($_POST['start']) ? $_POST['start'] : '';
			
			if(!empty($start)) {
			
				$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
			
			}
			
			$description = isset($_POST['description']) ? $_POST['description'] : '';
			
			$ppid = isset($_POST['task_project_id']) ? $_POST['task_project_id'] : '';
			
			$weight = isset($_POST['task_weight']) ? $_POST['task_weight'] : '';
			
			$milestones = get_post_meta($ppid, 'project_template', true);
			
			$milestones['milestones'][$task_milestone_id]['tasks']['task_id'] = $task_id + 1;
			
			$milestones['milestones'][$task_milestone_id]['tasks']['task_arrays'][$task_id] = array(
			
				'id' => $task_id,
			
				'title' => $task_title,
				
				'description' => $description,
				
				'start' => $start,
				
				'deadline' => $task_deadline,
				
				'weight' => $weight
			
			);
			
			update_post_meta($ppid, 'project_template', $milestones);
				
			$return =  array( 

				'error' 	=> false,

				'errors' 	=> __('The Task was successfully created.', 'cqpim')

			);
				
			header('Content-type: application/json');
					
			echo json_encode($return);
			
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> __('The Task could not be created at this time, please try again.', 'cqpim')

			);
				
			header('Content-type: application/json');
					
			echo json_encode($return);				
		
		}
		
		exit();
		
	}

	add_action( "wp_ajax_cqpim_create_subtask_template", 

		  	"cqpim_create_subtask_template");
	
	function cqpim_create_subtask_template() {
	
		if(isset($_POST['task_title'])) {
		
			$task_title = isset($_POST['task_title']) ? $_POST['task_title'] : '';
			
			$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
			
			$task_milestone_id = isset($_POST['task_milestone_id']) ? $_POST['task_milestone_id'] : '';
			
			$parent_id = isset($_POST['parent_id']) ? $_POST['parent_id'] : '';
			
			$task_deadline = isset($_POST['task_finish']) ? $_POST['task_finish'] : '';
			
			if(!empty($task_deadline)) {
			
				$task_deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $task_deadline)->getTimestamp();
			
			}
			
			$start = isset($_POST['start']) ? $_POST['start'] : '';
			
			if(!empty($start)) {
			
				$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
			
			}
			
			$description = isset($_POST['description']) ? $_POST['description'] : '';
			
			$ppid = isset($_POST['task_project_id']) ? $_POST['task_project_id'] : '';
			
			$weight = isset($_POST['task_weight']) ? $_POST['task_weight'] : '';
			
			$milestones = get_post_meta($ppid, 'project_template', true);
			
			$milestones['milestones'][$task_milestone_id]['tasks']['task_arrays'][$parent_id]['subtasks']['task_id'] = $task_id + 1;
			
			$milestones['milestones'][$task_milestone_id]['tasks']['task_arrays'][$parent_id]['subtasks']['task_arrays'][$task_id] = array(
			
				'id' => $task_id,
			
				'title' => $task_title,
				
				'description' => $description,
				
				'start' => $start,
				
				'deadline' => $task_deadline,
				
				'weight' => $weight
			
			);
			
			update_post_meta($ppid, 'project_template', $milestones);
				
			$return =  array( 

				'error' 	=> false,

				'errors' 	=> __('The Task was successfully created.', 'cqpim')

			);
				
			header('Content-type: application/json');
					
			echo json_encode($return);
			
		} else {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> __('The Task could not be created at this time, please try again.', 'cqpim')

			);
				
			header('Content-type: application/json');
					
			echo json_encode($return);				
		
		}
		
		exit();
		
	}
	
	add_action( "wp_ajax_cqpim_update_task_weight_template", "cqpim_update_task_weight_template");
	
	function cqpim_update_task_weight_template() {
	
		$template_id = isset($_POST['template_id']) ? $_POST['template_id'] : '';
	
		$weights = isset($_POST['weights']) ? $_POST['weights'] : '';
		
		$template = get_post_meta($template_id, 'project_template', true);
		
		foreach($weights as $weight) {
		
			$template['milestones'][$weight['ms_id']]['tasks']['task_arrays'][$weight['task_id']]['weight'] = $weight['weight'];
		
		}
	
		update_post_meta($template_id, 'project_template', $template);
		
		$return =  array( 

			'error' 	=> false,

			'errors' 	=> 'Task updated.'

		);
				
		header('Content-type: application/json');
					
		echo json_encode($return);	

		exit;
	
	}
	
	add_action( "wp_ajax_cqpim_update_subtask_weight_template", "cqpim_update_subtask_weight_template");
	
	function cqpim_update_subtask_weight_template() {
	
		$template_id = isset($_POST['template_id']) ? $_POST['template_id'] : '';
	
		$weights = isset($_POST['weights']) ? $_POST['weights'] : '';
		
		$template = get_post_meta($template_id, 'project_template', true);
		
		foreach($weights as $weight) {
		
			$template['milestones'][$weight['ms_id']]['tasks']['task_arrays'][$weight['parent_id']]['subtasks']['task_arrays'][$weight['task_id']]['weight'] = $weight['weight'];
		
		}
	
		update_post_meta($template_id, 'project_template', $template);
		
		$return =  array( 

			'error' 	=> false,

			'errors' 	=> 'Task updated.'

		);
				
		header('Content-type: application/json');
					
		echo json_encode($return);	

		exit;
	
	}
	
	// Update Task AJAX Callback
	
	add_action( "wp_ajax_nopriv_cqpim_update_task_template", 

			"cqpim_update_task_template");

	add_action( "wp_ajax_cqpim_update_task_template", 

		  	"cqpim_update_task_template");
	
	function cqpim_update_task_template() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		$tid = isset($_POST['tid']) ? $_POST['tid'] : '';
		
		$ms = isset($_POST['ms']) ? $_POST['ms'] : '';
		
		$title = isset($_POST['title']) ? $_POST['title'] : '';
		
		$description = isset($_POST['description']) ? $_POST['description'] : '';
		
		$start = isset($_POST['start']) ? $_POST['start'] : '';
		
		$deadline = isset($_POST['deadline']) ? $_POST['deadline'] : '';
		
		if(!empty($deadline)) {
		
			$deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $deadline)->getTimestamp();
		
		}
		
		if(!empty($start)) {
			
			$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
		
		}
		
		if(0) {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> 'Task ID cannot be found.'

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
		
		} else {
			
			$template = get_post_meta($tid, 'project_template', true);
						
			$template['milestones'][$ms]['tasks']['task_arrays'][$task_id]['title'] = $title;
			
			$template['milestones'][$ms]['tasks']['task_arrays'][$task_id]['description'] = $description;
			
			$template['milestones'][$ms]['tasks']['task_arrays'][$task_id]['start'] = $start;
			
			$template['milestones'][$ms]['tasks']['task_arrays'][$task_id]['deadline'] = $deadline;
			
			update_post_meta($tid, 'project_template', $template);
			
			$return =  array( 

				'error' 	=> false,

				'errors' 	=> 'Task updated.'

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);	
		
		}
		
		exit();
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_update_subtask_template", 

			"cqpim_update_subtask_template");

	add_action( "wp_ajax_cqpim_update_subtask_template", 

		  	"cqpim_update_subtask_template");
	
	function cqpim_update_subtask_template() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		$tid = isset($_POST['tid']) ? $_POST['tid'] : '';
		
		$ms = isset($_POST['ms']) ? $_POST['ms'] : '';
		
		$parent = isset($_POST['parent']) ? $_POST['parent'] : '';
		
		$title = isset($_POST['title']) ? $_POST['title'] : '';
		
		$description = isset($_POST['description']) ? $_POST['description'] : '';
		
		$start = isset($_POST['start']) ? $_POST['start'] : '';
		
		$deadline = isset($_POST['deadline']) ? $_POST['deadline'] : '';
		
		if(!empty($deadline)) {
		
			$deadline = DateTime::createFromFormat(get_option('cqpim_date_format'), $deadline)->getTimestamp();
		
		}
		
		if(!empty($start)) {
			
			$start = DateTime::createFromFormat(get_option('cqpim_date_format'), $start)->getTimestamp();
		
		}
		
		if(0) {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> 'Task ID cannot be found.'

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
		
		} else {
			
			$template = get_post_meta($tid, 'project_template', true);
						
			$template['milestones'][$ms]['tasks']['task_arrays'][$parent]['subtasks']['task_arrays'][$task_id]['title'] = $title;
			
			$template['milestones'][$ms]['tasks']['task_arrays'][$parent]['subtasks']['task_arrays'][$task_id]['description'] = $description;
			
			$template['milestones'][$ms]['tasks']['task_arrays'][$parent]['subtasks']['task_arrays'][$task_id]['start'] = $start;
			
			$template['milestones'][$ms]['tasks']['task_arrays'][$parent]['subtasks']['task_arrays'][$task_id]['deadline'] = $deadline;
			
			update_post_meta($tid, 'project_template', $template);
			
			$return =  array( 

				'error' 	=> false,

				'errors' 	=> 'Task updated.'

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);	
		
		}
		
		exit();
	
	}
	
	add_action( "wp_ajax_nopriv_cqpim_delete_task_template", 

			"cqpim_delete_task_template");

	add_action( "wp_ajax_cqpim_delete_task_template", 

		  	"cqpim_delete_task_template");
	
	function cqpim_delete_task_template() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		$ms = isset($_POST['ms']) ? $_POST['ms'] : '';
		
		$tid = isset($_POST['tid']) ? $_POST['tid'] : '';
		
		$template = get_post_meta($tid, 'project_template', true);	
				
		unset($template['milestones'][$ms]['tasks']['task_arrays'][$task_id]);
		
		if(empty($template['milestones'][$ms]['tasks']['task_arrays'])) {
			
			unset($template['milestones'][$ms]['tasks']['task_id']);
			
		}
		
		update_post_meta($tid, 'project_template', $template);		
		
		$return =  array( 

			'error' 	=> false,

			'messages' 	=> __('The task was successfully deleted.', 'cqpim')

		);
				
		header('Content-type: application/json');
					
		echo json_encode($return);			
		
		exit();
	
	}

	add_action( "wp_ajax_cqpim_delete_subtask_template", 

		  	"cqpim_delete_subtask_template");
	
	function cqpim_delete_subtask_template() {
	
		$task_id = isset($_POST['task_id']) ? $_POST['task_id'] : '';
		
		$ms = isset($_POST['ms']) ? $_POST['ms'] : '';
		
		$parent = isset($_POST['parent']) ? $_POST['parent'] : '';
		
		$tid = isset($_POST['tid']) ? $_POST['tid'] : '';
		
		$template = get_post_meta($tid, 'project_template', true);	
				
		unset($template['milestones'][$ms]['tasks']['task_arrays'][$parent]['subtasks']['task_arrays'][$task_id]);
		
		if(empty($template['milestones'][$ms]['tasks']['task_arrays'][$parent]['subtasks']['task_arrays'])) {
			
			unset($template['milestones'][$ms]['tasks']['task_arrays'][$parent]['subtasks']['task_id']);
			
		}
		
		update_post_meta($tid, 'project_template', $template);		
		
		$return =  array( 

			'error' 	=> false,

			'messages' 	=> __('The task was successfully deleted.', 'cqpim')

		);
				
		header('Content-type: application/json');
					
		echo json_encode($return);			
		
		exit();
	
	}

	add_action( "wp_ajax_cqpim_clear_all_template", 

		  	"cqpim_clear_all_template");
	
	function cqpim_clear_all_template() {
	
		$tid = isset($_POST['tid']) ? $_POST['tid'] : '';
		
		delete_post_meta($tid, 'project_template');
		
		$return =  array( 

			'error' 	=> false,

			'messages' 	=> __('The template was successfully cleared.', 'cqpim')

		);
				
		header('Content-type: application/json');
					
		echo json_encode($return);			
		
		exit();		
	
	}
	
	add_action( "wp_ajax_cqpim_apply_template", 

		  	"cqpim_apply_template");
	
	function cqpim_apply_template() {
	
		$item_ref = isset($_POST['quote_id']) ? $_POST['quote_id'] : '';
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		$template = isset($_POST['template']) ? $_POST['template'] : '';
		
		if(empty($template)) {
		
			$return =  array( 

				'error' 	=> true,

				'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('You must choose a template!', 'cqpim') . '</div>'

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
			
			exit();		
		
		} else {
		
			if($type == 'quote') {
			
				$elements = get_post_meta($item_ref, 'quote_elements', true);
				
			} else {
			
				$elements = get_post_meta($item_ref, 'project_elements', true);
			
			}
			
			if(!empty($elements)) {
			
				foreach($elements as $element) {
				
					$args = array(
								
						'post_type' => 'cqpim_tasks',
									
						'posts_per_page' => -1,
									
						'meta_key' => 'milestone_id',
									
						'meta_value' => $element['id'],
									
						'orderby' => 'date',
									
						'order' => 'ASC'
									
					);
								
					$tasks = get_posts($args);
								
					foreach($tasks as $task) {
						
						$args = array(
									
							'post_type' => 'cqpim_tasks',
										
							'posts_per_page' => -1,
										
							'post_parent' => $task->ID,
										
							'orderby' => 'date',
										
							'order' => 'ASC'
										
						);	

						$subtasks = get_posts($args);
						
						foreach($subtasks as $subtask) {
							
							wp_delete_post($subtask->ID);
							
						}
								
						wp_delete_post($task->ID);		
							
					}				
				
				}
				
				if($type == 'quote') {
				
					delete_post_meta($item_ref, 'quote_elements');
					
				} else {
				
					delete_post_meta($item_ref, 'project_elements');
				
				}
			
			}
			
			$template_contents = get_post_meta($template, 'project_template', true);
			
			if(empty($template_contents['milestones'])) {
			
				$return =  array( 

					'error' 	=> true,

					'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('The chosen template does not contain any Milestones or Tasks.', 'cqpim') . '</div>'

				);
						
				header('Content-type: application/json');
							
				echo json_encode($return);			
				
				exit();			
			
			} else {
			
				$milestones = array();
				
				$i = 0;
			
				foreach($template_contents['milestones'] as $milestone) {
				
					$mtitle = isset($milestone['title']) ? $milestone['title'] : '';
					
					$mdeadline = isset($milestone['deadline']) ? $milestone['deadline'] : '';
					
					$mstart = isset($milestone['start']) ? $milestone['start'] : '';
					
					$mcost = isset($milestone['cost']) ? $milestone['cost'] : '';
					
					$mweight = isset($milestone['weight']) ? $milestone['weight'] : '';
				
					$milestones[$item_ref . '-' . $i] = array(
					
						'title' => $milestone['title'],
						
						'id' => $item_ref . '-' . $i,
						
						'deadline' => $milestone['deadline'],
						
						'start' => $milestone['start'],
						
						'cost' => $milestone['cost'],
						
						'weight' => $milestone['weight'],
					
					);
					
					if(!empty($milestone['tasks']['task_arrays'])) {
					
						foreach($milestone['tasks']['task_arrays'] as $task) {
						
							$title = isset($task['title']) ? $task['title'] : '';
							
							$description = isset($task['description']) ? $task['description'] : '';
							
							$start = isset($task['start']) ? $task['start'] : '';
							
							$deadline = isset($task['deadline']) ? $task['deadline'] : '';
							
							$weight = isset($task['weight']) ? $task['weight'] : '';
						
							$new_task = array(
							
								'post_type' => 'cqpim_tasks',
								
								'post_status' => 'publish',
								
								'post_content' => '',
								
								'post_title' => $title,
								
								'post_password' => cqpim_random_string(10),
							
							);
							
							$task_pid = wp_insert_post( $new_task, true );

							if( is_wp_error( $task_pid ) ){
							
								$return =  array( 

									'error' 	=> true,

									'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem creating the tasks. Please try again later.', 'cqpim') . '</div>'

								);
										
								header('Content-type: application/json');
											
								echo json_encode($return);			
								
								exit();							
							
							} else {
							
								$task_updated = array(
									
									'ID' => $task_pid,
										
									'post_name' => $task_pid,
										
								);						
							
								wp_update_post( $task_updated );

								update_post_meta($task_pid, 'milestone_id', $item_ref . '-' . $i);	
								
								if($type == 'project') {
								
									update_post_meta($task_pid, 'project_id', $item_ref);
								
								} else {
								
									update_post_meta($task_pid, 'project_id', 0);
								
								}

								$task_details = array(
								
									'deadline' => $deadline,
									
									'status' => 'pending',
									
									'task_start' => $start,
									
									'task_description' => $description,
									
									'task_pc' => 0,
									
									'task_priority' => 'normal',
									
									'weight' => $weight,
								
								);
								
								update_post_meta($task_pid, 'task_details', $task_details);								
							
							}
							
							// Subtasks here
							
							if(!empty($task['subtasks']['task_arrays'])) {
								
								foreach($task['subtasks']['task_arrays'] as $subtask) {
									
									$title = isset($subtask['title']) ? $subtask['title'] : '';
									
									$description = isset($subtask['description']) ? $subtask['description'] : '';
									
									$start = isset($subtask['start']) ? $subtask['start'] : '';
									
									$deadline = isset($subtask['deadline']) ? $subtask['deadline'] : '';
									
									$weight = isset($subtask['weight']) ? $subtask['weight'] : '';
								
									$new_subtask = array(
									
										'post_type' => 'cqpim_tasks',
										
										'post_status' => 'publish',
										
										'post_content' => '',
										
										'post_title' => $title,
										
										'post_parent' => $task_pid,
										
										'post_password' => cqpim_random_string(10),
									
									);
									
									$subtask_pid = wp_insert_post( $new_subtask, true );

									if( is_wp_error( $subtask_pid ) ){
									
										$return =  array( 

											'error' 	=> true,

											'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There was a problem creating the tasks. Please try again later.', 'cqpim') . '</div>'

										);
												
										header('Content-type: application/json');
													
										echo json_encode($return);			
										
										exit();							
									
									} else {
									
										$task_updated = array(
											
											'ID' => $subtask_pid,
												
											'post_name' => $subtask_pid,
												
										);						
									
										wp_update_post( $task_updated );

										update_post_meta($subtask_pid, 'milestone_id', $item_ref . '-' . $i);	
										
										if($type == 'project') {
										
											update_post_meta($subtask_pid, 'project_id', $item_ref);
										
										} else {
										
											update_post_meta($subtask_pid, 'project_id', 0);
										
										}

										$task_details = array(
										
											'deadline' => $deadline,
											
											'status' => 'pending',
											
											'task_start' => $start,
											
											'task_description' => $description,
											
											'task_pc' => 0,
											
											'task_priority' => 'normal',
											
											'weight' => $weight,
										
										);
										
										update_post_meta($subtask_pid, 'task_details', $task_details);								
									
									}									
									
								}
								
							}
						
						}
					
					}
					
					$i++;
				
				}
				
				if($type == 'quote') {
				
					update_post_meta($item_ref, 'quote_elements', $milestones);
					
				} else {
				
					update_post_meta($item_ref, 'project_elements', $milestones);
				
				}
				
				$return =  array( 

					'error' 	=> false,

					'messages' 	=> '<div class="cqpim-alert cqpim-alert-success alert-display">' . __('The template was successfully applied.', 'cqpim') . '</div>'

				);
						
				header('Content-type: application/json');
							
				echo json_encode($return);			
				
				exit();
			
			}	
		
		}
	
	}
	
	add_action( "wp_ajax_cqpim_clear_all_action", 

		  	"cqpim_clear_all_action");
	
	function cqpim_clear_all_action() {
	
		$item_ref = isset($_POST['quote_id']) ? $_POST['quote_id'] : '';
		
		$type = isset($_POST['type']) ? $_POST['type'] : '';
				
		if($type == 'quote') {
		
			$elements = get_post_meta($item_ref, 'quote_elements', true);
			
		} else {
		
			$elements = get_post_meta($item_ref, 'project_elements', true);
		
		}
		
		if(!empty($elements)) {
		
			foreach($elements as $element) {
			
				$args = array(
							
					'post_type' => 'cqpim_tasks',
								
					'posts_per_page' => -1,
								
					'meta_key' => 'milestone_id',
								
					'meta_value' => $element['id'],
								
					'orderby' => 'date',
								
					'order' => 'ASC'
								
				);
							
				$tasks = get_posts($args);
							
				foreach($tasks as $task) {
					
					$args = array(
								
						'post_type' => 'cqpim_tasks',
									
						'posts_per_page' => -1,
									
						'post_parent' => $task->ID,
									
						'orderby' => 'date',
									
						'order' => 'ASC'
									
					);	

					$subtasks = get_posts($args);
					
					foreach($subtasks as $subtask) {
						
						wp_delete_post($subtask->ID);
						
					}
							
					wp_delete_post($task->ID);		
						
				}				
			
			}
			
			if($type == 'quote') {
			
				delete_post_meta($item_ref, 'quote_elements');
				
			} else {
			
				delete_post_meta($item_ref, 'project_elements');
			
			}
			
			$return =  array( 

				'error' 	=> false,

				'messages' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('Milestones & Tasks cleared successfully.', 'cqpim') . '</div>'

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
			
			exit();
		
		} else {

			$return =  array( 

				'error' 	=> true,

				'errors' 	=> '<div class="cqpim-alert cqpim-alert-danger alert-display">' . __('There is nothing to clear!', 'cqpim') . '</div>'

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
			
			exit();

		}
	
	}
	
	add_action( "wp_ajax_cqpim_update_task_weight", 

		  	"cqpim_update_task_weight");
	
	function cqpim_update_task_weight() {
	
		$weights = isset($_POST['weights']) ? $_POST['weights'] : '';
		
		if(empty($weights)) {
		
			$weights = array();
			
		}
		
		foreach($weights as $key => $weight) {
		
			$task_details = get_post_meta($key, 'task_details', true);
			
			$task_details['weight'] = isset($weight['weight']) ? $weight['weight'] : '';
			
			update_post_meta($key, 'task_details', $task_details);
		
		}
	
		$return =  array( 

			'error' 	=> false,
			
			'weights' => $weights,

		);
				
		header('Content-type: application/json');
					
		echo json_encode($return);			
		
		exit();
	
	}
	
	add_action( "wp_ajax_cqpim_save_custom_fields", 

		  	"cqpim_save_custom_fields");
	
	function cqpim_save_custom_fields() {

		$type = isset($_POST['type']) ? $_POST['type'] : '';
		
		$builder = isset($_POST['builder']) ? $_POST['builder'] : ''; 
		
		if(empty($type) || empty($builder)) {	
			
			$return =  array( 

				'error' 	=> true,

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
			
			exit();			
			
		} else {
			
			update_option('cqpim_custom_fields_' . $type, $builder);
			
			$return =  array( 

				'error' 	=> false,

			);
					
			header('Content-type: application/json');
						
			echo json_encode($return);			
			
			exit();			
			
			
		}

	}
	
	
	
	
	
	

	
