CQPIM - Clients, Quotes, Projects & Invoice Manager

Version 3.2.1

For documentation and help, please visit - 

http://www.cqpim.uk/docs

CHANGELOG

Version 3.2.1

- NEW - Added Support for Upcoming Suppliers / Expenses Plugin
- FIX - Minor bug fixes from v3.2

Version 3.2

- NEW - PHP7 Support

Version 3.1

- NEW - Redesigned Admin and Client Dashboards
- NEW - New Invoice Template Design
- NEW - CC address for outgoing emails
- NEW - HTML Email Template system

Version 3.0.5

- NEW - Direct Messaging System

Version 3.0.1

- FIXED - Missing Menu Toggle in Admin for Team Members
- FIXED - Email Encoding Issue on Outgoing Emails
- FIXED - Button Styling Conflict in WordPress 4.8

Version 3

- NEW - Secondary Sales Taxes
- NEW - Currency Override Per Client, Quote, Project and Invoice
- NEW - Client Tax Reg Numbers on Invoices
- NEW - Per Line Sales Tax on Invoices
- NEW - Disable Partial Invoice Payments per Invoice
- NEW - Currency Symbol Position Setting
- NEW - Dashboard Summary of Outstanding Invoice Totals
- NEW - Income Graphs on Admin Dashboard
- NEW - Invoice Paid Notifications
- DEFERRED - Subscriptions Area with Custom Fields
- NEW - Custom Fields in Support Tickets
- NEW - Custom Support Ticket Status� and Priorities
- NEW - Sub Tasks
- NEW - Milestones, Tasks and Invoicing in Support Tickets
- NEW - Existing User to Team Member Conversion Tool
- NEW - Responsive Admin Area
- NEW - Improved Responsive Client Dashboard Menu
- NEW - More Task Permissions
- NEW - Client Email Notification Preferences
- NEW - Function to Remove All Data
- NEW - Pending Quote Requests on Admin Dashboard
- NEW - General Project Information Page
- NEW - File Attachments in Quotes and Clients
- NEW - Improved File Viewer on Client Dashboard
- NEW - Task Complete Buttons
- NEW - Hide Completed Tasks in Projects
- NEW - Team Only Notes in Support Tickets
- NEW - Client Access Activity Logs
- NEW - Who�s Online Widget on Admin Dash
- NEW - Disable Gravatars / WP Avatar plugin support
- NEW - Custom Widget Area in Client Dashboard Sidebar
- NEW - Project Manager Assignment in Projects
- NEW - Assign Tasks to Client
- NEW - New, Improved Quote Form Builder
- NEW - Taxonomies in Clients and Projects

Version 2.9.63

- FIXED - Datatables translation issues on Frontend
- FIXED - Issue with overwriting Milestone Templates
- FIXED - Issue in translation of frontend quote form
- FIXED - Admin menu appearing on CQPIM login page for some installations
- FIXED - Tax not being added correctly to recurring invoices
- FIXED - Biyearly Recurring Invoice issue
- FIXED - Updated password reset tag in emails to CQPIM Reset Process


Version 2.9.62

- CHANGE - Improved Invoice payment gateway process
- FIXED - Fixed missing translations - Thank you to Giewont :-)
- FIXED - Bug Fixes

Version 2.9.61

- FIXED - Fixed various translation issues & Updated PO files
- FIXED - Update mPDF asset
- FIXED - Left align all tables
- FIXED - Replacement patterns when admin raises support ticket
- FIXED - php-imap generates errors

Version 2.9.6

- FIXED - Issue with Visual Editors - Upgraded jQuery UI to 1.11.4

Version 2.9.5

- NEW - Attachment Support for Email Piping
- FIXED - Dashboard Logo overlap
- FIXED - Call Gravatars from Secure URL
- FIXED - Untranslatable "Back to project" link in Tasks
- FIXED - Roles & Permissions deleted when deactivating plugin

Version 2.9.1

- FIXED - Admin file uploads in support tickets
- FIXED - Disabled sales tax in client not reflecting in invoices

Version 2.9

- NEW - Email Piping on Tasks & Support Tickets

Version 2.8.5

- NEW - Improved Client Dashboard (CQPIM Theme Only)
- NEW - Ajax Login
- NEW - Custom Ajax Password Reset
- NEW - Ajax Task/Ticket update
- NEW - Ajax File Uploader in Tasks & Tickets
- NEW - Company Branding on Login and Dashboard (CQPIM Theme Only)
- CHANGE - Ticket assignee can be left blank
- FIXED - Manual time entry on task adds twice
- FIXED - Alerts showing that tickets need attantion when they are resolved
- FIXED - Delete ticket permissions fixed
- FIXED - Admin resolved ticket switcher not working
- FIXED - Missing descriptions on quote forms
- FIXED - Ticket/Task notifications sending to updater
- FIXED - Client settings aren't translatable

Version 2.8

- NEW - Tax Settings Per Client
- NEW - Admin ability to create Support Tickets
- NEW - Show Support Tickets in Admin Client Area
- NEW - Ability to Set Default Ticket Assignee Per Client
- NEW - Client Access Permissions
- NEW - Milestone and Task Templates
- NEW - Ability to Edit/Delete Partial Invoice Payments
- NEW - Ability to add comments to Partial Invoice Payments
- NEW - Visually Improved Admin Dashboard Summary Page
- NEW - Ability to Disable Invoice Section
- NEW - Drag & Drop Milestone and Task Ordering

Version 2.7.71

- FIXED - Quote Forms delete fields when updating with no changes made
- FIXED - Team members can be added twice
- FIXED - Punctuation in quote form labels breaks form
- FIXED - Colorbox issue in Quote Forms with WP 4.4


Version 2.7.7

- FIXED - WP 4.4 Admin Bar Issue
- FIXED - WP 4.4 Admin CSS Issue
- FIXED - WP 4.4 Admin Colorbox Issue

Version 2.7.6

- FIXED - Bug in Quote Forms
- FIXED - Print views of contracts, quotes and invoices looping in some themes

Version 2.7.5

- NEW - Project & Invoicing Workflow Customisation
- Change - Added biyearly recurring invoice
- Fixed - Issue with quote form builder not appearing in Admin
- Fixed - Partial invoice payments currency bug
- Fixed - Calendar breaks if task with no dates is added
- Fixed - Company Number tag not working (%%COMPANY_NUMBER%%)
- Fixed - Inconsistent styling on invoice postcode field
- Fixed - Inability to delete roles
- Fixed - Unstyled admin bar appearing on CQPIM Login page
- Fixed - Infinite AJAX loop when adding tasks to project with no deadline
- Fixed - Some emails not sending from support tickets

Version 2.7.2

Bug fixes

Version 2.7.1

Fixed jQuery UI errors (causing some functionality to break)

Version 2.7

WARNING: BACKUP YOUR DATABASE BEFORE UPDATING TO 2.7. THIS IS DUE TO THE PLUGIN CONVERTING ALL DATES TO TIMESTAMPS.

New tasks system
Date format options
Client Dashboard logout redirect setting
Removed active theme styles from built in client dash
Added custom CSS setting for built in client dash
Added status filter on tasks pages
Separated admin all tasks view
Bulk operations for sending invoices/mark as paid/unpaid
Improved support tickets with assignment & files
Improved Admin/Team Member Dashboard
Team Member Calendars
Configurable URLs for Invoices/Projects/Quotes/Support Tickets & Tasks
Partial invoice payments
Create CQPIM client from existing User action
Changed "delete client" option to give option of unlinking user first
Fixed - Some Small Bugs

Version 2.6

New Client Dashboard Summary Page
Client Dashboard Alerts
Quote Form Builder (Anonymous Frontend and Client Dashboard)
Client Dashboard theme Switcher (Built in or current active theme)
Multiple contacts for clients & Client managed users
Ability for Clients to update their details
Stripe Integration
Ability to add and choose from multiple contract terms

Version 2.5.3

Fixed issue with Paypal Payments

Version 2.5.2

Fixed - AJAX Issue on Tasks Page

Version 2.5.1

Fixed - Issue with sequential invoice numbers

Version 2.5

Added i18n & PO/MO files
Change - Mobile Friendly Client Dashboard

Version 2.2.2

Redesign Client Dashboard

Version 2.2.1

Fixed error in Client Admin

Version 2.2

Added extra frequencies to Recurring Invoices
Added extra Term options to Invoice Terms
Added Task Timers on Tasks Page
Added PayPal Invoice Payments
Added jQuery DataTables to all plugin Tables
Added PDF Attachments to Invoice emails
Change - Improvements to Client Dashboard & Login

_____________________________________________________

Version 2.1

Fixed small bug in Invoice repeater fields
Fixed bug in Colorbox on Quote creation
Fixed error in PHP 5.4
Added Recurring Invoices/Monthly Billing Options
Added ability to create Ad-Hoc Projects
Added Clone/Duplicate Quote &amp; Project Function

_____________________________________________________

Version 2.0.2

Added Standalone Tasks Page with "Time Spent" Column

_____________________________________________________

Version 2.0.1

Added Support Tickets
Minor Bug Fixes