<?php

	$user = wp_get_current_user();

	$client_logs = get_post_meta($assigned, 'client_logs', true);

	if(empty($client_logs)) {

		$client_logs = array();
		
	}
	
	$title = get_the_title();
	
	$title = str_replace('Private:', '', $title);

	$now = time();

	$client_logs[$now] = array(

		'user' => $user->ID,
		
		'page' => sprintf(__('Project %1$s - %2$s (Update / Status Page)', 'cqpim'), get_the_ID(), $title)

	);
	
	update_post_meta($assigned, 'client_logs', $client_logs);
	
	$client_contract = get_post_meta($assigned, 'client_contract', true);

?>

	<div id="htm" class="cqpim-dash-column">
	
		<div class="cqpim-project-box">
		
			<div class="title">
				
					<h3><?php echo $title; ?></h3>
			
			</div>
			
			<div class="content">
			
				<ul class="cqpim-elements">
				
					<li>
					
						<h3><?php echo number_format((float)$pc_complete, 2, '.', ''); ?>%</h3>
					
						<span><?php _e('Complete', 'cqpim'); ?></span>
					
					</li>
					
					<li>
					
						<h3><?php echo $task_count; ?></h3>
					
						<span><?php _e('Tasks', 'cqpim'); ?></span>
					
					</li>
					
					<li>
					
						<h3><?php echo $days_to_due; ?></h3>
					
						<span><?php _e('Days to Launch!', 'cqpim'); ?></span>
					
					</li>
					
				</ul>
			
			</div>
		
		</div>	
		
		<div class="cqpim-project-box">
		
			<div class="title">
			
					<h3><?php _e('Project Status', 'cqpim'); ?></h3>
			
			</div>
			
			<div class="content">
			
				<table class="milestones sum-status">
				
					<thead>
					
						<tr>
					
							<th colspan="2"><?php _e('Prerequisites', 'cqpim'); ?></th>
						
						</tr>
					
					</thead>
					
					<tbody>
					
						<?php $status = ( !empty( $sent ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<?php $checked = get_option('enable_project_contracts'); 
										
						if(!empty($checked) && empty($client_contract)) { ?>
						
							<tr>
							
								<td><?php _e('Contract Sent', 'cqpim'); ?></td>
								
								<td><?php echo $status; ?></td>
							
							</tr>
							
							<?php $status = ( !empty( $confirmed ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
						
							<tr>
							
								<td><?php _e('Contract Signed', 'cqpim'); ?></td>
								
								<td><?php echo $status; ?></td>
							
							</tr>
						
						<?php } ?>
						
						<?php if($deposit && $deposit != 'none') { if(get_option('disable_invoices') != 1) { ?>
						
						<?php $status = ( !empty( $deposit_invoice_id ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<tr>
						
							<td><?php _e('Deposit Invoice Created', 'cqpim'); ?></td>
							
							<td><?php echo $status; ?></td>
						
						</tr>

						<?php $status = ( !empty( $deposit_sent ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<tr>
						
							<td><?php _e('Deposit Invoice Sent', 'cqpim'); ?></td>
							
							<td><?php echo $status; ?></td>
						
						</tr>	

						<?php $status = ( !empty( $deposit_paid ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<tr>
						
							<td><?php _e('Deposit Paid', 'cqpim'); ?></td>
							
							<td><?php echo $status; ?></td>
						
						</tr>						
						
						<?php } } ?>
						
						<tr>
					
							<th colspan="2"><?php _e('Milestones', 'cqpim'); ?></th>
						
						</tr>

						<?php 
						
						$ordered = array();
						
						$i = 0;
						
						$mi = 0;
						
						foreach($project_elements as $key => $element) {
						
							$weight = isset($element['weight']) ? $element['weight'] : $mi;
						
							$ordered[$weight] = $element;
							
							$mi++;
						
						}
						
						ksort($ordered);						
						
						foreach($ordered as $element) { 
						
						$status = isset($element['status']) ? $element['status'] : ''; ?>
					
						<?php $status = (  $status == 'complete' ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<tr>
						
							<td><?php echo $element['title']; ?></td>
							
							<td><?php echo $status; ?></td>
						
						</tr>
						
						<?php } ?>

						<tr>
					
							<th colspan="2"><?php _e('Completion', 'cqpim'); ?></th>
						
						</tr>	

						<?php $status = ( !empty( $signoff ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<tr>
						
							<td><?php _e('Signed Off / Launched', 'cqpim'); ?></td>
							
							<td><?php echo $status; ?></td>
						
						</tr>
						
						<?php if(get_option('disable_invoices') != 1  && get_option('invoice_workflow') != 1) { ?>

						<?php $status = ( !empty( $completion_invoice_id ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<tr>
						
							<td><?php _e('Completion Invoice Created', 'cqpim'); ?></td>
							
							<td><?php echo $status; ?></td>
						
						</tr>

						<?php $status = ( !empty( $completion_sent ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<tr>
						
							<td><?php _e('Completion Invoice Sent', 'cqpim'); ?></td>
							
							<td><?php echo $status; ?></td>
						
						</tr>	

						<?php $status = ( !empty( $completion_paid ) ) ? '<span class="task_complete">' . __('Complete', 'cqpim') . '</span>' : '<span class="task_pending">' . __('Pending', 'cqpim') . '</span>'; ?>
					
						<tr>
						
							<td><?php _e('Completion Invoice Paid', 'cqpim'); ?></td>
							
							<td><?php echo $status; ?></td>
						
						</tr>

						<?php } ?>						
					
					</tbody>
				
				</table>
			
			</div>
			
		</div>	
	
	</div>
	
	
	<div id="hta" style="padding-right:0; float:right" class="cqpim-dash-column">
	
		<div class="cqpim-project-box">
		
			<div class="title">
				
					<h3><?php _e('Project Updates', 'cqpim'); ?></h3>
			
			</div>
			
			<div class="content">

				<?php if($project_progress) {
				
					$project_progress = array_reverse($project_progress);
				
					echo '<ul class="project_summary_progress">';
				
					foreach($project_progress as $progress) {
					
						if(is_numeric($progress['date'])) { $date = date(get_option('cqpim_date_format') . ' H:i', $progress['date']); } else { $date = $date; }
					
						echo '<li>';
						
						echo '<div class="timeline-entry">';
						
						echo '<span class="timeline-by">' . $progress['by'] . '</span>';
						
						echo '<span class="timeline-date">' . $date . '</span>';
						
						echo '<span class="timeline-update">' . $progress['update'] . '</span>';
						
						echo '</div>';
						
						echo '</li>';
					
					}
					
					echo '</ul>';
				
				} ?>

			</div>
			
		</div>
	
	</div>
	
	<div class="clear"></div>