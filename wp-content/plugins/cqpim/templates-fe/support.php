<?php 

if (!current_user_can( 'cqpim_client' )) {

	$login_page = get_option('cqpim_login_page');
	
	$url = get_the_permalink($login_page);
	
	wp_redirect($url, 302);

} else {

	include_once( realpath(dirname(__FILE__)) . '/upload.php' );

	$user = wp_get_current_user(); 
	
	$login_page_id = get_option('cqpim_login_page');
	
	$login_url = get_the_permalink($login_page_id);
	
	$user_id = $user->ID;
	
	$dash_type = get_option('client_dashboard_type');
	
	$quote_form = get_option('cqpim_backend_form');
	
	$author = $post->post_author;
	
	// Assignment
	
	$args = array(
	
		'post_type' => 'cqpim_client',
		
		'posts_per_page' => -1,
		
		'post_status' => 'private'
	
	);
	
	$members = get_posts($args);
	
	foreach($members as $member) {
	
		$team_details = get_post_meta($member->ID, 'client_details', true);
		
		if($team_details['user_id'] == $author) {
		
			$assigned = $member->ID;
			
			$client_type = 'admin';
		
		}
	
	} 
	
	if(empty($assigned)) {
	
		foreach($members as $member) {
		
			$team_ids = get_post_meta($member->ID, 'client_ids', true);
			
			if(!is_array($team_ids)) {
			
				$team_ids = array($team_ids);
			
			}
			
			if(in_array($author, $team_ids)) {
			
				$assigned = $member->ID;
				
				$client_type = 'contact';
			
			}
		
		} 			
	
	}
	
	$client_details = get_post_meta($assigned, 'client_details', true);
	
	$client_ids = get_post_meta($assigned, 'client_ids', true);
	
	$client_ids_untouched = $client_ids;
	
	if(empty($client_ids_untouched)) {
		
		$client_ids_untouched = array();
		
	}
	
	$looper = get_post_meta($post->ID, 'looper', true);
	
	if(time() - $looper > 5) {
	
		$user = wp_get_current_user();

		$client_logs = get_post_meta($assigned, 'client_logs', true);

		if(empty($client_logs)) {

			$client_logs = array();
			
		}

		$now = time();
		
		$title = get_the_title();
		
		$title = str_replace('Private:', '', $title);

		$client_logs[$now] = array(

			'user' => $user->ID,
			
			'page' => sprintf(__('Support Ticket - %1$s', 'cqpim'), $title)

		);
		
		update_post_meta($assigned, 'client_logs', $client_logs);
		
		update_post_meta($post->ID, 'looper', time());
	
	}
	
}

?>
	
<?php 

	get_header();
	
?>

	<div  style="padding-left:0; margin:0 auto; max-width:1100px; padding:20px; background:#fff" class="cqpim-client-dash" role="main">	
	
		<div  style="background:0; padding:0 0 30px 0" class="cqpim-dash-header">
		
			<?php 
			
			$value = get_option('cqpim_disable_avatars');
			
			$user = wp_get_current_user();
			
			if(empty($value)) {
				
				echo '<div class="user_avatar" style="float:left; margin-right:20px">';
			
					echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 
			
				echo '</div>';
			
			} 
			
			?> 

			<h1><?php printf(__('Welcome back, %1$s', 'cqpim'), $user->display_name); ?><br /><span style="font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $user->user_email; ?></span> <span style="margin-left:10px;font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $client_details['client_company']; ?> <?php if($client_type == 'admin') { echo '&nbsp;&nbsp;(Main Contact)'; } ?></span></h1>
		
			<div class="clear"></div>
			
			<?php if($dash_type != 'inc') { ?>
			
				<?php $client_dash = get_option('cqpim_client_page'); ?>
			
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash); ?>"><?php _e('Dashboard', 'cqpim'); ?></a>
		
				<?php if(get_option('cqpim_messages_allow_client') == 1) { ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=messages'; ?>"><?php _e('Messages', 'cqpim'); ?></a>
				
				<?php } ?>				
				
				<?php if(!empty($quote_form)) { ?>
				
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=quote_form'; ?>"><?php _e('Request a Quote', 'cqpim'); ?></a>
				
				<?php } 
			
				$client_settings = get_option('allow_client_settings');
				
				if($client_settings == 1) { ?>				
					
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=settings'; ?>"><?php _e('Settings', 'cqpim'); ?></a>

				<?php } 
				
				$client_settings = get_option('allow_client_users');
				
				if($client_settings == 1) { ?>	

					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=contacts'; ?>"><?php _e('Users', 'cqpim'); ?></a>

				<?php } ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo wp_logout_url($login_url); ?>"><?php _e('Log Out', 'cqpim'); ?></a>
				
				<div class="clear"></div>
			
			<?php } ?>

		</div>
		
		<div class="cqpim-dash-content">
		
			<?php
			
			$user = wp_get_current_user(); 
			
			$user_id = $user->ID;
			
			$dash_page = get_option('cqpim_client_page');
		
			$dash_url = get_the_permalink($dash_page);
			
			$ticket_author = $post->post_author;
			
			$author_details = get_user_by('id', $ticket_author);
			
			$client_name = $author_details->display_name;
		
			$ticket_status = get_post_meta($post->ID, 'ticket_status', true);
			
			$ticket_priority = get_post_meta($post->ID, 'ticket_priority', true);
			
			$ticket_updated = get_post_meta($post->ID, 'last_updated', true);
			
			if(is_numeric($ticket_updated)) { $ticket_updated = date(get_option('cqpim_date_format') . ' H:i', $ticket_updated); } else { $ticket_updated = $ticket_updated; }
			
			if($ticket_status == 'open') {
			
				$status = '<span class="low_priority">' . __('Open', 'cqpim') . '</span>';
			
			} else if($ticket_status == 'resolved') {
			
				$status = '<span class="task_complete">' . __('Resolved', 'cqpim') . '</span>';
			
			} else if($ticket_status == 'hold') {
			
				$status = '<span class="high_priority">' . __('On Hold', 'cqpim') . '</span>';
			
			}
			
			if($ticket_priority == 'low') {
			
				$priority = '<span class="low_priority">' . __('Low', 'cqpim') . '</span>';
			
			} else if($ticket_priority == 'normal') {
			
				$priority = '<span class="normal_priority">' . __('Normal', 'cqpim') . '</span>';
			
			} else if($ticket_priority == 'high') {
			
				$priority = '<span class="high_priority">' . __('High', 'cqpim') . '</span>';
			
			} else if($ticket_priority == 'immediate') {
			
				$priority = '<span class="immediate_priority">' . __('Immediate', 'cqpim') . '</span>';
			
			}
			
			if(current_user_can( 'edit_cqpim_supports' ) OR $post->post_author == $user_id OR in_array($user_id, $client_ids_untouched)) { 
			
				if($post->post_author == $user_id OR in_array($user_id, $client_ids_untouched)) {
				
					update_post_meta($post->ID, 'unread', 0);
				
				} ?>
				
				<div <?php if($dash_type == 'inc') { ?>	class="masonry-grid"<?php } ?>>
				
				<div <?php if($dash_type != 'inc') { ?>	style="width:100%"<?php } ?> class="cqpim-dash-item grid-item">
								
						<h3><?php _e('Ticket Details', 'cqpim'); ?></h3>
							
					<div id="ticket_container" class="cqpim-dash-item-inside">
					
						<table class="milestones">
						
							<tbody>
							
								<tr>
								
									<td><strong><?php _e('Ticket ID', 'cqpim'); ?></strong></td>
									
									<td><?php echo $post->ID; ?></td>
								
								</td>
								
								<tr>
								
									<td><strong><?php _e('Ticket Title', 'cqpim'); ?></strong></td>
									
									<td><?php echo $post->post_title; ?></td>
								
								</td>
								
								<tr>
								
									<td><strong><?php _e('Ticket Created', 'cqpim'); ?></strong></td>
									
									<td><?php echo get_the_date(get_option('cqpim_date_format') . ' H:i'); ?></td>
								
								</td>
								
								<tr>
								
									<td><strong><?php _e('Last Updated', 'cqpim'); ?></strong></td>
									
									<td><?php echo $ticket_updated; ?></td>
								
								</td>
								
								<tr>
								
									<td><strong><?php _e('Ticket Priority', 'cqpim'); ?></strong></td>
									
									<td><?php echo $priority; ?></td>
								
								</td>
								
								<tr>
								
									<td><strong><?php _e('Ticket Status', 'cqpim'); ?></strong></td>
									
									<td><?php echo $status; ?></td>
								
								</td>
							
							</tbody>
						
						</table>
					
					</div>
							
				</div>
				
				<div <?php if($dash_type != 'inc') { ?>	style="width:100%"<?php } ?> class="cqpim-dash-item grid-item">
								
						<h3><?php _e('Ticket Files', 'cqpim'); ?></h3>
							
					<div id="ticket_container" class="cqpim-dash-item-inside">
					
						<?php 
						
						$all_attached_files = get_attached_media( '', $post->ID );
						
						if(!$all_attached_files) {
						
							echo '<p>' . __('There are no files uploaded to this ticket.', 'cqpim') . '</p>';
						
						} else {
						
							echo '<table class="milestones"><thead><tr>';
							
							echo '<th>' . __('File Name', 'cqpim') . '</th><th>' . __('Actions', 'cqpim') . '</th>';
							
							echo '</tr></thead><tbody>';
						
							foreach($all_attached_files as $file) {
							
								$file_object = get_post($file->ID);
								
								$link = get_the_permalink($file->ID);
							
								$user = get_user_by( 'id', $file->post_author );
							
								echo '<tr>';
								
								echo '<td style="text-align:left"><a class="cqpim-link" href="' . $file->guid . '" download="' . $file->post_title . '">' . $file->post_title . '</a><p style="margin:0; text-align:left; padding-left:0; padding-bottom:0">' . __('Uploaded on', 'cqpim') . ' ' . $file->post_date . ' ' . __('by', 'cqpim') . ' ' . $user->display_name . '</p></td>';
								
								echo '<td><a href="' . $file->guid . '" download="' . $file->post_title . '" class="download_file" value="' . $file->ID . '"><img title="Download File" src="' .  plugin_dir_url( __FILE__ ) . 'img/download.png' . '" /></a></td>';
								
								echo '</tr>';
							
							}
							
							echo '</tbody></table>';
						
						}
						
						?>
					
					</div>
							
				</div>
				
				<div <?php if($dash_type != 'inc') { ?>	style="width:100%"<?php } ?> class="cqpim-dash-item-double grid-item">
								
					<h3><?php _e('Ticket Updates', 'cqpim'); ?></h3>
							
					<div id="ticket_container" class="cqpim-dash-item-inside">
					
						<?php

						$ticket_updates = get_post_meta($post->ID, 'ticket_updates', true);
						
						$ticket_status = get_post_meta($post->ID, 'ticket_status', true);
						
						$ticket_priority = get_post_meta($post->ID, 'ticket_priority', true);
						
						if(empty($ticket_updates)) {
							
							$ticket_updates = array();
							
						}
						
						echo '<div style="border-bottom:1px solid #ececec; max-height:900px" class="project_messages">';
						
						$ticket_updates = array_reverse($ticket_updates);
						
						foreach($ticket_updates as $update) {
						
							if($update['type'] == 'client') {
							
								$user = get_post_meta($update['user'], 'client_details', true);
								
								$email = isset($user['client_email']) ? $user['client_email'] : '';
								
								$name = isset($user['client_contact']) ? $user['client_contact'] : '';
								

							} else {
							
								$user = get_post_meta($update['user'], 'team_details', true);
								
								$email = isset($user['team_email']) ? $user['team_email'] : '';
								
								$name = isset($user['team_name']) ? $user['team_name'] : '';
							
							}
							
							$changes = isset($update['changes']) ? $update['changes'] : array();
							
							$size = 80;
							
							if(isset($update['email'])) {
							
								$email = $update['email'];
								
							}		
							
							?>
						
							<div class="project_message">
							
								<?php 
								
								$value = get_option('cqpim_disable_avatars');
								
								if(empty($value)) {
									
									$user = get_user_by('email', $email);

									$email = $user->user_email;	
									
									echo '<div class="user_avatar">';
								
										echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 
								
									echo '</div>';
								
								} 
								
								?> 
								
								<?php if(!empty($value)) { ?>
								
									<div style="padding-left:0" class="message_details">
								
								<?php } else { ?>
								
									<div class="message_details">
								
								<?php } ?>
								
									<h4>
									
										<?php 
										
										if(isset($update['name'])) {
										
											echo $update['name'];
											
										} else {
										
											echo $name; 
											
										}
										
										?>						
										
									</h4>
									
									<span>
									
										<?php 
										
										if(is_numeric($update['time'])) { $at = date(get_option('cqpim_date_format') . ' H:i', $update['time']); } else { $at = $at; }
										
										echo $at; 
									
										if($changes) {
										
											foreach($changes as $change) {
											
												echo ' | ' . $change;
											
											}
										
										}
										
										?>
									
									</span>
									
									<div class="message">
									
										<?php echo wpautop($update['details']); ?>
									
									</div>
								
								</div>
								
								<div class="clear"></div>
							
							</div>
							
							<?php			
						
						}

						echo '</div>';	
				
						$string = cqpim_random_string(10);
						
						$_SESSION['upload_ids'] = array();
							
						$_SESSION['ticket_changes'] = array();
						
						?>
						
						<div id="add_ticket_update">
						
							<h4><?php _e('Update Ticket', 'cqpim'); ?></h4>
							
								<input type="hidden" name="action" value="update_ticket" />
								
								<input type="hidden" id="post_id" name="post_id" value="<?php echo $post->ID; ?>" />
							
								<div class="tasks_left">
								
									<h4><?php _e('Ticket Status:', 'cqpim'); ?></h4>
							
									<select style="width:100%" id="ticket_status_new" name="ticket_status_new">
									
										<option value="open" <?php if($ticket_status == 'open') { echo 'selected="selected"'; } ?>><?php _e('Open', 'cqpim'); ?></option>
										
										<option value="resolved" <?php if($ticket_status == 'resolved') { echo 'selected="selected"'; } ?>><?php _e('Resolved', 'cqpim'); ?></option>
										
										<option value="hold" <?php if($ticket_status == 'hold') { echo 'selected="selected"'; } ?>><?php _e('On Hold', 'cqpim'); ?></option>
									
									</select>
												
								</div>
								
								<div class="tasks_right">			
								
									<h4><?php _e('Ticket Priority:', 'cqpim'); ?></h4>
										
									<select style="width:100%" id="ticket_priority_new" name="ticket_priority_new">
									
										<option value="low" <?php if($ticket_priority == 'low') { echo 'selected="selected"'; } ?>><?php _e('Low', 'cqpim'); ?></option>
										
										<option value="normal" <?php if($ticket_priority == 'normal') { echo 'selected="selected"'; } ?>><?php _e('Normal', 'cqpim'); ?></option>
										
										<option value="high" <?php if($ticket_priority == 'high') { echo 'selected="selected"'; } ?>><?php _e('High', 'cqpim'); ?></option>
										
										<option value="immediate" <?php if($ticket_priority == 'immediate') { echo 'selected="selected"'; } ?>><?php _e('Immediate', 'cqpim'); ?></option>
									
									</select>
											
								</div>
								
								<div class="clear"></div>
								
								<div>
								
									<h4><?php _e('Upload Files', 'cqpim'); ?></h4>
									
									<form style="width:200px" id="upload" method="post" enctype="multipart/form-data">
									
										<div id="drop">
											
											<?php _e('Drop Here', 'cqpim'); ?>

											<a><?php _e('Browse', 'cqpim'); ?></a>
											
											<input type="file" name="upl" multiple />
											
										</div>
										
										<ul>
											
										</ul>
									
									</form>
									
								</div>
								
								<div class="clear"></div>
								
								<h4><?php _e('Message', 'cqpim'); ?></h4>
								
								<textarea style="width:100%; height:300px" id="ticket_update_new" required ></textarea>
								
								<div class="clear"></div>
								
								<input id="update_support" type="submit" class="metabox-add-button" style="margin-right:0" value="<?php _e('Update Ticket', 'cqpim'); ?>" />

								<br /><br />
						
						</div>				
					
					</div>
					
				</div>
				
				</div>
			
			<?php } else { ?>
			
				<?php if($dash_type != 'inc') { ?>
				
					<h1>Access Denied</h1>
					
				<?php } ?>
			
			<?php } ?>
			
		</div><!-- #content -->
		
	</div>
	
<?php  

	get_footer();
	
 ?>
