<?php 

	get_header();
	
 ?>

	<div id="content" role="main">	 
		
		<div class="cqpim-login">
		
			<div style=" border-radius:5px; -moz-border-radius:5px" class="cqpim-project-box">
				
				<div style="padding:20px;border-radius:5px; -moz-border-radius:5px" class="content">
				
					<h3><?php echo $post->post_title; ?></h3>
					
					<?php if(empty($_GET['h'])) { ?>
				
						<form id="cqpim-reset-pass">
				
							<?php wp_nonce_field('ajax-register-nonce', 'signonsecurity'); ?> 
						
							<input type="text" id="username" placeholder="<?php _e('Email Address', 'cqpim'); ?>" />
							
							<br /><br /><br />
							
							<input type="submit" class="cqpim-dash-button right" value="<?php _e('Reset Password', 'cqpim'); ?>" />
						
						</form>
						
						<?php $reset = get_option('cqpim_login_page'); 
						
						if(!empty($reset)) { ?>
						
							<a href="<?php echo get_the_permalink($reset); ?>" style="float:left" id="forgot" class="cqpim-dash-button"><?php _e('Back to Login', 'cqpim'); ?></a>
						
						<?php } ?>
					
					<?php } else { ?>
					
						<form id="reset_pass_conf">
					
							<?php wp_nonce_field('ajax-register-nonce', 'signonsecurity'); ?> 
							
							<input type="hidden" id="hash" value="<?php echo $_GET['h']; ?>" />
						
							<input type="password" id="password" placeholder="<?php _e('New Password', 'cqpim'); ?>" />
							
							<br /><br />
							
							<input type="password" id="password2" placeholder="<?php _e('Repeat Password', 'cqpim'); ?>" />
							
							<br /><br /><br />
							
							<input type="submit" class="cqpim-dash-button right" value="<?php _e('Reset Password', 'cqpim'); ?>" />
						
						</form>
						
						<?php $reset = get_option('cqpim_login_page'); 
						
						if(!empty($reset)) { ?>
						
							<a href="<?php echo get_the_permalink($reset); ?>" style="float:left" id="forgot" class="cqpim-dash-button"><?php _e('Back to Login', 'cqpim'); ?></a>
						
						<?php } ?>					
					
					<?php } ?>
					
					<div class="clear"></div>
					
					<br />
					
					<?php include_once(ABSPATH.'wp-admin/includes/plugin.php');
					
					if(is_plugin_active('cqpim-envato/cqpim-envato.php')) {
					
						$register_page = get_option('cqpim_envato_register_page'); ?>
						
						<br />
						
						<p><?php _e('Envato Buyer? Need to Register?', 'cqpim'); ?> <a class="cqpim-link" href="<?php echo get_the_permalink($register_page); ?>"><?php _e('Register Here', 'cqpim'); ?></a></p>
						
					<?php } ?>
					
					<div id="login_messages" style="display:none"></div>
				
				</div>
			
			</div>
		
		</div>
		
	</div>

<?php get_footer();
	
 ?>
	


