<?php 

if (!current_user_can( 'cqpim_client' )) {

	$login_page = get_option('cqpim_login_page');
	
	$url = get_the_permalink($login_page);
	
	wp_redirect($url, 302);

} else {

	$user = wp_get_current_user(); 
	
	$login_page_id = get_option('cqpim_login_page');
	
	$login_url = get_the_permalink($login_page_id);
	
	$user_id = $user->ID;
	
	$dash_type = get_option('client_dashboard_type');
	
	$quote_form = get_option('cqpim_backend_form');
	
	// Assignment
	
	$args = array(
	
		'post_type' => 'cqpim_client',
		
		'posts_per_page' => -1,
		
		'post_status' => 'private'
	
	);
	
	$members = get_posts($args);
	
	foreach($members as $member) {
	
		$team_details = get_post_meta($member->ID, 'client_details', true);
		
		if($team_details['user_id'] == $user->ID) {
		
			$assigned = $member->ID;
			
			$client_type = 'admin';
		
		}
	
	} 
	
	if(empty($assigned)) {
	
		foreach($members as $member) {
		
			$team_ids = get_post_meta($member->ID, 'client_ids', true);
			
			if(!is_array($team_ids)) {
			
				$team_ids = array($team_ids);
			
			}
			
			if(in_array($user->ID, $team_ids)) {
			
				$assigned = $member->ID;
				
				$client_type = 'contact';
			
			}
		
		} 			
	
	}
	
	$client_details = get_post_meta($assigned, 'client_details', true);
	
	$client_ids = get_post_meta($assigned, 'client_ids', true);
	
	$login_url = get_option('cqpim_logout_url');
	
	if(empty($login_url)) {
	
		$login_url = get_the_permalink($login_page_id);
		
	}
	
	$user = wp_get_current_user();

	$client_logs = get_post_meta($assigned, 'client_logs', true);

	if(empty($client_logs)) {

		$client_logs = array();
		
	}

	$now = time();

	$client_logs[$now] = array(

		'user' => $user->ID,
		
		'page' => sprintf(__('Project %1$s - %2$s (Contract Page)', 'cqpim'), get_the_ID(), $title)

	);
	
	update_post_meta($assigned, 'client_logs', $client_logs);
	
}

?>

<?php 

	get_header();
	
 ?>

	<div style="padding-left:0; margin:0 auto; max-width:1100px; padding:20px; background:#fff" class="cqpim-client-dash" role="main">	
	
		<div style="background:0; padding:0 0 30px 0" class="cqpim-dash-header">
		
			<?php 
			
			$value = get_option('cqpim_disable_avatars');
			
			$user = wp_get_current_user();
			
			if(empty($value)) {
				
				echo '<div class="user_avatar" style="float:left; margin-right:20px">';
			
					echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 
			
				echo '</div>';
			
			} 
			
			?> 

			<h1><?php printf(__('Welcome back, %1$s', 'cqpim'), $user->display_name); ?><br /><span style="font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $user->user_email; ?></span> <span style="margin-left:10px;font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $client_details['client_company']; ?> <?php if($client_type == 'admin') { echo '&nbsp;&nbsp;(Main Contact)'; } ?></span></h1>
		
			<div class="clear"></div>
			
				<div>
			
				<?php $client_dash = get_option('cqpim_client_page'); ?>
			
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash); ?>"><?php _e('Dashboard', 'cqpim'); ?></a>
		
				<?php if(get_option('cqpim_messages_allow_client') == 1) { ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=messages'; ?>"><?php _e('Messages', 'cqpim'); ?></a>
				
				<?php } ?>				
				
				
				<?php if(!empty($quote_form)) { ?>
				
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=quote_form'; ?>"><?php _e('Request a Quote', 'cqpim'); ?></a>
				
				<?php } 
			
				$client_settings = get_option('allow_client_settings');
				
				if($client_settings == 1) { ?>				
					
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=settings'; ?>"><?php _e('Settings', 'cqpim'); ?></a>

				<?php } 
				
				$client_settings = get_option('allow_client_users');
				
				if($client_settings == 1) { ?>	

					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=contacts'; ?>"><?php _e('Users', 'cqpim'); ?></a>

				<?php } ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo wp_logout_url($login_url); ?>"><?php _e('Log Out', 'cqpim'); ?></a>
				
			</div>
			
			<div class="clear"></div>
			
			<div>
						
			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($post->ID) . '/?page=summary&sub=updates'; ?>"><?php _e('Updates & Progress', 'cqpim'); ?></a>

			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($post->ID) . '/?page=summary&sub=milestones'; ?>"><?php _e('Milestones & Tasks', 'cqpim'); ?></a>
			
			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($post->ID) . '/?page=summary&sub=messages'; ?>"><?php _e('Messages', 'cqpim'); ?></a>

			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($post->ID) . '/?page=summary&sub=files'; ?>"><?php _e('Files', 'cqpim'); ?></a>
			
			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($post->ID) . '/?page=summary&sub=invoices'; ?>"><?php _e('Costs & Invoices', 'cqpim'); ?></a>			
			
			<div class="clear"></div>
			
			</div>
				
				<div class="clear"></div>

		</div>
		
		<div style="padding:0;" class="cqpim-dash-quote">
		
		<?php

		$user = wp_get_current_user(); 
				
		$user_id = $user->ID;

		$logo = get_option('company_logo');
				
		$logo_url = isset($logo['company_logo']) ? $logo['company_logo'] : '';

		$title = get_the_title();
				
		$title = str_replace('Private: ', '', $title);

		$company_name = get_option('company_name');

		$company_address = get_option('company_address');

		$company_postcode = get_option('company_postcode');

		$contract_text = get_option('default_contract_text');

		$contract_text = cqpim_replacement_patterns($contract_text, $post->ID, 'project');

		$currency = get_option('currency_symbol');
				
		$vat = get_post_meta($post->ID, 'tax_applicable', true);
		
		if(!empty($vat)) {
		
			$vat = get_post_meta($post->ID, 'tax_rate', true);
		
		}

		$tax_name = get_option('sales_tax_name');
				
		if(!empty($vat)) {
				
			$vat_string = '';
					
		} else {
				
			$vat_string = '';
					
		}

		$project_details = get_post_meta($post->ID, 'project_details', true);

		$project_elements = get_post_meta($post->ID, 'project_elements', true);

		$type = isset($project_details['quote_type']) ? $project_details['quote_type'] : '';
				
		$upper_type = ucfirst($type);

		$quote_id = isset($project_details['quote_id']) ? $project_details['quote_id'] : '';

		$quote_details = get_post_meta($quote_id, 'quote_details', true);

		$project_summary = isset($project_details['project_summary']) ? $project_details['project_summary'] : '';

		$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';

		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$project_client_ids = get_post_meta($client_id, 'client_ids', true);
		
		if(empty($project_client_ids)) {
			
			$project_client_ids = array();
			
		}

		$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';

		$client_terms = isset($client_details['invoice_terms']) ? $client_details['invoice_terms'] : '';

		$client_address = isset($client_details['client_address']) ? $client_details['client_address'] : '';

		$client_postcode = isset($client_details['client_postcode']) ? $client_details['client_postcode'] : '';

		$client_company_name = isset($client_details['client_company']) ? $client_details['client_company'] : '';

		$start_date = isset($project_details['start_date']) ? $project_details['start_date'] : '';

		$finish_date = isset($project_details['finish_date']) ? $project_details['finish_date'] : '';

		$deposit = isset($project_details['deposit_amount']) ? $project_details['deposit_amount'] : '';

		if($client_terms) {
					
			$invoice_terms = $client_terms;
						
		} else {
					
			$invoice_terms = get_option('company_invoice_terms');
						
		}
				
		?>

		<?php if(current_user_can( 'cqpim_view_project_contract' ) OR $client_user_id == $user_id  OR in_array($user->ID, $project_client_ids)) { ?>
				
			<div class="quote_logo">
				
				<img src="<?php echo $logo['company_logo']; ?>" />
				
			</div>
				
			<div class="quote_contacts">
				
			<?php echo get_option('company_name'); ?><br />
					
			<?php _e('Tel:', 'cqpim'); ?><?php echo get_option('company_telephone'); ?><br />
					
			<?php _e('Email:', 'cqpim'); ?> <a href="<?php echo get_option('company_sales_email'); ?>"><?php echo get_option('company_sales_email'); ?></a>
				
		</div>
				
		<div class="clear"></div>
		
		<a class="btn-printable" href="<?php echo get_the_permalink(); ?>?page=contract-print" target="_blank"><?php _e('VIEW PRINTABLE VERSION', 'cqpim'); ?></a>
		
		<div class="clear"></div>
		
		<br /><br />

		<h1><?php echo $title; ?></h1>

		<div class="contract-specifics">

			<h1><?php _e('CONTRACT DOCUMENTATION', 'cqpim'); ?></h1>

			<p><strong><?php _e('This is an agreement between "us"', 'cqpim'); ?></strong></p>
			
			<p><?php echo $company_name; ?></p>
			
			<p><?php echo $company_address; ?> <?php echo $company_postcode; ?></p>
			
			<p><strong><?php _e('and "you"', 'cqpim'); ?></strong></p>
			
			<p><?php echo $client_company_name; ?></p>
			
			<p><?php echo $client_address; ?> <?php echo $client_postcode; ?></p>

		</div>

		<h1><?php _e('ABOUT THE PROJECT', 'cqpim'); ?></h1>

		<?php
				
		if($project_summary) {
				
			echo '<h2>' . __('Summary', 'cqpim') . '</h2>';
					
			echo wpautop($project_summary);
				
		}
				
		?>

		<?php 
				
		if($start_date || $finish_date) {
				
			echo '<h2>' . __('Project Dates', 'cqpim') . '</h2>';
				
		}
				
		if($start_date) {
		
			if(is_numeric($start_date)) { $start_date = date(get_option('cqpim_date_format'), $start_date); } else { $start_date = $start_date; }
				
			echo '<p>' . __('Start Date', 'cqpim') . ' - ' . $start_date . '</p>';
					
		}
				
		if($finish_date) {
		
			if(is_numeric($finish_date)) { $finish_date = date(get_option('cqpim_date_format'), $finish_date); } else { $finish_date = $finish_date; }
				
			echo '<p>' . __('Completion/Launch Date', 'cqpim') . ' - ' . $finish_date . '</p>';
					
		}
				
		?>

		<h2><?php _e('Milestones', 'cqpim'); ?></h2>
				
		<?php
		
		if(!empty($project_elements)) {
		
			$msordered = array();
			
			$i = 0;
			
			$mi = 0;
			
			foreach($project_elements as $key => $element) {
			
				$weight = isset($element['weight']) ? $element['weight'] : $mi;
			
				$msordered[$weight] = $element;
				
				$mi++;
			
			}
			
			ksort($msordered);
		
			foreach($msordered as $element) { ?>
			
				<div class="quote_milestone">
			
					<h3><?php echo $element['title']; ?></h3>
					
					<div class="quote_milestone_content">
					
						<?php
						
						$args = array(
						
							'post_type' => 'cqpim_tasks',
							
							'posts_per_page' => -1,
							
							'meta_key' => 'milestone_id',
							
							'meta_value' => $element['id'],
							
							'orderby' => 'date',
							
							'order' => 'ASC'
							
						);
						
						$tasks = get_posts($args);
						
						if($tasks) {
						
							$ti = 0;
							
							$ordered = array();
							
							$wi = 0;
							
							foreach($tasks as $task) {
							
								$task_details = get_post_meta($task->ID, 'task_details', true);
								
								$weight = isset($task_details['weight']) ? $task_details['weight'] : $wi;
								
								if(empty($task->post_parent)) {
								
									$ordered[$weight] = $task;
								
								}
								
								$wi++;
							
							}
							
							ksort($ordered);
						
							echo '<ul>';
						
							foreach($ordered as $task) {
							
								$task_details = get_post_meta($task->ID, 'task_details', true);
							
								if(!empty($task_details['task_est_time'])) {
								
									$time = '(' . $task_details['task_est_time'] . ' ' . __('hours', 'cqpim') . ')';
									
								} else {
								
									$time = '';
								
								}
							
								echo '<li>' . $task->post_title . ' ' . $time;
								
								$ti++;
								
								$args = array(
								
									'post_type' => 'cqpim_tasks',
									
									'posts_per_page' => -1,
									
									'meta_key' => 'milestone_id',
									
									'meta_value' => $element['id'],
									
									'post_parent' => $task->ID,
									
									'orderby' => 'date',
									
									'order' => 'ASC'
									
								);
								
								$subtasks = get_posts($args);
								
								if(!empty($subtasks)) {
									
									echo '<ul style="padding-left:20px">';
								
									$subordered = array();
									
									$sti = 0;
									
									$ssti = 0;
									
									foreach($subtasks as $subtask) {
									
										$task_details = get_post_meta($subtask->ID, 'task_details', true);
										
										$weight = isset($task_details['weight']) ? $task_details['weight'] : $sti;
										
										$subordered[$weight] = $subtask;
										
										$sti++;
									
									}
									
									ksort($subordered);
									
									foreach($subordered as $subtask) {
										
										$task_details = get_post_meta($subtask->ID, 'task_details', true);
									
										if(!empty($task_details['task_est_time'])) {
										
											$time = '(' . $task_details['task_est_time'] . ' ' . __('hours', 'cqpim') . ')';
											
										} else {
										
											$time = '';
										
										}
									
										echo '<li>' . $subtask->post_title . ' ' . $time .'</li>';												

										$ssti++;

									}
									
									echo '</ul>';
								
								}
								
								echo  '</li>';
							
							}
							
							echo '</ul>';
						
						}
						
						?>
					
					</div>
					
					<ul class="quote_milestone_additional">
					
						<?php
						
						if($element['start']) {
						
							if(is_numeric($element['start'])) { $element['start'] = date(get_option('cqpim_date_format'), $element['start']); } else { $element['start'] = $element['start']; }
						
							echo '<li><strong>' . __('Start Date', 'cqpim') . ': </strong> ' . $element['start'] . '</li>';
							
						} 
						
						if($element['deadline']) {
						
							if(is_numeric($element['deadline'])) { $element['deadline'] = date(get_option('cqpim_date_format'), $element['deadline']); } else { $element['deadline'] = $element['deadline']; }
							
							echo '<li><strong>' . __('Deadline', 'cqpim') . ': </strong> ' . $element['deadline'] . '</li>';											
							
						}
						
						
						if($element['cost']) {
						
							if($type == 'estimate') {
							
								$cost = preg_replace("/[^\\d.]+/","", $element['cost']);
							
								echo '<li><strong>' . __('Estimated Cost', 'cqpim') . ': </strong> ' . cqpim_calculate_currency($post->ID, $cost) . '</li>';											
							
							} else {
							
								$cost = preg_replace("/[^\\d.]+/","", $element['cost']);
							
								echo '<li><strong>' . __('Cost', 'cqpim') . ':</strong> ' . cqpim_calculate_currency($post->ID, $cost) . '</li>';																				
							
							}
						
						}
						
						?>
					
					</ul>
				
				</div>
			
			<?php $i++; }
			
			}
		
		?>
		
		<h2><?php _e('Cost Breakdown', 'cqpim'); ?></h2>
		
		<?php
		
		if($project_elements) {
			
			echo '<table class="quote_summary milestones"><thead><tr>';
			
			echo '<th>' . __('Milestone', 'cqpim') . '</th>';
			
			if($type == 'estimate') {
				
				echo '<th>' . __('Estimated Cost', 'cqpim') . '</th>';
			
			} else {
			
				echo '<th>' . __('Cost', 'cqpim') . '</th>';
				
			}
			
			echo '</tr></thead>';
			
			echo '<tbody>';
			
			$subtotal = 0;
			
			foreach($msordered as $key => $element) {
			
				$cost = preg_replace("/[^\\d.]+/","", $element['cost']);
			
				$subtotal = $subtotal + $cost;
			
				echo '<tr><td class="qtitle">' . $element['title'] . '</td>';
				
				echo '<td class="qcost">' . cqpim_calculate_currency($post->ID, $cost) . '</td></tr>';
			
			}
			
			$project_details = get_post_meta($post->ID, 'project_details', true);
			
			$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
			
			$client_details = get_post_meta($client_id, 'client_details', true);
			
			$client_tax = isset($client_details['tax_disabled']) ? $client_details['tax_disabled'] : '';
			
			$client_stax = isset($client_details['stax_disabled']) ? $client_details['stax_disabled'] : '';	
			
			if(!empty($vat) && empty($client_tax)) {
			
				$stax_rate = get_option('secondary_sales_tax_rate');
			
				$total_vat = $subtotal / 100 * $vat;
				
				$total_stax = $subtotal / 100 * $stax_rate;
				
				if(!empty($stax_rate) && empty($client_stax)) {
				
					$total = $subtotal + $total_vat + $total_stax;
				
				} else {
				
					$total = $subtotal + $total_vat;
					
				}
				
				$tax_name = get_option('sales_tax_name');
				
				$stax_name = get_option('secondary_sales_tax_name');
				
				$span = '';
				
				echo '<tr><td '.$span.' align="right" class="align-right"><strong>' . __('Subtotal', 'cqpim') . ': </strong></td><td class="subtotal">' . cqpim_calculate_currency($post->ID, $subtotal) . '</td></tr>';
				
				echo '<tr><td '.$span.' align="right" class="align-right"><strong>' . $tax_name . ': </strong></td><td class="subtotal">' . cqpim_calculate_currency($post->ID, $total_vat) . '</td></tr>';
				
				if(!empty($stax_rate) && empty($client_stax)) {
				
					echo '<tr><td '.$span.' align="right" class="align-right"><strong>' . $stax_name . ': </strong></td><td class="subtotal">' . cqpim_calculate_currency($post->ID, $total_stax) . '</td></tr>';
				
				}

				echo '<tr><td '.$span.' align="right" class="align-right"><strong>' . __('TOTAL', 'cqpim') . ': </strong></td><td class="subtotal">' . cqpim_calculate_currency($post->ID, $total) . '</td></tr>';
				
			} else {
			
				$span = '';
			
				echo '<tr><td '.$span.' align="right" class="align-right"><strong>' . __('TOTAL', 'cqpim') . ': </strong></td><td class="subtotal">' . cqpim_calculate_currency($post->ID, $subtotal) . '</td></tr>';	
			
			}
			
			echo '</tbody></table>'; 
			
		}
		
		if($type == 'estimate') { ?>
		
		<br />
		
		<h4><strong><?php _e('NOTE:', 'cqpim'); ?> </strong><?php _e('THIS IS AN ESTIMATE, SO THESE PRICES MAY NOT REFLECT THE FINAL PROJECT COST.', 'cqpim'); ?></h4>
		
		<?php } ?>
		
		<h2><?php _e('Payment Plan', 'cqpim'); ?></h2>
		
		<p><strong><?php _e('Deposit', 'cqpim'); ?></strong></p>
		
		<?php
		
		if($deposit == 'none') {
		
			echo '<p>' . __('We do not require an up-front deposit payment on this project. The full balance will be due on completion.', 'cqpim') . '</p>';
			
		} else {
		
			if(empty($subtotal)) {
			
				$subtotal = 0;
			
			}
		
			$deposit_amount = $subtotal / 100 * $deposit;
			
			echo '<p>';
			
			printf(__('We require an initial deposit payment of %1$s percent on this project which will be invoiced on acceptance.', 'cqpim'), $deposit);

			echo '</p>';
		}	
			
		?>
				
		<br />

		<h1><?php _e('TERMS &amp; CONDITIONS', 'cqpim'); ?></h1>

		<?php 
		
		$contract = isset($project_details['default_contract_text']) ? $project_details['default_contract_text'] : '';
		
		$terms = get_post_meta($post->ID, 'terms', true);
		
		if(empty($terms)) {
		
			if(empty($contract)) {
			
				$text = get_post_meta($contract_text, 'terms', true);
				
				$text = cqpim_replacement_patterns($text, $post->ID, 'project');
				
				echo wpautop($text);
				
			} else {
			
				$text = get_post_meta($contract, 'terms', true);
				
				$text = cqpim_replacement_patterns($text, $post->ID, 'project');
				
				echo wpautop($text);			
			
			}
		
		} else {
			
			echo wpautop($terms);
			
		}
		
		?>

		<hr />

		<div id="acceptance">
				
			<?php
						
			$is_confirmed = isset( $project_details['confirmed'] ) ? $project_details['confirmed'] : '';
						
			if(!$is_confirmed) { ?>
						
			<h1><?php _e('CONTRACT ACCEPTANCE', 'cqpim'); ?></h1>
						
			<?php echo wpautop(get_option('contract_acceptance_text')); ?>
										
			<div style="padding:40px" class="quote_acceptance">
						
				<form id="submit-quote-conf">
							
					<input type="hidden" id="project_id" value="<?php the_ID(); ?>" />
								
					<input type="hidden" id="pm_name" value="<?php echo get_the_author_meta( 'display_name' ); ?>" />
								
					<input type="text" id="conf_name" name="conf_name" placeholder="<?php _e('Enter your name', 'cqpim'); ?>" required />
								
					<input type="submit" id="accept_contract" value="<?php _e('Confirm Contract', 'cqpim'); ?>" />
								
					<div class="ajax_spinner" style="display: none;"></div>
								
					<div id="messages"></div>
							
				</form>	

			</div>
						
			<?php } else { 
						
				$conf_by = isset($project_details['confirmed_details']['by']) ? $project_details['confirmed_details']['by'] : '';
							
				$conf_date = isset($project_details['confirmed_details']['date']) ? $project_details['confirmed_details']['date'] : '';
				
				if(is_numeric($conf_date)) { $conf_date = date(get_option('cqpim_date_format') . ' H:i', $conf_date); } else { $conf_date = $conf_date; }
							
				$conf_ip = isset($project_details['confirmed_details']['ip']) ? $project_details['confirmed_details']['ip'] : '';				
						
				?>
							
				<div class="contract-confirmed">
							
					<h5><?php _e('THIS CONTRACT HAS BEEN CONFIRMED', 'cqpim'); ?></h5>
					
					<br />
					
					<p><?php printf(__('Confirmed by %1$s @ %2$s from IP Address %3$s', 'cqpim'), $conf_by, $conf_date, $conf_ip); ?></p>
									
				</div>
						
			<?php } ?>
				
		</div>
		
		<br /><br />
		
		<a class="btn-printable" href="<?php echo get_the_permalink(); ?>?page=contract-print" target="_blank"><?php _e('VIEW PRINTABLE VERSION', 'cqpim'); ?></a>
		
		<div class="clear"></div>

		<?php } else { ?>

		<h1><?php _e('Access Denied', 'cqpim'); ?></h1>

		<?php } ?>

	</div><!-- #content -->
	
	</div>

<?php 

	get_footer();
	
 ?>
 
 <?php exit; ?>



