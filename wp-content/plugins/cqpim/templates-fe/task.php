<?php 

if (!current_user_can( 'cqpim_client' )) {

	$login_page = get_option('cqpim_login_page');
	
	$server = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : '';
	
	$protocol = $server == 'on' ? 'https' : 'http';
	
	$request_url = $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	
	$site_url = site_url();
	
	$redirect = str_replace($site_url, '', $request_url);
	
	$url = get_the_permalink($login_page) . '?redirect=' . $redirect;
	
	wp_redirect($url, 302);

} else {

	include_once( realpath(dirname(__FILE__)) . '/upload.php' );

	$user = wp_get_current_user(); 
	
	$login_page_id = get_option('cqpim_login_page');
	
	$login_url = get_the_permalink($login_page_id);
	
	$user_id = $user->ID;
	
	$dash_type = get_option('client_dashboard_type');
	
	$quote_form = get_option('cqpim_backend_form');
	
	// Assignment
	
	$args = array(
	
		'post_type' => 'cqpim_client',
		
		'posts_per_page' => -1,
		
		'post_status' => 'private'
	
	);
	
	$members = get_posts($args);
	
	foreach($members as $member) {
	
		$team_details = get_post_meta($member->ID, 'client_details', true);
		
		if($team_details['user_id'] == $user->ID) {
		
			$assigned = $member->ID;
			
			$client_type = 'admin';
		
		}
	
	} 
	
	if(empty($assigned)) {
	
		foreach($members as $member) {
		
			$team_ids = get_post_meta($member->ID, 'client_ids', true);
			
			if(!is_array($team_ids)) {
			
				$team_ids = array($team_ids);
			
			}
			
			if(in_array($user->ID, $team_ids)) {
			
				$assigned = $member->ID;
				
				$client_type = 'contact';
			
			}
		
		} 			
	
	}
	
	$client_details = get_post_meta($assigned, 'client_details', true);
	
	$client_ids = get_post_meta($assigned, 'client_ids', true);
	
	$ppid = get_post_meta($post->ID, 'project_id', true); 
	
	$project_details = get_post_meta($ppid, 'project_details', true);
	
	$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
	
	$looper = get_post_meta($post->ID, 'looper', true);
	
	if(time() - $looper > 5) {
	
		$user = wp_get_current_user();

		$client_logs = get_post_meta($client_id, 'client_logs', true);

		if(empty($client_logs)) {

			$client_logs = array();
			
		}

		$now = time();
		
		$title = get_the_title();
		
		$title = str_replace('Protected:', '', $title);

		$client_logs[$now] = array(

			'user' => $user->ID,
			
			'page' => sprintf(__('Task - %1$s', 'cqpim'), $title)

		);
		
		update_post_meta($client_id, 'client_logs', $client_logs);
		
		update_post_meta($post->ID, 'looper', time());
	
	}
	
}


	get_header();
	
 ?>	

	<div style="padding-left:0; margin:0 auto; max-width:1100px; padding:20px; background:#fff" class="cqpim-client-dash tasks-fe" role="main">	
	
		<div style="background:0; padding:0 0 30px 0" class="cqpim-dash-header">
		
			<?php 
			
			$value = get_option('cqpim_disable_avatars');
			
			$user = wp_get_current_user();
			
			if(empty($value)) {
				
				echo '<div class="user_avatar" style="float:left; margin-right:20px">';
			
					echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 
			
				echo '</div>';
			
			} 
			
			?> 

			<h1><?php printf(__('Welcome back, %1$s', 'cqpim'), $user->display_name); ?><br /><span style="font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $user->user_email; ?></span> <span style="margin-left:10px;font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $client_details['client_company']; ?> <?php if($client_type == 'admin') { echo '&nbsp;&nbsp;(Main Contact)'; } ?></span></h1>
		
			<div class="clear"></div>
			
			<div>
			
				<?php $client_dash = get_option('cqpim_client_page'); ?>
			
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash); ?>"><?php _e('Dashboard', 'cqpim'); ?></a>
		
				<?php if(get_option('cqpim_messages_allow_client') == 1) { ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=messages'; ?>"><?php _e('Messages', 'cqpim'); ?></a>
				
				<?php } ?>			
				
				<?php if(!empty($quote_form)) { ?>
				
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=quote_form'; ?>"><?php _e('Request a Quote', 'cqpim'); ?></a>
				
				<?php } 
			
				$client_settings = get_option('allow_client_settings');
				
				if($client_settings == 1) { ?>				
					
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=settings'; ?>"><?php _e('Settings', 'cqpim'); ?></a>

				<?php } 
				
				$client_settings = get_option('allow_client_users');
				
				if($client_settings == 1) { ?>	

					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=contacts'; ?>"><?php _e('Users', 'cqpim'); ?></a>

				<?php } ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo wp_logout_url($login_url); ?>"><?php _e('Log Out', 'cqpim'); ?></a>
				
				<div class="clear"></div>
			
			</div>
			
			<div class="clear"></div>
			
			<div>
			
			<?php if(!empty($ppid)) { ?>
						
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '/?page=summary&sub=updates'; ?>"><?php _e('Updates & Progress', 'cqpim'); ?></a>

				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '/?page=summary&sub=milestones'; ?>"><?php _e('Milestones & Tasks', 'cqpim'); ?></a>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '/?page=summary&sub=messages'; ?>"><?php _e('Messages', 'cqpim'); ?></a>

				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '/?page=summary&sub=files'; ?>"><?php _e('Files', 'cqpim'); ?></a>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '/?page=summary&sub=invoices'; ?>"><?php _e('Costs & Invoices', 'cqpim'); ?></a>			
				
				<div class="clear"></div>
			
			<?php } ?>
			
			</div>
				
				<div class="clear"></div>

		</div>
		
		<div style="padding:0;" class="cqpim-dash-content">
		
			<?php

				if($assigned == $client_id) { ?>
				
				<div class="">
				
					<div class="cqpim-dash-item grid-item task_details">
					
						<h3><?php _e('Task Details', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">
						
							<?php

							$pid = get_post_meta($post->ID, 'project_id', true);
							
							$mid = get_post_meta($post->ID, 'milestone_id', true);
							
							$owner = get_post_meta($post->ID, 'owner', true);
							
							$client_check = preg_replace('/[0-9]+/', '', $owner);
							
							if($client_check == 'C') {
							
								$client = true;
								
							} else {
								
								$client = false;
								
							}
							
							if($owner) {
							
								if($client == true) {
								
									$id = preg_replace("/[^0-9,.]/", "", $owner);
									
									$client_object = get_user_by('id', $id);
									
									$task_owner = $client_object->display_name;
								
								} else {
							
									$team_details = get_post_meta($owner, 'team_details', true);
									
									$team_name = isset($team_details['team_name']) ? $team_details['team_name']: '';
									
									if(!empty($team_name)) {
									
										$task_owner = $team_name;
									
									}
								
								}
								
							} else {
							
								$task_owner = '';
								
							}
							
							$task_details = get_post_meta($post->ID, 'task_details', true);
							
							$task_watchers = get_post_meta($post->ID, 'task_watchers', true);
							
							$task_description = isset($task_details['task_description']) ? $task_details['task_description'] : '';
							
							$task_status = isset($task_details['status']) ? $task_details['status'] : '';
							
							$task_priority = isset($task_details['task_priority']) ? $task_details['task_priority'] : '';
							
							$task_start = isset($task_details['task_start']) ? $task_details['task_start'] : '';
							
							$task_deadline = isset($task_details['deadline']) ? $task_details['deadline'] : '';
							
							if(is_numeric($task_start)) { $task_start = date(get_option('cqpim_date_format'), $task_start); } else { $task_start = $task_start; }
							
							if(is_numeric($task_deadline)) { $task_deadline = date(get_option('cqpim_date_format'), $task_deadline); } else { $task_deadline = $task_deadline; }
							
							$task_est_time = isset($task_details['task_est_time']) ? $task_details['task_est_time'] : '';
							
							$task_pc = isset($task_details['task_pc']) ? $task_details['task_pc'] : '';
								
							echo '<p><strong>' . __('Description', 'cqpim') . ':</strong></p>';
							
							echo wpautop($task_description); 
							
							?>
							
							<div class="">
							
								<p><strong>
								
								<?php 
								
								_e('Assigned To', 'cqpim');
								
								echo ':</strong> '							
								
								?>								

								<select id="task_owner" name="task_owner">
									
									<?php 
									
										$contribs = get_post_meta($pid, 'project_contributors', true);
											
										if(!empty($contribs)) { ?>
										
											<optgroup label="<?php _e('Team Members', 'cqpim'); ?>">
										
											<?php foreach($contribs as $contrib) {
											
												$team_details = get_post_meta($contrib['team_id'], 'team_details', true);
											
												if($owner == $contrib['team_id']) { $selected = 'selected="selected"'; } else { $selected = ''; }
											
												echo '<option value="' . $contrib['team_id'] . '" ' . $selected . '>' . $team_details['team_name'] . '</option>';
											
											} ?>
											
											</optgroup>
										
										<?php }
									
									?>
									
									<optgroup label="<?php _e('Client', 'cqpim'); ?>">
									
										<?php foreach($client_ids as $id) { ?>
										
											<?php 
											
											$client = get_user_by('id', $id); 
											
											if($owner == 'C' . $client->ID) { $selected = 'selected="selected"'; } else { $selected = ''; }
											
											?>
											
											<option value="C<?php echo $client->ID; ?>" <?php echo $selected; ?>><?php echo $client->display_name; ?></option>
										
										<?php } ?>
									
									</optgroup>
								
								</select>	

								</p>
							
							</div>
							
							<div class="clear"></div>
							
							<div class="">
							
								<p><strong>
								
								<?php 
								
								_e('Task Status', 'cqpim'); 
								
								if($task_status == 'pending') { $task_status = __('Pending', 'cqpim'); } 
								
								if($task_status == 'progress') { $task_status = __('In Progress', 'cqpim'); } 
								
								if($task_status == 'complete') { $task_status = __('Complete', 'cqpim'); } 
								
								if($task_status == 'on_hold') { $task_status = __('On Hold', 'cqpim'); } 
								
								echo ':</strong> ' . ucwords($task_status);								
								
								?>
																
								</p>

							
							</div>
							
							<div class="">
							
								<p><strong>
								
								<?php 
								
								_e('Task Priority', 'cqpim');  
								
								if($task_priority == 'normal') { $task_priority = __('Normal', 'cqpim'); } 
								
								if($task_priority == 'low') { $task_priority = __('Low', 'cqpim'); } 
								
								if($task_priority == 'high') { $task_priority = __('High', 'cqpim'); } 
								
								if($task_priority == 'immediate') { $task_priority = __('Immediate', 'cqpim'); } 
								
								echo ':</strong> ' . ucwords($task_priority);									
								
								?>
																
								</p>
							
							</div>
							
							<div class="clear"></div>
							
							<div class="">
							
								<p><strong>
								
								<?php 
								
								_e('Start Date', 'cqpim');  
								
								echo ':</strong> ' . ucwords($task_start);								
								
								?>
																
								</p>
							
							</div>
							
							<div class="">
							
								<p><strong>
								
								<?php 
								
								_e('Deadline', 'cqpim');  
								
								echo ':</strong> ' . ucwords($task_deadline);								
								
								?>
																
								</p>
							
							</div>
							
							<div class="clear"></div>
							
							<div class="">
							
								<p><strong>
								
								<?php 
								
								_e('Estimated Time (Hours)', 'cqpim'); 
								
								echo ':</strong> ' . ucwords($task_est_time);								
								
								?>
																
								</p>
							
							</div>
							
							<div class="">
							
								<p><strong>
								
								<?php 
								
								_e('Percentage Complete', 'cqpim');
								
								echo ':</strong> ' . ucwords($task_pc) . '%';								
								
								?>
																
								</p>	
							
							</div>
							
							<div class="clear"></div>
						
						</div>
						
					</div>
				
					<div class="cqpim-dash-item grid-item">
					
						<h3><?php _e('Time Entries', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">
						
							<?php

							$time_spent = get_post_meta($post->ID, 'task_time_spent', true);
											
							if($time_spent) {

								$total = 0;

								echo '<ul class="time_spent">';
								
								foreach($time_spent as $key => $time) {
								
									$user = wp_get_current_user();
									
									$args = array(
									
										'post_type' => 'cqpim_teams',
										
										'posts_per_page' => -1,
										
										'post_status' => 'private'
									
									);
									
									$members = get_posts($args);
									
									foreach($members as $member) {
									
										$team_details = get_post_meta($member->ID, 'team_details', true);
										
										if($team_details['user_id'] == $user->ID) {
										
											$assigned = $member->ID;
										
										}
									
									}
									
									if($assigned == $time['team_id'] || current_user_can('cqpim_dash_view_all_tasks')) {
									
										$delete = ' - <a class="time_remove" href="#" data-key="'. $key .'" data-task="'. $post->ID .'">' . __('REMOVE', 'cqpim') . '</a>';
									
									} else {
									
										$delete = '';
									
									}
								
									echo '<li>' . $time['team'] . ' <span style="float:right" class="right"><strong>' . number_format((float)$time['time'], 2, '.', '') . ' ' . __('HOURS', 'cqpim') . '</strong> ' . $delete . '</span></li>';
								
									$total = $total + $time['time'];
								}
								
								echo '</ul>';
								
								$total = str_replace(',','.', $total);
								
								$time_split = explode('.', $total);
								
								if(!empty($time_split[1])) {
								
									$minutes = '0.' . $time_split[1];
								
									$minutes = $minutes * 60;
									
									$minutes = number_format((float)$minutes, 0, '.', '');
								
								} else {
								
									$minutes = '0';
									
								}
								
								if($time_split[0] > 1) {
								
									$hours  = 'hours';
									
								} else {
								
									$hours = 'hour';
								
								}
								
								echo '<br /><span><strong>TOTAL: ' . number_format((float)$total, 2, '.', '') . ' ' . __('hours', 'cqpim') . '</strong> (' . $time_split[0] . ' ' . $hours . ' + ' . $minutes . ' ' . __('minutes', 'cqpim') . ')</span> <div id="ajax_spinner_remove_time_'. $post->ID .'" class="ajax_spinner" style="display:none"></div>';

							} else {

								echo '<span style="display:block; padding:10px" class="task_over">' . __('This task does not have any time assigned to it', 'cqpim') . '</span>';

							}

							?>
						
						</div>
						
					</div>
					
					<div class="cqpim-dash-item grid-item">
					
						<h3><?php _e('Task Files', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">
							
							<?php 
							
							$all_attached_files = get_attached_media( '', $post->ID );
							
							if(!$all_attached_files) {
							
								echo '<p>' . __('There are no files uploaded to this task.', 'cqpim') . '</p>';
							
							} else {
							
								echo '<table class="milestones"><thead><tr>';
								
								echo '<th>' . __('File Name', 'cqpim') . '</th><th>' . __('Actions', 'cqpim') . '</th>';
								
								echo '</tr></thead><tbody>';
							
								foreach($all_attached_files as $file) {
								
									$file_object = get_post($file->ID);
									
									$link = get_the_permalink($file->ID);
								
									$user = get_user_by( 'id', $file->post_author );
								
									echo '<tr>';
									
									echo '<td style="text-align:left"><a class="cqpim-link" href="' . $file->guid . '" download="' . $file->post_title . '">' . $file->post_title . '</a><p style="margin:0; text-align:left; padding-left:0; padding-bottom:0">' . __('Uploaded on', 'cqpim') . ' ' . $file->post_date . ' ' . __('by', 'cqpim') . ' ' . $user->display_name . '</p></td>';
									
									echo '<td><a href="' . $file->guid . '" download="' . $file->post_title . '" class="download_file" value="' . $file->ID . '"><img title="Download File" src="' .  plugin_dir_url( __FILE__ ) . 'img/download.png' . '" /></a></td>';
									
									echo '</tr>';
								
								}
								
								echo '</tbody></table>';
							
							}
							
							?>
						
						</div>
						
					</div>
					
					<div class="cqpim-dash-item-triple grid-item">
					
						<h3><?php _e('Task Messages', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">
						
							<?php
				
								$string = cqpim_random_string(10);

								$messages = get_post_meta($post->ID, 'task_messages', true);
								
								if(empty($messages)) {
								
									echo '<p>' . __('No messages to show', 'cqpim') . '</p>'; ?>
									
						</div>
						
					</div>
					
					<div class="cqpim-dash-item-triple grid-item">
					
						<h3><?php _e('Update Task', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">
									
									<h4><?php _e('Upload Files', 'cqpim'); ?></h4>
									
									<input type="hidden" id="file_task_id" name="file_task_id" value="<?php echo $post->ID; ?>" />
							
									<input type="hidden" name="action" value="new_file" />
									
									<?php wp_nonce_field( 'new-post' ); ?>

									<input type="hidden" name="ip_address" value="<?php echo cqpim_get_client_ip(); ?>" />
									
									<form style="width:200px" id="upload" method="post" enctype="multipart/form-data">
									
										<div id="drop">
											
											<?php _e('Drop Here', 'cqpim'); ?>

											<a><?php _e('Browse', 'cqpim'); ?></a>
											
											<input type="file" name="upl" multiple />
											
										</div>
										
										<ul>
											
										</ul>
									
									</form>
									
									<?php echo '<h4>' . __('Add Message', 'cqpim') . '</h4>'; ?>
																	
									<textarea style="width:99%; height:180px" id="add_task_message" name="add_task_message"></textarea>
									
									<br /><br /><br />
									
									<a href="#" id="update_task" class="cqpim-dash-button medium"><?php _e('Update Task', 'cqpim'); ?></a>
									
									<div class="clear"></div>
									
								<?php
									
								} else {
								
									$messages = array_reverse($messages);
								
									foreach($messages as $key => $message) { 
									
										$user = get_user_by('id', $message['author']);

										$email = $user->user_email;		
										
										?>
									
										<div class="project_message">
										
											<?php 
											
											$value = get_option('cqpim_disable_avatars');
											
											if(empty($value)) {
												
												echo '<div class="user_avatar">';
											
													echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 
											
												echo '</div>';
											
											} 
											
											?> 
											
											<?php if(!empty($value)) { ?>
											
												<div style="padding-left:0" class="message_details">
											
											<?php } else { ?>
											
												<div class="message_details">
											
											<?php } ?>
											
												<h4>
												
													<?php echo $message['by']; ?>							
													
												</h4>
												
												<span><?php echo date('d/m/y H:i', $message['date']); ?></span>
												
												<div class="message">
												
													<?php echo wpautop($message['message']); ?>
												
												</div>
											
											</div>
											
											<div class="clear"></div>
										
										</div>
									
									<?php } ?>
									
								</div>
								
							</div>
							
							<?php
							
							$string = cqpim_random_string(10);
							
							$_SESSION['upload_ids'] = array();
								
							$_SESSION['ticket_changes'] = array();
							
							?>
							
							<div class="cqpim-dash-item-triple grid-item">
							
								<h3><?php _e('Update Task', 'cqpim'); ?></h3>
								
								<div class="cqpim-dash-item-inside">
									
									<h4><?php _e('Upload Files', 'cqpim'); ?></h4>
									
									<input type="hidden" id="file_task_id" name="file_task_id" value="<?php echo $post->ID; ?>" />
							
									<input type="hidden" name="action" value="new_file" />
									
									<?php wp_nonce_field( 'new-post' ); ?>

									<input type="hidden" name="ip_address" value="<?php echo cqpim_get_client_ip(); ?>" />
									
									<form style="width:200px" id="upload" method="post" enctype="multipart/form-data">
									
										<div id="drop">
											
											<?php _e('Drop Here', 'cqpim'); ?>

											<a><?php _e('Browse', 'cqpim'); ?></a>
											
											<input type="file" name="upl" multiple />
											
										</div>
										
										<ul>
											
										</ul>
									
									</form>
									
									<?php echo '<h4>' . __('Add Message', 'cqpim') . '</h4>'; ?>
																	
									<textarea style="width:99%; height:180px" id="add_task_message" name="add_task_message"></textarea>
									
									<br /><br /><br />
									
									<a href="#" id="update_task" class="cqpim-dash-button medium"><?php _e('Update Task', 'cqpim'); ?></a>
									
									<div class="clear"></div>
									
								<?php }
								
							?>
						
						</div>
						
					</div>
					
					<div class="clear"></div>
				
				</div>
					
				<?php } else {
				
					echo '<h1>' . __('ACCESS DENIED', 'cqpim') . '</h1>';
					
				} ?>

			<div class="clear"></div>

		</div><!-- #content -->
		
		<div class="clear"></div>
	
	</div>
	
	<div class="clear"></div>

<?php

	get_footer();
	
?>



