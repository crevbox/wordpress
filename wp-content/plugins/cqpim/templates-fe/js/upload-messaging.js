jQuery(document).ready(function() {
	
	jQuery('.cqpim-file-upload').on('change', function(e) {
		
		e.preventDefault();
		
		var element = jQuery(this);
		
		var id = jQuery(this).attr('id');

		var formData = new FormData();

		formData.append('action', 'upload-attachment');
		
		formData.append('async-upload', jQuery(this)[0].files[0]);
		
		formData.append('name', jQuery(this)[0].files[0].name);
		
		formData.append('_wpnonce', upload_config.nonce);

		jQuery.ajax({
			
			url: upload_config.upload_url,
			
			data: formData,
			
			processData: false,
			
			contentType: false,
			
			dataType: 'json',
			
			type: 'POST',
			
			beforeSend: function() {
				
				//jQuery(element).hide();
				
				jQuery('#upload_attachments').show().append('<span class="amber-span cqpim-uploading cqpim-messages-upload">' + upload_config.strings.uploading + '</span>'); 
				
			},
			
			success: function(resp) {
				
				console.log(resp);
				
				if ( resp.success ) {
					
					jQuery('.cqpim-uploading').remove();
					
					jQuery('.cqpim-upload-error').remove();
					
					jQuery('#upload_attachments').show().append('<span id="' + resp.data.id + '" class="green-span cqpim-messages-upload">' + upload_config.strings.success + ' - ' + resp.data.filename + ' - <a href="#" style="color:#fff" class="btn-change-image" data-id="' + resp.data.id + '">' + upload_config.strings.change + '</a></span>');
					
					ids = jQuery('#upload_attachment_ids').val();
					
					if(!ids) {
						
						jQuery('#upload_attachment_ids').val(resp.data.id);
						
					} else {
					
						jQuery('#upload_attachment_ids').val(ids + ',' + resp.data.id);
						
					}

				} else {
					
					jQuery('.cqpim-uploading').remove();
					
					jQuery('.cqpim-upload-error').remove();
					
					jQuery('#upload_attachments').show().append('<span class="red-span cqpim-messages-upload cqpim-upload-error">' + upload_config.strings.error + '</span>');
					
					jQuery(element).show();
					
				}
				
				
				
			}
			
		});
		
	});	
	
	jQuery('.btn-change-image').live( 'click', function(e) {
		
		e.preventDefault();
		
		var id = jQuery(this).data('id');
		
		var element = jQuery('#' + id).remove();
		
		var list = jQuery('#upload_attachment_ids').val();
		
		var new_list = cqpim_removeValue(list,id,',');
		
		jQuery('#upload_attachment_ids').val(new_list);
		
	});
	
	jQuery('.cqpim-file-upload').on('click', function() {
		
		var id = jQuery(this).data('id');
		
		jQuery(this).val('');
		
		//jQuery('#upload_attachment_ids').val('');
		
	});

});

var cqpim_removeValue = function(list, value, separator) {
	
  separator = separator || ",";
  
  var values = list.split(separator);
  
  for(var i = 0 ; i < values.length ; i++) {
	  
    if(values[i] == value) {
		
      values.splice(i, 1);
	  
      return values.join(separator);
	  
    }
	
  }
  
  return list;
  
}