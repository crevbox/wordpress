<?php 

$user = wp_get_current_user();

if (!in_array('cqpim_client', $user->roles)) {

	$login_page = get_option('cqpim_login_page');
	
	$url = get_the_permalink($login_page);
	
	wp_redirect($url, 302);
	
	exit();

} else {

	include_once( realpath(dirname(__FILE__)) . '/upload.php' );

	$user = wp_get_current_user(); 	
	
	$login_page_id = get_option('cqpim_login_page');
	
	$login_url = get_the_permalink($login_page_id);
	
	$user_id = $user->ID;
	
	$dash_type = get_option('client_dashboard_type');
	
	$theme = wp_get_theme();
	
	$quote_form = get_option('cqpim_backend_form');
	
	// Assignment
	
	$args = array(
	
		'post_type' => 'cqpim_client',
		
		'posts_per_page' => -1,
		
		'post_status' => 'private'
	
	);
	
	$members = get_posts($args);
	
	foreach($members as $member) {
	
		$team_details = get_post_meta($member->ID, 'client_details', true);
		
		if($team_details['user_id'] == $user->ID) {
		
			$assigned = $member->ID;
			
			$client_type = 'admin';
		
		}
	
	} 
	
	if(empty($assigned)) {
	
		foreach($members as $member) {
		
			$team_ids = get_post_meta($member->ID, 'client_ids', true);
			
			if(!is_array($team_ids)) {
			
				$team_ids = array($team_ids);
			
			}
			
			if(in_array($user->ID, $team_ids)) {
			
				$assigned = $member->ID;
				
				$client_type = 'contact';
			
			}
		
		} 			
	
	}
	
	$client_contract = get_post_meta($assigned, 'client_contract', true);
	
	$client_details = get_post_meta($assigned, 'client_details', true);
	
	$client_ids = get_post_meta($assigned, 'client_ids', true);
	
	$client_ids_untouched = $client_ids;
	
	if(empty($client_ids_untouched)) {
		
		$client_ids_untouched = array();
		
	}
	
	$login_url = get_option('cqpim_logout_url');
	
	if(empty($login_url)) {
	
		$login_url = get_the_permalink($login_page_id);
		
	}
	
}

// Is this an invoice payment redirect?

		$payment_status = isset($_POST['payment_status']) ? $_POST['payment_status'] : '';	
		
		if($payment_status == 'completed') {
			
			$last = $_SESSION['last_invoice'];
		
			cqpim_mark_invoice_paid($last, 'PayPal', $_SESSION['payment_amount_' . $last]);
			
			$payment = true;
		
		}
		
		$twocheck = isset($_GET['credit_card_processed']) ? $_GET['credit_card_processed'] : '';
		
		if($twocheck == 'Y') {
			
			$last = $_SESSION['last_invoice'];
		
			cqpim_mark_invoice_paid($last, '2Checkout', $_SESSION['payment_amount_' . $last]);
			
			$payment = true;
		
		}
		
		$stripe_token = isset($_POST['stripeToken']) ? $_POST['stripeToken'] : '';
		
		if(!empty($stripe_token)) {
			
			$last = $_SESSION['last_invoice'];
		
			require_once(plugin_dir_path( __FILE__ ) . 'stripe/init.php');
		
			\Stripe\Stripe::setApiKey(get_option('client_invoice_stripe_secret'));

			$token = $_POST['stripeToken'];

			try {
			
				$charge = \Stripe\Charge::create(array(
					"amount" => $_SESSION['payment_amount_' . $last] * 100,
					"currency" => get_option('currency_code'),
					"source" => $token,
					"description" => __('Invoice', 'cqpim') . $post->post_title)
				);
			} catch(\Stripe\Error\Card $e) {
			
			  
			}	
		
			cqpim_mark_invoice_paid($last, 'Stripe', $_SESSION['payment_amount_' . $last]);
			
			$payment = true;
		
		}

get_header();
	
?>

	<?php 

	$user = wp_get_current_user();

	$client_logs = get_post_meta($assigned, 'client_logs', true);

	if(empty($client_logs)) {

		$client_logs = array();
		
	}

	$now = time();

	$client_logs[$now] = array(

		'user' => $user->ID,
		
		'page' => __('Client Dashboard', 'cqpim')

	);
	
	update_post_meta($assigned, 'client_logs', $client_logs);
	
	?>

	<div style="padding-left:0; margin:0 auto; max-width:1100px; padding:20px; background:#fff" class="cqpim-client-dash" role="main">	
	
		<div style="background:0; padding:0 0 30px 0" class="cqpim-dash-header">
		
			<?php 
			
			$value = get_option('cqpim_disable_avatars');
			
			$user = wp_get_current_user();
			
			if(empty($value)) {
				
				echo '<div class="user_avatar" style="float:left; margin-right:20px">';
			
					echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 
			
				echo '</div>';
			
			} 
			
			?> 

			<h1><?php printf(__('Welcome back, %1$s', 'cqpim'), $user->display_name); ?><br /><span style="font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $user->user_email; ?></span> <span style="margin-left:10px;font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $client_details['client_company']; ?> <?php if($client_type == 'admin') { echo '&nbsp;&nbsp;(Main Contact)'; } ?></span></h1>
		
			<div class="clear"></div>
			
				<?php $client_dash = get_option('cqpim_client_page'); ?>
			
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash); ?>"><?php _e('Dashboard', 'cqpim'); ?></a>
		
				<?php if(get_option('cqpim_messages_allow_client') == 1) { 
				
					$messaging = true; ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=messages'; ?>"><?php _e('Messages', 'cqpim'); ?></a>
				
				<?php } ?>				
				
				<?php if(!empty($quote_form) && get_option('enable_quotes') == 1) { ?>
				
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=quote_form'; ?>"><?php _e('Request a Quote', 'cqpim'); ?></a>
				
				<?php } 
			
				$client_settings = get_option('allow_client_settings');
				
				if($client_settings == 1) { ?>				
					
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=settings'; ?>"><?php _e('Settings', 'cqpim'); ?></a>

				<?php } 
				
				$client_settings = get_option('allow_client_users');
				
				if($client_settings == 1) { ?>	

					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=contacts'; ?>"><?php _e('Users', 'cqpim'); ?></a>

				<?php } ?>
				
				<?php
				
				include_once(ABSPATH.'wp-admin/includes/plugin.php');
			
				if(is_plugin_active('cqpim-envato/cqpim-envato.php')) {				
				
				?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=add-envato-purchase'; ?>"><?php _e('My Envato Purchases', 'cqpim'); ?></a>
				
				<?php } ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo wp_logout_url($login_url); ?>"><?php _e('Log Out', 'cqpim'); ?></a>
				
				<div class="clear"></div>

		</div>
		
		<div style="padding:0" class="cqpim-dash-content">
		
		<?php if(!empty($payment) && $payment == true) { ?>
		
			<br />
		
			<div class="alert alert-success">
			  <strong><?php _e('Payment Successful.', 'cqpim'); ?></strong> <?php _e('Your payment has been accepted, thank you.', 'cqpim'); ?>
			</div>			
		
		<?php } ?>
		
		<?php if(empty($_GET['page'])) { ?>
		
			<div style="width:100%; background:0" class="cqpim-dash-item">
			
				<h3><?php _e('Alerts', 'cqpim'); ?></h3>
				
				<div class="cqpim-dash-item-inside">
				
				<?php 
				
					$alerts = array();
				
					// Check Invoices
					
					if(get_option('disable_invoices') != 1) {
					
					$args = array(
					
						'post_type' => 'cqpim_invoice',
						
						'posts_per_page' => -1,
						
						'post_status' => 'publish',
					
					);
					
					$invoices = get_posts($args);
					
					$outstanding_invoices = 0;
					
					$overdue_invoices = 0;
					
					foreach($invoices as $invoice) {
					
						$invoice_details = get_post_meta($invoice->ID, 'invoice_details', true);
						
						$client_id = isset($invoice_details['client_id']) ? $invoice_details['client_id'] : '';
						
						$client_details = get_post_meta($client_id, 'client_details', true);

						$client_ids = get_post_meta($client_id, 'client_ids', true);

							if(empty($client_ids)) {
							
								$client_ids = array();
								
							}						
			
						$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';

						$paid = isset($invoice_details['paid']) ? $invoice_details['paid'] : '';
						
						$sent = isset($invoice_details['sent']) ? $invoice_details['sent'] : '';

						$due = isset($invoice_details['terms_over']) ? $invoice_details['terms_over'] : '';
						
						$now = time();
								
						$on_receipt = isset($invoice_details['on_receipt']) ? $invoice_details['on_receipt'] : '';							

						if($client_user_id == $user->ID && empty($paid) && !empty($sent) || in_array($user->ID, $client_ids) && empty($paid) && !empty($sent)) {
						
							$outstanding_invoices++;
							
							if($due < $now) {
							
								$overdue_invoices++;
							
							}
						
						}							
					
					}
					
					if($overdue_invoices != 0) {
					
						$alerts[] = array(
						
							'type' => 'oinvoice',
							
							'data' => sprintf(_n('You have %s overdue invoice', 'You have %s overdue invoices', $overdue_invoices, 'cqpim'), $overdue_invoices),
							
						);
							
					}
					
				if($outstanding_invoices != 0) {
					
					if($outstanding_invoices == 1) {
						
						$alerts[] = array(
						
							'type' => 'invoice',
							
							'data' => __('You have 1 outstanding invoice', 'cqpim'),
							
						);						
						
					} else {
				
						$alerts[] = array(
						
							'type' => 'invoice',
							
							'data' => sprintf(__('You have %1$s outstanding invoices', 'cqpim'), $outstanding_invoices),
							
						);
					
					}
						
				}
					
					}
					
					if (get_option('enable_quotes') == 1) {
						
					// Check Quotes
					
					$args = array(
					
						'post_type' => 'cqpim_quote',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private',
					
					);
					
					$quotes = get_posts($args);
					
					$open_quotes = 0;
					
					foreach($quotes as $quote) {
					
						$quote_details = get_post_meta($quote->ID, 'quote_details', true); 
						
						$client_id = isset($quote_details['client_id']) ? $quote_details['client_id'] : '';
						
						$client_details = get_post_meta($client_id, 'client_details', true);

						$client_ids = get_post_meta($client_id, 'client_ids', true);							
						
						$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
						
						$sent = isset($quote_details['sent']) ? $quote_details['sent'] : ''; 
						
						$confirmed = isset($quote_details['confirmed']) ? $quote_details['confirmed'] : ''; 
						
						if(!is_array($client_ids)) {
						
							$client_ids = array();
							
						}
						
						if($client_user_id == $user->ID && empty($confirmed) && !empty($sent) || in_array($user->ID, $client_ids) && empty($confirmed) && !empty($sent) ) {
						
							$open_quotes++;
						
						}
					
					}
					
					if($open_quotes != 0) {
					
						$alerts[] = array(
						
							'type' => 'quote',
							
							'data' => sprintf(_n('You have %s new quote/estimate that requires your attention', 'You have %s new quotes/estimates that require your attention', $open_quotes, 'cqpim'), $open_quotes),
							
						);
						
					}
					
					}
					
					// Check Projects
					
					$args = array(
					
						'post_type' => 'cqpim_project',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private',
					
					);
					
					$projects = get_posts($args);
					
					$unsigned_contracts = 0;
					
					foreach($projects as $project) {
					
						$project_details = get_post_meta($project->ID, 'project_details', true); 
						
						$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
						
						$client_contract = get_post_meta($client_id, 'client_contract', true);
						
						$contracts_enabled = get_option('enable_project_contracts');
						
						$client_details = get_post_meta($client_id, 'client_details', true);

						$client_ids = get_post_meta($client_id, 'client_ids', true);
						
						$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
						
						$sent = isset($project_details['sent']) ? $project_details['sent'] : ''; 
						
						$confirmed = isset($project_details['confirmed']) ? $project_details['confirmed'] : '';
						
						if(empty($client_ids)) { $client_ids = array(); } 

						if($client_user_id == $user->ID && empty($confirmed) && !empty($sent) || in_array($user->ID, $client_ids) && empty($confirmed) && !empty($sent)) {
							
							if(empty($client_contract) && !empty($contracts_enabled)) {

								$unsigned_contracts++;
							
							}
						
						}
					
					}
					
					if($unsigned_contracts != 0) {
					
						$alerts[] = array(
						
							'type' => 'contract',
							
							'data' => sprintf(_n('You have %s contract that requires your attention', 'You have %s contracts that require your attention', $unsigned_contracts, 'cqpim'), $unsigned_contracts),
							
						);
						
					}
					
					// Check Tickets
					
					$user = wp_get_current_user();
					
					$args = array(
					
						'post_type' => 'cqpim_support',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private',
						
						'author__in' => $client_ids_untouched
					
					); 
					
					$tickets = get_posts($args);
					
					$unread_tickets = 0;

					foreach($tickets as $ticket) {
					
						$ticket_updated = get_post_meta($ticket->ID, 'unread', true);
						
						$ticket_status = get_post_meta($ticket->ID, 'ticket_status', true);
						
						if($ticket_updated == 1 && $ticket_status != 'resolved') {
						
							$unread_tickets++;
							
						}
					
					}
					
					if($unread_tickets != 0) {
					
						$alerts[] = array(
						
							'type' => 'support',
							
							'data' => sprintf(_n('You have %s open support ticket that requires your attention', 'You have %s open support tickets that require your attention', $unread_tickets, 'cqpim'), $unread_tickets),
							
						);
						
					}
					
					// Show the Alerts
					
					if(!empty($alerts)) {
					
						echo '<ul class="cqpim_alerts">';
					
						foreach($alerts as $alert) {
						
							echo '<li class="' . $alert['type'] . '">' . $alert['data'] . '</li>';
						
						}
						
						echo '</ul>';
					
					} else {
					
						echo '<p>' . __('You have no active alerts.', 'cqpim') . '</p>';
					
					}
							
					
				?>
				
			</div>
			
			</div>
		
		<?php } ?>
		
			<?php

				if(empty($_GET['page'])) { ?>
				
					<?php if(get_option('enable_quotes') == 1) { ?>
				
					<div style="width:100%; background:0" class="cqpim-dash-item">
					
						<h3><?php _e('Quotes/Estimates', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">

							<table class="milestones dataTable-CQ">

								<thead>
								
									<tr>
									
										<th><?php _e('Owner', 'cqpim'); ?></th>
									
										<th><?php _e('Title', 'cqpim'); ?></th>
										
										<th><?php _e('Created', 'cqpim'); ?></th>
										
										<th><?php _e('Status', 'cqpim'); ?></th>
									
									</tr>
									
								</thead>

								<tbody>
					
									<?php 
									
									$args = array(
									
										'post_type' => 'cqpim_quote',
										
										'posts_per_page' => -1,
										
										'post_status' => 'private',
									
									);
									
									$quotes = get_posts($args);
									
									$i = 0;
									
									foreach($quotes as $quote) { 
									
										$url = get_the_permalink($quote->ID); 
										
										$quote_details = get_post_meta($quote->ID, 'quote_details', true); 
										
										$client_contact = $quote_details['client_contact'];
										
										$client_id = isset($quote_details['client_id']) ? $quote_details['client_id'] : '';
										
										$client_details = get_post_meta($client_id, 'client_details', true);
										
										$client_contacts = get_post_meta($client_id, 'client_contacts', true);

										$client_ids = get_post_meta($client_id, 'client_ids', true);
										
										$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
										
										if($client_contact == $client_user_id) {
										
											$fao = $client_details['client_contact'];
										
										} else {
										
											$fao = isset($client_contacts[$client_contact]['name']) ? $client_contacts[$client_contact]['name'] : 'N/A';
										
										}
										
										if(empty($client_ids)) {
										
											$client_ids = array();
											
										}
										
										$sent = isset($quote_details['sent']) ? $quote_details['sent'] : ''; 
										
										$confirmed = isset($quote_details['confirmed']) ? $quote_details['confirmed'] : ''; 
										
										if(!$confirmed) {
										
											if(!$sent) {
											
												$status = '<span class="task_over">' . __('Not Sent', 'cqpim') . '</span>';
											
											} else {
											
												$status = '<span class="task_pending">' . __('New', 'cqpim') . '</span>';
											
											}
										
										} else {
										
											$status = '<span class="task_complete">' . __('Accepted', 'cqpim') . '</span>';
											
										}
										
										if($client_user_id == $user->ID && !empty($sent) || in_array($user->ID, $client_ids) && !empty($sent)) {
										
										?>						

											<tr>	
											
												<td><span class="nodesktop"><strong><?php _e('Owner', 'cqpim'); ?></strong>: </span> <?php echo $fao; ?></td>
											
												<td><span class="nodesktop"><strong><?php _e('Title', 'cqpim'); ?></strong>: </span> <a class="cqpim-link" href="<?php echo $url; ?>?page=quote"><?php echo $quote->post_title; ?></a></td>
												
												<td><span class="nodesktop"><strong><?php _e('Created', 'cqpim'); ?></strong>: </span> <?php echo get_the_date(get_option('cqpim_date_format') . ' H:i', $quote->ID); ?></td>
												
												<td><span class="nodesktop"><strong><?php _e('Status', 'cqpim'); ?></strong>: </span> <?php echo $status; ?></td>
											
											</tr>
										
										<?php 
										
											$i++;
										
										}
									
									} 
									
									if($i == 0) {
									
										echo '<tr><td>' . __('You do not have any current or past quotes', 'cqpim') . '</td><td></td><td></td><td></td></tr>';
									
									}
																
									?>
														
								</tbody>
							
							</table>	
				
						</div>
						
					</div>	

					<?php } ?>

				<?php } if(empty($_GET['page']) ) { ?>
				
					<div style="width:100%; background:0" class="cqpim-dash-item">
					
						<h3><?php _e('Projects', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">
						

					<table class="milestones dataTable-CP">

						<thead>
						
							<tr>
							
								<th><?php _e('Owner', 'cqpim'); ?></th>
							
								<th><?php _e('Title', 'cqpim'); ?></th>
															
								<?php $checked = get_option('enable_project_contracts'); 
								
								if(!empty($checked) && empty($client_contract)) { ?>
								
									<th><?php _e('View', 'cqpim'); ?></th>
									
								<?php } ?>
								
								<th><?php _e('Progress', 'cqpim'); ?></th>
								
								<th><?php _e('Open Tasks', 'cqpim'); ?></th>
								
								<th><?php _e('Days Until Launch', 'cqpim'); ?></th>
								
								<th><?php _e('Status', 'cqpim'); ?></th>
							
							</tr>
							
						</thead>

						<tbody>
			
							<?php 
							
							$args = array(
							
								'post_type' => 'cqpim_project',
								
								'posts_per_page' => -1,
								
								'post_status' => 'private',
							
							);
							
							$projects = get_posts($args);
							
							$i = 0;
							
							foreach($projects as $project) { 
							
								$url = get_the_permalink($project->ID); 
								
								$contract = $url . '?page=contract';
								
								$summary = $url . '?page=summary&sub=updates';
								
								$project_details = get_post_meta($project->ID, 'project_details', true); 
								
								$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
								
								$client_contact = isset($project_details['client_contact']) ? $project_details['client_contact'] : '';
								
								$owner = get_user_by('id', $client_contact);
								
								$client_details = get_post_meta($client_id, 'client_details', true);

								$client_ids = get_post_meta($client_id, 'client_ids', true);
								
								if(empty($client_ids)) {
								
									$client_ids = array();
									
								}
								
								$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
								
								$sent = isset($project_details['sent']) ? $project_details['sent'] : ''; 
								
								$confirmed = isset($project_details['confirmed']) ? $project_details['confirmed'] : ''; 
								
								$signoff = isset($project_details['signoff']) ? $project_details['signoff'] : ''; 
								
								$closed = isset($project_details['closed']) ? $project_details['closed'] : ''; 
								
								$finish_date = isset($project_details['finish_date']) ? $project_details['finish_date'] : '';
								
								if(!is_numeric($finish_date)) {
														
									$str_finish_date = str_replace('/','-', $finish_date);
								
									$unix_finish_date = strtotime($str_finish_date);
								
								} else {
								
									$unix_finish_date = $finish_date;
									
								}
								
								$current_date = time();
								
								$days_to_due = round(abs($current_date - $unix_finish_date) / 86400);
								
								$project_elements = get_post_meta($project->ID, 'project_elements', true); 
								
								if(empty($project_elements)) {
								
									$project_elements = array();
									
								}
								
								$task_count = 0;
								
								$task_total_count = 0;
								
								$task_complete_count = 0;
								
								foreach ($project_elements as $element) {
								
									$args = array(
									
										'post_type' => 'cqpim_tasks',
										
										'posts_per_page' => -1,
										
										'meta_key' => 'milestone_id',
										
										'meta_value' => $element['id'],
										
										'orderby' => 'date',
										
										'order' => 'ASC'
										
									);
									
									$tasks = get_posts($args);	
									
									foreach($tasks as $task) {
									
										$task_total_count++;
									
										$task_details = get_post_meta($task->ID, 'task_details', true);
										
										$status = isset($task_details['status']) ? $task_details['status']: '';
										
										if($status != 'complete') {
										
											$task_count++;
										
										}
										
										if($status == 'complete') {
										
											$task_complete_count++;
										
										}
									
									}
								
								}
								
								if($task_total_count != 0) {
								
									$pc_per_task = 100 / $task_total_count;
								
									$pc_complete = $pc_per_task * $task_complete_count;
								
								} else {
								
									$pc_complete = 0;
								
								}
								
								if(!$closed) {
								
									if(!$signoff) {
									
										if(!$confirmed) {
										
											if(!$sent) {
											
												$status = '<span class="task_over">' . __('New', 'cqpim') . '</span>';
											
											} else {
											
												$checked = get_option('enable_project_contracts'); 
										
												if(!empty($checked) && empty($client_contract)) {
											
													$status = '<span class="task_pending">' . __('Awaiting Contracts', 'cqpim') . '</span>';
													
												} else {
												
													$status = '<span class="task_complete">' . __('In Progress', 'cqpim') . '</span>';
												
												}
											
											}
										
										} else {
										
											$status = '<span class="task_complete">' . __('In Progress', 'cqpim') . '</span>';
											
										}
										
									} else {
									
										$status = '<span class="task_complete">' . __('Signed Off', 'cqpim') . '</span>';
									
									}
								
								} else {
								
									$status = '<span style="background:#333" class="task_complete">' . __('Closed', 'cqpim') . '</span>';
								
								}
								
								if($client_user_id == $user->ID || in_array($user->ID, $client_ids)) {
								
								?>						

									<tr>

										<td><span class="nodesktop"><strong><?php _e('Owner', 'cqpim'); ?></strong>: </span> <?php echo isset($owner->display_name) ? $owner->display_name : ''; ?></td>									
									
										<td><span class="nodesktop"><strong><?php _e('Title', 'cqpim'); ?></strong>: </span> <a class="cqpim-link" href="<?php echo $summary; ?>"><?php echo $project->post_title; ?></a></td>
										
										<?php $checked = get_option('enable_project_contracts'); 
										
										if(!empty($checked) && empty($client_contract)) { ?>									
												
											<td><span class="nodesktop"><strong><?php _e('View', 'cqpim'); ?></strong>: </span> <a class="cqpim-link" href="<?php echo $contract; ?>"><?php _e('Contract', 'cqpim'); ?></a></td>
												
										<?php } ?>

										<td><span class="nodesktop"><strong><?php _e('Progress', 'cqpim'); ?></strong>: </span> <?php echo number_format((float)$pc_complete, 2, '.', ''); ?>%</td>
										
										<td><span class="nodesktop"><strong><?php _e('Open Tasks', 'cqpim'); ?></strong>: </span> <?php echo $task_count; ?></td>
										
										<td><span class="nodesktop"><strong><?php _e('Days to Launch', 'cqpim'); ?></strong>: </span> <?php echo $days_to_due; ?></td>
										
										<td><span class="nodesktop"><strong><?php _e('Status', 'cqpim'); ?></strong>: </span> <?php echo $status; ?></td>
									
									</tr>
								
								<?php 
								
									$i++;
								
								}
							
							} 
							
							if($i == 0) {
							
								echo '<tr><td>' . __('You do not have any current or past Projects', 'cqpim') . '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
							
							}
														
							?>
												
						</tbody>
					
					</table>
				
						</div>
						
					</div>

				<?php } if(empty($_GET['page'])) { ?>
				
					<div style="width:100%; background:0" class="cqpim-dash-item">
					
						<h3><?php _e('Support Tickets', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">

				<div id="ticket_container" class="content">
			
				<?php 
				
				$user = wp_get_current_user();
				
				$status = isset($_SESSION['ticket_status']) ? $_SESSION['ticket_status'] : array('open','hold');
				
				$args = array(
				
					'post_type' => 'cqpim_support',
					
					'posts_per_page' => -1,
					
					'post_status' => 'private',
					
					'author__in' => $client_ids_untouched,
					
					'meta_key' => 'ticket_status',
					
					'meta_value' => $status
				
				); 
				
				$tickets = get_posts($args);
				
				$total_tickets = count($tickets);
				
				if($tickets) {
				
					echo '<table class="milestones files dataTable-CST">';
					
					echo '<thead><tr><th>' . __('ID', 'cqpim') . '</th><th>' . __('Ticket Title', 'cqpim') . '</th><th>' . __('Ticket Owner', 'cqpim') . '</th><th>' . __('Priority', 'cqpim') . '</th><th>' . __('Created', 'cqpim') . '</th><th>' . __('Last Updated', 'cqpim') . '</th><th>' . __('Status', 'cqpim') . '</th></tr></thead>';
					
					echo '<tbody>';
				
					foreach($tickets as $ticket) {
					
						$ticket_status = get_post_meta($ticket->ID, 'ticket_status', true);
						
						$owner = get_user_by('id', $ticket->post_author);
						
						if($ticket_status == 'open') {
						
							$tstatus = '<span class="low_priority">' . __('Open', 'cqpim') . '</span>';
						
						} else if($ticket_status == 'resolved') {
						
							$tstatus = '<span class="task_complete">' . __('Resolved', 'cqpim') . '</span>';
						
						} else if($ticket_status == 'hold') {
						
							$tstatus = '<span class="high_priority">' . __('On Hold', 'cqpim') . '</span>';
						
						}
						
						$ticket_priority = get_post_meta($ticket->ID, 'ticket_priority', true);
						
						$ticket_updated = get_post_meta($ticket->ID, 'last_updated', true);
						
						if(is_numeric($ticket_updated)) { $ticket_updated = date(get_option('cqpim_date_format') . ' H:i', $ticket_updated); } else { $ticket_updated = $ticket_updated; }
						
						if($ticket_priority == 'low') {
						
							$priority = '<span class="low_priority">' . __('Low', 'cqpim') . '</span>';
						
						} else if($ticket_priority == 'normal') {
						
							$priority = '<span class="normal_priority">' . __('Normal', 'cqpim') . '</span>';
						
						} else if($ticket_priority == 'high') {
						
							$priority = '<span class="high_priority">' . __('High', 'cqpim') . '</span>';
						
						} else if($ticket_priority == 'immediate') {
						
							$priority = '<span class="immediate_priority">' . __('Immediate', 'cqpim') . '</span>';
						
						}
					
						echo '<tr>';
						
						echo '<td><span class="nodesktop"><strong>' . __('Ticket ID', 'cqpim') . '</strong>: </span> ' . $ticket->ID . '</td>';
					
						echo '<td><span class="nodesktop"><strong>' . __('Title', 'cqpim') . '</strong>: </span> <a class="cqpim-link" href="' . get_the_permalink($ticket->ID) . '">' . $ticket->post_title . '</a></td>';
						
						echo '<td><span class="nodesktop"><strong>' . __('Owner', 'cqpim') . '</strong>: </span> ' . $owner->display_name . '</td>';
						
						echo '<td><span class="nodesktop"><strong>' . __('Priority', 'cqpim') . '</strong>: </span> ' . $priority . '</td>';
						
						echo '<td><span class="nodesktop"><strong>' . __('Created', 'cqpim') . '</strong>: </span> ' . get_the_date(get_option('cqpim_date_format') . ' H:i', $ticket->ID) . '</td>';
						
						echo '<td><span class="nodesktop"><strong>' . __('Updated', 'cqpim') . '</strong>: </span> ' . $ticket_updated . '</td>';
						
						echo '<td><span class="nodesktop"><strong>' . __('Status', 'cqpim') . '</strong>: </span> ' . $tstatus . '</td>';
						
						echo '</tr>';
					
					}
					
					echo '</tbody>';
					
					echo '</table>';
				
				} else {
				
					if(is_array($status)) {
				
						echo '<p style="padding:20px">' . __('No open support tickets found...', 'cqpim') . '</p>';
						
					} else if($status == 'resolved') {
					
						echo '<p style="padding:20px">' . __('No resolved support tickets found...', 'cqpim') . '</p>';
						
					}
				
				}
				
				if($status == 'resolved') { ?>
				
				<button id="switch_to_resolved" class="metabox-add-button left"><?php _e('View Open Tickets', 'cqpim'); ?> <div id="support_loader_2" class="ajax_loader" style="display:none"></div></button>
			
				<?php } else { ?>
				
				<button id="switch_to_resolved" class="metabox-add-button left"><?php _e('View Resolved Tickets', 'cqpim'); ?> <div id="support_loader_2" class="ajax_loader" style="display:none"></div></button>
				
				<?php } ?>
				
				<a href="<?php echo get_the_permalink($client_dash) . '?page=add-support-ticket'; ?>" class="metabox-add-button"><?php _e('Add Support Ticket', 'cqpim'); ?></a>
				
				<div class="clear"></div>
			
			</div>
				
						</div>
						
					</div>

				<?php } if(empty($_GET['page'])) { if(get_option('disable_invoices') != 1) { ?>
				
					<div style="width:100%; background:0" class="cqpim-dash-item">
					
						<h3><?php _e('Invoices', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">
						

				<?php
				
			$args = array(
			
				'post_type' => 'cqpim_invoice',
				
				'posts_per_page' => -1,
				
				'post_status' => 'publish',
				
				'orderby' => 'date',
				
				'order' => 'DESC'
			
			);
			
			$invoices = get_posts($args);
			
			if($invoices) {
			
				$currency = get_option('currency_symbol');
			
				echo '<table class="milestones dataTable-CI">';
				
				echo '<thead>';
				
				echo '<tr><th>' . __('Invoice ID', 'cqpim') . '</th><th>' . __('Owner', 'cqpim') . '</th><th>' . __('Invoice Date', 'cqpim') . '</th><th>' . __('Due Date', 'cqpim') . '</th><th>' . __('Amount', 'cqpim') . '</th><th>' . __('Status', 'cqpim') . '</th></tr>';
				
				echo '</thead>';
				
				echo '<tbody>';
				
				$i = 0;
				
				foreach($invoices as $invoice) {
				
					$invoice_id = get_post_meta($invoice->ID, 'invoice_id', true);
					
					$client_contact = get_post_meta($invoice->ID, 'client_contact', true);
					
					$owner = get_user_by('id', $client_contact);
				
					$invoice_details = get_post_meta($invoice->ID, 'invoice_details', true);
					
					$client_id = isset($invoice_details['client_id']) ? $invoice_details['client_id'] : '';
					
					$client_details = get_post_meta($client_id, 'client_details', true);
					
					$client_ids = get_post_meta($client_id, 'client_ids', true);
		
					$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
					
					$invoice_totals = get_post_meta($invoice->ID, 'invoice_totals', true);
					
					$tax_rate = get_option('sales_tax_rate');
					
					if(!empty($tax_rate)) {
					
						$total = isset($invoice_totals['total']) ? $invoice_totals['total'] : '';
						
					} else {
					
						$total = isset($invoice_totals['sub']) ? $invoice_totals['sub'] : '';
						
					}					
					
					$invoice_date = isset($invoice_details['invoice_date']) ? $invoice_details['invoice_date'] : '';
					
					if(is_numeric($invoice_date)) { $invoice_date = date(get_option('cqpim_date_format'), $invoice_date); } else { $invoice_date = $invoice_date; }
									
					$sent = isset($invoice_details['sent']) ? $invoice_details['sent'] : '';
						
					$paid = isset($invoice_details['paid']) ? $invoice_details['paid'] : '';
						
					$due = isset($invoice_details['terms_over']) ? $invoice_details['terms_over'] : '';
					
					$on_receipt = isset($invoice_details['on_receipt']) ? $invoice_details['on_receipt'] : '';
					
					$due = isset($invoice_details['terms_over']) ? $invoice_details['terms_over'] : '';
					
					if(empty($on_receipt)) {
					
						$due_readable = date(get_option('cqpim_date_format'), $due);
						
					} else {
					
						$due_readable = __('Due on Receipt', 'cqpim');
						
					}
						
					$current_date = time();
						
					$link = get_the_permalink($invoice->ID);
					
					$password = md5($invoice->post_password);
					
					$url = $link;
						
					if(!$paid) {
						
						if($current_date > $due) {
						
							if(empty($on_receipt)) {
							
								$status = '<span class="task_over"><strong>' . __('OVERDUE', 'cqpim') . '</strong></span>';
								
							} else {
							
								$status = '<span class="task_over"><strong>' . __('Due on Receipt', 'cqpim') . '</strong></span>';
							
							}
								
						} else {
							
							if(!$sent) {
									
								$status = '<span class="task_over">' . __('New', 'cqpim') . '</span>';
									
							} else {
								
								$status = '<span class="task_pending">' . __('Outstanding', 'cqpim') . '</span>';
									
							}
								
						}
							
					} else {
							
						$status = '<span class="task_complete">' . __('PAID', 'cqpim') . '</span>';
							
					}		
					
					if(!is_array($client_ids)) {
						
						$client_ids = array($client_ids);
						
					}

					if($client_user_id == $user->ID && !empty($sent) || in_array($user->ID, $client_ids)) {
					
						echo '<tr>';
						
						echo '<td><span class="nodesktop"><strong>' . __('ID', 'cqpim') . '</strong>: </span> <a class="cqpim-link" href="' . $url . '" >' . $invoice_id . '</a></td>';

						echo '<td><span class="nodesktop"><strong>' . __('Owner', 'cqpim') . '</strong>: </span> ' . $owner->display_name . '</td>';

						echo '<td><span class="nodesktop"><strong>' . __('Date', 'cqpim') . '</strong>: </span> ' . $invoice_date . '</td>';

						echo '<td><span class="nodesktop"><strong>' . __('Due', 'cqpim') . '</strong>: </span> ' . $due_readable . '</td>';

						echo '<td><span class="nodesktop"><strong>' . __('Amount', 'cqpim') . '</strong>: </span> ' . $currency . '' . $total . '</td>';
							
						echo '<td><span class="nodesktop"><strong>' . __('Status', 'cqpim') . '</strong>: </span> ' . $status . '</td>';
						
						echo '</tr>';
							
						$i++;

					}
				
				}
				
				if($i == 0) {
				
					echo '<tr><td>' . __('You do not have any invoices to show.', 'cqpim') . '</td><td></td><td></td><td></td><td></td><td></td></tr>';
				
				}
				
				echo '</tbody>';
				
				echo '</table>';
			
			}
			
			?>
			
					</div>
					
				</div>
				
			<?php } } if(!empty($messaging) && isset( $_GET['page'] ) && $_GET['page'] == 'messages' ) { ?>
			
				<?php 

				$user = wp_get_current_user();

				$client_logs = get_post_meta($assigned, 'client_logs', true);

				if(empty($client_logs)) {

					$client_logs = array();
					
				}

				$now = time();

				$client_logs[$now] = array(

					'user' => $user->ID,
					
					'page' => __('Client Dashboard Messages Page', 'cqpim')

				);
				
				update_post_meta($assigned, 'client_logs', $client_logs);

				$user = wp_get_current_user();
				
				$users = cqpim_retrieve_messageble_users($user->ID, 'client');
				
				$search = array();

				if(!empty($users)) {
					
					foreach($users as $key => $suser) {
					
						$search[] = array(
						
							'id' => $key,
							
							'name' => $suser,
						
						);
					
					}
					
				}	
				
				$search = json_encode($search);
				
				$conversations = cqpim_fetch_conversations($user->ID);
				
				$text = __('Search for team member name...', 'cqpim');
				
				wp_enqueue_script('upload-form-js', plugin_dir_url( __FILE__ ) . 'js/upload-messaging.js', array('jquery'), '0.1.0', true);

				$data = array(
				
					'upload_url' => admin_url('async-upload.php'),
					
					'ajax_url'   => admin_url('admin-ajax.php'),
					
					'nonce'      => wp_create_nonce('media-form'),
					
					'strings' => array(
					
						'uploading' => __('Uploading...', 'cqpim'),
						
						'success' => __('Successfully uploaded', 'cqpim'),
						
						'change' => __('Remove', 'cqpim'),
						
						'error' => __('Failed to upload file. It may not be on our list of allowed extensions. Please try again.', 'cqpim')
					
					),
				);

				wp_localize_script( 'upload-form-js', 'upload_config', $data );	
				
				$localise_messaging = array(
				
					'ajaxurl' => admin_url('admin-ajax.php'),
				
					'dialogs' => array(
					
						'deleteconv' => __('Delete Conversation', 'cqpim'),
						
						'leaveconv' => __('Leave Conversation', 'cqpim'),
						
						'removeconv' => __('Remove User', 'cqpim'),
						
						'addconv' => __('Add User', 'cqpim'),
						
						'cancel' => __('Cancel', 'cqpim'),
					
					)
				
				);
				
				wp_localize_script( 'cqpim-client-messaging', 'messaging', $localise_messaging );	

				?>

				<div>
					
					<?php if(!empty($_GET['convdeleted'])) { ?>
					
						<div id="cqpim-message" class="success">
						
							<i class="fa fa-check" aria-hidden="true"></i> <?php _e('The conversation was successfully deleted.', 'cqpim'); ?>
						
						</div>
					
					<?php } ?>
					
					<?php if(!empty($_GET['convcreated'])) { ?>
					
						<div id="cqpim-message" class="success">
						
							<i class="fa fa-check" aria-hidden="true"></i> <?php _e('The conversation was successfully created.', 'cqpim'); ?>
						
						</div>
					
					<?php } ?>
					
					<?php if(!empty($_GET['convleft'])) { ?>
					
						<div id="cqpim-message" class="success">
						
							<i class="fa fa-check" aria-hidden="true"></i> <?php _e('You have been removed from the conversation.', 'cqpim'); ?>
						
						</div>
					
					<?php } ?>
					
					<?php if(!empty($_GET['convremoved'])) { ?>
					
						<div id="cqpim-message" class="success">
						
							<i class="fa fa-check" aria-hidden="true"></i> <?php _e('The user has been removed.', 'cqpim'); ?>
						
						</div>
					
					<?php } ?>
					
					<?php if(!empty($_GET['convadded'])) { ?>
					
						<div id="cqpim-message" class="success">
						
							<i class="fa fa-check" aria-hidden="true"></i> <?php _e('The user has been added.', 'cqpim'); ?>
						
						</div>
					
					<?php } ?>
					
					<?php if(empty($conversations)) { ?>
					
						<div id="cqpim-new-message" style="display:none">
						
							<h3><?php _e('New Conversation', 'cqpim'); ?></h3>
							
							<p><?php _e('You can add any team members who are currently assigned to open projects that you own.', 'cqpim'); ?></p>
							
							<form id="cqpim-create-new-message">
							
								<p><span class="cqpim-heading"><?php _e('Recipients:', 'cqpim'); ?></span><input type="text" id="to" name="to" placeholder="<?php echo $text; ?>" /></p>

								<div class="clear"></div>
								
								<p><span class="cqpim-heading"><?php _e('Subject:', 'cqpim'); ?></span><input type="text" id="subject" name="subject" /></p>
								
								<p><span class="cqpim-heading"><?php _e('Message:', 'cqpim'); ?></span><textarea id="message" name="message" /></textarea></p>
								
								<div class="clear"></div>
								
								<p><span class="cqpim-heading"><?php _e('Attachments:', 'cqpim'); ?></span>
								
									<input type="file" class="cqpim-file-upload" name="async-upload" id="attachments" multiple />
									
									<div id="upload_attachments"></div>
									
									<div class="clear"></div>
									
									<input type="hidden" name="image_id" id="upload_attachment_ids">
									
									<input type="hidden" name="action" value="image_submission">						
								
								</p>
				
								<button id="send" class="cqpim-new-button green right"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?php _e('Send', 'cqpim'); ?></button>	
								
								<button id="cancel" class="cqpim-new-button red right"><i class="fa fa-times" aria-hidden="true"></i> <?php _e('Cancel', 'cqpim'); ?></button>
								
								<div class="clear"></div>
								
								<div id="message-ajax-response"></div>
								
							</form>
						
						</div>
						
						<div id="cqpim-no-messages">
						
							<p><?php _e('You do not have any messages.', 'cqpim'); ?></p>
							
							<br />
							
							<a class="cqpim-link" href="#" id="send-message">
							
								<img src="<?php echo plugin_dir_url( 'cqpim' ); ?>cqpim/img/add-new.png">
								
								<p><?php _e('Start a conversation...', 'cqpim'); ?></p>
							
							</a>					
						
						</div>
					
					<?php } else { ?>
					
						<?php $conversation = isset($_GET['conversation']) ? $_GET['conversation'] : '';
						
						if(!empty($conversation)) {
							
							$args = array(
							
								'post_type' => 'cqpim_conversations',
								
								'posts_per_page' => 1,
								
								'post_status' => 'private',
								
								'meta_query'    => array(
								
									array(
									
										'key'       => 'conversation_id',
										
										'value'     => $conversation,
										
										'compare'   => '=',
										
									),
									
								),
							
							);
							
							$conversation = get_posts($args); 
							
							$conversation = $conversation[0]; 
							
							$conversation_id = get_post_meta($conversation->ID, 'conversation_id', true);
							
							$recipients = get_post_meta($conversation->ID, 'recipients', true);
							
							if(!in_array($user->ID, $recipients)) {
								
								echo '<h1>' . __('ACCESS DENIED', 'cqpim') . '</h1>';
								
								return;
								
							}
							
							$args = array(
							
								'post_type' => 'cqpim_messages',
								
								'posts_per_page' => -1,
								
								'post_status' => 'private',
								
								'meta_query' => array(
								
									array(
									
										'key'       => 'conversation_id',
										
										'value'     => $conversation_id,
										
										'compare'   => '=',
										
									),
									
								),
								
								'order' => 'DESC',

								'orderby' => 'meta_value',
								
								'meta_key' => 'stamp'
							
							);
							
							$messages = get_posts($args); ?>
							
							<h3><span id="cqpim-title-editable"><?php echo str_replace('Private: ', '', get_the_title($conversation->ID)); ?></span><input type="text" id="cqpim-title-editable-field" value="<?php echo get_the_title($conversation->ID); ?>" /></h3>
							
							<br />
							
							<input type="hidden" id="jq-user-id" value="<?php echo $user->ID; ?>" />
							
							<input type="hidden" id="jq-conv-id" value="<?php echo $conversation->ID; ?>" />
							
							<button id="cqpim-convo-reply" class="cqpim-new-button green right"><i class="fa fa-reply" aria-hidden="true"></i><span class="desktop_only"> <?php _e('Reply', 'cqpim'); ?></span></button>
							
							<button id="cqpim-convo-leave" class="cqpim-new-button amber right"><i class="fa fa-sign-out" aria-hidden="true"></i><span class="desktop_only"> <?php _e('Leave', 'cqpim'); ?></span></button>

							<?php if($user->ID == $conversation->post_author || current_user_can('cqpim_do_all')) { ?>
							
								<button id="cqpim-convo-delete" class="cqpim-new-button red right"><i class="fa fa-trash" aria-hidden="true"></i><span class="desktop_only"> <?php _e('Delete', 'cqpim'); ?></span></button>
							
							<?php } ?>
							
							<div class="clear"></div>
							
							<div id="delete-confirm" style="display:none" title="<?php _e('Delete Conversation', 'cqpim'); ?>">
								
								<p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php _e('This conversation and all messages will be permanently deleted. Are you sure?', 'cqpim'); ?></p>
							
							</div>
							
							<div id="leave-confirm" style="display:none" title="<?php _e('Leave Conversation', 'cqpim'); ?>">
								
								<p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php _e('Are you sure you want to leave the conversation?', 'cqpim'); ?></p>
							
							</div>
							
							<div id="remove-confirm" style="display:none" title="<?php _e('Remove User', 'cqpim'); ?>">
								
								<p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php _e('Choose which user you would like to remove and click Remove User.', 'cqpim'); ?></p>
							
								<select id="cqpim-remove-user">
								
									<?php foreach($recipients as $recipient) { 
									
										$recip = get_user_by('id', $recipient); ?>
									
										<option value="<?php echo $recip->ID; ?>"><?php echo $recip->display_name; ?></option>
									
									<?php } ?>
								
								</select>
							
							</div>
							
							<div id="add-confirm" style="display:none" title="<?php _e('Add User', 'cqpim'); ?>">
								
								<p><i class="fa fa-check" aria-hidden="true"></i> <?php _e('Search for a user to add to the conversation.', 'cqpim'); ?></p>
							
								<input type="text" id="to" />
							
							</div>
							
							<br />
							
							<div id="cqpim-reply-message" style="display:none">
							
								<h3><?php _e('Reply to Conversation', 'cqpim'); ?></h3>
								
								<form id="cqpim-create-new-message">
									
									<p><span class="cqpim-heading"><?php _e('Message:', 'cqpim'); ?></span><textarea id="message" name="message" /></textarea></p>
									
									<div class="clear"></div>
									
									<p><span class="cqpim-heading"><?php _e('Attachments:', 'cqpim'); ?></span>
									
										<input type="file" class="cqpim-file-upload" name="async-upload" id="attachments" multiple />
										
										<div id="upload_attachments"></div>
										
										<div class="clear"></div>
										
										<input type="hidden" name="image_id" id="upload_attachment_ids">
										
										<input type="hidden" name="action" value="image_submission">						
									
									</p>
					
									<button id="send-reply" class="cqpim-new-button green right" data-conversation="<?php echo $conversation->ID; ?>"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?php _e('Send', 'cqpim'); ?></button>	
									
									<button id="cancel-reply" class="cqpim-new-button red right"><i class="fa fa-times" aria-hidden="true"></i> <?php _e('Cancel', 'cqpim'); ?></button>
									
									<div class="clear"></div>
									
								</form>
							
							</div>	

							<br />
							
							<div id="cqpim-dmessage-container">
							
								<?php foreach($messages as $message) { 
								
									$sender = get_post_meta($message->ID, 'sender', true);
									
									$system = get_post_meta($message->ID, 'system', true);
									
									$sender_obj = get_user_by('id', $sender);
									
									if($user->ID != $sender) {
										
										$read = get_post_meta($message->ID, 'read', true);
										
										if(!empty($read) && is_array($read) && !in_array($user->ID, $read)) {
											
											$read[] = $user->ID;
											
										}
										
										update_post_meta($message->ID, 'read', $read);
										
									}
									
									$update = get_post_meta($message->ID, 'message', true);
									
									$stamp = get_post_meta($message->ID, 'stamp', true);
									
									if($sender == $user->ID) {
										
										$class = ' own';
										
									} else {
										
										$class = '';
										
									}
									
									if(!empty($system)) {
										
										$class = ' system';
										
									}
								
									if(!empty($system)) {
										
										echo '<div style="text-align:center; clear:both">';
										
									} ?>
								
									<div class="cqpim-dmessage-bubble<?php echo $class; ?>">
									
										<div class="cqpim-messagelist-avatar" style="float:right; margin-left:20px;">
										
											<?php echo get_avatar( $sender_obj->ID, 80, '', $sender_obj->display_name, array('force_display' => true)); ?>

										</div>
									
										<?php echo $update; ?>
										
										<div class="clear"></div>
										
										<?php $all_attached_files = get_attached_media( '', $message->ID ); 
										
										if(!empty($all_attached_files)) { ?>
										
											<div class="cqpim-dmessage-attachments<?php echo $class; ?>">
											
												<div><strong><i class="fa fa-paperclip" aria-hidden="true"></i> <?php _e('Attachments', 'cqpim'); ?></strong></div>
											
												<ul>
												
													<?php foreach($all_attached_files as $file) { ?>
													
														<li><a href="<?php echo get_the_permalink($file->ID); ?>"><?php echo $file->post_title; ?></a> | <i class="fa fa-download" aria-hidden="true"></i>  <a href="<?php echo $file->guid; ?>" download ><?php _e('Download', 'cqpim'); ?></a></li>
													
													<?php } ?>
												
												</ul>
											
											</div>
										
										<?php } ?>
										
										<div class="cqpim-dmessage-date<?php echo $class; ?>">
										
											<i class="fa fa-paper-plane" aria-hidden="true"></i>
										
											<?php 
											
											$date = date(get_option('cqpim_date_format') . ' H:i', $stamp);
											
											printf(__('Posted by %1$s on %2$s', 'cqpim'), $sender_obj->display_name, $date);
											
											$read = get_post_meta($message->ID, 'read', true);
											
											if(!empty($read)) {
												
												echo '&nbsp;&nbsp;';
												
												echo '<i class="fa fa-envelope-open" aria-hidden="true"></i> ';
												
												_e('Seen by:', 'cqpim') . ' ';
												
												$count = count($read);
												
												$i = 0;
												
												foreach($read as $p) {
													
													$i++;
													
													$po = get_user_by('id', $p);
													
													echo ' ' . $po->display_name;
													
													if($i != $count) { echo ','; }
													
												}
												
											}
											
											$piping = get_post_meta($message->ID, 'piping', true);
											
											if(!empty($piping)) {
												
												echo ' - ' . __('Sent via email', 'cqpim');
												
											}
											
											?>
										
										</div>
									
									</div>	

									<?php if(!empty($system)) {
										
										echo '</div>';
										
									} ?>							
								
								<?php } ?>	

							</div>
							
						<?php } else { ?>
						
							<button id="send-message" class="cqpim-new-button green"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?php _e('New Conversation', 'cqpim'); ?></button>
							
							<div id="cqpim-new-message" style="display:none">
							
								<h3><?php _e('New Conversation', 'cqpim'); ?></h3>
								
								<p><?php _e('You can add any team members who are currently assigned to open projects that you own.', 'cqpim'); ?></p>
								
								<form id="cqpim-create-new-message">
								
									<p><span class="cqpim-heading"><?php _e('Recipients:', 'cqpim'); ?></span><input type="text" id="to" name="to" placeholder="<?php echo $text; ?>" /></p>

									<div class="clear"></div>
									
									<p><span class="cqpim-heading"><?php _e('Subject:', 'cqpim'); ?></span><input type="text" id="subject" name="subject" /></p>
									
									<p><span class="cqpim-heading"><?php _e('Message:', 'cqpim'); ?></span><textarea id="message" name="message" /></textarea></p>
									
									<div class="clear"></div>
									
									<p><span class="cqpim-heading"><?php _e('Attachments:', 'cqpim'); ?></span>
									
										<input type="file" class="cqpim-file-upload" name="async-upload" id="attachments" multiple />
										
										<div id="upload_attachments"></div>
										
										<div class="clear"></div>
										
										<input type="hidden" name="image_id" id="upload_attachment_ids">
										
										<input type="hidden" name="action" value="image_submission">						
									
									</p>
					
									<button id="send" class="cqpim-new-button green right"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?php _e('Send', 'cqpim'); ?></button>	
									
									<button id="cancel" class="cqpim-new-button red right"><i class="fa fa-times" aria-hidden="true"></i> <?php _e('Cancel', 'cqpim'); ?></button>
									
									<div class="clear"></div>
									
									<div id="message-ajax-response"></div>
									
								</form>
							
							</div>
							
							<br />
						
							<table class="milestones messages-table">
							
								<thead>
								
									<tr>
									
										<th><?php _e('Subject', 'cqpim'); ?></th>
										
										<th><?php _e('Created', 'cqpim'); ?></th>
										
										<th><?php _e('Updated', 'cqpim'); ?></th>
										
										<th><?php _e('Members', 'cqpim'); ?></th>
									
									</tr>
								
								</thead>
								
								<tbody>
								
									<?php foreach($conversations as $conversation) { 
									
										$id = get_post_meta($conversation->ID, 'conversation_id', true);
									
										$created = get_post_meta($conversation->ID, 'created', true);
										
										$updated = get_post_meta($conversation->ID, 'updated', true);
										
										$update_user = get_user_by('id', $updated['by']);
										
										$update_user = $update_user->display_name;
										
										$members = get_post_meta($conversation->ID, 'recipients', true);
										
										$args = array(
										
											'post_type' => 'cqpim_messages',
											
											'posts_per_page' => -1,
											
											'post_status' => 'private',
											
											'meta_query' => array(
											
												array(
												
													'key'       => 'conversation_id',
													
													'value'     => $id,
													
													'compare'   => '=',
													
												),
												
											),
										
										);
										
										$messages = get_posts($args);
										
										$read_val = true;
										
										foreach($messages as $message) {
											
											$read = get_post_meta($message->ID, 'read', true);
											
											if(!in_array($user->ID, $read)) {
												
												$read_val = false;
												
											}
											
											$dash_page = get_option('cqpim_client_page');
											
										}
									
										?>
									
										<tr<?php if(empty($read_val)) { echo ' class="cqpim-unread"'; } ?>>
										
											<td><?php if(empty($read_val)) { echo '<i class="fa fa-envelope" aria-hidden="true"></i> '; } else { echo '<i class="fa fa-envelope-open" aria-hidden="true"></i> '; } ?>&nbsp;&nbsp;<a style="color:#222" href="<?php echo get_the_permalink($dash_page) . '?page=messages&conversation=' . $id; ?>"><?php echo str_replace('Private: ','', get_the_title($conversation->ID)); ?></a></td>
											
											<td><?php echo date(get_option('cqpim_date_format') . ' H:i', $created); ?></td>
											
											<td><?php echo date(get_option('cqpim_date_format') . ' H:i', $updated['at']) . ' - ' . $update_user; ?></td>
											
											<td>
											
												<?php foreach($members as $member) {
													
													$recip = get_user_by('id', $member);
													
													echo '<div class="cqpim-messagelist-avatar">';
													
													echo get_avatar( $recip->ID, 80, '', $recip->display_name, array('force_display' => true));

													echo '</div>';
													
												} ?>
											
											</td>
										
										</tr>
									
									<?php } ?>
								
								</tbody>
							
							</table>
						
						<?php } ?>
					
					<?php } ?>
					
					
					
					<script>
					
						jQuery(document).ready(function() {
							
							jQuery('#to').tokenInput(<?php echo $search; ?>, {
								hintText: '<?php echo $text; ?>',
								noResultsText: '<?php _e('No Results', 'cqpim'); ?>',
								searchingText: '<?php _e('Searching', 'cqpim'); ?>'
							});						
							
						});
					
					</script>
				
				</div>
			
			
			
			<?php } if(!empty($quote_form) && isset( $_GET['page'] ) && $_GET['page'] == 'quote_form' ) { ?>
			
			<?php 
			
			$user = wp_get_current_user();
			
			$client_logs = get_post_meta($assigned, 'client_logs', true);
			
			if(empty($client_logs)) {
			
				$client_logs = array();
				
			}
			
			$now = time();
			
			$client_logs[$now] = array(
			
				'user' => $user->ID,
				
				'page' => __('Client Dashboard Quotes Page', 'cqpim')
			
			);
			
			update_post_meta($assigned, 'client_logs', $client_logs);
				
			?>
			
				<div style="width:100%; background:0" class="cqpim-dash-item">
				
					<h3><?php _e('Request a Quote', 'cqpim'); ?></h3>
					
					<div class="cqpim-dash-item-inside">
					
					<?php
			
					$form = get_option('cqpim_backend_form');

					$form_data = get_post_meta($form, 'builder_data', true);
					
					if(!empty($form_data)) {

						$form_data = json_decode($form_data);

						$fields = $form_data;
					
					}

					echo '<form id="cqpim_backend_form">';

					if(!empty($fields)) {

						echo '<div id="cqpim_backend_quote">';
						
						//echo '<pre>';
						
						//print_r($fields);
						
						//echo '</pre>';

						foreach($fields as $field) {
						
							$id = strtolower($field->label);
							
							$id = str_replace(' ', '_', $id);
							
							$id = str_replace('-', '_', $id);
							
							$id = preg_replace('/[^\w-]/', '', $id);
							
							if(!empty($field->required) && $field->required == 1) {
							
								$required = 'required';
								
								$ast = '<span style="color:#F00">*</span>';
								
							} else {
							
								$required = '';
								
								$ast = '';
								
							}
							
							echo '<div style="padding-bottom:12px" class="cqpim_form_item">';
							
							if($field->type != 'header') {
							
								echo '<label style="display:block; padding-bottom:5px" for="' . $id . '">' . $field->label . ' ' . $ast . '</label>';
							
							}
						
							if($field->type == 'header') {
							
								echo '<' . $field->subtype . ' class="' . $field->className . '">' . $field->label . '</' . $field->subtype . '>';
							
							} elseif($field->type == 'text') {
							
								echo '<input type="text" class="' . $field->className . '" id="' . $id . '" ' . $required . ' />';
							
							} elseif($field->type == 'website') {
							
								echo '<input type="url" class="' . $field->className . '" id="' . $id . '" ' . $required . ' />';
							
							} elseif($field->type == 'number') {
							
								echo '<input type="number" class="' . $field->className . '" id="' . $id . '" ' . $required . ' />';
							
							} elseif($field->type == 'textarea') {
							
								echo '<textarea class="' . $field->className . '" id="' . $id . '" ' . $required . '></textarea>';
							
							} elseif($field->type == 'date') {
							
								echo '<input class="' . $field->className . '" type="date" id="' . $id . '" ' . $required . ' />';
							
							} elseif($field->type == 'email') {
							
								echo '<input type="email" class="' . $field->className . '" id="' . $id . '" ' . $required . ' />';
							
							} elseif($field->type == 'file') {
				
								$multiple = isset($field->multiple) ? 'true' : 'false';
								
								echo '<input type="file" class="cqpim-file-upload" name="async-upload" id="' . $id . '" data-multiple="' . $multiple . '"/>';
								
								echo '<div id="upload_messages_' . $id . '"></div>';
								
								echo'<input type="hidden" name="image_id" id="upload_' . $id . '">';
								
								echo '<input type="hidden" name="action" value="image_submission">';
							
							} elseif($field->type == 'checkbox') {
							
								if(!empty($field->toggle) && $field->toggle == true) {
								
									$toggle = 'toggle="true"';
								
								} else {
								
									$toggle = '';
									
								}
							
								echo '<input type="checkbox" ' . $toggle . ' class="' . $field->className . '" value="' . $option->value . '" name="' . $id . '" /> ' . $option->label . '<br />';
							
							} elseif($field->type == 'checkbox-group') {
								
								$options = $field->values;
								
								foreach($options as $option) {
							
									echo '<input type="checkbox" class="' . $field->className . '" value="' . $option->value . '" name="' . $id . '" /> ' . $option->label . '<br />';
									
								}
							
							} elseif($field->type == 'radio-group') {
							
								$options = $field->values;
								
								foreach($options as $option) {
							
									echo '<input type="radio" class="' . $field->className . '" value="' . $option->value . '" name="' . $id . '" ' . $required . ' /> ' . $option->label . '<br />';
									
								}
							
							} elseif($field->type == 'select') {
							
								$options = $field->values;
								
								echo '<select class="' . $field->className . '" id="' . $id . '" ' . $required . '>';
								
									foreach($options as $option) {	
								
										echo '<option value="' . $option->value . '">' . $option->label . '</option>';
										
									}
								
								echo '</select>';
							
							}
							
							if(!empty($field->other) && $field->other == 1) {
							
								echo '<br />';
							
								echo __('Other:', 'cqpim') . '<input style="width:100%" type="text" id="' . $id . '_other" />';
							
							}
							
							if(!empty($field->description)) {
							
								echo '<p>' . $field->description . '</p>';
							
							}
							
							echo '</div>';
						
						}
						
						echo '<br />';
						
						echo '<input type="submit" id="cqpim_submit_backend" value="' . __('Submit Quote Request', 'cqpim') . '" /><br /><div id="form_spinner" style="clear:both; display:none; background:url(' . plugins_url() . '/cqpim/css/img/ajax-loader.gif) center center no-repeat; width:16px; height:16px; padding:10px 0 0 5px; margin-top:15px"></div>';

						echo '<div style="margin-top:20px" id="cqpim_submit_backend_messages"></div>';
						
						echo '</div>';
						
					} else {

						echo '<p>' . __('You have not added any fields to the selected form', 'cqpim') . '</p>';
						
					}

					echo '</form>';	
			
			?>
			
					</div>
					
				</div>			
			
			<?php } ?>
			
			<?php if(!empty($quote_form)) {
				
				echo '<input type="hidden" id="client" value="' . $assigned . '" />';
				
				wp_enqueue_script('upload-form-js', plugin_dir_url( __FILE__ ) . 'js/upload.js', array('jquery'), '0.1.0', true);

				$data = array(
				
					'upload_url' => admin_url('async-upload.php'),
					
					'ajax_url'   => admin_url('admin-ajax.php'),
					
					'nonce'      => wp_create_nonce('media-form'),
					
					'strings' => array(
					
						'uploading' => __('Uploading...', 'cqpim'),
						
						'success' => __('Successfully uploaded', 'cqpim'),
						
						'change' => __('Change File', 'cqpim'),
						
						'error' => __('Failed to upload file. It may not be on our list of allowed extensions. Please try again.', 'cqpim')
					
					),
				);

				wp_localize_script( 'upload-form-js', 'upload_config', $data );
				
				?>
			
				<script type="text/javascript">
				
					jQuery(document).ready(function() {
									
						jQuery('#cqpim_backend_form').on('submit', function(e) {
						
							e.preventDefault();
							
							var spinner = jQuery('#form_spinner');
							
							var client = jQuery('#client').val();
							
							<?php
							
							if(empty($fields)) {
							
								$fields = array();
								
							}
							
							foreach($fields as $field) {
							
								$id = strtolower($field->label);
								
								$id = str_replace(' ', '_', $id);
								
								$id = str_replace('-', '_', $id);
								
								$id = preg_replace('/[^\w-]/', '', $id);	
								
								if($field->type != 'header') {

									if($field->type == 'text' || 
									
										$field->type == 'number' || 
										
										$field->type == 'email' || 
										
										$field->type == 'textarea' ||
										
										$field->type == 'website' || 
										
										$field->type == 'select' || 
										
										$field->type == 'date' || 
										
										$field->type == 'number') {
									
										echo 'var ' . $id . ' = jQuery("#' . $id . '").val();';
										
										echo 'if(!' . $id . ') { ' . $id . ' = ""; };';
									
									} elseif($field->type == 'checkbox-group' || $field->type == 'checkbox') {

										echo 'var ' . $id . ' = jQuery("input[name=' . $id . ']:checked").map(function() { return jQuery(this).val(); }).get();';
										
										echo 'if(!' . $id . ') { ' . $id . ' = ""; };';

									} elseif($field->type == 'radio-group') {

										echo 'var ' . $id . ' = jQuery("input[name=' . $id . ']:checked").val();';
										
										echo 'if(!' . $id . ') { ' . $id . ' = ""; };';

									} elseif($field->type == 'file') {

										echo 'var ' . $id . ' = jQuery("#upload_' . $id . '").val();';
										
										echo 'if(!' . $id . ') { ' . $id . ' = ""; };';

									}
									
									if(!empty($field->other) && $field->other == 1) {
									
										echo 'var ' . $id . '_other = jQuery("#' . $id . '_other").val();';
									
									}
								
								}
							
							}
							
							?>
							
							var data = {
							
								'action' : 'cqpim_backend_quote_submission',
								
								'client' : client,
							
								<?php
								
								foreach($fields as $field) {
								
									if($field->type != 'header') {
								
										$id = strtolower($field->label);
										
										$id = str_replace(' ', '_', $id);
										
										$id = str_replace('-', '_', $id);
										
										$id = preg_replace('/[^\w-]/', '', $id);

										if($field->type == 'file') {
											
											echo "'cqpimuploader_" . $id . "' : " . $id . ",";
											
										} else {

											echo "'" . $id . "' : " . $id . ",";
											
										}
										
										if(!empty($field->other) && $field->other == 1) {
										
											echo 'var ' . $id . '_other = jQuery("#' . $id . '_other").val();';
										
										}	

									}
								
								}
								
								?>
							
							};
							
							jQuery.ajax({

								url: '<?php echo admin_url() . 'admin-ajax.php'; ?>',

								data: data,

								type: 'POST',

								dataType: 'json',

								beforeSend: function(){

									// show spinner

									spinner.show();

									// disable form elements while awaiting data

									jQuery('#cqpim_submit_backend').prop('disabled', true);

								},

							}).done(function(response){
							
								if(response.error == true) {
								
									spinner.hide();

									// re-enable form elements so that new enquiry can be posted

									jQuery('#cqpim_submit_backend').prop('disabled', false);
								
									jQuery('#cqpim_submit_backend_messages').html(response.message);
									
								} else {

									spinner.hide();

									// re-enable form elements so that new enquiry can be posted

									jQuery('#cqpim_submit_backend').prop('disabled', false);
									
									jQuery('#cqpim_submit_backend_messages').html(response.message);
								
								}

							});
						
						});
					
					});
				
				</script>			
			
			<?php } ?>
			
			<?php if(isset( $_GET['page'] ) && $_GET['page'] == 'add-envato-purchase'  ) { ?>
			
				<?php

					$user = wp_get_current_user();

					$client_logs = get_post_meta($assigned, 'client_logs', true);

					if(empty($client_logs)) {

						$client_logs = array();
						
					}

					$now = time();

					$client_logs[$now] = array(

						'user' => $user->ID,
						
						'page' => __('Client Dashboard Add Envato Item Page', 'cqpim')

					);
					
					update_post_meta($assigned, 'client_logs', $client_logs);

					$user = wp_get_current_user();
					
					$args = array(
					
						'post_type' => 'cqpim_client',
						
						'posts_per_page' => -1,
						
						'post_status' => 'private',
						
					);
					
					$clients = get_posts($args);
					
					foreach($clients as $client) {
					
						$client_details = get_post_meta($client->ID, 'client_details', true);
						
						$client_user_id = $client_details['user_id'];
						
						if($user->ID === $client_user_id) {
						
							$client_object_id = $client->ID;
						
						}
					
					}
					
					if(empty($client_object_id)) {
					
						foreach($clients as $client) {
						
							$client_ids = get_post_meta($client->ID, 'client_ids', true);
							
							if(in_array($user->ID, $client_ids)) {
							
								$client_object_id = $client->ID;
								
								$client_type = 'contact';
							
							}
						
						} 			
					
					}
					
					$envato_details = get_post_meta($client_object_id, 'envato_details', true);	
					
				?>
						
					<?php if(empty($envato_details)) { ?>
					
						<p><?php _e('You do not have a valid licence for any of our items, or you haven\'t registered them', 'cqpim'); ?></p>
						
					<?php } else {
					
						echo '<table class="milestones dash">';
						
							echo '<thead>';
							
								echo '<tr><th style="border-left:1px solid #ececec">' . __('Item ID', 'cqpim') . '</th><th>' . __('Item Name', 'cqpim') . '</th><th>' . __('Purchase Date', 'cqpim') . '</th><th>' . __('Licence', 'cqpim') . '</th><th>' . __('Support Expiry', 'cqpim') . '</th><th>' . __('Purchase Code', 'cqpim') . '</th></tr>';
							
							echo '</thead>';
							
							echo '<tbody>';
							
								foreach($envato_details as $purchase) {
								
									echo '<tr>';
									
									echo '<td style="border-left:1px solid #ececec">' . $purchase['item_id'] . '</td>';
									
									echo '<td>' . $purchase['item_name'] . '</td>';
									
									echo '<td>' . $purchase['created_at'] . '</td>';
									
									echo '<td>' . $purchase['licence'] . '</td>';
									
									if(!empty($purchase['supported_until'])) {
									
										echo '<td>' . $purchase['supported_until'] . '</td>';
									
									} else {
									
										echo '<td>' . __('EXPIRED', 'cqpim-envato') . '</td>';
									
									}
									
									echo '<td>' . $purchase['purchase_code'] . '</td>';
									
									echo '</tr>';
								
								}
							
							echo '</tbody>';
						
						echo '</table>';	
					
					} ?>
					
					<br /><br />
					
					<a href="#" id="add-envato-code-trigger" class="cqpim-dash-button medium"><?php _e('Add Purchase Code', 'cqpim'); ?></a>
					
					<div id="add-envato-code-container" style="display:none">
					
						<div id="add-envato-code-div">
						
							<div style="padding:12px">
						
								<h3><?php _e('Add Envato Purchase Code', 'cqpim'); ?></h3>
								
								<p><?php _e('Enter the purchase code of the item that you would like to add to your account.', 'cqpim'); ?></p>
								
								<input style="width:92%; padding:2.5%" type="text" id="purchase_code" />
								
								<br /><br />
								
								<button class="cancel-colorbox"><?php _e('Cancel', 'cqpim'); ?></button>
								
								<button id="add-envato-code" class="metabox-add-button"><?php _e('Add Purchase Code', 'cqpim'); ?></button>
								
								<div class="clear"></div>
								
								<div style="margin-top:20px; display:none" id="login_messages"></div>
							
							</div>
						
						</div>
					
					</div>			
			
			<?php } ?>
			
			<?php if(isset( $_GET['page'] ) && $_GET['page'] == 'add-support-ticket'  ) { 
				
				$string = cqpim_random_string(10);
				
				$user = wp_get_current_user();

				$client_logs = get_post_meta($assigned, 'client_logs', true);

				if(empty($client_logs)) {

					$client_logs = array();
					
				}

				$now = time();

				$client_logs[$now] = array(

					'user' => $user->ID,
					
					'page' => __('Client Dashboard Add Support Ticket Page', 'cqpim')

				);
				
				update_post_meta($assigned, 'client_logs', $client_logs);
			
				?>

				<div id="cqpim_backend_quote">
					
						<input type="hidden" name="action" value="new_ticket" />
						
						<?php
						
						include_once(ABSPATH.'wp-admin/includes/plugin.php');
					
						if(is_plugin_active('cqpim-envato/cqpim-envato.php')) {	
						
							$messages = get_option('raise_support_messages');
							
							$messages = str_replace('%%TIME%%', current_time('l dS F, g:iA'), $messages);
							
							echo $messages;

							$items = get_option('cqpim_envato_items'); ?>
							
							<h4><?php _e('Which product do you need help with?', 'cqpim'); ?></h4>
							
							<select name="ticket_item" id="ticket_item" required >
							
								<option value=""><?php _e('Choose a product...', 'cqpim'); ?></option>
								
								<?php
								
								foreach($items as $item) {
								
									echo '<option value="' . $item['id'] . '">' . $item['name'] . '</option>';
								
								}
								
								?>
							
							</select>
							
							<div style="margin-top:20px" id="login_messages"></div>
							
							<input type="hidden" id="reject_reason" value="" />
							
							<?php

						}
						
						?>
					
						<h4><?php _e('Ticket Title:', 'cqpim'); ?></h4>
				
						<input style="width:100%" type="text" id="ticket_title" required />

						<h4><?php _e('Ticket Priority:', 'cqpim'); ?></h4>
							
						<select style="width:100%" id="ticket_priority_new" name="ticket_priority_new">
						
							<option value="low"><?php _e('Low', 'cqpim'); ?></option>
							
							<option value="normal"><?php _e('Normal', 'cqpim'); ?></option>
							
							<option value="high"><?php _e('High', 'cqpim'); ?></option>
							
							<option value="immediate"><?php _e('Immediate', 'cqpim'); ?></option>
						
						</select>	
						
						<?php
						
							$_SESSION['upload_ids'] = array();
							
							$_SESSION['ticket_changes'] = array();
						
						?>

						<h4><?php _e('Upload Files', 'cqpim'); ?></h4>
						
						<form id="upload" method="post" enctype="multipart/form-data">
						
							<div id="drop">
								
								<?php _e('Drop Here', 'cqpim'); ?>

								<a><?php _e('Browse', 'cqpim'); ?></a>
								
								<input type="file" name="upl" multiple />
								
							</div>
							
							<ul>
								
							</ul>
						
						</form>

						<h4><?php _e('Details', 'cqpim'); ?></h4>				

						<textarea style="width:97.7%; height:300px; padding:1%" id="ticket_update_new" name="ticket_update_new" required ></textarea>
					
						<div class="clear"></div>
					
						<input id="support-submit" type="submit" class="metabox-add-button" style="margin-right:0" value="<?php _e('Create Ticket', 'cqpim'); ?>" />
					
					<br />
				
				</div>
			
			<?php } ?>
			
			<?php if(isset( $_GET['page'] ) && $_GET['page'] == 'settings') { ?>
			
			<?php 
			
			$user = wp_get_current_user();

			$client_logs = get_post_meta($assigned, 'client_logs', true);

			if(empty($client_logs)) {

				$client_logs = array();
				
			}

			$now = time();

			$client_logs[$now] = array(

				'user' => $user->ID,
				
				'page' => __('Client Dashboard Settings Page', 'cqpim')

			);
			
			update_post_meta($assigned, 'client_logs', $client_logs);
			
			$client_settings = get_option('allow_client_settings');
			
			if($client_settings == 1) { ?>
				
					<div <?php if($dash_type != 'inc') { echo 'style="width:100%; background:0"'; } ?> class="cqpim-dash-item">
					
						<h3><?php _e('Settings', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">

					<div id="cqpim_backend_quote">
					
						<?php
						
							if($client_type == 'admin') {
													
								$client_details = get_post_meta($assigned, 'client_details', true);
								
							} else {
							
								$client_details = get_post_meta($assigned, 'client_details', true);
							
								$client_contacts = get_post_meta($assigned, 'client_contacts', true);
								
								foreach($client_contacts as $key => $contact) {
								
									if($key == $user_id) {
									
										$client_details['client_telephone'] = $contact['telephone'];
										
										$client_details['client_contact'] = $contact['name'];
										
										$client_details['client_email'] = $contact['email'];
									
									}
								
								}
							
							}
						
						?>
				
						<form id="client_settings">
						
							<h4><?php _e('My Details', 'cqpim'); ?></h3>
							
							<label for="client_email"><?php _e('Email Address', 'cqpim'); ?></label>
							
							<input style="width:98%; padding:1%" type="text" id="client_email" name="client_email" value="<?php echo $user->user_email; ?>" required />
							
							<label for="client_phone"><?php _e('Telephone', 'cqpim'); ?></label>
							
							<input style="width:98%; padding:1%" type="text" id="client_phone" name="client_phone" value="<?php echo isset($client_details['client_telephone']) ? $client_details['client_telephone'] : ''; ?>" required />
							
							<label for="client_email"><?php _e('Display Name', 'cqpim'); ?></label>
							
							<input style="width:98%; padding:1%" type="text" id="client_name" name="client_name" value="<?php echo $user->display_name; ?>" required />
							
							<h4 style="margin-top:20px"><?php _e('Company Details', 'cqpim'); ?></h3>
							
							<label for="company_name"><?php _e('Company Name', 'cqpim'); ?></label>
							
							<input style="width:98%; padding:1%" type="text" id="company_name" name="company_name" value="<?php echo isset($client_details['client_company']) ? $client_details['client_company'] : ''; ?>" required />
							
							<label for="company_address"><?php _e('Company Address', 'cqpim'); ?></label>
							
							<textarea style="width:98%; padding:1%; height:100px" id="company_address" name="company_address" required ><?php echo isset($client_details['client_address']) ? $client_details['client_address'] : ''; ?></textarea>

							<label for="company_postcode"><?php _e('Company Postcode', 'cqpim'); ?></label>
							
							<input style="width:98%; padding:1%" type="text" id="company_postcode" name="company_postcode" value="<?php echo isset($client_details['client_postcode']) ? $client_details['client_postcode'] : ''; ?>" required />							
							
							<h4 style="margin-top:20px"><?php _e('Change Password', 'cqpim'); ?></h3>
							
							<label for="client_pass"><?php _e('New Password', 'cqpim'); ?></label>
							
							<input style="width:98%; padding:1%" type="password" id="client_pass" name="client_pass" value="" />
							
							<label for="client_pass_rep"><?php _e('Repeat New Password', 'cqpim'); ?></label>
							
							<input style="width:98%; padding:1%" type="password" id="client_pass_rep" name="client_pass_rep" value=""  />
							
							<h4 style="margin-top:20px"><?php _e('Email Notification Preferences', 'cqpim'); ?></h4>
							
							<p><strong><?php _e('Tasks', 'cqpim'); ?></strong></p>
							
							<?php 
							
							if($client_type == 'admin') { 
							
								$notifications = get_post_meta($assigned, 'client_notifications', true);
							
							} else {
								
								$client_contacts = get_post_meta($assigned, 'client_contacts', true);
								
								$notifications = $client_contacts[$user->ID]['notifications'];
								
							}
							
							$no_tasks = isset($notifications['no_tasks']) ? $notifications['no_tasks']: 0;
							
							$no_tasks_comment = isset($notifications['no_tasks_comment']) ? $notifications['no_tasks_comment']: 0;
							
							$no_tickets = isset($notifications['no_tickets']) ? $notifications['no_tickets']: 0;
							
							$no_tickets_comment = isset($notifications['no_tickets_comment']) ? $notifications['no_tickets_comment']: 0;
							
							?>
							
							<input type="checkbox" name="no_tasks" id="no_tasks" value="1" <?php if($no_tasks == 1) { echo 'checked="checked"'; } ?> /> <?php _e('Disable all task notification emails.', 'cqpim'); ?>
							
							<br />
							
							<input type="checkbox" name="no_tasks_comment" id="no_tasks_comment" value="1" <?php if($no_tasks_comment == 1) { echo 'checked="checked"'; } ?>  /> <?php _e('Only notify me if a task has a new comment added.', 'cqpim'); ?>
							
							<br />
							
							<p><strong><?php _e('Support Tickets', 'cqpim'); ?></strong></p>
							
							<input type="checkbox" name="no_tickets" id="no_tickets" value="1" <?php if($no_tickets == 1) { echo 'checked="checked"'; } ?>  /> <?php _e('Disable all ticket notification emails.', 'cqpim'); ?>
							
							<br />
							
							<input type="checkbox" name="no_tickets_comment" id="no_tickets_comment" value="1" <?php if($no_tickets_comment == 1) { echo 'checked="checked"'; } ?>  /> <?php _e('Only notify me if a ticket has a new comment added.', 'cqpim'); ?>
							
							<br />
							
							<input style="width:100%" type="hidden" id="client_type" name="client_type" value="<?php echo $client_type; ?>" />
							
							<input style="width:100%" type="hidden" id="client_object" name="client_object" value="<?php echo $assigned; ?>" />
							
							<input style="width:100%" type="hidden" id="client_user_id" name="client_user_id" value="<?php echo $user->ID; ?>" />
							
							<br />
							
							<input style="float:left; margin-left:0" type="submit" id="client_settings_submit" class="metabox-add-button" value="<?php _e('Update Settings', 'cqpim'); ?>" />
						
							<div class="clear"></div>
							
							<div id="settings_spinner" style="clear:both; display:none; background:url(<?php echo plugins_url(); ?>/cqpim/css/img/ajax-loader.gif) center center no-repeat; width:16px; height:16px; padding:10px 0 0 5px; margin-top:15px"></div>
							
							<div class="clear"></div>
							
							<br />
							
							<div id="settings_messages"></div>
							
						</form>
					
					</div>
				
						</div>
						
					</div>
				
				<?php } ?>	
			
			<?php } ?>	
			
			<?php if(isset( $_GET['page'] ) && $_GET['page'] == 'contacts') { ?>
			
			<?php 
			
			$client_settings = get_option('allow_client_users');
			
			$user = wp_get_current_user();

			$client_logs = get_post_meta($assigned, 'client_logs', true);

			if(empty($client_logs)) {

				$client_logs = array();
				
			}

			$now = time();

			$client_logs[$now] = array(

				'user' => $user->ID,
				
				'page' => __('Client Dashboard Contacts Page', 'cqpim')

			);
			
			update_post_meta($assigned, 'client_logs', $client_logs);
			
			if($client_settings == 1) { ?>
				
					<div <?php if($dash_type != 'inc') { echo 'style="width:100%; background:0"'; } ?> class="cqpim-dash-item">
					
						<h3><?php _e('Contacts', 'cqpim'); ?></h3>
						
						<div class="cqpim-dash-item-inside">
					
				<p><?php _e('If you would like to give multiple users at your organisation access to your client dashboard, you can do so here.', 'cqpim'); ?></p>
				
				<?php 
				
				$client_contacts = get_post_meta($assigned, 'client_contacts', true);
				
				if(empty($client_contacts)) {
				
					echo '<p>' . __('You have not added any additional contacts', 'cqpim') . '</p>';
				
				} else {
				
					foreach($client_contacts as $key => $contact) {
						
						echo '<div class="team_member">';
						
						echo '<div class="team_delete"><button style="margin-right:5px" class="edit-milestone" value="'. $key . '"><img title="Edit Contact" src="' . plugin_dir_url( __FILE__ ) . 'img/edit.png"></button><button class="delete_team" value="' . $key . '"><img title="Remove Client Contact" src="' . plugin_dir_url( __FILE__ ) . 'img/delete.png"></button><div id="ajax_spinner_team_' . $key . '" class="ajax_spinner" style="display:none"></div></div>';
						
						$value = get_option('cqpim_disable_avatars');
						
						if(empty($value)) {
							
							echo '<div class="user_avatar" style="float:left">';
						
								echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 
						
							echo '</div>';
						
						} 
						
						if(!empty($value)) {
						
							echo '<div style="padding-left:0" class="team_details">';
						
						} else {
							
							echo '<div class="team_details">';
							
						}
						
						echo '<strong>' . __('Name:', 'cqpim') . ' </strong>' . $contact['name'] . '<br />';
						
						echo '<strong>' . __('User ID:', 'cqpim') . ' </strong>' . $contact['user_id'] . '<br />';
						
						echo '<strong>' . __('Email:', 'cqpim') . ' </strong>' . $contact['email'] . '<br />';
						
						echo '<strong>' . __('Telephone:', 'cqpim') . ' </strong>' . $contact['telephone'] . '<br />';
						
						echo '</div>';
						
						echo '<div class="clear"></div>';
						
						echo '</div>';			
					
					}
					
					echo '<div class="clear"></div>';
					
					foreach($client_contacts as $key => $contact) { ?>
					
						<div id="contact_edit_container_<?php echo $key; ?>" style="display:none">
						
							<div id="contact_edit_<?php echo $key; ?>" class="contact_edit">
							
								<div style="padding:12px">
							
									<h3><?php _e('Edit Contact', 'cqpim'); ?> - <?php echo $contact['name']; ?></h3>
									
									<label for="contact_name_<?php echo $key; ?>"><?php _e('Contact Name', 'cqpim'); ?></label>
									
									<input type="text" id="contact_name_<?php echo $key; ?>" name="contact_name_<?php echo $key; ?>" value="<?php echo $contact['name']; ?>" />
									
									<br /><br />
									
									<label for="contact_email_<?php echo $key; ?>"><?php _e('Contact Email', 'cqpim'); ?></label>
									
									<input type="text" id="contact_email_<?php echo $key; ?>" name="contact_email_<?php echo $key; ?>" value="<?php echo $contact['email']; ?>" />
									
									<br /><br />
									
									<label for="contact_telephone_<?php echo $key; ?>"><?php _e('Contact Telephone', 'cqpim'); ?></label>
									
									<input type="text" id="contact_telephone_<?php echo $key; ?>" name="contact_telephone_<?php echo $key; ?>" value="<?php echo $contact['telephone']; ?>" />
									
									<br /><br />
									
									<h3><?php _e('Reset Password', 'cqpim'); ?></h3>
									
									<input class="pass" type="password" id="new_password_<?php echo $key; ?>" name="new_password_<?php echo $key; ?>" placeholder="<?php _e('Enter new password', 'cqpim'); ?>" />
									
									<br /><br />
									
									<input class="pass" type="password" id="confirm_password_<?php echo $key; ?>" name="confirm_password_<?php echo $key; ?>" placeholder="<?php _e('Confirm new password', 'cqpim'); ?>" />
									
									<br /><br />
									
									<input type="checkbox" id="send_new_password_<?php echo $key; ?>" name="send_new_password_<?php echo $key; ?>" value="1" /> <?php _e('Send the contact\'s new password by email', 'cqpim'); ?>
									
									<br /><br />
									
									<input class="pass" type="hidden" id="pass_type_<?php echo $key; ?>" name="pass_type_<?php echo $key; ?>" value="contact" />
									
									<div id="client_team_messages_<?php echo $key; ?>"></div>							
									
									<button class="cancel-colorbox"><?php _e('Cancel', 'cqpim'); ?></button>
												
									<button id="contact_edit_submit_<?php echo $key; ?>" class="metabox-add-button contact_edit_submit" value="<?php echo $key; ?>"><?php _e('Edit Contact', 'cqpim'); ?><span id="ajax_spinner_contact_<?php echo $key; ?>" class="ajax_loader" style="display:none"></span></button>
							
								</div>
							
							</div>
						
						</div>
					
					<?php
					
					}
					
				} 
			
			} if($client_settings == 1) { ?>
			
				<button style="float:left; margin-left:0" id="add_client_team" class="metabox-add-button" value="<?php echo $client_user; ?>"><?php _e('Add Contact', 'cqpim'); ?></button>

			<?php } ?>
			
			<div class="clear"></div>
			
			<div id="add_client_team_ajax_container" style="display:none">
			
				<div id="add_client_team_ajax">
				
					<div style="padding:12px">
				
						<h3><?php _e('Add Contact', 'cqpim'); ?></h3>
						
						<p><?php _e('Adding a contact will create a new login and give the user access to the client dashboard.', 'cqpim'); ?></p>
						
						<label for="contact_name"><?php _e('Contact Name', 'cqpim'); ?></label>
						
						<input type="text" id="contact_name" name="contact_name" />
						
						<br /><br />
						
						<label for="contact_email"><?php _e('Contact Email', 'cqpim'); ?></label>
						
						<input type="text" id="contact_email" name="contact_email" />
						
						<br /><br />
						
						<label for="contact_telephone"><?php _e('Contact Telephone', 'cqpim'); ?></label>
						
						<input type="text" id="contact_telephone" name="contact_telephone" />
						
						<br /><br />
						
						<input type="checkbox" id="send_contact_details" name="send_contact_details" value="1" /> <?php _e('Send the contact login details by email', 'cqpim'); ?>
						
						<br /><br />
						
						<input type="hidden" id="post_ID" name="post_id" value="<?php echo $assigned; ?>" />
						
						<div id="client_team_messages"></div>
						
						<button class="cancel-colorbox"><?php _e('Cancel', 'cqpim'); ?></button>
									
						<button id="add_client_team_submit" class="metabox-add-button"><?php _e('Add Client Contact', 'cqpim'); ?><span id="ajax_spinner_add_contact" class="ajax_loader" style="display:none"></span></button>

					</div>
				
				</div>
			
			</div>
			
					</div>
					
				</div>
			
			<?php } ?>
			
		
		</div>
		
	</div>
	
<?php

	get_footer();
	
 ?>
	


