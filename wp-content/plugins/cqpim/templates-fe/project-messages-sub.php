<?php

	$user = wp_get_current_user();

	$client_logs = get_post_meta($assigned, 'client_logs', true);

	if(empty($client_logs)) {

		$client_logs = array();
		
	}
	
	$title = get_the_title();
	
	$title = str_replace('Private:', '', $title);

	$now = time();

	$client_logs[$now] = array(

		'user' => $user->ID,
		
		'page' => sprintf(__('Project %1$s - %2$s (Messages Page)', 'cqpim'), get_the_ID(), $title)

	);
	
	update_post_meta($assigned, 'client_logs', $client_logs);

?>

	<div class="cqpim-project-box">
	
		<div class="title">
			
			<h3><?php _e('Project Messages', 'cqpim'); ?></h3>
		
		</div>
		
		<div class="content">
		
			<?php
			
			$project_messages = get_post_meta($post->ID, 'project_messages', true);
			
			if(!$project_messages) {
			
				echo '<p style="padding:15px;">' . __('No messages to show...', 'cqpim') . '</p>';
			
			} else {
			
				echo '<div class="project_messages">';
				
				$project_messages = array_reverse($project_messages);
				
				foreach($project_messages as $key => $message) { 
				
					$user_object = get_user_by('id', $message['author']);
				
					$email = $user_object->user_email;

					if($message['visibility'] != 'internal') {
					
					?>
					
						<div class="project_message">
						
					<?php 
					
					$value = get_option('cqpim_disable_avatars');
					
					if(empty($value)) {
						
						echo '<div class="user_avatar">';
					
							echo get_avatar( $user_object->ID, 80, '', false, array('force_display' => true) ); 
					
						echo '</div>';
					
					} 
					
					?> 
					
					<?php if(!empty($value)) { ?>
					
						<div style="padding-left:0" class="message_details">
					
					<?php } else { ?>
					
						<div class="message_details">
					
					<?php } ?>
							
								<h4>
								
									<?php echo $message['by']; ?>						
								
								</h4>
								
								<span><?php echo date(get_option('cqpim_date_format') . ' H:i', $message['date']); ?></span>
								
								<div class="message">
								
									<?php echo wpautop($message['message']); ?>
								
								</div>
							
							</div>
							
							<div class="clear"></div>
						
						</div>
					
					<?php }
					
					}
				
				echo '</div>';		
			
			} ?>
			
			<button id="add_message_trigger" class="metabox-add-button"><?php _e('Send Message', 'cqpim'); ?></button>
			
			<div class="clear"></div>
			
			<div style="display:none">
			
				<div id="add_message">
				
					<div style="padding:12px; text-align:left">
				
						<h3><?php _e('Send Message', 'cqpim'); ?></h3>
						
						<input type="hidden" id="message_who" value="client" />
						
						<input type="hidden" id="add_message_visibility" name="add_message_visibility" value="all" />
						
						<input type="hidden" id="post_ID" name="post_ID" value="<?php echo $post->ID; ?>" />
						
						<p><strong><?php _e('Message', 'cqpim'); ?></strong></p>
						
						<textarea style="width:95%; height:300px;min-width:400px" id="add_message_text" name="add_message_text"></textarea>
						
						<br />
						
						<div id="message_messages"></div>
						
						<button id="add_message_ajax" class="metabox-add-button"><?php _e('Send Message', 'cqpim'); ?> <div id="ajax_spinner_message" class="ajax_loader" style="display:none"></div></button>
						
						<div class="clear"></div>
					
					</div>
				
				</div>
			
			</div>	
		
		</div>
		
	</div>
	
	<div class="clear"></div>