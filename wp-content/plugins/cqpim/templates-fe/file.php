<?php 

if (!current_user_can( 'cqpim_client' )) {

	$login_page = get_option('cqpim_login_page');
	
	$server = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : '';
	
	$protocol = $server == 'on' ? 'https' : 'http';
	
	$request_url = $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	
	$site_url = site_url();
	
	$redirect = str_replace($site_url, '', $request_url);
	
	$url = get_the_permalink($login_page) . '?redirect=' . $redirect;
	
	wp_redirect($url, 302);

} else {

	$user = wp_get_current_user(); 
	
	$login_page_id = get_option('cqpim_login_page');
	
	$login_url = get_the_permalink($login_page_id);
	
	$user_id = $user->ID;
	
	$dash_type = get_option('client_dashboard_type');
	
	$quote_form = get_option('cqpim_backend_form');
	
	// Assignment
	
	$args = array(
	
		'post_type' => 'cqpim_client',
		
		'posts_per_page' => -1,
		
		'post_status' => 'private'
	
	);
	
	$members = get_posts($args);
	
	foreach($members as $member) {
	
		$team_details = get_post_meta($member->ID, 'client_details', true);
		
		if($team_details['user_id'] == $user->ID) {
		
			$assigned = $member->ID;
			
			$client_type = 'admin';
		
		}
	
	} 
	
	if(empty($assigned)) {
	
		foreach($members as $member) {
		
			$team_ids = get_post_meta($member->ID, 'client_ids', true);
			
			if(!is_array($team_ids)) {
			
				$team_ids = array($team_ids);
			
			}
			
			if(in_array($user->ID, $team_ids)) {
			
				$assigned = $member->ID;
				
				$client_type = 'contact';
			
			}
		
		} 			
	
	}
	
	$client_details = get_post_meta($assigned, 'client_details', true);
	
	$client_ids = get_post_meta($assigned, 'client_ids', true);
	
	$parent_task = $post->post_parent;
	
	$ppid = get_post_meta($parent_task, 'project_id', true);
	
	$project_details = get_post_meta($ppid, 'project_details', true);
	
	$client_id = isset($project_details['client_id']) ? $project_details['client_id'] : '';
	
	if($parent_object->post_type == 'cqpim_support') {
	
		// Assignment

		$args = array(

			'post_type' => 'cqpim_client',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'

		);

		$members = get_posts($args);

		foreach($members as $member) {

			$team_details = get_post_meta($member->ID, 'client_details', true);
			
			if($team_details['user_id'] == $parent_object->post_author) {
			
				$client_id = $member->ID;
				
				$client_type = 'admin';
			
			}

		} 

		if(empty($client_id)) {

			foreach($members as $member) {
			
				$team_ids = get_post_meta($member->ID, 'client_ids', true);
				
				if(in_array($parent_object->post_author, $team_ids)) {
				
					$client_id = $member->ID;
					
					$client_type = 'contact';
				
				}
			
			} 			

		}		
	
	}
	
}
	
get_header();
	
 ?>

	<div style="padding-left:0; margin:0 auto; max-width:1100px; padding:20px; background:#fff" class="cqpim-client-dash" role="main">	
	
		<div style="background:0; padding:0 0 30px 0" class="cqpim-dash-header">
		
			<?php 
			
			$value = get_option('cqpim_disable_avatars');
			
			if(empty($value)) {
				
				echo '<div class="user_avatar" style="margin-right:20px">';
			
					echo get_avatar( $user->ID, 80, '', false, array('force_display' => true) ); 
			
				echo '</div>';
			
			} 
			
			?> 

			<h1><?php printf(__('Welcome back, %1$s', 'cqpim'), $user->display_name); ?><br /><span style="font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $email; ?></span> <span style="margin-left:10px;font-weight:700; font-size:12px; line-height:14px; padding:4px 8px" class="task_complete"><?php echo $client_details['client_company']; ?> <?php if($client_type == 'admin') { echo '&nbsp;&nbsp;(Main Contact)'; } ?></span></h1>
		
			<div class="clear"></div>
			
				<div>
			
				<?php $client_dash = get_option('cqpim_client_page'); ?>
			
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash); ?>"><?php _e('Dashboard', 'cqpim'); ?></a>
		
				<?php if(get_option('cqpim_messages_allow_client') == 1) { ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=messages'; ?>"><?php _e('Messages', 'cqpim'); ?></a>
				
				<?php } ?>				
				
				
				<?php if(!empty($quote_form)) { ?>
				
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=quote_form'; ?>"><?php _e('Request a Quote', 'cqpim'); ?></a>
				
				<?php } 
			
				$client_settings = get_option('allow_client_settings');
				
				if($client_settings == 1) { ?>				
					
					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=settings'; ?>"><?php _e('Settings', 'cqpim'); ?></a>

				<?php } 
				
				$client_settings = get_option('allow_client_users');
				
				if($client_settings == 1) { ?>	

					<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($client_dash) . '?page=contacts'; ?>"><?php _e('Users', 'cqpim'); ?></a>

				<?php } ?>
				
				<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo wp_logout_url($login_url); ?>"><?php _e('Log Out', 'cqpim'); ?></a>
				
			</div>
			
			<div class="clear"></div>
			
			<div>
						
			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '?page=summary&sub=updates'; ?>"><?php _e('Updates & Progress', 'cqpim'); ?></a>

			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '?page=summary&sub=milestones'; ?>"><?php _e('Milestones & Tasks', 'cqpim'); ?></a>
			
			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '?page=summary&sub=messages'; ?>"><?php _e('Messages', 'cqpim'); ?></a>

			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '?page=summary&sub=files'; ?>"><?php _e('Files', 'cqpim'); ?></a>
			
			<a style="width:auto; margin-right:12px" class="btn-printable" href="<?php echo get_the_permalink($ppid) . '?page=summary&sub=invoices'; ?>"><?php _e('Costs & Invoices', 'cqpim'); ?></a>			
			
			<div class="clear"></div>
			
			</div>
				
				<div class="clear"></div>

		</div>
		
		<div style="padding:0;">
		
			<div style="width:100%; background:0" class="cqpim-dash-item">
			
				<h3><?php the_title(); ?></h3>
				
				<div class="cqpim-dash-item-inside">
		
			<?php

				if($assigned == $client_id) { 
				
					if($post->post_mime_type == 'application/pdf') { ?>
				
					<iframe src="http://docs.google.com/gview?url=<?php echo wp_get_attachment_url($post->ID) ?>&embedded=true" class="cqpim_attachment_pdf" frameborder="0"></iframe> 
					
					<?php } elseif($post->post_mime_type == 'image/jpeg' || $post->post_mime_type == 'image/png' || $post->post_mime_type == 'image/gif') { ?>
					
						<div style="text-align:center">
					
							<img style="max-width:100%" src="<?php echo $post->guid; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
						
						</div>
						
					<?php }	else {

						the_content();
						
					}
					
				} else {
				
					echo '<h1 style="margin-top:0">' . __('ACCESS DENIED', 'cqpim') . '</h1>';
					
				}
			
			 ?>		

			</div>
			
			</div>

		</div><!-- #content -->
	
	</div>

<?php get_footer(); ?>



