<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="ie ie9" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php wp_title(); ?></title>   
    <?php wp_head(); ?>
</head>

<body style="padding-bottom:30px;" <?php body_class(); ?>>
	
	<div id="content" role="main">
	
		<?php
		
		$_SESSION['last_invoice'] = $post->ID;
		
		$user = wp_get_current_user(); 
			
		$user_id = $user->ID;
		
		$logo = get_option('company_logo');
				
		$logo_url = isset($logo['company_logo']) ? $logo['company_logo'] : '';
		
		$title = get_the_title();
				
		$title = str_replace('Protected: ', '', $title);
		
		$company_name = get_option('company_name');
		
		$company_address = get_option('company_address');
		
		$company_number = get_option('company_number');
		
		$company_address = str_replace(',', '<br />', $company_address);
		
		$company_postcode = get_option('company_postcode');
		
		$company_telephone = get_option('company_telephone');
		
		$company_accounts_email = get_option('company_accounts_email');
		
		$currency = get_option('currency_symbol');
				
		$vat_rate = get_post_meta($post->ID, 'tax_rate', true);
		
		$svat_rate = get_post_meta($post->ID, 'stax_rate', true);
				
		$tax_name = get_option('sales_tax_name');
		
		$tax_reg = get_option('sales_tax_reg');
		
		$stax_name = get_option('secondary_sales_tax_name');
		
		$stax_reg = get_option('secondary_sales_tax_reg');
		
		$invoice_terms = get_option('company_invoice_terms');
				
		if($vat_rate) {
				
			$vat_string = '';
					
		} else {
				
			$vat_string = '';
					
		} 
		
		$invoice_details = get_post_meta($post->ID, 'invoice_details', true);
		
		$invoice_payments = get_post_meta($post->ID, 'invoice_payments', true);
		
		if(empty($invoice_payments)) {
		
			$invoice_payments = array();
			
		}
		
		$received = 0;
		
		foreach($invoice_payments as $payment) {
		
			$amount = isset($payment['amount']) ? $payment['amount'] : 0;
			
			$received = $received + $amount;
		
		}
		
		$invoice_id = get_post_meta($post->ID, 'invoice_id', true);
		
		$client_contact = get_post_meta($post->ID, 'client_contact', true);
		
		$owner = get_user_by('id', $client_contact);
		
		$project_id = isset($invoice_details['project_id']) ? $invoice_details['project_id'] : '';
		
		$invoice_date = isset($invoice_details['invoice_date']) ? $invoice_details['invoice_date'] : '';

		$allow_partial = isset($invoice_details['allow_partial']) ? $invoice_details['allow_partial'] : '';
		
		if(is_numeric($invoice_date)) { $invoice_date = date(get_option('cqpim_date_format'), $invoice_date); } else { $invoice_date = $invoice_date; }
		
		$deposit = isset($invoice_details['deposit']) ? $invoice_details['deposit'] : '';
		
		$due = isset($invoice_details['due']) ? $invoice_details['due'] : '';
		
		if(is_numeric($due)) { $due = date(get_option('cqpim_date_format'), $due); } else { $due = $due; }
		
		if(!empty($project_id)) {
		
			$project_details = get_post_meta($project_id, 'project_details', true);
			
			$project_ref = isset($project_details['quote_ref']) ? $project_details['quote_ref'] : '';
			
		} else {
		
			$project_ref = __('N/A', 'cqpim');
			
		}

		$client_id = isset($invoice_details['client_id']) ? $invoice_details['client_id'] : '';
		
		$client_details = get_post_meta($client_id, 'client_details', true);
		
		$client_ids = get_post_meta($client_id, 'client_ids', true);
		
		$client_user_id = isset($client_details['user_id']) ? $client_details['user_id'] : '';
		
		$client_company = isset($client_details['client_company']) ? $client_details['client_company'] : '';
		
		$client_address = isset($client_details['client_address']) ? $client_details['client_address'] : '';
		
		$client_postcode = isset($client_details['client_postcode']) ? $client_details['client_postcode'] : '';
		
		$client_telephone = isset($client_details['client_telephone']) ? $client_details['client_telephone'] : '';
		
		$client_email = isset($client_details['client_email']) ? $client_details['client_email'] : '';
		
		$client_tax_name = isset($client_details['client_tax_name']) ? $client_details['client_tax_name']: '';
		
		$client_stax_name = isset($client_details['client_stax_name']) ? $client_details['client_stax_name']: '';
		
		$client_tax_reg = isset($client_details['client_tax_reg']) ? $client_details['client_tax_reg']: '';
		
		$client_stax_reg = isset($client_details['client_stax_reg']) ? $client_details['client_stax_reg']: '';
		
		$line_items = get_post_meta($post->ID, 'line_items', true);
		
		$totals = get_post_meta($post->ID, 'invoice_totals', true);
			
		$sub = isset($totals['sub']) ? $totals['sub'] : '';
			
		$vat = isset($totals['tax']) ? $totals['tax'] : '';
		
		$svat = isset($totals['stax']) ? $totals['stax'] : '';
			
		$total = isset($totals['total']) ? $totals['total'] : '';
		
		$invoice_footer = get_option('client_invoice_footer');
		
		$invoice_footer = cqpim_replacement_patterns($invoice_footer, $post->ID, 'invoice');
		
		$terms_over = isset($invoice_details['terms_over']) ? $invoice_details['terms_over'] : '';
		
		$on_receipt = isset($invoice_details['on_receipt']) ? $invoice_details['on_receipt'] : '';
		
		$pass = isset($_GET['pwd']) ? $_GET['pwd'] : '';
		
		$looper = get_post_meta($post->ID, 'looper', true);
		
		if(time() - $looper > 5 && in_array('cqpim_client', $user->roles)) {
		
			$user = wp_get_current_user();

			$client_logs = get_post_meta($client_id, 'client_logs', true);

			if(empty($client_logs)) {

				$client_logs = array();
				
			}

			$now = time();

			$client_logs[$now] = array(

				'user' => $user->ID,
				
				'page' => sprintf(__('Invoice - %1$s', 'cqpim'), $title)

			);
			
			update_post_meta($client_id, 'client_logs', $client_logs);
			
			update_post_meta($post->ID, 'looper', time());
		
		}
		
		if(current_user_can( 'edit_cqpim_invoices' ) OR $client_user_id == $user_id OR $pass == md5($post->post_password) OR in_array($user->ID, $client_ids)) { ?>
	
			<div style="width:350px; margin:0" class="quote_logo">
						
				<img src="<?php echo $logo['company_logo']; ?>" />
						
			</div>
						
			<div style="margin:0; padding:0;font-size:30px" class="quote_contacts">
						
				<?php _e('INVOICE', 'cqpim'); ?>
						
			</div>
						
			<div class="clear"></div>
			
			<br /><br />
			
			<?php

			$now = time();
			
			if(empty($on_receipt)) {
			
				if(empty($invoice_details['paid'])) {
				
					if($terms_over) {
					
						if($now > $terms_over) {
						
							echo '<div class="unsent_quote">' . __('THIS INVOICE IS OVERDUE', 'cqpim') . '</div><br /><br /><br />';		
						
						}
					
					}
				
				}
			
			}
			
			?>
			
			<?php if(!empty($invoice_details['paid']) && $invoice_details['paid'] == true) { ?>	
				
				<div style="margin:0 5%" class="contract-confirmed">
							
					<h5 style="margin:0"><?php _e('THIS INVOICE HAS BEEN PAID', 'cqpim'); ?></h5>
					
				</div>
				
				<br /><br />
						
			<?php } ?>
			
			<div>
			
				<div style="float:left;width:42%;background:#f9f9f9;padding:10px;text-align:left" class="invoice_company">

					<h3 style="margin:0"><?php _e('Invoice From:', 'cqpim'); ?></h3>
					
					<br />
					
						<?php echo $company_name; ?> <br />
						
						<?php echo wpautop($company_address); ?>
						
						<?php echo wpautop($company_postcode); ?>
					
					<p>
					
						<?php _e('Tel:', 'cqpim'); ?> <?php echo $company_telephone; ?><br />
									
						<?php _e('Email:', 'cqpim'); ?> <a href="<?php echo $company_accounts_email; ?>"><?php echo $company_accounts_email; ?></a><br />
						
						<?php if($company_number) { ?>
						
							<?php _e('Company Reg Number:', 'cqpim'); ?> <?php echo $company_number; ?>
						
						<?php } ?>
					
					</p>
				

				
				</div>
				
				<div style="float:right;width:42%;background:#f9f9f9;padding:10px;text-align:left" class="invoice_client">
				
					<h3 style="margin:0"><?php _e('Invoice To:', 'cqpim'); ?></h3>
					
					<br />
					
						<?php if(!empty($owner)) { echo __('FAO:', 'cqpim') . ' ' . $owner->display_name . '<br />'; } ?>
					
						<?php echo $client_company; ?> <br />
						
						<?php echo wpautop($client_address); ?>
						
						<?php echo wpautop($client_postcode); ?>
					
					<p>
					
						<?php if(!empty($owner)) { ?>
									
							<?php _e('Email:', 'cqpim'); ?> <a href="<?php echo $owner->user_email; ?>"><?php echo $owner->user_email; ?></a>
							
						<?php } else { ?>
						
							<?php _e('Email:', 'cqpim'); ?> <a href="<?php echo $client_email; ?>"><?php echo $client_email; ?></a>
						
						<?php } ?>
						
					</p>
					
					<?php if(!empty($client_tax_name)) { ?>
					
						<?php if(!empty($client_tax_name)) { ?>
						
							<?php printf(__('%1$s Reg Number: ', 'cqpim'), $client_tax_name); ?> <?php echo $client_tax_reg; ?>
							
						<?php } ?>
						
						<?php if(!empty($client_stax_name)) { ?>
						
							<br /><?php printf(__('%1$s Reg Number: ', 'cqpim'), $client_stax_name); ?> <?php echo $client_stax_reg; ?>
						
						<?php } ?>
					
					<?php } ?>
				
				</div>
				
				<div class="clear"></div>
			
			</div>
			
			<div class="clear"></div>
			
			<br />
			
			<table style="width:100%; border: 1px solid #fff; border-collapse:collapse" id="invoice_info">
			
				<thead>
				
					<tr style="background:#f9f9f9; border:1px solid #fff">
					
						<?php if($vat_rate) { ?>
					
						<th style="border:1px solid #fff"><?php echo get_option('sales_tax_name'); ?> <?php _e('Number', 'cqpim'); ?></th>
						
						<?php } ?>
						
						<?php if($svat_rate) { ?>
					
						<th style="border:1px solid #fff"><?php echo get_option('secondary_sales_tax_name'); ?> <?php _e('Number', 'cqpim'); ?></th>
						
						<?php } ?>
						
						<th style="border:1px solid #fff"><?php _e('Date', 'cqpim'); ?></th>
						
						<th style="border:1px solid #fff"><?php _e('Invoice #', 'cqpim'); ?></th>
						
						<th style="border:1px solid #fff"><?php if($due) { _e('Due', 'cqpim'); } else { _e('Terms', 'cqpim'); } ?></th>
						
						<th style="border:1px solid #fff"><?php _e('Project', 'cqpim'); ?></th>
					
					</tr>
				
				</thead>
				
				<tbody>
				
					<tr style="border:1px solid #fff">
						<?php if($vat_rate) { ?>
					
						<td style="border:1px solid #fff"><?php echo $tax_reg; ?></td>
						
						<?php } ?>
						
						<?php if($svat_rate) { ?>
					
						<td style="border:1px solid #fff"><?php echo $stax_reg; ?></td>
						
						<?php } ?>
						
						<td style="border:1px solid #fff"><?php echo $invoice_date; ?></td>
						
						<td style="border:1px solid #fff"><?php echo $invoice_id; ?></td>
						
						<?php if(empty($on_receipt)) { ?>
						
							<td style="border:1px solid #fff"><?php if($due) { echo $due; } else { echo $invoice_terms . ' ' . __('days', 'cqpim'); } ?></td>
						
						<?php } else { ?>
						
							<td style="border:1px solid #fff"><?php _e('Due on Receipt', 'cqpim'); ?></td>
						
						<?php } ?>
						
						<td style="border:1px solid #fff"><?php echo $project_ref; ?></td>
					
					</tr>
				
				</tbody>
			
			</table>
			
			<br />
			
			<table style="width:100%; border: 1px solid #fff; border-collapse:collapse;" id="invoice_items">
			
				<thead>
				
					<tr style="background:#f9f9f9">
						
						<th style="border:1px solid #fff"><?php _e('Qty', 'cqpim'); ?></th>
						
						<th style="border:1px solid #fff"><?php _e('Description', 'cqpim'); ?></th>
						
						<th style="border:1px solid #fff"><?php _e('Rate', 'cqpim'); ?></th>
						
						<th style="border:1px solid #fff"><?php _e('Total', 'cqpim'); ?></th>
					
					</tr>
				
				</thead>
				
				<tbody>
				
				<?php 
				
				if(empty($line_items)) {
				
					$line_items = array();
					
				}				
				
				foreach($line_items as $item) { ?>
				
					<tr>
						
						<td style="border:1px solid #fff"><?php echo $item['qty']; ?></td>
						
						<td style="border:1px solid #fff"><?php echo $item['desc']; ?></td>
						
						<td style="border:1px solid #fff"><?php echo cqpim_calculate_currency($post->ID, $item['price']); ?></td>
						
						<td style="border:1px solid #fff"><?php echo cqpim_calculate_currency($post->ID, $item['sub']); ?></td>
					
					</tr>
					
				<?php } ?>
				
					<tr>
						
						<td style="border:1px solid #fff" class="no_border" colspan="2"></td>
						
						<td class="no_border" style="text-align:right; border:1px solid #fff"><strong><?php if($vat_rate) { ?><?php _e('Subtotal:', 'cqpim'); ?><?php } else { ?><?php _e('TOTAL:', 'cqpim'); ?><?php } ?></strong></td>
						
						<td style="border:1px solid #fff"><?php echo cqpim_calculate_currency($post->ID, $sub); ?></td>
					
					</tr>
					
				<?php 
				
				$outstanding = $sub;
				
				if($vat_rate) { 
				
					$outstanding = $total;
					
					$tax_name = get_option('sales_tax_name'); ?>
					
					<tr>
						
						<td style="border:1px solid #fff" class="no_border" colspan="2"></td>
						
						<td class="no_border" style="text-align:right; border:1px solid #fff"><strong><?php echo $tax_name; ?>:</strong></td>
						
						<td style="border:1px solid #fff"><?php echo cqpim_calculate_currency($post->ID, $vat); ?></td>
					
					</tr>
					
					<?php if(!empty($svat_rate)) { ?>
					
						<tr>
							
							<td style="border:1px solid #fff" class="no_border" colspan="2"></td>
							
							<td class="no_border" style="text-align:right; border:1px solid #fff"><strong><?php echo $stax_name; ?>:</strong></td>
							
							<td style="border:1px solid #fff"><?php echo cqpim_calculate_currency($post->ID, $svat); ?></td>
						
						</tr>					
					
					<?php } ?>
					
					<tr>
						
						<td style="border:1px solid #fff" class="no_border" colspan="2"></td>
						
						<td class="no_border" style="text-align:right; border:1px solid #fff"><strong><?php _e('TOTAL:', 'cqpim'); ?></strong></td>
						
						<td style="border:1px solid #fff"><?php echo cqpim_calculate_currency($post->ID, $total); ?></td>
					
					</tr>
					
				<?php } ?>
				
					<tr>
						
						<td style="border:1px solid #fff" class="no_border" colspan="2"></td>
						
						<td class="no_border" style="text-align:right; border:1px solid #fff"><strong><?php _e('Received:', 'cqpim'); ?></strong></td>
						
						<td style="border:1px solid #fff"><?php echo cqpim_calculate_currency($post->ID, $received); ?></td>
					
					</tr>
					
					<tr>
						
						<td style="border:1px solid #fff" class="no_border" colspan="2"></td>
						
						<td class="no_border" style="text-align:right; border:1px solid #fff"><strong><?php _e('Outstanding:', 'cqpim'); ?></strong></td>
						
						<?php $outstanding = $outstanding - $received; ?>
						
						<td style="border:1px solid #fff"><?php echo cqpim_calculate_currency($post->ID, $outstanding); ?></td>
					
					</tr>
				
				</tbody>
			
			</table>
			
			<br />
			
			<div style="text-align:left" class="invoice_footer">
			
				<p><?php echo wpautop($invoice_footer); ?></p>
				
				<br />
				
				<?php if(empty($invoice_details['paid'])) { 
				
					if(!empty($_GET['atp'])) {
					
						$_SESSION['payment_amount_' . $post->ID] = $_GET['atp'];
					
					} else {
				
						$_SESSION['payment_amount_' . $post->ID] = number_format((float)$outstanding, 2, '.', '');
					
					}
					
					$stripe = get_option('client_invoice_stripe_key');
					
					$paypal = get_option('client_invoice_paypal_address');
					
					$vat = get_option('sales_tax_rate');
					
					if(function_exists('cqpim_twocheck_return_sid')) {
						
						$twocheck = cqpim_twocheck_return_sid();
						
					}
					
					if(empty($vat)) {
					
						$total = $sub;
						
					}
					
					$partial = get_option('client_invoice_allow_partial');
					
					$user = wp_get_current_user();
					
					$return = get_option('cqpim_client_page');
					
					$return = get_the_permalink($return);
					
					if (in_array('cqpim_client', $user->roles)) {
					
						if(!empty($stripe) && !empty($user->ID) || !empty($twocheck) && !empty($user->ID) || !empty($paypal) && !empty($user->ID)) {	

							echo '<div id="stripe-pay" style="display:none">';

								if(!empty($allow_partial)) {
								
									echo '<div id="payment-amount"><br />';
									
									echo '<span style="font-weight:bold">' . __('Amount to pay', 'cqpim') . ' (' . cqpim_calculate_currency($post->ID) . '): </span> ';
							
									echo '<input style="padding:5px" type="text" id="amount_to_pay" value="' . $_SESSION['payment_amount_' . $post->ID] . '" />';
								
									echo ' <button style="color:#fff; background:#222; padding:7px;border-radius:5px; -moz-border-radius:5px; border:0" id="save_amount">' . __('Update', 'cqpim') . '</button><span id="amount_spinner" class="ajax_spinner" style="display:none"></span><br />';
									
									echo '</div><br />';							
								
								}
								
								echo '<button style="color:#fff; background:#222; padding:7px;border-radius:5px; -moz-border-radius:5px; border:0; cursor:pointer" id="cqpim_pay_now">' . sprintf(__('Pay %1$s Now', 'cqpim'), cqpim_calculate_currency($post->ID, $_SESSION['payment_amount_' . $post->ID])) . '</button>';
							
							echo '</div>'; ?>
							
							<div id="cqpim_payment_methods_container" style="display:none">
							
								<div id="cqpim_payment_methods" style="padding:10px">
								
									<h3><?php _e('Payment Methods', 'cqpim'); ?></h3>
									
									<ul>
									
										<?php if(!empty($paypal)) { ?>
											
											<li>
											
												<form action="https://www.paypal.com/cgi-bin/webscr" method="post" class="paypal">
													<input type="hidden" name="cmd" value="_xclick">
													<input type="hidden" name="business" value="<?php echo get_option('client_invoice_paypal_address'); ?>">
													<input type="hidden" name="item_name" value="<?php echo get_option('company_name'); ?> - <?php _e('Invoice', 'cqpim'); ?> #<?php echo $invoice_id ?>">
													<input type="hidden" id="paypal-amount" name="amount" value="<?php echo $_SESSION['payment_amount_' . $post->ID]; ?>">
													<input type="hidden" name="quantity" value="1">
													<input type="hidden" name="currency_code" value="<?php echo get_post_meta($post->ID, 'currency_code', true); ?>">
													<input type="hidden" name="first_name" value="">
													<input type="hidden" name="no_shipping" value="1">
													<input type="hidden" name="rm" value="2">
													<input type="hidden" name="return" value="<?php echo $return; ?>">
													<input type="hidden" name="cancel_return" value="<?php echo $return; ?>">
													<input type="hidden" name="notify_url" value="<?php echo $return; ?>">
													<input style="max-width:100%" type="image" src="<?php echo plugin_dir_url( 'cqpim' ) . 'cqpim/img/ec-button.png'; ?> " name="submit" alt="<?php _e('Pay with Paypal', 'cqpim'); ?>">
												</form>										
											
											</li>
											
										<?php } ?>
										
										<?php if(!empty($stripe)) { ?>
											
											<li>
											
												<form action="<?php echo $return; ?>" method="POST">
												  <script
													src="https://checkout.stripe.com/checkout.js" class="stripe-button"
													data-zip-code="true"
													data-email="<?php echo $client_email; ?>"
													data-label="<?php _e('Pay with Stripe', 'cqpim'); ?>"
													data-currency="<?php echo get_post_meta($post->ID, 'currency_code', true); ?>"
													data-allow-remember-me="false"
													data-key="<?php echo get_option('client_invoice_stripe_key'); ?>"
													data-name="<?php echo get_option('company_name'); ?>"
													data-description="<?php _e('Invoice', 'cqpim'); ?> #<?php echo $invoice_id; ?>"
													data-amount="<?php echo $_SESSION['payment_amount_' . $post->ID] * 100; ?>">
													</script>
												</form>										
											
											</li>
											
										<?php } ?>
										
										<?php if(!empty($twocheck)) { ?>
											
											<li>
											
												<?php
												
													if(function_exists('cqpim_twocheck_return_button')) {
														
														$name = __('Invoice', 'cqpim') . ' #' . $invoice_id;
														
														$price = $_SESSION['payment_amount_' . $post->ID];

														echo cqpim_twocheck_return_button($name, $price, $return, $client_email); 
														
													}
												
												?>
											
											</li>
											
										<?php } ?>
									
									</ul>
								
								</div>
							
							</div>

						<?php }	

					}
				
				} ?>
				
				<div class="clear"></div>
			
			</div>
			
			<br /><br />
			
			<?php if(!empty($invoice_details['paid']) && $invoice_details['paid'] == true) { ?>	
				
				<div style="margin:0 5%" class="contract-confirmed">
							
					<h5 style="margin:0"><?php _e('THIS INVOICE HAS BEEN PAID', 'cqpim'); ?></h5>
					
				</div>
				
				<br />
						
			<?php } ?>
		
		<?php } else { ?>
		
			<h1><?php _e('Access Denied', 'cqpim'); ?></h1>
		
		<?php } ?>
		
		
	</div><!-- #content -->

<?php wp_footer(); ?>
</body>
</html>
<?php exit(); ?>
