<?php
 
 /**

 * Define and add the metaboxes

 */

add_action( 'add_meta_boxes', 'add_terms_cpt_metaboxes' );

function add_terms_cpt_metaboxes(){

	add_meta_box( 

		'terms_template', 

		__('Terms & Conditions', 'cqpim'),

		'terms_metabox_callback', 

		'cqpim_terms', 

		'normal',
		
		'high'

	);
	
	if(!current_user_can('publish_cqpim_terms')) {
	
		remove_meta_box( 'submitdiv', 'cqpim_terms', 'side' );
		
	}

}

function terms_metabox_callback( $post ) {

	// Add an nonce field so we can check for it later.

 	wp_nonce_field( 

 	 	'terms_metabox', 

 	 	'terms_metabox_nonce' );
		
		$terms = get_post_meta($post->ID, 'terms', true);
		
		if(empty($terms)) {
		
			$terms = '';
			
		}
		
		$editor_id = 'terms';  // only lower-case letters !

		$settings  = array(

			'textarea_name' => 'terms',

			'textarea_rows' => 80,

			'media_buttons' => FALSE

		);
		
		echo '<input type="submit" class="cqpim_button font-blue border-blue right op" value="' . __('Update Terms Template', 'cqpim') . '"/><div class="clear"></div><br />';

		wp_editor( $terms, $editor_id, $settings );
		
		echo '<input type="submit" class="cqpim_button mt-20 font-blue border-blue right op" value="' . __('Update Terms Template', 'cqpim') . '"/><div class="clear"></div>';
		
		
}

add_action( 'save_post', 'save_terms_metabox_data' );

function save_terms_metabox_data( $post_id ){

	//  verify this came from the our screen and with proper authorization

	// Check if our nonce is set.

	if ( ! isset( $_POST['terms_metabox_nonce'] ) )

	    return $post_id;

	// Verify that the nonce is valid.

	$nonce = $_POST['terms_metabox_nonce'];

	if ( ! wp_verify_nonce( $nonce, 'terms_metabox' ) )

	    return $post_id;

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 

	    return $post_id;

	// Check the user's permissions.

	if ( 'page' == $_POST['post_type'] ) {

	    if ( ! current_user_can( 'edit_page', $post_id ) )

	        return $post_id;

	  	} else {

	    if ( ! current_user_can( 'edit_post', $post_id ) )

	        return $post_id;

	}
	
	if(!empty($_POST['terms'])) {
	
		update_post_meta($post_id, 'terms', $_POST['terms']);
	
	}
	


}
