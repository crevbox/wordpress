<?php

	// Include Core Metabox Indexes
	
	require_once( 'client/main.php' );
	
	require_once( 'forms/main.php' );
	
	require_once( 'invoice/main.php' );
	
	require_once( 'project/main.php' );
	
	require_once( 'quote/main.php' );
	
	require_once( 'support/main.php' );
	
	require_once( 'task/main.php' );
	
	require_once( 'teams/main.php' );
	
	require_once( 'terms/main.php' );
	
	require_once( 'templates/main.php' );