<?php
 
	 /**

	 * Define and add the metaboxes

	 */

	add_action( 'add_meta_boxes', 'add_teams_cpt_metaboxes' );

	function add_teams_cpt_metaboxes(){

		add_meta_box( 

			'team_details', 

			__('Team Member Details', 'cqpim'),

			'team_details_metabox_callback', 

			'cqpim_teams', 

			'side',
			
			'high'

		);
		
		add_meta_box( 

			'team_tasks', 

			__('Open Tasks', 'cqpim'),

			'team_tasks_metabox_callback', 

			'cqpim_teams', 

			'normal'

		);
		
		add_meta_box( 

			'team_projects', 

			__('Projects', 'cqpim'),

			'team_projects_metabox_callback', 

			'cqpim_teams', 

			'normal'

		);
		
		if(is_plugin_active('cqpim-expenses/cqpim-expenses.php') && current_user_can('cqpim_view_expenses_admin')) {
			
			add_meta_box( 

				'team_expenses', 

				__('Expenses', 'cqpim'),

				'team_expenses_metabox_callback', 

				'cqpim_teams', 

				'normal'

			);			
			
		}
		
		if(!current_user_can('publish_cqpim_teams')) {
		
			remove_meta_box( 'submitdiv', 'cqpim_teams', 'side' );
			
		}

	}

	require_once('team_details.php');
	
	require_once('team_tasks.php');
	
	require_once('team_projects.php');
	
	require_once('team_expenses.php');

