<?php



// Quote Summary Metabox

function quote_summary_metabox_callback( $post ) {

	$meta = get_post_meta($post->ID, 'quote_elements', true);
	
	//print_r($meta);

	// Add an nonce field so we can check for it later.

 	wp_nonce_field( 

 	 	'quote_summary_metabox', 

 	 	'quote_summary_metabox_nonce' );
		
		$quote_details = get_post_meta($post->ID, 'quote_details', true);
		
		$quote_summary = isset($quote_details['quote_summary']) ? $quote_details['quote_summary']: '';
		
		
		
		if($quote_summary) {
		
			$content = $quote_summary;
			
		} else {
		
			$content = '';
			
		}
		
		$editor_id = 'quotesummary';  // only lower-case letters !

		$settings  = array(

			'textarea_name' => 'quote_summary',

			'textarea_rows' => 12,

			'media_buttons' => FALSE,
			
			'tinymce' => true

		);

		wp_editor( $content, $editor_id, $settings );
		

		
		echo '<div class="clear"></div>';
		
}

// Save Quote Summary

add_action( 'save_post', 'save_quote_summary_metabox_data' );

function save_quote_summary_metabox_data( $post_id ){

	//  verify this came from the our screen and with proper authorization

	// Check if our nonce is set.

	if ( ! isset( $_POST['quote_summary_metabox_nonce'] ) )

	    return $post_id;

	// Verify that the nonce is valid.

	$nonce = $_POST['quote_summary_metabox_nonce'];

	if ( ! wp_verify_nonce( $nonce, 'quote_summary_metabox' ) )

	    return $post_id;

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 

	    return $post_id;

	// Check the user's permissions.

	if ( 'page' == $_POST['post_type'] ) {

	    if ( ! current_user_can( 'edit_page', $post_id ) )

	        return $post_id;

	  	} else {

	    if ( ! current_user_can( 'edit_post', $post_id ) )

	        return $post_id;

	}
	
	if(isset($_POST['quote_summary'])) {
	
		$quote_details = get_post_meta($post_id, 'quote_details', true);
		
		$quote_summary = isset($_POST['quote_summary']) ? $_POST['quote_summary'] : '';
		
		$quote_details['quote_summary'] = $quote_summary;
		
		update_post_meta($post_id, 'quote_details', $quote_details);
	
	}
	
}