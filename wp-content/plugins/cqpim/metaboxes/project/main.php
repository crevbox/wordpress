<?php
 
 /**

 * Define and add the metaboxes

 */
 


	add_action( 'add_meta_boxes', 'add_project_cpt_metaboxes' );

	function add_project_cpt_metaboxes(){

		$screen = get_current_screen();
		
		if($screen->post_type == 'cqpim_project') {

		// Determine if the user has access to this project
		
		global $post;
		
		$project_contributors = get_post_meta($post->ID, 'project_contributors', true);
		
		if(!$project_contributors) {
		
			$project_contributors = array();
		
		}
		
		$user = wp_get_current_user();
		
		$args = array(
		
			'post_type' => 'cqpim_teams',
			
			'posts_per_page' => -1,
			
			'post_status' => 'private'
		
		);
		
		$members = get_posts($args);
		
		foreach($members as $member) {
		
			$team_details = get_post_meta($member->ID, 'team_details', true);
			
			if($team_details['user_id'] == $user->ID) {
			
				$assigned = $member->ID;
			
			}
		
		}
		
		foreach($project_contributors as $contributor) {
			
			if(!empty($contributor['team_id']) && $assigned == $contributor['team_id']) {
				
				$access = true;
				
			}
			
		}
		
		if(!empty($access) || current_user_can('cqpim_view_all_projects') || current_user_can('cqpim_create_new_project') && cqpim_is_edit_page('new') || $post->post_author == $user->ID) {

			add_meta_box( 

				'project_summary', 

				__('Project Brief', 'cqpim'),

				'project_summary_metabox_callback', 

				'cqpim_project',

				'normal',
					
				'high'

			);

			if(current_user_can('cqpim_view_project_client_page')) {
			
				add_meta_box( 

					'general_project_notes', 

					__('General Project Information', 'cqpim'), 

					'general_project_info_metabox_callback', 

					'cqpim_project',

					'normal',
					
					'high'

				);		

			}
						
			add_meta_box( 

				'project_contributors', 

				__('Team Members', 'cqpim'),

				'project_contributors_metabox_callback', 

				'cqpim_project',

				'normal',
					
				'high'

			);
			
			
			if(current_user_can('edit_cqpim_invoices')) {
			
			if(get_option('disable_invoices') != 1) {

				add_meta_box( 

					'project_invoices', 

					__('Project Invoices', 'cqpim'),

					'project_invoices_metabox_callback', 

					'cqpim_project',

					'normal',
					
					'high'

				);
				
				}
			
			}

			add_meta_box( 

				'project_elements', 

				__('Milestones & Tasks', 'cqpim'), 

				'project_elements_metabox_callback', 

				'cqpim_project',
				
				'normal'

			);
			
			add_meta_box( 

				'project_details', 

				__('Project Details & Status', 'cqpim'),

				'project_details_metabox_callback', 

				'cqpim_project',
				
				'side',
				
				'high'

			);
			
			if(current_user_can('cqpim_view_project_financials')) {
			
				add_meta_box( 

					'project_financials', 

					__('Project Financials', 'cqpim'), 

					'project_financials_metabox_callback', 

					'cqpim_project',

					'normal'

				);
				
				if(is_plugin_active('cqpim-expenses/cqpim-expenses.php')) {
					
					add_meta_box( 

						'project_expenses', 

						__('Project Expenses', 'cqpim'), 

						'project_expenses_metabox_callback', 

						'cqpim_project',

						'normal'

					);					
					
				}
				
				$setting = get_option('allow_project_currency_override');
				
				if($setting == 1) {
				
					add_meta_box( 

						'project_currency', 

						__('Project Currency Settings', 'cqpim'), 

						'project_currency_metabox_callback', 

						'cqpim_project',
						
						'side',
						
						'high'

					);		
				
				}
			
			}

			add_meta_box( 

				'project_notes', 

				__('My Project Notes', 'cqpim'),

				'project_notes_metabox_callback', 

				'cqpim_project',

				'normal'

			);
			
			if(current_user_can('upload_files')) {
			
				add_meta_box( 

					'project_files', 

					__('Project Files', 'cqpim'),

					'project_files_metabox_callback', 

					'cqpim_project',

					'normal'

				);
			
			}
			
			add_meta_box( 

				'project_messages', 

				__('Project Messages', 'cqpim'), 

				'project_messages_metabox_callback', 

				'cqpim_project',

				'normal'

			);
			
			add_meta_box( 

				'project_updates', 

				__('Project Updates', 'cqpim'),

				'project_updates_metabox_callback', 

				'cqpim_project',

				'normal'

			);
		
		} else {
		
			add_meta_box( 

				'project_denied', 

				__('Access Denied', 'cqpim'),

				'project_denied_metabox_callback', 

				'cqpim_project',

				'normal'

			);	
			
			remove_meta_box( 'submitdiv', 'cqpim_project', 'side' );
			
			}

		}
		
	}

	require_once( 'access_denied.php' );
	
	require_once( 'project_summary.php' );
	
	require_once( 'general_info.php' );
	
	require_once( 'project_currency.php' );
	
	require_once( 'team_members.php' );
	
	require_once( 'milestones.php' );
	
	require_once( 'project_details.php' );
	
	require_once( 'project_invoices.php' );
	
	require_once( 'project_financials.php' );
	
	require_once( 'project_expenses.php' );
	
	require_once( 'project_notes.php' );
	
	require_once( 'project_files.php' );
	
	require_once( 'project_messages.php' );
	
	require_once( 'project_updates.php' );


