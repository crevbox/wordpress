<?php

// A list of permitted file extensions
$allowed = get_option('cqpim_allowed_extensions');
$allowed = explode(',', $allowed);

if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

	if(!in_array(strtolower($extension), $allowed)){
		echo '{"status":"error"}';
		exit;
	}

	$file = $_FILES['upl'];
					
	$attachment_id = upload_user_file( $file );
	
	$_SESSION['upload_ids'][] = $attachment_id;
	
	$attachment_obj = wp_get_attachment_metadata( $attachment );
	
	update_post_meta($attachment_id, 'cqpim', true);
	
	$attachment = get_post($attachment_id);
	
	$filename = $attachment->post_title;
	
	$_SESSION['ticket_changes'][] = sprintf(__('Uploaded file: %1$s', 'cqpim'), $filename);					

	if(!empty($attachment_id)){
	
		echo '{"status":"success"}';
		
		exit;
		
	} else {
	
		echo '{"status":"success"}';
		
		exit;
	
	}
}