<?php



function support_notes_metabox_callback( $post ) {


	// Add an nonce field so we can check for it later.

 	wp_nonce_field( 

 	 	'support_notes_metabox', 

 	 	'support_notes_metabox_nonce' );
		
		//$_SESSION['times'] = 0;
		
		//echo $_SESSION['times'];
		
		$ticket_notes = get_post_meta($post->ID, 'ticket_notes', true);
		
		$editor_id = 'ticketnotes';  // only lower-case letters !

		$settings  = array(

			'textarea_name' => 'ticket_notes',

			'textarea_rows' => 12,

			'media_buttons' => FALSE,
			
			'tinymce' => true,

		);

		wp_editor( $ticket_notes, $editor_id, $settings );		
		
}

// Save Ticket Update

add_action( 'save_post', 'save_support_notes_metabox_data' );

function save_support_notes_metabox_data( $post_id ){

	//  verify this came from the our screen and with proper authorization

	// Check if our nonce is set.

	if ( ! isset( $_POST['support_notes_metabox_nonce'] ) )

	    return $post_id;

	// Verify that the nonce is valid.

	$nonce = $_POST['support_notes_metabox_nonce'];

	if ( ! wp_verify_nonce( $nonce, 'support_notes_metabox' ) )

	    return $post_id;

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 

	    return $post_id;

	// Check the user's permissions.

	if ( 'page' == $_POST['post_type'] ) {

	    if ( ! current_user_can( 'edit_page', $post_id ) )

	        return $post_id;

	  	} else {

	    if ( ! current_user_can( 'edit_post', $post_id ) )

	        return $post_id;

	}
	
	if(!empty($_POST['ticket_notes'])) {
	
		update_post_meta($post_id, 'ticket_notes', $_POST['ticket_notes']);
	
	}
	
}