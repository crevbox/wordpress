<?php

	add_action( 'add_meta_boxes', 'add_contact_cpt_metaboxes' );

	function add_contact_cpt_metaboxes(){

		add_meta_box( 

			'contact_details', 

			__('Client Details', 'cqpim'),

			'contact_details_metabox_callback', 

			'cqpim_client', 

			'side',
			
			'high'

		);
		
		$contracts = get_option('enable_project_contracts');
		
		if(!empty($contracts)) {
		
			add_meta_box( 

				'client_contracts', 

				__('Contract Settings', 'cqpim'),

				'contract_settings_metabox_callback', 

				'cqpim_client', 

				'side'

			);
		
		}
		
		add_meta_box( 

			'client_notes', 

			__('Client Notes', 'cqpim'),

			'client_notes_metabox_callback', 

			'cqpim_client', 

			'normal'

		);
		
		add_meta_box( 

			'client_files', 

			__('Client Files', 'cqpim'),

			'client_files_metabox_callback', 

			'cqpim_client', 

			'normal'

		);
		
		add_meta_box( 

			'client_team', 

			__('Client Contacts', 'cqpim'),

			'client_team_metabox_callback', 

			'cqpim_client', 

			'normal'

		);
		
		if(current_user_can('edit_cqpim_invoices')) {
		
			if(get_option('disable_invoices') != 1) {
			
				add_meta_box( 

					'client_financials', 

					__('Client Financials', 'cqpim'), 

					'client_financials_metabox_callback', 

					'cqpim_client', 

					'normal'

				);
				
				add_meta_box( 

					'client_rec_invoices', 

					__('Recurring Invoices', 'cqpim'), 

					'client_recinvoices_metabox_callback', 

					'cqpim_client', 

					'normal'

				);
				
				add_meta_box( 

					'client_invoices', 

					__('Client Invoices', 'cqpim'), 

					'client_invoices_metabox_callback', 

					'cqpim_client', 

					'normal'

				);
				
			}
		
		}
		
		add_meta_box( 

			'client_projects', 

			__('Client Projects', 'cqpim'),

			'client_projects_metabox_callback', 

			'cqpim_client', 

			'normal'

		);
		
		if(current_user_can('edit_cqpim_supports')) {
		
			add_meta_box( 

				'client_tickets', 

				__('Client Tickets', 'cqpim'), 

				'client_tickets_metabox_callback', 

				'cqpim_client', 

				'normal'

			);
		
		}
		
		add_meta_box( 

			'client_logs', 

			__('Client Logs', 'cqpim'),

			'client_logs_metabox_callback', 

			'cqpim_client', 

			'normal',
			
			'low'

		);
		
		if(!current_user_can('publish_cqpim_clients')) {
		
			remove_meta_box( 'submitdiv', 'cqpim_client', 'side' );
			
		}

	}
	
	require_once( 'contact_details.php' );
	
	require_once( 'projects.php' );
	
	require_once( 'financials.php' );
	
	require_once( 'rec_invoices.php' );
	
	require_once( 'invoices.php' );
	
	require_once( 'files.php' );
	
	require_once( 'notes.php' );
	
	require_once( 'teams.php' );
	
	require_once( 'tickets.php' );
	
	require_once( 'logs.php' );
	
	require_once( 'contracts.php' );


