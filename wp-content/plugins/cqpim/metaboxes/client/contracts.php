<?php


// Client Notes Metabox

function contract_settings_metabox_callback( $post ) {

	// Add an nonce field so we can check for it later.
	
	//$meta = get_post_meta($post->ID, '', true);
	
	//echo '<pre>';
	
	//print_r($meta);
	
	//echo '</pre>';

 	wp_nonce_field( 

 	 	'contract_settings_metabox', 

 	 	'contract_settings_metabox_nonce' ); 
		
		$client_contract = get_post_meta($post->ID, 'client_contract', true); ?>
		
		<input type="checkbox" name="disable_contracts" value="1" <?php if(!empty($client_contract)) { echo 'checked="checked"'; } ?> /> <?php _e('Disable project contracts for this client', 'cqpim'); ?>
		
		<?php
}

// Save Client Notes Fields

add_action( 'save_post', 'save_contract_settings_metabox_data' );

function save_contract_settings_metabox_data( $post_id ){

	//  verify this came from the our screen and with proper authorization

	// Check if our nonce is set.

	if ( ! isset( $_POST['contract_settings_metabox_nonce'] ) )

	    return $post_id;

	// Verify that the nonce is valid.

	$nonce = $_POST['contract_settings_metabox_nonce'];

	if ( ! wp_verify_nonce( $nonce, 'contract_settings_metabox' ) )

	    return $post_id;

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 

	    return $post_id;

	// Check the user's permissions.

	if ( 'page' == $_POST['post_type'] ) {

	    if ( ! current_user_can( 'edit_page', $post_id ) )

	        return $post_id;

	  	} else {

	    if ( ! current_user_can( 'edit_post', $post_id ) )

	        return $post_id;

	}
	
	$disable_contracts = isset($_POST['disable_contracts']) ? $_POST['disable_contracts'] : '';
	
	update_post_meta($post_id, 'client_contract', $disable_contracts);
	
}