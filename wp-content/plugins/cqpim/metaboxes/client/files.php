<?php

function client_files_metabox_callback( $post ) {

 	wp_nonce_field( 

 	 	'client_files_metabox', 

 	 	'client_files_metabox_nonce' );
		
	$client_details = get_post_meta($post->ID, 'client_details', true);	?>
	
	<?php 
	
	$all_attached_files = get_attached_media( '', $post->ID );
	
	if(!$all_attached_files) {
	
		echo '<div class="cqpim-alert cqpim-alert-info alert-display">' . __('There are no files attached to this client.', 'cqpim') . '</div>';
	
	} else {
	
		echo '<table class="cqpim_table"><thead><tr>';
		
		echo '<th>' . __('File Name', 'cqpim') . '</th><th>' . __('File Type', 'cqpim') . '</th><th>' . __('Uploaded', 'cqpim') . '</th><th>' . __('Uploaded By', 'cqpim') . '</th><th>' . __('Actions', 'cqpim') . '</th>';
		
		echo '</tr></thead><tbody>';
	
		foreach($all_attached_files as $file) {
		
			$file_object = get_post($file->ID);
			
			$link = get_the_permalink($file->ID);
			
			$path = get_attached_file($file->ID);
			
			$type = wp_check_filetype($path);
		
			$user = get_user_by( 'id', $file->post_author );
		
			echo '<tr>';
			
			echo '<td><a href="' . $file->guid . '" target="_blank">' . $file->post_title . '</a></td>';
			
			echo '<td>' . $type['ext'] . '</td>';
			
			echo '<td>' . $file->post_date . '</td>';
			
			echo '<td>' . $user->display_name . '</td>';
			
			echo '<td><a href="' . $file->guid . '" download="' . $file->post_title . '" class="cqpim_button cqpim_small_button border-green font-green" value="' . $file->ID . '"><i class="fa fa-download" aria-hidden="true"></i></a> <button class="delete_file cqpim_button cqpim_small_button border-red font-red" data-id="' . $file->ID . '" value=""><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
			
			echo '</tr>';
		
		}
		
		echo '</tbody></table>';
	
	} ?>
	
	<input class="mt-20" type="file" name="cqpim_upload_files[]" multiple>
	
	<?php
		
}

add_action( 'save_post', 'save_client_files_metabox_data' );

function save_client_files_metabox_data( $post_id ){

	if ( ! isset( $_POST['client_files_metabox_nonce'] ) )

	    return $post_id;

	$nonce = $_POST['client_files_metabox_nonce'];

	if ( ! wp_verify_nonce( $nonce, 'client_files_metabox' ) )

	    return $post_id;

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 

	    return $post_id;

	if ( 'page' == $_POST['post_type'] ) {

	    if ( ! current_user_can( 'edit_page', $post_id ) )

	        return $post_id;

	  	} else {

	    if ( ! current_user_can( 'edit_post', $post_id ) )

	        return $post_id;

	}

	if(!empty($_FILES['cqpim_upload_files']['name'][0])) {
	
		$files = array();
	
		$files_unsorted = $_FILES['cqpim_upload_files'];
		
		foreach($files_unsorted['name'] as $key => $name) {
		
			$files[$key]['name'] = $name;
		
		}
		
		foreach($files_unsorted['type'] as $key => $type) {
		
			$files[$key]['type'] = $type;
		
		}
		
		foreach($files_unsorted['tmp_name'] as $key => $tmp_name) {
		
			$files[$key]['tmp_name'] = $tmp_name;
		
		}
		
		foreach($files_unsorted['error'] as $key => $error) {
		
			$files[$key]['error'] = $error;
		
		}
		
		foreach($files_unsorted['size'] as $key => $size) {
		
			$files[$key]['size'] = $size;
		
		}
		
		$attachment_id = array();
		
		foreach($files as $file) {
		
			$attachment_id[] = upload_user_file( $file, $post_id );
		
		}
		
		foreach($attachment_id as $attachment) {
		
			$attachment_obj = basename( get_attached_file( $attachment ) );
			
			update_post_meta($attachment, 'cqpim', true);
			
			if(!empty($attachment_obj)) {
			
				$filename = str_replace('cqpim-uploads/', '', $attachment_obj);
			
			}
		
		}
	
	}

	if( isset( $_POST['delete_file'] ) ){

		$att_to_delete = $_POST['delete_file'];

		foreach ( $att_to_delete as $key => $attID ) {
		
			$file = get_post($attID);
			
			$task_object = get_post($post_id);
			
			$task_link = '<a class="cqpim-link" href="' . get_the_permalink($post_id) . '">' . $task_object->post_title . '</a>';

			//wp_delete_attachment( $attID, TRUE );
			global $wpdb;
			
			$wpdb->query(
				
				"
				UPDATE $wpdb->posts 
				SET post_parent = ''
				WHERE ID = $attID
				AND post_type = 'attachment'
				"
				
			);

		}

	}	
	
}