<?php
 
	 /**

	 * Define and add the metaboxes

	 */

	add_action( 'add_meta_boxes', 'add_invoice_cpt_metaboxes' );

	function add_invoice_cpt_metaboxes(){
	
		add_meta_box( 

			'invoice_payments', 

			__('Payments / Deductions', 'cqpim'),

			'invoice_payments_metabox_callback', 

			'cqpim_invoice',
			
			'normal',

			'low'

		);

		add_meta_box( 

			'invoice_client_project', 

			__('Invoice Details', 'cqpim'),

			'invoice_client_project_metabox_callback', 

			'cqpim_invoice',
			
			'side',

			'high'

		);
		
		$setting = get_option('allow_invoice_currency_override');
		
		if($setting == 1) {
		
			add_meta_box( 

				'invoice_currency', 

				__('Invoice Currency Settings', 'cqpim'), 

				'invoice_currency_metabox_callback', 

				'cqpim_invoice',
				
				'side',
				
				'high'

			);		
		
		}
		
		add_meta_box( 

			'invoice_line_items', 

			__('Line Items', 'cqpim'),

			'invoice_items_metabox_callback', 

			'cqpim_invoice',
			
			'normal',

			'high'

		);
		
		if(!current_user_can('publish_cqpim_invoices')) {
		
			remove_meta_box( 'submitdiv', 'cqpim_invoice', 'side' );
			
		}

	}
	
	require_once( 'payments.php' );
	
	require_once( 'invoice_currency.php' );

	require_once( 'client_details.php' );
	
	require_once( 'line_items.php' );

