<?php $active_plugins = (array) get_option( 'active_plugins', array() );?>
<form id="smart-plugin-loader">
      <ul>
      <?php
            foreach ($active_plugins as $plugin):
                  $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/' . $plugin);
      ?>
            <li>
                  <h3><?php echo esc_html($plugin_data['Name']);?></h3>
            </li>
      <?php endforeach;?>
      </ul>
</form>
