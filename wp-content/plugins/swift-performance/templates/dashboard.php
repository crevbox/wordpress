<?php
	$cache_status 	= Swift_Performance::cache_status();

	$update_info 	= Swift_Performance::check_api();
	$latest		= (isset($update_info['version']) ? $update_info['version'] : 0);
	$update_available = version_compare(SWIFT_PERFORMANCE_VER, $latest, '<=');
?>
<div class="swift-message swift-hidden"></div>
<div class="swift-dashboard">
	<div class="swift-dashboard-item">
		<h3><?php esc_html_e('Actions', 'swift-performance');?></h3>
		<ul>
			<?php if(Swift_Performance::check_option('enable-caching', 1) || Swift_Performance::check_option('merge-scripts', 1) || Swift_Performance::check_option('merge-styles', 1)):?>
			<?php if(Swift_Performance::check_option('enable-caching', 1)):?>
            <li><a href="#" id="swift-performance-clear-cache" class="swift-performance-control"><?php esc_html_e('Clear Cache', 'swift-performance');?></a></li>
      <?php elseif(Swift_Performance::check_option('merge-scripts', 1) || Swift_Performance::check_option('merge-styles', 1)):?>
            <li><a href="#" id="swift-performance-clear-assets-cache" class="swift-performance-control"><?php esc_html_e('Clear Assets Cache', 'swift-performance');?></a></li>
      <?php endif;?>
      <?php if(Swift_Performance::check_option('enable-caching', 1) || Swift_Performance::check_option('merge-scripts', 1) || Swift_Performance::check_option('merge-styles', 1)):?>
            <li><a href="#" id="swift-performance-prebuild-cache" class="swift-performance-control"><?php esc_html_e('Prebuild Cache', 'swift-performance');?></a></li>
            <li><a href="#" id="swift-performance-cache-status" class="swift-performance-control"><?php esc_html_e('Cache Status', 'swift-performance');?></a></li>
      <?php endif;?>
	<li><a href="#" id="swift-performance-log" class="swift-performance-control"><?php esc_html_e('Show Log', 'swift-performance');?></a></li>
      <?php if(get_option('swift_performance_rewrites') != ''):?>
            <li><a href="#" id="swift-performance-show-rewrite" class="swift-performance-control"><?php esc_html_e('Show Rewrite Rules', 'swift-performance');?></a></li>
      <?php endif;?>
      <?php endif;?>
			<li><a href="<?php echo esc_url(add_query_arg(array('page' => 'swift-performance', 'subpage' => 'setup'), admin_url('tools.php')));?>"><?php esc_html_e('Setup Wizard', 'swift-performance');?></a></li>
		</ul>
	</div>
	<div class="swift-dashboard-item">
		<h3><?php esc_html_e('Informations', 'swift-performance');?></h3>
		<ul>
			<li>
				<ul>
					<li><strong><?php esc_html_e('Cached Pages', 'swift-performance');?></strong></li>
					<li class="text-right"><?php echo count($cache_status['files']);?></li>
				</ul>
				<ul>
					<li><strong><?php esc_html_e('Cached Size', 'swift-performance');?></strong></li>
					<li class="text-right"><?php echo sprintf(esc_html__('%s Mb', 'swift-performance'), number_format($cache_status['cache_size']/1024/1024,2));?></li>
				</ul>
				<ul>
					<li><strong><?php esc_html_e('API Connection', 'swift-performance');?></strong></li>
					<?php if (!empty($update_info)):?>
						<li class="text-right text-green"><?php esc_html_e('OK', 'swift-performance');?></li>
					<?php else:?>
						<li class="text-right text-red"><?php esc_html_e('FAIL', 'swift-performance');?></li>
					<?php endif;?>
				</ul>
			</li>
		</ul>
	</div>
	<div class="swift-dashboard-item">
		<h3><?php esc_html_e('Version', 'swift-performance');?></h3>
		<ul>
			<li>
				<ul>
					<li><strong><?php esc_html_e('Current Version', 'swift-performance');?></strong></li>
					<li class="text-right text-<?php echo ($update_available ? 'red' : 'green');?>"><?php echo SWIFT_PERFORMANCE_VER;?></li>
				</ul>
				<?php if ($update_available):?>
				<ul>
					<li><strong><?php esc_html_e('Latest Version', 'swift-performance');?></strong></li>
					<li class="text-right text-green"><?php echo esc_html($latest);?></li>
				</ul>
				<?php endif;?>
			</li>
		</ul>
		<?php if ($update_available):?>
			<a href="<?php echo esc_url(admin_url('plugins.php'));?>" class="swift-btn swift-btn-green"><?php esc_html_e('Update Now', 'swift-performance')?></a>
		<?php endif;?>
	</div>
</div>
<div class="swift-box swift-hidden">
	<h3></h3>
	<div class="swift-box-inner">
		<pre></pre>
	</div>
</div>
