<?php
/**
 * Plugin Name: Swift Performance
 * Plugin URI: http://swift-performance.swteplugins.com
 * Description: Boost your WordPress site
 * Version: 1.2
 * Author: SWTE
 * Author URI: https://swteplugins.com
 * Text Domain: swift-performance
 */

class Swift_Performance {

	/**
	 * Loaded modules
	 */
	public $modules = array();

	/**
	 * Global Memcached API object
	 */
	public $memcached;

	/**
	 * Create instance
	 */
	public function __construct() {
		do_action('swift_performance_before_init');

		// Clean htaccess and scheduled events on deactivate
		register_deactivation_hook( __FILE__, array('Swift_Performance', 'deactivate'));

		// Regenerate htaccess on activation
		register_activation_hook( __FILE__, array('Swift_Performance', 'activate'));

		// Set constants
		if (!defined('SWIFT_PERFORMANCE_URI')){
			define('SWIFT_PERFORMANCE_URI', trailingslashit(plugins_url() . '/'. basename(__DIR__)));
		}

		if (!defined('SWIFT_PERFORMANCE_DIR')){
			define('SWIFT_PERFORMANCE_DIR', trailingslashit(__DIR__));
		}

		if (!defined('SWIFT_PERFORMANCE_VER')){
			define('SWIFT_PERFORMANCE_VER', '1.2');
		}

		if (!defined('SWIFT_PERFORMANCE_API_URL')){
			define('SWIFT_PERFORMANCE_API_URL', 'http://api.swteplugins.com/sp_v2.2/');
		}


		// Fix framework URL (symlink issue)
		add_filter('reduxsa/_url', function(){
			return SWIFT_PERFORMANCE_URI . 'includes/framework/';
		});

		// Include framework
		include_once 'includes/framework/framework.php';
		include_once 'includes/framework/framework-config.php';



		// Include setup wizard
		if (is_admin()){
			global $swift_performance_setup;
			require_once SWIFT_PERFORMANCE_DIR . 'includes/setup/setup.php';
			$swift_performance_setup = new Swift_Performance_Setup();

			add_action('admin_init', function(){
				if (!defined('DOING_AJAX') && get_transient('swift-performance-setup') === 'uid:'.get_current_user_id()){
					delete_transient('swift-performance-setup');
					wp_redirect(add_query_arg(array('page' => 'swift-performance', 'subpage' => 'setup'), admin_url('tools.php')));
					die;
				}
			});
		}

		// Init Swift Performance
		$this->init();

		// Load textdomain
		add_action('init', function(){
			load_plugin_textdomain('swift-performance');
		});

		// Load assets on backend
		add_action('admin_enqueue_scripts', array($this, 'load_assets'));

		// Ajax handlers
		add_action('wp_ajax_swift_performance_clear_cache', array($this, 'ajax_clear_all_cache'));
		add_action('wp_ajax_swift_performance_clear_assets_cache', array($this, 'ajax_clear_assets_cache'));
		add_action('wp_ajax_swift_performance_prebuild_cache', array($this, 'ajax_prebuild_cache'));
		add_action('wp_ajax_swift_performance_show_rewrites', array($this, 'ajax_show_rewrites'));
		add_action('wp_ajax_swift_performance_cache_status', array($this, 'ajax_cache_status'));
		add_action('wp_ajax_swift_performance_show_log', array($this, 'ajax_show_log'));


		// Create prebuild cache hook
		add_action( 'swift_performance_prebuild_cache', array('Swift_Performance', 'prebuild_cache'));

		// Clear cache, manage rewrite rules, scheduled jobs after options was saved
		add_action('wp_ajax_swift_performance_options_ajax_save', array('Swift_Performance', 'options_saved'), 0);

		// Add actions to redux header
		add_action('reduxsa/page/swift_performance_options/form/before', function(){
		      require 'includes/framework/settings-header.php';
		});

		// Create cache expiry cron schedule
		add_filter( 'cron_schedules',	function ($schedules){
			// Common cache
			$schedules['swift_performance_cache_expiry'] = array(
				'interval' => max(Swift_Performance::get_option('cache-garbage-collection-time'), 1),
				'display' => __('Swift Performance Cache Expiry')
			);

			// Assets cache
			$schedules['swift_performance_assets_cache_expiry'] = array(
				'interval' => 3600,
				'display' => __('Swift Performance Assets Cache Expiry')
			);

			return $schedules;
		});

		// Admin menus
		add_action('admin_bar_menu', array('Swift_Performance', 'toolbar_items'),100);

		// Clear caches
		add_action('init', function(){
			if (!isset($_GET['swift-performance-action'])){
				return;
			}

			if ($_GET['swift-performance-action'] == 'clear-all-cache' && current_user_can('manage_options') && isset($_GET['_wpnonce']) && wp_verify_nonce($_GET['_wpnonce'], 'clear-swift-cache')){
				Swift_Performance_Cache::clear_all_cache();
				self::add_notice(esc_html__('All cache cleared', 'swift-performance'), 'success');
			}

			if ($_GET['swift-performance-action'] == 'clear-assets-cache' && current_user_can('manage_options') && isset($_GET['_wpnonce']) && wp_verify_nonce($_GET['_wpnonce'], 'clear-swift-assets-cache')){
				Swift_Performance_Asset_Manager::clear_assets_cache();
				self::add_notice(esc_html__('Assets cache cleared', 'swift-performance'), 'success');
			}

			if ($_GET['swift-performance-action'] == 'purge-cdn' && current_user_can('manage_options') && isset($_GET['_wpnonce']) && wp_verify_nonce($_GET['_wpnonce'], 'purge-swift-cdn')){
				if (self::check_option('enable-caching', 1)){
					Swift_Performance_Cache::clear_all_cache();
				}
				else if (self::check_option('merge-scripts',1) || self::check_option('merge-styles',1)){
					Swift_Performance_Asset_Manager::clear_assets_cache();
				}
				else {
					Swift_Performance_CDN_Manager::purge_cdn();
				}
			}
		});

		// Show runtime Messages
		add_action('admin_notices', array($this, 'admin_notices'));

		// Heartbeat
		add_action('init', function(){
			// Disable on specific pages
			$disabled_pages = array();
			foreach ((array)Swift_Performance::get_option('disable-heartbeat') as $key => $value) {
				if ($value == 1){
					$disabled_pages = array_merge($disabled_pages, explode(',',$key));
				}
			}
			if (!empty($disabled_pages)){
				global $pagenow;
				if (in_array($pagenow, $disabled_pages)){
					wp_deregister_script('heartbeat');
				}
			}
		},1);

		// Override frequency
		add_filter( 'heartbeat_settings', function($settings){
			$interval = Swift_Performance::get_option('heartbeat-frequency');

			if (!empty($interval)){
				$settings['interval'] = $interval;
			}
			return $settings;
		});

		// Create clear cache hook for scheduled events
		if (Swift_Performance::check_option('cache-expiry-mode', 'timebased')){
			add_action('swift_performance_clear_cache', array('Swift_Performance_Cache', 'clear_all_cache'));
			add_action('swift_performance_clear_expired', array('Swift_Performance_Cache', 'clear_expired'));
		}

		// Create clear assets cache hook for scheduled events
		add_action('swift_performance_clear_assets_cache', array('Swift_Performance_Asset_Manager', 'clear_assets_cache'));

		// Plugin updater
		if (self::check_option('purchase-key', '', '!=')){
			require 'includes/puc/plugin-update-checker.php';
			$update_checker = Puc_v4_Factory::buildUpdateChecker(
				add_query_arg('purchase_key', self::get_option('purchase-key'), SWIFT_PERFORMANCE_API_URL . 'update/'),
				__FILE__,
				'swift-performance'
			);

			// Add purchase key to download url
			add_filter('puc_request_info_result-swift-performance', function($info){
				$info->download_url = str_replace('[[PARAMETERS]]', '?purchase_key=' . Swift_Performance::get_option('purchase-key') . '&site=' . site_url(), $info->download_url);
				return $info;
			});
		}

		// Add plugin actions
		add_filter('plugin_action_links', function ($links, $file) {
			if ($file == plugin_basename(__FILE__)) {
				$settings_link = '<a href="' . add_query_arg('subpage', 'settings', menu_page_url('swift-performance', false)) . '">'.__('Settings','swift-performance').'</a>';
				array_unshift($links, $settings_link);
			}

			return $links;
		}, 10, 2);

		// Log 404 queries
		add_action('template_redirect', function(){
			if (is_404()){
				Swift_Performance::log('404 Error: ' . $_SERVER['REQUEST_URI'], 6);
			}
		});

		do_action('swift_performance_init');
	}

	/**
	 * Load assets
	 */
	public function load_assets($hook) {
		if($hook == 'tools_page_swift-performance') {
			wp_enqueue_script( 'swift-performance', SWIFT_PERFORMANCE_URI . 'js/scripts.js', array('jquery'), SWIFT_PERFORMANCE_VER );
			wp_localize_script( 'swift-performance', 'swift_performance', array('nonce' => wp_create_nonce('swift-performance-ajax-nonce')));
			wp_enqueue_style( 'swift-performance', SWIFT_PERFORMANCE_URI . 'css/styles.css', array(), SWIFT_PERFORMANCE_VER );
		}
	}

	/**
	 * Init Swift Performance
	 */
	public function init(){
		if (!defined('SWIFT_PERFORMANCE_CACHE_DIR')){
			define('SWIFT_PERFORMANCE_CACHE_DIR', trailingslashit(self::get_option('cache-path')).'swift-performance/' . parse_url(self::home_url(), PHP_URL_HOST) . '/');
		}

		if (!defined('SWIFT_PERFORMANCE_CACHE_URL')){
			define('SWIFT_PERFORMANCE_CACHE_URL', str_replace(ABSPATH, self::home_url(), SWIFT_PERFORMANCE_CACHE_DIR));
		}


		// Cache
		$this->modules['cache'] =  require_once 'modules/cache/cache.php';

		// CDN Manager
		if (self::check_option('enable-cdn', 1)){
			$this->modules['cdn-manager'] =  require_once 'modules/cdn/cdn-manager.php';
		}

		// Asset Manager
		$this->modules['asset-manager'] = require_once 'modules/asset-manager/asset-manager.php';


		// Image optimizer
		$this->modules['image-optimizer'] =  require_once 'modules/image-optimizer/image-optimizer.php';

		// Google Analytics
		if (self::check_option('bypass-ga', 1)){
			$this->modules['ga'] =  require_once 'modules/google-analytics/google-analytics.php';
		}
	}

	/**
	 * Print admin notices
	 */
	public function admin_notices(){
		$messages = get_option('swift_performance_messages', array());
		foreach((array)$messages as $message){
			$class = ($message['type'] == 'success' ? 'updated' : ($message['type'] == 'warning' ? 'update-nag' : ($message['type'] == 'error' ? 'error' : 'notice')));
			echo '<div class="'.$class.'" style="padding:25px 10px 10px 10px;position: relative;display: block;"><span style="color:#888;position:absolute;top:5px;left:5px;">'.esc_html__('Swift Performance','swift-performance').'</span>'.$message['message'].'</div>';
		}
		delete_option('swift_performance_messages');
	}

	/**
	 * Clear all cache ajax callback
	 */
	public function ajax_clear_all_cache(){
		// Check user and nonce
		if (!current_user_can('manage_options') || !isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'swift-performance-ajax-nonce')){
			wp_send_json(
				array(
					'type' => 'critical',
					'text' => __('Your session has expired. Please refresh the page and try again.', 'swift-performance')
				)
			);
		}
		Swift_Performance::log('Ajax action: (clear all cache)', 9);

		Swift_Performance_Cache::clear_all_cache();
		wp_send_json(
			array(
				'type' => 'success',
				'text' => __('Cache cleared', 'swift-performance')
			)
		);
	}

	/**
	 * Clear assets cache ajax callback
	 */
	public function ajax_clear_assets_cache(){
		// Check user and nonce
		if (!current_user_can('manage_options') || !isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'swift-performance-ajax-nonce')){
			wp_send_json(
				array(
					'type' => 'critical',
					'text' => __('Your session has expired. Please refresh the page and try again.', 'swift-performance')
				)
			);
		}

		Swift_Performance::log('Ajax action: (clear assets cache)', 9);

		Swift_Performance_Asset_Manager::clear_assets_cache();
		wp_send_json(
			array(
				'type' => 'success',
				'text' => __('Assets cache cleared', 'swift-performance')
			)
		);
	}

	/**
	 * Prebuild cache ajax callback
	 */
	public function ajax_prebuild_cache(){
		// Check user and nonce
		if (!current_user_can('manage_options') || !isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'swift-performance-ajax-nonce')){
			wp_send_json(
				array(
					'type' => 'critical',
					'text' => __('Your session has expired. Please refresh the page and try again.', 'swift-performance')
				)
			);
		}

		Swift_Performance::log('Ajax action: (prebuild cache)', 9);

		wp_schedule_single_event(time(), 'swift_performance_prebuild_cache');
		wp_send_json(
			array(
				'type' => 'info',
				'text' => __('Prebuilding cache is in progress', 'swift-performance')
			)
		);
	}

	/**
	 * Show the rewrite rules
	 */
	public function ajax_show_rewrites(){
		// Check user and nonce
		if (!current_user_can('manage_options') || !isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'swift-performance-ajax-nonce')){
			wp_send_json(
				array(
					'type' => 'critical',
					'text' => __('Your session has expired. Please refresh the page and try again.', 'swift-performance')
				)
			);
		}

		switch (self::server_software()){
			case 'apache':
				$htaccess = trailingslashit(str_replace(site_url(), ABSPATH, self::home_url())) . '.htaccess';

		            if (file_exists($htaccess) && is_writable($htaccess)){
		               	$message = __('It seems that your htaccess is writable, you don\'t need to add rules manually.', 'swift-performance');
		            }
				else {
					$message = __('It seems that your htaccess is NOT writable, you need to add rules manually.', 'swift-performance');
				}
				break;
			case 'nginx':
				$message = __('You need to add rewrite rules manually to your Nginx config file.', 'swift-performance');
				break;
			default:
				$message = __('Caching with rewrites currently available on Apache and Nginx only.', 'swift-performance');


		}

		wp_send_json(
			array(
				'title'	=> esc_html__('Rewrite Rules', 'swift-performance'),
				'type'	=> 'info',
				'text'	=> $message,
				'rewrites'	=> get_option('swift_performance_rewrites'),
			)
		);
	}

	/**
	 * Show the cache status
	 */
	public function ajax_cache_status(){
		// Check user and nonce
		if (!current_user_can('manage_options') || !isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'swift-performance-ajax-nonce')){
			wp_send_json(
				array(
					'type' => 'critical',
					'text' => __('Your session has expired. Please refresh the page and try again.', 'swift-performance')
				)
			);
		}

		$result = self::cache_status();

		wp_send_json(
			array(
				'title'	=> esc_html__('Cache status', 'swift-performance'),
				'type' => 'info',
				'status' => esc_html__('Cache status', 'swift-performance') . "\n" . sprintf(esc_html__('%s cached pages', 'swift-performance'), count($result['files'])) . sprintf(esc_html__(' (%s Mb)', 'swift-performance'), number_format($result['cache_size']/1024/1024,2)) . "\n\n" . esc_html__('Files:','swift-performance') . "\n" . esc_html(implode("\n", $result['files'])),
			)
		);
	}

	/**
	 * Show the latest log
	 */
	public function ajax_show_log(){
		// Check user and nonce
		if (!current_user_can('manage_options') || !isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'swift-performance-ajax-nonce')){
			wp_send_json(
				array(
					'type' => 'critical',
					'text' => __('Your session has expired. Please refresh the page and try again.', 'swift-performance')
				)
			);
		}

		if (file_exists(Swift_Performance::get_option('log-path') . date('Y-m-d') . '.txt')){
			$log = explode("\n", file_get_contents(Swift_Performance::get_option('log-path') . date('Y-m-d') . '.txt'));
			$log = array_reverse($log);
			$log = implode("\n", $log);
		}
		else {
			$log = __('Log is empty', 'swift-performance');
		}

		wp_send_json(
			array(
				'title'	=> sprintf(esc_html__('Log - %s', 'swift-performance'), date_i18n(get_option( 'date_format' ))),
				'type'	=> 'info',
				'status'	=> $log
			)
		);
	}

	/**
	 * Prebuild cache callback
	 */
	public static function prebuild_cache(){
		global $wpdb;
		$current_process = mt_rand(0,PHP_INT_MAX);
		set_transient('swift_performance_prebuild_cache_pid', $current_process, 600);
		Swift_Performance::log('Prebuild cache ('.$current_process.') start', 9);

		set_time_limit(600);
		global $wpdb;
		$post_types = array();
		foreach (get_post_types(array('publicly_queryable' => true)) as $post_type){
			$post_types[] = "'{$post_type}'";
		}
		$posts = $wpdb->get_results("SELECT option_value as ID FROM {$wpdb->options} WHERE option_name = 'page_on_front' UNION SELECT meta_value as ID FROM {$wpdb->postmeta} WHERE meta_key = '_menu_item_object_id' UNION SELECT ID FROM {$wpdb->posts} WHERE post_status = 'publish' AND post_type IN(".implode(',', $post_types).")", ARRAY_A);
		foreach ($posts as $post){
			$prebuild_process = $wpdb->get_var("SELECT option_value FROM {$wpdb->options} WHERE option_name = '_transient_swift_performance_prebuild_cache_pid'");
			if ($prebuild_process !== false && $prebuild_process != $current_process){
				Swift_Performance::log('Prebuild cache ('.$current_process.') stop', 9);
				break;
			}
			$permalink = get_permalink($post['ID']);

			$cache_path = trailingslashit(SWIFT_PERFORMANCE_CACHE_DIR) . parse_url($permalink, PHP_URL_PATH) . trailingslashit('desktop/unauthenticated/') . 'index.html';

			Swift_Performance::log('Check cached file is exsists: ' . $cache_path, 9);
			if (file_exists($cache_path)){
				continue;
			}

			Swift_Performance::log('Prebuild cache hit page: ' . $permalink, 9);
			wp_remote_get($permalink, array('headers' => array('X-merge-assets' => 'true'), 'useragent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:52.0) Gecko/20100101 Firefox/52.0', 'timeout' => 120));
			if (Swift_Performance::check_option('mobile-support', 1)){
				wp_remote_get($permalink, array('headers' => array('X-merge-assets' => 'true'), 'useragent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25', 'timeout' => 120));
			}
		}
	}

	/**
	 * Add toolbar options
	 * @param WP_Admin_Bar $admin_bar
	 */
	public static function toolbar_items($admin_bar){
		if (current_user_can('manage_options')){
			$current_page = site_url(str_replace(site_url(), '', 'http'.(isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']));

			$admin_bar->add_menu(array(
				'id'    => 'swift-performance',
				'title' => esc_html__('Swift Performance', 'swift-performance'),
				'href'  => esc_url(admin_url('tools.php?page=swift-performance',false)),
			 ));

			if(Swift_Performance::check_option('purchase-key', '', '!=')){
 				$admin_bar->add_menu(array(
 					'id'    => 'swift-image-optimizer',
 					'parent' => 'swift-performance',
 					'title' => esc_html__('Image Optimizer', 'swift-performance'),
 					'href'  => esc_url(add_query_arg(array('page' => 'swift-performance', 'subpage' => 'image-optimizer'), admin_url('tools.php')))
 				));
 			}
			if(Swift_Performance::check_option('enable-caching', 1)){
				$admin_bar->add_menu(array(
					'id'    => 'clear-swift-cache',
					'parent' => 'swift-performance',
					'title' => esc_html__('Clear Cache', 'swift-performance'),
					'href'  => esc_url(wp_nonce_url(add_query_arg('swift-performance-action', 'clear-all-cache', $current_page), 'clear-swift-cache')),
				));
			}
			if(Swift_Performance::check_option('merge-scripts', 1) || Swift_Performance::check_option('merge-styles', 1)){
				$admin_bar->add_menu(array(
					'id'    => 'clear-swift-assets-cache',
					'parent' => 'swift-performance',
					'title' => esc_html__('Clear Assets Cache', 'swift-performance'),
					'href'  => esc_url(wp_nonce_url(add_query_arg('swift-performance-action', 'clear-assets-cache', $current_page), 'clear-swift-assets-cache')),
				));
			}

			if (Swift_Performance::check_option('enable-cdn',1) && Swift_Performance::check_option('maxcdn-key','','!=') && Swift_Performance::check_option('maxcdn-secret','','!=')){
				$admin_bar->add_menu(array(
					'id'    => 'purge-swift-cdn',
					'parent' => 'swift-performance',
					'title' => esc_html__('Purge CDN (All zones)', 'swift-performance'),
					'href'  => esc_url(wp_nonce_url(add_query_arg('swift-performance-action', 'purge-cdn', $current_page), 'purge-swift-cdn')),
				));
			}

			$admin_bar->add_menu(array(
				'id'    => 'swift-settings',
				'parent' => 'swift-performance',
				'title' => esc_html__('Settings', 'swift-performance'),
				'href'  => esc_url(add_query_arg(array('page' => 'swift-performance', 'subpage' => 'settings'), admin_url('tools.php'))),
			));

			$admin_bar->add_menu(array(
				'id'    => 'swift-setup-wizard',
				'parent' => 'swift-performance',
				'title' => esc_html__('Setup Wizard', 'swift-performance'),
				'href'  => esc_url(add_query_arg(array('page' => 'swift-performance', 'subpage' => 'setup'), admin_url('tools.php'))),
			));
		}
	}

	/**
	 * Clean htaccess and scheduled hooks on deactivation
	 */
	public static function deactivate(){
		wp_clear_scheduled_hook('swift_performance_clear_cache');
		wp_clear_scheduled_hook('swift_performance_clear_assets_cache');
		self::write_rewrite_rules();
		self::early_loader(true);
	}

	/**
	 * Generate htaccess and scheduled hooks on activation
	 */
	public static function activate(){
		// Prepare the setup wizard
		set_transient('swift-performance-setup', 'uid:'.get_current_user_id(), 300);

		// Schedule clear cache if cache mode is timebased
		if (self::check_option('enable-caching', 1) && self::check_option('cache-expiry-mode', 'timebased')){
			if (!wp_next_scheduled( 'swift_performance_clear_expired')) {
				wp_schedule_event(time() + self::get_option('cache-expiry-time'), 'swift_performance_cache_expiry', 'swift_performance_clear_expired');
			}
		}

		// Schedule clear assets cache if proxy is enabled
		if (self::check_option('merge-scripts', 1) && self::check_option('proxy-3rd-party-assets', 1)){
			if (!wp_next_scheduled( 'swift_performance_clear_assets_cache')) {
				wp_schedule_event(time(), 'swift_performance_assets_cache_expiry', 'swift_performance_clear_assets_cache');
			}
		}

		// Build rewrite rules
		$rules = self::build_rewrite_rules();
		self::write_rewrite_rules($rules);

		// Early loader
		self::early_loader();
	}

	/**
	 * Write rewrite rules, clear scheduled hooks, set schedule (if necessary), clear cache on save
	 */
	public static function options_saved(){
		// Build rewrite rules
		$rules = self::build_rewrite_rules();
		self::write_rewrite_rules($rules);

		// Clear previously scheduled hooks
		wp_clear_scheduled_hook('swift_performance_clear_cache');
		wp_clear_scheduled_hook('swift_performance_clear_assets_cache');

		// Clear cache
		add_action('shutdown', function(){
			Swift_Performance_Cache::clear_all_cache();
		},PHP_INT_MAX);

		// Schedule clear cache if cache mode is timebased
		if (self::check_option('enable-caching', 1) && self::check_option('cache-expiry-mode', 'timebased')){
			if (!wp_next_scheduled( 'swift_performance_clear_expired')) {
    				wp_schedule_event(time() + self::get_option('cache-expiry-time'), 'swift_performance_cache_expiry', 'swift_performance_clear_expired');
  			}
		}

		// Schedule clear assets cache if proxy is enabled
		if (self::check_option('merge-scripts', 1) && self::check_option('proxy-3rd-party-assets', 1)){
			if (!wp_next_scheduled( 'swift_performance_clear_assets_cache')) {
    				wp_schedule_event(time(), 'swift_performance_assets_cache_expiry', 'swift_performance_clear_assets_cache');
  			}
		}

		self::early_loader();
	}

	/**
	 * Build rewrite rules based on settings and server software
	 */
	public static function build_rewrite_rules(){
		$rules = $errors = array();
		$server_software = self::server_software();
		try{
			if (isset($_REQUEST['data'])){
				parse_str(urldecode($_REQUEST['data']), $data);
			}
			else {
				$data['swift_performance_options'] = get_option('swift_performance_options', array());
			}

			// Compression
			if (isset($data['swift_performance_options']['enable-caching']) && $data['swift_performance_options']['enable-caching'] == 1 && isset($data['swift_performance_options']['enable-gzip']) && $data['swift_performance_options']['enable-gzip'] == 1){
				switch($server_software){
					case 'apache':
						$rules['compression'] = apply_filters('swift_performance_browser_gzip', file_get_contents(SWIFT_PERFORMANCE_DIR . 'modules/cache/rewrites/htaccess-deflate.txt'));
						break;
					case 'nginx':
						$rules['compression'] = apply_filters('swift_performance_browser_gzip', file_get_contents(SWIFT_PERFORMANCE_DIR . 'modules/cache/rewrites/nginx-deflate.txt'));
						break;
					default:
						throw new Exception(esc_html__('Advanced Cache Control doesn\'t supported on your server', 'swift-performance'));
				}
			}
			// Browser cache
			if (isset($data['swift_performance_options']['enable-caching']) && $data['swift_performance_options']['enable-caching'] == 1 && isset($data['swift_performance_options']['browser-cache']) && $data['swift_performance_options']['browser-cache'] == 1){
				switch($server_software){
					case 'apache':
						$rules['cache-control'] = apply_filters('swift_performance_browser_cache', file_get_contents(SWIFT_PERFORMANCE_DIR . 'modules/cache/rewrites/htaccess-browser-cache.txt'));
						break;
					case 'nginx':
						$rules['cache-control'] = apply_filters('swift_performance_browser_cache', file_get_contents(SWIFT_PERFORMANCE_DIR . 'modules/cache/rewrites/nginx-browser-cache.txt'));
						break;
					default:
						throw new Exception(esc_html__('Advanced Cache Control doesn\'t supported on your server', 'swift-performance'));
				}
			}
			if (isset($data['swift_performance_options']['enable-caching']) && $data['swift_performance_options']['enable-caching'] == 1 && isset($data['swift_performance_options']['caching-mode']) && $data['swift_performance_options']['caching-mode'] == 'disk_cache_rewrite'){
				switch($server_software){
					case 'apache':
						$rules['basic'] = apply_filters('swift_performance_cache_rewrites', include_once SWIFT_PERFORMANCE_DIR . 'modules/cache/rewrites/htaccess.php');
						break;
					case 'nginx':
						$rules['basic'] = apply_filters('swift_performance_cache_rewrites', include_once SWIFT_PERFORMANCE_DIR . 'modules/cache/rewrites/nginx.php');;
						break;
					default:
						throw new Exception(esc_html__('Rewrite mode isn\'t supported on your server', 'swift-performance'));
				}
			}
			// CORS
			if (isset($data['swift_performance_options']['enable-cdn']) && $data['swift_performance_options']['enable-cdn'] == 1 && isset($data['swift_performance_options']['enable-cdn-ssl']) && $data['swift_performance_options']['enable-cdn-ssl'] == 1){
				switch($server_software){
					case 'apache':
						$rules['cors'] = apply_filters('swift_performance_cors_rules', file_get_contents(SWIFT_PERFORMANCE_DIR . 'modules/cdn/rewrites/htaccess.txt'));
						break;
					case 'nginx':
						$rules['cache-control'] = apply_filters('swift_performance_cors_rules', file_get_contents(SWIFT_PERFORMANCE_DIR . 'modules/cdn/rewrites/nginx.txt'));
						break;
					default:
						throw new Exception(esc_html__('Advanced Cache Control doesn\'t supported on your server', 'swift-performance'));
				}
			}

			Swift_Performance::log('Build rewrite rules', 9);
			return $rules;
		}
		catch(Exception $e){
			self::add_notice($e->getMessage(), 'error');
			Swift_Performance::log('Build rewrite rules error: ' . $e->getMessage, 1);
		}
	}


	/**
	 * Write rewrite rules if it is possible, otherwise add warning with rules
	 * @param array $rules
	 */
	public static function write_rewrite_rules($rules = array()){
		$multisite_padding = (is_multisite() ? ' - ' . hash('crc32',home_url()) : '');
		$server_software = self::server_software();
		if ($server_software == 'apache' && file_exists(ABSPATH . '.htaccess')){
			if (is_writable(ABSPATH . '.htaccess')){
				$rewrites = '';
				$htaccess = file_get_contents(ABSPATH . '.htaccess');
				$htaccess = preg_replace("~###BEGIN Swift Performance{$multisite_padding}###(.*)###END Swift Performance{$multisite_padding}###\n?~is", '', $htaccess);
				if (!empty($rules)){
					$rewrites = "###BEGIN Swift Performance{$multisite_padding}###\n" . implode("\n", $rules) . "\n###END Swift Performance{$multisite_padding}###\n";
					$htaccess = $rewrites . $htaccess;
				}
				@file_put_contents(ABSPATH . '.htaccess', $htaccess);
				update_option('swift_performance_rewrites', $rewrites, false);
			}
		}
		else if ($server_software == 'nginx'){
			$rewrites = "###BEGIN Swift Performance{$multisite_padding}###\n" . implode("\n", $rules) . "\n###END Swift Performance{$multisite_padding}###\n";
			update_option('swift_performance_rewrites', $rewrites, false);
		}
	}

	/**
	 * Set messages
	 */
	public static function add_notice($message, $type = 'info'){
		$messages = get_option('swift_performance_messages', array());
		$messages[md5($message.$type)] = array('message' => $message, 'type' => $type);
		update_option('swift_performance_messages', $messages);
	}

	/**
	 * Get free threads
	 * @return boolean is there free thread or not
	 */
	public static function get_thread(){
		$max_threads = (defined('SWIFT_PERFORMANCE_THREADS') ? SWIFT_PERFORMANCE_THREADS : self::get_option('max-threads'));
		// Always returns true if limit threads is not enabled or if max-threads option is zero or it isn't exists
		if (self::check_option('limit-threads', 1, '!=') || empty($max_threads)){
			return true;
		}

		$current_threads = get_transient('swift-performance-threads');
		if ((int)$current_threads > (int)$max_threads){
			@$GLOBALS['swift_performance']->modules['cache']->disabled_cache = true;
			return false;
		}
		return true;
	}

	/**
	 * Lock worker thread
	 * @param string $hook action hook to unlock thread
	 */
	public static function lock_thread($hook){
		$current_threads = get_transient('swift-performance-threads');
		set_transient('swift-performance-threads', ($current_threads === false ? 1 : $current_threads + 1), 120);
		if (!empty($hook)){
			add_action($hook, array('Swift_Performance', 'unlock_thread'));
		}
	}

	/**
	 * Unlock worker thread
	 */
	public static function unlock_thread(){
		$current_threads = get_transient('swift-performance-threads');
		if ($current_threads !== false && $current_threads >= 1){
			set_transient('swift-performance-threads', $current_threads - 1, 120);
		}

	}


	/**
	 * Extend is_admin to check if current page is login or register page
	 */
	public static function is_admin() {
		global $pagenow;
    		return (is_admin() || in_array( $pagenow, array( 'wp-login.php', 'wp-register.php' )) || (isset($_GET['vc_editable']) && $_GET['vc_editable'] == 'true') || isset($_GET['customize_theme']) );
	}

	/**
	 * Bypass built in function to be able call it early
	 */
	public static function is_user_logged_in(){
		return (isset($_COOKIE[LOGGED_IN_COOKIE]) && !empty($_COOKIE[LOGGED_IN_COOKIE]));
	}

	/**
	 * Bypass built in function to be able call it early
	 */
	public static function is_404(){
		global $wp_query;
		return (isset( $wp_query ) && !empty($wp_query) ? is_404() : false);
	}

	/**
	 * Bypass built in function to be able call it early
	 */
	public static function is_feed(){
		global $wp_query;
		return (isset( $wp_query ) && !empty($wp_query) ? is_feed() : false);
	}

	/**
	 * Check is the current page an AMP page
	 */
	public static function is_amp($buffer) {
    		return (preg_match('~<html([^>])?\samp(\s|>)~', $buffer));
	}

	public static function is_mobile() {
		if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
			$is_mobile = false;
		} elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false // many mobile devices (all iPhone, iPad, etc.)
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mobi') !== false ) {
				$is_mobile = true;
		} else {
			$is_mobile = false;
		}

		return $is_mobile;
	}

	/**
	 * Bypass built in function to be able get unfiltered home url
	 * @return string
	 */
	public static function home_url(){
		if (defined('WP_HOME')){
			return trailingslashit(WP_HOME);
		}

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if (isset($alloptions['home'])){
			$home_url = $alloptions['home'];
		}
		else {
			global $wpdb;
			$home_url = $wpdb->get_var("SELECT option_value FROM {$wpdb->options} WHERE option_name = 'home'");
		}
		return trailingslashit($home_url);
	}

	/**
	 * Check Swift Performance settings
	 * @param string $key
	 * @param mixed $value
	 * @return boolean
	 */
	public static function check_option($key, $value, $condition = '='){
	      global $swift_performance_options;
	      if ($condition == '='){
	            return self::get_option($key) == $value;
	      }
	      else if ($condition == '!='){
	            return self::get_option($key) != $value;
	      }
		else if (strtoupper($condition) == 'IN'){
			return in_array(self::get_option($key), (array)$value);
		}

	}

	/**
	 * Get Swift Performance option
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public static function get_option($key, $default = ''){
	      global $swift_performance_options;
		if (empty($swift_performance_options)){
		    $swift_performance_options = get_option('swift_performance_options', array());
		}
	      if (isset($swift_performance_options[$key])){
	            return apply_filters('swift_performance_option', $swift_performance_options[$key], $key);
	      }
	      else {
	            return apply_filters('swift_performance_option', false, $key);
	      }
	}

	/**
	 * Set Swift Performance option runtime
	 * @param string $key
	 * @param mixed $default
	 */
	public static function set_option($key, $value){
		global $swift_performance_options;
		$swift_performance_options[$key] = $value;
	}

	/**
	 * Update Swift Performance option permanently
	 * @param string $key
	 * @param mixed $default
	 */
	public static function update_option($key, $value){
		global $swift_performance_options;
		$swift_performance_options[$key] = $value;
		update_option('swift_performance_options', $swift_performance_options);
	}

	/*
	 * Install/Uninstall Early Loader
	 */
	public static function early_loader($deactivate = false){
		if (isset($_REQUEST['data'])){
			parse_str(urldecode($_REQUEST['data']), $data);
		}
		else {
			$data = array();
			$data['swift_performance_options'] = get_option('swift_performance_options', array());
		}
		// Use Loader

		if (!$deactivate && isset($data['swift_performance_options']['early-load']) && $data['swift_performance_options']['early-load'] == 1 && isset($data['swift_performance_options']['caching-mode']) && $data['swift_performance_options']['caching-mode'] != 'disk_cache_rewrite'){
			// Create mu-plugins dir if not exists
			if (!file_exists(WPMU_PLUGIN_DIR)){
				@mkdir(WPMU_PLUGIN_DIR, 0777);
			}
			// Copy loader to mu-plugins
			if (file_exists(WPMU_PLUGIN_DIR)){
				@copy(SWIFT_PERFORMANCE_DIR . 'modules/cache/loader.php', trailingslashit(WPMU_PLUGIN_DIR) . 'swift-performance-loader.php');
			}
		}
		else if (file_exists(trailingslashit(WPMU_PLUGIN_DIR) . 'swift-performance-loader.php')){
			@unlink(trailingslashit(WPMU_PLUGIN_DIR) . 'swift-performance-loader.php');
		}
	}

	/**
	 * Determine the server software
	 */
	public static function server_software(){
		return (preg_match('~(apache|litespeed|LNAMP|Shellrent)~i', $_SERVER['SERVER_SOFTWARE']) ? 'apache' : (preg_match('~(nginx|flywheel)~i', $_SERVER['SERVER_SOFTWARE']) ? 'nginx' : 'unknown'));
	}

	/**
	 * Check API connection
	 * @return array|boolean
	 */
	public static function check_api(){
		$validate = wp_remote_get(SWIFT_PERFORMANCE_API_URL . 'update?purchase_key=' . Swift_Performance::get_option('purchase-key') . '&site=' . urlencode(home_url()), array('timeout' => 60));

		if (!is_wp_error($validate)){
                  if ($validate['response']['code'] == 200){
                        $details = json_decode($validate['body'], true);
				return $details;
                  }
                  else {
                        return false;
                  }
            }

		return false;

	}

	/**
	 * Use compute API
	 * @param array $args
	 * @return string|boolean false on error, response string on success
	 */
	public static function compute_api($args, $mode = 'css'){
		// CSS mode
		if ($mode == 'css'){
			$api_url	= SWIFT_PERFORMANCE_API_URL . 'compute/';
			$option	= 'use-compute-api';
		}
		// JS mode
		else {
			$api_url	= SWIFT_PERFORMANCE_API_URL . 'compute_scripts/';
			$option = 'use-compute-api-scripts';
		}
		if (self::check_option('purchase-key', '') || self::check_option($option, 1, '!=')){
			return false;
		}
		Swift_Performance::log('Call API ('.$mode.') ' . $node->src, 9);
		$response = wp_remote_post (
			$api_url ,array(
					'timeout' => 300,
					'sslverify' => false,
					'user-agent' => 'SwiftPerformance',
					'headers' => array (
							'X-ENVATO-PURCHASE-KEY' => trim (self::get_option('purchase-key'))
					),
					'body' => array (
							'site' => trailingslashit(home_url()),
							'args' => json_encode($args),
							'v'	 => SWIFT_PERFORMANCE_VER
					)
			)
		);

		if (is_wp_error($response)){
			Swift_Performance::log('Compute API (mode: '.$mode.') request error: ' . $reponse->get_error_message(), 1);
			return false;
		}
		else{
			if ($response['response']['code'] != 200){
				Swift_Performance::log('Compute API (mode: '.$mode.') request error: HTTP error ' . $response['response']['code'], 1);
				return false;
			}
			if (empty($response['body'])){
				Swift_Performance::log('Compute API (mode: '.$mode.') body was empty', 6);
				return false;
			}

			return $response['body'];
		}
	}

	/**
       * Get image id from url
       * @param string $url
       * @return int
       */
      public static function get_image_id($url){
            $upload_dir = wp_upload_dir();
		global $wpdb;

            $image = preg_replace('~-(\d*)x(\d*)\.(jpe?g|gif|png)$~', '.$3', str_replace(trailingslashit(apply_filters('swift_performance_media_host', preg_replace('~https?:~','',$upload_dir['baseurl']))), '', preg_replace('~https?:~','', apply_filters('swift_performance_get_image_id_url', $url) )));
            return $wpdb->get_var("SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = '_wp_attached_file' AND meta_value = '{$image}'");
      }

	/**
	 * Get canonicalized path from URL
	 * @param string $address
	 * @return string
	 */
	public static function canonicalize($address){
	    $address = explode('/', $address);
	    $keys = array_keys($address, '..');

	    foreach($keys AS $keypos => $key){
	        array_splice($address, $key - ($keypos * 2 + 1), 2);
	    }

	    $address = implode('/', $address);
	    $address = str_replace('./', '', $address);

	    return $address;
	}

	/**
	 * Write log
	 * @param string $event
	 * @param loglevel $event
	 */
	public static function log($event, $loglevel = 9){
		$loglevels = array(
			'9' => 'Event',
			'8' => 'Notice',
			'6' => 'Warning',
			'1' => 'Error'
		);
		if (Swift_Performance::check_option('enable-logging', 1) && Swift_Performance::get_option('loglevel') <= $loglevel){
		    $file	= Swift_Performance::get_option('log-path') . date('Y-m-d') . '.txt';
		    $entry	= date('Y-m-d H:i:s') . ' [' . $loglevels[$loglevel] . '] ' . $event . "\n";
		    @file_put_contents($file, $entry, FILE_APPEND);
		}
	}

	/**
	 * Admin panel template loader
	 */
	public static function panel_template(){
		include_once SWIFT_PERFORMANCE_DIR . 'templates/header.php';

		$subpage = (isset($_GET['subpage']) ? $_GET['subpage'] : 'dashboard');

		switch ($subpage) {
			case 'dasboard':
			default:
				include_once SWIFT_PERFORMANCE_DIR . 'templates/dashboard.php';
				break;
			case 'settings':
				return true;
			case 'image-optimizer':
				include_once SWIFT_PERFORMANCE_DIR . 'templates/image-optimizer.php';
				break;
			case 'db-optimizer':
				include_once SWIFT_PERFORMANCE_DIR . 'templates/db-optimizer.php';
				break;
			case 'smart-plugin-loader':
				include_once SWIFT_PERFORMANCE_DIR . 'templates/smart-plugin-loader.php';
				break;
			case 'smart-asset-loader':
				include_once SWIFT_PERFORMANCE_DIR . 'templates/smart-asset-loader.php';
				break;
		}

		return false;
	}

	/**
	 * Return available menu elements as an array
	 */
	public static function get_menu(){
		$elements = array(
			array('slug' => 'dashboard', 'name' => __('Dashboard', 'swift-performance')),
			array('slug' => 'settings', 'name' =>  __('Settings', 'swift-performance')),
			array('slug' => 'image-optimizer', 'name' =>  __('Image Optimizer', 'swift-performance')),
		);
		return $elements;
	}

	/**
	 * Return actual cache sizes and cached files
	 * @return array
	 */
	public static function cache_status(){
		$files = array();
		$cache_size = 0;
		if (Swift_Performance::check_option('caching-mode', array('disk_cache_rewrite', 'disk_cache_php'), 'IN') && file_exists(SWIFT_PERFORMANCE_CACHE_DIR)){
			$Directory = new RecursiveDirectoryIterator(SWIFT_PERFORMANCE_CACHE_DIR);
			$Iterator = new RecursiveIteratorIterator($Directory);
			$Regex = new RegexIterator($Iterator, '/((index|404)\.html|index.xml)$/i', RecursiveRegexIterator::GET_MATCH);
			foreach($Regex as $filename=>$file){
				if (filesize($filename) > 0){
					$url			= trim(preg_replace('~(desktop|mobile)/(authenticated|unauthenticated)(/[abcdef0-9]*)?/((index|404)\.html|index.xml)~','',str_replace(SWIFT_PERFORMANCE_CACHE_DIR, trailingslashit(basename(SWIFT_PERFORMANCE_CACHE_DIR)), $filename)),'/');
					$files[$url] 	= $url;
					$cache_size		+= filesize($filename);
					if (file_exists($filename . '.gz')){
						$cache_size		+= filesize($filename.'.gz');
					}
				}
			}
		}
		else if (Swift_Performance::check_option('caching-mode', 'memcached_php')){
			$memcached = Swift_Performance_Cache::get_memcache_instance();
			$keys = $memcached->getAllKeys();
			foreach($keys as $item) {
			    if(preg_match('~^swift-performance~', $item)) {
				  $url = preg_replace('~^swift-performance', '', $permalink);
				  $files[$url] = $url;
				  $cache_size  += strlen($filename);
			    }
			}
		}

		usort($files, function($a,$b){
			return substr_count($a,'/') > substr_count($b,'/');
		});

		return array(
			'files' => $files,
			'cache_size' => $cache_size
		);
	}

}

if (!isset($GLOBALS['swift_performance']) || empty($GLOBALS['swift_performance'])){
	$GLOBALS['swift_performance'] = new Swift_Performance();
}
?>
