<?php

class Swift_Performance_Setup {

	/**
	 * Array of steps
	 * @var array
	 */
	public $steps = array();

	/**
	 * Current step
	 * @var array
	 */
	public $current_step = array();

	/**
	 * Show steps in footer
	 * @var boolean
	 */
	public $show_steps = true;

	/**
	 * Localization array for JS
	 * @var array
	 */
	public $localize = array();

	/**
	 * Analyze array
	 */
	public $analyze = array();

	public $disable_continue = false;

	/**
	 * Catch pseudo function calls and do nothing (we use init for early catch the page);
	 * @param string $function
	 * @param array $params
	 */
	public function __call($function, $params){
		// Do nothing
	}

	/**
	 * Create instance
	 */
	public function __construct() {
		// Ajax handlers
		add_action('wp_ajax_swift_performance_setup', array($this, 'ajax_handler'));

		// Return if page is not the Swift Performance Setup Wizard page
		if (!isset($_GET['subpage']) || $_GET['subpage'] != 'setup' || !isset($_GET['page']) || $_GET['page'] != 'swift-performance' ){
			return false;
		}

		ini_set('display_errors', 0);

		// Set installer directory path
		if (!defined('SWIFT_PERFORMANCE_SETUP_DIR')){
			define ('SWIFT_PERFORMANCE_SETUP_DIR', SWIFT_PERFORMANCE_DIR . 'includes/setup/');
		}

		// Set installer directory URI
		if (!defined('SWIFT_PERFORMANCE_SETUP_URI')){
			define('SWIFT_PERFORMANCE_SETUP_URI', SWIFT_PERFORMANCE_URI . 'includes/setup/');
		}

		// Init steps
		$this->steps = array(
				array(
					'title'	=> (isset($_REQUEST['swift-nonce']) ? esc_html__('Purchase key', 'swift-performance') : esc_html__('Welcome', 'swift-performance')),
					'id'		=> 'purchase-key',
				),
				array(
					'title'		=> ((isset($_REQUEST['swift-nonce']) || Swift_Performance::check_option('purchase-key', '')) ? esc_html__('Analyze your site', 'swift-performance') : esc_html__('Welcome', 'swift-performance')),
					'id'			=> 'analyze',
					'disable-skip'	=> true
				),
				array(
					'title'	=> esc_html__('Caching mode', 'swift-performance'),
					'id'		=> 'caching',
				),
				array(
					'title'	=> esc_html__('Manage Assets', 'swift-performance'),
					'id'		=> 'manage-assets',
				),
				array(
					'title'	=> esc_html__('Images', 'swift-performance'),
					'id'		=> 'images',
				),
				array(
					'title'	=> esc_html__('Finish', 'swift-performance'),
					'id'		=> 'finish',
				)
		);

		// Init
		add_action('admin_init', array($this, 'init'));

		// Change wp title
		add_action('wp_title', array($this, 'wp_title'));
	}

	/**
	 * Init setup wizard
	 */
	public function init(){
		$swift_performance_purchase_key = Swift_Performance::get_option('purchase-key');

		if (!current_user_can('manage_options')){
			return;
		}

		// Localization
		$this->localize = array(
				'i18n' => array(
						'Upload' => esc_html__('Upload', 'swift-performance'),
						'Modify' => esc_html__('Modify', 'swift-performance'),
						'Please wait...' => esc_html__('Please wait...', 'swift-performance')
				),
				'ajax_url'		=> add_query_arg('page', 'swift_performance_setup', admin_url('admin-ajax.php')),
				'nonce'		=> wp_create_nonce('swift-performance-setup'),
				'tgm_endpoint'	=> add_query_arg('page',$GLOBALS['tgmpa']->menu,admin_url('admin.php'))
		);

		// Enqueue Setup Wizard CSS
		wp_enqueue_style('swift-performance-setup', SWIFT_PERFORMANCE_SETUP_URI . 'css/setup.css');

		// Enqueue Setup Wizard JS
		wp_enqueue_script('swift-performance-setup', SWIFT_PERFORMANCE_SETUP_URI . 'js/setup.js');
		wp_localize_script('swift-performance-setup', 'swift_performance', $this->localize);

		//WP admin styles
		wp_enqueue_style( 'wp-admin' );

		// Set current step
		if (!empty($swift_performance_purchase_key)){
			$step = isset($_REQUEST['step']) ? (int)$_REQUEST['step'] : 1;
			unset($this->steps[0]);
		}
		else{
			$step = isset($_REQUEST['step']) ? (int)$_REQUEST['step'] : 0;
		}
		$this->current_step 		= $this->steps[$step];
		$this->current_step['index']	= $step;

		// Do current step actions
		$this->do_step();

		// Render step
		$this->render();
	}

	/**
	 * Do current step actions
	 */
	public function do_step(){
		if (isset($_REQUEST['swift-nonce']) && wp_verify_nonce($_REQUEST['swift-nonce'], 'swift-performance-setup') && current_user_can('manage_options')){
			switch ($this->current_step['id']){
				// Analyze
				case 'analyze':
					// Reset Redux
					global $swift_performance_options;
					$reduxsa = ReduxSAFrameworkInstances::get_instance('swift_performance_options');
					$swift_performance_options = $reduxsa->_default_values();
					update_option('swift_performance_options', $reduxsa->_default_values());

					// Empty cache
					Swift_Performance_Cache::recursive_rmdir();

					$this->analyze();
				break;
				case 'caching':
					// Empty cache
					Swift_Performance_Cache::recursive_rmdir();

					// Analyze
					$this->analyze();
					// Set caching mode to rewrites if it is available
					if (Swift_Performance::server_software() == 'apache' && $this->analyze['htaccess'] && !isset($this->analyze['missing_apache_modules']['mod_rewrite'])){
						Swift_Performance::update_option('caching-mode', 'disk_cache_rewrite');
						try {
							// Generate and write htaccess rules
							$rules = Swift_Performance::build_rewrite_rules();
							Swift_Performance::write_rewrite_rules($rules);
						}
						catch (Exception $e){
							self::print_notice(array('type' => 'error', 'message' => $e->get_error_message()));
						}

					}

				break;
				case 'manage-assets':
					// Empty cache
					Swift_Performance_Cache::recursive_rmdir();

					// Set cache expiry mode
					$expiry_mode = ($_POST['cache-expiry-mode'] == 'timebased' ? 'timebased' : 'intelligent');
					Swift_Performance::update_option('cache-expiry-mode', $expiry_mode);
					// Automated prebuild cache
					Swift_Performance::update_option('automated_prebuild_cache', (isset($_POST['automated-prebuild-cache']) && $_POST['automated-prebuild-cache'] == 'enabled' ? 1 : 0));
					// Browser cache
					Swift_Performance::update_option('browser-cache', (isset($_POST['browser-cache']) && $_POST['browser-cache'] == 'enabled' ? 1 : 0));
					// Gzip
					Swift_Performance::update_option('enable-gzip', (isset($_POST['enable-gzip']) && $_POST['enable-gzip'] == 'enabled' ? 1 : 0));
				break;
				case 'images':
					// Empty cache
					Swift_Performance_Cache::recursive_rmdir();

					$mode = isset($_POST['optimize-assets']) ? $_POST['optimize-assets'] : '';
					if ($mode == 'cache-only'){
						Swift_Performance::update_option('merge-scripts', 0);
						Swift_Performance::update_option('merge-styles', 0);
					}
					else if ($mode == 'merge-only'){
						Swift_Performance::update_option('merge-scripts', 1);
						Swift_Performance::update_option('merge-styles', 1);
						Swift_Performance::update_option('critical-css', 0);
					}
					else if ($mode == 'full'){
						Swift_Performance::update_option('use-compute-api', 1);
						Swift_Performance::update_option('merge-scripts', 1);
						Swift_Performance::update_option('merge-styles', 1);
						Swift_Performance::update_option('merge-background-only', (isset($_POST['merge-background-only']) && $_POST['merge-background-only'] == 'enabled' ? 1 : 0));
					}

					// Limit threads & bypass css import
					if (in_array($mode, array('merge-only', 'full')) && isset($_POST['limit-threads']) && $_POST['limit-threads'] == 'enabled'){
						Swift_Performance::update_option('bypass-css-import', (isset($_POST['bypass-css-import']) && $_POST['bypass-css-import'] == 'enabled' ? 1 : 0));

						Swift_Performance::update_option('limit-threads', 1);
						Swift_Performance::update_option('max-threads', max(0, (int)$_POST['max-threads']));
					}


				break;
				case 'finish':
					// Empty cache
					Swift_Performance_Cache::recursive_rmdir();

					$lazyload 		= (isset($_POST['lazyload']) && $_POST['lazyload'] == 'enabled' ? 1 : 0);
					$optimize 		= (isset($_POST['optimize-images']) && $_POST['optimize-images'] == 'enabled' ? 1 : 0);
					$lossy    		= ($optimize && isset($_POST['enable-lossy-optimiztazion']) && $_POST['enable-lossy-optimiztazion'] == 'enabled' ? 1 : 0);
					$keep_original    = ($optimize && $lossy && isset($_POST['keep-original-images']) && $_POST['keep-original-images'] == 'enabled' ? 1 : 0);

					Swift_Performance::update_option('lazy-load-images', $lazyload);
					Swift_Performance::update_option('optimize-uploaded-images', $optimize);
					Swift_Performance::update_option('enable-lossy-optimiztazion', $lossy);
					Swift_Performance::update_option('keep-original-images', $keep_original);
				break;
			}
		}
	}

	/**
	 * Analyze environment and fill up analyze array
	 */
	public function analyze(){
		// Check other cache/minify plugins to avoid conflicts and double cacheing
		// W3TC
		if (class_exists('\\W3TC\\Root_Loader')){
			$this->disable_continue = true;
			$this->analyze['plugin_conflicts']['W3TC'] = 'W3 Total Cache';
		}
		// WP Super Cache
		if (function_exists('wp_cache_set_home')){
			$this->disable_continue = true;
			$this->analyze['plugin_conflicts']['WPSupercache'] = 'WP Supercache';
		}
		// WP Rocket
		if (defined('WP_ROCKET_VERSION')){
			$this->disable_continue = true;
			$this->analyze['plugin_conflicts']['WPRocket'] = 'WP Rocket';
		}
		// WP Fastest cache
		if (class_exists('WpFastestCache')){
			$this->disable_continue = true;
			$this->analyze['plugin_conflicts']['WPFastestCache'] = 'WP Fastest Cache';
		}
		// Autoptimize
		if (defined('AUTOPTIMIZE_PLUGIN_DIR')){
			$this->disable_continue = true;
			$this->analyze['plugin_conflicts']['Autoptimize'] = 'Autoptimize';
		}
		// BWP Minify
		if (class_exists('BWP_MINIFY')){
			$this->disable_continue = true;
			$this->analyze['plugin_conflicts']['BWPMinify'] = 'Better WordPress Minify';
		}
		// BWP Minify
		if (function_exists('sg_cachepress_start')){
			$this->disable_continue = true;
			$this->analyze['plugin_conflicts']['SGOptimizer'] = 'SG Optimizer';
		}

		// If apache
		if (Swift_Performance::server_software() == 'apache' && function_exists('apache_get_modules')){
			// Check modules
			$this->analyze['missing_apache_modules'] = array_diff(array(
				'mod_expires',
				'mod_deflate',
				'mod_setenvif',
				'mod_headers',
				'mod_filter',
				'mod_rewrite',
			), apache_get_modules());
		}

		// Check htaccess
		$htaccess = trailingslashit(str_replace(site_url(), ABSPATH, Swift_Performance::home_url())) . '.htaccess';

		if (!file_exists($htaccess)){
			@touch($htaccess);
			if (!file_exists($htaccess)){
				$this->analyze['htaccess'] = false;
			}
		}
		else if (!is_writable($htaccess)){
			$this->analyze['htaccess'] = false;
		}
		else {
			$this->analyze['htaccess'] = true;
		}
	}

	/**
	 * Render current step
	 */
	public function render(){
		$template = 'start-wizard';

		if (defined('DOING_AJAX')){
			$GLOBALS['hook_suffix'] = 'swift-performance';
			return;
		}

		// Verify nonce
		if (isset($_REQUEST['swift-nonce']) && wp_verify_nonce($_REQUEST['swift-nonce'], 'swift-performance-setup') && current_user_can('manage_options')){
			// Save settings
			$this->_save_settings();

			// Set template
			$template = $this->current_step['id'];
		}

		// Get header part
		$this->_get_template_part('admin-header');

		// Get Body
		if (!isset($_REQUEST['swift-nonce']) || !wp_verify_nonce($_REQUEST['swift-nonce'], 'swift-performance-setup') && current_user_can('manage_options')){
			$this->_get_template_part($template);
			$this->show_steps = false;
		}
		else{
			$this->_get_template_part($template);
		}

		// Get Footer
		$this->_get_template_part('admin-footer');

		// Exit
		die;
	}

	/**
	 * Print prev/next step links
	 */
	public function step_links() {
		$current 	= $this->current_step['index'];
		$prev		= isset($this->steps[$current-1]) ? '<a class="swift-btn swift-btn-gray swift-btn-lg" href="'. esc_url(wp_nonce_url(add_query_arg('step', ($current-1), add_query_arg('subpage', 'setup', menu_page_url('swift-performance', false))), 'swift-performance-setup', 'swift-nonce')) . '">'.esc_html__('Previous step', 'swift-performance').'</a>' : '';
		if (isset($this->steps[$current+1])){
			$skip = '<a class="swift-btn swift-btn-gray swift-btn-lg swift-skip-step" href="'. esc_url(wp_nonce_url(add_query_arg('step', ($current+1), add_query_arg('subpage', 'setup', menu_page_url('swift-performance', false))), 'swift-performance-setup', 'swift-nonce')) . '">'.esc_html__('Skip this step', 'swift-performance').'</a>';
			$next = wp_nonce_field('swift-performance-setup', 'swift-nonce').
					'<input type="hidden" name="step" value="'.($current + 1).'">'.
					'<input type="hidden" name="swift-performance-setup-action" value="'.esc_attr($this->current_step['id']).'">'.
					'<button class="swift-btn swift-btn-green swift-setup-next" ' . ($this->disable_continue == true ? 'disabled' : '') . '>'.esc_html__('Continue', 'swift-performance').'</button>';
		}
		echo '<div class="swift-setup-btn-wrapper">';
		echo $prev;
		if (!isset($this->current_step['disable-skip']) || !$this->current_step['disable-skip']){
			echo $skip;
		}
		echo $next;
		echo '</div>';
	}

	/**
	 * Handle ajax requests
	 */
	public function ajax_handler(){
		if (!isset($_REQUEST['swift-nonce']) || !wp_verify_nonce($_REQUEST['swift-nonce'], 'swift-performance-setup') && current_user_can('manage_options')){
			wp_die(0);
		}

		if (isset($_REQUEST['ajax-action'])){
			switch ($_REQUEST['ajax-action']){
				case 'pagespeed':
					// Generate cache if not exists
					wp_remote_get(home_url(), array('headers' => array('X-merge-assets' => 'true'), 'useragent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:52.0) Gecko/20100101 Firefox/52.0', 'timeout' => 120));
					if (Swift_Performance::check_option('mobile-support', 1)){
						wp_remote_get(home_url(), array('headers' => array('X-merge-assets' => 'true'), 'useragent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25', 'timeout' => 120));
					}
					$strategy = isset($_POST['strategy']) ? $_POST['strategy'] : 'desktop';
					$result = wp_remote_get(SWIFT_PERFORMANCE_API_URL . 'pagespeed?strategy='.$strategy.'&purchase_key=' . Swift_Performance::get_option('purchase-key') . '&site=' . urlencode(home_url()), array('timeout' => 60));
					if (!is_wp_error($result)){
						$result = json_decode($result['body'], true);
						if ($result !== false && isset($result['score'])){
							echo $result['score'];
						}
						else {
							echo '?';
						}
					}
					break;
			}
		}
		wp_die();
	}

	/**
	 * Go back to the previous step
	 */
	private function _revert_step(){
		$index 				= $this->current_step['index']-1;
		$this->current_step 		= $this->steps[$index];
		$this->current_step['index']	= $index;
	}

	/**
	 * Save settings
	 */
	private function _save_settings(){
		if (current_user_can('manage_options') && isset($_POST['swift-performance-setup-action'])){
			switch ($_POST['swift-performance-setup-action']){
				case 'basic-settings':
					$swift_performance_options = get_option('swift_performance_options');
					foreach ($_POST['swift_performance_setup'] as $key => $value){
						$swift_performance_options[$key] = $value;
					}
					update_option('swift_performance_options', $swift_performance_options);
					break;
				case 'purchase-key':
					//Verify purchase key via Swift API
					$validate = wp_remote_get(SWIFT_PERFORMANCE_API_URL . 'validate?purchase_key=' . $_POST['envato-purchase-key'] . '&site=' . urlencode(home_url()), array('timeout' => 60));

					//Handle HTTP errors
					if (is_wp_error($validate)) {
						self::print_notice(array('type' => 'error', 'message' => esc_html__('Couldn\'t connect to API server, please try again.', 'swift-performance')));
						// Go back;
						$this->_revert_step();
					}
					else{
						if ($validate['response']['code'] == 200){
							unset($this->steps[0]);
 							global $swift_performance_purchase_key;
 							$swift_performance_purchase_key = $_POST['envato-purchase-key'];
 							Swift_Performance::update_option('purchase-key', $swift_performance_purchase_key);
			                  }
			                  else {
							// Go back;
							$this->_revert_step();
							self::print_notice(array('type' => 'error', 'message' => esc_html__('Purchase Key is invalid', 'swift-performance') ));
			                  }

					}
					break;
			}
		}
	}

	/**
	 * Includes the given template
	 * @param string $template
	 */
	private function _get_template_part($template) {
		if (strpos($template, '.') !== false){
			return false;
		}
		if (file_exists(SWIFT_PERFORMANCE_SETUP_DIR . 'templates/' . $template . '.php')){
			include SWIFT_PERFORMANCE_SETUP_DIR . 'templates/' . $template . '.php';
		}
	}

	/**
	 * Function to overwrite <title> tag
	 */
	public function wp_title(){
		return esc_html__( 'Swift Performance Setup Wizard - ', 'swift-performance' );
	}

	/**
	 * Print admin notice
	 * @param array $message
	 */
	public static function print_notice($message){
		$class = ($message['type'] == 'success' ? 'updated' : ($message['type'] == 'warning' ? 'update-nag' : ($message['type'] == 'error' ? 'error' : 'notice')));
		echo '<div class="swift-performance-notice '.$class.'" style="padding:25px 10px 10px 10px;position: relative;display: block;"><span style="color:#888;position:absolute;top:5px;left:5px;">'.esc_html__('Swift Performance','swift-performance').'</span>'.$message['message'].'</div>';
	}

}
