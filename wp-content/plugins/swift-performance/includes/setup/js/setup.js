(function() {

	// Merge assets in background checkbox show/hide
	jQuery(document).on('click change','.swift-performance-box-select [name="optimize-assets"]', function(){
		jQuery('#merge-background-only-container, #limit-threads-container').addClass('swift-hidden');
		if (jQuery('[name="optimize-assets"]:checked').val() == 'merge-only' || jQuery('[name="optimize-assets"]:checked').val() == 'full'){
			jQuery('#merge-background-only-container, #limit-threads-container').removeClass('swift-hidden');
		}
	});

	// Lossy options
	jQuery(document).on('click change','#optimize-images-enabled', function(){
		jQuery('#enable-lossy-optimiztazion-container').addClass('swift-hidden');
		if (jQuery('#optimize-images-enabled:checked').length > 0){
			jQuery('#enable-lossy-optimiztazion-container').removeClass('swift-hidden');
		}
		else {
			jQuery('#enable-lossy-optimiztazion').removeAttr('checked').trigger('change');
		}
	});

	// Lossy options
	jQuery(document).on('click change','#enable-lossy-optimiztazion', function(){
		jQuery('#keep-original-images-container').addClass('swift-hidden');
		if (jQuery('#enable-lossy-optimiztazion:checked').length > 0){
			jQuery('#keep-original-images-container').removeClass('swift-hidden');
		}
	});

	jQuery(function(){
		// Fire select on Load
		jQuery('.swift-performance-box-select [name="optimize-assets"]').trigger('change');

		// Pagespeed
		if (jQuery('.swift-pagespeed').length > 0){
			jQuery('.swift-pagespeed').each(function(){
				if (jQuery(this).hasClass('cached')){
					return true;
				}
				var that = jQuery(this);
				var strategy = jQuery(that).hasClass('strategy-mobile') ? 'mobile' : 'desktop';
				var timer = setTimeout(function(){
					jQuery(that).removeClass('swift-loading');
					jQuery(that).addClass('timeout');
					jQuery(that).empty().html('<span class="dashicons dashicons-clock"></span>');
				}, 120000);
				jQuery.post(swift_performance.ajax_url, {'action': 'swift_performance_setup', 'ajax-action': 'pagespeed', 'swift-nonce': swift_performance.nonce, 'strategy': strategy}, function(response){
					if (response > 0){
						if (response < 60) {
							jQuery(that).addClass('red');
						}
						else if (response < 90) {
							jQuery(that).addClass('yellow');
						}
						else {
							jQuery(that).addClass('green');
						}
					}
					jQuery(that).removeClass('swift-loading');
					jQuery(that).empty().text(response);
					clearTimeout(timer);
				});
			});
		}
	});

	/**
	 * Localization
	 * @param string text
	 * @return string
	 */
	function __(text){
		if (typeof swift_performance_setup.i18n[text] !== 'undefined'){
			return swift_performance_setup.i18n[text];
		}
		else {
			return text;
		}
	}
})();
