<?php global $swift_performance_setup; ?>
<h1><?php esc_html_e('Caching', 'swift-performance'); ?></h1>
<h2><?php esc_html_e('Cache expiry mode', 'swift-performance')?></h2>
<ul class="swift-performance-box-select">
      <li>
            <input type="radio" name="cache-expiry-mode" value="timebased" id="timebased" checked>
            <label for="timebased">
                  <h3><?php esc_html_e('Time based mode', 'swift-performance');?></h3>
                  <span><?php esc_html_e('Best choice for most sites and webshops. WooCommerce, BBPress and Buddypress support is included', 'swift-performance')?></span>
            </label>
      </li>
      <li>
            <input type="radio" name="cache-expiry-mode" value="intelligent" id="intelligent">
            <label for="intelligent">
                  <h3><?php esc_html_e('Intelligent mode', 'swift-performance');?></h3>
                  <span><?php esc_html_e('Choose intelligent mode if some of your pages are modified frequently (eg: live scores)', 'swift-performance')?></span>
            </label>
      </li>
</ul>

<div class="swift-p-row">
      <input type="checkbox" name="automated-prebuild-cache" value="enabled" id="automated-prebuild-cache" checked>
      <label for="automated-prebuild-cache">
            <?php esc_html_e('Prebuild Cache Automatically', 'swift-performance');?>
      </label>
      <p><em><?php esc_html_e('Enable this option to prebuild the cache.', 'swift-performance')?></em></p>
</div>

<div class="swift-p-row">
      <input type="checkbox" name="browser-cache" value="enabled" id="browser-cache" checked>
      <label for="browser-cache">
            <?php esc_html_e('Enable Browser Cache', 'swift-performance');?>
            <?php if (Swift_Performance::server_software() == 'apache' && isset($swift_performance_setup->analyze['missing_apache_modules']['mod_expires'])):?>
            <span class="swift-performance-warning swift-performance-browser-cache-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('Please enable mod_expires in order to work', 'swift-performance');?></span>
            <?php endif;?>
      </label>
      <p><em><?php esc_html_e('If you enable this option it will generate htacess/nginx rules for browser cache.', 'swift-performance')?></em></p>
</div>

<div class="swift-p-row">
      <input type="checkbox" name="enable-gzip" value="enabled" id="enable-gzip" checked>
      <label for="enable-gzip">
            <?php esc_html_e('Enable Gzip', 'swift-performance');?>
            <?php if (Swift_Performance::server_software() == 'apache' && (isset($swift_performance_setup->analyze['missing_apache_modules']['mod_deflate']) || isset($swift_performance_setup->analyze['missing_apache_modules']['mod_filter'])) ):?>
            <span class="swift-performance-warning swift-performance-gzip-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('Please enable mod_deflate and mod_filter in order to work', 'swift-performance');?></span>
            <?php endif;?>
      </label>
      <p><em><?php esc_html_e(' If you enable this option it will generate htacess/nginx rules for gzip compression.', 'swift-performance')?></em></p>
</div>
