<?php global $swift_performance_setup;?>
<h1><?php esc_html_e('Images', 'swift-performance'); ?></h1>

<div class="swift-p-row">
      <input type="checkbox" name="lazyload" value="enabled" id="lazyload-enabled" checked>
      <label for="lazyload-enabled">
            <?php esc_html_e('Enable Lazyload', 'swift-performance');?>
      </label>
      <p><em><?php esc_html_e('Load images only when they appear in the browser’s viewport.', 'swift-performance')?><em></p>
</div>

<div class="swift-p-row">
      <input type="checkbox" name="optimize-images" value="enabled" id="optimize-images-enabled" checked>
      <label for="optimize-images-enabled">
            <?php esc_html_e('Optimize images on upload', 'swift-performance');?>
            <?php if (Swift_Performance::check_option('purchase-key','')):?>
            <div class="swift-performance-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('Image Optimizer and Compute API requires a valid purchase key', 'swift-performance');?></div>
            <?php endif;?>
      </label>
      <p><em><?php esc_html_e('Enable if you would like to optimize the images during the upload using the our Image Optimization API service.', 'swift-performance')?><em></p>
</div>

<div id="enable-lossy-optimiztazion-container" class="swift-p-row">
      <input type="checkbox" name="enable-lossy-optimiztazion" value="enabled" id="enable-lossy-optimiztazion" checked>
      <label for="enable-lossy-optimiztazion">
            <?php esc_html_e('Enable Lossy Optimization', 'swift-performance');?>
            <?php if (Swift_Performance::check_option('purchase-key','')):?>
            <div class="swift-performance-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('Image Optimizer and Compute API requires a valid purchase key', 'swift-performance');?></div>
            <?php endif;?>
      </label>
      <p><em><?php esc_html_e('Enable slightly lossy optimization for Image Optimizer', 'swift-performance');?><em></p>
</div>

<div id="keep-original-images-container" class="swift-p-row">
      <input type="checkbox" name="keep-original-images" value="enabled" id="keep-original-images" checked>
      <label for="keep-original-images">
            <?php esc_html_e('Keep Original Images', 'swift-performance');?>
            <?php if (Swift_Performance::check_option('purchase-key','')):?>
            <div class="swift-performance-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('Image Optimizer and Compute API requires a valid purchase key', 'swift-performance');?></div>
            <?php endif;?>
      </label>
      <p><em><?php esc_html_e('If you enable this option you will be able to keep and restore original images. It will take some storage', 'swift-performance')?><em></p>
</div>
