<?php global $swift_performance_setup;?>
<h1><?php esc_html_e('Manage Assets', 'swift-performance'); ?></h1>
<h2><?php esc_html_e('Optimize static resources', 'swift-performance')?></h2>
<ul class="swift-performance-box-select three">
      <li>
            <input type="radio" name="optimize-assets" value="cache-only" id="cache-only">
            <label for="cache-only">
                  <h3><?php esc_html_e('Cache only', 'swift-performance');?></h3>
                  <span><?php esc_html_e('Use Swift Performance only for caching', 'swift-performance')?></span>
            </label>
      </li>
      <li>
            <input type="radio" name="optimize-assets" value="merge-only" id="merge-only">
            <label for="merge-only">
                  <h3><?php esc_html_e('Minimal optimization', 'swift-performance');?></h3>
                  <span><?php esc_html_e('Use caching and optimize static resources', 'swift-performance')?></span>
            </label>
      </li>
      <li>
            <input type="radio" name="optimize-assets" value="full" id="full" checked>
            <label for="full">
                  <h3><?php esc_html_e('Full optimization', 'swift-performance');?></h3>
                  <span><?php esc_html_e('Caching + Optimize static resources + Critical CSS', 'swift-performance')?></span>
            </label>
            <?php if (Swift_Performance::check_option('purchase-key', '')): ?>
            <span class="swift-performance-warning swift-performance-compute-api-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('Add a valid purchase key in order to use Compute API', 'swift-performance');?></span>
            <?php endif;?>
      </li>
</ul>

<div id="bypass-import-container" class="swift-p-row">
      <input type="checkbox" name="bypass-css-import" value="enabled" id="bypass-css-import" checked>
      <label for="bypass-css-import">
            <?php esc_html_e('Bypass CSS Import', 'swift-performance');?>
      </label>
      <p><em><?php esc_html_e('Include imported CSS files in merged styles.', 'swift-performance')?></em></p>
</div>

<div class="swift-hidden swift-p-row" id="merge-background-only-container">
      <input type="checkbox" name="merge-background-only" value="enabled" id="merge-background-only" checked>
      <label for="merge-background-only">
            <?php esc_html_e('Merge Assets in Background', 'swift-performance');?>
      </label>
      <p><em><?php esc_html_e('In some cases the generating the critical CSS takes some time. If you enable this option the plugin will generate it asynchronously in the background.', 'swift-performance')?></em></p>
</div>

<div class="swift-hidden swift-p-row" id="limit-threads-container">
      <input type="checkbox" name="limit-threads" value="enabled" id="limit-threads">
      <label for="limit-threads">
            <?php esc_html_e('Limit Simultaneous Threads', 'swift-performance');?>
      </label>
      <div class="swift-performance-max-threads">
            <label><?php esc_html_e('Maximum threads', 'swift-performance')?> </label>
            <input type="number" min="1" name="max-threads" value="10">
      </div>
      <p><em><?php esc_html_e('Limit maximum simultaneous threads. It can be useful on shared hosting environment to avoid 508 errors.', 'swift-performance')?></em></p>
</div>
