<?php global $swift_performance_setup;?>
<?php if (Swift_Performance::check_option('purchase-key', '', '!=')):?>
      <h1><?php esc_html_e('Current Performance', 'swift-performance'); ?></h1>
      <div class="swift-pagespeed-container">
            <b><?php esc_html_e('Mobile', 'swift-performance')?></b>
            <div title="<?php esc_html_e('PageSpeed Score', 'swift-performance')?>" class="swift-pagespeed swift-loading strategy-mobile">
                  <div class="swift-loader-circle">
                    <div class="swift-loader-circle1 swift-loader-child"></div>
                    <div class="swift-loader-circle2 swift-loader-child"></div>
                    <div class="swift-loader-circle3 swift-loader-child"></div>
                    <div class="swift-loader-circle4 swift-loader-child"></div>
                    <div class="swift-loader-circle5 swift-loader-child"></div>
                    <div class="swift-loader-circle6 swift-loader-child"></div>
                    <div class="swift-loader-circle7 swift-loader-child"></div>
                    <div class="swift-loader-circle8 swift-loader-child"></div>
                    <div class="swift-loader-circle9 swift-loader-child"></div>
                    <div class="swift-loader-circle10 swift-loader-child"></div>
                    <div class="swift-loader-circle11 swift-loader-child"></div>
                    <div class="swift-loader-circle12 swift-loader-child"></div>
                  </div>
            </div>
      </div>
      <div class="swift-pagespeed-container">
            <b><?php esc_html_e('Desktop', 'swift-performance')?></b>
            <div title="<?php esc_html_e('PageSpeed Score', 'swift-performance')?>" class="swift-pagespeed swift-loading">
                  <div class="swift-loader-circle">
                    <div class="swift-loader-circle1 swift-loader-child"></div>
                    <div class="swift-loader-circle2 swift-loader-child"></div>
                    <div class="swift-loader-circle3 swift-loader-child"></div>
                    <div class="swift-loader-circle4 swift-loader-child"></div>
                    <div class="swift-loader-circle5 swift-loader-child"></div>
                    <div class="swift-loader-circle6 swift-loader-child"></div>
                    <div class="swift-loader-circle7 swift-loader-child"></div>
                    <div class="swift-loader-circle8 swift-loader-child"></div>
                    <div class="swift-loader-circle9 swift-loader-child"></div>
                    <div class="swift-loader-circle10 swift-loader-child"></div>
                    <div class="swift-loader-circle11 swift-loader-child"></div>
                    <div class="swift-loader-circle12 swift-loader-child"></div>
                  </div>
            </div>
      </div>
      <br><br>
<?php endif;?>
<h1><?php esc_html_e('Requirements', 'swift-performance'); ?></h1>
<ul class="swift-performance-setup-errors">
<?php if(!empty($swift_performance_setup->analyze['plugin_conflicts'])):?>
<li class="swift-performance-fail"><span class="dashicons dashicons-no"></span><?php esc_html_e('Plugin conflicts', 'swift-performance');?></li>
<li class="swift-performance-setup-errors-section-details">
      <?php echo sprintf(esc_html__('Please %sdeactivate%s the following plugins and refresh this page', 'swift-performance'), '<a href="' . admin_url('plugins.php') . '" target="_blank">', '</a>');?> <a class="swift-refresh" href="<?php echo esc_url(site_url($_SERVER['REQUEST_URI']))?>"><span class="dashicons dashicons-update"></span></a>
      <ul>
            <?php foreach($swift_performance_setup->analyze['plugin_conflicts'] as $plugin):?>
                  <li class="swift-performance-fail"><?php echo esc_html($plugin)?></li>
            <?php endforeach;?>
      </ul>
</li>
<?php else:?>
      <li class="swift-performance-pass"><span class="dashicons dashicons-yes"></span><?php esc_html_e('Plugin conflicts', 'swift-performance');?></li>
<?php endif;?>

<?php if(!empty($swift_performance_setup->analyze['missing_apache_modules'])):?>
<li class="swift-performance-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('Enable Apache Modules', 'swift-performance');?></li>
<li class="swift-performance-setup-errors-section-details">
      <?php esc_html_e('Please enable the following Apache modules to get better performance', 'swift-performance');?>
      <ul>
      <?php foreach($swift_performance_setup->analyze['missing_apache_modules'] as $module):?>
            <li><?php echo esc_html($module)?></li>
      <?php endforeach;?>
      </ul>
</li>
<?php else:?>
      <li class="swift-performance-pass"><span class="dashicons dashicons-yes"></span><?php esc_html_e('Enabled Apache modules', 'swift-performance');?></li>
<?php endif;?>

<?php if(Swift_Performance::server_software() == 'apache' && !$swift_performance_setup->analyze['htaccess']):?>
<li class="swift-performance-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('htaccess is not writable. Please change file permissions to get better performance', 'swift-performance');?></li>
<?php elseif(Swift_Performance::server_software() == 'apache'):?>
      <li class="swift-performance-pass"><span class="dashicons dashicons-yes"></span><?php esc_html_e('Rewrites are working', 'swift-performance');?></li>
<?php endif;?>

<?php if(Swift_Performance::check_option('purchase-key','')):?>
      <li class="swift-performance-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('Image Optimizer and Compute API requires a valid purchase key', 'swift-performance');?></li>
<?php else: ?>
<?php $details = Swift_Performance::check_api();?>
      <?php if($details == false):?>
            <li class="swift-performance-fail"><span class="dashicons dashicons-no"></span><?php esc_html_e('API Connection error', 'swift-performance');?></li>
            <li class="swift-performance-setup-errors-section-details"><?php echo sprintf(esc_html__('API is not reachable. Please contact support %shere%s', 'swift-performance'), '<a href="http://swift-performance.swteplugins.com/open-a-ticket/" target="_blank">', '</a>');?></li>
      <?php elseif(version_compare(SWIFT_PERFORMANCE_VER, $details['version'], '<')):?>
            <li class="swift-performance-warning"><span class="dashicons dashicons-warning"></span><?php esc_html_e('There is a new version available, please update now', 'swift-performance');?></li>
      <?php else:?>
            <li class="swift-performance-pass"><span class="dashicons dashicons-yes"></span><?php esc_html_e('API connection', 'swift-performance');?></li>
      <?php endif; ?>
<?php endif; ?>
