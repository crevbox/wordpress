<?php
global $swift_performance_setup;
$swift_performance_setup->show_steps = false;
?>
<h1><?php esc_html_e('Your website is ready!', 'swift-performance'); ?></h1>
<?php if (Swift_Performance::check_option('purchase-key', '', '!=')):?>
      <p><?php esc_html_e('Current Performance', 'swift-performance'); ?></p>
      <div class="swift-pagespeed-container">
            <b><?php esc_html_e('Mobile', 'swift-performance')?></b>
            <div title="<?php esc_html_e('PageSpeed Score', 'swift-performance')?>" class="swift-pagespeed swift-loading strategy-mobile" data-url="<?php echo home_url();?>">
                  <div class="swift-loader-circle">
                    <div class="swift-loader-circle1 swift-loader-child"></div>
                    <div class="swift-loader-circle2 swift-loader-child"></div>
                    <div class="swift-loader-circle3 swift-loader-child"></div>
                    <div class="swift-loader-circle4 swift-loader-child"></div>
                    <div class="swift-loader-circle5 swift-loader-child"></div>
                    <div class="swift-loader-circle6 swift-loader-child"></div>
                    <div class="swift-loader-circle7 swift-loader-child"></div>
                    <div class="swift-loader-circle8 swift-loader-child"></div>
                    <div class="swift-loader-circle9 swift-loader-child"></div>
                    <div class="swift-loader-circle10 swift-loader-child"></div>
                    <div class="swift-loader-circle11 swift-loader-child"></div>
                    <div class="swift-loader-circle12 swift-loader-child"></div>
                  </div>
            </div>
      </div>
      <div class="swift-pagespeed-container">
            <b><?php esc_html_e('Desktop', 'swift-performance')?></b>
            <div title="<?php esc_html_e('PageSpeed Score', 'swift-performance')?>" class="swift-pagespeed swift-loading" data-url="<?php echo home_url();?>">
                  <div class="swift-loader-circle">
                    <div class="swift-loader-circle1 swift-loader-child"></div>
                    <div class="swift-loader-circle2 swift-loader-child"></div>
                    <div class="swift-loader-circle3 swift-loader-child"></div>
                    <div class="swift-loader-circle4 swift-loader-child"></div>
                    <div class="swift-loader-circle5 swift-loader-child"></div>
                    <div class="swift-loader-circle6 swift-loader-child"></div>
                    <div class="swift-loader-circle7 swift-loader-child"></div>
                    <div class="swift-loader-circle8 swift-loader-child"></div>
                    <div class="swift-loader-circle9 swift-loader-child"></div>
                    <div class="swift-loader-circle10 swift-loader-child"></div>
                    <div class="swift-loader-circle11 swift-loader-child"></div>
                    <div class="swift-loader-circle12 swift-loader-child"></div>
                  </div>
            </div>
      </div>
      <br><br>
<?php endif;?>
<p>
	<?php if (Swift_Performance::check_option('purchase-key', '', '!=')):?>
		<a href="<?php echo esc_url(add_query_arg('page', 'swift-performance-optimize-images', admin_url('upload.php'))); ?>" class="swift-btn swift-btn-green"><?php echo esc_html__('Optimize images', 'swift-performance'); ?></a>
	<?php endif;?>
	<a href="<?php echo esc_url(menu_page_url('swift-performance',false)); ?>" class="swift-btn swift-btn-gray"><?php echo esc_html__('Swift Performance Settings', 'swift-performance'); ?></a>
	<a href="<?php echo admin_url(); ?>" class="swift-btn swift-btn-gray"><?php echo esc_html__('Back to dashboard', 'swift-performance'); ?></a>
</p>
<p><?php esc_html_e('What\'s next?', 'swift-performance'); ?></p>
<div class="swift-setup-row">
	<div class="swift-setup-col">
		<ul>
			<li><a href="http://swift-performance.swteplugins.com/documentation/" target="_blank"><?php esc_html_e('Documentation', 'swift-performance'); ?></a></li>
			<li><a href="http://swift-performance.swteplugins.com/open-a-ticket/" target="_blank"><?php esc_html_e('Support', 'swift-performance'); ?></a></li>
			<li><a href="http://codecanyon.net/user/swte/follow" target="_blank"><?php esc_html_e('Follow us on Envato', 'swift-performance'); ?></a></li>
		</ul>
	</div>
</div>
