<?php

class ReduxSA_VendorURL{
      public static function get_url($handle){
            switch ($handle) {
                  case 'select2-css':
                        return ReduxSAFramework::$_url . 'assets/css/select2.css';
                  case 'select2-js':
                        return ReduxSAFramework::$_url . 'assets/js/select2.min.js';
            }

      }
}

    /**
     * ReduxSAFramework Config File
     */

    if ( ! class_exists( 'ReduxSA' ) ) {
        return;
    }

    // Get page list
    global $wpdb;
    foreach ($wpdb->get_results("SELECT ID, post_title FROM {$wpdb->posts} WHERE post_type = 'page' AND post_status = 'publish'", ARRAY_A) as $_page){
          $reduxsa_pages[$_page['ID']] = $_page['post_title'];
    }

    // Check IP source automatically for GA
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'HTTP_CF_CONNECTING_IP','REMOTE_ADDR') as $source) {
         if (isset($_SERVER[$source]) && !empty($_SERVER[$source])){
                     $reduxsa_ga_ip_source = $source;
                     break;
         }
   }

    // Basic options
    $cache_modes = array(
          'disk_cache_rewrite' => esc_html__('Disk Cache with Rewrites', 'swift-performance'),
          'disk_cache_php' => esc_html__('Disk Cache with PHP', 'swift-performance'),
    );

    if (class_exists('Memcached')){
          $cache_modes['memcached_php'] = esc_html__('Memcached with PHP', 'swift-performance');
    }

    // Memcached support
    // TODO memcache isn't implemented in 1.0
    if (false && class_exists('Memcached')){
      $cache_modes['memcached_php'] = esc_html__('Memcached with PHP', 'swift-performance');
    }

    /**
     * Validate Purchase Key via API
     * @param array $field
     * @param mixed $value
     * @param mixed $existing_value
     * @return array
     */
    function swift_performance_validate_purchase_key_callback($field, $value, $existing_value) {
      $error            = true;
      $field['msg']     = esc_html__('API connection error, please try later', 'swift-performance');
      $return['value']  = '';

      $validate = wp_remote_get(SWIFT_PERFORMANCE_API_URL . 'validate?purchase_key=' . $value . '&site=' . urlencode(home_url()));

            if (!is_wp_error($validate)){
                  if ($validate['response']['code'] == 200){
                        $return['value'] = $value;
                        $error = false;
                  }
                  else {
                        $field['msg'] = esc_html__('Purchase Key is invalid', 'swift-performance');
                  }
            }

            if ($error == true) {
                $return['error']  = $field;
            }
            return $return;
      }

      /**
       * Check is the cache path exists and writable
       * @param array $field
       * @param mixed $value
       * @param mixed $existing_value
       * @return array
       */
      function swift_performance_validate_cache_path_callback($field, $value, $existing_value) {
            $return['value']  = $value;
            $error = false;

            if (!file_exists($value)){
                  @mkdir($value, 0777, true);
                  if (!file_exists($value)){
                        $error = true;
                        $field['msg'] = esc_html__('Cache directory doesn\'t exists', 'swift-performance');
                  }
            }
            else if (!is_dir($value)){
                  $error = true;
                  $field['msg'] = esc_html__('Cache directory should be a directory', 'swift-performance');
            }
            else if (!is_writable($value)){
                  $error = true;
                  $field['msg'] = esc_html__('Cache directory isn\'t writable for WordPress. Please change the permissions.', 'swift-performance');
            }

            if ($error == true) {
                  $return['value']  = $existing_value;
                  $return['error']  = $field;
            }
            return $return;
    }

    /**
     * Check is the log path exists and writable
     * @param array $field
     * @param mixed $value
     * @param mixed $existing_value
     * @return array
     */
    function swift_performance_validate_log_path_callback($field, $value, $existing_value) {
          $return['value']  = $value;
          $error = false;

          if (!file_exists($value)){
                @mkdir($value, 0777, true);
                if (!file_exists($value)){
                      $error = true;
                      $field['msg'] = esc_html__('Log directory doesn\'t exists', 'swift-performance');
                }
          }
          else if (!is_dir($value)){
                $error = true;
                $field['msg'] = esc_html__('Log directory should be a directory', 'swift-performance');
          }
          else if (!is_writable($value)){
                $error = true;
                $field['msg'] = esc_html__('Log directory isn\'t writable for WordPress. Please change the permissions.', 'swift-performance');
          }

          if ($error == true) {
                $return['value']  = $existing_value;
                $return['error']  = $field;
          }
          return $return;
 }

    /**
     * Check is the mu-plugins path exists and writable
     * @param array $field
     * @param mixed $value
     * @param mixed $existing_value
     * @return array
     */
    function swift_performance_validate_muplugins_callback($field, $value, $existing_value) {
          $muplugins_dir = WPMU_PLUGIN_DIR;
          $return['value']  = $value;
          $error = false;

          if ($value == 1){
                if (!file_exists($muplugins_dir)){
                      @mkdir($muplugins_dir, 0777);
                      if (!file_exists($muplugins_dir)){
                            $error = true;
                            $field['msg'] = esc_html__('MU Plugins directory doesn\'t exists', 'swift-performance');
                      }
                }
                else if (!is_writable($muplugins_dir)){
                      $error = true;
                      $field['msg'] = esc_html__('MU Plugins directory isn\'t writable for WordPress. Please change the permissions.', 'swift-performance');
                }
          }

          if ($error == true) {
                $return['value']  = $existing_value;
                $return['error']  = $field;
          }
          return $return;
 }

    /**
     * Check is htaccess writable
     * @param array $field
     * @param mixed $value
     * @param mixed $existing_value
     * @return array
     */
    function swift_performance_validate_cache_mode_callback($field, $value, $existing_value) {
          $return['value']  = $value;
          $error = false;

          // Check htaccess only for Apache
          if (Swift_Performance::server_software() != 'apache'){
                return $return;
          }

          $htaccess = ABSPATH . '.htaccess';

          if (!file_exists($htaccess)){
                @touch($htaccess);
                if (!file_exists($htaccess)){
                      $error = true;
                      $field['msg'] = esc_html__('htaccess doesn\'t exists', 'swift-performance');
                }
          }
          else if (!is_writable($htaccess)){
                $error = true;
                $field['msg'] = esc_html__('htaccess isn\'t writable for WordPress. Please change the permissions.', 'swift-performance');
          }

          if ($error == true) {
                $return['value']  = $existing_value;
                $return['error']  = $field;
          }
          return $return;
 }


    $opt_name = "swift_performance_options";

    $args = array(
        'opt_name'             => $opt_name,
        'display_name'         => esc_html__('Settings', 'swift-performance'),
        'display_version'      => false,
        'menu_type'            => 'submenu',
        'allow_sub_menu'       => true,
        'menu_title'           => __( 'Swift Performance', 'swift-performance' ),
        'page_title'           => __( 'Swift Performance', 'swift-performance' ),
        'google_api_key'       => '',
        'google_update_weekly' => false,
        'async_typography'     => true,
        'admin_bar'            => false,
        'admin_bar_icon'       => 'dashicons-dashboard',
        'admin_bar_priority'   => 50,
        'global_variable'      => '',
        'dev_mode'             => false,
        'update_notice'        => false,
        'customizer'           => true,
        'page_priority'        => 2,
        'page_parent'          => 'tools.php',
        'page_permissions'     => 'manage_options',
        'menu_icon'            => '',
        'last_tab'             => '',
        'page_icon'            => 'icon-dashboard',
        'page_slug'            => 'swift-performance',
        'save_defaults'        => true,
        'default_show'         => false,
        'default_mark'         => '',
        'show_import_export'   => true,
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => false,
        'output_tag'           => false,
        'database'             => '',
        'use_cdn'              => false,
        'footer_credit'        => ' ',
        'hints'                => array(
              'icon'          => 'el el-question-sign',
              'icon_position' => 'right',
              'icon_color'    => 'lightgray',
              'icon_size'     => 'normal',
              'tip_style'     => array(
                  'color'   => 'light',
                  'shadow'  => true,
                  'rounded' => false,
                  'style'   => '',
              ),
              'tip_position'  => array(
                  'my' => 'top left',
                  'at' => 'bottom right',
              ),
              'tip_effect'    => array(
                  'show' => array(
                      'effect'   => 'slide',
                      'duration' => '500',
                      'event'    => 'mouseover',
                  ),
                  'hide' => array(
                      'effect'   => 'slide',
                      'duration' => '500',
                      'event'    => 'click mouseleave',
                  ),
            ),
        )
      );

    ReduxSA::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     *
     * ---> START SECTIONS
     *
     */

     ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'General', 'swift-performance' ),
                 'id' => 'general-tab',
            )
      );

     ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'General', 'swift-performance' ),
                 'id' => 'general-sub',
                 'subsection' => true,
                 'fields' => array (
                        array(
                           'id'         => 'purchase-key',
                           'type'       => 'text',
                           'title'      => esc_html__( 'Purchase Key', 'swift-performance' ),
                           'validate_callback' => 'swift_performance_validate_purchase_key_callback',
                           'default'    => Swift_Performance::get_option('purchase-key')
                        ),
                        array(
                             'id'	=> 'whitelabel',
                             'type'	=> 'checkbox',
                             'title' => esc_html__( 'Hide Footprints', 'swift-performance' ),
                             'subtitle' => esc_html__('Prevent to add Swift Performance response header and HTML comment', 'swift-performance'),
                             'default' => 0
                        ),
                        array(
                             'id'         => 'use-compute-api',
                             'type'       => 'checkbox',
                             'title'      => esc_html__( 'Use Compute API', 'swift-performance' ),
                             'subtitle'   => esc_html__('Speed up merging process and decrease CPU usage.', 'swift-performance'),
                             'default'    => 0,
                             'required'   => array('purchase-key', '!=', '')
                        ),
                        array(
                             'id'	=> 'limit-threads',
                             'type'	=> 'checkbox',
                             'title' => esc_html__( 'Limit Simultaneous Threads', 'swift-performance' ),
                             'subtitle' => esc_html__('Limit maximum simultaneous threads. It can be useful on shared hosting environment to avoid 508 errors.', 'swift-performance'),
                             'default' => 0
                        ),
                        array(
                             'id'         => 'max-threads',
                             'type'       => 'text',
                             'title'	=> esc_html__('Maximum Threads', 'swift-performance'),
                             'subtitle'   => esc_html__('Number of maximum simultaneous threads.', 'swift-performance'),
                             'default'    => 10,
                             'required'   => array('limit-threads', '=', 1),
                        ),
                        array(
                             'id'	=> 'enable-logging',
                             'type'	=> 'checkbox',
                             'title' => esc_html__( 'Debug Log', 'swift-performance' ),
                             'subtitle' => esc_html__('Enable debug logging', 'swift-performance'),
                             'default' => 0
                        ),
                        array(
                            'id'         => 'loglevel',
                            'type'       => 'select',
                            'title'	     => esc_html__( 'Loglevel', 'swift-performance' ),
                            'required'  => array('enable-logging', '=', 1),
                            'options'    => array(
                                  '9'   => esc_html__('All', 'swift-performance'),
                                  '6' => esc_html__('Warning', 'swift-performance'),
                                  '1' => esc_html__('Error', 'swift-performance'),
                            ),
                            'default'    => '1'
                       ),
                        array(
                              'id'	      => 'log-path',
                              'type'	=> 'text',
                              'title'	=> esc_html__( 'Log Path', 'swift-performance' ),
                              'default'   => WP_CONTENT_DIR . '/swift-logs-'.hash('crc32', mt_rand(0,PHP_INT_MAX)).'/',
                              'required'  => array('enable-logging', '=', 1),
                              'validate_callback' => 'swift_performance_validate_log_path_callback',
                        ),
                  )
            )
      );

      ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'Tweaks', 'swift-performance' ),
                 'id' => 'general-tweaks',
                 'subsection' => true,
                 'fields' => array (
                        array(
                             'id'	=> 'normalize-static-resources',
                             'type'	=> 'checkbox',
                             'title'	=> esc_html__( 'Normalize Static Resources', 'swift-performance' ),
                             'subtitle' => esc_html__('Remove unnecessary query string from CSS, JS and image files.', 'swift-performance'),
                             'default' => 1
                        ),
                        array(
                             'id'         => 'dns-prefetch',
                             'type'       => 'checkbox',
                             'title'      => esc_html__( 'Prefetch DNS', 'swift-performance' ),
                             'subtitle'   => esc_html__('Prefetch DNS automatically.', 'swift-performance'),
                             'default'    => 1,
                        ),
                        array(
                             'id'         => 'dns-prefetch-js',
                             'type'       => 'checkbox',
                             'title'      => esc_html__( 'Collect domains from scripts', 'swift-performance' ),
                             'subtitle'   => esc_html__('Collect domains from scripts for DNS Prefetch.', 'swift-performance'),
                             'default'    => 1,
                             'required'   => array('dns-prefetch', '=', 1),
                        ),
                        array(
                             'id'         => 'exclude-dns-prefetch',
                             'type'       => 'multi_text',
                             'title'	=> esc_html__('Exclude DNS Prefetch', 'swift-performance'),
                             'subtitle'   => esc_html__('Exclude domains from DNS prefetch.', 'swift-performance'),
                             'required'   => array('dns-prefetch', '=', 1),
                        ),
                  )
            )
      );


      ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'Heartbeat', 'swift-performance' ),
                 'id' => 'general-heartbeat',
                 'subsection' => true,
                 'fields' => array (
                        array(
                           'id'	=> 'disable-heartbeat',
                           'type'	=> 'checkbox',
                           'title' => esc_html__( 'Disable Heartbeat', 'swift-performance' ),
                           'options' => array(
                                  'index.php'                                            => 'Dashboard',
                                  'edit.php,post.php,post-new.php'                       => 'Posts/Pages',
                                  'upload.php,media-new.php'                             => 'Media',
                                  'edit-comments.php,comment.php'                        => 'Comments',
                                  'nav-menus.php'                                        => 'Menus',
                                  'widgets.php'                                          => 'Widgets',
                                  'theme-editor.php,plugin-editor.php'                   => 'Theme/Plugin Editor',
                                  'users.php,user-new.php,user-edit.php,profile.php'     => 'Users',
                                  'tools.php'                                            => 'Tools',
                                  'options-general.php'                                  => 'Settings',

                           ),
                           'default' => 'default'
                        ),
                        array(
                             'id'         => 'heartbeat-frequency',
                             'type'       => 'select',
                             'title'	=> esc_html__('Heartbeat Frequency', 'swift-performance'),
                             'subtitle'	=> esc_html__('Override heartbeat frequency in seconds', 'swift-performance'),
                             'options'    => array(
                                   10 => 10,
                                   20 => 20,
                                   30 => 30,
                                   40 => 40,
                                   50 => 50,
                                   60 => 60,
                                   70 => 70,
                                   80 => 80,
                                   90 => 90,
                                   100 => 100,
                                   110 => 110,
                                   120 => 120,
                                   130 => 130,
                                   140 => 140,
                                   150 => 150,
                                   160 => 160,
                                   170 => 170,
                                   180 => 180,
                                   190 => 190,
                                   200 => 200,
                                   210 => 210,
                                   220 => 220,
                                   230 => 230,
                                   240 => 240,
                                   250 => 250,
                                   260 => 260,
                                   270 => 270,
                                   280 => 280,
                                   290 => 290,
                                   300 => 300
                             ),
                        )
                  )
            )
      );

      ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'Google Analytics', 'swift-performance' ),
                 'id' => 'general-ga',
                 'subsection' => true,
                 'fields' => array (
                        array(
                           'id'         => 'bypass-ga',
                           'type'       => 'checkbox',
                           'title'      => esc_html__( 'Bypass Google Analitics', 'swift-performance' ),
                           'default'    => 0,
                        ),
                        array(
                          'id'         => 'ga-tracking-id',
                          'type'       => 'text',
                          'title'      => esc_html__( 'Tracking ID', 'swift-performance' ),
                          'subtitle'   => esc_html__('Eg: UA-123456789-12', 'swift-performance'),
                          'required'   => array('bypass-ga', '=', 1),
                        ),
                        array(
                             'id'         => 'ga-ip-source',
                             'type'       => 'select',
                             'title'	=> esc_html__('IP Source', 'swift-performance'),
                             'subtitle'	=> sprintf(esc_html__('Select IP source if your server is behind proxy (eg: Cloudflare). Recommended: %s', 'swift-performance'), $reduxsa_ga_ip_source),
                             'options'    => array(
                                   'HTTP_CLIENT_IP' => 'HTTP_CLIENT_IP',
                                   'HTTP_X_FORWARDED_FOR' => 'HTTP_X_FORWARDED_FOR',
                                   'HTTP_X_FORWARDED' => 'HTTP_X_FORWARDED',
                                   'HTTP_X_CLUSTER_CLIENT_IP' => 'HTTP_X_CLUSTER_CLIENT_IP',
                                   'HTTP_FORWARDED_FOR' => 'HTTP_FORWARDED_FOR',
                                   'HTTP_FORWARDED' => 'HTTP_FORWARDED',
                                   'HTTP_CF_CONNECTING_IP' => 'HTTP_CF_CONNECTING_IP',
                                   'REMOTE_ADDR' => 'REMOTE_ADDR'
                             ),
                             'default'    => $reduxsa_ga_ip_source,
                             'required'   => array('bypass-ga', '=', 1),
                        )
                  )
            )
      );

      ReduxSA::setSection ( $opt_name, array (
                  'title' => esc_html__( 'Images', 'swift-performance' ),
                  'id' => 'general-images',
                  'icon' => 'el el-picture',
                  'fields' => array (
                         array(
                              'id'         => 'optimize-uploaded-images',
                              'type'       => 'checkbox',
                              'title'      => esc_html__( 'Optimize Images', 'swift-performance' ),
                              'subtitle'   => sprintf(esc_html__('Enable if you would like to optimize the images during the upload using the our Image Optimization API service. Already uploaded images can be optimized %shere%s', 'swift-performance'), '<a href="'.esc_url(add_query_arg('page', 'swift-performance-optimize-images', admin_url('upload.php'))).'" target="_blank">', '</a>'),
                              'default'    => 0,
                              'required'   => array('purchase-key', '!=', '')

                         ),
                         array(
                             'id'         => 'enable-lossy-optimiztazion',
                             'type'       => 'checkbox',
                             'title'      => esc_html__('Enable Lossy Optimization', 'swift-performance'),
                             'subtitle'   => esc_html__('Enable slightly lossy optimization for Image Optimizer', 'swift-performance'),
                             'default'    => 0,
                             'required'   => array('purchase-key', '!=', '')
                         ),
                         array(
                             'id'         => 'keep-original-images',
                             'type'       => 'checkbox',
                             'title'      => esc_html__('Keep Original Images', 'swift-performance'),
                             'subtitle'   => esc_html__('If you enable this option you will be able to keep and restore original images. It will take some storage', 'swift-performance'),
                             'default'    => 0,
                             'required'   => array(
                                   array('purchase-key', '!=', ''),
                                   array('enable-lossy-optimiztazion', '=', 1)
                             )
                         ),
                         array(
                             'id'         => 'base64-small-images',
                             'type'       => 'checkbox',
                             'title'      => esc_html__( 'Inline Small Images', 'swift-performance' ),
                             'subtitle'   => esc_html__('Use base64 encoded inline images for small images', 'swift-performance'),
                             'default'    => 0,
                         ),
                         array(
                             'id'         => 'base64-small-images-size',
                             'type'       => 'text',
                             'title'      => esc_html__( 'File Size Limit (bytes)', 'swift-performance' ),
                             'subtitle'   => esc_html__('File size limit for inline images', 'swift-performance'),
                             'default'    => '1000',
                             'required'   => array('base64-small-images', '=', 1),
                         ),
                         array(
                              'id'         => 'exclude-base64-small-images',
                              'type'       => 'multi_text',
                              'title'	=> esc_html__('Exclude Images', 'swift-performance'),
                              'subtitle'   => esc_html__('Exclude images from being embedded if one of these strings is found in the match.', 'swift-performance'),
                              'required'   => array('base64-small-images', '=', 1),
                         ),
                         array(
                              'id'         => 'lazy-load-images',
                              'type'       => 'checkbox',
                              'title'      => esc_html__( 'Lazy Load', 'swift-performance' ),
                              'subtitle'   => esc_html__('Enable if you would like lazy load for images.', 'swift-performance'),
                              'default'    => 1
                         ),
                         array(
                              'id'         => 'exclude-lazy-load',
                              'type'       => 'multi_text',
                              'title'	=> esc_html__('Exclude Images', 'swift-performance'),
                              'subtitle'   => esc_html__('Exclude images from being lazy loaded if one of these strings is found in the match.', 'swift-performance'),
                              'required'   => array('lazy-load-images', '=', 1),
                         ),
                         array(
                              'id'         => 'load-images-on-user-interaction',
                              'type'       => 'checkbox',
                              'title'      => esc_html__( 'Load Images on User Interaction', 'swift-performance' ),
                              'subtitle'   => esc_html__('Enable if you would like to load full images only on user interaction (mouse move, sroll, device motion)', 'swift-performance'),
                              'default'    => 0,
                              'required'   => array('lazy-load-images', '=', 1),
                         ),
                         array(
                             'id'         => 'base64-lazy-load-images',
                             'type'       => 'checkbox',
                             'title'      => esc_html__( 'Inline Lazy Load Images', 'swift-performance' ),
                             'subtitle'   => esc_html__('Use base64 encoded inline images for lazy load', 'swift-performance'),
                             'default'    => 1,
                             'required'   => array('lazy-load-images', '=', 1),
                         ),
                         array(
                             'id'         => 'force-responsive-images',
                             'type'       => 'checkbox',
                             'title'      => esc_html__( 'Force Responsive Images', 'swift-performance' ),
                             'subtitle'   => esc_html__('Force all images to use srcset attribute if it is possible', 'swift-performance'),
                             'default'    => 0,
                         ),
                   )
             )
       );

      ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'Asset Manager', 'swift-performance' ),
                 'id' => 'asset-manager-tab',
                 'icon' => 'el el-list-alt',
           )
      );

      ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'General', 'swift-performance' ),
                 'id' => 'asset-manager-general',
                 'subsection' => true,
                 'fields' => array (
                        array(
                             'id'         => 'merge-assets-logged-in-users',
                             'type'       => 'checkbox',
                             'title'      => esc_html__( 'Merge Assets for Logged in Users', 'swift-performance' ),
                             'subtitle'   => esc_html__('Enable if you would like to merge styles and scripts for logged in users as well.', 'swift-performance'),
                             'default'    => 0
                        ),
                        array(
                             'id'         => 'merge-background-only',
                             'type'       => 'checkbox',
                             'title'      => esc_html__( 'Merge Assets in Background', 'swift-performance' ),
                             'subtitle'   => esc_html__('In some cases the generating the critical CSS takes some time. If you enable this option the plugin will generate it in the background.', 'swift-performance'),
                             'default'    => 0,
                             'required'   => array('enable-caching', '=', 1)
                        ),
                        array(
                             'id'         => 'minify-html',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Minify HTML', 'swift-performance' ),
                             'default'    => 0,
                        ),
                        array(
                             'id'         => 'disable-emojis',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Disable Emojis', 'swift-performance' ),
                             'default'    => 0,
                        ),
                  )
            )
      );

      ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'Scripts', 'swift-performance' ),
                 'id' => 'asset-manager-js',
                 'subsection' => true,
                 'fields' => array (
                        array(
                             'id'         => 'merge-scripts',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Merge Scripts', 'swift-performance' ),
                             'subtitle'   => esc_html__('Merge javascript files to reduce number of HTML requests ', 'swift-performance'),
                             'default'    => 0,
                             'ajax_save' => false
                        ),
                        array(
                             'id'         => 'merge-scripts-exlude-3rd-party',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Exclude 3rd Party Scripts', 'swift-performance' ),
                             'subtitle'   => esc_html__('Exclude 3rd party scripts from merged scripts', 'swift-performance'),
                             'required'   => array('merge-scripts', '=', 1),
                             'default'    => 0,
                        ),
                        array(
                             'id'         => 'exclude-scripts',
                             'type'       => 'multi_text',
                             'title'	=> esc_html__('Exclude Scripts', 'swift-performance'),
                             'subtitle'   => esc_html__('Exclude scripts from being merged if one of these strings is found in the match.', 'swift-performance'),
                             'required'   => array('merge-scripts', '=', 1),
                        ),
                        array(
                             'id'         => 'exclude-script-localizations',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__('Exclude Script Localizations', 'swift-performance'),
                             'subtitle'   => esc_html__('Exclude javascript localizations from merged scirpts.', 'swift-performance'),
                             'default'    => 1,
                             'required'   => array('merge-scripts', '=', 1),
                        ),
                        array(
                             'id'         => 'minify-scripts',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Minify Javascripts', 'swift-performance' ),
                             'default'    => 1,
                             'required'   => array('merge-scripts', '=', 1),
                        ),
                        array(
                             'id'         => 'use-compute-api-scripts',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Better Minify with API', 'swift-performance' ),
                             'subtitle'   => esc_html__('Use better minify with Compute API.', 'swift-performance'),
                             'default'    => 1,
                             'required'   => array(
                                   array('merge-scripts', '=', 1),
                                   array('minify-scripts', '=', 1),
                             )
                        ),
                        array(
                             'id'         => 'proxy-3rd-party-assets',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Proxy 3rd Party Assets', 'swift-performance' ),
                             'subtitle'	=> esc_html__( 'Proxy 3rd party javascript and CSS files which created by javascript (eg: Google Analytics)', 'swift-performance' ),
                             'default'    => 0,
                             'required'   => array('merge-scripts', '=', 1),
                        ),
                        array(
                             'id'         => 'exclude-3rd-party-assets',
                             'type'       => 'multi_text',
                             'title'	=> esc_html__( 'Exclude 3rd Party Assets', 'swift-performance' ),
                             'subtitle'   => esc_html__('Exclude scripts from being proxied if one of these strings is found in the match.', 'swift-performance'),
                             'required'   => array('merge-scripts', '=', 1),
                        ),
                        array(
                             'id'         => 'inline-merged-scripts',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Print merged scripts inline', 'swift-performance' ),
                             'subtitle'   => esc_html__('Enable if you would like to print merged scripts into the footer, instead of a seperated file.', 'swift-performance'),
                             'required'   => array('merge-scripts', '=', 1),
                        ),
                        array(
                             'id'         => 'lazy-load-scripts',
                             'type'       => 'multi_text',
                             'title'	=> esc_html__('Lazy Load Scripts (BETA)', 'swift-performance'),
                             'subtitle'   => esc_html__('Load scripts only after first user interaction, if one of these strings is found in the match.', 'swift-performance'),
                             'required'   => array('merge-scripts', '=', 1),
                        ),
                  )
            )
      );

      ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'Styles', 'swift-performance' ),
                 'id' => 'asset-manager-css',
                 'subsection' => true,
                 'fields' => array (
                        array(
                             'id'         => 'merge-styles',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Merge Styles', 'swift-performance' ),
                             'subtitle'   => esc_html__('Merge CSS files to reduce number of HTML requests', 'swift-performance'),
                             'default'    => 0,
                             'ajax_save' => false
                        ),
                        array(
                             'id'         => 'critical-css',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Generate Critical CSS', 'swift-performance' ),
                             'default'    => 1,
                             'required'   => array('merge-styles', '=', 1),
                        ),
                        array(
                             'id'         => 'compress-css',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Compress CSS', 'swift-performance' ),
                             'subtitle'	=> esc_html__( 'Extra compress for critical CSS (BETA)', 'swift-performance' ),
                             'default'    => 0,
                             'required'   => array(
                                   array('merge-styles', '=', 1),
                                   array('critical-css', '=', 1),
                             ),
                        ),
                        array(
                             'id'         => 'remove-keyframes',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Remove Keyframes', 'swift-performance' ),
                             'subtitle'	=> esc_html__( 'Remove CSS animations from critical CSS', 'swift-performance' ),
                             'default'    => 1,
                             'required'   => array(
                                   array('merge-styles', '=', 1),
                                   array('critical-css', '=', 1),
                             ),
                        ),
                        array(
                             'id'         => 'inline_critical_css',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__('Print critical CSS inline', 'swift-performance'),
                             'subtitle'   => esc_html__('Enable if you would like to print the critical CSS into the header, instead of a seperated CSS file.', 'swift-performance'),
                             'required'   => array(
                                   array('merge-styles', '=', 1),
                                   array('critical-css', '=', 1),
                             ),
                             'default'    => 1,
                        ),
                        array(
                             'id'         => 'inline_full_css',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__('Print full CSS inline', 'swift-performance'),
                             'subtitle'   => esc_html__('Enable if you would like to print the merged CSS into the footer, instead of a seperated CSS file.', 'swift-performance'),
                             'required'   => array('merge-styles', '=', 1),
                             'default'    => 1,
                        ),
                        array(
                             'id'         => 'minify-css',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__( 'Minify CSS', 'swift-performance' ),
                             'default'    => 1,
                             'required'   => array('merge-styles', '=', 1),
                        ),
                        array(
                             'id'         => 'bypass-css-import',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__('Bypass CSS Import', 'swift-performance'),
                             'subtitle'   => esc_html__('Include imported CSS files in merged styles.', 'swift-performance'),
                             'default'    => 0,
                             'required'   => array('merge-styles', '=', 1),
                        ),
                        array(
                             'id'         => 'merge-styles-exclude-3rd-party',
                             'type'       => 'checkbox',
                             'title'	=> esc_html__('Exclude 3rd Party CSS', 'swift-performance'),
                             'subtitle'   => esc_html__('Exclude 3rd party CSS files (eg: Google Fonts CSS) from merged styles', 'swift-performance'),
                             'required'   => array('merge-styles', '=', 1),
                             'default'    => 0,
                        ),
                        array(
                             'id'         => 'exclude-styles',
                             'type'       => 'multi_text',
                             'title'	=> esc_html__( 'Exclude Styles', 'swift-performance' ),
                             'subtitle'   => esc_html__('Exclude style from being merged if one of these strings is found in the match. ', 'swift-performance'),
                             'required'   => array('merge-styles', '=', 1),
                        ),
                        array(
                             'id'         => 'include-styles',
                             'type'       => 'multi_text',
                             'title'	=> esc_html__( 'Include Styles', 'swift-performance' ),
                             'subtitle'   => esc_html__('Include styles manually. With this option you can preload css files what are loaded with javascript', 'swift-performance'),
                             'required'   => array('merge-styles', '=', 1),
                        ),
                  )
            )
      );

      ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'Caching', 'swift-performance' ),
                 'id' => 'chache-tab',
                 'icon' => 'el el-graph',
                 'fields' => array (
                       array(
                             'id'         => 'enable-caching',
                             'type'	      => 'checkbox',
                             'title'      => esc_html__( 'Enable Caching', 'swift-performance' ),
                             'default'    => 1,
                             'ajax_save' => false
                       ),
                       array(
                             'id'                => 'caching-mode',
                             'type'              => 'select',
                             'title'	       => esc_html__( 'Caching Mode', 'swift-performance' ),
                             'options'           => $cache_modes,
                             'default'           => 'disk_cache_php',
                             'required'          => array('enable-caching', '=', 1),
                             'validate_callback' => 'swift_performance_validate_cache_mode_callback'
                       ),
                       array(
                              'id'	      => 'memcached-host',
                              'type'	=> 'text',
                              'title'	=> esc_html__( 'Memcached Host', 'swift-performance' ),
                              'default'   => 'localhost',
                              'required'  => array(
                                    array('caching-mode', '=', 'memcached_php'),
                                    array('enable-caching', '=', 1)
                              ),
                       ),
                       array(
                              'id'	      => 'memcached-port',
                              'type'	=> 'text',
                              'title'	=> esc_html__( 'Memcached Port', 'swift-performance' ),
                              'default'   => '11211',
                              'required'  => array(
                                    array('caching-mode', '=', 'memcached_php'),
                                    array('enable-caching', '=', 1)
                              ),
                       ),
                       array(
                              'id'	      => 'early-load',
                              'type'	=> 'checkbox',
                              'title'	=> esc_html__( 'Early Loader', 'swift-performance' ),
                              'subtitle'  => esc_html__( 'Use Swift Performance Loader mu-plugin ', 'swift-performance' ),
                              'default'   => 1,
                              'required'  => array(
                                    array('caching-mode', '!=', 'disk_cache_rewrite'),
                                    array('enable-caching', '=', 1)
                              ),
                              'validate_callback' => 'swift_performance_validate_muplugins_callback',
                       ),
                       array(
                              'id'	      => 'cache-path',
                              'type'	=> 'text',
                              'title'	=> esc_html__( 'Cache Path', 'swift-performance' ),
                              'default'   => WP_CONTENT_DIR . '/cache/',
                              'required'  => array(
                                    array('caching-mode', 'contains', 'disk_cache'),
                                    array('enable-caching', '=', 1)
                              ),
                              'validate_callback' => 'swift_performance_validate_cache_path_callback',
                       ),
                       array(
                            'id'         => 'cache-expiry-mode',
                            'type'       => 'select',
                            'title'	     => esc_html__( 'Cache Expiry Mode', 'swift-performance' ),
                            'required'   => array('enable-caching', '=', 1),
                            'options'    => array(
                                  'timebased'   => esc_html__('Time based mode', 'swift-performance'),
                                  'intelligent' => esc_html__('Intelligent mode', 'swift-performance'),
                            ),
                            'default'    => 'timebased'
                       ),
                       array(
                              'id'	      => 'cache-expiry-time',
                              'type'	=> 'text',
                              'title'	=> esc_html__( 'Cache Expiry Time', 'swift-performance' ),
                              'subtitle'  => esc_html__( 'Cache expiry time in seconds', 'swift-performance' ),
                              'default'   => '1800',
                              'required'  => array('cache-expiry-mode', '=', 'timebased')
                       ),
                       array(
                              'id'	      => 'cache-garbage-collection-time',
                              'type'	=> 'text',
                              'title'	=> esc_html__( 'Garbage Collection Interval', 'swift-performance' ),
                              'subtitle'  => esc_html__( 'How often should check the expired cached pages (in seconds)', 'swift-performance' ),
                              'default'   => '300',
                              'required'  => array('cache-expiry-mode', '=', 'timebased')
                       ),
                       array(
                           'id'          => 'automated_prebuild_cache',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Prebuild Cache Automatically', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'This option will prebuild the cache after it was cleared', 'swift-performance' ),
                           'default'     => 0,
                           'required'  => array('cache-expiry-mode', '=', 'timebased')
                       ),
                       array(
                           'id'          => 'resource-saving-mode',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Resource saving mode', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'This option will reduce intelligent cache check requests. Recommended for limited resource severs', 'swift-performance' ),
                           'default'     => 1,
                           'required'  => array('cache-expiry-mode', '=', 'intelligent')
                       ),
                       array(
                           'id'          => 'disable-instant-reload',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Disable Instant Reload', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'If you disable instant reload the plugin will override the cache if intelligent cache detect changes, however it won\'t replace the page content instantly for the user.', 'swift-performance' ),
                           'default'     => 1,
                           'required'  => array('cache-expiry-mode', '=', 'intelligent')
                       ),
                       array(
                           'id'          => 'enable-caching-logged-in-users',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Enable Caching for logged in users', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'This option can increase the total cache size, depending on the count of your users.', 'swift-performance' ),
                           'default'     => 0,
                           'required'  => array('cache-expiry-mode', '=', 'intelligent')
                       ),
                       array(
                           'id'          => 'mobile-support',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Enable Mobile Device Support', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'You can create separate cache for mobile devices, it can be useful if your site not just responsive, but it has a separate mobile theme/layout (eg: AMP). ', 'swift-performance' ),
                           'default'     => 0,
                           'required'    => array('enable-caching', '=', 1),
                       ),
                       array(
                           'id'          => 'browser-cache',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Enable Browser Cache', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'If you enable this option it will generate htacess/nginx rules for browser cache. (Expire headers should be configured on your server as well)', 'swift-performance' ),
                           'default'     => 1,
                           'required'   => array('enable-caching', '=', 1),
                           'ajax_save' => false
                       ),
                       array(
                           'id'          => 'enable-gzip',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Enable Gzip', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'If you enable this option it will generate htacess/nginx rules for gzip compression. (Compression should be configured on your server as well)', 'swift-performance' ),
                           'default'     => 1,
                           'required'   => array('enable-caching', '=', 1),
                           'ajax_save' => false
                       ),
                       array(
                           'id'          => '304-header',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Send 304 Header', 'swift-performance' ),
                           'default'     => 0,
                           'required'   => array('enable-caching', '=', 1),
                       ),
                       array(
                           'id'          => 'cache-404',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Cache 404 pages', 'swift-performance' ),
                           'default'     => 1,
                           'required'   => array('enable-caching', '=', 1),
                       ),
                       array(
                           'id'         => 'exclude-pages',
                           'type'       => 'select',
                           'multi'      => true,
                           'title'      => esc_html__( 'Exclude Pages', 'swift-performance' ),
                           'subtitle'   => esc_html__( 'Select pages which shouldn\'t be cached.', 'swift-performance' ),
                           'required'   => array('enable-caching', '=', 1),
                           'options'    => $reduxsa_pages
                       ),
                       array(
                           'id'         => 'exclude-strings',
                           'type'       => 'multi_text',
                           'title'      => esc_html__( 'Exclude URLs', 'swift-performance' ),
                           'subtitle'   => esc_html__( 'URLs which contains that string won\'t be cached. Use leading/trailing # for regex', 'swift-performance' ),
                           'required'   => array('enable-caching', '=', 1),
                           'default'    => 0,
                       ),
                       array(
                           'id'         => 'exclude-content-parts',
                           'type'       => 'multi_text',
                           'title'      => esc_html__( 'Exclude Content Parts', 'swift-performance' ),
                           'subtitle'   => esc_html__( 'Pages which contains that string won\'t be cached. Use leading/trailing # for regex', 'swift-performance' ),
                           'required'   => array('enable-caching', '=', 1),
                           'default'    => 0,
                       ),
                       array(
                           'id'         => 'exclude-useragents',
                           'type'       => 'multi_text',
                           'title'      => esc_html__( 'Exclude User Agents', 'swift-performance' ),
                           'subtitle'   => esc_html__( 'User agents which contains that string won\'t be cached. Use leading/trailing # for regex', 'swift-performance' ),
                           'required'   => array('enable-caching', '=', 1),
                           'default'    => 0,
                       ),
                       array(
                           'id'          => 'exclude-crawlers',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Exclude Crawlers', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'Exclude known crawlers from cache', 'swift-performance' ),
                           'default'     => 0,
                           'required'   => array('enable-caching', '=', 1),
                       ),
                       array(
                           'id'          => 'dynamic-caching',
                           'type'        => 'checkbox',
                           'title'       => esc_html__( 'Enable Dynamic Caching', 'swift-performance' ),
                           'subtitle'    => esc_html__( 'If you enable this option you can specify cacheable $_GET and $_POST requests', 'swift-performance' ),
                           'default'     => 0,
                           'required'   => array('enable-caching', '=', 1),
                       ),
                       array(
                           'id'         => 'cacheable-dynamic-requests',
                           'type'       => 'multi_text',
                           'title'      => esc_html__( 'Cacheable Dynamic Requests', 'swift-performance' ),
                           'subtitle'   => esc_html__( 'Specify $_GET and/or $_POST keys what should be cached. Eg: "s" to cache search requests', 'swift-performance' ),
                           'default'    => 0,
                           'required'   => array('dynamic-caching', '=', 1)
                       ),
                       array(
                           'id'         => 'cacheable-ajax-actions',
                           'type'       => 'multi_text',
                           'title'      => esc_html__( 'Cacheable AJAX Actions', 'swift-performance' ),
                           'subtitle'   => esc_html__( 'With this option you can cache resource-intensive AJAX requests', 'swift-performance' ),
                           'default'    => 0,
                           'required'   => array('enable-caching', '=', 1)
                       ),
                       array(
                           'id'         => 'ajax-cache-expiry-time',
                           'type'	    => 'text',
                           'title'	    => esc_html__( 'AJAX Cache Expiry Time', 'swift-performance' ),
                           'subtitle'   => esc_html__( 'Cache expiry time for AJAX requests in seconds', 'swift-performance' ),
                           'default'    => '1440',
                           'required'  => array(
                                 array('cache-expiry-mode', '=', 'timebased')
                           )
                       ),
                  )
            )
      );

     ReduxSA::setSection ( $opt_name, array (
                 'title' => esc_html__( 'CDN', 'swift-performance' ),
                 'desc' => __('Speed up your website with', 'swift-performance').' <a href="//tracking.maxcdn.com/c/258716/3968/378" target="_blank">MaxCDN</a>',
                 'id' => 'cdn-tab',
                 'icon' => 'el el-tasks',
                 'fields' => array (
                       array(
                                   'id'	=> 'enable-cdn',
                                   'type'	=> 'checkbox',
                                   'title' => esc_html__( 'Enable CDN', 'swift-performance' ),
                                   'default' => 0
                       ),
                       array(
                                   'id'	=> 'cdn-hostname-master',
                                   'type'	=> 'text',
                                   'title'	=> esc_html__( 'CDN Hostname', 'swift-performance' ),
                                   'required' => array('enable-cdn', '=', 1)
                       ),
                       array(
                                   'id'	=> 'cdn-hostname-slot-1',
                                   'type'	=> 'text',
                                   'title' => esc_html__( 'CDN Hostname for Javascript ', 'swift-performance' ),
                                   'required' => array('cdn-hostname-master', '!=', ''),
                                   'subtitle' => esc_html__('Use different hostname for javascript files', 'swift-performance'),
                       ),
                       array(
                                   'id'	=> 'cdn-hostname-slot-2',
                                   'type'	=> 'text',
                                   'title'	=> esc_html__( 'CDN Hostname for Media files', 'swift-performance' ),
                                   'required' => array('cdn-hostname-slot-1', '!=', ''),
                                   'subtitle' => esc_html__('Use different hostname for media files', 'swift-performance'),
                       ),
                       array(
                                   'id'	=> 'enable-cdn-ssl',
                                   'type'	=> 'checkbox',
                                   'title'	=> esc_html__( 'Enable CDN on SSL', 'swift-performance' ),
                                   'default' => 0,
                                   'subtitle' => esc_html__('You can specify different hostname(s) for SSL, or leave them blank for use the same host on HTTP and SSL', 'swift-performance'),
                                   'required' => array('enable-cdn', '=', 1)
                       ),
                       array(
                                   'id'	=> 'cdn-hostname-master-ssl',
                                   'type'	=> 'text',
                                   'title'	=> esc_html__( 'SSL CDN Hostname', 'swift-performance' ),
                                   'required' => array('enable-cdn-ssl', '=', 1)
                       ),
                       array(
                                   'id'	=> 'cdn-hostname-slot-1-ssl',
                                   'type'	=> 'text',
                                   'title'	=> esc_html__( 'CDN Hostname for Javascript ', 'swift-performance' ),
                                   'required' => array('cdn-hostname-master-ssl', '!=', ''),
                                   'subtitle' => esc_html__('Use different hostname for javascript files', 'swift-performance'),
                       ),
                       array(
                                   'id'	=> 'cdn-hostname-slot-2-ssl',
                                   'type'	=> 'text',
                                   'title'	=> esc_html__( 'CDN Hostname for Media files', 'swift-performance' ),
                                   'required' => array('cdn-hostname-slot-1-ssl', '!=', ''),
                                   'subtitle' => esc_html__('Use different hostname for media files', 'swift-performance'),
                       ),
                       array(
                                   'id'	=> 'maxcdn-alias',
                                   'type'	=> 'text',
                                   'title'	=> esc_html__( 'MAXCDN Alias', 'swift-performance' ),
                                   'required' => array('enable-cdn', '=', 1),
                       ),
                       array(
                                   'id'	=> 'maxcdn-key',
                                   'type'	=> 'text',
                                   'title'	=> esc_html__( 'MAXCDN Consumer Key', 'swift-performance' ),
                                   'required' => array('enable-cdn', '=', 1),
                       ),
                       array(
                                   'id'	=> 'maxcdn-secret',
                                   'type'	=> 'text',
                                   'title'	=> esc_html__( 'MAXCDN Consumer Secret', 'swift-performance' ),
                                   'required' => array('enable-cdn', '=', 1),
                       ),
                  )
            )
      );

    /*
     *
     * ---> END SECTIONS
     *
     */

     add_action( 'admin_menu', 'remove_reduxsa_menu',12 );
     function remove_reduxsa_menu() {
         remove_submenu_page('tools.php','reduxsa-about');
     }

?>
