jQuery(function(){
      var _interval;

      function clear_messages(){
            jQuery('.swift-message').empty();
            jQuery('.swift-message').attr('class', 'swift-message swift-hidden');
      }

      // Clear messages if any buttons was clicked
      jQuery(document).on('click', '.swift-performance-control', clear_messages);

      // Clear cache
      jQuery(document).on('click', '#swift-performance-clear-cache', function(e){
            jQuery('body').addClass('swift-loading');
            jQuery.post(ajaxurl, {action: 'swift_performance_clear_cache', '_wpnonce' : swift_performance.nonce}, function(response){
                  jQuery('.swift-message').removeClass('swift-hidden');
                  jQuery('.swift-message').addClass(response.type).text(response.text);
                  jQuery('body').removeClass('swift-loading');
                  setTimeout(clear_messages,2000);
            });
            e.preventDefault();
      });

      // Clear assets cache
      jQuery(document).on('click', '#swift-performance-clear-assets-cache', function(e){
            jQuery('body').addClass('swift-loading');
            jQuery.post(ajaxurl, {action: 'swift_performance_clear_assets_cache', '_wpnonce' : swift_performance.nonce}, function(response){
                  jQuery('.swift-message').removeClass('swift-hidden');
                  jQuery('.swift-message').addClass(response.type).text(response.text);
                  jQuery('body').removeClass('swift-loading');
                  setTimeout(clear_messages,2000);
            });
            e.preventDefault();
      });

      // Prebuild cache
      jQuery(document).on('click', '#swift-performance-prebuild-cache', function(e){
            jQuery('body').addClass('swift-loading');
            jQuery.post(ajaxurl, {action: 'swift_performance_prebuild_cache', '_wpnonce' : swift_performance.nonce}, function(response){
                  jQuery('.swift-message').removeClass('swift-hidden');
                  jQuery('.swift-message').addClass(response.type).text(response.text);
                  jQuery('body').removeClass('swift-loading');
                  setTimeout(clear_messages,2000);
            });
            e.preventDefault();
      });

      // Show Rewrite Rules
      jQuery(document).on('click', '#swift-performance-show-rewrite', function(e){
            clearInterval(_interval);

            jQuery('body').addClass('swift-loading');
            jQuery.post(ajaxurl, {action: 'swift_performance_show_rewrites', '_wpnonce' : swift_performance.nonce}, function(response){
                  if (typeof response.text !== 'undefined' && response.text.length > 0){
                        jQuery('.swift-message').removeClass('swift-hidden');
                        jQuery('.swift-message').addClass(response.type).text(response.text);
                  }

                  jQuery('.swift-box').removeClass('swift-hidden');
                  jQuery('.swift-box h3').text(response.title);
                  jQuery('.swift-box pre').text(response.rewrites);
                  jQuery('body').removeClass('swift-loading');
            });
            e.preventDefault();
      });

      // Show Cache Status
      jQuery(document).on('click', '#swift-performance-cache-status', function(e){
            clearInterval(_interval);

            jQuery('body').addClass('swift-loading');
            jQuery.post(ajaxurl, {action: 'swift_performance_cache_status', '_wpnonce' : swift_performance.nonce}, function(response){
                  if (typeof response.text !== 'undefined' && response.text.length > 0){
                        jQuery('.swift-message').removeClass('swift-hidden');
                        jQuery('.swift-message').addClass(response.type).text(response.text);
                  }

                  jQuery('.swift-box').removeClass('swift-hidden');
                  jQuery('.swift-box h3').text(response.title);
                  jQuery('.swift-box pre').text(response.status);
                  jQuery('body').removeClass('swift-loading');
            });
            _interval = setInterval(function(){
                  jQuery.post(ajaxurl, {action: 'swift_performance_cache_status', '_wpnonce' : swift_performance.nonce}, function(response){
                        jQuery('.swift-box pre').text(response.status);
                  });
            }, 5000);
            e.preventDefault();
      });

      // Show Log
      jQuery(document).on('click', '#swift-performance-log', function(e){
            clearInterval(_interval);

            jQuery('body').addClass('swift-loading');
            jQuery.post(ajaxurl, {action: 'swift_performance_show_log', '_wpnonce' : swift_performance.nonce}, function(response){
                  if (typeof response.text !== 'undefined' && response.text.length > 0){
                        jQuery('.swift-message').removeClass('swift-hidden');
                        jQuery('.swift-message').addClass(response.type).text(response.text);
                  }

                  jQuery('.swift-box').removeClass('swift-hidden');
                  jQuery('.swift-box h3').text(response.title);
                  jQuery('.swift-box pre').text(response.status);
                  jQuery('body').removeClass('swift-loading');
            });
            _interval = setInterval(function(){
                  jQuery.post(ajaxurl, {action: 'swift_performance_show_log', '_wpnonce' : swift_performance.nonce}, function(response){
                        jQuery('.swift-box pre').text(response.status);
                  });
            }, 5000);
            e.preventDefault();
      });

      // Redux toggle all checkboxes
      jQuery(document).on('click', '.reduxsa-toggle-all', function(){
            jQuery(this).closest('ul').find('input[type="checkbox"]').trigger('click');
      });
});
