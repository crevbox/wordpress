<?php
/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Buddypress_Share
 * @subpackage Buddypress_Share/includes
 * @author     Wbcom Designs <admin@wbcomdesigns.com>
 */

class Buddypress_Share_Deactivator {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     * @access public 
     * @since    1.0.0
     */
    public static function deactivate() {

    }

}
