=== BuddyPress Activity Social Share ===
Contributors: vapvarun,wbcomdesigns
Donate link: https://wbcomdesigns.com
Tags: buddypress,activity,share
Requires at least: 3.0.1
Tested up to: 4.8.1
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This Plugin will extend the BuddyPress functionality and share activity post to social sites.

== Description ==

This Plugin will add an extended feature to the big name “BuddyPress” that will allow to share Activity “Post Updates” to the social sites.
* [Plugin Homepage](https://wbcomdesigns.com/downloads/buddypress-activity-social-share/)
* [FAQ](https://wbcomdesigns.com/helpdesk/article-categories/buddypress-activity-social-share/)


== Installation ==
This section describes how to install the plugin and get it working.

1. Download the zip file and extract it.
2. Upload `bp-activity-social-share` directory to the `/wp-content/plugins/` directory
3. Activate the plugin through the \'Plugins\' menu.
4. Alternatively you can use WordPress Plugin installer from Dashboard->Plugins->Add New to add this plugin
5. Enjoy
If you need additional help you can contact us for [Custom Development](https://wbcomdesigns.com/hire-us/).

== Frequently Asked Questions ==

= Does This plugin requires BuddyPress =
Yes, It needs you to have BuddyPress installed and activated.

= Where Do I Ask for support? =
Please visit [wbcomdesigns] (http://wbcomdesigns.com/) for any query related to plugin and Buddypress.

== Screenshots ==

1. It is displaying default social services under share button after activation screenshot-1.png
2. It is displaying default plugin setting and enabled services in admin setting page after activation screenshot-2.png
3. This screenshot shows share button for non-logged in site visitors and corresponds to screenshot-3.png.

== Changelog ==
= 1.0.3 =
* Fix - Translation Fixes
* Enhancement - Enable sharing for logged out users
* Enhancement - Allow to add more social icons
= 1.0.2 =
* Third Version.
= 1.0.1 =
* Second Version.
= 1.0.0 =
* first version.
= 1.0.0 =
* JS fixes for BP 2.7.
