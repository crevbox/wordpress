��    !      $  /   ,      �     �     �            	   /  q   9     �     �     �     �  �   �     i     �  U   �  d   �     H     Q  	   f     p     �     �     �     �     �     �     �  _   �     4     L  	   T  ;   ^     �  �  �     i     p     �  !   �  	   �  �   �     �	     �	     �	     �	  �   �	     {
     �
  b   �
  p        s  *   |  	   �  (   �     �     �     �               +     4  a   B     �     �  	   �  J   �             	                                                                                    
      !                                                             Action Add Social Services BuddyPress Share BuddyPress Share Settings Component Default is set to open window in popup. If this option is disabled then services open in new tab instead popup.   Description Email Extra Options Facebook Facebook is an American for-profit corporation and online social media and social networking service based in Menlo Park, California, United States. Font Awesome Icon Class Google Plus Google Plus is an interest-based social network that is owned and operated by Google. LinkedIn is a business and employment-oriented social networking service that operates via websites. Linkedin Open as popup window Pinterest Please enable share services! Pocket Reddit Save Changes Select Service Settings Share Social Sites Sorry, but this plugin requires the BuddyPress Plugin to be installed and active. <br><a href=" This field is required! Twitter WordPress You do not have sufficient permissions to access this page. select Project-Id-Version: Buddypress Activity Share
POT-Creation-Date: 2017-05-29 17:59+0530
PO-Revision-Date: 2017-05-29 18:00+0530
Last-Translator: 
Language-Team: Wbcomdesigns
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: languages
X-Poedit-SearchPath-1: .
 Action Ajouter des services sociaux BuddyPress Partager BuddyPress Paramètres de partage Composant La valeur par défaut est configurée pour ouvrir la fenêtre dans la fenêtre contextuelle. Si cette option est désactivée, Les services s'ouvrent dans un nouvel onglet à la place des fenêtres contextuelles. La description Email Options supplémentaires Facebook Facebook est une société américaine à but lucratif et des médias sociaux en ligne et Service de réseau social basé à Menlo Park, Californie, États-Unis. Font Awesome Icon Class Google Plus Google Plus est un réseau social axé sur les intérêts qui est détenu et exploité Par Google. LinkedIn est un service de réseau social axé sur les entreprises et l'emploi Qui fonctionne via les sites Web. Linkedin Ouvrir sous forme de fenêtre contextuelle Pinterest Veuillez activer les services partagés! Pocket Reddit Sauvegarder les modifications Sélectionnez le service Paramètres Partager Sites sociaux Désolé, mais ce plugin nécessite l'installation du plugin BuddyPress et Ctif. <br> <a href = " Ce champ est requis! Twitter WordPress Vous n'avez pas les autorisations suffisantes pour accéder à cette page. sélectionner 